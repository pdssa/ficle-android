package com.pds.boletaelectronica;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Formatos;
import com.pds.common.Usuario;
import com.pds.common.VentasHelper;
import com.pds.common.activity.TimerActivity;
import com.pds.common.dao.VentaDao;
import com.pds.common.db.ProductTable;
import com.pds.common.model.Venta;

import java.text.DecimalFormat;
import java.util.List;


public class ConsultarVentasActivity extends TimerActivity {

    private Config config;
    private Usuario _user;
    private ListView listVentas;
    private DecimalFormat FORMATO_DECIMAL;
    private View btnSearchVentas;
    private EditText ingresoVenta;
    private  View searchText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultar_ventas);

        try {

            //Inicializo las Views
            Init_Views();

            //Inicializo los métodos
            Init_Metodos();

            //customizar por pais
            Init_CustomConfigPais(config.PAIS);

        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        obtieneListaVentas("-1");
    }

    private void Init_Views() {
        config = new Config(this);

        listVentas = (ListView) findViewById(R.id.lvVentas);
        listVentas.setEmptyView(findViewById(android.R.id.empty));
        btnSearchVentas = findViewById(R.id.btn_cons_caf_busq);
        ingresoVenta = (EditText) findViewById(R.id.txt_cons_caf_busq);
    }

    private void Init_Metodos() {
        btnSearchVentas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //realizar una busqueda con el numero ingreso en el EditText
                if (ingresoVenta.getText().toString().equals("")){
                   obtieneListaVentas("-1");
                }else{
                    obtieneListaVentas(ingresoVenta.getText().toString());
                }
                //cerramos el teclado

                ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(ingresoVenta.getWindowToken(), 0);
            }
        });
    }


    private void Init_CustomConfigPais(String codePais) {

        FORMATO_DECIMAL = codePais.equals("AR") ? Formatos.DecimalFormat : Formatos.DecimalFormat_CH;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_consultar_ventas, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void obtieneListaVentas(String numeroVenta) {
        List<Venta> list = null;

        try {
            if (numeroVenta.equals("-1")) {
                //Busco todas las ventas por fecha DESC
                list = new VentaDao(getContentResolver()).list(null, null, "_id DESC", null, null, null);
            } else {
                //busco una venta específica según el numero de venta ingresado
                list = new VentaDao(getContentResolver()).list("_id = " + numeroVenta + " or " + VentasHelper.VENTA_FC_FOLIO + " = " + numeroVenta, null, "_id DESC", null, null, null);
            }

        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }
        cargaListaVentas(list);
    }

    class ConsultaVentasAdapter extends ArrayAdapter<Venta> {
        private Context context;
        private List<Venta> datos;

        public ConsultaVentasAdapter(Context context, List<Venta> datos) {
            super(context, android.R.layout.simple_list_item_1, datos);

            this.context = context;
            this.datos = datos;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // En primer lugar "inflamos" una nueva vista, que será la que se
            // mostrará en la celda del ListView. Para ello primero creamos el
            // inflater, y después inflamos la vista.
            if (convertView == null) {
                convertView = LayoutInflater.from(context)
                        .inflate(R.layout.list_item_venta, parent, false);
            }

            Venta row = datos.get(position);

            // A partir de la vista, recogeremos los controles que contiene para
            // poder manipularlos.
            // Recogemos los TextView para mostrar datos
            ((TextView) convertView.findViewById(R.id.cv_item_num_venta)).setText(row.getNroVenta());
            ((TextView) convertView.findViewById(R.id.cv_item_monto)).setText(Formatos.FormateaDecimal(row.getTotal(), FORMATO_DECIMAL, "$"));
            ((TextView) convertView.findViewById(R.id.cv_item_medio_pago)).setText(row.getMedio_pago());
            ((TextView) convertView.findViewById(R.id.cv_item_fecha)).setText(Formatos.FormateaDate(row.getFechaHora_Date(), Formatos.FullDateTimeFormatNoSeconds));

            return convertView;
        }
    }

    private void cargaListaVentas(List<Venta> list){

        ConsultaVentasAdapter adapter = new ConsultaVentasAdapter(this, list);
        listVentas.setAdapter(adapter);
    }
}
