package com.pds.boletaelectronica;

import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Formatos;
import com.pds.common.VentasHelper;
import com.pds.common.activity.TimerActivity;
import com.pds.common.dao.TipoDocDao;
import com.pds.common.dao.VentaDao;
import com.pds.common.db.TipoDocTable;
import com.pds.common.model.TipoDoc;
import com.pds.common.model.Venta;
import com.pds.common.receiver.SyncService;

import java.util.List;


public class DocumentosActivity extends TimerActivity {

    List<Venta> lista;
    ListView lvwDocumentos;
    Spinner spnTiposDoc;
    TipoDoc tipoDoc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_documentos);

        findViewById(R.id.documentos_back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        spnTiposDoc = (Spinner) findViewById(R.id.documentos_search_spinner);
        //spnTiposDoc.setOnItemSelectedListener();

        lvwDocumentos = (ListView) findViewById(R.id.documentos_list);
        lvwDocumentos.setEmptyView(findViewById(android.R.id.empty));
        /*lvwDocumentos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (findViewById(R.id.fragment) != null) {
                    //agregamos los detalles a la venta
                    Venta venta = lista.get(position);
                    venta.addDetalles(getContentResolver());

                    PreviewDocFragment fragment = PreviewDocFragment.newInstance(venta);
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment, fragment);
                    ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
                    ft.commit();
                }
            }
        });*/

        lvwDocumentos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Venta vta = (Venta) lvwDocumentos.getItemAtPosition(position);
                if ((vta.getFc_validation() != null) && (vta.getFc_id() == -1) && (vta.getSync().equals("S")))
                    Toast.makeText(getApplicationContext(), vta.getFc_validation(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void cargaSpinnerTipoDoc() {

        List<TipoDoc> listTipoDoc = new TipoDocDao(getContentResolver()).list("habilitado == 1", null, null);
        listTipoDoc.add(0, new TipoDoc("-1", "Seleccione..."));

        ArrayAdapter<TipoDoc> tipoDocAdapter = new ArrayAdapter<TipoDoc>(this, android.R.layout.simple_spinner_item, listTipoDoc);
        tipoDocAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnTiposDoc.setAdapter(tipoDocAdapter);

        this.addSeletectItemListeners();
    }

    private void addSeletectItemListeners() {
        spnTiposDoc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tipoDoc = (TipoDoc) spnTiposDoc.getItemAtPosition(position);
                cargarListaDeVentas(tipoDoc.getCodigo());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        cargarListaDeVentas("-1");
        cargaSpinnerTipoDoc();
    }

    private void cargarListaDeVentas(String tipoDoc) {
        //ventas que tengan folio asociado
        if (tipoDoc.equals("-1")) {
            if (lista == null) {
                lista = new VentaDao(getContentResolver()).list(VentasHelper.VENTA_FC_FOLIO + "<> ''", null, "_id DESC");
                lvwDocumentos.setAdapter(new DocumentosArrayAdapter(this, lista));
            } else {
                lista.clear();
                lista.addAll(new VentaDao(getContentResolver()).list(VentasHelper.VENTA_FC_FOLIO + "<> ''", null, "_id DESC"));
                ((DocumentosArrayAdapter) lvwDocumentos.getAdapter()).notifyDataSetChanged();
            }
        } else {
            if (lista == null) {
                lista = new VentaDao(getContentResolver()).list(VentasHelper.VENTA_FC_FOLIO + "<> '' AND " + VentasHelper.VENTA_FC_TYPE + "==" + tipoDoc, null, "_id DESC");
                lvwDocumentos.setAdapter(new DocumentosArrayAdapter(this, lista));
            } else {
                lista.clear();
                lista.addAll(new VentaDao(getContentResolver()).list(VentasHelper.VENTA_FC_FOLIO + "<> '' AND " + VentasHelper.VENTA_FC_TYPE + "==" + tipoDoc, null, "_id DESC"));
                ((DocumentosArrayAdapter) lvwDocumentos.getAdapter()).notifyDataSetChanged();
            }
        }
    }

    public class DocumentosArrayAdapter extends ArrayAdapter<Venta> {
        private final Context context;
        private final List<Venta> ventas;

        public DocumentosArrayAdapter(Context context, List<Venta> list) {
            super(context, R.layout.documento_item, list);
            this.context = context;
            this.ventas = list;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View rowView = inflater.inflate(R.layout.documento_item, parent, false);
            /*if (position % 2 != 0)
                rowView.setBackgroundColor(Color.LTGRAY);
            else
                rowView.setBackgroundColor(Color.WHITE);*/

            if ((ventas.get(position).getFc_id() == -1) && (ventas.get(position).getSync().equals("N")))
                rowView.setBackgroundColor(Color.LTGRAY);//documento aún no sincronizado
            else if ((ventas.get(position).getFc_id() == -1) && (ventas.get(position).getSync().equals("S")))
                if (ventas.get(position).getFc_validation() == null) {
                    rowView.setBackgroundColor(Color.YELLOW);//documento enviado aún pendiente de conocer estado
                } else {
                    rowView.setBackgroundColor(Color.RED);//documento enviado pero devuelto con error
                }
            else
                rowView.setBackgroundColor(Color.GREEN);//documento generado OK en FM

            TextView fechaTextView = (TextView) rowView.findViewById(R.id.documento_fecha);
            fechaTextView.setText(ventas.get(position).getFecha());

            TextView folioTextView = (TextView) rowView.findViewById(R.id.documento_folio);
            folioTextView.setText(ventas.get(position).getFc_folio());

            TextView totalTextView = (TextView) rowView.findViewById(R.id.documento_total);
            totalTextView.setText(Formatos.FormateaDecimal(ventas.get(position).getTotal(), Formatos.DecimalFormat_CH, "$"));

            //Agrego el tipo de Documento
            TipoDoc tipoDoc = obtieneTipoDocumento(ventas.get(position).getFc_type());

            TextView tipoDocTextView = (TextView) rowView.findViewById(R.id.documento_tipo_doc);
            tipoDocTextView.setText(tipoDoc == null ? "" : tipoDoc.getNombre());

            return rowView;
        }
    }

    private TipoDoc obtieneTipoDocumento(String codTipoDoc) {
        TipoDoc tipoDoc = null;

        try {
            List<TipoDoc> tipoDocs = new TipoDocDao(getContentResolver()).list(TipoDocTable.COLUMN_CODIGO + " = '" + codTipoDoc + "'", null, null, null, null, null);

            if (tipoDocs.size() > 0) {
                tipoDoc = tipoDocs.get(0);
            }

        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }

        return tipoDoc;
    }
}
