package com.pds.boletaelectronica;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

//import com.onbarcode.barcode.android.AndroidColor;
//import com.onbarcode.barcode.android.IBarcode;
//import com.onbarcode.barcode.android.PDF417;
import com.exavore.PDF417.PDF417;
import com.exavore.PDF417.PDF417_eccLevel;
import com.exavore.PDF417.PDF417_size;
import com.pds.common.Config;
import com.pds.common.dao.VentaDao;
import com.pds.boletaelectronicalibrary.NuevaBoletaFragment;
import com.pds.common.model.Venta;

import android_serialport_api.Printer;
import android_serialport_api.TicketBuilder;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PreviewDocFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PreviewDocFragment extends Fragment {
    private static final String ARG_PARAM1 = "venta";

    private Venta venta;
    private WebView webView;
    private Button btnSync;
    private ImageView image;

    private boolean print = false;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param _venta Parameter 1.
     * @return A new instance of fragment PreviewDocFragment.
     */
    public static PreviewDocFragment newInstance(Venta _venta) {
        PreviewDocFragment fragment = new PreviewDocFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, _venta);
        fragment.setArguments(args);
        return fragment;
    }

    public PreviewDocFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            venta = getArguments().getParcelable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_preview_doc, container, false);

        image = (ImageView) view.findViewById(R.id.imageView);

        view.findViewById(R.id.preview_doc_back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getActivity().getFragmentManager();
                Fragment fragment = fm.findFragmentById(R.id.fragment);
                if (fragment != null) {
                    //cerramos el fragment
                    fm.beginTransaction()
                            .remove(fragment)
                            .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                            .commit();
                }
            }
        });

        view.findViewById(R.id.preview_doc_print_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //TIMBRE ELECTRONICO

                 //String stamp = null;
                //Config config = new Config(getActivity(), false);

                try {

                    //TIMBRE ELECTRONICO -> PDF417
                    /*PDF417 barcode = new PDF417(stamp);
                    barcode.setEccLevel(new PDF417_eccLevel(5));
                    barcode.setAspectRatio(5);
                    barcode.setSize(new PDF417_size(40, 0));
                    barcode.setTruncated(false);*/

                    //final Bitmap bitmap = barcode.encode();
                    final Bitmap bitmap = NuevaBoletaFragment.newInstance(null, null, 0, NuevaBoletaFragment.FUNCION.IMPRESION, false, "").StampToPDF417(venta.getFc_Timbre(), false);

                    /*final TicketBuilder ticket = new TicketBuilder(config);
                    ticket.set_Venta(venta);
                    venta.addDetalles(getActivity().getContentResolver());
                    ticket.set_Detalles(venta.getDetallesAbstract());
                    ticket.set_timbre(bitmap);

                    final Printer _printer = Printer.getInstance(config.PRINTER_MODEL, getActivity());

                    _printer.printerCallback = new Printer.PrinterCallback() {
                        @Override
                        public void onPrinterReady() {
                            ticket.Print(_printer);
                            //_printer.printBitmap(bitmap);
                            //_printer.footer();
                        }
                    };

                    //iniciamos la printer
                    if (!_printer.initialize()) {
                        Toast.makeText(getActivity(), "Error al inicializar", Toast.LENGTH_SHORT).show();

                        _printer.end();
                    }
                    */
                    //Toast.makeText(getActivity(), String.format("W:{0} H:{1} ", bitmap.getWidth() , bitmap.getHeight()), Toast.LENGTH_LONG);
                    //Log.i("BITMAP",String.format("W:%d H:%d ", bitmap.getWidth() , bitmap.getHeight()) );
                    image.setImageBitmap(bitmap);
                    //print = true;

                    //  Write PDF417
                    //Bitmap bitmap = BitmapFactory.decodeFile("/mnt/sdcard/download/bitmap.png");
                    //Bitmap bitmap = PDF417.encodeAsBitmap(stamp, BarcodeFormat.PDF_417, 384, 128);//3 * 886, 4 * 180);

                    //Toast.makeText(getActivity(), String.format("w: %d ; h: %d", bitmap.getWidth(), bitmap.getHeight()), Toast.LENGTH_SHORT).show();

                    //FileOutputStream out = new FileOutputStream("/mnt/sdcard/download/bitmap.png");
                    //bitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is yo
                    //out.close();

                    //byte[] bit =
                    //PrinterGrande printer = (PrinterGrande) Printer.getInstance("BANANA");
                    //printer.decodeBitmap(bitmap);
                    //printer.PrintBmp(bitmap, null);
                    //printer.newPrint(bitmap);

                    //image.setImageBitmap(bitmap);


                } catch (Exception ex) {
                    Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_SHORT).show();
                }
                //Toast.makeText(getActivity(), stamp, Toast.LENGTH_SHORT).show();
            }
        });

        btnSync = (Button) view.findViewById(R.id.preview_doc_sync_button);
       /* btnSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _view) {
                /*if (view.findViewById(R.id.fragment_sync) != null) {

                    NuevaBoletaFragment fragment = NuevaBoletaFragment.newInstance(venta, R.id.fragment_sync);
                    fragment.setNuevaBoletaListener(new NuevaBoletaFragment.NuevaBoletaListener() {
                        @Override
                        public void onBoletaGenerada(long id, String doc_type, String folio, String v) {
                            //GrabarVenta(_venta, id, doc_type, folio, v);

                            //asociamos los datos de la FC recien generada
                            venta.setFC(id, doc_type, folio, v);

                            //VENTA -> grabar
                            new VentaDao(getActivity().getContentResolver()).saveOrUpdate(venta);

                            Toast.makeText(getActivity(), String.format("boleta %d  %s  %s", id, folio, v), Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onError(String mensaje) {
                            Toast.makeText(getActivity(), mensaje, Toast.LENGTH_SHORT).show();
                        }
                    });


                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_sync, fragment);
                    ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
                    ft.commit();
                }
            }
        });*/

        webView = (WebView) view.findViewById(R.id.preview_doc_webview);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Previsualizando. Aguarde por favor...");
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                String html = "<html><body><center>Lo sentimos, la opcion de previsualizacion no esta disponible actualmente</center><br/><small>Error:<br/>" + description + "</small></body></html>";
                view.loadData(html, "text/html", null);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                view.zoomIn();
                view.zoomIn();
            }
        });

        if (venta.getFc_id() != -1)
            webView.loadUrl(String.format("http://certificacion.facturamovil.cl/document/visualization/%d?v=%s", venta.getFc_id(), venta.getFc_validation()));
        else {
            String html = "<html><body><br/><br/><br/><center>Documento generado de forma offline.<br/>Debe sincronizarlo para poder previsualizar</center></body></html>";
            webView.loadData(html, "text/html", null);
            btnSync.setVisibility(View.VISIBLE);
        }

    }


    //ZXing
    //final Bitmap bitmap = PDF417.encodeAsBitmap(stamp, BarcodeFormat.PDF_417, 384, 128, print);//3 * 886, 4 * 180);

    //OnBarcode
    /*
    PDF417 barcode = new PDF417();
    barcode.setData(stamp);

    // PDF 417 Error Correction Level
    barcode.setEcl(PDF417.ECL_5);
    barcode.setRowCount(30);
    barcode.setColumnCount(5);
    barcode.setDataMode(PDF417.M_BYTE);

    barcode.setTruncated(false);

    barcode.setProcessTilde(true);

    // unit of measure for X, Y, LeftMargin, RightMargin, TopMargin, BottomMargin
    barcode.setUom(IBarcode.UOM_PIXEL);
    // barcode module width in pixel
    barcode.setX(1f);
    barcode.setXtoYRatio(0.3f);

    barcode.setLeftMargin(0f);
    barcode.setRightMargin(0f);
    barcode.setTopMargin(0f);
    barcode.setBottomMargin(0f);

    // barcode image resolution in dpi
    barcode.setResolution(72);

    // barcode bar color and background color in Android device
    barcode.setForeColor(AndroidColor.black);
    barcode.setBackColor(AndroidColor.white);

    barcode.setAutoResize(false);

    barcode.setBarcodeWidth(384);
    barcode.setBarcodeHeight(128);

    //specify your barcode drawing area

    final Bitmap bitmap = Bitmap.createBitmap(384, 128, Bitmap.Config.ARGB_8888);
    RectF bounds = new RectF(0, 0, 0, 0);
    Canvas canvas = new Canvas(bitmap);
    barcode.drawBarcode(canvas, bounds);
    */
}
