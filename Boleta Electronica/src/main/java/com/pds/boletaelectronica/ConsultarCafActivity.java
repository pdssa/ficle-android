package com.pds.boletaelectronica;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Formatos;
import com.pds.common.Usuario;
import com.pds.common.activity.TimerActivity;
import com.pds.common.dao.RangoFolioDao;
import com.pds.common.dao.TipoDocDao;
import com.pds.common.db.TipoDocTable;
import com.pds.common.model.RangoFolio;
import com.pds.common.model.TipoDoc;

import java.util.List;


public class ConsultarCafActivity extends TimerActivity {

    private Usuario _user;
    private ListView listCaf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultar_caf);

        try {

            //evitamos el sleep durante la ejecucion de esta aplicacion
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

            //logueo Usuario *****************************************************
            //obtenemos los parametros pasamos a la actividad
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                //tomamos el id de usuario
                //_id_user_login = extras.getInt("_id_user_login");
                this._user = (Usuario) extras.getSerializable("current_user");
            }

            //no tenemos user logueado...volvemos a EntryPoint
            if (_user == null) {
                Toast.makeText(this, R.string.unknown_user, Toast.LENGTH_LONG).show();
                this.finish();
            } else {

                //Inicializo las Views
                Init_Views();
            }

        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        CargarListaCaf();
    }

    private void Init_Views() {
        listCaf = (ListView) findViewById(R.id.lsv_cons_caf);
        listCaf.setEmptyView(findViewById(android.R.id.empty));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_consultar_caf, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class ConsultaCafAdapter extends ArrayAdapter<RangoFolio> {
        private Context context;
        private List<RangoFolio> datos;

        public ConsultaCafAdapter(Context context, List<RangoFolio> datos) {
            super(context, android.R.layout.simple_list_item_1, datos);

            this.context = context;
            this.datos = datos;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // En primer lugar "inflamos" una nueva vista, que será la que se
            // mostrará en la celda del ListView. Para ello primero creamos el
            // inflater, y después inflamos la vista.
            if (convertView == null) {
                convertView = LayoutInflater.from(context)
                        .inflate(R.layout.list_item_cons_caf, parent, false);
            }

            RangoFolio row = datos.get(position);

            TipoDoc tipoDoc = obtieneTipoDocumento(row.getTipo_doc());

            // A partir de la vista, recogeremos los controles que contiene para poder manipularlos.
            // Recogemos los TextView para mostrar datos
            ((TextView) convertView.findViewById(R.id.lbl_cons_caf_tipo_doc)).setText(tipoDoc == null ? "" : tipoDoc.getNombre());
            ((TextView) convertView.findViewById(R.id.lbl_cons_caf_desde)).setText(String.valueOf(row.getDesde()));
            ((TextView) convertView.findViewById(R.id.lbl_cons_caf_hasta)).setText(String.valueOf(row.getHasta()));
            ((TextView) convertView.findViewById(R.id.lbl_cons_caf_ultimo)).setText(String.valueOf(row.getUltimo()));
            ((TextView) convertView.findViewById(R.id.lbl_cons_caf_fecha)).setText(Formatos.FormateaDate(row.getFec_ref(), Formatos.FullDateTimeFormatNoSeconds));
            ((TextView) convertView.findViewById(R.id.lbl_cons_caf_disponibles)).setText(String.valueOf(row.getRestantes()));
            ((TextView) convertView.findViewById(R.id.lbl_cons_caf_completado)).setText(String.valueOf(row.estaCompleto()?"SI":"NO"));

            return convertView;
        }
    }

    private TipoDoc obtieneTipoDocumento(String codTipoDoc) {
        TipoDoc tipoDoc = null;

        try {
            List<TipoDoc> tipoDocs = new TipoDocDao(getContentResolver()).list(TipoDocTable.COLUMN_CODIGO + " = '" + codTipoDoc + "'", null, null, null, null, null);

            if (tipoDocs.size() > 0) {
                tipoDoc = tipoDocs.get(0);
            }

        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }

        return tipoDoc;
    }

    public void CargarListaCaf() {
        List<RangoFolio> list = new RangoFolioDao(getContentResolver()).list(null, null, null, null, null, null);

        ConsultaCafAdapter adapter = new ConsultaCafAdapter(this, list);
        listCaf.setAdapter(adapter);
    }

}
