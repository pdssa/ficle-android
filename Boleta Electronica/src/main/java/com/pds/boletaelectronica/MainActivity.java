package com.pds.boletaelectronica;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Usuario;
import com.pds.common.activity.TimerMainActivity;
import com.pds.common.dao.RangoFolioDao;
import com.pds.common.model.RangoFolio;
import com.pds.common.pref.BoletaConfig;

import java.util.Date;

import android_serialport_api.Printer;
import android_serialport_api.PrinterGrande;

public class MainActivity extends TimerMainActivity {

    private View btnConsultaVentas;
    private View btnConsultaCaf;
    private View btnConsultaDocumentos;
    private Usuario _user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            ((TextView) findViewById(R.id.txtVersion)).setText("Version:" + getPackageManager().getPackageInfo(this.getPackageName().toString(), 0).versionName);

            SharedPreferences.Editor editor = getSharedPreferences(BoletaConfig.SHARED_PREF_NAME, 0).edit();
            editor.putString(BoletaConfig.BOLETA_PDS_HOST, "190.111.56.80");
            editor.putString(BoletaConfig.BOLETA_PDS_PORT, "37800");
            editor.commit();

            //logueo Usuario *****************************************************
            //obtenemos los parametros pasamos a la actividad
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                //tomamos el id de usuario
                //_id_user_login = extras.getInt("_id_user_login");
                this._user = (Usuario) extras.getSerializable("current_user");
            }

            //no tenemos user logueado...volvemos a EntryPoint
            if (_user == null) {
                Toast.makeText(this, R.string.unknown_user, Toast.LENGTH_LONG).show();
                this.finish();
            } else {

                Init_Views();

                Init_Metodos();

                //String _caf = "<?xml version=\"1.0\"?><AUTORIZACION><CAF version=\"1.0\"><DA><RE>79662080-3</RE><RS>AGRICOLA DON POLLO LIMITADA</RS><TD>33</TD><RNG><D>5801</D><H>6800</H></RNG><FA>2014-08-26</FA><RSAPK><M>pX6wuY3k0D0dpLFL012NnFL/jQQSAbB4VWFACsvuP2LJZ4C240p7TeihEBlcascdsPwYkwV72kwkZYlTEipU8Q==</M><E>Aw==</E></RSAPK><IDK>100</IDK></DA><FRMA algoritmo=\"SHA1withRSA\">CaHiBY802GQ11hFpjNGVq02Jo+Zq/TPGDxcZq2Bp1d11r2jLIbVzJc2BzyjP/+AkgZjxw7/AxL1wsK2ZlyMxPg==</FRMA></CAF><RSASK>-----BEGIN RSA PRIVATE KEY-----MIIBOQIBAAJBAKV+sLmN5NA9HaSxS9NdjZxS/40EEgGweFVhQArL7j9iyWeAtuNKe03ooRAZXGrHHbD8GJMFe9pMJGWJUxIqVPECAQMCQG5UddEJQzV+E8Mg3TeTs72Mql4CtqvK+uOWKrHdSX+WHitwEp47MdfVesnsT/Qz6G4Mz6efY6yBRFV1ixsaxUMCIQDYFV9l6RTWN7Yxr+6DCwvsICalVgXl8n04rjVF7yqgrwIhAMQQ+TUM3NpScjcxSGFxbVTrwjvBkIBlDQU3I7x6V4xfAiEAkA4/mUYN5CUkIR/0V1yynWrEbjlZQ/b+Jcl42Uocax8CIQCCtft4sz3m4aF6INrroPON8oF9K7Wq7giuJMJ9puUIPwIgSBAcoqWGlPc4IC+gA31dfzq3uniU5Pm89iDblQLPQ68=-----END RSA PRIVATE KEY-----</RSASK><RSAPUBK>-----BEGIN PUBLIC KEY-----MFowDQYJKoZIhvcNAQEBBQADSQAwRgJBAKV+sLmN5NA9HaSxS9NdjZxS/40EEgGweFVhQArL7j9iyWeAtuNKe03ooRAZXGrHHbD8GJMFe9pMJGWJUxIqVPECAQM=-----END PUBLIC KEY-----</RSAPUBK></AUTORIZACION>";
                /*
                RangoFolio r = new RangoFolio();
                r.setDesde(1);
                r.setHasta(1000);
                r.setTipo_doc("a");
                r.setUltimo(0);
                r.setCaf(caf);
                r.setFec_ref(new Date());

                new RangoFolioDao(getContentResolver()).save(r);*/

                /*RangoFolio r = new RangoFolioDao(getContentResolver()).find(1);

                String caf = r.getCaf();*/
            }

        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    private void Init_Views() {
        btnConsultaVentas = findViewById(R.id.contador_consulta_ventas);
        btnConsultaCaf = findViewById(R.id.contador_consulta_caf);
        btnConsultaDocumentos = findViewById(R.id.contador_consulta_documentos);
    }

    private void Init_Metodos() {
        btnConsultaVentas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ConsultarVentasActivity.class);
                intent.putExtra("current_user", _user);
                startActivity(intent);
            }
        });

        btnConsultaCaf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ConsultarCafActivity.class);
                intent.putExtra("current_user", _user);
                startActivity(intent);
            }
        });

        btnConsultaDocumentos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, DocumentosActivity.class);
                intent.putExtra("current_user", _user);
                startActivity(intent);
            }
        });
    }


    /*
    public void boleta() {
        try {
        /*Client*/
            /*
            Client client = new Client();
            client.setCode("66666666-6"); //cliente vacío
            */
        /*Unit*
            Unit unit = new Unit();
            unit.setCode("Unid");

        /*Product*
            Product product = new Product();
            product.setCode("001");
            product.setName("Product 001");
            product.setUnit(unit);
            product.setPrice((double) 5000);

        /*Detail*
            Detail detail = new Detail();
            detail.setPosition(1);
            detail.setProduct(product);
            detail.setQuantity(2);
            detail.setDescription("Product XXX");

        /*Document type*
            DocumentType documentType = new DocumentType();
            documentType.setCode("33");

        /*Payment*
            Payment payment = new Payment();
            payment.setPosition(1);
            payment.setAmount((double) 100);
            payment.setDate("2014-10-06");
            payment.setDescription("TEST HERNAN");

            /*Company*
            Company company = new Company();
            company.setCode("112233456");
            company.setAddress("Calle 1234");
            company.setName("la gran empresa");

        /*Boleta*
            Ticket document = new Ticket();
            //Document document = new Document();
            document.setDate("2014-10-06");
            document.setCompany(company);
            //document.setClient(client);
            document.setDocumentType(documentType);
            document.newDetails();
            document.addDetail(detail);
            document.newPayments();
            document.addPayment(payment);
            document.setTotal("1000");

            OfflineServices offlineServices = OfflineServices.getInstance();
            String stamp = offlineServices.getStamp(document, xmlCaf, "5802");

            ((TextView)findViewById(R.id.hello_world)).setText(stamp);

            BarcodePDF417 pdf417 = new BarcodePDF417();
            pdf417.setText(stamp);

            pdf417.paintCode();

            Image img = pdf417.getImage();

        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }
*/
    /*
    public void setUp(Venta venta) throws Exception {

        /*RestService*
        RestService restService = RestService.getInstance();
        restService.setUseProduction(false);

        /*Client*
        Client client = new Client();
        client.setCode("66666666-6"); //cliente vacío
        /*client.setCode("15637715-5");
        client.setAddress("Jose Domingo Cañas 1550");
        client.setName("Sebastian Diaz Moreno");
        client.setLine("ASESORES Y CONSULTORES EN INFORMÁTICA (SOFTWARE)");
        client.setMunicipality("Ñuñoa");*

        /*Unit*
        Unit unit = new Unit();
        unit.setCode("Unid");

        /*Product*
        Product product = new Product();
        product.setCode("001");
        product.setName("Product 001");
        product.setUnit(unit);
        product.setPrice((double) 5000);

        /*Detail*
        Detail detail = new Detail();
        detail.setPosition(1);
        detail.setProduct(product);
        detail.setQuantity(2);
        detail.setDescription("Product XXX");

        /*Service*
        /*
        Service service = new Service();
        service.setName("service 001");
        service.setPrice((double) 6000);
        service.setUnit(unit);
        *

        /*Document type*
        DocumentType documentType = new DocumentType();
        documentType.setCode("33");

        /*References*
        /*
        Reference reference = new Reference();
        reference.setPosition(1);
        reference.setDocumentType(documentType);
        reference.setReferencedFolio("199");
        reference.setDate("2014-10-06");
        reference.setDescription("Description for reference test sdk");
        *

        /*Payment*
        Payment payment = new Payment();
        payment.setPosition(1);
        payment.setAmount((double) 100);
        payment.setDate("2014-10-06");
        payment.setDescription("TEST HERNAN");

        /*Global discount*
        /*
        GlobalDiscount discount = new GlobalDiscount();
        discount.setPosition(1);
        discount.setAmount((double) 100);
        discount.setPercent(10);
        discount.setType("D");
        discount.setUnit("%");
        discount.setDescription("Global discount for test sdk");
        *

        /*User*
        User user = null;
        try {
            user = restService.authenticate("demo", "demodemo");
        } catch (Exception ex) {
            System.out.println("Error al autenticar. " + ex.getMessage());
        }

        /*Company*
        Company company = user.getCompanies().get(0);

        /*Invoice*
        /*
        Invoice invoice = new Invoice();
        invoice.setDate("2014-10-06");
        invoice.setCompany(company);
        invoice.setClient(client);
        invoice.setDocumentType(documentType);
        invoice.newDetails();
        invoice.addDetail(detail);
        invoice.setTotal("10000");
        *

        /*Boleta*
        Document document = new Document();
        document.setDate("2014-10-06");
        document.setCompany(company);
        document.setClient(client);
        document.setDocumentType(documentType);
        document.newDetails();
        document.addDetail(detail);
        document.newPayments();
        document.addPayment(payment);
        document.setTotal("1000");

        Response response = restService.sendTicket(user, company, document);

        if (response.getSuccess()) {
            Toast.makeText(MainActivity.this, "Boleta generada. Folio #" + response.getAssignedFolio(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(MainActivity.this, "Error: ", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.configuracion) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    */
}
