package com.pds.ficle.gestprod.fragment;

import android.app.ListFragment;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.AndroidCharacter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.pds.ficle.gestprod.R;
import com.pds.ficle.gestprod.activity.ProviderActivity;
import com.pds.common.dao.ProviderDao;
import com.pds.common.model.Provider;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProviderListFragment extends ListFragment {
    private final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");

    private static final String ARG_PARAM1 = "search";

    ProviderArrayAdapter listAdapter;
    List<Provider> list;
    boolean searchScreen = false;
    private ProviderListFragmentInteractionListener mListener;

    public static ProviderListFragment newInstance(boolean _searchScreen) {
        ProviderListFragment fragment = new ProviderListFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_PARAM1, _searchScreen);
        fragment.setArguments(args);
        return fragment;
    }

    public ProviderListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            searchScreen = getArguments().getBoolean(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        list = getList();
        listAdapter = new ProviderArrayAdapter(getActivity(), list);
        setListAdapter(listAdapter);

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume(){
        super.onResume();

        list.clear();
        list.addAll(getList());
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        v.setBackgroundColor(getResources().getColor(R.color.light_blue));

        Provider selectedProvider = (Provider) getListAdapter().getItem(position);

        if(searchScreen){
            //es pantalla de busqueda => retornamos el item seleccionado
            if(this.mListener != null) {
                this.mListener.onProviderSelected(selectedProvider);
            }
        }
        else {
            //comportamiento habitual
            Intent intent = new Intent(getActivity(), ProviderActivity.class);
            intent.putExtra("provider", selectedProvider);
            getActivity().startActivity(intent);
            getActivity().overridePendingTransition(0, 0);
        }
    }

    private List<Provider> getList() {
        return new ProviderDao(this.getActivity().getContentResolver()).list(null,null,"name");
    }

    public class ProviderArrayAdapter extends ArrayAdapter<String> {
        private final Context context;
        private final List<Provider> providers;

        public ProviderArrayAdapter(Context context, List providers) {
            super(context, R.layout.list_item_provider, providers);
            this.context = context;
            this.providers = providers;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View rowView = inflater.inflate(R.layout.list_item_provider, parent, false);
            if (position % 2 != 0)
                rowView.setBackgroundColor(Color.LTGRAY);
            else
                rowView.setBackgroundColor(Color.WHITE);

            TextView nameTextView = (TextView) rowView.findViewById(R.id.list_item_provider_name);
            nameTextView.setText(providers.get(position).getName());

            TextView emailTextView = (TextView) rowView.findViewById(R.id.list_item_provider_email);
            //emailTextView.setText(providers.get(position).getEmail());
            emailTextView.setText(providers.get(position).getDescription());

            TextView addressTextView = (TextView) rowView.findViewById(R.id.list_item_provider_address);
            addressTextView.setText(providers.get(position).getAddress());

            TextView phoneNumberTextView = (TextView) rowView.findViewById(R.id.list_item_provider_phone_number);
            phoneNumberTextView.setText(providers.get(position).getPhoneNumber());

            TextView taxIdTextView = (TextView) rowView.findViewById(R.id.list_item_provider_tax_id);
            taxIdTextView.setText(providers.get(position).getTax_id());

            return rowView;
        }
    }


    public interface ProviderListFragmentInteractionListener {
        public void onProviderSelected(Provider provider);
    }

    public void setProviderListFragmentInteractionListener(ProviderListFragmentInteractionListener mListener) {
        this.mListener = mListener;
    }
}
