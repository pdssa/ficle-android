/*
package com.pds.ficle.gestprod.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.pds.common.dao.ProductDao;
import com.pds.common.db.ProductTable;
import com.pds.common.model.Product;
import com.pds.common.util.Window;
import com.pds.ficle.gestprod.R;

import java.util.concurrent.Callable;


*/
/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AsociarCodeFragment.OnAsociarCodeListener} interface
 * to handle interaction events.
 * Use the {@link AsociarCodeFragment#newInstance} factory method to
 * create an instance of this fragment.
 *//*

public class AsociarCodeFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "product";
    private static final String ARG_PARAM2 = "edit";

    private Product product;
    private boolean editMode;

    private OnAsociarCodeListener mListener;
    private Button btnAsociar;
    private Button btnCancelar;
    private Button btnNewCode;
    private EditText productText;
    private EditText codeEditText;
    private String _codigo;
    //private ImageButton btnSearchProduct;
    private Product productOld;

    */
/**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment AsociarCodeFragment.
     *//*

    public static AsociarCodeFragment newInstance(Product param1, boolean edit) {
        AsociarCodeFragment fragment = new AsociarCodeFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, param1);
        args.putBoolean(ARG_PARAM2, edit);
        fragment.setArguments(args);
        return fragment;
    }

    public AsociarCodeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            product = getArguments().getParcelable(ARG_PARAM1);
            editMode = getArguments().getBoolean(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_asociar_code, container, false);

        Init_Views(view);

        Init_Eventos(view);

        SelectProduct(product);

        SetMode();

        */
/*if (editMode)
            btnSearchProduct.setVisibility(View.VISIBLE);
        else
            btnSearchProduct.setVisibility(View.INVISIBLE);*//*


        return view;
    }

    private void SelectProduct(Product p) {

        product = p;

        if (p != null) {
            productText.setText(p.getName());

            codeEditText.setText(p.getIspCode());

            if (!TextUtils.isEmpty(p.getIspCode()))
                editMode = true;//ya tenemos un codigo rapido, entonces modo edicion

        }

        if(!editMode) {
            //posicionamos sobre codigo
            Window.FocusViewShowSoftKeyboard(getActivity(), codeEditText);

            Window.AddDoneKeyboardAction(codeEditText, new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    btnAsociar.performClick();
                    return null;
                }
            });
        }

        //codeEditText.requestFocus();
        //((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

   */
/* private void SetMode(){
        if(editMode){
            codeEditText.setEnabled(false);
            btnNewCode.setVisibility(View.INVISIBLE);
            btnAsociar.setText("BORRAR");
            btnAsociar.setBackgroundResource(R.drawable.boton_redondo_amarillo);
            btnAsociar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Borrar();
                }
            });
        }
        else{
            codeEditText.setEnabled(true);
            btnNewCode.setVisibility(View.VISIBLE);
            btnAsociar.setText(getString(R.string.button_grabar));
            btnAsociar.setBackgroundResource(R.drawable.boton_redondo_verde);
            btnAsociar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Validar();
                }
            });
        }
    }*//*


    private void Init_Views(View view) {
        btnAsociar = (Button) view.findViewById(R.id.frg_asociar_code_btn_grabar);
        btnCancelar = (Button) view.findViewById(R.id.frg_asociar_code_btn_cancelar);
        btnNewCode = (Button) view.findViewById(R.id.frg_asociar_code_btn_code);
        codeEditText = (EditText) view.findViewById(R.id.product_new_code);
        productText = (EditText) view.findViewById(R.id.product_name_text);
        //btnSearchProduct = (ImageButton) view.findViewById(R.id.frg_asociar_code_btn_search);
    }

    private void Init_Eventos(View view) {
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cerrar();
            }
        });
        btnAsociar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Validar();
            }
        });
        view.findViewById(R.id.img_dummy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        btnNewCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BuscarCodigoDisponible();
            }
        });
        */
/*btnSearchProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ProductSearchActivity.class);
                intent.putExtra("TypeResultSearch", ProductSearchActivity.TypeResultSearch.SEARCH_PRODUCT_BACK);
                startActivityForResult(intent, 2);
            }
        });*//*

    }

    */
/*@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 2) {//requerimos un product
            if (resultCode == Activity.RESULT_OK) {
                Object result = data.getParcelableExtra("result");
                SelectProduct((Product) result);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                //SelectProvider(null);
            }
        }
    }*//*


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnAsociarCodeListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void Cerrar() {

        //ocultamos el teclado
        Window.CloseSoftKeyboard(getActivity());
        //((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(codeEditText.getWindowToken(), 0);

        FragmentManager fm = getFragmentManager();

        Fragment fragment = fm.findFragmentById(R.id.prod_search_frg_stock_edit);

        FragmentTransaction ft = fm.beginTransaction();

        ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);

        ft.hide(fragment);

        ft.commit();
    }

    public void BuscarCodigoDisponible() {

        //buscamos codigos disponibles desde el 001 al 999
        for (int i = 1; i < 1000; i++) {

            String code = String.format("%03d", i);

            if (!CodigoEnUso(code, -1)) {
                codeEditText.setText(code);
                return;
            }
        }

        Toast.makeText(getActivity(), "No hay más codigos rápidos disponibles", Toast.LENGTH_LONG).show();

    }

    */
/*public void Confirmar() {

        if(editMode && productOld != null){
            new AlertDialog.Builder(getActivity())
                    .setIcon(android.R.drawable.ic_menu_help)
                    .setTitle("Reemplazo de Código Rápido")
                    .setMessage(Html.fromHtml("<big>El código rápido <b>\"" + _codigo + "\"</b> se encuentra asociado al producto <b>\"" + productOld.getName() + "\"</b>. Desea asignarlo al producto <b>\"" + product.getName() + "\"</b>?</big>"))
                    .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Grabar();
                        }

                    })
                    .setNegativeButton("NO", null)
                    .setCancelable(false)
                    .show();

        }
        else {
            new AlertDialog.Builder(getActivity())
                    .setIcon(android.R.drawable.ic_menu_help)
                    .setTitle("Nuevo Código Rápido")
                    .setMessage(Html.fromHtml("<big>Confirma la asignación del código rápido <b>\"" + _codigo + "\"</b> al producto <b>\"" + product.getName() + "\"</b> ?</big>"))
                    .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Grabar();
                        }

                    })
                    .setNegativeButton("NO", null)
                    .setCancelable(false)
                    .show();
        }
    }*//*


  */
/*  public void Borrar(){
        productTask = new ProductTask();
        productTask.execute("");
    }

    public void Validar() {
        if (productTask != null) {
            return;
        }

        boolean cancel = false;

        // Reset errors.
        codeEditText.setError(null);

        // Store values
        _codigo = codeEditText.getText().toString();

        //los ponemos en cero para que luego al parsearlo como double no de error de formato
        if (TextUtils.isEmpty(_codigo)) {
            codeEditText.setError("Debe asociar un codigo");
            cancel = true;
        } else {
            _codigo = "000" + _codigo;
            _codigo = _codigo.substring(_codigo.length() - 3);

            //vemos si el codigo ya está en uso o no
            if(!editMode) {//para el caso de edicion, vamos a tratarlo de otra forma
                if (CodigoEnUso(_codigo, product.getId())) {//pasamos el ID del producto, para que no tome como que está en uso con él mismo
                    codeEditText.setError("Código en uso");
                    codeEditText.setText(_codigo);
                    cancel = true;
                }
            }
            else{
                //vamos a buscar si algun producto tiene el codigo, y vamos a pedir confirmacion para pisarlo
                productOld = productCodigo(_codigo, product.getId(), getActivity());
            }
        }

        if (!cancel)
            Confirmar();
    }*//*


    private boolean CodigoEnUso(String code, long id_producto) {
        return CodigoEnUso(code, id_producto, -1, getActivity());
    }

    public static boolean CodigoEnUso(String code, long id_producto, long id_combo, Context context) {
        //vemos si el codigo está en uso por un combo habilitado
        boolean en_uso = false;

        */
/*if (id_combo != -1)
            en_uso = new ComboDao(context.getContentResolver()).list("baja = 0 and codigo = '" + code + "' and _id <> " + String.valueOf(id_combo), null, null).size() > 0;
        else
            en_uso = new ComboDao(context.getContentResolver()).list("baja = 0 and codigo = '" + code + "' ", null, null).size() > 0;
*//*

        if (en_uso)
            return true;
        else {//vemos si el codigo está en uso por un producto diferente
            if (id_producto != -1)
                return new ProductDao(context.getContentResolver()).list("removed = 0 and "+ ProductTable.COLUMN_ISP_CODE+" = '" + code + "' and _id <> " + String.valueOf(id_producto), null, null).size() > 0;
            else
                return new ProductDao(context.getContentResolver()).list("removed = 0 and "+ ProductTable.COLUMN_ISP_CODE+" = '" + code + "' ", null, null).size() > 0;
        }
    }

    private static Product productCodigo(String code, long id_producto, Context context){
        return new ProductDao(context.getContentResolver()).first("removed = 0 and "+ ProductTable.COLUMN_ISP_CODE+" = '" + code + "' and _id <> " + String.valueOf(id_producto), null, null);
    }

    */
/*public void Grabar() {

        productTask = new ProductTask();
        productTask.execute(_codigo);

    }*//*


  */
/*  public class ProductTask extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {

            boolean result = true;

            product.setIspCode(params[0]);//asignamos el codigo al producto

            if(editMode && productOld != null) {
                productOld.setIspCode(""); //limpiamos el codigo asignado al producto anterior

                result = new ProductDao(getActivity().getContentResolver()).saveOrUpdate(productOld);
            }

            if(result)
                return new ProductDao(getActivity().getContentResolver()).saveOrUpdate(product);
            else
                return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            productTask = null;

            CharSequence text;
            if (success)
                text = getString(R.string.message_save_success_product);
            else
                text = getString(R.string.message_save_failed_product);

            Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();

            if (success) {
                if (mListener != null)
                    mListener.onCodeAsociado(_codigo);

                Cerrar();

            }

        }

        @Override
        protected void onCancelled() {
            productTask = null;
        }
    }
*//*

    */
/**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     *//*

    public interface OnAsociarCodeListener {
        public void onCodeAsociado(String code);
    }

}
*/
