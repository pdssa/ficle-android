package com.pds.ficle.gestprod.activity;

import android.app.DatePickerDialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.activity.Actividad;
import com.pds.common.Formatos;
import com.pds.common.dao.CompraDao;
import com.pds.common.dao.CompraDetalleDao;
import com.pds.common.dao.ProductDao;
import com.pds.common.dao.ProviderDao;
import com.pds.common.dao.ProviderProductDao;
import com.pds.common.model.Compra;
import com.pds.common.model.CompraDetalle;
import com.pds.common.model.Product;
import com.pds.common.model.Provider;
import com.pds.common.model.ProviderProduct;
import com.pds.ficle.gestprod.R;
import com.pds.ficle.gestprod.fragment.AddCompraItemFragment;
import com.pds.ficle.gestprod.util.Resources;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class CargaCompraActivity extends Actividad implements AddCompraItemFragment.FcItemFragmentInteractionListener {

    private static final String TAG = CargaCompraActivity.class.getSimpleName();
    private Button btnGrabar;
    private Button btnCancelar;
    private EditText txtFecha;
    private EditText txtNumero;
    private ImageButton btnFindProveedor;
    private Button btnAddItem;
    private EditText txtTaxId;
    private TextView txtRazonSocial;
    private Compra _cabecera;
    private List<CompraDetalle> _items;
    private ListView lstDetalleCompra;
    private ItemsArrayAdapter listAdapter;
    private TextView txtExento;
    private TextView txtNeto;
    private TextView txtImpuesto;
    private TextView txtTotal;

    private int editPosition;

    private Config config;
    private DecimalFormat FORMATO_DECIMAL;
    private DecimalFormat FORMATO_DECIMAL_SECUNDARIO;

    private final int REQUEST_PROVEEDORES = 1;
    private final int REQUEST_PRODUCTOS = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carga_fc);

        AsignarViews();
        AsignarEventos();

        config = new Config(this);

        Init_CustomConfigPais(config.PAIS);


        Intent i = getIntent();
        long idCompra = i.getLongExtra("id_compra", 0);
        if (idCompra != 0) {
            ViewCompra(idCompra);
        } else {
            NuevaCompra();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void Init_CustomConfigPais(String codePais) {

        String code = codePais == null ? "CH" : codePais;

            FORMATO_DECIMAL = code.equals("AR") ? Formatos.DecimalFormat_US : Formatos.DecimalFormat_CH;
            FORMATO_DECIMAL_SECUNDARIO = code.equals("AR") ? Formatos.DecimalFormat : Formatos.DecimalFormat_US;




        ((TextView)findViewById(R.id.fc_lbl_tax_id)).setText(Resources.getStringFromResource(this, code, "label_tax_id"));
        txtTaxId.setHint(Resources.getStringFromResource(this,code, "prompt_tax_id"));

        ((TextView)findViewById(R.id.fc_lbl_numero)).setText(Resources.getStringFromResource(this,code, "fc_txt_numero"));
        txtNumero.setHint(Resources.getStringFromResource(this,code, "fc_txt_numero_hint"));
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode){
            case REQUEST_PROVEEDORES:{//requerimos un provider
                if (resultCode == RESULT_OK) {
                    Object result = data.getParcelableExtra("result");
                    SelectProvider((Provider) result);
                }
                if (resultCode == RESULT_CANCELED) {
                    //Write your code if there's no result
                    //SelectProvider(null);
                }
            }break;
            case REQUEST_PRODUCTOS:{//requerimos un producto
                if (resultCode == RESULT_OK) {
                    Object result = data.getParcelableExtra("result");

                    IniciarFragmentAddEditItem((Product) result, null);//mandamos a crear el item con el producto seleccionado
                }
                if (resultCode == RESULT_CANCELED) {
                    //Write your code if there's no result
                    //SelectProvider(null);
                }
            }break;
        }

    }

    private void AsignarViews() {
        btnGrabar = (Button) findViewById(R.id.fc_btn_grabar);
        btnCancelar = (Button) findViewById(R.id.fc_back_button);
        btnFindProveedor = (ImageButton) findViewById(R.id.fc_ibtn_find_prov);
        btnAddItem = (Button) findViewById(R.id.fc_btn_add_item);
        txtFecha = (EditText) findViewById(R.id.fc_txt_fecha);
        txtNumero = (EditText) findViewById(R.id.fc_txt_numero);
        txtTaxId = (EditText) findViewById(R.id.fc_txt_tax_id);
        txtRazonSocial = (TextView) findViewById(R.id.fc_txt_proveedor);
        lstDetalleCompra = (ListView) findViewById(R.id.compras_item_list_container);
        //totales
        txtExento = (TextView) findViewById(R.id.fc_txt_exento);
        txtNeto = (TextView) findViewById(R.id.fc_txt_neto);
        txtImpuesto = (TextView) findViewById(R.id.fc_txt_iva);
        txtTotal = (TextView) findViewById(R.id.fc_txt_total);
    }

    private void AsignarEventos() {
        btnGrabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Confirmar();
            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IniciarBusquedaProductos(_cabecera.getProveedor());
            }
        });

        btnFindProveedor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CargaCompraActivity.this, ProviderManagerActivity.class);
                intent.putExtra("search", true);
                startActivityForResult(intent, REQUEST_PROVEEDORES);
                overridePendingTransition(0, 0);
            }
        });

        lstDetalleCompra.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CompraDetalle det = (CompraDetalle) parent.getItemAtPosition(position);

                editPosition = position;

                IniciarFragmentAddEditItem(null, det);
            }
        });


        txtTaxId.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                    List<Provider> providers = new ProviderDao(getContentResolver()).list("tax_id = '" + ((EditText) view).getText().toString() + "'", null, null);

                    if (providers.size() > 0) {
                        SelectProvider(providers.get(0));
                    } else {
                        SelectProvider(null);
                    }
                }
            }
        });

        txtFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PickDate();
            }
        });
        txtFecha.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) PickDate();
            }
        });
    }

    public void Confirmar() {
        super.onSave(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Validar();
            }
        });
    }

    public void Validar() {
        boolean cancel = false;

        if (_cabecera.getProveedor() == null) {
            txtTaxId.setError("Proveedor obligatorio");
            txtTaxId.requestFocus();
            cancel = true;
        }

        if (txtFecha.getText().toString().equals("")){
            txtFecha.setError("La fecha es obligatoria");
            cancel = true;
        }else{
            txtFecha.setError(null);
        }

        if (txtNumero.getText().toString().equals("")){
            txtNumero.setError("El Número de Folio es obligatorio");
            cancel = true;
        }else{
            txtNumero.setError(null);
        }

        if (!cancel)
            Grabar();
    }

    public void Grabar() {
        try {

            //primero vamos a grabar las relaciones entre productos cargados y proveedor
            if (RevisaGrabaRelaciones()) {

                //continuamos con la grabacion de la compra
                _cabecera.setNumero(txtNumero.getText().toString().trim());

                if (!new CompraDao(getContentResolver()).save(_cabecera)) {
                    Toast.makeText(CargaCompraActivity.this, "Error al generar la Factura de Compra", Toast.LENGTH_SHORT).show();
                } else {

                    CompraDetalleDao compraDetalleDao = new CompraDetalleDao(getContentResolver());
                    ProductDao productDao = new ProductDao(getContentResolver(), config.SYNC_STOCK ? 1 : 0);

                    for (CompraDetalle det : _items) {

                        //asociamos la cabecera y grabamos
                        det.setCompra(_cabecera);

                        compraDetalleDao.save(det);

                        //actualizamos el producto con la info de la compra
                        Product _prd = det.getProducto();

                        _prd.setStock(_prd.getStock() + det.getCantidad());
                        _prd.setPurchasePrice(det.getPrecio());
                        _prd.setSalePrice(det.getPrecio_vta());

                        productDao.saveOrUpdate(_prd);
                    }

                    Toast.makeText(CargaCompraActivity.this, "Compra generada correctamente!", Toast.LENGTH_SHORT).show();

                    NuevaCompra();
                }
            }

        } catch (Exception ex) {
            Toast.makeText(CargaCompraActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public boolean RevisaGrabaRelaciones() {

        ProviderProductDao providerProductDao = new ProviderProductDao(getContentResolver());
        for (CompraDetalle det : _items) {
            //vemos si existe ya la relacion, sino la creamos
            if (!providerProductDao.existeRelacion(det.getProducto().getId(), _cabecera.getProveedor().getId())) {

                boolean result = providerProductDao.save(new ProviderProduct(det.getProducto().getId(), _cabecera.getProveedor().getId()));
                Log.i(TAG, "RevisaGrabaRelaciones: "+result);
                /*
                new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_menu_help)
                        .setTitle("Confirmar nueva relacion Producto-Proveedor")
                        .setMessage(Html.fromHtml("<big>Confirma la relación entre el producto <b>\"" + det.getProducto().getName() + "\"</b> y el proveedor <b>\"" + _cabecera.getProveedor().getName() + "\"</b> ?</big>"))
                        .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                providerProductDao.save(new ProviderProduct(det.getProducto().getId(), _cabecera.getProveedor().getId()));
                                //llamada recursiva
                                Grabar();
                            }

                        })
                        .setNegativeButton("NO", null)
                        .setCancelable(false)
                        .show();

                return false; //aun quedan relaciones por crear
                */
            }
        }

        return true;//ya no quedan relaciones por crear
    }

    public void PickDate() {
        Calendar hoy = Calendar.getInstance();
        DatePickerDialog datePicker = new DatePickerDialog(CargaCompraActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                Calendar fecha = Calendar.getInstance();
                fecha.set(year, monthOfYear, dayOfMonth);

                _cabecera.setFecha(fecha.getTime());

                txtFecha.setText(Formatos.FormateaDate(_cabecera.getFecha(), Formatos.DateFormatSlash));
            }
        }, hoy.get(Calendar.YEAR), hoy.get(Calendar.MONTH), hoy.get(Calendar.DAY_OF_MONTH));
        datePicker.show();
    }

    public void SelectProvider(Provider _provider) {
        if (_provider != null) {
            _cabecera.setProveedor(_provider);
            txtRazonSocial.setText(_provider.getName());
            txtTaxId.setError(null);
            txtTaxId.setText(_provider.getTax_id());
            btnAddItem.setEnabled(true);//lo habilitamos para ingresar items
        } else {
            //TODO: provider not found
            txtRazonSocial.setText("");
            txtTaxId.setError("Proveedor no existente");
            btnAddItem.setEnabled(true);//lo deshabilitamos para que no puedan ingresar items
        }
    }

    private void NuevaCompra() {
        this._cabecera = new Compra();

        btnAddItem.setEnabled(false);//lo deshabilitamos por default

        editPosition = 0;

        if (_items == null) {
            _items = new ArrayList<CompraDetalle>();
            listAdapter = new ItemsArrayAdapter(this, _items);
            lstDetalleCompra.setAdapter(listAdapter);
        } else {
            this._items.clear();
            listAdapter.notifyDataSetChanged();
        }

        txtFecha.setText(null);
        txtFecha.setError(null);
        txtTaxId.setText(null);
        txtTaxId.setError(null);
        txtRazonSocial.setText(null);
        txtNumero.setText(null);
        txtNumero.setError(null);

        ActualizarTotal();
    }

    private void ViewCompra(long idCompra) {
        this._cabecera = new CompraDao(getContentResolver()).find(idCompra);

        txtFecha.setText(Formatos.FormateaDate(this._cabecera.getFecha(), Formatos.DateFormatSlash));
        txtTaxId.setText(this._cabecera.getProveedor().getTax_id());
        txtRazonSocial.setText(this._cabecera.getProveedor().getName());
        txtNumero.setText(this._cabecera.getNumero());

        this._items = new CompraDetalleDao(getContentResolver()).list(String.format("id_compra = %d", idCompra), null, null);
        listAdapter = new ItemsArrayAdapter(this, _items);
        lstDetalleCompra.setAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();

        btnAddItem.setEnabled(false);
        btnFindProveedor.setEnabled(false);
        btnGrabar.setVisibility(View.GONE);
        btnCancelar.setText("VOLVER");

        txtFecha.setEnabled(false);
        txtTaxId.setEnabled(false);
        txtNumero.setEnabled(false);

        lstDetalleCompra.setOnItemClickListener(null);

        ActualizarTotal();
    }


    @Override
    public void onItemCreated(CompraDetalle new_item) {
        this._items.add(new_item);
        //listAdapter = new ItemsArrayAdapter(this, _items);
        listAdapter.notifyDataSetChanged();

        ActualizarTotal();

        //agregamos y nos mantenemos en pantalla para continuar..
        Toast.makeText(this, "Item agregado correctamente a la compra", Toast.LENGTH_SHORT).show();

        IniciarBusquedaProductos(_cabecera.getProveedor());//iniciamos nueva busqueda

    }

    @Override
    public void onItemEdited(CompraDetalle edited_item) {
        _items.set(editPosition, edited_item);

        listAdapter.notifyDataSetChanged();

        ActualizarTotal();
    }

    private void ActualizarTotal() {

        double exento = 0, neto = 0, iva = 0, total = 0;

        for (CompraDetalle det : _items) {
            exento += det.getSubtotal_exento();
            neto += det.getSubtotal_neto_grav();
            iva += det.getSubtotal_impuesto();
            total += det.getTotal();
        }

        //asociamos a la cabecera
        _cabecera.setSubtotal_exento(exento);
        _cabecera.setSubtotal_impuesto(iva);
        _cabecera.setSubtotal_neto_grav(neto);
        _cabecera.setTotal(total);

        //escribimos los totales
        txtExento.setText(Formatos.FormateaDecimal(exento, FORMATO_DECIMAL, "$"));
        txtNeto.setText(Formatos.FormateaDecimal(neto, FORMATO_DECIMAL, "$"));
        txtImpuesto.setText(Formatos.FormateaDecimal(iva, FORMATO_DECIMAL, "$"));
        txtTotal.setText(Formatos.FormateaDecimal(total, FORMATO_DECIMAL, "$"));

    }

    public void IniciarBusquedaProductos(Provider _provider) {
        Intent intent = new Intent(this, ProductSearchActivity.class);
        intent.putExtra("TypeResultSearch", ProductSearchActivity.TypeResultSearch.SEARCH_PRODUCT_BACK);
        intent.putExtra("Filter_Provider", _provider);
        startActivityForResult(intent, REQUEST_PRODUCTOS);
    }


    public void IniciarFragmentAddEditItem(Product product_add, CompraDetalle item_edit) {
        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.frgAddEditItem) != null) {

            // Create a new Fragment to be placed in the activity layout
            AddCompraItemFragment fragment = new AddCompraItemFragment().newInstance(product_add, item_edit, FORMATO_DECIMAL, FORMATO_DECIMAL_SECUNDARIO);

            //AddFcItemFragment fragment = new AddFcItemFragment();
            fragment.setFcItemFragmentInteractionListener(this);

            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.frgAddEditItem, fragment);
            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            ft.commit();
        }
    }

    public class ItemsArrayAdapter extends ArrayAdapter<CompraDetalle> {
        private final Context context;
        private List<CompraDetalle> items;

        public ItemsArrayAdapter(Context context, List<CompraDetalle> items) {
            super(context, R.layout.list_item_compra, items);
            this.context = context;
            this.items = items;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View rowView = inflater.inflate(R.layout.list_item_compra, parent, false);
            if (position % 2 != 0)
                rowView.setBackgroundColor(Color.LTGRAY);
            else
                rowView.setBackgroundColor(Color.WHITE);

            ((TextView) rowView.findViewById(R.id.list_item_compra_codigo)).setText(TextUtils.isEmpty(items.get(position).getProducto().getCode())? "S/C" :  items.get(position).getProducto().getCode());
            ((TextView) rowView.findViewById(R.id.list_item_compra_descripcion)).setText(items.get(position).getProducto().getName());
            ((TextView) rowView.findViewById(R.id.list_item_compra_cantidad)).setText(String.valueOf(items.get(position).getCantidad()));
            ((TextView) rowView.findViewById(R.id.list_item_compra_precio)).setText(Formatos.FormateaDecimal(items.get(position).getPrecio(), FORMATO_DECIMAL, "$"));
            ((TextView) rowView.findViewById(R.id.list_item_compra_subtotal)).setText(Formatos.FormateaDecimal(items.get(position).getTotal() - items.get(position).getSubtotal_impuesto(), FORMATO_DECIMAL, "$"));

            return rowView;
        }

    }

}
