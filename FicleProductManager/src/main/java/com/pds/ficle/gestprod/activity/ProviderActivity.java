package com.pds.ficle.gestprod.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Logger;
import com.pds.common.activity.TimerActivity;
import com.pds.common.db.ProviderTable;
import com.pds.ficle.gestprod.R;
import com.pds.common.dao.ProviderDao;
import com.pds.common.model.Provider;
import com.pds.ficle.gestprod.util.Resources;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ProviderActivity extends TimerActivity {
    private final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");

    private Button addButton;
    private Button cancelButton;
    private Button deleteImageView;
    //private ImageView addDepartmentImage;
    //private ImageView addSubDepartmentImage;
    private EditText altaEditText;
    private EditText nameEditText;
    private EditText descriptionEditText;
    private EditText contactEditText;
    private EditText emailEditText;
    private EditText phoneNumberEditText;
    private EditText addressEditText;
    private EditText cityEditText;
    private EditText taxIdEditText;
    //private Spinner departmentSpinner;
    //private Spinner subDepartmentSpinner;

    private String name;
    private String alta;
    private String description;
    private String contact;
    //private Department department;
    //private SubDepartment subDepartment;
    private String email;
    private String phoneNumber;
    private String address;
    private String city;
    private boolean edit;
    private String tax_id; //RUT o CUIT

    private Provider provider;

    private ProviderTask providerTask = null;

    /*private ArrayAdapter<Department> departmentArrayAdapter;
    private List<Department> departmentList;
    private ArrayAdapter<SubDepartment> subDepartmentArrayAdapter;
    private List<SubDepartment> subDepartmentList;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider);

        Intent intent = getIntent();
        provider = intent.getParcelableExtra("provider");
        if (provider != null) {
            edit = true;
            name = provider.getName();
            alta = DATE_FORMAT.format(provider.getAlta());
            description = provider.getDescription();
            //department = provider.getDepartment();
            //subDepartment = provider.getSubDepartment();
            email = provider.getEmail();
            phoneNumber = provider.getPhoneNumber();
            contact = provider.getContact();
            address = provider.getAddress();
            city = provider.getCity();
            tax_id = provider.getTax_id().trim().toUpperCase();
        } else {
            provider = new Provider();
            alta = DATE_FORMAT.format(new Date());
        }

        altaEditText = (EditText) findViewById(R.id.provider_alta_date);
        altaEditText.setText(alta);

        nameEditText = (EditText) findViewById(R.id.provider_name);
        nameEditText.setText(name);

        descriptionEditText = (EditText) findViewById(R.id.provider_description);
        descriptionEditText.setText(description);

        contactEditText = (EditText) findViewById(R.id.provider_contact);
        contactEditText.setText(contact);

        emailEditText = (EditText) findViewById(R.id.provider_email);
        emailEditText.setText(email);

        phoneNumberEditText = (EditText) findViewById(R.id.provider_phone_number);
        phoneNumberEditText.setText(phoneNumber);

        addressEditText = (EditText) findViewById(R.id.provider_address);
        addressEditText.setText(address);

        cityEditText = (EditText) findViewById(R.id.provider_city);
        cityEditText.setText(city);

        taxIdEditText = (EditText) findViewById(R.id.provider_tax_id);
        taxIdEditText.setText(tax_id);

        /*departmentList = getListDepartments();
        departmentArrayAdapter = new ArrayAdapter<Department>(this, android.R.layout.simple_spinner_item, departmentList);
        departmentArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        departmentSpinner = (Spinner) findViewById(R.id.provider_department);
        departmentSpinner.setAdapter(departmentArrayAdapter);

        departmentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Department d = (Department) departmentSpinner.getItemAtPosition(position);

                subDepartmentList.clear();
                subDepartmentList.addAll(getListSubDepartments(
                        (String.format("department_id = %d", d.getId())) ));
                subDepartmentArrayAdapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        if (department != null) {
            int index = SpinnerUtils.getIndexForElement(departmentSpinner, new SpinnerUtils.Predicate<Department>() {
                @Override
                public boolean evaluate(Department element) {
                    return department.getId() == element.getId();
                }
            });
            departmentSpinner.setSelection(index);
        }

        if (departmentSpinner.getSelectedItem() != null )
            subDepartmentList = getListSubDepartments(
                    (String.format("department_id = %d", ((Department) departmentSpinner.getSelectedItem()).getId())) );
        else
            subDepartmentList = new ArrayList<SubDepartment>();

        subDepartmentArrayAdapter = new ArrayAdapter<SubDepartment>(this, android.R.layout.simple_spinner_item, subDepartmentList);
        subDepartmentArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        subDepartmentSpinner = (Spinner) findViewById(R.id.provider_sub_department);
        subDepartmentSpinner.setAdapter(subDepartmentArrayAdapter);

        if (subDepartment != null) {
            int index = SpinnerUtils.getIndexForElement(subDepartmentSpinner, new SpinnerUtils.Predicate<SubDepartment>() {
                @Override
                public boolean evaluate(SubDepartment element) {
                    return subDepartment.getId() == element.getId();
                }
            });
            subDepartmentSpinner.setSelection(index);
        }

        addDepartmentImage = (ImageView) findViewById(R.id.provider_department_add_icon);
        addDepartmentImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProviderActivity.this, DepartmentActivity.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });

        addSubDepartmentImage = (ImageView) findViewById(R.id.provider_sub_department_add_icon);
        addSubDepartmentImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProviderActivity.this, SubDepartmentActivity.class);
                intent.putExtra("departmentId", departmentSpinner.getSelectedItemId());
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });
        */
        cancelButton = (Button) findViewById(R.id.provider_back_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        addButton = (Button) findViewById(R.id.provider_add_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate();
            }
        });

        /*if (edit) {
            deleteImageView = (Button) findViewById(R.id.provider_delete);
            deleteImageView.setVisibility(View.VISIBLE);
            deleteImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    delete();
                }
            });
        }*/


        Init_CustomConfigPais(new Config(this).PAIS);
    }

    @Override
    public void onResume(){
        super.onResume();

        /*departmentList.clear();
        departmentList.addAll(getListDepartments());
        departmentArrayAdapter.notifyDataSetChanged();

        subDepartmentList.clear();
        if (departmentSpinner.getSelectedItem() != null )
            subDepartmentList.addAll(getListSubDepartments(
                    String.format("department_id = %d", ((Department) departmentSpinner.getSelectedItem()).getId()) ));
        subDepartmentArrayAdapter.notifyDataSetChanged();*/
    }

    private void Init_CustomConfigPais(String codePais) {

        ((TextView)findViewById(R.id.provider_tax_id_lbl)).setText(Resources.getStringFromResource(this, codePais, "label_tax_id"));
        taxIdEditText.setHint(Resources.getStringFromResource(this,codePais, "prompt_tax_id"));
        ((TextView)findViewById(R.id.provider_city_lbl)).setText(Resources.getStringFromResource(this,codePais, "label_city"));
        cityEditText.setHint(Resources.getStringFromResource(this,codePais, "prompt_city"));
        ((TextView)findViewById(R.id.provider_description_lbl)).setText(Resources.getStringFromResource(this,codePais, "label_nombre_fantasia"));
        descriptionEditText.setHint(Resources.getStringFromResource(this, codePais, "prompt_nombre_fantasia"));
    }



/*
    public List<Department> getListDepartments(){
        List<Department> lst = (new DepartmentDao(getContentResolver())).list();
        lst.add(0,new Department(-1, "Seleccione..."));
        return lst;
    }

    public List<SubDepartment> getListSubDepartments(String filter){
        List<SubDepartment> lst = (new SubDepartmentDao(getContentResolver())).list(filter, null, null);
        lst.add(0,new SubDepartment(-1, "Seleccione..."));
        return lst;
    }
*/
    public void validate() {
        if (providerTask != null) {
            return;
        }

        boolean cancel = false;

        // Reset errors.
        nameEditText.setError(null);
        contactEditText.setError(null);
        emailEditText.setError(null);
        phoneNumberEditText.setError(null);
        addressEditText.setError(null);
        cityEditText.setError(null);
        altaEditText.setError(null);
        descriptionEditText.setError(null);
        taxIdEditText.setError(null);

        name = nameEditText.getText().toString();
        alta = altaEditText.getText().toString();
        description = descriptionEditText.getText().toString();
        //department = (Department) departmentSpinner.getSelectedItem();
        //subDepartment = (SubDepartment) subDepartmentSpinner.getSelectedItem();
        contact = contactEditText.getText().toString();
        email = emailEditText.getText().toString();
        phoneNumber = phoneNumberEditText.getText().toString();
        address = addressEditText.getText().toString();
        city = cityEditText.getText().toString();
        tax_id = taxIdEditText.getText().toString().trim().toUpperCase();

        if (TextUtils.isEmpty(name)) {
            nameEditText.setError(getString(R.string.error_field_required));
            cancel = true;
        }

        if (TextUtils.isEmpty(tax_id)) {
            taxIdEditText.setError(getString(R.string.error_field_required));
            cancel = true;
        }

        if(!unicidadTaxId(provider, tax_id)){
            taxIdEditText.setError("Proveedor ya existente");
            cancel = true;
        }

        if (!TextUtils.isEmpty(email) && !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailEditText.setError(getString(R.string.error_invalid_email));
            cancel = true;
        }

        if (!TextUtils.isEmpty(phoneNumber) && !TextUtils.isDigitsOnly(phoneNumber)) {
            phoneNumberEditText.setError(getString(R.string.error_not_only_numbers));
            cancel = true;
        }

        if (!cancel) {
            providerTask = new ProviderTask();
            providerTask.execute((Void) null);
        }
    }

    /*public void delete() {
        //Se deshabilita la funcionalidad, dado que hay tablas involucradas: product_providers / compra, etc
        ProviderDao providerDao = new ProviderDao(getContentResolver());

        List<Provider> providers = providerDao.list(String.format("provider_id = %d and removed = 0", provider.getId()), null, null);
        if (providers.size() == 0) {
            providerDao.delete(provider);
            Toast.makeText(getApplicationContext(), R.string.message_delete_success_provider, Toast.LENGTH_SHORT).show();
            onBackPressed();
        } else {
            Toast.makeText(getApplicationContext(), R.string.message_delete_denied_provider, Toast.LENGTH_SHORT).show();
        }
    }*/

    public boolean unicidadTaxId(Provider provider, String tax_id) {
        if (TextUtils.isEmpty(tax_id))
            return true;

        tax_id = tax_id.replace("-", "").replace(".","").toUpperCase().trim();

        List<Provider> check = new ProviderDao(getContentResolver()).list(String.format("upper(replace(replace(" + ProviderTable.COLUMN_TAXID + ",'-',''),'.','')) = upper('" + tax_id + "') and _id <> %d ", provider.getId()), null, null);

        return check.size() == 0;
    }


    public class ProviderTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {
            provider.setName(name);
            provider.setDescription(description);
            //provider.setDepartment(department.getId() != -1 ? department : null);
            provider.setDepartment(null);
            //provider.setSubDepartment(subDepartment.getId() != -1 ? subDepartment : null);
            provider.setSubDepartment(null);
            provider.setContact(contact);
            provider.setEmail(email);
            provider.setPhoneNumber(phoneNumber);
            provider.setAddress(address);
            provider.setCity(city);
            provider.setTax_id(tax_id);
            try {
                provider.setAlta(DATE_FORMAT.parse(alta));
            } catch (ParseException e) {
                Log.d("ProviderActivity", "Alta date parse error.");
            }

            Log.d("ProviderActivity", "provider added");
            boolean result = new ProviderDao(getContentResolver()).saveOrUpdate(provider);

            if(result)
                Logger.RegistrarEvento(getContentResolver(), "i", "ALTA PROVEEDOR", "COD: " + provider.getTax_id(), "id: " + String.valueOf(provider.getId()));

            return result;

        }

        @Override
        protected void onPostExecute(final Boolean success) {
            providerTask = null;

            CharSequence text;
            if(success)
                text = getString(R.string.message_save_success_provider);
            else
                text = getString(R.string.message_save_failed_provider);
            Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();

            if(success)
                onBackPressed();
        }

        @Override
        protected void onCancelled() {
            providerTask = null;
        }
    }

}

