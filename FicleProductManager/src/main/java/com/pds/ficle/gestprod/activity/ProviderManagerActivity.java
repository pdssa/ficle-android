package com.pds.ficle.gestprod.activity;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.pds.common.Config;
import com.pds.common.activity.TimerActivity;
import com.pds.common.model.Provider;
import com.pds.ficle.gestprod.R;
import com.pds.ficle.gestprod.fragment.ProviderListFragment;
import com.pds.ficle.gestprod.util.Resources;

public class ProviderManagerActivity extends TimerActivity implements ProviderListFragment.ProviderListFragmentInteractionListener {

    private Button newButton;
    private Button backButton;

    private boolean searchScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_manager);

        searchScreen = false;

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            //vemos si es invocado como screen de búsqueda
            searchScreen =  extras.getBoolean("search", false);
        }

        newButton = (Button) findViewById(R.id.provider_manager_add_button);
        newButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProviderManagerActivity.this, ProviderActivity.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });

        backButton = (Button) findViewById(R.id.department_manager_back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        FragmentManager fm = getFragmentManager();

        if (fm.findFragmentById(android.R.id.content) == null) {
            ProviderListFragment list = ProviderListFragment.newInstance(searchScreen);
            list.setProviderListFragmentInteractionListener(this);
            fm.beginTransaction().add(R.id.provider_manager_list_container, list).commit();
        }

        Init_CustomConfigPais(new Config(this).PAIS);
    }


    @Override
    public void onProviderSelected(Provider provider) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result",provider);
        setResult(RESULT_OK,returnIntent);
        finish();
    }

    private void Init_CustomConfigPais(String codePais) {

        ((TextView)findViewById(R.id.prov_manag_tax_id_lbl)).setText(Resources.getStringFromResource(this, codePais, "label_tax_id"));
        ((TextView)findViewById(R.id.prov_manag_descr_id_lbl)).setText(Resources.getStringFromResource(this, codePais, "label_nombre_fantasia"));

    }


}
