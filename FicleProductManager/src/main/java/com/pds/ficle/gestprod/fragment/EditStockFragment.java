package com.pds.ficle.gestprod.fragment;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.pds.common.Formatos;
import com.pds.common.Usuario;
import com.pds.common.model.HistMovStock;
import com.pds.common.model.Product;
import com.pds.ficle.gestprod.R;

import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EditStockFragment.OnEditStockInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EditStockFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditStockFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "product";
    private static final String ARG_PARAM2 = "user";

    private Product product;
    private Usuario user;

    private OnEditStockInteractionListener mListener;
    private Button btnGrabar;
    private Button btnCancelar;
    private EditText txtCantidad, txtResultado, txtObservaciones;
    private RadioButton rbtAjusteIngreso, rbtAjusteEgreso;
    private RadioGroup rbtAjusteGroup;

    public void setOnEditStockInteractionListener(OnEditStockInteractionListener mListener) {
        this.mListener = mListener;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param _product Producto a editar.
     * @return A new instance of fragment EditStockFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EditStockFragment newInstance(Product _product, Usuario _user) {
        EditStockFragment fragment = new EditStockFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, _product);
        args.putSerializable(ARG_PARAM2, _user);
        fragment.setArguments(args);
        return fragment;
    }

    public EditStockFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            product = getArguments().getParcelable(ARG_PARAM1);
            user = (Usuario) getArguments().getSerializable(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_stock, container, false);

        Init_Views(view);

        Init_Eventos(view);

        if (product != null) {
            //txtCantidad.setText("0");
            txtResultado.setText(Integer.toString((int) product.getStock()));
        }

        return view;
    }

    private void Init_Views(View view) {
        btnGrabar = (Button) view.findViewById(R.id.frg_edit_stock_btn_aceptar);
        btnCancelar = (Button) view.findViewById(R.id.frg_edit_stock_btn_cancelar);
        txtCantidad = (EditText) view.findViewById(R.id.frg_edit_stock_cantidad);
        txtResultado = (EditText) view.findViewById(R.id.frg_edit_stock_result);
        txtObservaciones = (EditText) view.findViewById(R.id.frg_edit_stock_observaciones);
        rbtAjusteEgreso = (RadioButton) view.findViewById(R.id.frg_edit_stock_rbt_egr);
        rbtAjusteIngreso = (RadioButton) view.findViewById(R.id.frg_edit_stock_rbt_ing);
        rbtAjusteGroup = (RadioGroup) view.findViewById(R.id.frg_edit_stock_rbt);
    }

    private void Init_Eventos(View view) {
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cerrar();
            }
        });
        btnGrabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CerrarItem();
            }
        });
        view.findViewById(R.id.img_dummy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cerrar();
            }
        });
        view.findViewById(R.id.frg_edit_stock_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        txtCantidad.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ActualizarResultado();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        rbtAjusteIngreso.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ActualizarResultado();
            }
        });
        rbtAjusteEgreso.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ActualizarResultado();
            }
        });
    }

    private Double Resultado;
    private Double Cantidad;

    private void ActualizarResultado(){
        String s = txtCantidad.getText().toString().trim();
        if (!TextUtils.isEmpty(s)) {
            Cantidad = Double.parseDouble(s);

            if(rbtAjusteIngreso.isChecked())
                Resultado = product.getStock() + Cantidad;
            else if(rbtAjusteEgreso.isChecked())
                Resultado = product.getStock() - Cantidad;
            else
                Resultado = product.getStock(); //hasta q no selecione el modo, no variamos el resultado
        } else
            Resultado = product.getStock();

        txtResultado.setText(Integer.toString(Resultado.intValue()));
    }

    public void CerrarItem() {

        //clear errors
        rbtAjusteIngreso.setError(null);
        txtCantidad.setError(null);

        String cantidad = txtCantidad.getText().toString().trim();

        String operacion = rbtAjusteIngreso.isChecked() ? "I" : (rbtAjusteEgreso.isChecked() ? "E" : null);

        if(TextUtils.isEmpty(operacion)) {
            rbtAjusteIngreso.setError("Debe seleccionar un tipo de ajuste");
        }
        else if(TextUtils.isEmpty(cantidad) || !TextUtils.isDigitsOnly(cantidad)){
            txtCantidad.setError("Cantidad ingresada no valida");
        }
        else {

            //Double q = Double.parseDouble(cantidad);
            //Double r = product.getStock() + q;

            HistMovStock stk = new HistMovStock();
            stk.setFecha(new Date());
            stk.setUsuario(user.getLogin());
            stk.setObservacion(txtObservaciones.getText().toString());
            stk.setStock(Resultado.intValue());
            stk.setCantidad(Cantidad.intValue());
            stk.setMonto(0);
            stk.setOperacion(operacion);
            stk.setProductoid(product.getId());

            if (mListener != null) {
                mListener.onStockEdited(stk, Resultado);
            }

            Cerrar();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnEditStockInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " debe implementar OnEditStockInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void Cerrar() {
        if (getActivity().getCurrentFocus() != null)
            ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        //getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        FragmentManager fm = getActivity().getFragmentManager();

        Fragment fragment = fm.findFragmentById(R.id.product_abm_frg_edit_stock);

        FragmentTransaction ft = fm.beginTransaction();

        ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);

        ft.hide(fragment);

        ft.commit();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnEditStockInteractionListener {
        public void onStockEdited(HistMovStock histMovStock, double nuevoStock);
    }

}
