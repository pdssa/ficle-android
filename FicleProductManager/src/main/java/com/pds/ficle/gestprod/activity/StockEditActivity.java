package com.pds.ficle.gestprod.activity;

import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Formato;
import com.pds.common.Logger;
import com.pds.common.activity.Actividad;
import com.pds.common.Formatos;
import com.pds.common.dao.CompraDao;
import com.pds.common.dao.CompraDetalleDao;
import com.pds.common.dao.ProductDao;
import com.pds.common.dao.ProviderDao;
import com.pds.common.dao.ProviderProductDao;
import com.pds.common.model.Compra;
import com.pds.common.model.CompraDetalle;
import com.pds.common.model.Product;
import com.pds.common.model.Provider;
import com.pds.common.model.ProviderProduct;
import com.pds.ficle.gestprod.R;
import com.pds.ficle.gestprod.fragment.ProvidersFragment;
import com.pds.ficle.gestprod.util.Resources;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;


public class StockEditActivity extends Actividad implements ProvidersFragment.OnProvidersInteractionListener {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "product_edit";

    private Product productEdit;

    //private OnFragmentInteractionListener mListener;
    private Button btnGrabar;
    private Button btnCancelar;
    private EditText salePriceEditText;
    private EditText minQuantityEditText;
    private EditText stockQuantityEditText;
    private EditText addStockQuantityEditText;
    private EditText newStockQuantityEditText;
    private EditText purchasePriceEditText;
    private View seccion_ultimo_pedido;

    private String minQuantity;
    private String stockQuantity;
    private int _stockQuantity;
    private int _addQuantity;
    private String purchasePrice;
    private String salePrice;
    private String newStockQuantity;

    private ProductTask productTask;
    private EditText txtTaxId;
    private TextView txtRazonSocial;
    private ImageButton btnFindProvider;
    private Provider providerSelected;

    private Config config;

    //private DecimalFormat FORMATO_DECIMAL;
    //private DecimalFormat FORMATO_DECIMAL_SECUNDARIO;
    private Formato.Decimal_Format FORMATO_DECIMAL;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment StockEditFragment.
     */
    /*
    public static StockEditFragment newInstance(Product _productEdit) {
        StockEditFragment fragment = new StockEditFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, _productEdit);
        fragment.setArguments(args);
        return fragment;
    }
    public StockEditFragment() {
        // Required empty public constructor
    }
    */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_edit);

        Intent intent = getIntent();

        if (intent.getParcelableExtra(ARG_PARAM1) != null) {
            productEdit = intent.getParcelableExtra(ARG_PARAM1);
        }

        Init_Views();

        Init_Eventos();

        config = new Config(this);

        Init_CustomConfigPais(config.PAIS);

        if (productEdit != null) {
            salePrice = Formato.FormateaDecimal(productEdit.getSalePrice(), FORMATO_DECIMAL);
            if (productEdit.getSalePrice() != 0)
                salePriceEditText.setText(salePrice);
            purchasePrice = Formato.FormateaDecimal(productEdit.getPurchasePrice(), FORMATO_DECIMAL);
            if (productEdit.getPurchasePrice() != 0)
                purchasePriceEditText.setText(purchasePrice);
            _stockQuantity = (int) productEdit.getStock();
            stockQuantity = Integer.toString(_stockQuantity);
            stockQuantityEditText.setText(stockQuantity);
            minQuantity = Integer.toString((int) productEdit.getMinStock());
            if (!minQuantity.equals("0"))
                minQuantityEditText.setText(minQuantity);
            _addQuantity = 0;
            //addStockQuantityEditText.setText(Integer.toString(_addQuantity));
            newStockQuantityEditText.setText(Integer.toString(_addQuantity + _stockQuantity));

            MostrarDatosUltimaCompra(productEdit);
        }
    }

    private void Init_CustomConfigPais(String codePais) {

        //FORMATO_DECIMAL = codePais.equals("AR") ? Formatos.DecimalFormat_US : Formatos.DecimalFormat_CH;
        //FORMATO_DECIMAL_SECUNDARIO = codePais.equals("AR") ? Formatos.DecimalFormat : Formatos.DecimalFormat_US;

        FORMATO_DECIMAL = Formato.getDecimal_Format(codePais);

        txtTaxId.setHint(Resources.getStringFromResource(this, codePais, "prompt_tax_id"));
    }



    /*
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            View view =  inflater.inflate(R.layout.activity_stock_edit, container, false);

            Init_Views(view);

            Init_Eventos(view);

            if(productEdit != null){
                salePrice = Formatos.FormateaDecimal(productEdit.getSalePrice(), Formatos.DecimalFormat_CH);
                if(!salePrice.equals("0"))
                    salePriceEditText.setText(salePrice);
                purchasePrice = Formatos.FormateaDecimal(productEdit.getPurchasePrice(), Formatos.DecimalFormat_CH);
                if(!purchasePrice.equals("0"))
                    purchasePriceEditText.setText(purchasePrice);
                _stockQuantity = (int) productEdit.getStock();
                stockQuantity = Integer.toString(_stockQuantity);
                stockQuantityEditText.setText(stockQuantity);
                minQuantity = Integer.toString((int)productEdit.getMinStock());
                if(!minQuantity.equals("0"))
                    minQuantityEditText.setText(minQuantity);
                _addQuantity = 0;
                //addStockQuantityEditText.setText(Integer.toString(_addQuantity));
                newStockQuantityEditText.setText(Integer.toString(_addQuantity + _stockQuantity));
            }

            return view;
        }
    */
    private void Init_Views() {
        btnGrabar = (Button) findViewById(R.id.frg_stock_edit_btn_grabar);
        btnCancelar = (Button) findViewById(R.id.frg_stock_edit_btn_cancelar);
        btnFindProvider = (ImageButton) findViewById(R.id.provider_btn_find_prov);
        salePriceEditText = (EditText) findViewById(R.id.product_sale_price);
        minQuantityEditText = (EditText) findViewById(R.id.product_min_quantity);
        stockQuantityEditText = (EditText) findViewById(R.id.product_stock_quantity);
        purchasePriceEditText = (EditText) findViewById(R.id.product_purchase_price);
        addStockQuantityEditText = (EditText) findViewById(R.id.product_add_quantity);
        addStockQuantityEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s))
                    _addQuantity = Integer.parseInt(s.toString());
                else
                    _addQuantity = 0;

                newStockQuantityEditText.setText(Integer.toString(_addQuantity + _stockQuantity));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        newStockQuantityEditText = (EditText) findViewById(R.id.product_new_quantity);
        txtRazonSocial = (TextView) findViewById(R.id.provider_txt_proveedor);
        txtTaxId = (EditText) findViewById(R.id.provider_tax_id);
        seccion_ultimo_pedido = findViewById(R.id.frg_edit_stock_ultima_compra);
    }

    private void Init_Eventos() {
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();//Cerrar();
            }
        });
        btnGrabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Confirmar();
            }
        });
        btnFindProvider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AbrirFragmentProveedoresFrecuentes();
            }
        });
        findViewById(R.id.img_dummy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        txtTaxId.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                    List<Provider> providers = new ProviderDao(getContentResolver()).list("tax_id = '" + ((EditText) view).getText().toString() + "'", null, null);

                    if (providers.size() > 0) {
                        SelectProvider(providers.get(0));
                    } else {
                        SelectProvider(null);
                    }
                }
            }
        });
    }

    public void AbrirFragmentProveedoresFrecuentes(){
        if (findViewById(R.id.frg_stock_edit_provider_fragment) != null) {
            // Create a new Fragment to be placed in the activity layout
            ProvidersFragment fragment = ProvidersFragment.newInstance(productEdit);
            fragment.setOnProvidersInteractionListener(this);
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.frg_stock_edit_provider_fragment, fragment);
            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            ft.commit();
        }
    }

    private void MostrarDatosUltimaCompra(Product p) {

        try {
            //obtenemos el ultimo pedido donde se pidio este producto
            CompraDetalle ult_item = new CompraDetalleDao(getContentResolver()).first(String.format("id_producto = %d", p.getId()), null, "id_compra DESC");

            seccion_ultimo_pedido.setVisibility(View.VISIBLE);

            if (ult_item == null) {//puede no haberse comprado nunca
                ((TextView) seccion_ultimo_pedido.findViewById(R.id.frg_edit_stock_ult_cpa_f_txt)).setText("NO REGISTRA");
                ((TextView) seccion_ultimo_pedido.findViewById(R.id.frg_edit_stock_ult_cpa_q_txt)).setText("0");
                ((TextView) seccion_ultimo_pedido.findViewById(R.id.frg_edit_stock_ult_cpa_p_txt)).setText("0");
                ((TextView) seccion_ultimo_pedido.findViewById(R.id.frg_edit_stock_ult_cpa_prov_txt)).setText("");
            } else {

                Compra ult_compra = new CompraDao(getContentResolver()).find(ult_item.getIdCompra());

                ((TextView) seccion_ultimo_pedido.findViewById(R.id.frg_edit_stock_ult_cpa_f_txt)).setText(String.format("# %d - %s", ult_compra.getId(), Formatos.FormateaDate(ult_compra.getFecha(), Formatos.DateFormatSlash)));
                ((TextView) seccion_ultimo_pedido.findViewById(R.id.frg_edit_stock_ult_cpa_q_txt)).setText(String.valueOf(ult_item.getCantidad()));
                ((TextView) seccion_ultimo_pedido.findViewById(R.id.frg_edit_stock_ult_cpa_p_txt)).setText(Formato.FormateaDecimal(ult_item.getPrecio(), FORMATO_DECIMAL, Formato.getCurrencySymbol(FORMATO_DECIMAL)));
                ((TextView) seccion_ultimo_pedido.findViewById(R.id.frg_edit_stock_ult_cpa_prov_txt)).setText(ult_compra.getProveedor() != null ? ult_compra.getProveedor().getName() : "");

            }

        }
        catch (Exception ex){
            Logger.RegistrarEvento(this,"e","CompraProducto","Error al consultar datos ult.cpa");
        }
    }

    public void SelectProvider(Provider _provider) {
        providerSelected = _provider;

        if (_provider != null) {
            txtRazonSocial.setText(_provider.getName());
            txtTaxId.setError(null);
            txtTaxId.setText(_provider.getTax_id());
        } else {
            //provider not found
            txtRazonSocial.setText("");
            txtTaxId.setError("Proveedor no existente");
            txtTaxId.requestFocus();
        }
    }

    /*@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void Cerrar() {
        FragmentManager fm = getFragmentManager();

        Fragment fragment = fm.findFragmentById(R.id.prod_search_frg_stock_edit);

        FragmentTransaction ft = fm.beginTransaction();

        ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);

        ft.hide(fragment);

        ft.commit();
    }*/

    public void Confirmar() {

        super.onSave(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Grabar();
            }
        });

        //View view = getLayoutInflater().inflate(R.layout.dialog_confirm_new_stock, null);
        /*
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_menu_help)
                .setTitle("Confirmar nuevo stock")
                //.setMessage("Confirma que el nuevo stock a asignar al producto \"" + productEdit.getName() + "\" es: " + newStockQuantityEditText.getText().toString() + "?")
                .setMessage(Html.fromHtml("<big>Confirma que el nuevo stock a asignar al producto <b>\"" + productEdit.getName() + "\"</b> es: <h2>" + newStockQuantityEditText.getText().toString() + "</h2>?</big>"))
                .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Grabar();
                    }

                })
                .setNegativeButton("NO", null)
                .setCancelable(false)
                //.setView(view)
                .show();
        */
    }

    public void Grabar() {
        if (productTask != null) {
            return;
        }

        boolean cancel = false;

        // Reset errors.
        salePriceEditText.setError(null);
        minQuantityEditText.setError(null);
        stockQuantityEditText.setError(null);
        purchasePriceEditText.setError(null);
        txtTaxId.setError(null);

        // Store values at the time of the save attempt.
        salePrice = salePriceEditText.getText().toString().trim();
        minQuantity = minQuantityEditText.getText().toString().trim();
        newStockQuantity = newStockQuantityEditText.getText().toString().trim();
        purchasePrice = purchasePriceEditText.getText().toString().trim();

        //los ponemos en cero para que luego al parsearlo como double no de error de formato
        if (TextUtils.isEmpty(salePrice)) {
            salePrice = "0";
        }

        if (TextUtils.isEmpty(minQuantity)) {
            minQuantity = "0";
        }

        if (TextUtils.isEmpty(newStockQuantity)) {
            newStockQuantity = "0";
        }

        if (TextUtils.isEmpty(purchasePrice)) {
            purchasePrice = "0";
        }

        if(providerSelected == null){
            txtTaxId.setError("Proveedor obligatorio");
            //txtTaxId.requestFocus();
            cancel = true;
        }

        if(purchasePrice.equals("0")){
            purchasePriceEditText.setError("Precio obligatorio");
            purchasePriceEditText.requestFocus();
            cancel = true;
        }

        if(_addQuantity == 0){
            addStockQuantityEditText.setError("Cantidad comprada obligatoria");
            addStockQuantityEditText.requestFocus();
            cancel = true;
        }


        if (!cancel) {

            ProviderProductDao providerProductDao = new ProviderProductDao(getContentResolver());

            //vemos si existe ya la relacion
            if(!providerProductDao.existeRelacion(productEdit.getId(),providerSelected.getId()))
                 //no existe => la creamos
                providerProductDao.save(new ProviderProduct(productEdit.getId(),providerSelected.getId()));

            productTask = new ProductTask();
            productTask.execute((Void) null);

            /*
            if (((ProvidersFragment) getFragmentManager().findFragmentById(R.id.frg_stock_edit_provider_fragment)).Grabar()) {
                productTask = new ProductTask();
                productTask.execute((Void) null);
            } else
                Toast.makeText(this, "Error: no se pudo guardar la relación entre el producto y el proveedor seleccionado", Toast.LENGTH_LONG).show();
            */
        }
    }

    @Override
    public void onProviderSeleccionado(Provider provider) {
        SelectProvider(provider);
    }

    public class ProductTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {

            productEdit.setSalePrice(Formato.ParseaDecimal(salePrice, FORMATO_DECIMAL, true));
            productEdit.setMinStock(Double.parseDouble(minQuantity));
            productEdit.setStock(Double.parseDouble(newStockQuantity));
            productEdit.setPurchasePrice(Formato.ParseaDecimal(purchasePrice, FORMATO_DECIMAL, true));

            //creamos una factura de compra con un solo item
            Compra _cabecera = new Compra();
            _cabecera.setProveedor(providerSelected);
            _cabecera.setFecha(new Date());
            _cabecera.setNumero("0000-00000000");

            //asociamos un item a la cabecera
            CompraDetalle _detalle = new CompraDetalle();
            _detalle.setCompra(_cabecera);
            _detalle.setIdProducto(productEdit.getId());
            _detalle.setProducto(productEdit);
            _detalle.setCantidad(_addQuantity);
            _detalle.setPrecio(productEdit.getPurchasePrice());
            _detalle.setPrecio_vta(productEdit.getSalePrice());

            double subtotal = _detalle.getPrecio() * _detalle.getCantidad();
            _detalle.setSubtotal_exento(0);
            _detalle.setSubtotal_neto_grav(0);
            _detalle.setSubtotal_impuesto(0);

            if (productEdit.getIdTax() == 4) //EXENTO
                _detalle.setSubtotal_exento(subtotal);
            else //GRAVADO, NO GRAVADO, INCLUIDO
            {
                double iva = productEdit.getIva() / 100;

                _detalle.setSubtotal_neto_grav(subtotal);
                _detalle.setSubtotal_impuesto(subtotal * iva);
            }

            _detalle.setTotal(_detalle.getSubtotal_exento() + _detalle.getSubtotal_neto_grav() + _detalle.getSubtotal_impuesto());

            _cabecera.setSubtotal_exento(_detalle.getSubtotal_exento());
            _cabecera.setSubtotal_impuesto(_detalle.getSubtotal_neto_grav());
            _cabecera.setSubtotal_neto_grav(_detalle.getSubtotal_impuesto());
            _cabecera.setTotal(_detalle.getTotal());

            if (!new CompraDao(getContentResolver()).save(_cabecera)) {
                Log.e("StockEditActivity", "Error al generar la Factura de Compra");
                return false;
            } else {

                CompraDetalleDao compraDetalleDao = new CompraDetalleDao(getContentResolver());
                compraDetalleDao.save(_detalle);
            }

            RevisaGrabaRelacion(productEdit, _cabecera.getProveedor());

            Log.d("StockEditActivity", "product added/updated");
            return new ProductDao(getContentResolver(),config.SYNC_STOCK ? 1 : 0).saveOrUpdate(productEdit);
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            productTask = null;

            CharSequence text;
            if (success)
                text = getString(R.string.message_save_success_product);
            else
                text = getString(R.string.message_save_failed_product);

            Toast.makeText(StockEditActivity.this, text, Toast.LENGTH_SHORT).show();

            if (success)
                finish();//Cerrar();

        }

        @Override
        protected void onCancelled() {
            productTask = null;
        }

        public void RevisaGrabaRelacion(Product product, Provider provider) {

            ProviderProductDao providerProductDao = new ProviderProductDao(getContentResolver());
                //vemos si existe ya la relacion, sino la creamos
                if (!providerProductDao.existeRelacion(product.getId(), provider.getId())) {
                    providerProductDao.save(new ProviderProduct(product.getId(), provider.getId()));
                }
        }
    }



}
