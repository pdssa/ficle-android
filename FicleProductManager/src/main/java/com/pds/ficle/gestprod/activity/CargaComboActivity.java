/*
package com.pds.ficle.gestprod.activity;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Formatos;
import com.pds.common.activity.TimerActivity;
import com.pds.common.dao.ComboDao;
import com.pds.common.dao.ComboItemDao;
import com.pds.common.dao.ProductDao;
import com.pds.common.db.ProductTable;
import com.pds.common.model.Combo;
import com.pds.common.model.ComboItem;
import com.pds.common.model.Product;
import com.pds.ficle.gestprod.R;
import com.pds.ficle.gestprod.fragment.AddComboProductoSecundario;
import com.pds.ficle.gestprod.util.ViewUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.pds.ficle.gestprod.fragment.AsociarCodeFragment.CodigoEnUso;

public class CargaComboActivity extends TimerActivity implements AddComboProductoSecundario.OnComboFragmentInteractionListener {

    private Config config;
    private DecimalFormat FORMATO_DECIMAL;
    private Button btnGrabar;
    private Button btnCancelar;
    private Button btnEliminar;
    private Button btnAddItem;
    private Button btnSiguiente;
    private ImageButton btnAddProducto;
    private EditText txtDescripcionProducto;
    private EditText txtDescripcion;
    private TextView lblPrecioReferencia;
    private EditText txtNombre;
    private EditText txtPrecio;
    private EditText txtCantidad;
    private ListView lstItems;
    private List<ComboItem> _items;
    private List<ComboItem> _itemsDel;
    private ItemsArrayAdapter listAdapter;
    private int editPosition;
    private boolean flag;
    private Combo _combo;
    private Product _productPpal;
    private EditText txtCodigoRapido;

    private Timer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carga_combo);

        //Obtengo el Formato de Decimales segun la configuación de país que tenga
        config = new Config(this);
        Init_CustomConfigPais(config.PAIS);

        AsignarViews();

        AsignarEventos();

        _combo = getIntent().getParcelableExtra("combo_edit");

        if (_combo == null) {
            //_combo = new Combo();
            _items = new ArrayList<ComboItem>();

            btnAddItem.setEnabled(false);

            WizardStep(WIZARD_STEP_NOMBRE);//iniciamos el wizard de creacion de combo
        } else {
            //datos combo
            txtNombre.setText(_combo.getNombre());
            txtDescripcion.setText(_combo.getDescripcion());
            txtPrecio.setText(Formatos.FormateaDecimal(_combo.getPrecio(), FORMATO_DECIMAL));
            txtCodigoRapido.setText(_combo.getCodigo());

            //datos producto ppal
            txtCantidad.setText(Integer.toString((int) _combo.getCantidad()));
            SelectProduct(_combo.getProductoPrincipal(getContentResolver()), false);

            //items
            _items = _combo.getItemsCombo(getContentResolver());

            VistaEdit();

            if (_productPpal != null && _items != null) {

                for (ComboItem item : _items) {
                    //actualizamos los precios de los items del combo
                    Product p_item = item.getProducto(getContentResolver());
                    item.setProducto_precio_vta(p_item != null ? p_item.getSalePrice() : 0);
                }

                //actualizamos el precio de referencia del combo
                ActualizarPrecioReferenciaCombo(_items, _combo.getCantidad() * _productPpal.getSalePrice(), _combo.getPrecio());

            }
        }

        listAdapter = new ItemsArrayAdapter(CargaComboActivity.this, _items);
        lstItems.setAdapter(listAdapter);
    }

    private void AsignarViews() {
        btnGrabar = (Button) findViewById(R.id.carga_combo_btn_grabar);
        btnCancelar = (Button) findViewById(R.id.carga_combo_back_button);
        btnEliminar = (Button) findViewById(R.id.carga_combo_delete_button);
        btnAddItem = (Button) findViewById(R.id.button_combo_add_producto_secundario);
        btnAddProducto = (ImageButton) findViewById(R.id.btn_carga_combo_buscar_producto);
        btnSiguiente = (Button) findViewById(R.id.carga_combo_btn_siguiente);
        txtDescripcionProducto = (EditText) findViewById(R.id.txt_carga_combo_producto_principal);
        lblPrecioReferencia = (TextView) findViewById(R.id.lbl_carga_combo_precio_referncia);
        txtDescripcion = (EditText) findViewById(R.id.txt_carga_combo_descripcion);
        txtNombre = (EditText) findViewById(R.id.txt_carga_combo_nombre);
        txtPrecio = (EditText) findViewById(R.id.txt_carga_combo_precio);
        txtCantidad = (EditText) findViewById(R.id.txt_carga_combo_cantidad);
        txtCodigoRapido = (EditText) findViewById(R.id.txt_carga_combo_codigo_rapido);

        lstItems = (ListView) findViewById(R.id.list_item_combos);
        lstItems.setEmptyView(findViewById(android.R.id.empty));

        //definimos las "zonas touch" del activity
        ViewUtils.DefineViewGroup(this, txtNombre.getId(), R.id.label_carga_combo_nombre);
        ViewUtils.DefineViewGroup(this, txtCantidad.getId(), R.id.label_carga_combo_cantidad);
        ViewUtils.DefineViewGroup(this, txtPrecio.getId(), R.id.label_carga_combo_precio);
        ViewUtils.DefineViewGroup(this, txtDescripcion.getId(), R.id.label_carga_combo_descripcion);
        ViewUtils.DefineViewGroup(this, txtCodigoRapido.getId(), R.id.lbl_carga_combo_codigo_rapido);

    }

    private void VistaEdit() {
        txtNombre.setVisibility(View.VISIBLE);
        findViewById(R.id.label_carga_combo_nombre).setVisibility(View.VISIBLE);
        txtDescripcion.setVisibility(View.VISIBLE);
        findViewById(R.id.label_carga_combo_descripcion).setVisibility(View.VISIBLE);

        findViewById(R.id.carga_combo_seccion_prod).setVisibility(View.VISIBLE);

        txtCantidad.setVisibility(View.VISIBLE);
        findViewById(R.id.label_carga_combo_cantidad).setVisibility(View.VISIBLE);
        findViewById(R.id.carga_combo_seccion_cant_prec).setVisibility(View.VISIBLE);
        txtPrecio.setVisibility(View.VISIBLE);
        findViewById(R.id.label_carga_combo_precio).setVisibility(View.VISIBLE);
        txtCodigoRapido.setVisibility(View.VISIBLE);
        findViewById(R.id.lbl_carga_combo_codigo_rapido).setVisibility(View.VISIBLE);
        findViewById(R.id.carga_combo_seccion_cod_rap).setVisibility(View.VISIBLE);
        findViewById(R.id.carga_combo_sec_items_1).setVisibility(View.VISIBLE);
        findViewById(R.id.carga_combo_sec_items_2).setVisibility(View.VISIBLE);

        btnGrabar.setVisibility(View.VISIBLE);
        btnSiguiente.setVisibility(View.GONE);
        btnEliminar.setVisibility(View.VISIBLE);
        btnAddItem.setEnabled(true);

    }

    private void IniciarBusquedaProductoPpal(int requestcode) {
        Intent intent = new Intent(CargaComboActivity.this, ProductSearchActivity.class);
        intent.putExtra("TypeResultSearch", ProductSearchActivity.TypeResultSearch.SEARCH_PRODUCT_BACK);
        startActivityForResult(intent, requestcode);
    }

    private void volver() {
        onBackPressed();
    }

    private void AsignarEventos() {
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                volver();
            }
        });

        btnGrabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Grabar();
            }
        });

        btnAddProducto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IniciarBusquedaProductoPpal(REQUEST_PRODUCT);
            }
        });

        btnAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IniciarFragmentAddEditItem();
            }
        });

        btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Eliminar();
            }
        });

        lstItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ComboItem det = (ComboItem) parent.getItemAtPosition(position);

                editPosition = position;

                IniciarFragmentAddEditItem(det);
            }
        });

        btnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WizardStep(WIZARD_NEXT_STEP);
            }
        });

        */
/*txtCodigo.setFilters(new InputFilter[]{new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++)
                    if (!Character.isLetter(source.charAt(i)) &&
                            !Character.isSpaceChar(source.charAt(i)) &&
                            !Character.isDigit(source.charAt(i))
                            ) //solo aceptamos: letras, numeros y espacio
                        return "";

                return null;
            }
        }});

        txtCodigo.addTextChangedListener(new TextWatcher() {

            private final long DELAY = 500; // in ms

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (timer == null)
                    timer = new Timer();
                else {
                    timer.cancel();
                    timer = new Timer();
                }

                final Editable _s = s;

                if (s.length() > 0) {
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (!ObtenerProductoByCode(_s.toString()))
                                        SelectProduct(null, false);//no encontramos code

                                    timer.cancel();
                                }
                            });
                        }
                    }, DELAY);
                } //else ((ImageView) findViewById(R.id.prod_search_btn_find)).setImageResource(R.drawable.lupa);
            }
        });
        *//*

        txtCantidad.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    //InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    //imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                    ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                }

            }
        });
        TextWatcher actualizarPrecioReferencia = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!TextUtils.isEmpty(s))
                    ActualizarPrecioReferenciaCombo(_items);//actualizamos el precio de referencia al cambiar la cantidad
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        txtCantidad.addTextChangedListener(actualizarPrecioReferencia);
        txtPrecio.addTextChangedListener(actualizarPrecioReferencia);

        txtCodigoRapido.setFilters(new InputFilter[]{new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++)
                    if (!Character.isLetter(source.charAt(i)) &&
                            !Character.isSpaceChar(source.charAt(i)) &&
                            !Character.isDigit(source.charAt(i))
                            ) //solo aceptamos: letras, numeros y espacio
                        return "";

                return null;
            }
        }});

        txtCodigoRapido.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus && !TextUtils.isEmpty(txtCodigoRapido.getText())) {
                    String codRapido = String.format("%03d", Integer.valueOf(txtCodigoRapido.getText().toString()));

                    Long codeCombo = _combo != null ? _combo.getId() : -1;

                    if (CodigoEnUso(codRapido, -1, codeCombo, CargaComboActivity.this)) {
                        txtCodigoRapido.setError("Codigo en uso");
                    } else {
                        txtCodigoRapido.setError(null);
                    }
                }
            }
        });

    }

    private void agregarCantidadProducto(Editable s) {
        if (!s.equals(".")) {
            if (flag) {
                flag = false;
                this.txtCantidad.setText("");
                this.txtCantidad.setText(this.txtCantidad.getText().toString() + s);
            } else {
                this.txtCantidad.setText(this.txtCantidad.getText().toString() + s);
            }
        }
    }

    private void Init_CustomConfigPais(String codePais) {
        FORMATO_DECIMAL = codePais.equals("AR") ? Formatos.DecimalFormat_US : Formatos.DecimalFormat_CH;
    }

    private final int REQUEST_PRODUCT_WIZARD = 1;
    private final int REQUEST_PRODUCT = 2;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_PRODUCT: {
                if (resultCode == Activity.RESULT_OK) {
                    Object result = data.getParcelableExtra("result");
                    SelectProduct((Product) result, true);
                }
                if (resultCode == Activity.RESULT_CANCELED) {
                    //Write your code if there's no result
                    //SelectProvider(null);
                }
            }
            break;
            case REQUEST_PRODUCT_WIZARD: {
                if (resultCode == Activity.RESULT_OK) {
                    Object result = data.getParcelableExtra("result");

                    findViewById(R.id.carga_combo_seccion_prod).setVisibility(View.VISIBLE);

                    SelectProduct((Product) result, false);

                    WIZARD_NEXT_STEP = WIZARD_STEP_DESCRIPCION;

                    WizardStep(WIZARD_STEP_DESCRIPCION);
                }
                if (resultCode == Activity.RESULT_CANCELED) {
                    //Write your code if there's no result
                    //SelectProvider(null);
                }
            }
            break;
        }
    }

    public void IniciarFragmentAddEditItem() {
        IniciarFragmentAddEditItem(null);
    }

    public void IniciarFragmentAddEditItem(ComboItem item_edit) {
        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.frgAddEditItem) != null) {

            // Create a new Fragment to be placed in the activity layout
            AddComboProductoSecundario fragment = new AddComboProductoSecundario().newInstance(item_edit);

            fragment.setOnComboFragmentInteractionListener(this);

            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.frgAddEditItem, fragment);
            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            ft.commit();
        }
    }

    public void SelectProduct(Product _product, boolean newProduct) {
        if (_product != null) {
            _productPpal = _product;
            //txtCodigo.setText(_product.getCode());
            txtDescripcionProducto.setText(_product.getName());
            if (timer != null)
                timer.cancel();

            btnAddItem.setEnabled(true);

            if (newProduct) {
                txtCantidad.setText("1");
                txtCantidad.requestFocus();
            }

        } else {
            //product not found
            _productPpal = null;
            txtDescripcionProducto.setText(null);
            txtDescripcionProducto.setError("Producto no existente");
            //txtCodigo.setError("Producto no existente");
        }
    }

    private boolean ObtenerProductoByCode(String code) {
        try {

            boolean pudoLeer = true;

            List<Product> productos = new ProductDao(getContentResolver()).list(ProductTable.COLUMN_CODE + " = '" + code + "' AND removed = 0", null, null);

            if (productos.size() > 0) {
                // en el caso de haber mas de uno, nos quedamos solo con el primero
                Product p = productos.get(0);
                SelectProduct(p, true);
            } else {
                pudoLeer = false;
            }

            return pudoLeer;

        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
            return false;
        }
    }

    @Override
    public void onItemCreated(ComboItem new_item) {
        this._items.add(new_item);

        listAdapter.notifyDataSetChanged();

        ActualizarPrecioReferenciaCombo(this._items);
    }

    @Override
    public void onItemEdited(ComboItem edited_item) {
        _items.set(editPosition, edited_item);

        listAdapter.notifyDataSetChanged();

        ActualizarPrecioReferenciaCombo(this._items);
    }

    @Override
    public void onItemDeleted(ComboItem deleted_item) {
        _items.remove(editPosition);

        if (_itemsDel == null)
            _itemsDel = new ArrayList<ComboItem>();

        _itemsDel.add(deleted_item);

        listAdapter.notifyDataSetChanged();

        ActualizarPrecioReferenciaCombo(this._items);
    }

    private void ActualizarPrecioReferenciaCombo(List<ComboItem> items) {
        try {
            String precio = txtPrecio.getText().toString().trim();
            String cantidad = txtCantidad.getText().toString().trim();

            if (!TextUtils.isEmpty(precio) && !TextUtils.isEmpty(cantidad)) {
                if (_productPpal != null)
                    ActualizarPrecioReferenciaCombo(items, Double.parseDouble(cantidad) * _productPpal.getSalePrice(), Formatos.ParseaDecimal(precio, FORMATO_DECIMAL));
            }
        } catch (Exception ex) {
            Toast.makeText(this, "Error al calcular precio de referencia: " + ex.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    private void ActualizarPrecioReferenciaCombo(List<ComboItem> items, double precioProductoPpal, double precioCombo) {
        try {
            double totalItems = precioProductoPpal;

            for (ComboItem item : items) {
                //if(item.getProducto_precio_vta() != null)
                totalItems += (item.getCantidad() * item.getProducto_precio_vta());
            }

            double diferencia = totalItems != 0 ? ((precioCombo /totalItems) - 1) * 100 : 100;

            //redondeamos en dos decimales:
            diferencia = Math.round(diferencia * 100);
            diferencia = diferencia/100;

            String s_diferencia = Formatos.FormateaDecimal(diferencia, Formatos.DecimalFormat_US) + "%";

            lblPrecioReferencia.setText(String.format("Precio de referencia:   %s   %s", Formatos.FormateaDecimal(totalItems, FORMATO_DECIMAL, Formatos.getCurrencySymbol(config.PAIS)), s_diferencia));
            lblPrecioReferencia.setVisibility(View.VISIBLE);
        } catch (Exception ex) {
            Toast.makeText(this, "Error al calcular precio de referencia: " + ex.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    private final int WIZARD_STEP_NOMBRE = 1;
    private final int WIZARD_STEP_PRODUCTO_PPAL = 2;
    private final int WIZARD_STEP_DESCRIPCION = 3;
    private final int WIZARD_STEP_CANTIDAD = 4;
    private final int WIZARD_STEP_PRECIO = 5;
    private final int WIZARD_STEP_CODIGO_RAPIDO = 6;
    private final int WIZARD_STEP_ITEMS = 7;

    private int WIZARD_NEXT_STEP = WIZARD_STEP_NOMBRE;

    private void WizardStep(int nextStep) {
        switch (nextStep) {
            case WIZARD_STEP_NOMBRE: {
                ViewFocus(txtNombre, R.id.label_carga_combo_nombre, 0);

                WIZARD_NEXT_STEP = WIZARD_STEP_PRODUCTO_PPAL;
            }
            break;
            case WIZARD_STEP_PRODUCTO_PPAL: {
                //validamos si se ingresó el nombre
                txtNombre.setError(null);
                if (TextUtils.isEmpty(txtNombre.getText().toString().trim())) {
                    txtNombre.setError("Nombre del combo es obligatorio");
                    txtNombre.requestFocus();
                    return;
                }

                //llamamos a la busqueda del producto
                IniciarBusquedaProductoPpal(REQUEST_PRODUCT_WIZARD);
            }
            break;
            case WIZARD_STEP_DESCRIPCION: {
                ViewFocus(txtDescripcion, R.id.label_carga_combo_descripcion, 0);

                WIZARD_NEXT_STEP = WIZARD_STEP_CANTIDAD;
            }
            break;
            case WIZARD_STEP_CANTIDAD: {
                //no validamos si se ingresó la descripcion, es opcional

                ViewFocus(txtCantidad, R.id.label_carga_combo_cantidad, R.id.carga_combo_seccion_cant_prec);

                WIZARD_NEXT_STEP = WIZARD_STEP_PRECIO;
            }
            break;
            case WIZARD_STEP_PRECIO: {
                //validamos si se ingresó la cantidad
                txtCantidad.setError(null);
                if (TextUtils.isEmpty(txtCantidad.getText().toString().trim())) {
                    txtCantidad.setError("Cantidad del producto principal del combo es obligatorio");
                    txtCantidad.requestFocus();
                    return;
                }

                ViewFocus(txtPrecio, R.id.label_carga_combo_precio, R.id.carga_combo_seccion_cant_prec);

                WIZARD_NEXT_STEP = WIZARD_STEP_CODIGO_RAPIDO;
            }
            break;
            case WIZARD_STEP_CODIGO_RAPIDO: {
                //validamos si se ingresó el precio de venta
                txtPrecio.setError(null);
                if (TextUtils.isEmpty(txtPrecio.getText().toString().trim())) {
                    txtPrecio.setError("Precio del combo es obligatorio");
                    txtPrecio.requestFocus();
                    return;
                }

                //ya tenemos precio, mostramos un precio de referencia sin items aún
                ActualizarPrecioReferenciaCombo(new ArrayList<ComboItem>());

                ViewFocus(txtCodigoRapido, R.id.lbl_carga_combo_codigo_rapido, R.id.carga_combo_seccion_cod_rap);

                WIZARD_NEXT_STEP = WIZARD_STEP_ITEMS;

            }
            break;
            case WIZARD_STEP_ITEMS: {
                //llegamos al final, ocultamos el boton de "siguente" y habilitamos el "grabar"
                btnGrabar.setVisibility(View.VISIBLE);
                btnSiguiente.setVisibility(View.GONE);

                //iniciamos el agregado de items

                findViewById(R.id.carga_combo_sec_items_1).setVisibility(View.VISIBLE);
                findViewById(R.id.carga_combo_sec_items_2).setVisibility(View.VISIBLE);


                btnAddItem.performClick();

                WIZARD_NEXT_STEP = 0;
            }
            break;
        }
    }

    private void ViewFocus(final View view, int labelViewId, int viewContainerId) {
        view.setVisibility(View.VISIBLE);
        view.requestFocus();

        if (view.getId() == txtDescripcion.getId())
            //la descripcion se toma despues de volver de la pantalla de productos, entonces ponemos un delay para que muestre el teclado
            view.postDelayed(new Runnable() {
                @Override
                public void run() {
                    ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(view, 0);
                }
            }, 200);
        else
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(view, 0);

        if (labelViewId != 0)
            findViewById(labelViewId).setVisibility(View.VISIBLE);

        if (viewContainerId != 0)
            findViewById(viewContainerId).setVisibility(View.VISIBLE);

    }

    public class ItemsArrayAdapter extends ArrayAdapter<ComboItem> {
        private final Context context;
        private List<ComboItem> items;

        public ItemsArrayAdapter(Context context, List<ComboItem> items) {
            super(context, R.layout.list_item_compra, items);
            this.context = context;
            this.items = items;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View rowView = inflater.inflate(R.layout.list_item_pedido_detalle, parent, false);

            ComboItem item = items.get(position);

            Product p = item.getProducto(getContentResolver());

            ((TextView) rowView.findViewById(R.id.list_item_producto_cod)).setText(TextUtils.isEmpty(p.getCode()) ? "S/C" : p.getCode());
            ((TextView) rowView.findViewById(R.id.list_item_pedidos_producto_det)).setText(p.getName());
            ((TextView) rowView.findViewById(R.id.list_item_pedidos_cantidad)).setText(Integer.toString((int) item.getCantidad()));

            return rowView;
        }

    }

    private void Eliminar() {
        try {
            if (_combo != null) {
                _combo.setBaja(true);
                _combo.setSync("N");

                ComboDao comboDao = new ComboDao(getContentResolver());

                boolean result = comboDao.saveOrUpdate(_combo);

                //tengo que desmarcar el producto como ppal si era el unico combo que tenia asociado
                boolean updateProduct = (comboDao.first(String.format("producto_ppal_id = %d and baja = 0", _productPpal.getId()), null, null) == null);

                if (updateProduct) {
                    //ya no quedan combos habilitados con ese producto como principal
                    //grabamos el producto como principal de un combo
                    _productPpal.setType(""); //lo marcamos como producto ppal de un combo

                    new ProductDao(getContentResolver()).saveOrUpdate(_productPpal);
                }

                if (result) {
                    Toast.makeText(CargaComboActivity.this, "Combo eliminado correctamente", Toast.LENGTH_SHORT).show();

                    btnCancelar.performClick();
                } else
                    Toast.makeText(CargaComboActivity.this, "No se ha podido eliminar el combo", Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(CargaComboActivity.this, "No se ha podido eliminar el combo", Toast.LENGTH_SHORT).show();

        } catch (Exception ex) {
            Toast.makeText(CargaComboActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void Grabar() {
        try {
            //****validaciones*****
            boolean cancel = false;

            //clear errors
            txtNombre.setError(null);
            txtPrecio.setError(null);
            txtCantidad.setError(null);
            txtDescripcionProducto.setError(null);
            txtCodigoRapido.setError(null);

            String nombre = txtNombre.getText().toString().trim();
            String descripcion = txtDescripcion.getText().toString().trim();
            String precio = txtPrecio.getText().toString().trim();
            String cantidad = txtCantidad.getText().toString().trim();
            String codigo = "";
            if(!TextUtils.isEmpty(txtCodigoRapido.getText().toString().trim()))
                codigo = String.format("%03d", Integer.valueOf(txtCodigoRapido.getText().toString().trim()));

            if (TextUtils.isEmpty(nombre)) {
                txtNombre.setError("Nombre del combo es obligatorio");
                cancel = true;
            }

            if (TextUtils.isEmpty(precio)) {
                txtPrecio.setError("Precio del combo es obligatorio");
                cancel = true;
            }

            if (TextUtils.isEmpty(cantidad)) {
                txtCantidad.setError("Cantidad del producto principal del combo es obligatorio");
                cancel = true;
            }

            if (_productPpal == null) {
                txtDescripcionProducto.setError("Producto principal del combo es obligatorio");
                cancel = true;
            }

            //Valida si el código rápido ya existe
            Long codeCombo = _combo != null ? _combo.getId() : -1;
            if (CodigoEnUso(codigo, -1, codeCombo, CargaComboActivity.this) && !TextUtils.isEmpty(codigo)) {
                txtCodigoRapido.setError("Codigo en uso");
                cancel = true;
            }

            //codigo comentado porque se definió que se puede crear un combo sin productos secuandarios
           */
/* if (_items.size() == 0) {
                Toast.makeText(CargaComboActivity.this, "Debe ingresar al menos un producto secundario al combo", Toast.LENGTH_SHORT).show();
                cancel = true;
            }*//*


            if (cancel)
                return;

            //****GRABAR*****
            boolean nuevo = false;

            if (_combo == null) {
                _combo = new Combo();

                _combo.setFecha_alta(new Date());

                nuevo = true;
            }

            _combo.setPrecio(Formatos.ParseaDecimal(precio, FORMATO_DECIMAL));
            _combo.setCantidad(Double.parseDouble(cantidad));
            _combo.setNombre(nombre);
            _combo.setDescripcion(descripcion);
            _combo.setProducto_ppal_code(_productPpal.getCode());
            _combo.setProducto_ppal_id(_productPpal.getId());
            _combo.setSync("N");
            _combo.setCodigo(codigo);

            if (!new ComboDao(getContentResolver()).saveOrUpdate(_combo)) {
                Toast.makeText(CargaComboActivity.this, "Error al generar el combo", Toast.LENGTH_SHORT).show();
            } else {

                //grabamos el producto como principal de un combo
                _productPpal.setType("P"); //lo marcamos como producto ppal de un combo

                new ProductDao(getContentResolver()).saveOrUpdate(_productPpal);

                //grabamos los items, si los tiene
                if (_items != null) {
                    ComboItemDao comboItemDao = new ComboItemDao(getContentResolver());

                    for (ComboItem det : _items) {

                        //asociamos la cabecera y grabamos
                        det.setCombo_id(_combo.getId());
                        det.setSync("N");

                        comboItemDao.saveOrUpdate(det);
                    }

                    if (_itemsDel != null) {
                        //si hay items para eliminar...
                        for (ComboItem det : _itemsDel) {
                            comboItemDao.delete(det);
                        }
                    }
                }

                if (nuevo)
                    Toast.makeText(CargaComboActivity.this, "Combo generado correctamente!", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(CargaComboActivity.this, "Combo actualizado correctamente!", Toast.LENGTH_SHORT).show();

                volver();
            }


        } catch (Exception ex) {
            Toast.makeText(CargaComboActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
*/
