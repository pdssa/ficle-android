package com.pds.ficle.gestprod.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Formatos;
import com.pds.common.dao.PedidoDao;
import com.pds.common.dao.PedidoDetalleDao;
import com.pds.common.dao.ProductDao;
import com.pds.common.db.ProductTable;
import com.pds.common.model.Pedido;
import com.pds.common.model.PedidoDetalle;
import com.pds.common.model.Product;
import com.pds.ficle.gestprod.R;
import com.pds.ficle.gestprod.activity.ProductSearchActivity;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AddPedidoItemFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AddPedidoItemFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddPedidoItemFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "product_add";
    private static final String ARG_PARAM2 = "item_edit";

    private PedidoItemFragmentInteractionListener mListener;
    private Button btnGrabar;
    private Button btnCancelar;
    private Button btnAddItem;
    private EditText txtCantidad;
    private EditText txtCodigo;
    private EditText txtDescripcion;
    private PedidoDetalle itemEdit;
    private Product productEdit;
    private Product productAdd;
    private ImageButton btnFindProd;
    private View seccion_ultimo_pedido;
    private TextView cant_stock;
    private TextView stock_actual;
    private boolean flagItemEdit = false;

    private Timer timer;

    public static AddPedidoItemFragment newInstance(Product productAdd, PedidoDetalle itemEdit) {
        AddPedidoItemFragment fragment = new AddPedidoItemFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, productAdd);
        args.putParcelable(ARG_PARAM2, itemEdit);
        //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public AddPedidoItemFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            productAdd = getArguments().getParcelable(ARG_PARAM1);
            itemEdit = getArguments().getParcelable(ARG_PARAM2);
            if (itemEdit != null) {
                productEdit = itemEdit.getProducto();
                flagItemEdit = true;
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_pedido_item, container, false);

        Init_Views(view);

        Init_Eventos(view);

        if (itemEdit != null) {
            CargarDatos(
                    itemEdit.getProducto().getCode(),
                    itemEdit.getProducto().getName(),
                    itemEdit.getCantidad(),
                    itemEdit.getProducto().getStock(),
                    itemEdit.getProducto()
            );
        } else if (productAdd != null) {
            SelectProduct(productAdd);
        }

        /*
        //posamos el foco sobre el codigo, pero quitamos el teclado, para que puedan ingresar el codigo via scanner
        txtCodigo.requestFocus();

        ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(txtCodigo.getWindowToken(), 0);
        */
        return view;
    }

    /*@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 2) {//requerimos un product
            if (resultCode == Activity.RESULT_OK) {
                Object result = data.getParcelableExtra("result");
                flagItemEdit = false;
                SelectProduct((Product) result);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                //SelectProvider(null);
            }
        }
    }*/

    private void Init_Views(View view) {
        btnGrabar = (Button) view.findViewById(R.id.frg_add_item_btn_grabar);
        btnCancelar = (Button) view.findViewById(R.id.frg_add_item_btn_cancelar);
        txtCantidad = (EditText) view.findViewById(R.id.frg_add_item_txt_cantidad);
        //txtCantidad.setRawInputType(Configuration.KEYBOARD_12KEY);
        txtCodigo = (EditText) view.findViewById(R.id.frg_add_item_txt_codigo);
        txtDescripcion = (EditText) view.findViewById(R.id.frg_add_item_txt_descripcion);
        //btnFindProd = (ImageButton) view.findViewById(R.id.fc_add_item_btn_find_prod);
        seccion_ultimo_pedido = view.findViewById(R.id.frg_add_item_ultimo_pedido);
        cant_stock = (TextView) view.findViewById(R.id.frg_add_item_cant_stock);
        stock_actual = (TextView) view.findViewById(R.id.frg_add_item_stock_actual);
    }

    private void Init_Eventos(final View view) {
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cerrar();
            }
        });

        btnGrabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GrabarItem();
            }
        });
        view.findViewById(R.id.img_dummy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cerrar();
            }
        });
        view.findViewById(R.id.frg_add_item_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        /*btnAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GrabarItem();
            }
        });

        btnFindProd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ProductSearchActivity.class);
                intent.putExtra("TypeResultSearch", ProductSearchActivity.TypeResultSearch.SEARCH_PRODUCT_BACK);
                startActivityForResult(intent, 2);
            }
        });

        txtCodigo.setFilters(new InputFilter[]{new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++)
                    if (!Character.isLetter(source.charAt(i)) &&
                            !Character.isSpaceChar(source.charAt(i)) &&
                            !Character.isDigit(source.charAt(i))
                            ) //solo aceptamos: letras, numeros y espacio
                        return "";

                return null;
            }
        }});

        txtCodigo.addTextChangedListener(new TextWatcher() {

            private final long DELAY = 500; // in ms

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (timer == null)
                    timer = new Timer();
                else {
                    timer.cancel();
                    timer = new Timer();
                }

                final Editable _s = s;

                if (s.length() > 0) {
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (ObtenerProductoByCode(_s.toString()))
                                        txtCantidad.requestFocus();
                                    else
                                        SelectProduct(null);//no encontramos code

                                    timer.cancel();
                                }
                            });
                        }
                    }, DELAY);
                } //else ((ImageView) findViewById(R.id.prod_search_btn_find)).setImageResource(R.drawable.lupa);
            }
        });*/

        txtCantidad.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                }
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        if (timer != null)
            timer.cancel();
    }

    public void GrabarItem() {
        try {

            txtCodigo.setError(null);
            txtCantidad.setError(null);

            if (productEdit == null) {
                txtCodigo.setError("Producto obligatorio");
                return ;
            }

            if (TextUtils.isEmpty(txtCantidad.getText())) {
                txtCantidad.setError("Debe ingresar una cantidad");
                return ;
            }

            boolean newItem = itemEdit == null;

            if (newItem)
                itemEdit = new PedidoDetalle();

            int cantidad = TextUtils.isEmpty(txtCantidad.getText()) ? 0 : Integer.parseInt(txtCantidad.getText().toString());

            itemEdit.setIdProducto(productEdit.getId());
            itemEdit.setProducto(productEdit);
            itemEdit.setCantidad(cantidad);


            if (mListener != null) {
                if (newItem) {
                    mListener.onItemCreated(itemEdit);
                } else {
                    mListener.onItemEdited(itemEdit);
                }

                Cerrar();
            }

        } catch (Exception ex) {
            Toast.makeText(AddPedidoItemFragment.this.getActivity(), "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void Cerrar() {
        if (getActivity().getCurrentFocus() != null)
            ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        //getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        FragmentManager fm = getActivity().getFragmentManager();

        Fragment fragment = fm.findFragmentById(R.id.frgAddEditItem);

        FragmentTransaction ft = fm.beginTransaction();

        ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);

        ft.hide(fragment);

        ft.commit();
    }

    private void ClearFragment() {
        itemEdit = null;
        productEdit = null;
        txtDescripcion.setText(null);
        txtCodigo.setText(null);
        txtCantidad.setText(null);
        cant_stock.setText(null);
        cant_stock.setVisibility(View.INVISIBLE);
        stock_actual.setVisibility(View.INVISIBLE);
        seccion_ultimo_pedido.setVisibility(View.INVISIBLE);

        txtCodigo.requestFocus();

        ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(txtCodigo.getWindowToken(), 0);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    public interface PedidoItemFragmentInteractionListener {
        public void onItemCreated(PedidoDetalle new_item);

        public void onItemEdited(PedidoDetalle edited_item);
    }

    public void setPedidoItemFragmentInteractionListener(PedidoItemFragmentInteractionListener mListener) {
        this.mListener = mListener;
    }

    private void CargarDatos(String codigo, String descripcion, int cantidad, double stock, Product _p) {
        txtCantidad.setText(cantidad != 0 ? String.valueOf(cantidad) : null);
        txtCodigo.setText(codigo);
        txtDescripcion.setText(descripcion);
        cant_stock.setText(String.valueOf(Formatos.FormateaDecimal(stock, Formatos.DecimalFormat_CH)));
        cant_stock.setVisibility(View.VISIBLE);
        stock_actual.setVisibility(View.VISIBLE);

        MostrarDatosUltimoPedido(_p);
    }

    private void MostrarDatosUltimoPedido(Product p) {

        //obtenemos el ultimo pedido donde se pidio este producto
        PedidoDetalle ult_item = new PedidoDetalleDao(getActivity().getContentResolver()).first(String.format("id_producto = %d", p.getId()), null, "id_pedido DESC");

        seccion_ultimo_pedido.setVisibility(View.VISIBLE);

        if (ult_item == null) {//puede no haberse pedido nunca
            ((TextView) seccion_ultimo_pedido.findViewById(R.id.frg_add_item_ult_ped_f_txt)).setText("NO REGISTRA");
            ((TextView) seccion_ultimo_pedido.findViewById(R.id.frg_add_item_ult_ped_q_txt)).setText("0");
        } else {

            Pedido ult_pedido = new PedidoDao(getActivity().getContentResolver()).find(ult_item.getId_pedido());

            ((TextView) seccion_ultimo_pedido.findViewById(R.id.frg_add_item_ult_ped_f_txt)).setText(String.format("# %d - %s", ult_pedido.getId(), Formatos.FormateaDate(ult_pedido.getFecha(), Formatos.DateFormatSlash)));
            ((TextView) seccion_ultimo_pedido.findViewById(R.id.frg_add_item_ult_ped_q_txt)).setText(String.valueOf(ult_item.getCantidad()));
        }

    }

    public void SelectProduct(Product _product) {
        if (_product != null) {
            productEdit = _product;
            txtCodigo.setError(null);

            if (!flagItemEdit) {
                CargarDatos(
                        _product.getCode(),
                        _product.getName(),
                        0,
                        _product.getStock(),
                        _product
                );
            } else {
                CargarDatos(
                        _product.getCode(),
                        _product.getName(),
                        itemEdit.getCantidad(),
                        _product.getStock(),
                        _product
                );
            }

            //posamos el foco sobre cantidad
            txtCantidad.requestFocus();

        } else {
            //TODO: product not found
            productEdit = null;
            txtDescripcion.setText(null);
            txtCodigo.setError("Producto no existente");
        }

    }

    private boolean ObtenerProductoByCode(String code) {
        try {

            boolean pudoLeer = true;

            List<Product> productos = new ProductDao(getActivity().getContentResolver()).list(ProductTable.COLUMN_CODE + " = '" + code + "' AND removed = 0", null, null);

            if (productos.size() > 0) {
                // en el caso de haber mas de uno, nos quedamos solo con el primero
                Product p = productos.get(0);
                SelectProduct(p);

            } else {
                pudoLeer = false;
            }

            return pudoLeer;

        } catch (Exception ex) {
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
            return false;
        }
    }
}
