package com.pds.ficle.gestprod.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Formato;
import com.pds.common.Logger;
import com.pds.common.Usuario;
import com.pds.common.dao.HistMovStockDao;
import com.pds.common.dao.ProductDao;
import com.pds.common.model.HistMovStock;
import com.pds.common.model.Product;
import com.pds.common.util.Window;
import com.pds.ficle.gestprod.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EditStockNewFragment2.OnEditStockInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EditStockNewFragment2#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditStockNewFragment2 extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "product";
    private static final String ARG_PARAM2 = "user";

    private Product product;
    private Usuario user;

    private OnEditStockInteractionListener mListener;
    private Button btnSgte, btnCancelar, btnBack;
    private TextView txtTitle;
    private LinearLayout container;
    //private EditText txtCantidad, txtObservaciones;
    //private ImageButton btnAjusteIngreso, btnAjusteEgreso;

    public void setOnEditStockInteractionListener(OnEditStockInteractionListener mListener) {
        this.mListener = mListener;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param _product Producto a editar.
     * @return A new instance of fragment EditStockFragment.
     */
    public static EditStockNewFragment2 newInstance(Product _product, Usuario _user) {
        EditStockNewFragment2 fragment = new EditStockNewFragment2();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, _product);
        args.putSerializable(ARG_PARAM2, _user);
        fragment.setArguments(args);
        return fragment;
    }

    public EditStockNewFragment2() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            product = getArguments().getParcelable(ARG_PARAM1);
            user = (Usuario) getArguments().getSerializable(ARG_PARAM2);
        }
    }

    private List<WizardStep> steps;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_new_stock2, container, false);

        Init_Views(view);

        Init_Eventos(view);

        /*if (product != null) {
            //txtCantidad.setText("0");
            txtStockActual.setText(Formato.FormateaDecimal(product.getStock(), Formato.Decimal_Format.CH));
        }*/

        //armamos los pasos del wizard
        WizardStep step1 = new WizardStep(R.string.edit_stock_wiz_tipo_aju, R.layout.edit_stock_wiz_step_tipo, stepTipoChange, View.INVISIBLE);
        WizardStep step2 = new WizardStep(R.string.edit_stock_wiz_cant_sum, R.layout.edit_stock_wiz_step_stock_aj, stepCantidadChange);
        WizardStep step3 = new WizardStep(R.string.edit_stock_wiz_stock_min, R.layout.edit_stock_wiz_step_stock_min, stepStockMinChange);

        //alta
        step1.flows(null, step2);
        step2.flows(step1, step3);
        step3.flows(step2, null);

        steps = new ArrayList<WizardStep>();
        steps.add(step1);
        steps.add(step2);
        steps.add(step3);

        return view;
    }

    private WizardStepChangeListener stepTipoChange = new WizardStepChangeListener() {
        @Override
        public void onStepBack() {
        }

        @Override
        public void onStepShow(final WizardStep _step) {
            //cerramos el teclado
            Window.CloseSoftKeyboard(getActivity());

            container.findViewById(R.id.edit_stock_wiz_step_tipo_btn_actual).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    operacion = "A";
                    execWizStep(_step.nextStep);
                }
            });
            container.findViewById(R.id.edit_stock_wiz_step_tipo_btn_restar).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    operacion = "E";
                    execWizStep(_step.nextStep);
                }
            });
            container.findViewById(R.id.edit_stock_wiz_step_tipo_btn_sumar).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    operacion = "I";
                    execWizStep(_step.nextStep);
                }
            });


        }

        @Override
        public boolean onStepNext() {
            return true;
        }
    };

    private WizardStepChangeListener stepCantidadChange = new WizardStepChangeListener() {
        @Override
        public void onStepBack() {
        }

        @Override
        public void onStepShow(WizardStep _step) {

            if (operacion.equalsIgnoreCase("A")) {
                _step.titleId = R.string.edit_stock_wiz_cant_actual;
                txtTitle.setText(_step.titleId);
                ((TextView) container.findViewById(R.id.frg_edit_stock_lbl_ajust)).setText("Cantidad en Stock");

                container.findViewById(R.id.frg_edit_stock_act_lay).setVisibility(View.GONE);
                container.findViewById(R.id.frg_edit_stock_new_lay).setVisibility(View.GONE);

            } else {

                container.findViewById(R.id.frg_edit_stock_act_lay).setVisibility(View.VISIBLE);
                container.findViewById(R.id.frg_edit_stock_new_lay).setVisibility(View.VISIBLE);

                if (operacion.equalsIgnoreCase("I")) {
                    _step.titleId = R.string.edit_stock_wiz_cant_sum;
                    txtTitle.setText(_step.titleId);
                    ((TextView) container.findViewById(R.id.frg_edit_stock_lbl_ajust)).setText("Cantidad a Sumar");
                } else if (operacion.equalsIgnoreCase("E")) {
                    _step.titleId = R.string.edit_stock_wiz_cant_rest;
                    txtTitle.setText(_step.titleId);
                    ((TextView) container.findViewById(R.id.frg_edit_stock_lbl_ajust)).setText("Cantidad a Restar");
                }

                //actualizamos el nuevo stock
                ((EditText) container.findViewById(R.id.frg_edit_stock_ajust)).addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        String cantidad = s.toString().trim();

                        double nuevoStock = ActualizarResultado(operacion, cantidad);
                        ((TextView) container.findViewById(R.id.frg_edit_stock_result)).setText(Formato.FormateaDecimal(nuevoStock, Formato.Decimal_Format.CH));

                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                //mostarmos el stock actual
                ((TextView) container.findViewById(R.id.frg_edit_stock_actual)).setText(Formato.FormateaDecimal(product.getStock(), Formato.Decimal_Format.CH));

            }

            Window.FocusViewShowSoftKeyboard(getActivity(), container.findViewById(R.id.frg_edit_stock_ajust));

            Window.AddDoneKeyboardAction(((EditText) container.findViewById(R.id.frg_edit_stock_ajust)), _step.doneAction());


        }

        @Override
        public boolean onStepNext() {

            EditText txtCantidad = ((EditText) container.findViewById(R.id.frg_edit_stock_ajust));

            String cantidad = txtCantidad.getText().toString().trim();

            //validamos que sea obligatorio
            txtCantidad.setError(null);

            if (TextUtils.isEmpty(cantidad) || !TextUtils.isDigitsOnly(cantidad)) {
                txtCantidad.setError("Cantidad ingresada no valida");
                txtCantidad.requestFocus();
                return false;
            } else {

                if (operacion.equalsIgnoreCase("A")) {
                    //tomamos el nuevo stock
                    Resultado = Double.parseDouble(cantidad);
                    Cantidad = 0d;
                }

                //actualizamos el stock del producto
                product.setStock(Resultado);

                return true;
            }
        }

        private double ActualizarResultado(String tipoAjuste, String cant) {

            if (!TextUtils.isEmpty(cant)) {
                Cantidad = Double.parseDouble(cant);

                if (tipoAjuste.equals("I"))
                    Resultado = product.getStock() + Cantidad;
                else if (tipoAjuste.equals("E"))
                    Resultado = product.getStock() - Cantidad;
                else
                    Resultado = product.getStock(); //hasta q no selecione el modo, no variamos el resultado
            } else
                Resultado = product.getStock();

            return Resultado;

        }

    };

    private WizardStepChangeListener stepStockMinChange = new WizardStepChangeListener() {
        @Override
        public void onStepBack() {
        }

        @Override
        public void onStepShow(WizardStep _step) {
            //mostramos el stock min que teniamos
            if (product.getMinStock() != 0)
                ((EditText) container.findViewById(R.id.prod_wiz_step_stock_min_txt_stock)).setText(Formato.FormateaDecimal(product.getMinStock(), Formato.Decimal_Format.CH));

            Window.FocusViewShowSoftKeyboard(getActivity(), container.findViewById(R.id.prod_wiz_step_stock_min_txt_stock));

            Window.AddDoneKeyboardAction(((EditText) container.findViewById(R.id.prod_wiz_step_stock_min_txt_stock)), _step.doneAction());

        }

        @Override
        public boolean onStepNext() {
            //tomamos el campo stock minimo ingresado
            EditText txtStockMin = ((EditText) container.findViewById(R.id.prod_wiz_step_stock_min_txt_stock));

            String stockMin = txtStockMin.getText().toString().trim();

            if (TextUtils.isEmpty(stockMin)) {
                product.setMinStock(0);
            } else {
                product.setMinStock(Formato.ParseaDecimal(stockMin, Formato.Decimal_Format.CH));
            }

            //**** grabamos los cambios ****
            boolean saved = new ProductDao(getActivity().getContentResolver()).saveOrUpdate(product);

            if (saved) {

                Logger.RegistrarEvento(getActivity().getContentResolver(), "i", "MODIF. STOCK", "SKU: " + product.getCode(), "Stock: " + String.valueOf(Resultado.intValue()));

                HistMovStock stk = new HistMovStock();
                stk.setFecha(new Date());
                stk.setUsuario(user.getLogin());
                stk.setObservacion("");
                stk.setStock(Resultado.intValue());
                stk.setCantidad(Cantidad.intValue());
                stk.setMonto(0);
                stk.setOperacion(operacion);
                stk.setProductoid(product.getId());

                new HistMovStockDao(getActivity().getContentResolver()).save(stk);

                if (mListener != null) {
                    mListener.onStockChanged(product);
                }

                Cerrar();

            } else {
                Toast.makeText(getActivity(), "No se pudo actualizar el producto", Toast.LENGTH_SHORT).show();
            }

            return true;

        }
    };

    @Override
    public void onStart() {
        super.onStart();

        //ejecutamos el primer paso
        execWizStep(steps.get(0));
    }

    public void execWizStep(WizardStep step) {
        //aqui deberíamos completar el paso

        //title
        txtTitle.setText(step.titleId);

        final WizardStepChangeListener listener = step.stepChangeListener;

        //back
        if (step.previousStep != null) {
            final WizardStep previousStep = step.previousStep;
            btnBack.setVisibility(View.VISIBLE);
            btnBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        listener.onStepBack();
                    execWizStep(previousStep);
                }
            });
        } else
            btnBack.setVisibility(View.INVISIBLE);

        //next
        if (step.nextStep != null) {
            final WizardStep nextStep = step.nextStep;
            btnSgte.setText("SIGUIENTE");
            btnSgte.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        if (listener.onStepNext())
                            execWizStep(nextStep);
                    } else
                        execWizStep(nextStep);
                }
            });
            step.nextButton = btnSgte;
        } else {
            btnSgte.setText("GRABAR");
            btnSgte.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        if (listener.onStepNext()) {

                        }
                }
            });
            step.nextButton = btnSgte;
        }
        btnSgte.setVisibility(step.nextButtonViewMode);


        //inflate view
        container.removeAllViews();

        if (step.viewId != -1) {

            View view = getActivity().getLayoutInflater().inflate(step.viewId, null);
            container.addView(view, 0);

        }

        //ejecutamos el evento
        if (listener != null)
            listener.onStepShow(step);
    }

    private void Init_Views(View view) {
        btnBack = (Button) view.findViewById(R.id.frg_edit_stock_btn_back);
        btnSgte = (Button) view.findViewById(R.id.frg_edit_stock_btn_aceptar);
        btnCancelar = (Button) view.findViewById(R.id.frg_edit_stock_btn_cancelar);
        //txtCantidad = (EditText) view.findViewById(R.id.frg_edit_stock_cantidad);
        //txtStockActual = (TextView) view.findViewById(R.id.frg_edit_stock_actual);
        //txtNuevoStock = (TextView) view.findViewById(R.id.frg_edit_stock_result);

        txtTitle = (TextView) view.findViewById(R.id.frg_edit_stock_lbl_titulo);

        //btnAjusteEgreso = (ImageButton) view.findViewById(R.id.frg_edit_stock_btn_egr);
        //btnAjusteIngreso = (ImageButton) view.findViewById(R.id.frg_edit_stock_btn_ing);
        container = ((LinearLayout) view.findViewById(R.id.frg_edit_stock_step));
    }

    private void Init_Eventos(View view) {
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cerrar();
            }
        });
        /*btnGrabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CerrarItem();
            }
        });*/
        view.findViewById(R.id.img_dummy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cerrar();
            }
        });
        view.findViewById(R.id.frg_edit_stock_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        /*txtCantidad.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ActualizarResultado();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });*/
        /*
        btnAjusteIngreso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActualizarResultado("I");
            }
        });
        btnAjusteEgreso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActualizarResultado("E");
            }
        });
       */
    }

    private Double Resultado;
    private Double Cantidad;
    private String operacion;

    /*
        private void ActualizarResultado(String tipoAjuste){
            operacion = tipoAjuste;
            String s = txtCantidad.getText().toString().trim();
            if (!TextUtils.isEmpty(s)) {
                Cantidad = Double.parseDouble(s);

                if(tipoAjuste.equals("I"))
                    Resultado = product.getStock() + Cantidad;
                else if(tipoAjuste.equals("E"))
                    Resultado = product.getStock() - Cantidad;
                else
                    Resultado = product.getStock(); //hasta q no selecione el modo, no variamos el resultado
            } else
                Resultado = product.getStock();

            txtNuevoStock.setText(Formato.FormateaDecimal(Resultado, Formato.Decimal_Format.CH));
        }

        public void CerrarItem() {

            //clear errors
            txtCantidad.setError(null);

            String cantidad = txtCantidad.getText().toString().trim();

            if(TextUtils.isEmpty(operacion)) {
                txtCantidad.setError("Debe seleccionar un tipo de ajuste: - / +");
            }
            else if(TextUtils.isEmpty(cantidad) || !TextUtils.isDigitsOnly(cantidad)){
                txtCantidad.setError("Cantidad ingresada no valida");
            }
            else {

                //actualizamos el stock del producto y grabamos
                product.setStock(Resultado);

                boolean saved = new ProductDao(getActivity().getContentResolver()).saveOrUpdate(product);

                if(saved) {

                    HistMovStock stk = new HistMovStock();
                    stk.setFecha(new Date());
                    stk.setUsuario(user.getLogin());
                    stk.setObservacion(txtObservaciones.getText().toString());
                    stk.setStock(Resultado.intValue());
                    stk.setCantidad(Cantidad.intValue());
                    stk.setMonto(0);
                    stk.setOperacion(operacion);
                    stk.setProductoid(product.getId());

                    new HistMovStockDao(getActivity().getContentResolver()).save(stk);

                    if (mListener != null) {
                        mListener.onStockChanged(product);
                    }

                    Cerrar();

                }
                else{
                    Toast.makeText(getActivity(), "No se pudo actualizar el producto",Toast.LENGTH_SHORT).show();
                }


            }
        }
    */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnEditStockInteractionListener) activity;

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " debe implementar OnEditStockInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void Cerrar() {
        //cerramos el teclado
        Window.CloseSoftKeyboard(getActivity());

        FragmentManager fm = getActivity().getFragmentManager();

        Fragment fragment = fm.findFragmentById(R.id.product_abm_frg_edit_stock);

        FragmentTransaction ft = fm.beginTransaction();

        ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);

        ft.hide(fragment);

        ft.commit();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnEditStockInteractionListener {
        public void onStockChanged(Product product);
    }

}
