package com.pds.ficle.gestprod.util;

import android.app.Activity;
import android.view.View;

/**
 * Created by Hernan on 23/07/2015.
 */
public abstract class ViewUtils {

    //este metodo define zonas touch, si se presiona sobre el slave view, el foco se traslada al master view
    public static void DefineViewGroup(final Activity activity, final int masterViewId, int slaveViewId){
        activity.findViewById(slaveViewId).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.findViewById(masterViewId).requestFocus();
            }
        });
    }

    //este metodo define zonas touch, si se presiona sobre el slave view, el foco se traslada al master view
    public static void DefineViewGroup(final View container, final int masterViewId, int slaveViewId){
        container.findViewById(slaveViewId).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                container.findViewById(masterViewId).requestFocus();
            }
        });
    }
}
