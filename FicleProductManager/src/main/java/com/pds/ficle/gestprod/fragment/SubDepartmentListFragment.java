package com.pds.ficle.gestprod.fragment;

import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.pds.ficle.gestprod.R;
import com.pds.ficle.gestprod.activity.SubDepartmentActivity;
import com.pds.common.dao.SubDepartmentDao;
import com.pds.common.model.SubDepartment;

import java.text.SimpleDateFormat;
import java.util.List;

public class SubDepartmentListFragment extends ListFragment {
    private final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");

    SubDepartmentArrayAdapter listAdapter;
    List<SubDepartment> list;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        list = getList();
        listAdapter = new SubDepartmentArrayAdapter(getActivity(), list);
        setListAdapter(listAdapter);

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume(){
        super.onResume();

        list.clear();
        list.addAll(getList());
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        v.setBackgroundColor(getResources().getColor(R.color.light_blue));

        SubDepartment selectedSubDepartment = (SubDepartment) getListAdapter().getItem(position);

        Intent intent = new Intent(getActivity(), SubDepartmentActivity.class);
        intent.putExtra("subDepartment", selectedSubDepartment);
        getActivity().startActivity(intent);
        getActivity().overridePendingTransition(0, 0);
    }

    private List<SubDepartment> getList() {
        return new SubDepartmentDao(this.getActivity().getContentResolver()).list();
    }

    public class SubDepartmentArrayAdapter extends ArrayAdapter<String> {
        private final Context context;
        private final List<SubDepartment> subDepartments;

        public SubDepartmentArrayAdapter(Context context, List subDepartments) {
            super(context, R.layout.list_item_sub_department, subDepartments);
            this.context = context;
            this.subDepartments = subDepartments;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View rowView = inflater.inflate(R.layout.list_item_sub_department, parent, false);
            if (position % 2 != 0)
                rowView.setBackgroundColor(Color.LTGRAY);
            else
                rowView.setBackgroundColor(Color.WHITE);

            TextView nameTextView = (TextView) rowView.findViewById(R.id.list_item_sub_department_name);
            nameTextView.setText(subDepartments.get(position).getName());

            TextView departmentTextView = (TextView) rowView.findViewById(R.id.list_item_sub_department_department);
            departmentTextView.setText(subDepartments.get(position).getDepartment().getName());

            TextView descriptionTextView = (TextView) rowView.findViewById(R.id.list_item_sub_department_description);
            descriptionTextView.setText(subDepartments.get(position).getDescription());

            return rowView;
        }
    }
}
