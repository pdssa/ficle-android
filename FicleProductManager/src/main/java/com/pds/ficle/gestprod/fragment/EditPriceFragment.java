package com.pds.ficle.gestprod.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Formato;
import com.pds.common.Logger;
import com.pds.common.Usuario;
import com.pds.common.dao.HistMovStockDao;
import com.pds.common.dao.ProductDao;
import com.pds.common.model.Combo;
import com.pds.common.model.HistMovStock;
import com.pds.common.model.Product;
import com.pds.common.util.Window;
import com.pds.ficle.gestprod.R;

import java.util.Date;
import java.util.concurrent.Callable;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EditPriceFragment.OnEditPriceInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EditPriceFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditPriceFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "product";
    private static final String ARG_PARAM2 = "formato";

    private Product product;

    private OnEditPriceInteractionListener mListener;
    private Button btnGrabar, btnCancelar, btnNext, btnBack;
    private TextView txtPrecioActual, txtTitulo;
    private EditText txtNuevoPrecio;
    private Formato.Decimal_Format DECIMAL_FORMAT;

    public void setOnEditPriceInteractionListener(OnEditPriceInteractionListener mListener) {
        this.mListener = mListener;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param _product Producto a editar.
     * @return A new instance of fragment EditPriceFragment.
     */
    public static EditPriceFragment newInstance(Product _product, Formato.Decimal_Format _decimal) {
        EditPriceFragment fragment = new EditPriceFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, _product);
        args.putSerializable(ARG_PARAM2, _decimal);
        fragment.setArguments(args);
        return fragment;
    }

    public EditPriceFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            product = getArguments().getParcelable(ARG_PARAM1);
            DECIMAL_FORMAT = (Formato.Decimal_Format) getArguments().getSerializable(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_price, container, false);

        Init_Views(view);

        Init_Eventos(view);


        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        Step1();

    }

    private void Init_Views(View view) {
        btnGrabar = (Button) view.findViewById(R.id.frg_edit_price_btn_aceptar);
        btnCancelar = (Button) view.findViewById(R.id.frg_edit_price_btn_cancelar);
        btnBack = (Button) view.findViewById(R.id.frg_edit_price_btn_back);
        btnNext = (Button) view.findViewById(R.id.frg_edit_price_btn_next);

        txtNuevoPrecio = (EditText) view.findViewById(R.id.frg_edit_price_nuevo);

        txtPrecioActual = (TextView) view.findViewById(R.id.frg_edit_price_actual);

        txtTitulo = (TextView) view.findViewById(R.id.frg_edit_price_lbl_title);
    }

    private void Init_Eventos(View view) {
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cerrar();
            }
        });
        btnGrabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CerrarItem();
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Step2();
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Step1();
            }
        });

        view.findViewById(R.id.img_dummy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cerrar();
            }
        });
        view.findViewById(R.id.frg_edit_price_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
    }

    double newSalePrice = 0;
    double newPurchasePrice = 0;

    private void Step1() {
        btnBack.setVisibility(View.GONE);
        btnGrabar.setVisibility(View.GONE);
        btnNext.setVisibility(View.VISIBLE);

        txtTitulo.setText(getString(R.string.edit_price_precio_vta));

        txtNuevoPrecio.setText("");
        txtNuevoPrecio.setError(null);

        txtPrecioActual.setText(Formato.FormateaDecimal(product.getSalePrice(), DECIMAL_FORMAT, Formato.getCurrencySymbol(DECIMAL_FORMAT)));

        Window.FocusViewShowSoftKeyboard(getActivity(), txtNuevoPrecio);

        Window.AddDoneKeyboardAction(txtNuevoPrecio, new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                btnNext.performClick();
                return null;
            }
        });

    }

    private void Step2() {

        //obtenemos el precio de venta ingresado

        //validamos que sea obligatorio
        txtNuevoPrecio.setError(null);

        String salePrice = txtNuevoPrecio.getText().toString().trim();

        if (TextUtils.isEmpty(salePrice)) {
            txtNuevoPrecio.setError(getString(R.string.error_field_required));
            txtNuevoPrecio.requestFocus();
        } else {

            newSalePrice = Formato.ParseaDecimal(salePrice, DECIMAL_FORMAT, true);

            btnBack.setVisibility(View.VISIBLE);
            btnGrabar.setVisibility(View.VISIBLE);
            btnNext.setVisibility(View.GONE);

            txtTitulo.setText(getString(R.string.edit_price_precio_cpa));

            txtNuevoPrecio.setText("");

            txtPrecioActual.setText(Formato.FormateaDecimal(product.getPurchasePrice(), DECIMAL_FORMAT, Formato.getCurrencySymbol(DECIMAL_FORMAT)));

            Window.FocusViewShowSoftKeyboard(getActivity(), txtNuevoPrecio);

            Window.AddDoneKeyboardAction(txtNuevoPrecio, new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    btnGrabar.performClick();
                    return null;
                }
            });

        }
    }

    public void CerrarItem() {

        //validamos que sea obligatorio
        //txtNuevoPrecio.setError(null);

        String purchasePrice = txtNuevoPrecio.getText().toString().trim();

        if (TextUtils.isEmpty(purchasePrice)) {
            //txtNuevoPrecio.setError(getString(R.string.error_field_required));
            //txtNuevoPrecio.requestFocus();

            //sino ingresan mantenemos el precio de compra
            newPurchasePrice = product.getPurchasePrice();
        }

        newPurchasePrice = Formato.ParseaDecimal(purchasePrice, DECIMAL_FORMAT, true);

        btnBack.setVisibility(View.VISIBLE);
        btnGrabar.setVisibility(View.VISIBLE);
        btnNext.setVisibility(View.GONE);

        txtTitulo.setText(getString(R.string.edit_price_precio_cpa));

        txtNuevoPrecio.setText("");

        //actualizamos los precios del producto y grabamos
        product.setSalePrice(newSalePrice);
        product.setPurchasePrice(newPurchasePrice);

        boolean saved = new ProductDao(getActivity().getContentResolver()).saveOrUpdate(product);

        if (saved) {

            Logger.RegistrarEvento(getActivity().getContentResolver(), "i", "MODIF. PRECIO",
                    "SKU: " + product.getCode(),
                    "Prc.Vta.: " + Formato.FormateaDecimal(product.getSalePrice(), DECIMAL_FORMAT, Formato.getCurrencySymbol(DECIMAL_FORMAT))
            );

            if (mListener != null) {
                mListener.onPriceChanged(product);
            }

            Cerrar();

        } else {
            Toast.makeText(getActivity(), "No se pudo actualizar el producto", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnEditPriceInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " debe implementar OnEditStockInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void Cerrar() {
        //cerramos el teclado
        Window.CloseSoftKeyboard(getActivity());

        FragmentManager fm = getActivity().getFragmentManager();

        Fragment fragment = fm.findFragmentById(R.id.product_abm_frg_edit_stock);

        FragmentTransaction ft = fm.beginTransaction();

        ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);

        ft.hide(fragment);

        ft.commit();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnEditPriceInteractionListener {
        public void onPriceChanged(Product product);
    }

}
