package com.pds.ficle.gestprod.util;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.EAN13Writer;
import com.google.zxing.oned.EAN8Writer;
import com.pds.common.dao.ProductDao;
import com.pds.common.db.ProductTable;
import com.pds.common.model.Product;

import java.util.Hashtable;
import java.util.List;

import android_serialport_api.Printer;

/**
 * Created by Hernan on 06/11/2015.
 */
public abstract class SkuUtils {

    public static String FormatSkuLabel(String input) {
        //"1  1 1 1 1 1 1  1 1 1 1 1 6"

        int len = input.length();

        if (len > 8) {
            //PADDING EAN13
            String a = ("0000000000000" + input);
            String _a = a.substring(a.length() - 13);

            String _first = _a.substring(1, 7);//primeros 6
            _first = _first.substring(0, 1) + " " + _first.substring(1, 2) + " " + _first.substring(2, 3) + " " + _first.substring(3, 4) + " " + _first.substring(4, 5) + " " + _first.substring(5, 6);
            String _second = _a.substring(7); //ultimos 6
            _second = _second.substring(0, 1) + " " + _second.substring(1, 2) + " " + _second.substring(2, 3) + " " + _second.substring(3, 4) + " " + _second.substring(4, 5) + " " + _second.substring(5, 6);
            String _end = _a.substring(0, 1) + "  " + _first + "  " + _second;

            return _end;
        } else {
            //PADDING EAN8
            String a = ("00000000" + input);
            String _a = a.substring(a.length() - 8);

            String _first = _a.substring(0, 4);//primeros 4
            _first = _first.substring(0, 1) + " " + _first.substring(1, 2) + " " + _first.substring(2, 3) + " " + _first.substring(3, 4);
            String _second = _a.substring(4); //ultimos 4
            _second = _second.substring(0, 1) + " " + _second.substring(1, 2) + " " + _second.substring(2, 3) + " " + _second.substring(3, 4);
            String _end = _first + "  " + _second;

            return _end;
        }

    }


    public static void ImprimirEtiqueta(Context context, String PRINTER_MODEL, final String sku, final String nombre, final String precio, final boolean incluirPrecio, final String precioSecund) {

        if (PRINTER_MODEL != null) {
            if (PRINTER_MODEL.equalsIgnoreCase("POWA")) {
                //TODO: bug with asynctask and powa
                ImprimirEtiquetaTask.printLabel(context, PRINTER_MODEL, sku, nombre, precio, incluirPrecio, precioSecund);
            } else {
                new ImprimirEtiquetaTask(context, PRINTER_MODEL, sku, nombre, precio, incluirPrecio, precioSecund).execute();
            }
            ///-----------------------------------------------------------------------------------
        }
    }


    public static void ImprimirEtiqueta(final Context context, final String PRINTER_MODEL, final String sku, final String nombre, final String precio) {

        new AlertDialog.Builder(context)
                .setIcon(android.R.drawable.ic_menu_help)
                .setTitle("IMPRIMIR CODIGO DE BARRA")
                .setMessage("Incluir el precio de venta en la impresión?")
                .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ImprimirEtiqueta(context, PRINTER_MODEL, sku, nombre, precio, true, "");
                    }

                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ImprimirEtiqueta(context, PRINTER_MODEL, sku, nombre, precio, false, "");
                    }

                })
                .show();

    }

    public static String GenerateSku(Product product, ProductDao productDao) {

        //tomamos el cod de categoria, y armamos un SKU correlativo

        String catCode = product.getSubDepartment().getCodigo() + "000000000";

        catCode = catCode.substring(0, 9); //largo 9 + 3 + 1

        if (catCode.startsWith("0"))
            catCode = "9" + catCode.substring(1); //reemplazamos el primer 0 por un 9, sino, los scanners no toman el cero inicial

        String candidate;

        //podemos formar hasta 1000 productos...xxxxxxxxx000d
        for (int i = 0; i < 1000; i++) {

            //vamos sumando 1 al final, calcular checsum, y ver si existe...
            candidate = String.format("%s%03d", catCode, i);

            candidate = String.format("%s%d", candidate, GetCheckSumDigit(candidate));

            if (!SkuEnUso(productDao, candidate)) {
                return candidate;
            }
        }

        //no pudimos encontrar un SKU disponible para la categoria...
        return "";

    }

    public static String GenerateSKUv2(String codComercio, Product product, ProductDao productDao) {

        //999xxxxxxaaac, donde xxxxxx es el id de comercio, aaa es un correlativo desde 000 a 999, c es el CRC del EAN13.

        String padCodComercio = "000000" + codComercio;
        String preCode = "999" + padCodComercio.substring(padCodComercio.length() - 6) ;//999xxxxxx

        String candidate;

        //podemos formar hasta 1000 productos...999xxxxxxaaac
        for (int i = 0; i < 1000; i++) {

            //vamos sumando 1 al final, calcular checsum, y ver si existe...
            candidate = String.format("%s%03d", preCode, i);

            candidate = String.format("%s%d", candidate, GetCheckSumDigit(candidate));

            if (!SkuEnUso(productDao, candidate)) {
                return candidate;
            }
        }

        //no pudimos encontrar un SKU disponible...
        return "";
    }

    private static boolean SkuEnUso(ProductDao dao, String code) {

        List<Product> check = dao.list(ProductTable.COLUMN_CODE + "='" + code + "' and removed = 0", null, null);

        return check.size() != 0;
    }

    private static int GetCheckSumDigit(String ean13) {
        int val = 0;
        for (int i = 0; i < ean13.length(); i++) {
            val += ((int) Integer.parseInt(ean13.charAt(i) + "")) * ((i % 2 == 0) ? 1 : 3);
        }

        int checksum_digit = 10 - (val % 10);
        if (checksum_digit == 10) checksum_digit = 0;

        return checksum_digit;
    }

}
