/*
package com.pds.ficle.gestprod.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Formato;
import com.pds.common.Logger;
import com.pds.common.activity.Actividad;
import com.pds.common.activity.TimerActivity;
import com.pds.common.dao.ComboDao;
import com.pds.common.dao.ComboItemDao;
import com.pds.common.dao.ProductDao;
import com.pds.common.hardware.ScannerPOWA;
import com.pds.common.model.Combo;
import com.pds.common.model.ComboItem;
import com.pds.common.model.Product;
import com.pds.common.util.Window;
import com.pds.ficle.gestprod.R;
import com.pds.ficle.gestprod.fragment.WizardStep;
import com.pds.ficle.gestprod.fragment.WizardStepChangeListener;
import com.pds.ficle.gestprod.util.SkuUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ComboWizardActivity extends Actividad {

    private List<WizardStep> steps;
    private Combo combo;
    private List<ComboItem> _items;
    private WizardStep currentStep;
    private Formato.Decimal_Format FORMATO_DECIMAL;
    private Config config;
    private Button btnCancelar;

    private WizardStep stepProdsSecundAdd;
    private WizardStep stepProdsPpalAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_combo_wizard);

        //cancelar
        btnCancelar = (Button) findViewById(R.id.combo_wiz_btn_cancel);
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ocultamos el teclado al salir
                Window.CloseSoftKeyboard(ComboWizardActivity.this);

                volver();//salimos
            }
        });

        //formato
        config = new Config(this);
        FORMATO_DECIMAL = Formato.getDecimal_Format(config.PAIS);

        //armamos los pasos del wizard
        WizardStep step1 = new WizardStep(R.string.combo_wiz_inicial, R.layout.combo_wiz_step_inicial, stepInicialChange);
        WizardStep step2 = new WizardStep(R.string.combo_wiz_nombre, R.layout.combo_wiz_step_nombre, stepNombreChange);
        WizardStep step3 = new WizardStep(R.string.combo_wiz_descripcion, R.layout.combo_wiz_step_descripcion, stepDescripcionChange);
        stepProdsPpalAdd = new WizardStep(R.string.combo_wiz_prod_ppal, R.layout.combo_wiz_step_prod_ppal, stepProdPpal);
        WizardStep step5 = new WizardStep(R.string.combo_wiz_prod_ppal_cant, R.layout.combo_wiz_step_cantidad_ppal, stepProdPpalCantidadChange);

        WizardStep step6 = new WizardStep(R.string.combo_wiz_prod_secund, R.layout.combo_wiz_step_prodsecunds, stepProdSecund, View.GONE);

        stepProdsSecundAdd = new WizardStep(R.string.combo_wiz_prod_secund_add, R.layout.combo_wiz_step_prod_secund_add, stepProdSecundAdd);
        WizardStep step62 = new WizardStep(R.string.combo_wiz_prod_secund_cant, R.layout.combo_wiz_step_cantidad_secund, stepProdSecundCantidadChange);

        WizardStep step7 = new WizardStep(R.string.combo_wiz_prod_precio, R.layout.combo_wiz_step_precio, stepPrecioChange);
        WizardStep end = new WizardStep(R.string.combo_wiz_precio_valida, R.layout.combo_wiz_step_validar, stepEndChange);


        //alta
        step1.flows(null, step2);
        step2.flows(step1, step3);
        step3.flows(step2, stepProdsPpalAdd);
        stepProdsPpalAdd.flows(step3, step5);
        step5.flows(stepProdsPpalAdd, step6);
        step6.flows(step5, step7);

        stepProdsSecundAdd.flows(step6, step62);
        step62.flows(stepProdsSecundAdd, step6);

        step7.flows(step6, end);
        end.flows(step7, null);

        steps = new ArrayList<WizardStep>();
        steps.add(step1);
        steps.add(step2);
        steps.add(step3);
        steps.add(stepProdsPpalAdd);
        steps.add(step5);
        steps.add(step6);
        steps.add(stepProdsSecundAdd);
        steps.add(step62);
        steps.add(step7);
        steps.add(end);

        //mostramos el primero
        execWizStep(step1);

    }

    private void volver(){
        super.onBackPressed();//asi llamamos al super...
    }

    private ScannerPOWA _scanner;

    @Override
    protected void onResume() {
        super.onResume();

        Log.d("Test", "Wiz-Resume");


        try {
            //si tenemos una printer POWA, entonces iniciamos el scanner POWA S10
            if (config.PRINTER_MODEL != null && config.PRINTER_MODEL.equals("POWA")) {
                _scanner = new ScannerPOWA(this);
                _scanner._scannerCallback = new ScannerPOWA.ScannerCallback() {
                    @Override
                    public void onScannerRead(String text) {
                        //si está activado el step SKU PPAL
                        if(STEP_SKU == 1 || STEP_SKU == 2)
                            ((EditText) findViewById(R.id.combo_wiz_step_cod_barra_txt_code)).setText(text);
                    }
                };
            }
        } catch (Exception ex) {
            Toast.makeText(ComboWizardActivity.this, ex.getMessage(),Toast.LENGTH_SHORT ).show();
        }
    }

    @Override
    protected void onPause() {
        if (_scanner != null){
            _scanner.end();
            _scanner = null;
        }

        Log.d("Test", "Wiz-Pause");

        super.onPause();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();

        if (currentStep != null) {
            if (currentStep.previousStep != null)
                execWizStep(currentStep.previousStep);
            else
                super.onBackPressed();//btnCancelar.performClick();
        }
    }

    private void updateComboCreation(int idStep, TextView txtView) {
        String _text = "";

        switch (idStep) {
            case 1: {
                //NOMBRE
                _text += String.format("Nombre: %s<br/>", combo.getNombre());
            }
            break;
            case 2: {
                //DESCRIPCION
                String _comboTxt = String.format("Nombre corto: %s", combo.getDescripcion());

                _text += String.format("Nombre: %s<br/>%s", combo.getNombre(), _comboTxt);
            }
            break;
            case 3: {
                //PROD PPAL
                //String _comboTxt = String.format("%s", combo.getProducto_ppal_code());
                String _comboTxt = String.format("Producto principal:<br/>%s", productoPpal.getName());

                _text += String.format("Nombre: %s<br/>Nombre corto: %s<br/>%s", combo.getNombre(), combo.getDescripcion(), _comboTxt);

            }
            break;
            case 4: {
                //CANTIDAD PROD PPAL
                //String _comboTxt = String.format("%s", combo.getCantidad());
                String _comboTxt = String.format("Producto principal:<br/>%s x %s", Formato.FormateaDecimal(combo.getCantidad(), Formato.Decimal_Format.US), productoPpal.getName());

                _text += String.format("Nombre: %s<br/>Nombre corto: %s<br/>%s", combo.getNombre(), combo.getDescripcion(), _comboTxt);
            }
            break;
            case 61: {
                //PRODUCTO SECUNDARIO
                _text = String.format("Producto secundario:<br/>%s", productoSecundTemp.getName());
            }
            break;
        }


        txtView.setText(Html.fromHtml(_text));
    }

    private String COMBO_PREFIX = "COMBO";

    private WizardStepChangeListener stepInicialChange = new WizardStepChangeListener() {
        @Override
        public void onStepBack() {
            //no hay paso anterior...
        }

        @Override
        public void onStepShow(WizardStep _step) {
            //ocultamos teclado
            Window.CloseSoftKeyboard(ComboWizardActivity.this);
        }

        @Override
        public boolean onStepNext() {
            //solo avanzamos
            return true;
        }
    };

    private WizardStepChangeListener stepNombreChange = new WizardStepChangeListener() {
        @Override
        public void onStepBack() {
        }

        @Override
        public void onStepShow(WizardStep _step) {
            //mostramos el nombre que teniamos
            if (combo != null)
                ((EditText) findViewById(R.id.combo_wiz_step_nombre_txt_nombre)).setText(combo.getNombre());
            else
                combo = new Combo();//instanciamos un objeto combo

            Window.FocusViewShowSoftKeyboard(ComboWizardActivity.this, findViewById(R.id.combo_wiz_step_nombre_txt_nombre));

            Window.AddDoneKeyboardAction(((EditText) findViewById(R.id.combo_wiz_step_nombre_txt_nombre)), _step.doneAction());
        }

        @Override
        public boolean onStepNext() {
            //tomamos el campo nombre ingresado

            EditText txtNombre = ((EditText) findViewById(R.id.combo_wiz_step_nombre_txt_nombre));

            String nombre = txtNombre.getText().toString().trim();

            //validamos que sea obligatorio
            txtNombre.setError(null);

            if (TextUtils.isEmpty(nombre)) {
                txtNombre.setError(getString(R.string.error_field_required));
                txtNombre.requestFocus();
                return false;
            } else {
                //agregamos el prefijo "COMBO"
                if (!nombre.startsWith(COMBO_PREFIX)) {
                    nombre = COMBO_PREFIX + " " + nombre;
                }

                combo.setNombre(nombre);
                return true;
            }
        }
    };

    private WizardStepChangeListener stepDescripcionChange = new WizardStepChangeListener() {
        @Override
        public void onStepBack() {
        }

        @Override
        public void onStepShow(WizardStep _step) {
            //mostramos el nombre que teniamos
            if (combo != null)
                ((EditText) findViewById(R.id.combo_wiz_step_descripcion_txt_descripcion)).setText(combo.getDescripcion());

            if (TextUtils.isEmpty(combo.getDescripcion())) {
                //si no tengo nombre corto, y el nombre ingresado, es menor a 15, entonces lo usamos como nombre corto
                if (combo.getNombre().length() <= 14)
                    ((EditText) findViewById(R.id.combo_wiz_step_descripcion_txt_descripcion)).setText(combo.getNombre());
            }

            updateComboCreation(1, (TextView) findViewById(R.id.combo_wiz_step_descripcion_lbl_combotemp));

            Window.FocusViewShowSoftKeyboard(ComboWizardActivity.this, findViewById(R.id.combo_wiz_step_descripcion_txt_descripcion));

            Window.AddDoneKeyboardAction(((EditText) findViewById(R.id.combo_wiz_step_descripcion_txt_descripcion)), _step.doneAction());

        }

        @Override
        public boolean onStepNext() {
            //tomamos el campo nombre ingresado

            EditText txtDescripcion = ((EditText) findViewById(R.id.combo_wiz_step_descripcion_txt_descripcion));

            String descripcion = txtDescripcion.getText().toString().trim();

            //validamos que sea obligatorio
            txtDescripcion.setError(null);

            if (TextUtils.isEmpty(descripcion)) {
                txtDescripcion.setError(getString(R.string.error_field_required));
                txtDescripcion.requestFocus();
                return false;
            } else {
                combo.setDescripcion(descripcion);
                return true;
            }

        }
    };

    private WizardStepChangeListener stepPrecioChange = new WizardStepChangeListener() {


        class PrecioWatcher implements TextWatcher {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s)) {
                    double precio = Formato.ParseaDecimal(s.toString(), FORMATO_DECIMAL, true);

                    ActualizarPrecioReferenciaCombo(combo, precio, _items, productoPpal.getSalePrice());//actualizamos el precio de referencia al cambiar la cantidad
                }
                //mostramos el precio por gr / ml
                //((TextView) findViewById(R.id.prod_wiz_step_precio_lbl_precio_decena)).setText(String.format("%s: %s", label, Formato.FormateaDecimal(precio_por_decena, FORMATO_DECIMAL, Formato.getCurrencySymbol(FORMATO_DECIMAL))));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        }

        private PrecioWatcher precioChanged = new PrecioWatcher();

        private void asignarWatcher(boolean asignar) {
            ((EditText) findViewById(R.id.combo_wiz_step_precio_txt_precio)).removeTextChangedListener(precioChanged);

            if (asignar) {
                ((EditText) findViewById(R.id.combo_wiz_step_precio_txt_precio)).addTextChangedListener(precioChanged);
            }
        }

        @Override
        public void onStepBack() {
        }

        @Override
        public void onStepShow(WizardStep _step) {
            asignarWatcher(true);

            //mostramos el precio de venta que teniamos
            if (combo != null && combo.getPrecio() != 0)
                ((EditText) findViewById(R.id.combo_wiz_step_precio_txt_precio)).setText(Formato.FormateaDecimal(combo.getPrecio(), FORMATO_DECIMAL));

            //actualizamos lo que mostramos como referencia
            ActualizarPrecioReferenciaCombo(combo, combo.getPrecio(), _items, productoPpal.getSalePrice());

            Window.FocusViewShowSoftKeyboard(ComboWizardActivity.this, findViewById(R.id.combo_wiz_step_precio_txt_precio));

            Window.AddDoneKeyboardAction(((EditText) findViewById(R.id.combo_wiz_step_precio_txt_precio)), _step.doneAction());

            updateComboCreation(4, (TextView) findViewById(R.id.combo_wiz_step_precio_lbl_combotemp));
        }

        @Override
        public boolean onStepNext() {

            //tomamos el campo precio venta ingresado

            EditText txtPrecioVta = ((EditText) findViewById(R.id.combo_wiz_step_precio_txt_precio));

            //validamos que sea obligatorio
            txtPrecioVta.setError(null);

            String salePrice = txtPrecioVta.getText().toString().trim();

            if (TextUtils.isEmpty(salePrice)) {
                txtPrecioVta.setError(getString(R.string.error_field_required));
                txtPrecioVta.requestFocus();

                return false;
            } else {
                double precio = Formato.ParseaDecimal(salePrice, FORMATO_DECIMAL, true);

                if (precio <= 0) {
                    txtPrecioVta.setError(getString(R.string.error_invalid_value));
                    txtPrecioVta.requestFocus();
                    return false;
                } else {
                    combo.setPrecio(precio);
                    return true;
                }
            }
        }

        private void ActualizarPrecioReferenciaCombo(Combo combo, double precioCombo, List<ComboItem> items, double precioProductoPpal) {
            try {
                double totalItems = combo.getCantidad() * precioProductoPpal;//costo del producto ppal

                if (items != null) {
                    for (ComboItem item : items) {//costo de productos secundarios
                        totalItems += (item.getCantidad() * item.getProducto_precio_vta());
                    }
                }

                double diferencia = totalItems != 0 ? ((precioCombo / totalItems) - 1) * 100 : 100;

                //redondeamos en dos decimales:
                diferencia = Math.round(diferencia * 100);
                diferencia = diferencia / 100;

                String s_diferencia = Formato.FormateaDecimal(diferencia, FORMATO_DECIMAL) + "%";

                String texto = String.format("Precio de lista total: %s<br/>", Formato.FormateaDecimal(totalItems, FORMATO_DECIMAL, Formato.getCurrencySymbol(FORMATO_DECIMAL)));

                //solo mostramos la diferencia, si hay algún precio asignado por el usuario
                if(precioCombo != 0)
                    texto += String.format("<font color='%s'>Diferencia: %s</font>", diferencia >= 0 ? "green" : "red", s_diferencia);

                ((TextView) findViewById(R.id.combo_wiz_step_precio_lbl_referencia)).setText(Html.fromHtml(texto));
                //lblPrecioReferencia.setVisibility(View.VISIBLE);

            } catch (Exception ex) {
                Toast.makeText(ComboWizardActivity.this, "Error al calcular precio de lista total: " + ex.toString(), Toast.LENGTH_SHORT).show();
            }
        }
    };

    private int STEP_SKU = 0;
    private Product productoPpal;
    private Product productoSecundTemp;

    private static final int REQ_BUSQ_PROD_PPAL = 1;
    private static final int REQ_BUSQ_PROD_SECD = 2;
    private void IniciarBusquedaProductoManual(int requestcode) {
        Intent intent = new Intent(ComboWizardActivity.this, ProductSearchActivity.class);
        intent.putExtra("TypeResultSearch", ProductSearchActivity.TypeResultSearch.SEARCH_PRODUCT_BACK);
        startActivityForResult(intent, requestcode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_BUSQ_PROD_PPAL: {
                if (resultCode == Activity.RESULT_OK) {
                    Object result = data.getParcelableExtra("result");

                    if(result != null){
                        productoPpal = (Product) result;

                        ((EditText) findViewById(R.id.combo_wiz_step_cod_barra_txt_code)).setText(productoPpal.getCode());

                        stepProdsPpalAdd.nextButton.performClick();
                    }
                }
                */
/*if (resultCode == Activity.RESULT_CANCELED) {

                    //Write your code if there's no result
                    //SelectProvider(null);
                }*//*

            }
            break;
            case REQ_BUSQ_PROD_SECD: {
                if (resultCode == Activity.RESULT_OK) {
                    Object result = data.getParcelableExtra("result");

                    if(result != null){
                        productoSecundTemp = (Product) result;

                        ((EditText) findViewById(R.id.combo_wiz_step_cod_barra_txt_code)).setText(productoSecundTemp.getCode());

                        stepProdsSecundAdd.nextButton.performClick();
                    }
                }
                */
/*if (resultCode == Activity.RESULT_CANCELED) {

                    //Write your code if there's no result
                    //SelectProvider(null);
                }*//*

            }
            break;
        }
    }

    private WizardStepChangeListener stepProdPpal = new WizardStepChangeListener() {
        private void skuStep(boolean t) {
            STEP_SKU = (t ? 1 : 0);
        }

        @Override
        public void onStepBack() {
            skuStep(false);
        }

        @Override
        public void onStepShow(final WizardStep _step) {
            skuStep(true);

            //mostramos el codigo que teniamos
            if (productoPpal != null)
                ((EditText) findViewById(R.id.combo_wiz_step_cod_barra_txt_code)).setText(productoPpal.getCode());

            updateComboCreation(2, (TextView) findViewById(R.id.combo_wiz_step_cod_barra_lbl_combotemp));

            findViewById(R.id.combo_wiz_step_cod_barra_btn_manual).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    IniciarBusquedaProductoManual(REQ_BUSQ_PROD_PPAL);
                }
            });

            Window.FocusViewShowSoftKeyboard(ComboWizardActivity.this, findViewById(R.id.combo_wiz_step_cod_barra_txt_code));

            Window.AddDoneKeyboardAction(((EditText) findViewById(R.id.combo_wiz_step_cod_barra_txt_code)), _step.doneAction());
        }

        @Override
        public boolean onStepNext() {
            //creamos el producto y tomamos el campo codigo

            EditText txtCode = ((EditText) findViewById(R.id.combo_wiz_step_cod_barra_txt_code));

            //validamos que sea obligatorio
            txtCode.setError(null);

            String code = txtCode.getText().toString().trim();

            if (TextUtils.isEmpty(code)) {
                txtCode.setError(getString(R.string.error_field_required));
                txtCode.requestFocus();
                return false;
            } else {

                List<Product> ppal = new ProductDao(getContentResolver()).getByCode(code);

                if (ppal != null && ppal.size() > 0) {

                    productoPpal = ppal.get(0);

                    combo.setProducto_ppal_code(productoPpal.getCode());
                    combo.setProducto_ppal_id(productoPpal.getId());

                    skuStep(false);
                    return true;
                } else {
                    txtCode.setError("Producto desconocido");
                    txtCode.requestFocus();
                    return false;
                }

            }

        }
    };

    private WizardStepChangeListener stepProdPpalCantidadChange = new WizardStepChangeListener() {
        @Override
        public void onStepBack() {
        }

        @Override
        public void onStepShow(WizardStep _step) {
            //mostramos la cantidad que teniamos
            if (combo != null && combo.getCantidad() != 0) {
                ((EditText) findViewById(R.id.combo_wiz_step_cantidad_txt_cantidad)).setText(Formato.FormateaDecimal(combo.getCantidad(), Formato.Decimal_Format.US));
            }

            //((TextView) findViewById(R.id.prod_wiz_step_contenido_lbl_contenido)).setText(String.format("Ingrese cantidad de %s:", product.getUnit().toUpperCase()));

            updateComboCreation(3, (TextView) findViewById(R.id.combo_wiz_step_cantidad_lbl_combotemp));

            Window.FocusViewShowSoftKeyboard(ComboWizardActivity.this, findViewById(R.id.combo_wiz_step_cantidad_txt_cantidad));

            Window.AddDoneKeyboardAction(((EditText) findViewById(R.id.combo_wiz_step_cantidad_txt_cantidad)), _step.doneAction());

        }

        @Override
        public boolean onStepNext() {

            //tomamos el campo cantidad ingresado
            EditText txtCantidad = ((EditText) findViewById(R.id.combo_wiz_step_cantidad_txt_cantidad));

            //validamos que sea obligatorio
            txtCantidad.setError(null);

            String cantidad = txtCantidad.getText().toString().trim();

            if (TextUtils.isEmpty(cantidad)) {
                txtCantidad.setError(getString(R.string.error_field_required));
                txtCantidad.requestFocus();
                return false;
            } else {
                double cant = Formato.ParseaDecimal(cantidad, Formato.Decimal_Format.US);

                if (cant <= 0) {
                    txtCantidad.setError(getString(R.string.error_invalid_value));
                    txtCantidad.requestFocus();
                    return false;
                } else {
                    combo.setCantidad(cant);
                    return true;
                }
            }
        }

        */
/*private void assignUnit(String unit) {
            product.setUnit(unit);

            updateLabelContenido();
        }*//*


        */
/*private void updateLabelContenido() {

            //unidad
            String contenido = ((EditText) findViewById(R.id.prod_wiz_step_contenido_txt_contenido)).getText().toString().trim();
            String lbl = String.format("%s%s", contenido, product.getUnit());

            ((TextView) findViewById(R.id.prod_wiz_step_contenido_lbl_contenido)).setText(lbl);
        }*//*



    };

    private WizardStepChangeListener stepProdSecund = new WizardStepChangeListener() {
        @Override
        public void onStepBack() {
        }

        @Override
        public void onStepShow(final WizardStep _step) {
            //ocultamos teclado
            Window.CloseSoftKeyboard(ComboWizardActivity.this);

            findViewById(R.id.combo_wiz_step_prodsecunds_si).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    execWizStep(stepProdsSecundAdd);
                }
            });

            findViewById(R.id.combo_wiz_step_prodsecunds_no).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    execWizStep(_step.nextStep);//saltamos al sgte paso, solicitar PRECIO COMBO
                }
            });

            //mostramos info del producto principal
            ((TextView) findViewById(R.id.combo_wiz_step_prodsecunds_ppal_cod)).setText(TextUtils.isEmpty(productoPpal.getCode()) ? "S/C" : productoPpal.getCode());
            ((TextView) findViewById(R.id.combo_wiz_step_prodsecunds_ppal_desc)).setText(productoPpal.getName());
            ((TextView) findViewById(R.id.combo_wiz_step_prodsecunds_ppal_cant)).setText(Integer.toString((int) combo.getCantidad()));

            //actualizamos los items
            if (_items != null) {
                ListView lstItems = (ListView) findViewById(R.id.combo_wiz_step_prodsecunds_lst_items);
                lstItems.setEmptyView(findViewById(android.R.id.empty));
                ItemsArrayAdapter listAdapter = new ItemsArrayAdapter(ComboWizardActivity.this, _items);
                lstItems.setAdapter(listAdapter);

                if (_items.size() > 0)
                    ((TextView) findViewById(R.id.combo_wiz_step_prodsecunds_lbl_msj)).setText(getString(R.string.combo_wiz_prod_secund_add_lbl_2));
                else
                    ((TextView) findViewById(R.id.combo_wiz_step_prodsecunds_lbl_msj)).setText(getString(R.string.combo_wiz_prod_secund_add_lbl_1));
            } else
                ((TextView) findViewById(R.id.combo_wiz_step_prodsecunds_lbl_msj)).setText(getString(R.string.combo_wiz_prod_secund_add_lbl_1));
        }

        @Override
        public boolean onStepNext() {
            //solo avanzamos
            return true;
        }
    };

    private WizardStepChangeListener stepProdSecundAdd = new WizardStepChangeListener() {
        private void skuStep(boolean t) {
            STEP_SKU = (t ? 2 : 0);
        }

        @Override
        public void onStepBack() {
            productoSecundTemp = null;

            skuStep(false);
        }

        @Override
        public void onStepShow(final WizardStep _step) {
            productoSecundTemp = null;

            skuStep(true);

            findViewById(R.id.combo_wiz_step_cod_barra_btn_manual).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    IniciarBusquedaProductoManual(REQ_BUSQ_PROD_SECD);
                }
            });

            //updateComboCreation(2, (TextView) findViewById(R.id.combo_wiz_step_cod_barra_lbl_combotemp));

            Window.FocusViewShowSoftKeyboard(ComboWizardActivity.this, findViewById(R.id.combo_wiz_step_cod_barra_txt_code));

            Window.AddDoneKeyboardAction(((EditText) findViewById(R.id.combo_wiz_step_cod_barra_txt_code)), _step.doneAction());
        }

        @Override
        public boolean onStepNext() {
            //creamos el producto y tomamos el campo codigo

            EditText txtCode = ((EditText) findViewById(R.id.combo_wiz_step_cod_barra_txt_code));

            //validamos que sea obligatorio
            txtCode.setError(null);

            String code = txtCode.getText().toString().trim();

            if (TextUtils.isEmpty(code)) {
                txtCode.setError(getString(R.string.error_field_required));
                txtCode.requestFocus();
                return false;
            } else {

                List<Product> secund = new ProductDao(getContentResolver()).getByCode(code);

                if (secund != null && secund.size() > 0) {

                    productoSecundTemp = secund.get(0);

                    skuStep(false);
                    return true;
                } else {
                    txtCode.setError("Producto desconocido");
                    txtCode.requestFocus();
                    return false;
                }

            }

        }
    };

    private WizardStepChangeListener stepProdSecundCantidadChange = new WizardStepChangeListener() {
        @Override
        public void onStepBack() {
        }

        @Override
        public void onStepShow(WizardStep _step) {
            //mostramos la cantidad que teniamos
            */
/*if (productoSecundTemp != null && combo.getCantidad() != 0) {
                ((EditText) findViewById(R.id.combo_wiz_step_cantidad_txt_cantidad)).setText(Formato.FormateaDecimal(combo.getCantidad(), Formato.Decimal_Format.US));
            }*//*


            //((TextView) findViewById(R.id.prod_wiz_step_contenido_lbl_contenido)).setText(String.format("Ingrese cantidad de %s:", product.getUnit().toUpperCase()));

            updateComboCreation(61, (TextView) findViewById(R.id.combo_wiz_step_cantidad_lbl_combotemp));

            Window.FocusViewShowSoftKeyboard(ComboWizardActivity.this, findViewById(R.id.combo_wiz_step_cantidad_txt_cantidad));

            Window.AddDoneKeyboardAction(((EditText) findViewById(R.id.combo_wiz_step_cantidad_txt_cantidad)), _step.doneAction());

        }

        @Override
        public boolean onStepNext() {

            //tomamos el campo cantidad ingresado
            EditText txtCantidad = ((EditText) findViewById(R.id.combo_wiz_step_cantidad_txt_cantidad));

            //validamos que sea obligatorio
            txtCantidad.setError(null);

            String cantidad = txtCantidad.getText().toString().trim();

            if (TextUtils.isEmpty(cantidad)) {
                txtCantidad.setError(getString(R.string.error_field_required));
                txtCantidad.requestFocus();
                return false;
            } else {
                double cant = Formato.ParseaDecimal(cantidad, Formato.Decimal_Format.US);

                if (cant <= 0) {
                    txtCantidad.setError(getString(R.string.error_invalid_value));
                    txtCantidad.requestFocus();
                    return false;
                } else {
                    //creamos un item de combo
                    ComboItem item = new ComboItem();
                    item.setCantidad(cant);
                    item.setProducto_id(productoSecundTemp.getId());
                    item.setProducto_code(productoSecundTemp.getCode());
                    item.setProducto_precio_vta(productoSecundTemp.getSalePrice());
                    item.setSync("N");

                    //inicializamos la lista de items
                    if (_items == null)
                        _items = new ArrayList<ComboItem>();

                    _items.add(item);

                    productoSecundTemp = null;

                    return true;
                }
            }
        }

        */
/*private void assignUnit(String unit) {
            product.setUnit(unit);

            updateLabelContenido();
        }*//*


        */
/*private void updateLabelContenido() {

            //unidad
            String contenido = ((EditText) findViewById(R.id.prod_wiz_step_contenido_txt_contenido)).getText().toString().trim();
            String lbl = String.format("%s%s", contenido, product.getUnit());

            ((TextView) findViewById(R.id.prod_wiz_step_contenido_lbl_contenido)).setText(lbl);
        }*//*



    };

    private WizardStepChangeListener stepEndChange = new WizardStepChangeListener() {
        @Override
        public void onStepBack() {
        }

        @Override
        public void onStepShow(WizardStep _step) {
            //cerramos el teclado
            Window.CloseSoftKeyboard(ComboWizardActivity.this);

            final String precio_formateado = Formato.FormateaDecimal(combo.getPrecio(), FORMATO_DECIMAL, Formato.getCurrencySymbol(FORMATO_DECIMAL));

            findViewById(R.id.combo_wiz_step_valida_btn_print).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SkuUtils.ImprimirEtiqueta(ComboWizardActivity.this, config.PRINTER_MODEL, combo.getProducto_ppal_code(), combo.getNombre(), "", false, "");
                }
            });

            findViewById(R.id.combo_wiz_step_valida_btn_print_precio).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SkuUtils.ImprimirEtiqueta(ComboWizardActivity.this, config.PRINTER_MODEL, combo.getProducto_ppal_code(), combo.getNombre(), precio_formateado, true, "");
                }
            });

            //mostramos los datos del combo
            if (combo != null) {
                ((TextView) findViewById(R.id.combo_wiz_step_valida_lbl_nombre)).setText(combo.getNombre());
                ((TextView) findViewById(R.id.combo_wiz_step_valida_lbl_desc_corta)).setText(combo.getDescripcion());
                ((TextView) findViewById(R.id.combo_wiz_step_valida_lbl_precio)).setText(precio_formateado);
                ((TextView) findViewById(R.id.combo_wiz_step_valida_lbl_sku)).setText(combo.getProducto_ppal_code());

                //actualizamos los items
                List<ComboItem> _itemsMasPrincipal = new ArrayList<ComboItem>();

                //creamos un ComboItem temporalmente para el producto principal
                ComboItem itemPpalTemp = new ComboItem();
                itemPpalTemp.setProducto_code(productoPpal.getCode());
                itemPpalTemp.setProducto_id(productoPpal.getId());
                itemPpalTemp.setCantidad(combo.getCantidad());

                _itemsMasPrincipal.add(itemPpalTemp);

                if(_items != null)
                    _itemsMasPrincipal.addAll(_items);

                ListView lstItems = (ListView) findViewById(R.id.combo_wiz_step_valida_lst_items);
                lstItems.setEmptyView(findViewById(android.R.id.empty));
                ItemsArrayAdapter listAdapter = new ItemsArrayAdapter(ComboWizardActivity.this, _itemsMasPrincipal);
                lstItems.setAdapter(listAdapter);

            }

        }

        @Override
        public boolean onStepNext() {

            try {

                combo.setFecha_alta(new Date());
                combo.setCodigo("");
                combo.setSync("N");

                //crear combo
                boolean success = new ComboDao(getContentResolver()).saveOrUpdate(combo);

                if (success) {

                    //grabamos el producto como principal de un combo
                    productoPpal.setType("P"); //lo marcamos como producto ppal de un combo

                    new ProductDao(getContentResolver()).saveOrUpdate(productoPpal);

                    //grabamos los items, si los tiene
                    if (_items != null) {
                        ComboItemDao comboItemDao = new ComboItemDao(getContentResolver());

                        for (ComboItem det : _items) {
                            //asociamos la cabecera y grabamos
                            det.setCombo_id(combo.getId());
                            det.setSync("N");

                            comboItemDao.saveOrUpdate(det);
                        }
                    }

                    Logger.RegistrarEvento(getContentResolver(), "i", "ALTA COMBO", "SKU PPAL: " + combo.getProducto_ppal_code(), "id: " + String.valueOf(combo.getId()));

                    AlertDialog.Builder builder = new AlertDialog.Builder(ComboWizardActivity.this);
                    builder.setIcon(android.R.drawable.ic_dialog_info);
                    builder.setTitle("NUEVO COMBO");
                    builder.setMessage(Html.fromHtml(String.format("El combo <b>\"%s\"</b> fue generado correctamente!", combo.getNombre())));
                    builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface _dialog, int which) {
                            _dialog.dismiss();

                            finish();
                        }
                    });
                    builder.setCancelable(false);
                    builder.show();

                    //Toast.makeText(getApplicationContext(), "Combo generado correctamente!", Toast.LENGTH_SHORT).show();
                    return false;
                } else {
                    Toast.makeText(getApplicationContext(), "Error al generar el combo", Toast.LENGTH_SHORT).show();
                    return false;
                }

            } catch (Exception ex) {
                Toast.makeText(ComboWizardActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();

                return false;

            }
        }
    };

    public void execWizStep(WizardStep step) {
        //aqui deberíamos completar el paso

        currentStep = step;

        //title
        ((TextView) findViewById(R.id.combo_wiz_lbl_title)).setText(step.titleId);

        final WizardStepChangeListener listener = step.stepChangeListener;

        //back
        if (step.previousStep != null) {
            final WizardStep previousStep = step.previousStep;
            findViewById(R.id.combo_wiz_btn_back).setVisibility(View.VISIBLE);
            findViewById(R.id.combo_wiz_btn_back).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        listener.onStepBack();
                    execWizStep(previousStep);
                }
            });
        } else
            findViewById(R.id.combo_wiz_btn_back).setVisibility(View.INVISIBLE);

        //next
        if (step.nextStep != null) {
            final WizardStep nextStep = step.nextStep;
            ((Button) findViewById(R.id.combo_wiz_btn_next)).setText("SIGUIENTE");
            findViewById(R.id.combo_wiz_btn_next).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        if (listener.onStepNext())
                            execWizStep(nextStep);
                    } else
                        execWizStep(nextStep);
                }
            });
            step.nextButton = ((Button) findViewById(R.id.combo_wiz_btn_next));
        } else {
            ((Button) findViewById(R.id.combo_wiz_btn_next)).setText("GRABAR");
            findViewById(R.id.combo_wiz_btn_next).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        if (listener.onStepNext()) {
                            finish();//salimos
                        }
                }
            });
        }
        ((Button) findViewById(R.id.combo_wiz_btn_next)).setVisibility(step.nextButtonViewMode);


        //inflate view
        LinearLayout container = ((LinearLayout) findViewById(R.id.combo_wiz_lay_step));
        container.removeAllViews();

        if (step.viewId != -1) {
            View view = getLayoutInflater().inflate(step.viewId, null);
            container.addView(view, 0);
        }

        //ejecutamos el evento
        if (listener != null)
            listener.onStepShow(step);
    }

    public class ItemsArrayAdapter extends ArrayAdapter<ComboItem> {
        private final Context context;
        private List<ComboItem> items;

        public ItemsArrayAdapter(Context context, List<ComboItem> items) {
            super(context, R.layout.list_item_pedido_detalle, items);
            this.context = context;
            this.items = items;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View rowView = inflater.inflate(R.layout.list_item_pedido_detalle, parent, false);

            ComboItem item = items.get(position);

            Product p = item.getProducto(getContentResolver());

            ((TextView) rowView.findViewById(R.id.list_item_producto_cod)).setText(TextUtils.isEmpty(p.getCode()) ? "S/C" : p.getCode());
            ((TextView) rowView.findViewById(R.id.list_item_pedidos_producto_det)).setText(p.getName());
            ((TextView) rowView.findViewById(R.id.list_item_pedidos_cantidad)).setText(Integer.toString((int) item.getCantidad()));

            return rowView;
        }

    }
}
*/
