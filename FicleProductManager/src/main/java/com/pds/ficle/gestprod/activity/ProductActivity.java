package com.pds.ficle.gestprod.activity;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.EAN13Writer;
import com.pds.common.Config;
import com.pds.common.Formatos;
import com.pds.common.Usuario;
import com.pds.common.activity.TimerActivity;
import com.pds.common.dao.CategoriaDao;
import com.pds.common.dao.HistMovStockDao;
import com.pds.common.db.ProductTable;
import com.pds.common.model.Categoria;
import com.pds.common.model.HistMovStock;
import com.pds.ficle.gestprod.R;
import com.pds.common.dao.DepartmentDao;
import com.pds.common.dao.ProductDao;
import com.pds.common.dao.ProviderDao;
import com.pds.common.dao.SubDepartmentDao;
import com.pds.common.dao.TaxDao;
import com.pds.common.model.Department;
import com.pds.common.model.Product;
import com.pds.common.model.Provider;
import com.pds.common.model.SubDepartment;
import com.pds.common.model.Tax;
import com.pds.ficle.gestprod.fragment.EditStockFragment;
import com.pds.ficle.gestprod.util.SkuUtils;
import com.pds.ficle.gestprod.util.SpinnerUtils;
import com.pds.ficle.gestprod.util.ViewUtils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import android_serialport_api.Printer;

public class ProductActivity extends TimerActivity implements EditStockFragment.OnEditStockInteractionListener {
    private final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");

    private Button addButton;
    private Button cancelButton;
    private Button imprimirButton;
    private ImageView addDepartmentImage;
    private ImageView addSubDepartmentImage;
    private ImageView addProviderImage;
    private Button deleteButton;
    private EditText altaEditText;
    private EditText nameEditText;
    private EditText descriptionEditText;
    private EditText brandEditText;
    private EditText presentationEditText;
    private EditText salePriceEditText;
    private EditText taxPercentEditText;
    private EditText minQuantityEditText;
    private EditText codeEditText;
    private TextView stockQuantityEditText;
    private EditText unitEditText;
    private EditText purchasePriceEditText;
    private EditText txtMargen;
    private Spinner departmentSpinner;
    private Spinner subDepartmentSpinner;
    private Spinner taxSpinner;
    private Spinner providerSpinner;
    private Switch weighableSwitch;
    private Switch shorcutSwitch;
    private EditText contenidoEditText;
    private EditText categoriaEditText;

    private Spinner nivel1Spinner;
    private Spinner nivel2Spinner;
    private Spinner nivel3Spinner;
    private Spinner nivel4Spinner;
    private Spinner nivel5Spinner;

    private ArrayAdapter<Categoria> nivel1ArrayAdapter;
    private List<Categoria> nivel1List;
    private ArrayAdapter<Categoria> nivel2ArrayAdapter;
    private List<Categoria> nivel2List;
    private ArrayAdapter<Categoria> nivel3ArrayAdapter;
    private List<Categoria> nivel3List;
    private ArrayAdapter<Categoria> nivel4ArrayAdapter;
    private List<Categoria> nivel4List;
    private ArrayAdapter<Categoria> nivel5ArrayAdapter;
    private List<Categoria> nivel5List;

    private String name;
    private String alta;
    private String description;
    private String brand;
    private String presentation;
    private String salePrice;
    private String minQuantity;
    private String code;
    private String stockQuantity;
    private String unit;
    private String contenido;
    private String purchasePrice;
    private boolean weighable;
    private boolean shorcut;
    private Department department;
    private SubDepartment subDepartment;
    private Provider provider;
    private Tax tax;
    private String codigo_padre;

    private List<Tax> taxList;

    private ProductTask productTask = null;

    Product product;
    private boolean edit = false;
    private boolean modoSinSKU = false;

    private ArrayAdapter<Department> departmentArrayAdapter;
    private List<Department> departmentList;
    private ArrayAdapter<SubDepartment> subDepartmentArrayAdapter;
    private List<SubDepartment> subDepartmentList;
    private ArrayAdapter<Provider> providerArrayAdapter;
    private List<Provider> providerList;

    private DecimalFormat FORMATO_DECIMAL, FORMATO_DECIMAL_LBL;

    private HistMovStock cambioStock;
    private Usuario loggedUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        departmentList = new ArrayList<Department>();
        subDepartmentList = new ArrayList<SubDepartment>();
        providerList = new ArrayList<Provider>();

        nivel1List = new ArrayList<Categoria>();
        nivel2List = new ArrayList<Categoria>();
        nivel3List = new ArrayList<Categoria>();
        nivel4List = new ArrayList<Categoria>();
        nivel5List = new ArrayList<Categoria>();

        final Config cfg = new Config(this);

        Init_CustomConfigPais(cfg.PAIS);

        Intent intent = getIntent();
        String searchString = intent.getStringExtra("searchString");//parametro usado para buscar en la pantalla de searchActivity
        product = intent.getParcelableExtra("product");
        loggedUser = (Usuario) intent.getSerializableExtra("current_user");

        if (product == null) {
            product = new Product();
            product.setAutomatic(Product.TYPE_AUTOM.PROD_ALTA_MANUAL);
            alta = DATE_FORMAT.format(new Date());
            department = null;
            subDepartment = null;
            if (!TextUtils.isEmpty(searchString)) {
                //si tenemos algun parametro de busqueda para precargar en el NUEVO producto, lo usamos
                if (TextUtils.isDigitsOnly(searchString))
                    code = searchString; //si es numerico, lo usamos como codigo
                else
                    name = searchString; //sino, como nombre
            }

            if (TextUtils.isEmpty(code)) {
                //no hay SKU => modo SIN SKU
                findViewById(R.id.product_abm_producto_seccion_con_sku).setVisibility(View.GONE);
                findViewById(R.id.product_abm_producto_seccion_sin_sku).setVisibility(View.VISIBLE);
                findViewById(R.id.product_abm_presentacion_seccion).setVisibility(View.GONE);
                modoSinSKU = true;
            } else {
                //vino un SKU => modo CON SKU
                findViewById(R.id.product_abm_producto_seccion_con_sku).setVisibility(View.VISIBLE);
                findViewById(R.id.product_abm_producto_seccion_sin_sku).setVisibility(View.GONE);
            }

            //int layoutProduct = !TextUtils.isEmpty(code) ? R.layout.seccion_product_sin_sku_layout : R.layout.seccion_product_con_sku_layout;
            /*ViewGroup parent = (ViewGroup) findViewById(R.id.product_abm_producto_seccion);
            View view = getLayoutInflater().inflate(layoutProduct, parent, false);
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)view.getLayoutParams();
            layoutParams.setMargins(0,13,0,0);
            view.setLayoutParams(layoutParams);
            parent.addView(view, 0);*/


        } else {
            edit = true;
            alta = DATE_FORMAT.format(product.getAlta());
            name = product.getName();
            description = product.getDescription();
            brand = product.getBrand();
            presentation = product.getPresentation();
            salePrice = FormateaDecimal(product.getSalePrice(), FORMATO_DECIMAL);
            //TODO check this value, should it need to be integer?
            minQuantity = Integer.toString((int) product.getMinStock());
            code = product.getCode();
            //TODO check this value, should it need to be integer?
            stockQuantity = Integer.toString((int) product.getStock());
            unit = product.getUnit();
            purchasePrice = FormateaDecimal(product.getPurchasePrice(), FORMATO_DECIMAL);
            weighable = product.isWeighable();
            shorcut = product.isQuickAccess();
            contenido = Formatos.FormateaDecimal(product.getContenido(), Formatos.DecimalFormat_CH);

            department = new DepartmentDao(getContentResolver()).find(product.getIdDepartment());
            //department = product.getDepartment();
            subDepartment = new SubDepartmentDao(getContentResolver()).find(product.getIdSubdepartment());
            //subDepartment = product.getSubDepartment();
            provider = product.getIdProvider() != -1 ? new ProviderDao(getContentResolver()).find(product.getIdProvider()) : null;
            //provider = product.getProvider();
            tax = new TaxDao(getContentResolver()).find(product.getIdTax());
            //tax = product.getTax();

            codigo_padre = product.getCodigoPadre();
        }

        findViewById(R.id.product_btn_edit_stock).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IniciarFragmentEditStock(product);
            }
        });

        altaEditText = (EditText) findViewById(R.id.product_alta_date);
        altaEditText.setText(alta);

        nameEditText = (EditText) findViewById(R.id.product_name);
        nameEditText.setText(name);
        brandEditText = (EditText) findViewById(R.id.product_brand);
        brandEditText.setText(brand);
        presentationEditText = (EditText) findViewById(R.id.product_presentation);
        presentationEditText.setText(presentation);
        salePriceEditText = (EditText) findViewById(R.id.product_sale_price);
        salePriceEditText.setText(!TextUtils.isEmpty(salePrice) ? (salePrice.equals("0") ? null : salePrice) : null);
        minQuantityEditText = (EditText) findViewById(R.id.product_min_quantity);
        minQuantityEditText.setText(!TextUtils.isEmpty(minQuantity) ? (minQuantity.equals("0") ? null : minQuantity) : null);
        codeEditText = (EditText) findViewById(R.id.product_code);
        codeEditText.setText(code);
        stockQuantityEditText = (TextView) findViewById(R.id.product_stock_quantity);
        stockQuantityEditText.setText(stockQuantity);
        if (!edit) {
            stockQuantityEditText.setEnabled(true);
            findViewById(R.id.product_btn_edit_stock).setEnabled(false);
        }
        unitEditText = (EditText) findViewById(R.id.product_unit);
        unitEditText.setText(unit);
        descriptionEditText = (EditText) findViewById(R.id.product_description);
        descriptionEditText.setText(description);
        purchasePriceEditText = (EditText) findViewById(R.id.product_purchase_price);
        purchasePriceEditText.setText(!TextUtils.isEmpty(purchasePrice) ? (purchasePrice.equals("0") ? null : purchasePrice) : null);
        contenidoEditText = (EditText) findViewById(R.id.product_contenido);
        contenidoEditText.setText(contenido);
        categoriaEditText = (EditText) (modoSinSKU ? findViewById(R.id.product_categoria_2) : findViewById(R.id.product_categoria));
        taxPercentEditText = (EditText) findViewById(R.id.product_tax_percent);
        //taxPercentEditText.setText("19");//TODO: HARCODED

        txtMargen = (EditText) findViewById(R.id.product_margen);

        departmentList = getListDepartments(); //traemos TODOS los departamentos
        departmentArrayAdapter = new ArrayAdapter<Department>(this, android.R.layout.simple_spinner_item, departmentList);
        departmentArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        departmentSpinner = (Spinner) findViewById(R.id.product_department);
        departmentSpinner.setAdapter(departmentArrayAdapter);

        if (department != null) {//seleccionamos el departamento del producto editado
            int index = SpinnerUtils.getIndexForElement(departmentSpinner, new SpinnerUtils.Predicate<Department>() {
                @Override
                public boolean evaluate(Department element) {
                    return department.getId() == element.getId();
                }
            });
            departmentSpinner.setSelection(index);
        }

        if (departmentSpinner.getSelectedItem() != null) { //si hay un departamento seleccionado => traemos sus subdepartamentos y sus providers
            subDepartmentList = getListSubDepartments(
                    String.format("department_id = %d", ((Department) departmentSpinner.getSelectedItem()).getId()));

            providerList = getListProvider(
                    String.format("department_id = %d OR department_id = 0", ((Department) departmentSpinner.getSelectedItem()).getId()));
        } else {
            subDepartmentList = new ArrayList<SubDepartment>(); //no traemos subdepartamentos
            providerList = getListProvider( //traemos los proveedores que no tienen departamentos asignados
                    String.format("department_id = 0", ((Department) departmentSpinner.getSelectedItem()).getId()));
        }

        subDepartmentArrayAdapter = new ArrayAdapter<SubDepartment>(this, android.R.layout.simple_spinner_item, subDepartmentList);
        subDepartmentArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        subDepartmentSpinner = (Spinner) findViewById(R.id.product_sub_department);
        subDepartmentSpinner.setAdapter(subDepartmentArrayAdapter);

        if (subDepartment != null) { //seleccionamos el departamento del producto editado
            int index = SpinnerUtils.getIndexForElement(subDepartmentSpinner, new SpinnerUtils.Predicate<SubDepartment>() {
                @Override
                public boolean evaluate(SubDepartment element) {
                    return subDepartment.getId() == element.getId();
                }
            });
            subDepartmentSpinner.setSelection(index);
        }

        if (subDepartmentSpinner.getSelectedItem() != null)//si hay un subdepartamento seleccionado => traemos sus providers
            providerList.addAll(getListProvider(
                    String.format("subdepartment_id = %d OR subdepartment_id=0", ((SubDepartment) subDepartmentSpinner.getSelectedItem()).getId())));

        providerArrayAdapter = new ArrayAdapter<Provider>(this, android.R.layout.simple_spinner_item, providerList);
        providerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        providerSpinner = (Spinner) findViewById(R.id.product_provider);
        providerSpinner.setAdapter(providerArrayAdapter);

        if (provider != null) {//seleccionamos el provider del producto editado
            int index = SpinnerUtils.getIndexForElement(providerSpinner, new SpinnerUtils.Predicate<Provider>() {
                @Override
                public boolean evaluate(Provider element) {
                    return provider.getId() == element.getId();
                }
            });
            providerSpinner.setSelection(index);
        }

        taxList = new TaxDao(getContentResolver()).list();
        taxSpinner = (Spinner) findViewById(R.id.product_tax);
        ArrayAdapter<Tax> taxAdapter = new ArrayAdapter<Tax>(this, android.R.layout.simple_spinner_item, taxList);
        taxAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        taxSpinner.setAdapter(taxAdapter);
        taxSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tax = (Tax) taxSpinner.getItemAtPosition(position);
                taxPercentEditText.setText(tax.getAmount().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (edit) {
            if (tax != null) {
                int index = SpinnerUtils.getIndexForElement(taxSpinner, new SpinnerUtils.Predicate<Tax>() {
                    @Override
                    public boolean evaluate(Tax o) {
                        return o.getId() == tax.getId();
                    }
                });
                taxSpinner.setSelection(index);
            }
        }

        this.addSeletectItemListeners();

        categoriaEditText.setText(codigo_padre); //lo agregamos acá para que no influya el load de los combos

        weighableSwitch = (Switch) findViewById(R.id.product_weighable);
        weighableSwitch.setChecked(weighable);
        weighableSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                weighable = isChecked;
            }
        });

        shorcutSwitch = (Switch) findViewById(R.id.product_shorcut);
        shorcutSwitch.setChecked(shorcut);
        shorcutSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                shorcut = isChecked;
            }
        });

        cancelButton = (Button) findViewById(R.id.product_back_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        addButton = (Button) findViewById(R.id.product_add_button);
        if (modoSinSKU) {
            //save modo sin SKU
            addButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    validateModoSinSKU();
                }
            });
        } else {
            //save normal
            addButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    validate();
                }
            });
        }

        addDepartmentImage = (ImageView) findViewById(R.id.product_department_add_icon);
        addDepartmentImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProductActivity.this, DepartmentActivity.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });

        addSubDepartmentImage = (ImageView) findViewById(R.id.product_sub_department_add_icon);
        addSubDepartmentImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProductActivity.this, SubDepartmentActivity.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });

        addProviderImage = (ImageView) findViewById(R.id.product_provider_add_icon);
        addProviderImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProductActivity.this, ProviderActivity.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });

        if (edit) {

            imprimirButton = (Button) findViewById(R.id.product_imprimir_button);
            imprimirButton.setVisibility(View.VISIBLE);
            imprimirButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SkuUtils.ImprimirEtiqueta(ProductActivity.this, cfg.PRINTER_MODEL, code, name, Formatos.FormateaDecimal(product.getSalePrice(), Formatos.getDecimalFormat(cfg.PAIS), Formatos.getCurrencySymbol(cfg.PAIS)));
                }
            });

            deleteButton = (Button) findViewById(R.id.product_delete_button);
            deleteButton.setVisibility(View.VISIBLE);
            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    delete();
                }
            });

            adjustEditMode();
        }


        if (modoSinSKU) {
            nivel1ArrayAdapter = new ArrayAdapter<Categoria>(this, android.R.layout.simple_spinner_item, nivel1List);
            nivel1ArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            nivel1Spinner = (Spinner) findViewById(R.id.product_nivel_1);
            nivel1Spinner.setAdapter(nivel1ArrayAdapter);

            nivel2ArrayAdapter = new ArrayAdapter<Categoria>(this, android.R.layout.simple_spinner_item, nivel2List);
            nivel2ArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            nivel2Spinner = (Spinner) findViewById(R.id.product_nivel_2);
            nivel2Spinner.setAdapter(nivel2ArrayAdapter);

            nivel3ArrayAdapter = new ArrayAdapter<Categoria>(this, android.R.layout.simple_spinner_item, nivel3List);
            nivel3ArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            nivel3Spinner = (Spinner) findViewById(R.id.product_nivel_3);
            nivel3Spinner.setAdapter(nivel3ArrayAdapter);

            nivel4ArrayAdapter = new ArrayAdapter<Categoria>(this, android.R.layout.simple_spinner_item, nivel4List);
            nivel4ArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            nivel4Spinner = (Spinner) findViewById(R.id.product_nivel_4);
            nivel4Spinner.setAdapter(nivel4ArrayAdapter);

            nivel5ArrayAdapter = new ArrayAdapter<Categoria>(this, android.R.layout.simple_spinner_item, nivel5List);
            nivel5ArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            nivel5Spinner = (Spinner) findViewById(R.id.product_nivel_5);
            nivel5Spinner.setAdapter(nivel5ArrayAdapter);

            AddCategoriasListeners();

            fillNextCategoria(1, "");
        }


        ChangeMargenWatcher change_vta = new ChangeMargenWatcher(ChangeMargenWatcher.TYPE_VENTA);
        ChangeMargenWatcher change_mgn = new ChangeMargenWatcher(ChangeMargenWatcher.TYPE_MARGEN);
        ChangeMargenWatcher change_cpa = new ChangeMargenWatcher(ChangeMargenWatcher.TYPE_COMPRA);

        change_vta.setListener(change_mgn);
        change_mgn.setListener(change_vta);
        change_cpa.setListener(change_vta);

        purchasePriceEditText.addTextChangedListener(change_cpa);
        purchasePriceEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                }
            }
        });

        salePriceEditText.addTextChangedListener(change_vta);
        salePriceEditText.setOnFocusChangeListener(purchasePriceEditText.getOnFocusChangeListener());

        txtMargen.addTextChangedListener(change_mgn);
        txtMargen.setOnFocusChangeListener(txtMargen.getOnFocusChangeListener());


        //definimos las "zonas touch" del activity
        ViewUtils.DefineViewGroup(this, salePriceEditText.getId(), R.id.lbl_product_sale_price);
        ViewUtils.DefineViewGroup(this, minQuantityEditText.getId(), R.id.lbl_product_min_quantity);
        ViewUtils.DefineViewGroup(this, purchasePriceEditText.getId(), R.id.lbl_product_purchase_price);
        ViewUtils.DefineViewGroup(this, txtMargen.getId(), R.id.lbl_product_margen);

        ViewUtils.DefineViewGroup(this, descriptionEditText.getId(), R.id.lbl_product_description);
        ViewUtils.DefineViewGroup(this, brandEditText.getId(), R.id.lbl_product_brand);
        ViewUtils.DefineViewGroup(this, presentationEditText.getId(), R.id.lbl_product_presentation);
        ViewUtils.DefineViewGroup(this, unitEditText.getId(), R.id.lbl_product_unit);
        ViewUtils.DefineViewGroup(this, contenidoEditText.getId(), R.id.lbl_product_contenido);
        ViewUtils.DefineViewGroup(this, codeEditText.getId(), R.id.lbl_product_code);
    }

    private void Init_CustomConfigPais(String codePais) {

        //FORMATO_DECIMAL = codePais.equals("AR") ? Formatos.DecimalFormat_US : Formatos.DecimalFormat_CH;
        //FORMATO_DECIMAL_SECUND = codePais.equals("AR") ? Formatos.DecimalFormat : Formatos.DecimalFormat_US;

        //este formato se usa en...
        if (codePais.equals("AR")) {
            FORMATO_DECIMAL = Formatos.DecimalFormat_US;
            FORMATO_DECIMAL_LBL = Formatos.DecimalFormat;
        }
        else if (codePais.equals("PE")){
            FORMATO_DECIMAL = Formatos.DecimalFormat_PE;
            FORMATO_DECIMAL_LBL = FORMATO_DECIMAL;
        }
        else{
            FORMATO_DECIMAL = Formatos.DecimalFormat_CH;
            FORMATO_DECIMAL_LBL = FORMATO_DECIMAL;
        }

    }

    private String FormateaDecimal(double value, DecimalFormat format) {
        if (value != 0)
            return Formatos.FormateaDecimal(value, format);
        else
            return "0";
    }

    public void adjustEditMode() {
        if (product.getAutomatic() == Product.TYPE_AUTOM.PROD_ALTA_AUTOM) {
            //no tiene la opcion de eliminar
            deleteButton.setVisibility(View.GONE);

            //*******SECCION PRODUCTO***********
            //no puede editar el nombre
            nameEditText.setEnabled(false);
            //no se puede cambiar el depto
            departmentSpinner.setEnabled(false);
            departmentSpinner.setOnItemSelectedListener(null);
            //no se puede cambiar el subdepto
            subDepartmentSpinner.setEnabled(false);
            subDepartmentSpinner.setOnItemSelectedListener(null);
            //no se puede editar el codigo
            codeEditText.setEnabled(false);
            //no puede editar condicion de iva
            taxSpinner.setEnabled(false);
            taxPercentEditText.setEnabled(false);

            //******SECCION PRESENTACION*********
            //no puede editar marca
            brandEditText.setEnabled(false);
            //no puede editar presentacion
            presentationEditText.setEnabled(false);
            //no puede editar unidad
            unitEditText.setEnabled(false);
            //no puede editar contenido
            contenidoEditText.setEnabled(false);
            //no puede editar pesable
            weighableSwitch.setEnabled(false);
        }
    }

    private void addSeletectItemListeners() {
        departmentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Department d = (Department) departmentSpinner.getItemAtPosition(position);

                //traemos los subdepartamentos del departamento seleccionado
                subDepartmentList.clear();
                subDepartmentList.addAll(getListSubDepartments(
                        String.format("department_id = %d", d.getId())));
                subDepartmentArrayAdapter.notifyDataSetChanged();

                //traemos los providers del departamento seleccionado
                providerList.clear();
                providerList.addAll(getListProvider(
                        String.format("department_id = %d OR department_id=0", d.getId())));
                providerArrayAdapter.notifyDataSetChanged();

                //agregamos la categoria
                if (d.getId() != -1) {
                    codigo_padre = d.getCodigo();
                    categoriaEditText.setText(d.getCodigo());
                } else {
                    codigo_padre = "";
                    categoriaEditText.setText("");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        subDepartmentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SubDepartment d = (SubDepartment) subDepartmentSpinner.getItemAtPosition(position);

                //traemos los providers del subdepartamento seleccionado
                providerList.clear();
                providerList.addAll(getListProvider(
                        String.format("subdepartment_id = %d OR subdepartment_id=0", d.getId())));
                providerArrayAdapter.notifyDataSetChanged();

                //agregamos la categoria
                if (d.getId() != -1) {
                    codigo_padre = d.getCodigo();
                    categoriaEditText.setText(d.getCodigo());
                } else {
                    codigo_padre = ((Department) departmentSpinner.getSelectedItem()).getCodigo();
                    categoriaEditText.setText(((Department) departmentSpinner.getSelectedItem()).getCodigo());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    private double getDouble(String value) {
        if (TextUtils.isEmpty(value))
            return 0;
        else
            return Formatos.ParseaDecimal(value.replace("$", "").replace("%", "").trim(), FORMATO_DECIMAL);
    }

    class ChangeMargenWatcher implements TextWatcher {

        public static final int TYPE_COMPRA = 0;
        public static final int TYPE_VENTA = 1;
        public static final int TYPE_MARGEN = 2;

        private int type;
        private ChangeMargenWatcher listener;

        public void setListener(ChangeMargenWatcher listener) {
            this.listener = listener;
        }

        public ChangeMargenWatcher(int type) {
            this.type = type;
        }


        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (type == TYPE_COMPRA) {
                if (!TextUtils.isEmpty(s)) {
                    //si cambiamos el precio de compra, actualizamos el precio de venta segun el margen fijado
                    double precio_cpa = getDouble(s.toString());

                    double margen = getDouble(txtMargen.getText().toString());

                    double precio_vta = precio_cpa * (1 + (margen / 100));

                    salePriceEditText.removeTextChangedListener(listener);
                    salePriceEditText.setText(Formatos.FormateaDecimal(precio_vta, FORMATO_DECIMAL));
                    salePriceEditText.addTextChangedListener(listener);

                    //double precio_vta = Formatos.ParseaDecimal(!TextUtils.isEmpty(txtPrecioVta.getText().toString()) ? txtPrecioVta.getText().toString() : "0", FORMATO);

                    //txtMargen.setText(Formatos.FormateaDecimal(precio_vta - precio_cpa, FORMATO) + " %");
                }
            } else if (type == TYPE_VENTA) {

                if (!TextUtils.isEmpty(s)) {
                    //si cambiamos el precio de venta, actualizamos el margen
                    double precio_vta = getDouble(s.toString());

                    double precio_cpa = getDouble(purchasePriceEditText.getText().toString());

                    double margen = precio_cpa != 0 ? ((precio_vta / precio_cpa) - 1) * 100 : 100;

                    txtMargen.removeTextChangedListener(listener);
                    txtMargen.setText(Formatos.FormateaDecimal(margen, FORMATO_DECIMAL) + " %");
                    txtMargen.addTextChangedListener(listener);
                }

            } else if (type == TYPE_MARGEN) {

                if (!TextUtils.isEmpty(s)) {
                    //si cambiamos el margen, modificamos el precio de venta
                    double margen = getDouble(s.toString());

                    double precio_cpa = getDouble(purchasePriceEditText.getText().toString());

                    double precio_vta = precio_cpa * (1 + (margen / 100));

                    salePriceEditText.removeTextChangedListener(listener);
                    salePriceEditText.setText(Formatos.FormateaDecimal(precio_vta, FORMATO_DECIMAL));
                    salePriceEditText.addTextChangedListener(listener);

                    //txtMargen.setText(Formatos.FormateaDecimal(margen, FORMATO) + " %");
                }
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
        }


    }

    @Override
    public void onResume() {
        super.onResume();

        /*
        //recargamos la lista de departamentos
        departmentList.clear();
        departmentList.addAll(getListDepartments());
        departmentArrayAdapter.notifyDataSetChanged();

        //recargamos la lista de subdepartamentos, del departamento seleccionado
        subDepartmentList.clear();
        Department selectedDepartment = (Department) departmentSpinner.getSelectedItem();
        if (selectedDepartment != null) {
            subDepartmentList.addAll(getListSubDepartments(
                    String.format("department_id = %d", selectedDepartment.getId())));

            providerList.clear();
            providerList.addAll(getListProvider(
                    String.format("department_id = %d OR department_id=0", ((SubDepartment) subDepartmentSpinner.getSelectedItem()).getId())));
            providerArrayAdapter.notifyDataSetChanged();
        }
        subDepartmentArrayAdapter.notifyDataSetChanged();

        //recargamos la lista de providers
        providerList.clear();
        SubDepartment selectedSubdepartment = (SubDepartment) subDepartmentSpinner.getSelectedItem();
        if (selectedSubdepartment != null) { //del subdepartamento seleccionado
            providerList.addAll(getListProvider(
                    String.format("subdepartment_id = %d OR subdepartment_id=0", ((SubDepartment) subDepartmentSpinner.getSelectedItem()).getId())));
        } else if (selectedDepartment != null) { //del departamento seleccionado
            providerList.addAll(getListProvider(
                    String.format("subdepartment_id = %d OR subdepartment_id=0", ((SubDepartment) subDepartmentSpinner.getSelectedItem()).getId())));
        } else { //todos los que no tienen departamento
            providerList.addAll(getListProvider(
                    String.format("department_id=0")));
        }
        providerArrayAdapter.notifyDataSetChanged();*/
    }

    @Override
    protected void onDestroy() {

        //ocultamos el teclado
        ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(null, 0);

        super.onDestroy();
    }

    private List<Department> getListDepartments() {
        //no mostramos deptos genericos, asi no pueden asociarles productos
        List<Department> lst = (new DepartmentDao(getContentResolver())).list("generic = 0", null, "name ASC");
        lst.add(0, new Department(-1, "Seleccione..."));
        return lst;
    }

    private List<SubDepartment> getListSubDepartments(String filter) {
        //no mostramos subdeptos genericos, asi no pueden asociarles productos
        List<SubDepartment> lst = (new SubDepartmentDao(getContentResolver())).list(filter + " and generic = 0", null, "name ASC");
        lst.add(0, new SubDepartment(-1, "Seleccione..."));
        return lst;
    }

    private List<Provider> getListProvider(String filter) {
        List<Provider> lst = (new ProviderDao(getContentResolver())).list(filter, null, null);
        lst.add(0, new Provider(-1, "Seleccione..."));
        return lst;
    }

    private List<Categoria> getListCategorias(int nivel, String codigo_padre) {
        List<Categoria> lst = new ArrayList<Categoria>();

        if (!codigo_padre.equals("...")) {
            String filter = String.format("nivel = %d", nivel);
            if (nivel > 1)
                filter += " and cod_padre = '" + codigo_padre + "'";
            lst = (new CategoriaDao(getContentResolver())).list(filter, null, "nombre ASC");
        }

        lst.add(0, new Categoria(-1, "Seleccione...", nivel, codigo_padre));
        return lst;
    }

    private void fillNextCategoria(int nivel, String codigo_padre) {

        List<Categoria> cat = getListCategorias(nivel, codigo_padre);
        boolean visible = cat.size() > 1;
        int visibility = cat.size() > 1 ? View.VISIBLE : View.INVISIBLE;

        switch (nivel) {
            case 1: {
                nivel1Spinner.setVisibility(visibility);
                findViewById(R.id.product_lbl_nivel_1).setVisibility(visibility);
                nivel1List.clear();
                nivel1List.addAll(cat);
                nivel1ArrayAdapter.notifyDataSetChanged();
                nivel1Spinner.setSelection(0);
            }
            break;
            case 2: {
                nivel2Spinner.setVisibility(visibility);
                findViewById(R.id.product_lbl_nivel_2).setVisibility(visibility);
                nivel2List.clear();
                nivel2List.addAll(cat);
                nivel2ArrayAdapter.notifyDataSetChanged();
                nivel2Spinner.setSelection(0);
            }
            break;
            case 3: {
                nivel3Spinner.setVisibility(visibility);
                findViewById(R.id.product_lbl_nivel_3).setVisibility(visibility);
                nivel3List.clear();
                nivel3List.addAll(cat);
                nivel3ArrayAdapter.notifyDataSetChanged();
                nivel3Spinner.setSelection(0);
            }
            break;
            case 4: {
                nivel4Spinner.setVisibility(visibility);
                findViewById(R.id.product_lbl_nivel_4).setVisibility(visibility);
                nivel4List.clear();
                nivel4List.addAll(cat);
                nivel4ArrayAdapter.notifyDataSetChanged();
                nivel4Spinner.setSelection(0);
            }
            break;
            case 5: {
                nivel5Spinner.setVisibility(visibility);
                findViewById(R.id.product_lbl_nivel_5).setVisibility(visibility);
                nivel5List.clear();
                nivel5List.addAll(cat);
                nivel5ArrayAdapter.notifyDataSetChanged();
                nivel5Spinner.setSelection(0);
            }
            break;
        }

    }

    private AdapterView.OnItemSelectedListener onCategoriaSelected = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Categoria categoria = (Categoria) parent.getItemAtPosition(position);

            if (categoria.getNumero_nivel() < 5)
                fillNextCategoria(categoria.getNumero_nivel() + 1, categoria.getCodigo());//llenamos el siguiente nivel

            if (categoria.getId() != -1) {
                codigo_padre = categoria.getCodigo();
                categoriaEditText.setText(categoria.getCodigo());
            } else if (!categoria.getCodigo_padre().equals("...")) {
                codigo_padre = categoria.getCodigo_padre();
                categoriaEditText.setText(categoria.getCodigo_padre());
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    private void AddCategoriasListeners() {
        nivel1Spinner.setOnItemSelectedListener(onCategoriaSelected);

        nivel2Spinner.setOnItemSelectedListener(onCategoriaSelected);

        nivel3Spinner.setOnItemSelectedListener(onCategoriaSelected);

        nivel4Spinner.setOnItemSelectedListener(onCategoriaSelected);

        nivel5Spinner.setOnItemSelectedListener(onCategoriaSelected);
    }

    public void IniciarFragmentEditStock(Product item_edit) {
        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.product_abm_frg_edit_stock) != null) {

            // Create a new Fragment to be placed in the activity layout
            EditStockFragment fragment = EditStockFragment.newInstance(item_edit, loggedUser);

            fragment.setOnEditStockInteractionListener(this);

            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.product_abm_frg_edit_stock, fragment);
            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            ft.commit();
        }
    }

    public void validate() {
        if (productTask != null) {
            return;
        }

        boolean cancel = false;

        // Reset errors.
        nameEditText.setError(null);
        brandEditText.setError(null);
        presentationEditText.setError(null);
        salePriceEditText.setError(null);
        minQuantityEditText.setError(null);
        codeEditText.setError(null);
        descriptionEditText.setError(null);
        stockQuantityEditText.setError(null);
        unitEditText.setError(null);
        purchasePriceEditText.setError(null);

        // Store values at the time of the login attempt.
        name = nameEditText.getText().toString();
        brand = brandEditText.getText().toString();
        presentation = presentationEditText.getText().toString();
        minQuantity = minQuantityEditText.getText().toString();
        code = codeEditText.getText().toString();
        alta = altaEditText.getText().toString();
        description = descriptionEditText.getText().toString();
        stockQuantity = stockQuantityEditText.getText().toString();
        unit = unitEditText.getText().toString();
        contenido = contenidoEditText.getText().toString();
        salePrice = salePriceEditText.getText().toString();
        purchasePrice = purchasePriceEditText.getText().toString();

        department = (Department) departmentSpinner.getSelectedItem();
        subDepartment = (SubDepartment) subDepartmentSpinner.getSelectedItem();
        //provider = (Provider) providerSpinner.getSelectedItem();
        //TODO: rlp assign a tax
        //tax = (Tax) taxSpinner.getSelectedItem();

        if (TextUtils.isEmpty(name)) {
            nameEditText.setError(getString(R.string.error_field_required));
            cancel = true;
        }

        if (!edit && TextUtils.isEmpty(code)) {
            codeEditText.setError(getString(R.string.error_field_required));
            cancel = true;
        }

        if (department == null) {
            Toast.makeText(this
                    , "Error: No existen departamentos. Por favor, registre al menos un departamento."
                    , Toast.LENGTH_SHORT).show();
            cancel = true;
        } else if (department.getId() == -1) {
            Toast.makeText(this
                    , "Error: Debe seleccionar un departamento."
                    , Toast.LENGTH_SHORT).show();
            cancel = true;
        } else if (subDepartment == null) {
            Toast.makeText(this
                    , "Error: No existen sub departamentos para el departamento seleccionado. Por favor, registre al menos uno.", Toast.LENGTH_SHORT).show();
            cancel = true;
        } else if (subDepartment.getId() == -1) {
            Toast.makeText(this
                    , "Error: Debe seleccionar un sub departamento.", Toast.LENGTH_SHORT).show();
            cancel = true;
        } /*else if (provider == null) {
            Toast.makeText(getApplicationContext()
                    , "No existen proveedores para el sub departamento seleccionado.", Toast.LENGTH_SHORT).show();
            cancel = true;
        }*/
        //validamos unicidad de SKU, otro producto no puede tener el mismo SKU
        else if (!unicidadSKU(product, code)) {
            codeEditText.setError("Codigo ya asociado");
            cancel = true;
        }

        //los ponemos en cero para que luego al parsearlo como double no de error de formato
        if (TextUtils.isEmpty(salePrice)) {
            salePrice = "0";
        }

        if (TextUtils.isEmpty(minQuantity)) {
            minQuantity = "0";
        }

        if (TextUtils.isEmpty(stockQuantity)) {
            stockQuantity = "0";
        }

        if (TextUtils.isEmpty(purchasePrice)) {
            purchasePrice = "0";
        }

        if (TextUtils.isEmpty(contenido)) {
            contenido = "0";
        }

        if (!cancel) {
            productTask = new ProductTask();
            productTask.execute((Void) null);
        }
    }

    public void validateModoSinSKU() {
        if (productTask != null) {
            return;
        }

        boolean cancel = false;

        // Reset errors.
        nameEditText.setError(null);
        descriptionEditText.setError(null);
        stockQuantityEditText.setError(null);
        minQuantityEditText.setError(null);
        purchasePriceEditText.setError(null);
        salePriceEditText.setError(null);

        // Store values at the time of save.
        alta = altaEditText.getText().toString();
        name = nameEditText.getText().toString();
        description = descriptionEditText.getText().toString();
        stockQuantity = stockQuantityEditText.getText().toString();
        minQuantity = minQuantityEditText.getText().toString();
        purchasePrice = purchasePriceEditText.getText().toString();
        salePrice = salePriceEditText.getText().toString();
        contenido = "0";
        code = "";
        brand = "";

        tax = (Tax) taxSpinner.getItemAtPosition(0);

        Categoria nivel1 = (Categoria) nivel1Spinner.getSelectedItem();
        Categoria nivel2 = (Categoria) nivel2Spinner.getSelectedItem();
        Categoria nivel3 = (Categoria) nivel3Spinner.getSelectedItem();
        Categoria nivel4 = (Categoria) nivel4Spinner.getSelectedItem();
        Categoria nivel5 = (Categoria) nivel5Spinner.getSelectedItem();

        if (TextUtils.isEmpty(name)) {
            nameEditText.setError(getString(R.string.error_field_required));
            cancel = true;
        }

        if (nivel1.getId() == -1) {
            Toast.makeText(this, "Error: Debe seleccionar un NIVEL 1", Toast.LENGTH_SHORT).show();
            cancel = true;
            return;
        } else if (nivel2.getId() == -1) {
            Toast.makeText(this, "Error: Debe seleccionar un NIVEL 2", Toast.LENGTH_SHORT).show();
            cancel = true;
            return;
        } else if (nivel3.getId() == -1) {
            Toast.makeText(this, "Error: Debe seleccionar un NIVEL 3", Toast.LENGTH_SHORT).show();
            cancel = true;
            return;
        } else if (nivel4Spinner.getVisibility() == View.VISIBLE && nivel4.getId() == -1) {
            Toast.makeText(this, "Error: Debe seleccionar un NIVEL 4", Toast.LENGTH_SHORT).show();
            cancel = true;
            return;
        } else if (nivel5Spinner.getVisibility() == View.VISIBLE && nivel5.getId() == -1) {
            Toast.makeText(this, "Error: Debe seleccionar un NIVEL 5", Toast.LENGTH_SHORT).show();
            cancel = true;
            return;
        }


        List<Department> departments = new DepartmentDao(getContentResolver()).list("codigo = '" + nivel2.getCodigo() + "'", null, null);
        if (departments.size() < 1) {
            //no existe department => lo creamos
            Department d = new Department("D;" + nivel2.getNombre() + ";0;0;" + nivel2.getCodigo());
            new DepartmentDao(getContentResolver()).save(d);
        }

        List<SubDepartment> subDepartments = new SubDepartmentDao(getContentResolver()).list("codigo = '" + nivel3.getCodigo() + "'", null, null);
        if (subDepartments.size() < 1) {
            //no existe subdepartment => lo creamos
            SubDepartment sd = new SubDepartment("S;" + nivel3.getNombre() + ";0;0;" + nivel3.getCodigo() + ";" + nivel3.getCodigo_padre());
            new SubDepartmentDao(getContentResolver()).save(sd);

            subDepartment = sd;
            department = sd.getDepartment();
        } else if (subDepartments.size() > 0) {
            subDepartment = subDepartments.get(0);
            department = subDepartments.get(0).getDepartment();
        } else {
            subDepartment = null;
            department = null;
        }


        if (department == null) {
            Toast.makeText(this
                    , "Error: No existen departamentos. Por favor, registre al menos un departamento."
                    , Toast.LENGTH_SHORT).show();
            cancel = true;
        } else if (subDepartment == null) {
            Toast.makeText(this
                    , "Error: No existen subdepartamentos. Por favor, registre al menos un departamento."
                    , Toast.LENGTH_SHORT).show();
            cancel = true;
        }

        //los ponemos en cero para que luego al parsearlo como double no de error de formato
        if (TextUtils.isEmpty(salePrice)) {
            salePrice = "0";
        }

        if (TextUtils.isEmpty(minQuantity)) {
            minQuantity = "0";
        }

        if (TextUtils.isEmpty(stockQuantity)) {
            stockQuantity = "0";
        }

        if (TextUtils.isEmpty(purchasePrice)) {
            purchasePrice = "0";
        }

        if (!cancel) {

            product.setGeneric(false);

            productTask = new ProductTask();
            productTask.execute((Void) null);
        }
    }

    public boolean unicidadSKU(Product product, String code) {
        if (TextUtils.isEmpty(code))
            return true;

        List<Product> check = new ProductDao(getContentResolver()).list(String.format(ProductTable.COLUMN_CODE + "='" + code + "' and _id <> %d and removed = 0", product.getId()), null, null);

        return check.size() == 0;
    }

    public void delete() {
        new ProductDao(getContentResolver()).delete(product);
        Toast.makeText(getApplicationContext(), R.string.message_delete_success_product, Toast.LENGTH_SHORT).show();
        onBackPressed();
    }

    @Override
    public void onStockEdited(HistMovStock histMovStock, double nuevoStock) {

        cambioStock = histMovStock;

        stockQuantity = Integer.toString((int) nuevoStock);
        stockQuantityEditText.setText(stockQuantity);
    }

    public class ProductTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {

            if (cambioStock != null)
                new HistMovStockDao(getContentResolver()).save(cambioStock);

            Log.d("ProductActivity", "add product");
            product.setName(name.toUpperCase());
            try {
                product.setAlta(DATE_FORMAT.parse(alta));
            } catch (ParseException e) {
                Log.e("ProductActivity", "Failed parsing alta", e);
            }
            product.setDescription(description.toUpperCase());
            product.setBrand(brand);
            product.setPresentation(presentation);
            product.setSalePrice(Formatos.ParseaDecimal(salePrice, FORMATO_DECIMAL));
            product.setPurchasePrice(Formatos.ParseaDecimal(purchasePrice, FORMATO_DECIMAL));
            product.setMinStock(Double.parseDouble(minQuantity));
            product.setCode(code);
            product.setStock(Double.parseDouble(stockQuantity));
            product.setUnit(unit);
            product.setWeighable(weighable);
            product.setQuickAccess(shorcut);
            product.setDepartment(department);
            product.setSubDepartment(subDepartment);
            product.setProvider(provider != null ? (provider.getId() != -1 ? provider : null) : null);
            product.setTax(tax);
            product.setIva(tax.getAmount());
            product.setContenido(Formatos.ParseaDecimal(contenido, Formatos.DecimalFormat_CH));
            product.setCodigoPadre(codigo_padre);

            Log.d("ProductActivity", "product added/updated");
            boolean saved = new ProductDao(getContentResolver()).saveOrUpdate(product);
            return saved;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            productTask = null;

            CharSequence text;
            if (success)
                text = getString(R.string.message_save_success_product);
            else
                text = getString(R.string.message_save_failed_product);
            Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT);
            toast.show();

            if (success)
                onBackPressed();
        }

        @Override
        protected void onCancelled() {
            productTask = null;
        }
    }

}

