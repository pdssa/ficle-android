package com.pds.ficle.gestprod.fragment;

import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.pds.common.Formatos;
import com.pds.common.model.Provider;
import com.pds.ficle.gestprod.R;
import com.pds.ficle.gestprod.activity.ProductActivity;
import com.pds.common.dao.ProductDao;
import com.pds.common.model.Product;

import java.text.SimpleDateFormat;
import java.util.List;

public class ProductListFragment extends ListFragment {
    private final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");

    private static final String ARG_PARAM1 = "search";

    ProductArrayAdapter listAdapter;
    List<Product> list;
    boolean searchScreen = false;
    private ProductListFragmentInteractionListener mListener;


    public static ProductListFragment newInstance(boolean _searchScreen) {
        ProductListFragment fragment = new ProductListFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_PARAM1, _searchScreen);
        fragment.setArguments(args);
        return fragment;
    }

    public ProductListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            searchScreen = getArguments().getBoolean(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        list = getList();
        listAdapter = new ProductArrayAdapter(getActivity(), list);
        setListAdapter(listAdapter);

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume(){
        super.onResume();

        list.clear();
        list.addAll(getList());
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        v.setBackgroundColor(getResources().getColor(R.color.light_blue));

        Product selectedProduct = (Product) getListAdapter().getItem(position);

        if(searchScreen){
            //es pantalla de busqueda => retornamos el item seleccionado
            if(this.mListener != null) {
                this.mListener.onProductSelected(selectedProduct);
            }
        }
        else {
            //comportamiento habitual
            Intent intent = new Intent(getActivity(), ProductActivity.class);
            intent.putExtra("product", selectedProduct);
            getActivity().startActivity(intent);
            getActivity().overridePendingTransition(0, 0);
        }



    }

    private List<Product> getList() {
        return new ProductDao(this.getActivity().getContentResolver()).list("removed = 0", null, null);
    }

    public class ProductArrayAdapter extends ArrayAdapter<String> {
        private final Context context;
        private final List<Product> products;

        public ProductArrayAdapter(Context context, List products) {
            super(context, R.layout.list_item_product, products);
            this.context = context;
            this.products = products;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View rowView = inflater.inflate(R.layout.list_item_product, parent, false);
            if (position % 2 != 0)
                rowView.setBackgroundColor(Color.LTGRAY);
            else
                rowView.setBackgroundColor(Color.WHITE);

            TextView nameTextView = (TextView) rowView.findViewById(R.id.list_item_product_name);
            nameTextView.setText(products.get(position).getName());

            TextView departmentTextView = (TextView) rowView.findViewById(R.id.list_item_product_department);
            departmentTextView.setText(products.get(position).getDepartment().getName());

            TextView subDepartmentTextView = (TextView) rowView.findViewById(R.id.list_item_product_sub_department);
            subDepartmentTextView.setText(products.get(position).getSubDepartment().getName());

            TextView providerTextView = (TextView) rowView.findViewById(R.id.list_item_product_provider);
            Provider prov = products.get(position).getProvider();
            providerTextView.setText(prov != null ? prov.getName() : "");

            TextView salePriceTextView = (TextView) rowView.findViewById(R.id.list_item_product_sale_price);
            salePriceTextView.setText(Formatos.FormateaDecimal(products.get(position).getSalePrice(), Formatos.DecimalFormat_CH));

            return rowView;
        }
    }


    public interface ProductListFragmentInteractionListener {
        public void onProductSelected(Product product);
    }

    public void setProductListFragmentInteractionListener(ProductListFragmentInteractionListener mListener) {
        this.mListener = mListener;
    }
}
