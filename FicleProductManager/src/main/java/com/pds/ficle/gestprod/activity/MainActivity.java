package com.pds.ficle.gestprod.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Usuario;
import com.pds.common.activity.TimerMainActivity;
import com.pds.ficle.gestprod.R;
import com.pds.ficle.gestprod.util.UserHolder;


public class MainActivity extends TimerMainActivity {

    private View departmentImage;
    private View subDepartmentImage;
    private View providerImage;
    private View productImage;
    private View codigoRapidoImage;
    private View ingresoCompraProd;
    private View ingresoCompraProv;
    private View consultarPedido;
//    private View consultarCombos;
    private Usuario loggedUser = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);

            setContentView(R.layout.activity_main);

            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                Object currentUser = extras.getSerializable("current_user");
                if (currentUser instanceof Usuario) {
                    loggedUser = (Usuario) currentUser;
                }
            }

            if (loggedUser == null) {
                Toast.makeText(this, R.string.unknown_user, Toast.LENGTH_LONG).show();
                this.finish();
                return;
            }

            ((TextView) findViewById(R.id.txtVersion)).setText("Version:" + getPackageManager().getPackageInfo(this.getPackageName().toString(), 0).versionName);


            //Log.i("MainActivity", String.format("logged user %s %s", loggedUser.getNombre(), loggedUser.getApellido()));

            UserHolder.getInstance().setUser(loggedUser);

            findViewById(R.id.prod_main_btn_back).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });

            departmentImage = findViewById(R.id.main_department_icon);
            departmentImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MainActivity.this, DepartmentManagerActivity.class);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                }
            });

            subDepartmentImage =  findViewById(R.id.main_sub_department_icon);
            subDepartmentImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MainActivity.this, SubDepartmentManagerActivity.class);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                }
            });

            providerImage = findViewById(R.id.main_provider_icon);
            providerImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MainActivity.this, ProviderManagerActivity.class);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                }
            });

            productImage = findViewById(R.id.main_product_icon);
            productImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MainActivity.this, ProductSearchActivity.class);
                    intent.putExtra("TypeResultSearch", ProductSearchActivity.TypeResultSearch.FULL_PRODUCT_EDIT);
                    intent.putExtra("current_user", loggedUser);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                }
            });

            codigoRapidoImage = findViewById(R.id.main_fast_code);
            codigoRapidoImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MainActivity.this, CodigosRapidosActivity.class);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                }
            });

            ingresoCompraProd =  findViewById(R.id.main_ingreso_compra_producto);
            ingresoCompraProd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MainActivity.this, ProductSearchActivity.class);
                    intent.putExtra("TypeResultSearch", ProductSearchActivity.TypeResultSearch.STOCK_PRODUCT_EDIT);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                }
            });

            ingresoCompraProv = findViewById(R.id.main_ingreso_compra_proveedor);
            ingresoCompraProv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MainActivity.this, CargaCompraActivity.class);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                }
            });

            consultarPedido = findViewById(R.id.main_consulta_pedidos);
            consultarPedido.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MainActivity.this, ListaPedidosActivity.class);
                    intent.putExtra("current_user", loggedUser);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                }
            });

          /*  consultarCombos = findViewById(R.id.main_combos_icon);
            consultarCombos.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MainActivity.this, ListaComboActivity.class);
                    intent.putExtra("current_user", loggedUser);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                }
            });*/

            //setTimer(new Config(this).INACTIVITY_TIME);

        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

}
