package com.pds.ficle.gestprod.activity;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Formato;
import com.pds.common.Usuario;
import com.pds.common.activity.TimerActivity;
import com.pds.common.dao.CategoriaDao;
import com.pds.common.dao.DepartmentDao;
import com.pds.common.dao.HistMovStockDao;
import com.pds.common.dao.ProductDao;
import com.pds.common.dao.ProviderDao;
import com.pds.common.dao.SubDepartmentDao;
import com.pds.common.dao.TaxDao;
import com.pds.common.db.ProductTable;
import com.pds.common.model.Categoria;
import com.pds.common.model.Department;
import com.pds.common.model.HistMovStock;
import com.pds.common.model.Product;
import com.pds.common.model.Provider;
import com.pds.common.model.SubDepartment;
import com.pds.common.model.Tax;
import com.pds.ficle.gestprod.R;
import com.pds.ficle.gestprod.fragment.EditPriceFragment;
import com.pds.ficle.gestprod.fragment.EditStockFragment;
import com.pds.ficle.gestprod.fragment.EditStockNewFragment;
import com.pds.ficle.gestprod.fragment.EditStockNewFragment2;
import com.pds.ficle.gestprod.util.SkuUtils;
import com.pds.ficle.gestprod.util.SpinnerUtils;
import com.pds.ficle.gestprod.util.ViewUtils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class NewProductActivity extends TimerActivity implements EditStockNewFragment2.OnEditStockInteractionListener, EditPriceFragment.OnEditPriceInteractionListener {

    private Product product;
    private Config config;
    //private String codigo;
    private Formato.Decimal_Format FORMATO_DECIMAL;
    private Usuario loggedUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_new);

        //cargamos el producto
        Intent intent = getIntent();
        product = intent.getParcelableExtra("product");
        loggedUser = (Usuario) intent.getSerializableExtra("current_user");

        if (product == null || loggedUser == null) {
            Toast.makeText(this, "Error en parametros recibidos", Toast.LENGTH_SHORT).show();
            finish();
        } else {
            //formato
            config = new Config(this);
            FORMATO_DECIMAL = Formato.getDecimal_Format(config.PAIS);

            try {
                LoadProduct(product);
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, "No se puede mostrar un producto fuera de catálogo", Toast.LENGTH_SHORT).show();
                finish();
            }

          /*  findViewById(R.id.product_btn_edit_stock).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    IniciarFragmentEditStock(product);
                }
            });*/

           /* findViewById(R.id.product_btn_edit_price).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    IniciarFragmentEditPrice(product);
                }
            });*/

          /*  findViewById(R.id.product_edit_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(NewProductActivity.this, ProductWizardActivity.class);
                    intent.putExtra("product", product);
                    intent.putExtra("current_user", loggedUser);
                    startActivityForResult(intent, 1);
                    overridePendingTransition(0, 0);
                }
            });*/

            findViewById(R.id.product_back_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //finish();
                    volver();
                }
            });

            findViewById(R.id.product_btn_print).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SkuUtils.ImprimirEtiqueta(NewProductActivity.this, config.PRINTER_MODEL, product.getCode(), product.toString(), "", false, "");
                }
            });

            findViewById(R.id.product_btn_print_precio).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String precio_decenas = "";

                    if (product.isWeighable()) {
                        if (product.getUnit().equalsIgnoreCase("kg") || product.getUnit().equalsIgnoreCase("gr"))
                            precio_decenas = String.format("Precio x 100gr: %s", Formato.FormateaDecimal(product.getSalePrice() / 10, FORMATO_DECIMAL, Formato.getCurrencySymbol(FORMATO_DECIMAL)));
                        if (product.getUnit().equalsIgnoreCase("lt") || product.getUnit().equalsIgnoreCase("ml"))
                            precio_decenas = String.format("Precio x 100ml: %s", Formato.FormateaDecimal(product.getSalePrice() / 10, FORMATO_DECIMAL, Formato.getCurrencySymbol(FORMATO_DECIMAL)));
                    }

                    SkuUtils.ImprimirEtiqueta(NewProductActivity.this, config.PRINTER_MODEL, product.getCode(), product.toString(), Formato.FormateaDecimal(product.getSalePrice(), FORMATO_DECIMAL, Formato.getCurrencySymbol(FORMATO_DECIMAL)), true, precio_decenas);
                }
            });

            findViewById(R.id.product_report_error_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        volver();
    }

    private void volver(){
        //forzamos indicar a donde queremos volver
        /*Intent intent = new Intent(NewProductActivity.this, ProductSearchActivity.class);
        intent.putExtra("TypeResultSearch", ProductSearchActivity.TypeResultSearch.FULL_PRODUCT_EDIT);
        intent.putExtra("current_user", loggedUser);
        startActivity(intent);
        overridePendingTransition(0, 0);*/
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == 1) {
            //volvemos de la edición del producto => lo actualizamos
            product = data.getParcelableExtra("edit_product");

            LoadProduct(product);
        }
    }

    private void LoadProduct(Product product) {
        //solo habilitamos la opcion de reportar inconvenientes para productos de catalogo
        /*if(product.getAutomatic() == Product.TYPE_AUTOM.PROD_ALTA_AUTOM){
            findViewById(R.id.product_report_error_button).setVisibility(View.VISIBLE);

        }*/

        //cargamos las entidades relacionadas
        product.LoadDepartmentSubdepartmentTax(getContentResolver());

        ((TextView) findViewById(R.id.product_lbl_name)).setText(product.getPrintingName());
        ((TextView) findViewById(R.id.product_lbl_nombre)).setText(product.getName());
        ((TextView) findViewById(R.id.product_lbl_depto)).setText(product.getDepartment() != null ? product.getDepartment().getName() : "");
        ((TextView) findViewById(R.id.product_lbl_subdepto)).setText(product.getSubDepartment() != null ? product.getSubDepartment().getName() : "");
        ((TextView) findViewById(R.id.product_lbl_marca)).setText(product.getBrand());
        ((TextView) findViewById(R.id.product_lbl_contenido)).setText(product.isWeighable() ? product.getUnit() : product.getContenidoFormatted() + product.getUnit());

        //((TextView) findViewById(R.id.product_lbl_desc_corta)).setText(TextUtils.isEmpty(product.getDescription()) ? "S/ Desc. Corta" : product.getDescription());
       /* ((TextView) findViewById(R.id.product_lbl_pesable)).setText(Html.fromHtml(
                (product.isWeighable() ? "PESABLE" : "NO PESABLE") + "<br/>" +
                        (product.getTax() != null ? product.getTax().getName().toUpperCase().replace("GRAVADO", "AFECTO") : "")
        ));*/

        ((TextView) findViewById(R.id.product_lbl_precio)).setText(Formato.FormateaDecimal(product.getSalePrice(), FORMATO_DECIMAL, Formato.getCurrencySymbol(FORMATO_DECIMAL)));
        ((TextView) findViewById(R.id.product_lbl_sku)).setText(product.getCode());

        ((TextView) findViewById(R.id.product_lbl_fast_code)).setText(TextUtils.isEmpty(product.getIspCode()) ? "Ninguno" : product.getIspCode());


        ((TextView) findViewById(R.id.product_lbl_precio_cpa)).setText(Formato.FormateaDecimal(product.getPurchasePrice(), FORMATO_DECIMAL, Formato.getCurrencySymbol(FORMATO_DECIMAL)));

        double margen = product.getPurchasePrice() != 0 ? ((product.getSalePrice() / product.getPurchasePrice()) - 1) * 100 : 100;
        if(product.getPurchasePrice() == 0 && product.getSalePrice() == 0 )
            margen = 0;

        if (margen > 1000)
            ((TextView) findViewById(R.id.product_lbl_margen)).setText("+1000 %");
        else
            ((TextView) findViewById(R.id.product_lbl_margen)).setText(Formato.FormateaDecimal(margen, Formato.Decimal_Format.CH) + " %");

        ((TextView) findViewById(R.id.product_lbl_stock)).setText(Formato.FormateaDecimal(product.getStock(), Formato.Decimal_Format.CH));
        ((TextView) findViewById(R.id.product_lbl_stock_min)).setText(Formato.FormateaDecimal(product.getMinStock(), Formato.Decimal_Format.CH));


    }

    public void IniciarFragmentEditStock(Product item_edit) {
        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.product_abm_frg_edit_stock) != null) {

            // Create a new Fragment to be placed in the activity layout
            EditStockNewFragment2 fragment = EditStockNewFragment2.newInstance(item_edit, loggedUser);

            fragment.setOnEditStockInteractionListener(this);

            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.product_abm_frg_edit_stock, fragment);
            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            ft.commit();
        }
    }

    public void IniciarFragmentEditPrice(Product item_edit) {
        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.product_abm_frg_edit_stock) != null) {

            // Create a new Fragment to be placed in the activity layout
            EditPriceFragment fragment = EditPriceFragment.newInstance(item_edit, FORMATO_DECIMAL);

            fragment.setOnEditPriceInteractionListener(this);

            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.product_abm_frg_edit_stock, fragment);
            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            ft.commit();
        }
    }

    @Override
    public void onStockChanged(Product _newProduct) {
        product = _newProduct;

        LoadProduct(product);
    }

    @Override
    public void onPriceChanged(Product _newProduct) {
        product = _newProduct;

        LoadProduct(product);
    }
}
