/*
package com.pds.ficle.gestprod.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.pds.common.Config;
import com.pds.common.Formatos;
import com.pds.common.activity.TimerActivity;
import com.pds.common.fragment.CombosListFragment;
import com.pds.common.model.Combo;
import com.pds.ficle.gestprod.R;

import java.text.DecimalFormat;

public class ListaComboActivity extends TimerActivity implements CombosListFragment.ComboListFragmentInteractionListener {

    private ListView listaCombos;
    private Button btnVolver;
    private Button btnNuevo;
    private DecimalFormat FORMATO_DECIMAL;
    private Config config;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_combo);

        InitViews();

        InitEvents();

        config = new Config(this);

        Init_CustomConfigPais(config.PAIS);
    }

    @Override
    protected void onResume() {
        super.onResume();

        CargarListaCombos();
    }

    private void Init_CustomConfigPais(String codePais) {
        FORMATO_DECIMAL = codePais.equals("AR") ? Formatos.DecimalFormat : Formatos.DecimalFormat_CH;
    }

    private void CargarListaCombos(){
        CombosListFragment combosListFragment = CombosListFragment.newInstance(FORMATO_DECIMAL, false);

        combosListFragment.setComboListFragmentInteractionListener(this);

        getFragmentManager().beginTransaction().replace(R.id.lista_combo_fragment_container, combosListFragment).commit();
    }

    private void InitViews() {
        btnVolver = (Button) findViewById(R.id.lista_combo_back_button);
        btnNuevo = (Button) findViewById(R.id.lista_combo_add_button);
    }

    private void InitEvents() {
        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(ListaComboActivity.this, CargaComboActivity.class);
                Intent intent = new Intent(ListaComboActivity.this, ComboWizardActivity.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });
    }

    @Override
    public void onComboSelected(Combo combo) {
        //Intent intent = new Intent(ListaComboActivity.this, CargaComboActivity.class);
        Intent intent = new Intent(ListaComboActivity.this, NewComboActivity.class);
        intent.putExtra("combo_edit", combo);
        startActivity(intent);
        overridePendingTransition(0, 0);
    }
}
*/
