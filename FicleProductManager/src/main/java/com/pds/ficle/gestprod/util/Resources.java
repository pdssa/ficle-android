package com.pds.ficle.gestprod.util;

import android.content.Context;

/**
 * Created by Hernan on 08/07/2015.
 */
public abstract class Resources {
    public static String getStringFromResource(Context context, String codePais, String idResource) {
        String sufijo = codePais.equals("AR") ? "_AR" : (codePais.equals("PE") ? "_PE" : "");
        //String pack = codePais.equals("AR") ? "strings_AR" : "strings";
        int resId = context.getResources().getIdentifier(idResource + sufijo, "string", context.getPackageName());
        return context.getString(resId);
    }
}
