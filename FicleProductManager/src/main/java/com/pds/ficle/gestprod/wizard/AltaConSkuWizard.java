package com.pds.ficle.gestprod.wizard;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Formato;
import com.pds.common.dao.DepartmentDao;
import com.pds.common.dao.ProductDao;
import com.pds.common.dao.SubDepartmentDao;
import com.pds.common.dao.TaxDao;
import com.pds.common.model.Department;
import com.pds.common.model.Product;
import com.pds.common.model.SubDepartment;
import com.pds.common.util.Sku_Utils;
import com.pds.common.util.Window;
import com.pds.ficle.gestprod.R;
import com.pds.ficle.gestprod.fragment.WizardStep;
import com.pds.ficle.gestprod.fragment.WizardStepChangeListener;
import com.pds.ficle.gestprod.util.SkuUtils;
import com.pds.ficle.gestprod.util.SpinnerUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Hernan on 05/12/2016.
 */
public class AltaConSkuWizard extends BaseAltaWizard {

    private WizardStep stepPesables;
    private boolean editMode;
    private boolean STEP_SKU = false;
    private Product product;

    public AltaConSkuWizard(Activity context, ProductWizardListener productWizardListener, Product p, Config config){
        super(context, productWizardListener, config);

        editMode = p != null;//recibimos un producto para editar?

        if (editMode) {
            product = p;
            //algunas acciones para el edit...
            //1. si falta alguna entidad relacionada, las cargamos
            if (product.getDepartment() == null || product.getSubDepartment() == null || product.getTax() == null)
                product.LoadDepartmentSubdepartmentTax(activity.getContentResolver());
        }

        WizardStep step1 = new WizardStep(R.string.prod_wiz_cod_barra, R.layout.product_wiz_step_cod_barra, stepCodBarrasChange);
        WizardStep step2 = new WizardStep(R.string.prod_wiz_marca, R.layout.product_wiz_step_marca, stepMarcaChange);
        WizardStep step3 = new WizardStep(R.string.prod_wiz_nombre, R.layout.product_wiz_step_nombre, stepNombreChange);
        WizardStep step4 = new WizardStep(R.string.prod_wiz_pesable, R.layout.product_wiz_step_pesable, stepPesableChange);

        WizardStep step411 = new WizardStep(R.string.prod_wiz_unidad, R.layout.product_wiz_step_unidad, stepUnidadChange);//, View.GONE);
        WizardStep step412 = new WizardStep(R.string.prod_wiz_contenido, R.layout.product_wiz_step_contenido, stepContenidoChange);

        stepPesables = new WizardStep(R.string.prod_wiz_unidad_pesable, R.layout.product_wiz_step_medida_pesable, stepMedidaPesableChange, View.GONE);

        WizardStep step5 = new WizardStep(R.string.prod_wiz_categoria, R.layout.product_wiz_step_categoria, stepCategoriaChange);
        WizardStep step6 = new WizardStep(R.string.prod_wiz_precio_vta, R.layout.product_wiz_step_precio, stepPrecioChange);
        WizardStep step7 = new WizardStep(R.string.prod_wiz_descripcion, R.layout.product_wiz_step_descripcion, stepDescriptionChange);
        //WizardStep step9 = new WizardStep(R.string.prod_wiz_stock_min, R.layout.product_wiz_step_stock_min, stepStockMinChange);
        WizardStep end = new WizardStep(R.string.prod_wiz_precio_valida, R.layout.product_wiz_step_validar, stepEndChange);

        if (editMode && product.getAutomatic() == Product.TYPE_AUTOM.PROD_ALTA_AUTOM) {
            //si estamos editando un producto generado por catalogo, reducimos las opciones a editar
            step6.flows(null, step7);
            step7.flows(step6, end);
            end.flows(step7, null);

            steps = new ArrayList<WizardStep>();
            steps.add(step6);
            steps.add(step7);
            steps.add(end);
        } else {
            step1.flows(null, step2);
            step2.flows(step1, step3);
            step3.flows(step2, step4);
            step4.flows(step3, step411);//va al 411 porque x def tenemos NO PESABLES

            step411.flows(step4, step412);
            step412.flows(step411, step5);

            stepPesables.flows(step4, step5);

            step5.flows(step4, step6);
            step6.flows(step5, step7);
            step7.flows(step6, end);
            end.flows(step7, null);

            steps = new ArrayList<WizardStep>();
            steps.add(step1);
            steps.add(step2);
            steps.add(step3);
            steps.add(step4);
            steps.add(step411);
            steps.add(step412);
            steps.add(stepPesables);
            steps.add(step5);
            steps.add(step6);
            steps.add(step7);
            steps.add(end);
        }
    }

    private WizardStepChangeListener stepCodBarrasChange = new WizardStepChangeListener() {
        private boolean segundoIntento = false;
        private WizardStep nextStep;

        private void skuStep(boolean t) {
            STEP_SKU = t;
        }

        @Override
        public void onStepBack() {
            //no hay paso anterior...
        }

        @Override
        public void onStepShow(final WizardStep _step) {
            skuStep(true);
            //MODO_ALTA_SIN_SKU = false;
            nextStep = _step.nextStep;

            //mostramos el codigo que teniamos
            if (product != null)
                ((EditText) findViewById(R.id.prod_wiz_step_cod_barra_txt_code)).setText(product.getCode());
            else
                product = new Product();//instanciamos un objeto producto

            /*findViewById(R.id.prod_wiz_step_cod_barra_btn_sin_sku).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //***producto sin SKU***
                    skuStep(false);
                    MODO_ALTA_SIN_SKU = true;
                    execWizStep(_step.nextStep);//llamamos al siguiente step..
                }
            });*/

            Window.FocusViewShowSoftKeyboard(activity, findViewById(R.id.prod_wiz_step_cod_barra_txt_code));

            Window.AddDoneKeyboardAction(((EditText) findViewById(R.id.prod_wiz_step_cod_barra_txt_code)), _step.doneAction());
        }

        @Override
        public boolean onStepNext() {
            //creamos el producto y tomamos el campo codigo

            EditText txtCode = ((EditText) findViewById(R.id.prod_wiz_step_cod_barra_txt_code));

            //validamos que sea obligatorio
            txtCode.setError(null);

            String code = txtCode.getText().toString().trim();

            if (TextUtils.isEmpty(code)) {
                txtCode.setError(getString(R.string.error_field_required));
                txtCode.requestFocus();
                return false;
            } else {

                Product prodConSKU = unicidadSKU(product, code);

                if (prodConSKU != null) {
                    //abrimos el producto
                    skuStep(false);

                    productWizardListener.openProduct(prodConSKU);

                    return false;
                } else {

                    //vamos a revisar si el codigo ingresado es valido o no
                    if (!Sku_Utils.ValidateSKU(code) && !segundoIntento) {

                        new AlertDialog.Builder(activity)
                                .setIcon(android.R.drawable.stat_sys_warning)
                                .setTitle("Codigo de Barras")
                                .setMessage(Html.fromHtml("El codigo ingresado no es válido, intente nuevamente por favor"))
                                .setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        segundoIntento = true;

                                        //limpiamos el campo
                                        ((EditText) findViewById(R.id.prod_wiz_step_cod_barra_txt_code)).setText("");
                                    }
                                })
                                .setCancelable(false)
                                .show();

                        return false;
                    } else if (!Sku_Utils.ValidateSKU(code) && segundoIntento) {

                        final String codeF = code;

                        new AlertDialog.Builder(activity)
                                .setIcon(android.R.drawable.stat_sys_warning)
                                .setTitle("Codigo de Barras")
                                .setMessage(Html.fromHtml("El codigo ingresado no es válido, continuar de todas formas?"))
                                .setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        product.setCode(codeF);
                                        skuStep(false);

                                        execWizStep(nextStep);//avanzamos al siguiente paso
                                    }
                                })
                                .setNegativeButton("CANCELAR", null)
                                .setCancelable(false)
                                .show();

                        return false;
                    } else {
                        product.setCode(code);
                        skuStep(false);
                        return true;
                    }
                }
            }

        }

    };

    private WizardStepChangeListener stepMarcaChange = new WizardStepChangeListener() {
        @Override
        public void onStepBack() {
            //limpiamos la marca que tenga el producto
            //product.setBrand("");
        }

        @Override
        public void onStepShow(WizardStep _step) {
            //mostramos la marca que teniamos
            if (product != null)
                ((EditText) findViewById(R.id.prod_wiz_step_marca_txt_marca)).setText(product.getBrand());

            ((TextView) findViewById(R.id.prod_wiz_step_marca_lbl_example)).setText(Html.fromHtml(getString(R.string.prod_wiz_marca_examp)));

            updateProductCreation(1, (TextView) findViewById(R.id.prod_wiz_step_marca_lbl_prdtemp), product);

            Window.FocusViewShowSoftKeyboard(activity, findViewById(R.id.prod_wiz_step_marca_txt_marca));

            Window.AddDoneKeyboardAction(((EditText) findViewById(R.id.prod_wiz_step_marca_txt_marca)), _step.doneAction());
        }

        @Override
        public boolean onStepNext() {
            //tomamos el campo marca ingresado

            EditText txtMarca = ((EditText) findViewById(R.id.prod_wiz_step_marca_txt_marca));

            //validamos que sea obligatorio
            txtMarca.setError(null);

            String marca = txtMarca.getText().toString().trim();

            if (TextUtils.isEmpty(marca)) {
                txtMarca.setError(getString(R.string.error_field_required));
                txtMarca.requestFocus();
                return false;
            } else {
                product.setBrand(marca);
                return true;
            }
        }
    };

    private WizardStepChangeListener stepNombreChange = new WizardStepChangeListener() {
        @Override
        public void onStepBack() {
        }

        @Override
        public void onStepShow(WizardStep _step) {
            //mostramos el nombre que teniamos
            if (product != null)
                ((EditText) findViewById(R.id.prod_wiz_step_nombre_txt_nombre)).setText(product.getName());

            ((TextView) findViewById(R.id.prod_wiz_step_nombre_lbl_example)).setText(Html.fromHtml(getString(R.string.prod_wiz_nombre_examp)));

            updateProductCreation(2, (TextView) findViewById(R.id.prod_wiz_step_nombre_lbl_prdtemp), product);

            Window.FocusViewShowSoftKeyboard(activity, findViewById(R.id.prod_wiz_step_nombre_txt_nombre));

            Window.AddDoneKeyboardAction(((EditText) findViewById(R.id.prod_wiz_step_nombre_txt_nombre)), _step.doneAction());

        }

        @Override
        public boolean onStepNext() {
            //tomamos el campo nombre ingresado

            EditText txtNombre = ((EditText) findViewById(R.id.prod_wiz_step_nombre_txt_nombre));

            String nombre = txtNombre.getText().toString().trim();

            //validamos que sea obligatorio
            txtNombre.setError(null);

            if (TextUtils.isEmpty(nombre)) {
                txtNombre.setError(getString(R.string.error_field_required));
                txtNombre.requestFocus();
                return false;
            } else {
                product.setName(nombre);
                return true;
            }

        }
    };

    private WizardStepChangeListener stepPesableChange = new WizardStepChangeListener() {

        @Override
        public void onStepBack() {
        }

        @Override
        public void onStepShow(WizardStep _step) {
            //mostramos los datos que teniamos
            if (product != null && product.getTax() != null) {
                //seleccionamos la opcion de pesable
                int tipo_pesable = product.isWeighable() ? R.id.prod_wiz_step_pesable_rbt_pesable : R.id.prod_wiz_step_pesable_rbt_no_pesable;

                ((RadioButton) findViewById(R.id.prod_wiz_step_pesable_grp_pesable).findViewById(tipo_pesable)).setChecked(true);

                //seleccionamos la opcion de tax
                if (product.getTax() != null) {
                    if (product.getTax().getName().toUpperCase().equalsIgnoreCase("Gravado"))
                        ((RadioButton) findViewById(R.id.prod_wiz_step_pesable_grp_iva).findViewById(R.id.prod_wiz_step_pesable_rbt_afecto)).setChecked(true);

                    else if (product.getTax().getName().toUpperCase().equalsIgnoreCase("Exento"))
                        ((RadioButton) findViewById(R.id.prod_wiz_step_pesable_grp_iva).findViewById(R.id.prod_wiz_step_pesable_rbt_no_afecto)).setChecked(true);
                }
            }

            //cerramos el teclado
            Window.CloseSoftKeyboard(activity);

            //asignamos los eventos a las opciones
            /*Button btnNoPesable = ((Button) findViewById(R.id.prod_wiz_step_pesable_btn_no_pesable));
            Button btnPesable = ((Button) findViewById(R.id.prod_wiz_step_pesable_btn_pesable));

            btnNoPesable.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PesableTypeSelected(false);
                }
            });
            btnPesable.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PesableTypeSelected(true);
                }
            });*/

            updateProductCreation(3, (TextView) findViewById(R.id.prod_wiz_step_pesable_lbl_prdtemp), product);

        }

        @Override
        public boolean onStepNext() {

            int tipo_pesable = ((RadioGroup) findViewById(R.id.prod_wiz_step_pesable_grp_pesable)).getCheckedRadioButtonId();
            int tipo_iva = ((RadioGroup) findViewById(R.id.prod_wiz_step_pesable_grp_iva)).getCheckedRadioButtonId();

            //revisamos la opcion de pesable seleccionada
            switch (tipo_pesable) {
                case R.id.prod_wiz_step_pesable_rbt_pesable: {
                    product.setWeighable(true);
                }
                break;
                case R.id.prod_wiz_step_pesable_rbt_no_pesable: {
                    product.setWeighable(false);
                }
                break;
                default: {
                    //default NO PESABLE
                    product.setWeighable(false);

                    //Toast.makeText(ProductWizardActivity.this, "Error: Debe seleccionar una opcion: PESABLE / NO PESABLE", Toast.LENGTH_SHORT).show();
                    //return false;
                }
            }

            //revisamos la opcion de IVA seleccionada
            switch (tipo_iva) {
                case R.id.prod_wiz_step_pesable_rbt_afecto: {
                    product.setIvaEnabled(true);
                    product.setTax(new TaxDao(activity.getContentResolver()).list("name = 'Gravado'", null, null).get(0));
                    product.setIva(product.getTax().getAmount());
                }
                break;
                case R.id.prod_wiz_step_pesable_rbt_no_afecto: {
                    product.setIvaEnabled(true);
                    product.setTax(new TaxDao(activity.getContentResolver()).list("name = 'Exento'", null, null).get(0));
                    product.setIva(product.getTax().getAmount());
                }
                break;
                default: {
                    //default AFECTO
                    product.setIvaEnabled(true);
                    product.setTax(new TaxDao(activity.getContentResolver()).list("name = 'Gravado'", null, null).get(0));
                    product.setIva(product.getTax().getAmount());

                    //Toast.makeText(ProductWizardActivity.this, "Error: Debe seleccionar una opcion: AFECTO / EXENTO", Toast.LENGTH_SHORT).show();
                    //return false;
                }
            }

            if (!product.isWeighable())
                return true; //vamos al paso siguiente
            else {
                //tenemos que ir al paso para productos pesables
                execWizStep(stepPesables);
                return false;
            }
        }



        /*private void PesableTypeSelected(boolean pesable) {
            //asignamos la opcion
            product.setWeighable(pesable);

            execWizStep(this.step.nextStep);//llamamos al siguiente step..
        }*/
    };

    private WizardStepChangeListener stepPrecioChange = new WizardStepChangeListener() {


        class PrecioWatcher implements TextWatcher {
            public String label;


            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String salePrice = s.toString().trim();

                double precio_por_decena = 0;

                if (!TextUtils.isEmpty(s))
                    precio_por_decena = Formato.ParseaDecimal(salePrice, getDecimalFormat(), true) / 10;
                else
                    precio_por_decena = 0;

                //mostramos el precio por gr / ml
                ((TextView) findViewById(R.id.prod_wiz_step_precio_lbl_precio_decena)).setText(String.format("%s: %s", label, Formato.FormateaDecimal(precio_por_decena, getDecimalFormat(), Formato.getCurrencySymbol(getDecimalFormat()))));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        }

        private PrecioWatcher precioChanged = new PrecioWatcher();

        private void asignarWatcher(boolean asignar, String label) {
            ((EditText) findViewById(R.id.prod_wiz_step_precio_txt_precio)).removeTextChangedListener(precioChanged);

            if (asignar) {
                precioChanged.label = label;
                ((EditText) findViewById(R.id.prod_wiz_step_precio_txt_precio)).addTextChangedListener(precioChanged);
            }
        }

        @Override
        public void onStepBack() {
        }

        @Override
        public void onStepShow(WizardStep _step) {
            if (product.isWeighable()) {
                //si es pesable, pedimos precio por KILO o por LITRO
                //TODO: check unit in gs1
                if (product.getUnit().equalsIgnoreCase("kg") || product.getUnit().equalsIgnoreCase("gr")) {
                    ((TextView) findViewById(R.id.prod_wiz_step_precio_lbl_precio)).setText("Precio por KILOGRAMO:");

                    asignarWatcher(true, "Precio por 100 gr");
                } else if (product.getUnit().equalsIgnoreCase("lt") || product.getUnit().equalsIgnoreCase("ml") || product.getUnit().equalsIgnoreCase("cc")) {
                    ((TextView) findViewById(R.id.prod_wiz_step_precio_lbl_precio)).setText("Precio por LITRO:");

                    asignarWatcher(true, "Precio por 100 ml");
                } else {
                    ((TextView) findViewById(R.id.prod_wiz_step_precio_lbl_precio)).setText(String.format("Precio por %s:", product.getUnit().toUpperCase()));

                    asignarWatcher(false, "");
                }

            } else {
                ((TextView) findViewById(R.id.prod_wiz_step_precio_lbl_precio)).setText("Precio por UNIDAD:");

                asignarWatcher(false, "");
            }


            //mostramos el precio de venta que teniamos
            if (product != null && product.getSalePrice() != 0)
                ((EditText) findViewById(R.id.prod_wiz_step_precio_txt_precio)).setText(Formato.FormateaDecimal(product.getSalePrice(), getDecimalFormat()));

            Window.FocusViewShowSoftKeyboard(activity, findViewById(R.id.prod_wiz_step_precio_txt_precio));

            Window.AddDoneKeyboardAction(((EditText) findViewById(R.id.prod_wiz_step_precio_txt_precio)), _step.doneAction());

            updateProductCreation(6, (TextView) findViewById(R.id.prod_wiz_step_precio_lbl_prdtemp), product);


        }

        @Override
        public boolean onStepNext() {

            //tomamos el campo precio venta ingresado

            EditText txtPrecioVta = ((EditText) findViewById(R.id.prod_wiz_step_precio_txt_precio));

            //validamos que sea obligatorio
            //txtPrecioVta.setError(null);

            String salePrice = txtPrecioVta.getText().toString().trim();

            if (TextUtils.isEmpty(salePrice)) {
                //txtPrecioVta.setError(getString(R.string.error_field_required));
                //txtPrecioVta.requestFocus();

                //ahora es opcional
                product.setSalePrice(0);

                return true;
            } else {
                product.setSalePrice(Formato.ParseaDecimal(salePrice, getDecimalFormat(), true));

                return true;
            }
        }
    };

    private WizardStepChangeListener stepDescriptionChange = new WizardStepChangeListener() {
        @Override
        public void onStepBack() {
        }

        @Override
        public void onStepShow(WizardStep _step) {
            boolean propuesto = false;

            //mostramos la descripcion que teniamos
            if (product != null) {
                if (!TextUtils.isEmpty(product.getDescription()))
                    ((EditText) findViewById(R.id.prod_wiz_step_descripcion_txt_descripcion)).setText(product.getDescription());
                else {
                    //armamos un nombre corto formado por la marca + nombre + contenido + unidad
                    ((EditText) findViewById(R.id.prod_wiz_step_descripcion_txt_descripcion)).setText(product.buildShortName());

                    propuesto = true;
                }
            }
            updateProductCreation(7, (TextView) findViewById(R.id.prod_wiz_step_descripcion_lbl_prdtemp), product);

            Window.FocusViewShowSoftKeyboard(activity, findViewById(R.id.prod_wiz_step_descripcion_txt_descripcion));

            Window.AddDoneKeyboardAction(((EditText) findViewById(R.id.prod_wiz_step_descripcion_txt_descripcion)), _step.doneAction());

            if (propuesto) {
                //((EditText) findViewById(R.id.prod_wiz_step_descripcion_txt_descripcion)).setSelection(0);
                ((EditText) findViewById(R.id.prod_wiz_step_descripcion_txt_descripcion)).selectAll();
            }
        }

        @Override
        public boolean onStepNext() {
            //tomamos el campo descripcion ingresado

            EditText txtDescript = ((EditText) findViewById(R.id.prod_wiz_step_descripcion_txt_descripcion));

            String descrip = txtDescript.getText().toString().trim();

            //validamos que sea obligatorio
            txtDescript.setError(null);

            if (TextUtils.isEmpty(descrip)) {
                txtDescript.setError(getString(R.string.error_field_required));
                txtDescript.requestFocus();
                return false;
            } else {
                product.setDescription(descrip);
                return true;
            }

        }
    };

    private WizardStepChangeListener stepUnidadChange = new WizardStepChangeListener() {
        private WizardStep _step;

        private String _unitProd;

        @Override
        public void onStepBack() {
        }

        @Override
        public void onStepShow(WizardStep __step) {

            if (product != null)
                _unitProd = product.getUnit();

            _step = __step;

            findViewById(R.id.prod_wiz_step_contenido_btn_u_gr).setOnClickListener(new UnitClickListener("gr"));
            findViewById(R.id.prod_wiz_step_contenido_btn_u_kg).setOnClickListener(new UnitClickListener("kg"));
            findViewById(R.id.prod_wiz_step_contenido_btn_u_lt).setOnClickListener(new UnitClickListener("lt"));
            findViewById(R.id.prod_wiz_step_contenido_btn_u_ml).setOnClickListener(new UnitClickListener("ml"));
            findViewById(R.id.prod_wiz_step_contenido_btn_u_un).setOnClickListener(new UnitClickListener("un"));

            ((TextView) findViewById(R.id.prod_wiz_step_contenido_lbl_example)).setText(Html.fromHtml(getString(R.string.prod_wiz_contenido_examp)));

            updateProductCreation(4, (TextView) findViewById(R.id.prod_wiz_step_contenido_lbl_prdtemp), product);

            Window.CloseSoftKeyboard(activity);

        }

        @Override
        public boolean onStepNext() {
            //por default unidad especial (asi el sigte paso es omitido)
            if (TextUtils.isEmpty(_unitProd))
                _unitProd = "_un_";

            product.setUnit(_unitProd);

            return true;
        }

        private void assignUnit(String unit) {
            _unitProd = unit;

            _step.nextButton.performClick();

            //updateLabelContenido();
        }

        /*private void updateLabelContenido() {

            //unidad
            String contenido = ((EditText) findViewById(R.id.prod_wiz_step_contenido_txt_contenido)).getText().toString().trim();
            String lbl = String.format("%s%s", contenido, product.getUnit());

            ((TextView) findViewById(R.id.prod_wiz_step_contenido_lbl_contenido)).setText(lbl);
        }*/

        class UnitClickListener implements View.OnClickListener {

            private String unit;

            public UnitClickListener(String _unit) {
                this.unit = _unit;
            }

            @Override
            public void onClick(View v) {
                assignUnit(unit);
            }
        }
    };

    private WizardStepChangeListener stepContenidoChange = new WizardStepChangeListener() {
        @Override
        public void onStepBack() {
        }

        @Override
        public void onStepShow(WizardStep _step) {
            //mostramos el contenido que teniamos
            if (product != null) {
                if (product.getUnit().equalsIgnoreCase("_un_")) {
                    //por default una unidad
                    product.setUnit("un");
                    product.setContenido(1);
                    //saltamos al sgte paso
                    execWizStep(_step.nextStep);

                    return;
                } else if (product.getContenido() != 0) {
                    ((EditText) findViewById(R.id.prod_wiz_step_contenido_txt_contenido)).setText(product.getContenidoFormatted());
                }
            }

            ((TextView) findViewById(R.id.prod_wiz_step_contenido_lbl_contenido)).setText(String.format("Ingrese cantidad de %s:", product.getUnit().toUpperCase()));

            /*
            ((EditText) findViewById(R.id.prod_wiz_step_contenido_txt_contenido)).addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    updateLabelContenido();
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            */

            ((TextView) findViewById(R.id.prod_wiz_step_contenido_lbl_example)).setText(Html.fromHtml(getString(R.string.prod_wiz_contenido_examp)));

            updateProductCreation(4, (TextView) findViewById(R.id.prod_wiz_step_contenido_lbl_prdtemp), product);

            Window.FocusViewShowSoftKeyboard(activity, findViewById(R.id.prod_wiz_step_contenido_txt_contenido));

            Window.AddDoneKeyboardAction(((EditText) findViewById(R.id.prod_wiz_step_contenido_txt_contenido)), _step.doneAction());

        }

        @Override
        public boolean onStepNext() {

            //tomamos el campo contenido ingresado

            EditText txtContenido = ((EditText) findViewById(R.id.prod_wiz_step_contenido_txt_contenido));

            //validamos que sea obligatorio
            txtContenido.setError(null);

            String contenido = txtContenido.getText().toString().trim();

            if (TextUtils.isEmpty(contenido)) {
                //por default, contenido en 1
                product.setContenido(1);

                return true;

                //txtContenido.setError(getString(R.string.error_field_required));
                //txtContenido.requestFocus();
                //return false;
            } else {
                product.setContenido(Formato.ParseaDecimal(contenido, Formato.Decimal_Format.US));

                return true;
            }
        }

        /*private void assignUnit(String unit) {
            product.setUnit(unit);

            updateLabelContenido();
        }*/

        /*private void updateLabelContenido() {

            //unidad
            String contenido = ((EditText) findViewById(R.id.prod_wiz_step_contenido_txt_contenido)).getText().toString().trim();
            String lbl = String.format("%s%s", contenido, product.getUnit());

            ((TextView) findViewById(R.id.prod_wiz_step_contenido_lbl_contenido)).setText(lbl);
        }*/


    };

    private WizardStepChangeListener stepMedidaPesableChange = new WizardStepChangeListener() {
        private WizardStep _step;

        private String _unitProd;

        @Override
        public void onStepBack() {
        }

        @Override
        public void onStepShow(WizardStep __step) {

            if (product != null)
                _unitProd = product.getUnit();

            _step = __step;

            findViewById(R.id.prod_wiz_step_contenido_btn_u_kg).setOnClickListener(new UnitClickListener("gr"));
            findViewById(R.id.prod_wiz_step_contenido_btn_u_lt).setOnClickListener(new UnitClickListener("lt"));

            ((TextView) findViewById(R.id.prod_wiz_step_contenido_lbl_example)).setText(Html.fromHtml(getString(R.string.prod_wiz_contenido_examp)));

            updateProductCreation(4, (TextView) findViewById(R.id.prod_wiz_step_contenido_lbl_prdtemp), product);

            Window.CloseSoftKeyboard(activity);

        }

        @Override
        public boolean onStepNext() {
            product.setUnit(_unitProd);
            product.setContenido(0);//solo interesa la unidad

            return true;
        }

        private void assignUnit(String unit) {
            _unitProd = unit;

            _step.nextButton.performClick();

            //updateLabelContenido();
        }

        class UnitClickListener implements View.OnClickListener {

            private String unit;

            public UnitClickListener(String _unit) {
                this.unit = _unit;
            }

            @Override
            public void onClick(View v) {
                assignUnit(unit);
            }
        }
    };

    private WizardStepChangeListener stepCategoriaChange = new WizardStepChangeListener() {
        private List<Department> departmentList;
        private List<SubDepartment> subDepartmentList;

        private ArrayAdapter<Department> departmentArrayAdapter;
        private ArrayAdapter<SubDepartment> subDepartmentArrayAdapter;

        @Override
        public void onStepBack() {
        }

        @Override
        public void onStepShow(final WizardStep _step) {

            //cerramos el teclado
            Window.CloseSoftKeyboard(activity);

            //cargamos los combos de depto y subdepto
            departmentList = new ArrayList<Department>();
            subDepartmentList = new ArrayList<SubDepartment>();

            departmentList = getListDepartments(); //traemos TODOS los departamentos
            //inicializamos adapter y spinner de departamentos
            departmentArrayAdapter = new ArrayAdapter<Department>(activity, android.R.layout.simple_spinner_item, departmentList);
            departmentArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            ((Spinner) findViewById(R.id.product_department)).setAdapter(departmentArrayAdapter);

            //seleccionamos el depto/subdepto que teniamos
            Long idDepartment = -1l, idSubDepartment = -1l;

            if (product != null) {

                if (editMode) {
                    idDepartment = product.getIdDepartment();
                    idSubDepartment = product.getIdSubdepartment();
                } else {
                    if (product.getDepartment() != null)
                        idDepartment = product.getDepartment().getId();

                    if (product.getSubDepartment() != null)
                        idSubDepartment = product.getSubDepartment().getId();
                }
            }


            if (idDepartment != -1) {//seleccionamos el departamento del producto
                selectDepartmentById(idDepartment.intValue());
            }


            //si hay un departamento seleccionado => traemos sus subdepartamentos
            if (((Spinner) findViewById(R.id.product_department)).getSelectedItem() != null) {
                subDepartmentList = getListSubDepartments(((Department) ((Spinner) findViewById(R.id.product_department)).getSelectedItem()).getId()); //no traemos subdeptos
            } else
                subDepartmentList = getListSubDepartments(-1); //no traemos subdeptos

            //inicializamos adapter y spinner de subdepartamentos
            subDepartmentArrayAdapter = new ArrayAdapter<SubDepartment>(activity, android.R.layout.simple_spinner_item, subDepartmentList);
            subDepartmentArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            ((Spinner) findViewById(R.id.product_sub_department)).setAdapter(subDepartmentArrayAdapter);

            if (idSubDepartment != -1) { //seleccionamos el subdepartamento del producto editado
                selectSubDepartmentById(idSubDepartment.intValue());
            }

            final Long idDepartmentF = idDepartment;
            final Long idSubdepartmentF = idSubDepartment;

            //evento de seleccion de departamento
            ((Spinner) findViewById(R.id.product_department)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Department d = (Department) parent.getItemAtPosition(position);

                    //traemos los subdepartamentos del departamento seleccionado
                    subDepartmentList.clear();
                    subDepartmentList.addAll(getListSubDepartments(d.getId()));
                    subDepartmentArrayAdapter.notifyDataSetChanged();

                    if (idDepartmentF.intValue() != d.getId())
                        ((Spinner) findViewById(R.id.product_sub_department)).setSelection(0);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

            //evento de seleccion de subdepartamento
            ((Spinner) findViewById(R.id.product_sub_department)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    SubDepartment s = (SubDepartment) parent.getItemAtPosition(position);

                    //saltamos al sgte paso
                    if (idSubdepartmentF.intValue() != s.getId() && s.getId() != -1) {
                        _step.nextButton.performClick();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

            updateProductCreation(5, (TextView) findViewById(R.id.prod_wiz_step_categoria_lbl_prdtemp), product);

        }

        @Override
        public boolean onStepNext() {
            //tomamos el campo contenido ingresado

            Department department = (Department) ((Spinner) findViewById(R.id.product_department)).getSelectedItem();
            SubDepartment subDepartment = (SubDepartment) ((Spinner) findViewById(R.id.product_sub_department)).getSelectedItem();

            //validamos que sea obligatorio
            if (department == null || department.getId() == -1) {
                Toast.makeText(activity, "Error: Debe seleccionar un departamento", Toast.LENGTH_SHORT).show();
                return false;
            } else if (subDepartment == null || subDepartment.getId() == -1) {
                Toast.makeText(activity, "Error: Debe seleccionar un subdepartamento", Toast.LENGTH_SHORT).show();
                return false;
            } else {
                product.setDepartment(department);
                product.setSubDepartment(subDepartment);
                product.setCodigoPadre(subDepartment.getCodigo());

                return true;
            }
        }


        private List<Department> getListDepartments() {
            //no mostramos deptos genericos, asi no pueden asociarles productos
            List<Department> lst = (new DepartmentDao(activity.getContentResolver())).list("generic = 0", null, "name ASC");
            lst.add(0, new Department(-1, "Seleccione..."));
            return lst;
        }

        private List<SubDepartment> getListSubDepartments(long idDepartment) {
            //no mostramos subdeptos genericos, asi no pueden asociarles productos
            List<SubDepartment> lst = null;
            if (idDepartment != -1) {
                lst = (new SubDepartmentDao(activity.getContentResolver())).list(String.format("department_id = %d and generic = 0", idDepartment), null, "name ASC");
                lst.add(0, new SubDepartment(-1, "Seleccione..."));
            } else {
                lst = new ArrayList<SubDepartment>();
                lst.add(0, new SubDepartment(-1, "Seleccione un DEPARTAMENTO..."));
            }

            return lst;
        }

        private void selectDepartmentById(final int idDepartment) {
            int index = SpinnerUtils.getIndexForElement(((Spinner) findViewById(R.id.product_department)), new SpinnerUtils.Predicate<Department>() {
                @Override
                public boolean evaluate(Department element) {
                    return idDepartment == element.getId();
                }
            });
            ((Spinner) findViewById(R.id.product_department)).setSelection(index);
        }

        private void selectSubDepartmentById(final int idSubDepartment) {
            int index = SpinnerUtils.getIndexForElement(((Spinner) findViewById(R.id.product_sub_department)), new SpinnerUtils.Predicate<SubDepartment>() {
                @Override
                public boolean evaluate(SubDepartment element) {
                    return idSubDepartment == element.getId();
                }
            });
            ((Spinner) findViewById(R.id.product_sub_department)).setSelection(index);
        }
    };

    private WizardStepChangeListener stepEndChange = new WizardStepChangeListener() {
        @Override
        public void onStepBack() {
        }

        @Override
        public void onStepShow(WizardStep _step) {
            //cerramos el teclado
            Window.CloseSoftKeyboard(activity);

            if (TextUtils.isEmpty(product.getCode())) {
                //calculamos un SKU en base al codigo del comercio
                product.setCode(SkuUtils.GenerateSKUv2(getConfig().ID_COMERCIO, product, new ProductDao(activity.getContentResolver())));
                //calculamos un SKU en base a la categoria del producto
                //product.setCode(SkuUtils.GenerateSku(product, new ProductDao(getContentResolver())));
            }

            final String precio_formateado = Formato.FormateaDecimal(product.getSalePrice(), getDecimalFormat(), Formato.getCurrencySymbol(getDecimalFormat()));
            String precio_decenas = "";

            if (product.isWeighable()) {
                if (product.getUnit().equalsIgnoreCase("kg") || product.getUnit().equalsIgnoreCase("gr"))
                    precio_decenas = String.format("Precio x 100gr: %s", Formato.FormateaDecimal(product.getSalePrice() / 10, getDecimalFormat(), Formato.getCurrencySymbol(getDecimalFormat())));
                if (product.getUnit().equalsIgnoreCase("lt") || product.getUnit().equalsIgnoreCase("ml"))
                    precio_decenas = String.format("Precio x 100ml: %s", Formato.FormateaDecimal(product.getSalePrice() / 10, getDecimalFormat(), Formato.getCurrencySymbol(getDecimalFormat())));
            }


            final String precio_decenas_f = precio_decenas;

            findViewById(R.id.prod_wiz_step_valida_btn_print).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SkuUtils.ImprimirEtiqueta(activity, getConfig().PRINTER_MODEL, product.getCode(), product.toString(), "", false, "");
                }
            });

            findViewById(R.id.prod_wiz_step_valida_btn_print_precio).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SkuUtils.ImprimirEtiqueta(activity, getConfig().PRINTER_MODEL, product.getCode(), product.toString(), precio_formateado, true, precio_decenas_f);
                }
            });


            //mostramos los datos del producto
            if (product != null) {
                ((TextView) findViewById(R.id.prod_wiz_step_valida_lbl_nombre)).setText(product.getName());
                ((TextView) findViewById(R.id.prod_wiz_step_valida_lbl_desc_corta)).setText(product.getPrintingName());
                ((TextView) findViewById(R.id.prod_wiz_step_valida_lbl_precio)).setText(precio_formateado);
                ((Button) findViewById(R.id.prod_wiz_step_valida_btn_pesable)).setText(Html.fromHtml(
                        (product.isWeighable() ? "PESABLE" : "NO PESABLE") + "<br/>" +
                                product.getTax().getName().toUpperCase().replace("GRAVADO", "AFECTO")
                ));
                ((TextView) findViewById(R.id.prod_wiz_step_valida_lbl_sku)).setText(product.getCode());
                ((TextView) findViewById(R.id.prod_wiz_step_valida_lbl_depto)).setText(product.getDepartment().getName());
                ((TextView) findViewById(R.id.prod_wiz_step_valida_lbl_subdepto)).setText(product.getSubDepartment().getName());
                ((TextView) findViewById(R.id.prod_wiz_step_valida_lbl_marca)).setText(product.getBrand());
                ((TextView) findViewById(R.id.prod_wiz_step_valida_lbl_contenido)).setText(product.isWeighable() ? product.getUnit() : product.getContenidoFormatted() + product.getUnit());

            }

        }

        @Override
        public boolean onStepNext() {

            if (!editMode) {
                //colocamos algunos datos de inicializacion

                product.setAlta(new Date());
                product.setStock(0);
                product.setMinStock(0);
                product.setPurchasePrice(0);

                //ajustamos el tipo de producto
                product.setAutomatic(Product.TYPE_AUTOM.PROD_ALTA_MANUAL);

            }

            return productWizardListener.saveProduct(editMode, product);

        }
    };

    @Override
    public void inputByScannerSkuStep(String input) {
        //si está activado el step SKU
        if (STEP_SKU) {
            ((EditText) findViewById(R.id.prod_wiz_step_cod_barra_txt_code)).setText(input);
            nextStep();
        }
    }
}
