package com.pds.ficle.gestprod.fragment;

import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.ficle.gestprod.R;
import com.pds.ficle.gestprod.activity.DepartmentActivity;
import com.pds.common.dao.DepartmentDao;
import com.pds.common.model.Department;

import java.text.SimpleDateFormat;
import java.util.List;

public class DepartmentListFragment extends ListFragment {
    private final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");

    DepartmentArrayAdapter listAdapter;
    List<Department> list;

    public DepartmentListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        list = getList();
        listAdapter = new DepartmentArrayAdapter(getActivity(), list);
        setListAdapter(listAdapter);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        v.setBackgroundColor(getResources().getColor(R.color.light_blue));

        Department selectedDepartment = (Department) getListAdapter().getItem(position);

        Intent intent = new Intent(getActivity(), DepartmentActivity.class);
        intent.putExtra("department", selectedDepartment);
        getActivity().startActivity(intent);
        getActivity().overridePendingTransition(0, 0);
    }

    @Override
    public void onResume(){
        super.onResume();

        list.clear();
        list.addAll(getList());
        listAdapter.notifyDataSetChanged();
    }

    private List<Department> getList() {
        return new DepartmentDao(this.getActivity().getContentResolver()).list();
    }

    public class DepartmentArrayAdapter extends ArrayAdapter<String> {
        private final Context context;
        private List<Department> departments;

        public DepartmentArrayAdapter(Context context, List departments) {
            super(context, R.layout.list_item_department, departments);
            this.context = context;
            this.departments = departments;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View rowView = inflater.inflate(R.layout.list_item_department, parent, false);
            if (position % 2 != 0)
                rowView.setBackgroundColor(Color.LTGRAY);
            else
                rowView.setBackgroundColor(Color.WHITE);

            TextView nameTextView = (TextView) rowView.findViewById(R.id.list_item_department_name);
            nameTextView.setText(departments.get(position).getName());

            TextView descriptionTextView = (TextView) rowView.findViewById(R.id.list_item_department_description);
            descriptionTextView.setText(departments.get(position).getDescription());

//            ImageView editImageView = (ImageView) rowView.findViewById(R.id.list_item_department_edit);
//            editImageView.setOnClickListener(new EditOnClickListener(departments.get(position)));
//
//            ImageView deleteImageView = (ImageView) rowView.findViewById(R.id.list_item_department_delete);
//            deleteImageView.setOnClickListener(new DeleteOnClickListener(departments.get(position)));

            return rowView;
        }

//        @Override
//        public void notifyDataSetChanged() {
//
//        }
    }

    public class EditOnClickListener implements View.OnClickListener {
        private Department department;

        public EditOnClickListener(Department department) {
            this.department = department;
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(getActivity(), DepartmentActivity.class);
            intent.putExtra("name", department.getName());
            getActivity().startActivity(intent);
            getActivity().overridePendingTransition(0, 0);
        }
    }

    public class DeleteOnClickListener implements View.OnClickListener {
        private Department department;

        public DeleteOnClickListener(Department department) {
            this.department = department;
        }

        @Override
        public void onClick(View view) {
            Toast.makeText(getActivity(), "TODO: Implement delete method.", Toast.LENGTH_SHORT).show();
        }
    }
}
