package com.pds.ficle.gestprod.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Formatos;
import com.pds.common.dao.CompraDao;
import com.pds.common.dao.CompraDetalleDao;
import com.pds.common.dao.ProductDao;
import com.pds.common.db.ProductTable;
import com.pds.common.model.Compra;
import com.pds.common.model.CompraDetalle;
import com.pds.common.model.Product;
import com.pds.ficle.gestprod.R;
import com.pds.ficle.gestprod.activity.ProductSearchActivity;
import com.pds.ficle.gestprod.util.ViewUtils;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AddCompraItemFragment.FcItemFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AddCompraItemFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddCompraItemFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "item_edit";
    private static final String ARG_PARAM2 = "formato";
    private static final String ARG_PARAM3 = "formato_secundario";
    private static final String ARG_PARAM4 = "product_add";


    private CompraDetalle itemEdit;
    private Product productAdd;

    private FcItemFragmentInteractionListener mListener;
    private Button btnGrabar;
    private Button btnCancelar;
    private EditText txtCantidad;
    //private EditText txtCodigo;
    private EditText txtDescripcion;
    private EditText txtNombre;
    private TextView txtPrecio;
    private TextView txtPrecioVta;
    private TextView txtStockActual;
    private TextView txtMargen;
    //private EditText txtSubtotal;
    //private ImageButton btnFindProd;
    private View seccion_ultimo_pedido;
    private boolean flagItemEdit = false;
    private Product productEdit;

    private DecimalFormat FORMATO;
    private DecimalFormat FORMATO_SECUNDARIO;

    private Timer timer;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param itemEdit item a editar.
     * @return A new instance of fragment AddFcItemFragment.
     */
    public static AddCompraItemFragment newInstance(Product productAdd, CompraDetalle itemEdit, DecimalFormat _formato, DecimalFormat _formato_secund) {
        AddCompraItemFragment fragment = new AddCompraItemFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, itemEdit);
        args.putSerializable(ARG_PARAM2, _formato);
        args.putSerializable(ARG_PARAM3, _formato_secund);
        args.putParcelable(ARG_PARAM4, productAdd);
        fragment.setArguments(args);
        return fragment;
    }

    public AddCompraItemFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            productAdd = getArguments().getParcelable(ARG_PARAM4);
            itemEdit = getArguments().getParcelable(ARG_PARAM1);
            if (itemEdit != null) {
                productEdit = itemEdit.getProducto();
                flagItemEdit = true;
            }
            FORMATO = (DecimalFormat) getArguments().getSerializable(ARG_PARAM2);
            FORMATO_SECUNDARIO = (DecimalFormat) getArguments().getSerializable(ARG_PARAM3);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_fc_item, container, false);

        Init_Views(view);

        Init_Eventos(view);

        if (itemEdit != null) {
            CargarDatos(
                    itemEdit.getProducto().getCode(),
                    itemEdit.getProducto().getName(),
                    itemEdit.getProducto().getDescription(),
                    itemEdit.getCantidad(),
                    itemEdit.getPrecio(),
                    itemEdit.getPrecio_vta() != 0 ? itemEdit.getPrecio_vta() : itemEdit.getProducto().getSalePrice(),
                    itemEdit.getTotal() - itemEdit.getSubtotal_impuesto(),
                    itemEdit.getProducto().getStock()
            );
        } else if (productAdd != null) {
            SelectProduct(productAdd);
        }

        //posamos el foco sobre el codigo, pero quitamos el teclado, para que puedan ingresar el codigo via scanner
        /*txtCodigo.requestFocus();
        ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(txtCodigo.getWindowToken(), 0);*/

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 2) {//requerimos un product
            if (resultCode == Activity.RESULT_OK) {
                Object result = data.getParcelableExtra("result");
                SelectProduct((Product) result);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                //SelectProvider(null);
            }
        }
    }

    private void CargarDatos(String codigo, String nombre, String descripcion, int cantidad, double precio, double precio_vta, double subtotal, double stock_actual) {
        try {
            txtCantidad.setText(cantidad != 0 ? String.valueOf(cantidad) : null);
            //txtCodigo.setText(codigo);
            txtNombre.setText(nombre);
            txtDescripcion.setText(descripcion);
            txtPrecio.setText(precio != 0 ? Formatos.FormateaDecimal(precio, FORMATO, "$") : null);
            txtPrecioVta.setText(precio_vta != 0 ? Formatos.FormateaDecimal(precio_vta, FORMATO, "$") : null);
            //txtSubtotal.setText(subtotal != 0 ? Formatos.FormateaDecimal(subtotal, FORMATO) : null);
            txtStockActual.setText(String.valueOf(Formatos.FormateaDecimal(stock_actual, Formatos.DecimalFormat_CH)));

        } catch (Exception ex) {
            Log.e("cargarDatos:", "Error");
        }
    }

    private void MostrarDatosUltimaCompra(Product p) {

        //obtenemos la ultima compra donde se pidio este producto
        CompraDetalle ult_item = new CompraDetalleDao(getActivity().getContentResolver()).first(String.format("id_producto = %d", p.getId()), null, "id_compra DESC");

        if (ult_item != null) {
            txtPrecio.setText(ult_item.getPrecio() != 0 ? Formatos.FormateaDecimal(ult_item.getPrecio(), FORMATO_SECUNDARIO, "$") : null);
        }

        /*seccion_ultimo_pedido.setVisibility(View.VISIBLE);

        if (ult_item == null) {//puede no haberse comprado nunca
            ((TextView) seccion_ultimo_pedido.findViewById(R.id.frg_add_item_ult_cpa_f_txt)).setText("NO REGISTRA");
            ((TextView) seccion_ultimo_pedido.findViewById(R.id.frg_add_item_ult_cpa_q_txt)).setText("0");
            ((TextView) seccion_ultimo_pedido.findViewById(R.id.frg_add_item_ult_cpa_p_txt)).setText("0");
        } else {

            Compra ult_compra = new CompraDao(getActivity().getContentResolver()).find(ult_item.getIdCompra());

            ((TextView) seccion_ultimo_pedido.findViewById(R.id.frg_add_item_ult_cpa_f_txt)).setText(String.format("# %d - %s", ult_compra.getId(), Formatos.FormateaDate(ult_compra.getFecha(), Formatos.DateFormatSlash)));
            ((TextView) seccion_ultimo_pedido.findViewById(R.id.frg_add_item_ult_cpa_q_txt)).setText(String.valueOf(ult_item.getCantidad()));
            ((TextView) seccion_ultimo_pedido.findViewById(R.id.frg_add_item_ult_cpa_p_txt)).setText(Formatos.FormateaDecimal(ult_item.getPrecio(), FORMATO_SECUNDARIO, "$"));
        }*/

    }


    public void SelectProduct(Product _product) {
        try {
            if (_product != null) {
                productEdit = _product;
                txtNombre.setError(null);

                if (!flagItemEdit) {
                    CargarDatos(
                            _product.getCode(),
                            _product.getName(),
                            _product.getDescription(),
                            0,
                            _product.getPurchasePrice(),//pasamos precio compra en cero, se carga desde la ultima compra
                            _product.getSalePrice(),
                            0,
                            _product.getStock()
                    );

                } else {
                    CargarDatos(
                            _product.getCode(),
                            _product.getName(),
                            _product.getDescription(),
                            itemEdit.getCantidad(),
                            itemEdit.getPrecio(),
                            itemEdit.getPrecio_vta() != 0 ? itemEdit.getPrecio_vta() : _product.getSalePrice(),
                            itemEdit.getSubtotal_neto_grav(),
                            _product.getStock());
                }

                //posamos el foco sobre cantidad
                txtCantidad.requestFocus();
                ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

            } else {
                //TODO: product not found
                productEdit = null;
                txtNombre.setText(null);
                txtDescripcion.setText(null);
                txtPrecio.setText(null);
                txtPrecioVta.setText(null);
                txtNombre.setError("Producto no existente");
            }

        } catch (Exception ex) {
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    private void Init_Views(View view) {
        btnGrabar = (Button) view.findViewById(R.id.frg_add_item_btn_grabar);
        btnCancelar = (Button) view.findViewById(R.id.frg_add_item_btn_cancelar);
        txtCantidad = (EditText) view.findViewById(R.id.frg_add_item_txt_cantidad);
        //txtCodigo = (EditText) view.findViewById(R.id.frg_add_item_txt_codigo);
        txtDescripcion = (EditText) view.findViewById(R.id.frg_add_item_txt_descripcion);
        txtNombre = (EditText) view.findViewById(R.id.frg_add_item_txt_nombre);
        txtPrecio = (TextView) view.findViewById(R.id.frg_add_item_txt_precio);
        txtPrecioVta = (TextView) view.findViewById(R.id.frg_add_item_txt_precio_vta);
        //txtSubtotal = (EditText) view.findViewById(R.id.frg_add_item_txt_subtotal);
        //btnFindProd = (ImageButton) view.findViewById(R.id.fc_add_item_btn_find_prod);
        //seccion_ultimo_pedido = view.findViewById(R.id.frg_add_item_ultima_compra);
        txtStockActual = (TextView) view.findViewById(R.id.frg_add_item_stock_quantity);
        txtMargen = (TextView) view.findViewById(R.id.frg_add_item_txt_margen);

        //definimos las "zonas touch" del activity
        ViewUtils.DefineViewGroup(view, txtCantidad.getId(), R.id.frg_add_item_lbl_cantidad);
        ViewUtils.DefineViewGroup(view, txtPrecio.getId(), R.id.frg_add_item_lbl_precio);
        ViewUtils.DefineViewGroup(view, txtPrecioVta.getId(), R.id.frg_add_item_lbl_precio_vta);
        ViewUtils.DefineViewGroup(view, txtMargen.getId(), R.id.frg_add_item_lbl_margen);
    }

    private void Init_Eventos(View view) {
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cerrar();
            }
        });
        btnGrabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GrabarItem();
            }
        });
        view.findViewById(R.id.img_dummy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cerrar();
            }
        });
        view.findViewById(R.id.frg_add_item_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        /*btnFindProd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ProductSearchActivity.class);
                intent.putExtra("TypeResultSearch", ProductSearchActivity.TypeResultSearch.SEARCH_PRODUCT_BACK);
                startActivityForResult(intent, 2);
            }
        });
        txtCodigo.setFilters(new InputFilter[]{new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++)
                    if (!Character.isLetter(source.charAt(i)) &&
                            !Character.isSpaceChar(source.charAt(i)) &&
                            !Character.isDigit(source.charAt(i))
                            ) //solo aceptamos: letras, numeros y espacio
                        return "";

                return null;
            }
        }});*/
        /*txtCodigo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus && itemEdit==null){
                //esto es para que, en el caso de estar editando un item, no pierda la info en edicion, al salir del foco del codigo ,dado que vuelve a blanquear los campos

                    if(!TextUtils.isEmpty(((EditText) view).getText())) {
                        if (ObtenerProductoByCode(((EditText) view).getText().toString()))
                            txtPrecio.requestFocus();
                        else
                            SelectProduct(null);//no encontramos code
                    }
                }
            }
        });*/
        /*
        txtCodigo.addTextChangedListener(new TextWatcher() {

            private final long DELAY = 500; // in ms

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (timer == null)
                    timer = new Timer();
                else {
                    timer.cancel();
                    timer = new Timer();
                }

                final Editable _s = s;

                if (s.length() > 0) {
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (ObtenerProductoByCode(_s.toString()))
                                        txtPrecio.requestFocus();
                                    else
                                        SelectProduct(null);//no encontramos code

                                    timer.cancel();
                                }
                            });
                        }
                    }, DELAY);
                } //else ((ImageView) findViewById(R.id.prod_search_btn_find)).setImageResource(R.drawable.lupa);
            }
        });
        txtCantidad.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s) && !TextUtils.isEmpty(txtPrecio.getText().toString())) {
                    double cantidad = Formatos.ParseaDecimal(s.toString(), FORMATO);
                    double precio = Formatos.ParseaDecimal(txtPrecio.getText().toString(), FORMATO);

                    double subtotal = cantidad * precio;

                    txtSubtotal.setText(Formatos.FormateaDecimal(subtotal, FORMATO));
                } else
                    txtSubtotal.setText(null);

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });*/

        //final boolean mutual_edit = false; //con esta variable controlamos no entrar en un loop mutuo entre change margen y change precio venta

        ChangeMargenWatcher change_vta = new ChangeMargenWatcher(ChangeMargenWatcher.TYPE_VENTA);
        //ChangeMargenWatcher change_mgn = new ChangeMargenWatcher(ChangeMargenWatcher.TYPE_MARGEN);
        ChangeMargenWatcher change_cpa = new ChangeMargenWatcher(ChangeMargenWatcher.TYPE_COMPRA);

        change_vta.setListener(change_vta);
        ///change_mgn.setListener(change_vta);
        change_cpa.setListener(change_vta);

///FIXME:  HZ Start
   /*     txtPrecio.addTextChangedListener(change_cpa);
        txtPrecio.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                }
            }
        });*/

     /*   txtPrecioVta.addTextChangedListener(change_vta);
        txtPrecioVta.setOnFocusChangeListener(txtPrecio.getOnFocusChangeListener());*/
///FIXME:  HZ End


        //txtMargen.addTextChangedListener(change_mgn);
        //txtMargen.setOnFocusChangeListener(txtMargen.getOnFocusChangeListener());

        /*
        txtCantidad.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(!hasFocus){
                    if(!TextUtils.isEmpty(txtCantidad.getText().toString()) && !TextUtils.isEmpty(txtPrecio.getText().toString())){
                        double cantidad = Formatos.ParseaDecimal(txtCantidad.getText().toString(), Formatos.DecimalFormat_CH);
                        double precio = Formatos.ParseaDecimal(txtPrecio.getText().toString(), Formatos.DecimalFormat_CH);

                        double subtotal = cantidad * precio;

                        txtSubtotal.setText(Formatos.FormateaDecimal(subtotal, Formatos.DecimalFormat_CH));
                    }
                }

            }
        });*/
    }

    class ChangeMargenWatcher implements TextWatcher {

        public static final int TYPE_COMPRA = 0;
        public static final int TYPE_VENTA = 1;
        public static final int TYPE_MARGEN = 2;

        private int type;
        private ChangeMargenWatcher listener;

        public void setListener(ChangeMargenWatcher listener) {
            this.listener = listener;
        }

        public ChangeMargenWatcher(int type) {
            this.type = type;
        }


        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            /*if (type == TYPE_COMPRA) {
                if (!TextUtils.isEmpty(s)) {
                    //si cambiamos el precio de compra, actualizamos el precio de venta segun el margen fijado
                    double precio_cpa = getDouble(s.toString());

                    double margen = getDouble(txtMargen.getText().toString());

                    double precio_vta = precio_cpa * (1 + (margen / 100));

                    txtPrecioVta.removeTextChangedListener(listener);
                    txtPrecioVta.setText(Formatos.FormateaDecimal(precio_vta, FORMATO, "$"));
                    txtPrecioVta.addTextChangedListener(listener);

                    //double precio_vta = Formatos.ParseaDecimal(!TextUtils.isEmpty(txtPrecioVta.getText().toString()) ? txtPrecioVta.getText().toString() : "0", FORMATO);

                    //txtMargen.setText(Formatos.FormateaDecimal(precio_vta - precio_cpa, FORMATO) + " %");
                }
            }*/
            if (type == TYPE_COMPRA) {

                if (!TextUtils.isEmpty(s)) {
                    //si cambiamos el precio de compra, actualizamos el margen
                    double precio_cpa = getDouble(s.toString());

                    double precio_vta = getDouble(txtPrecioVta.getText().toString());

                    double margen = precio_cpa != 0 ? ((precio_vta / precio_cpa) - 1) * 100 : 100;

                    txtMargen.removeTextChangedListener(listener);
                    txtMargen.setText(Formatos.FormateaDecimal(margen, FORMATO) + " %");
                    txtMargen.addTextChangedListener(listener);

                    /*txtPrecioVta.removeTextChangedListener(listener);
                    txtPrecioVta.setText(Formatos.FormateaDecimal(precio_vta, FORMATO, "$"));
                    txtPrecioVta.addTextChangedListener(listener);*/

                    //double precio_vta = Formatos.ParseaDecimal(!TextUtils.isEmpty(txtPrecioVta.getText().toString()) ? txtPrecioVta.getText().toString() : "0", FORMATO);

                    //txtMargen.setText(Formatos.FormateaDecimal(precio_vta - precio_cpa, FORMATO) + " %");
                }
            } else if (type == TYPE_VENTA) {

                if (!TextUtils.isEmpty(s)) {
                    //si cambiamos el precio de venta, actualizamos el margen
                    double precio_vta = getDouble(s.toString());

                    double precio_cpa = getDouble(txtPrecio.getText().toString());

                    double margen = precio_cpa != 0 ? ((precio_vta / precio_cpa) - 1) * 100 : 100;

                    txtMargen.removeTextChangedListener(listener);
                    txtMargen.setText(Formatos.FormateaDecimal(margen, FORMATO) + " %");
                    txtMargen.addTextChangedListener(listener);
                }

            } else if (type == TYPE_MARGEN) {

                if (!TextUtils.isEmpty(s)) {
                    //si cambiamos el margen, modificamos el precio de venta
                    double margen = getDouble(s.toString());

                    double precio_cpa = getDouble(txtPrecio.getText().toString());

                    double precio_vta = precio_cpa * (1 + (margen / 100));

                    txtPrecioVta.removeTextChangedListener(listener);
                    txtPrecioVta.setText(Formatos.FormateaDecimal(precio_vta, FORMATO, "$"));
                    txtPrecioVta.addTextChangedListener(listener);

                    //txtMargen.setText(Formatos.FormateaDecimal(margen, FORMATO) + " %");
                }
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
        }


    }

    private double getDouble(String value) {
        if (TextUtils.isEmpty(value))
            return 0;
        else
            return Formatos.ParseaDecimal(value.replace(Formatos.CurrencySymbol, "").replace("%", "").replace(Formatos.CurrencySymbol_PE, "").trim(), FORMATO);
    }

    private boolean ObtenerProductoByCode(String code) {
        try {

            boolean pudoLeer = true;

            List<Product> productos = new ProductDao(getActivity().getContentResolver()).list(ProductTable.COLUMN_CODE + " = '" + code + "' AND removed = 0", null, null);

            if (productos.size() > 0) {
                // en el caso de haber mas de uno, nos quedamos solo con el primero
                Product p = productos.get(0);
                SelectProduct(p);

            } else {
                pudoLeer = false;
            }

            return pudoLeer;

        } catch (Exception ex) {
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
            return false;
        }
    }


    private void Cerrar() {
        if (getActivity().getCurrentFocus() != null)
            ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        //getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        FragmentManager fm = getActivity().getFragmentManager();

        Fragment fragment = fm.findFragmentById(R.id.frgAddEditItem);

        FragmentTransaction ft = fm.beginTransaction();

        ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);

        ft.hide(fragment);

        ft.commit();
    }

    public void GrabarItem() {
        try {

            txtNombre.setError(null);

            if (productEdit == null) {
                txtNombre.setError("Producto obligatorio");
                return;
            }

            boolean newItem = itemEdit == null;

            if (newItem)
                itemEdit = new CompraDetalle();

            int cantidad = TextUtils.isEmpty(txtCantidad.getText()) ? 0 : Integer.parseInt(txtCantidad.getText().toString());
            double precio = getDouble(txtPrecio.getText().toString());
            double precio_vta = getDouble(txtPrecioVta.getText().toString());//TextUtils.isEmpty(txtPrecioVta.getText()) ? 0 : Formatos.ParseaDecimal(txtPrecioVta.getText().toString(), FORMATO);

            itemEdit.setIdProducto(productEdit.getId());
            itemEdit.setProducto(productEdit);
            itemEdit.setCantidad(cantidad);
            itemEdit.setPrecio(precio);
            itemEdit.setPrecio_vta(precio_vta);

            double subtotal = itemEdit.getPrecio() * itemEdit.getCantidad();
            itemEdit.setSubtotal_exento(0);
            itemEdit.setSubtotal_neto_grav(0);
            itemEdit.setSubtotal_impuesto(0);

            if (productEdit.getIdTax() == 4) //EXENTO
                itemEdit.setSubtotal_exento(subtotal);
            else //GRAVADO, NO GRAVADO, INCLUIDO
            {
                double iva = productEdit.getIva() / 100;

                itemEdit.setSubtotal_neto_grav(subtotal);
                itemEdit.setSubtotal_impuesto(subtotal * iva);
            }

            itemEdit.setTotal(itemEdit.getSubtotal_exento() + itemEdit.getSubtotal_neto_grav() + itemEdit.getSubtotal_impuesto());

            if (mListener != null) {
                if (newItem)
                    mListener.onItemCreated(itemEdit);
                else
                    mListener.onItemEdited(itemEdit);
            }

            Cerrar();

        } catch (Exception ex) {
            Toast.makeText(AddCompraItemFragment.this.getActivity(), "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (FcItemFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement FcItemFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        if (timer != null)
            timer.cancel();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface FcItemFragmentInteractionListener {
        public void onItemCreated(CompraDetalle new_item);

        public void onItemEdited(CompraDetalle edited_item);
    }

    public void setFcItemFragmentInteractionListener(FcItemFragmentInteractionListener mListener) {
        this.mListener = mListener;
    }
}
