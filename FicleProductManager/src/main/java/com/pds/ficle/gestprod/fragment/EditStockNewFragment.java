package com.pds.ficle.gestprod.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Formato;
import com.pds.common.Usuario;
import com.pds.common.dao.HistMovStockDao;
import com.pds.common.dao.ProductDao;
import com.pds.common.model.HistMovStock;
import com.pds.common.model.Product;
import com.pds.common.util.Window;
import com.pds.ficle.gestprod.R;

import org.apache.http.conn.ssl.StrictHostnameVerifier;

import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EditStockNewFragment.OnEditStockInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EditStockNewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditStockNewFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "product";
    private static final String ARG_PARAM2 = "user";

    private Product product;
    private Usuario user;

    private OnEditStockInteractionListener mListener;
    private Button btnGrabar;
    private Button btnCancelar;
    private TextView txtStockActual, txtNuevoStock;
    private EditText txtCantidad, txtObservaciones;
    private ImageButton btnAjusteIngreso, btnAjusteEgreso;

    public void setOnEditStockInteractionListener(OnEditStockInteractionListener mListener) {
        this.mListener = mListener;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param _product Producto a editar.
     * @return A new instance of fragment EditStockFragment.
     */
    public static EditStockNewFragment newInstance(Product _product, Usuario _user) {
        EditStockNewFragment fragment = new EditStockNewFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, _product);
        args.putSerializable(ARG_PARAM2, _user);
        fragment.setArguments(args);
        return fragment;
    }

    public EditStockNewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            product = getArguments().getParcelable(ARG_PARAM1);
            user = (Usuario) getArguments().getSerializable(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_new_stock, container, false);

        Init_Views(view);

        Init_Eventos(view);

        if (product != null) {
            //txtCantidad.setText("0");
            txtStockActual.setText(Formato.FormateaDecimal(product.getStock(), Formato.Decimal_Format.CH));
        }

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        //abrir teclado
        Window.FocusViewShowSoftKeyboard(getActivity(), txtCantidad);
    }

    private void Init_Views(View view) {
        btnGrabar = (Button) view.findViewById(R.id.frg_edit_stock_btn_aceptar);
        btnCancelar = (Button) view.findViewById(R.id.frg_edit_stock_btn_cancelar);
        txtCantidad = (EditText) view.findViewById(R.id.frg_edit_stock_cantidad);
        txtStockActual = (TextView) view.findViewById(R.id.frg_edit_stock_actual);
        txtNuevoStock = (TextView) view.findViewById(R.id.frg_edit_stock_result);
        txtObservaciones = (EditText) view.findViewById(R.id.frg_edit_stock_observaciones);

        btnAjusteEgreso = (ImageButton) view.findViewById(R.id.frg_edit_stock_btn_egr);
        btnAjusteIngreso = (ImageButton) view.findViewById(R.id.frg_edit_stock_btn_ing);

    }

    private void Init_Eventos(View view) {
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cerrar();
            }
        });
        btnGrabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CerrarItem();
            }
        });
        view.findViewById(R.id.img_dummy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cerrar();
            }
        });
        view.findViewById(R.id.frg_edit_stock_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        /*txtCantidad.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ActualizarResultado();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });*/

        btnAjusteIngreso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActualizarResultado("I");
            }
        });
        btnAjusteEgreso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActualizarResultado("E");
            }
        });
    }

    private Double Resultado;
    private Double Cantidad;
    private String operacion;

    private void ActualizarResultado(String tipoAjuste){
        operacion = tipoAjuste;
        String s = txtCantidad.getText().toString().trim();
        if (!TextUtils.isEmpty(s)) {
            Cantidad = Double.parseDouble(s);

            if(tipoAjuste.equals("I"))
                Resultado = product.getStock() + Cantidad;
            else if(tipoAjuste.equals("E"))
                Resultado = product.getStock() - Cantidad;
            else
                Resultado = product.getStock(); //hasta q no selecione el modo, no variamos el resultado
        } else
            Resultado = product.getStock();

        txtNuevoStock.setText(Formato.FormateaDecimal(Resultado, Formato.Decimal_Format.CH));
    }

    public void CerrarItem() {

        //clear errors
        txtCantidad.setError(null);

        String cantidad = txtCantidad.getText().toString().trim();

        if(TextUtils.isEmpty(operacion)) {
            txtCantidad.setError("Debe seleccionar un tipo de ajuste: - / +");
        }
        else if(TextUtils.isEmpty(cantidad) || !TextUtils.isDigitsOnly(cantidad)){
            txtCantidad.setError("Cantidad ingresada no valida");
        }
        else {

            //actualizamos el stock del producto y grabamos
            product.setStock(Resultado);

            boolean saved = new ProductDao(getActivity().getContentResolver()).saveOrUpdate(product);

            if(saved) {

                HistMovStock stk = new HistMovStock();
                stk.setFecha(new Date());
                stk.setUsuario(user.getLogin());
                stk.setObservacion(txtObservaciones.getText().toString());
                stk.setStock(Resultado.intValue());
                stk.setCantidad(Cantidad.intValue());
                stk.setMonto(0);
                stk.setOperacion(operacion);
                stk.setProductoid(product.getId());

                new HistMovStockDao(getActivity().getContentResolver()).save(stk);

                if (mListener != null) {
                    mListener.onStockChanged(product);
                }

                Cerrar();

            }
            else{
                Toast.makeText(getActivity(), "No se pudo actualizar el producto",Toast.LENGTH_SHORT).show();
            }


        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnEditStockInteractionListener) activity;

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " debe implementar OnEditStockInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void Cerrar() {
        //cerramos el teclado
        Window.CloseSoftKeyboard(getActivity());

        FragmentManager fm = getActivity().getFragmentManager();

        Fragment fragment = fm.findFragmentById(R.id.product_abm_frg_edit_stock);

        FragmentTransaction ft = fm.beginTransaction();

        ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);

        ft.hide(fragment);

        ft.commit();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnEditStockInteractionListener {
        public void onStockChanged(Product product);
    }

}
