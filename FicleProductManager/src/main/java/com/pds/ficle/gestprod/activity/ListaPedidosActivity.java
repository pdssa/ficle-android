package com.pds.ficle.gestprod.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Formatos;
import com.pds.common.Usuario;
import com.pds.common.activity.TimerActivity;
import com.pds.common.dao.PedidoDao;
import com.pds.common.dao.ProviderDao;
import com.pds.common.db.PedidoTable;
import com.pds.common.db.ProviderTable;
import com.pds.common.model.Pedido;
import com.pds.common.model.Provider;
import com.pds.ficle.gestprod.R;

import java.util.List;

/**
 * Created by EMEKA on 14/01/2015.
 */
public class ListaPedidosActivity extends TimerActivity {

    private ListView listaPedidos;
    private Button btnVolver;
    private Button btnNuevo;
    private Usuario _user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_pedidos);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            //tomamos el id de usuario
            //_id_user_login = extras.getInt("_id_user_login");
            this._user = (Usuario) extras.getSerializable("current_user");
        }

        //no tenemos user logueado...volvemos a EntryPoint
        if (_user == null) {
            Toast.makeText(this, R.string.unknown_user, Toast.LENGTH_LONG).show();
            this.finish();
        } else {

            InitViews();

            InitEvents();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        cargarListaDePedidos();
    }

    private void InitViews() {
        listaPedidos = (ListView) findViewById(R.id.pedidos_list_ped);
        listaPedidos.setEmptyView(findViewById(android.R.id.empty));
        btnVolver = (Button) findViewById(R.id.lista_pedidos_back_button);
        btnNuevo = (Button) findViewById(R.id.lista_pedidos_add_button);
    }

    private void InitEvents() {
        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ListaPedidosActivity.this, CargaPedidoActivity.class);
                intent.putExtra("TypeResultSearch", ProductSearchActivity.TypeResultSearch.STOCK_PRODUCT_EDIT);
                intent.putExtra("current_user", _user);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });

        listaPedidos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Pedido pedido = (Pedido) listaPedidos.getItemAtPosition(position);
                Intent intent = new Intent(ListaPedidosActivity.this, CargaPedidoActivity.class);
                intent.putExtra("id_pedido", pedido.getId());
                intent.putExtra("current_user", _user);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });
    }

    private void cargarListaDePedidos() {
        List<Pedido> list = (new PedidoDao(getContentResolver())).list(null, null, PedidoTable.PEDIDO_FECHA + " DESC");
        ListaPedidosAdapter adapter = new ListaPedidosAdapter(this, list);
        listaPedidos.setAdapter(adapter);
    }

    //Creo la clase Adapter para cargar la lista de pedidos
    class ListaPedidosAdapter extends ArrayAdapter<Pedido> {
        private Context context;
        private List<Pedido> datos;

        public ListaPedidosAdapter(Context context, List<Pedido> datos) {
            super(context, android.R.layout.simple_list_item_1, datos);

            this.context = context;
            this.datos = datos;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // En primer lugar "inflamos" una nueva vista, que será la que se
            // mostrará en la celda del ListView. Para ello primero creamos el
            // inflater, y después inflamos la vista.
            if (convertView == null) {
                convertView = LayoutInflater.from(context)
                        .inflate(R.layout.list_item_pedido, parent, false);
            }

            Pedido row = datos.get(position);

            Provider p = ObtieneProviderPorId(row.getProviderid());

            // A partir de la vista, recogeremos los controles que contiene para
            // poder manipularlos.
            // Recogemos los TextView para mostrar datos
            ((TextView) convertView.findViewById(R.id.list_item_pedidos_nro)).setText(String.valueOf(row.getId()));
            ((TextView) convertView.findViewById(R.id.list_item_pedidos_proveedor)).setText(p.getName());
            ((TextView) convertView.findViewById(R.id.list_item_pedidos_fecha)).setText(Formatos.FormateaDate(row.getFecha(), Formatos.FullDateTimeFormatNoSeconds));
            ((TextView) convertView.findViewById(R.id.list_item_pedidos_estado)).setText(row.getEstadoDescripcion());

            if (row.getEstado().startsWith("A"))
                ((TextView) convertView.findViewById(R.id.list_item_pedidos_estado)).setTextColor(Color.GREEN);
            else if (row.getEstado().startsWith("E"))
                ((TextView) convertView.findViewById(R.id.list_item_pedidos_estado)).setTextColor(Color.YELLOW);
                else
                ((TextView) convertView.findViewById(R.id.list_item_pedidos_estado)).setTextColor(Color.RED);

            return convertView;
        }
    }



    private Provider ObtieneProviderPorId(Long providerid) {
        Provider provider = null;

        try {
            List<Provider> providers = new ProviderDao(getContentResolver()).list(ProviderTable.COLUMN_ID + " = " + String.valueOf(providerid), null, null);

            if (providers.size() > 0) {
                provider = providers.get(0);
            }

        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }

        return provider;
    }
}
