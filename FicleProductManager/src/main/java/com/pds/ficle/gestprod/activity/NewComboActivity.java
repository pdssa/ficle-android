/*
package com.pds.ficle.gestprod.activity;


import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Formato;
import com.pds.common.Logger;
import com.pds.common.Usuario;
import com.pds.common.activity.Actividad;
import com.pds.common.activity.TimerActivity;
import com.pds.common.dao.ComboDao;
import com.pds.common.dao.ProductDao;
import com.pds.common.dialog.ConfirmDialog;
import com.pds.common.model.Combo;
import com.pds.common.model.ComboItem;
import com.pds.common.model.Product;
import com.pds.ficle.gestprod.R;
import com.pds.ficle.gestprod.fragment.EditComboPriceFragment;
import com.pds.ficle.gestprod.fragment.EditPriceFragment;
import com.pds.ficle.gestprod.util.SkuUtils;

import java.util.ArrayList;
import java.util.List;

public class NewComboActivity extends TimerActivity implements EditComboPriceFragment.OnEditPriceInteractionListener {

    private Combo combo;
    private Product productoPpal;
    private Config config;
    private Formato.Decimal_Format FORMATO_DECIMAL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_combo);

        //cargamos el producto
        Intent intent = getIntent();
        combo = intent.getParcelableExtra("combo_edit");

        if (combo == null) {
            Toast.makeText(this, "Error en parametros recibidos", Toast.LENGTH_SHORT).show();
            finish();
        } else {
            //formato
            config = new Config(this);
            FORMATO_DECIMAL = Formato.getDecimal_Format(config.PAIS);

            LoadCombo(combo);
        }

        findViewById(R.id.combo_btn_edit_price).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IniciarFragmentEditPrice(combo);
            }
        });

        findViewById(R.id.combo_btn_delete_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ConfirmDialog(NewComboActivity.this, "ELIMINAR COMBO", "¿Esta seguro que desea eliminar el combo?")
                        .set_positiveButtonText("ACEPTAR")
                        .set_positiveButtonAction(new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Eliminar(combo);
                            }
                        })
                        .set_negativeButtonText("CANCELAR")
                        .set_negativeButtonAction(new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });

        findViewById(R.id.combo_btn_back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //finish();
                volver();
            }
        });


        findViewById(R.id.combo_btn_print).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SkuUtils.ImprimirEtiqueta(NewComboActivity.this, config.PRINTER_MODEL, combo.getProducto_ppal_code(), combo.getNombre(), "", false, "");
            }
        });

        findViewById(R.id.combo_btn_print_precio).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String precio_formateado = Formato.FormateaDecimal(combo.getPrecio(), FORMATO_DECIMAL, Formato.getCurrencySymbol(FORMATO_DECIMAL));

                SkuUtils.ImprimirEtiqueta(NewComboActivity.this, config.PRINTER_MODEL, combo.getProducto_ppal_code(), combo.getNombre(), precio_formateado, true, "");
            }
        });

    }


    private void LoadCombo(Combo _combo) {
        //datos combo
        ((TextView) findViewById(R.id.combo_wiz_step_valida_lbl_nombre)).setText(_combo.getNombre());
        ((TextView) findViewById(R.id.combo_wiz_step_valida_lbl_desc_corta)).setText(_combo.getDescripcion());
        ((TextView) findViewById(R.id.combo_wiz_step_valida_lbl_precio)).setText(Formato.FormateaDecimal(_combo.getPrecio(), FORMATO_DECIMAL, Formato.getCurrencySymbol(FORMATO_DECIMAL)));
        ((TextView) findViewById(R.id.combo_wiz_step_valida_lbl_sku)).setText(_combo.getProducto_ppal_code());

        ((TextView) findViewById(R.id.combo_lbl_name)).setText(TextUtils.isEmpty(_combo.getDescripcion()) ? _combo.getNombre() : _combo.getDescripcion());


        //producto ppal
        productoPpal = _combo.getProductoPrincipal(getContentResolver());

        if(productoPpal == null)
            return ;//TODO : check for null...

        //datos de los items
        List<ComboItem> _itemsMasPrincipal = new ArrayList<ComboItem>();

        //creamos un ComboItem temporalmente para el producto principal
        ComboItem itemPpalTemp = new ComboItem();
        itemPpalTemp.setProducto_code(productoPpal.getCode());
        itemPpalTemp.setProducto_id(productoPpal.getId());
        itemPpalTemp.setCantidad(_combo.getCantidad());

        _itemsMasPrincipal.add(itemPpalTemp);

        List<ComboItem> _items = _combo.getItemsCombo(getContentResolver());

        if (_items != null)
            _itemsMasPrincipal.addAll(_items);

        ListView lstItems = (ListView) findViewById(R.id.combo_wiz_step_valida_lst_items);
        lstItems.setEmptyView(findViewById(android.R.id.empty));
        ItemsArrayAdapter listAdapter = new ItemsArrayAdapter(NewComboActivity.this, _itemsMasPrincipal);
        lstItems.setAdapter(listAdapter);

    }

    public void IniciarFragmentEditPrice(Combo item_edit) {
        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.combo_abm_frg_edit_stock) != null) {

            // Create a new Fragment to be placed in the activity layout
            EditComboPriceFragment fragment = EditComboPriceFragment.newInstance(item_edit, FORMATO_DECIMAL);

            fragment.setOnEditPriceInteractionListener(this);

            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.combo_abm_frg_edit_stock, fragment);
            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            ft.commit();
        }
    }

    private void Eliminar(Combo _combo) {
        try {
            if (_combo != null) {
                _combo.setBaja(true);
                _combo.setSync("N");

                ComboDao comboDao = new ComboDao(getContentResolver());

                boolean result = comboDao.saveOrUpdate(_combo);

                //tengo que desmarcar el producto como ppal si era el unico combo que tenia asociado
                boolean updateProduct = (comboDao.first(String.format("producto_ppal_id = %d and baja = 0", productoPpal.getId()), null, null) == null);

                if (updateProduct) {
                    //ya no quedan combos habilitados con ese producto como principal
                    //grabamos el producto como principal de un combo
                    productoPpal.setType(""); //lo desmarcamos como producto ppal de un combo

                    new ProductDao(getContentResolver()).saveOrUpdate(productoPpal);
                }

                if (result) {
                    Logger.RegistrarEvento(getContentResolver(), "i", "BAJA COMBO", "SKU PPAL: " + _combo.getProducto_ppal_code(), "id: " + String.valueOf(_combo.getId()));

                    AlertDialog.Builder builder = new AlertDialog.Builder(NewComboActivity.this);
                    builder.setIcon(android.R.drawable.ic_dialog_info);
                    builder.setTitle("ELIMINAR COMBO");
                    builder.setMessage(Html.fromHtml(String.format("El combo <b>\"%s\"</b> fue eliminado correctamente!", _combo.getNombre())));
                    builder.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface _dialog, int which) {
                            _dialog.dismiss();

                            volver();
                        }
                    });
                    builder.setCancelable(false);
                    builder.show();

                } else
                    Toast.makeText(NewComboActivity.this, "No se ha podido eliminar el combo", Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(NewComboActivity.this, "No se ha podido eliminar el combo", Toast.LENGTH_SHORT).show();

        } catch (Exception ex) {
            Toast.makeText(NewComboActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPriceChanged(Combo _newCombo) {
        combo = _newCombo;

        LoadCombo(_newCombo);
    }

    @Override
    public void onBackPressed() {
        volver();
    }

    private void volver() {
        finish();
    }

    public class ItemsArrayAdapter extends ArrayAdapter<ComboItem> {
        private final Context context;
        private List<ComboItem> items;

        public ItemsArrayAdapter(Context context, List<ComboItem> items) {
            super(context, R.layout.list_item_compra, items);
            this.context = context;
            this.items = items;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View rowView = inflater.inflate(R.layout.list_item_pedido_detalle, parent, false);

            ComboItem item = items.get(position);

            Product p = item.getProducto(getContentResolver());

            ((TextView) rowView.findViewById(R.id.list_item_producto_cod)).setText(TextUtils.isEmpty(p.getCode()) ? "S/C" : p.getCode());
            ((TextView) rowView.findViewById(R.id.list_item_pedidos_producto_det)).setText(p.getName());
            ((TextView) rowView.findViewById(R.id.list_item_pedidos_cantidad)).setText(Integer.toString((int) item.getCantidad()));

            return rowView;
        }

    }
}

*/
