package com.pds.ficle.gestprod.wizard;

import android.app.Activity;
import android.content.Intent;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pds.common.Config;
import com.pds.common.Formato;
import com.pds.common.dao.ProductDao;
import com.pds.common.db.ProductTable;
import com.pds.common.model.Product;
import com.pds.ficle.gestprod.R;
import com.pds.ficle.gestprod.fragment.WizardStep;
import com.pds.ficle.gestprod.fragment.WizardStepChangeListener;

import java.util.List;

/**
 * Created by Hernan on 05/12/2016.
 */
public abstract class BaseAltaWizard {
    protected Activity activity;
    protected ProductWizardListener productWizardListener;
    private WizardStep currentStep;
    protected List<WizardStep> steps;
    private Config config;

    public BaseAltaWizard(Activity context, ProductWizardListener productWizardListener, Config config) {
        this.activity = context;
        this.productWizardListener = productWizardListener;
        this.config = config;
    }

    public void startWizard() {
        //mostramos el primero
        execWizStep(steps.get(0));
    }

    public boolean previousStep() {
        if (currentStep != null && currentStep.previousStep != null) {
            execWizStep(currentStep.previousStep);
            return true;
        }

        return false;
    }

    protected void nextStep(){
        if (currentStep != null && currentStep.nextStep != null) {
            currentStep.nextButton.performClick();
        }
    }

    protected Formato.Decimal_Format getDecimalFormat() {
        return Formato.getDecimal_Format(config.PAIS);
    }

    protected Config getConfig() {
        return config;
    }

    protected View findViewById(int viewId) {
        return activity.findViewById(viewId);
    }

    protected String getString(int stringResId) {
        return activity.getString(stringResId);
    }

    protected Product unicidadSKU(Product product, String code) {
        if (TextUtils.isEmpty(code))
            return null;

        List<Product> check = new ProductDao(this.activity.getContentResolver()).list(String.format(ProductTable.COLUMN_CODE + "='" + code + "' and _id <> %d and removed = 0", product.getId()), null, null);

        return (check.size() > 0 ? check.get(0) : null);
    }

    protected void execWizStep(WizardStep step) {
        //aqui deberíamos completar el paso

        currentStep = step;

        //title
        ((TextView) findViewById(R.id.prod_wiz_lbl_title)).setText(step.titleId);

        final WizardStepChangeListener listener = step.stepChangeListener;

        //back
        if (step.previousStep != null) {
            final WizardStep previousStep = step.previousStep;
            findViewById(R.id.prod_wiz_btn_back).setVisibility(View.VISIBLE);
            findViewById(R.id.prod_wiz_btn_back).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        listener.onStepBack();
                    execWizStep(previousStep);
                }
            });
        } else {
            //findViewById(R.id.prod_wiz_btn_back).setVisibility(View.INVISIBLE);
            //es el paso inicial, avisamos a la activity
            findViewById(R.id.prod_wiz_btn_back).setVisibility(View.VISIBLE);
            findViewById(R.id.prod_wiz_btn_back).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    productWizardListener.restartWizard();//
                }
            });
        }

        //next
        if (step.nextStep != null) {
            final WizardStep nextStep = step.nextStep;
            ((Button) findViewById(R.id.prod_wiz_btn_next)).setText("SIGUIENTE");
            findViewById(R.id.prod_wiz_btn_next).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        if (listener.onStepNext())
                            execWizStep(nextStep);
                    } else
                        execWizStep(nextStep);
                }
            });
            step.nextButton = ((Button) findViewById(R.id.prod_wiz_btn_next));
        } else {
            ((Button) findViewById(R.id.prod_wiz_btn_next)).setText("GRABAR");
            findViewById(R.id.prod_wiz_btn_next).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        if (listener.onStepNext()) {
                            productWizardListener.finishWizard();
                        }
                }
            });
        }
        findViewById(R.id.prod_wiz_btn_next).setVisibility(step.nextButtonViewMode);


        //inflate view
        LinearLayout container = ((LinearLayout) findViewById(R.id.prod_wiz_lay_step));
        container.removeAllViews();

        if (step.viewId != -1) {

            View view = activity.getLayoutInflater().inflate(step.viewId, null);
            container.addView(view, 0);

        }

        //ejecutamos el evento
        if (listener != null)
            listener.onStepShow(step);
    }

    protected void updateProductCreation(int idStep, TextView txtView, Product product) {
        String _text = "Su producto: ";

        switch (idStep) {
            case 1: {
                //SKU
                _text += String.format("%s<br/>", TextUtils.isEmpty(product.getCode()) ? "Sin Cod. Barras" : product.getCode());

            }
            break;
            case 2: {
                //MARCA
                String _productTxt = String.format("%s", product.getBrand());

                _text += String.format("%s<br/>%s", TextUtils.isEmpty(product.getCode()) ? "Sin Cod. Barras" : product.getCode(), _productTxt);

            }
            break;
            case 3: {
                //NOMBRE
                String _productTxt = String.format("%s %s", product.getBrand(), product.getName());

                _text += String.format("%s<br/>%s", TextUtils.isEmpty(product.getCode()) ? "Sin Cod. Barras" : product.getCode(), _productTxt);

            }
            break;
            case 4: {
                //PESABLE
                String _productTxt = String.format("%s %s", product.getBrand(), product.getName());

                _text += String.format("%s<br/>%s<br/>%s / %s", TextUtils.isEmpty(product.getCode()) ? "Sin Cod. Barras" : product.getCode(), _productTxt,
                        product.isWeighable() ? "PESABLE" : "NO PESABLE",
                        product.getTax() != null ? product.getTax().getName().toUpperCase().replace("GRAVADO", "AFECTO") : "");

            }
            break;
            case 5: {
                //CONTENIDO
                String _productTxt;

                if (product.isWeighable())
                    _productTxt = String.format("%s %s %s", product.getBrand(), product.getName(), product.getUnit());
                else
                    _productTxt = String.format("%s %s %s%s", product.getBrand(), product.getName(), product.getContenidoFormatted(), product.getUnit());

                _text += String.format("%s<br/>%s<br/>%s / %s", TextUtils.isEmpty(product.getCode()) ? "Sin Cod. Barras" : product.getCode(), _productTxt,
                        product.isWeighable() ? "PESABLE" : "NO PESABLE",
                        product.getTax() != null ? product.getTax().getName().toUpperCase().replace("GRAVADO", "AFECTO") : "");
            }
            break;
            case 6: {
                //CATEGORIA
                String _productTxt;

                if (product.isWeighable())
                    _productTxt = String.format("%s %s %s", product.getBrand(), product.getName(), product.getUnit());
                else
                    _productTxt = String.format("%s %s %s%s", product.getBrand(), product.getName(), product.getContenidoFormatted(), product.getUnit());

                _text += String.format("%s<br/>%s<br/>%s -> %s<br/>%s / %s", TextUtils.isEmpty(product.getCode()) ? "Sin Cod. Barras" : product.getCode(), _productTxt,
                        product.getDepartment() != null ? product.getDepartment().getName() : "",
                        product.getSubDepartment() != null ? product.getSubDepartment().getName() : "",
                        product.isWeighable() ? "PESABLE" : "NO PESABLE",
                        product.getTax() != null ? product.getTax().getName().toUpperCase().replace("GRAVADO", "AFECTO") : "");

            }
            break;
            case 7: {
                //PRECIO
                String _productTxt;

                if (product.isWeighable())
                    _productTxt = String.format("%s %s %s", product.getBrand(), product.getName(), product.getUnit());
                else
                    _productTxt = String.format("%s %s %s%s", product.getBrand(), product.getName(), product.getContenidoFormatted(), product.getUnit());

                _text += String.format("%s<br/>%s<br/>%s -> %s<br/>%s / %s<br/>Precio Venta: %s", TextUtils.isEmpty(product.getCode()) ? "Sin Cod. Barras" : product.getCode(), _productTxt,
                        product.getDepartment() != null ? product.getDepartment().getName() : "",
                        product.getSubDepartment() != null ? product.getSubDepartment().getName() : "",
                        product.isWeighable() ? "PESABLE" : "NO PESABLE",
                        product.getTax() != null ? product.getTax().getName().toUpperCase().replace("GRAVADO", "AFECTO") : "",
                        Formato.FormateaDecimal(product.getSalePrice(), getDecimalFormat(), Formato.getCurrencySymbol(getDecimalFormat()))
                );

            }
            break;
            case 8: {
                //DESCRIPCION
                String _productTxt;

                if (product.isWeighable())
                    _productTxt = String.format("%s %s %s", product.getBrand(), product.getName(), product.getUnit());
                else
                    _productTxt = String.format("%s %s %s%s", product.getBrand(), product.getName(), product.getContenidoFormatted(), product.getUnit());

                _text += String.format("%s<br/>%s<br/>%s -> %s<br/>%s / %s<br/>Precio Venta: %s<br/>%s", TextUtils.isEmpty(product.getCode()) ? "Sin Cod. Barras" : product.getCode(), _productTxt,
                        product.getDepartment() != null ? product.getDepartment().getName() : "",
                        product.getSubDepartment() != null ? product.getSubDepartment().getName() : "",
                        product.isWeighable() ? "PESABLE" : "NO PESABLE",
                        product.getTax() != null ? product.getTax().getName().toUpperCase().replace("GRAVADO", "AFECTO") : "",
                        Formato.FormateaDecimal(product.getSalePrice(), getDecimalFormat(), Formato.getCurrencySymbol(getDecimalFormat())),
                        product.getDescription()
                );
            }
            break;
        }


        txtView.setText(Html.fromHtml(_text));
    }

    public abstract void inputByScannerSkuStep(String input);

}
