package com.pds.ficle.gestprod.activity;

import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.activity.TimerActivity;
import com.pds.common.dao.ProductDao;
import com.pds.common.db.ProductTable;
import com.pds.common.hardware.ScannerPOWA;
import com.pds.common.model.Product;
import com.pds.common.util.Window;
import com.pds.ficle.gestprod.R;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Callable;

import android_serialport_api.Printer;

public class CodigosRapidosActivity extends TimerActivity{
    private EditText searchText;
    private Button backButton;
//    private Button asociarButton;
    private Button printListButton;
//    private Button borrarButton;
    ListView lstProductos;
    private ProductSearchActivity.ProductArrayAdapter adapter;
    private List<Product> productos;
    private final static int REQUEST_PRODUCTOS = 1;
    private Product selectedProduct;
    private Config config;
    private ScannerPOWA _scanner;
    private Printer _printer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_codigos_rapidos);
        try {
//            borrarButton = (Button) findViewById(R.id.cod_rap_btn_eliminar);
//            borrarButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    new AlertDialog.Builder(CodigosRapidosActivity.this)
//                            .setIcon(android.R.drawable.ic_menu_help)
//                            .setTitle("Eliminar Código Rápido")
//                            .setMessage(Html.fromHtml("<big>Desea eliminar el codigo rapido asociado al producto seleccionado?</big>"))
//                            .setPositiveButton("SI", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//
//                                    borrarButton.setVisibility(View.GONE);
//
//                                    //eliminamos el producto seleccionado
//                                    if (selectedProduct != null) {
//
//                                        //limpiamos el codigo rapido asignado al producto
//                                        selectedProduct.setIspCode("");
//
//                                        boolean result = new ProductDao(getContentResolver()).saveOrUpdate(selectedProduct);
//
//                                        String text = "";
//                                        if (result) {
//                                            text = "Codigo rapido eliminado correctamente";
//
//                                            Toast.makeText(CodigosRapidosActivity.this, text, Toast.LENGTH_SHORT).show();
//
//                                            CancelarSearch();//refrescamos la grilla
//                                        } else {
//                                            text = "No se pudo eliminar el codigo rapido";
//
//                                            Toast.makeText(CodigosRapidosActivity.this, text, Toast.LENGTH_SHORT).show();
//                                        }
//
//                                    }
//                                }
//
//                            })
//                            .setNegativeButton("NO", null)
//                            .setCancelable(false)
//                            .show();
//
//                }
//            });



            printListButton = (Button) findViewById(R.id.cod_rap_btn_print);

            backButton = (Button) findViewById(R.id.cod_rap_btn_back);
            backButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });

            searchText = (EditText) findViewById(R.id.cod_rap_txt_criteria);
            searchText.setFilters(new InputFilter[]{new InputFilter() {
                @Override
                public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                    for (int i = start; i < end; i++)
                        if (!Character.isLetter(source.charAt(i)) &&
                                !Character.isSpaceChar(source.charAt(i)) &&
                                !Character.isDigit(source.charAt(i))
                                ) //solo aceptamos: letras, numeros y espacio
                            return "";

                    return null;
                }
            }});
            searchText.addTextChangedListener(new TextWatcher() {
                private Timer timer = new Timer();
                private final long DELAY = 500; // in ms

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    timer.cancel();
                    timer = new Timer();

                    final Editable _s = s;

                    if (s.length() > 0) {
                        ((ImageView) findViewById(R.id.cod_rap_btn_find)).setImageResource(R.drawable.ic_action_cancel);
                        timer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        UpdateList(_s.toString());
                                    }
                                });
                            }
                        }, DELAY);
                    } else {
                        //CancelarSearch();
                        ((ImageView) findViewById(R.id.cod_rap_btn_find)).setImageResource(R.drawable.lupa);

                        UpdateList(null);
                    }
                }
            });

            findViewById(R.id.cod_rap_btn_find).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CancelarSearch();
                }
            });

            lstProductos = (ListView) findViewById(R.id.cod_rap_lst_productos);
            lstProductos.setEmptyView(findViewById(android.R.id.empty));
            lstProductos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Product _selectedProduct = ((ProductArrayAdapter) lstProductos.getAdapter()).getItem(position);

                    if (TextUtils.isEmpty(_selectedProduct.getIspCode())) {
                        //producto que no tiene codigo rapido => mandamos a asociar
                        IniciarFragmentCodigoRapido(_selectedProduct, false);
                        return;
                    }

                    //IniciarFragmentCodigoRapido(product, true);

                    if (_selectedProduct == selectedProduct) {
                        view.setSelected(false);

                        selectedProduct = null;

//                        borrarButton.setVisibility(View.GONE);
                    } else {
                        view.setSelected(true);

                        selectedProduct = _selectedProduct;

//                        borrarButton.setVisibility(View.VISIBLE);

                    }

                    RefreshAdapter();

                }
            });

            Window.AddEnterKeyboardAction(searchText, new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    return null;
                }
            });

            config = new Config(this);

            //si tenemos una printer POWA, entonces iniciamos el scanner POWA S10
            if (config.PRINTER_MODEL != null && config.PRINTER_MODEL.equals("POWA")) {
                _scanner = new ScannerPOWA(this);
                _scanner._scannerCallback = new ScannerPOWA.ScannerCallback() {
                    @Override
                    public void onScannerRead(String text) {
                        searchText.setText(text);
                    }
                };
            }
        } catch (Exception ex) {
            Toast.makeText(this, "Error: " + ex.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    private void IniciarFragmentCodigoRapido(Product product, boolean edit) {
        if (findViewById(R.id.prod_search_frg_stock_edit) != null) {


        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        CancelarSearch();
    }

    @Override
    protected void onDestroy() {
        if (_scanner != null)
            _scanner.end();

        super.onDestroy();
    }



    public void PrintFastCodeList(View view) {
        try {
            if (_printer == null)
                _printer = Printer.getInstance(config.PRINTER_MODEL, CodigosRapidosActivity.this);

            _printer.delayActivated = true;
            _printer.printerCallback = new Printer.PrinterCallback() {
                @Override
                public void onPrinterReady() {

                    _printer.format(1, 2, Printer.ALINEACION.CENTER);
                    _printer.printLine("LISTADO");
                    _printer.printLine("CODIGOS RAPIDOS");

                    _printer.cancelCurrentFormat();
                    _printer.sendSeparadorHorizontal(false);

                    //imprimimos un listado con los codigos rapidos
                    for(Product product : productos){

                        String descripcion = product.getPrintingName().trim();

                        int len = 3 + 1;//001 + " " + ____________

                        descripcion = descripcion.substring(0, Math.min(descripcion.length(), _printer.get_longitudLinea() - len)); //cortamos

                        //_printer.printOpposite(product.getFast_code(), descripcion);
                        _printer.printLine(product.getIspCode() + " " + descripcion);
                    }

                    _printer.sendSeparadorHorizontal(false);

                    _printer.footer();

                    _printer.end();
                }
            };

            if (!_printer.initialize()) {
                Toast.makeText(CodigosRapidosActivity.this, "Error al inicializar", Toast.LENGTH_SHORT).show();

                _printer.end();
            }
        } catch (Exception ex) {
            Toast.makeText(CodigosRapidosActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void SetEmptyLabel(int mode) {
        if (mode == 1)
            ((TextView) lstProductos.getEmptyView()).setText("No existen códigos rápidos asociados");
        else if (mode == 2)
            ((TextView) lstProductos.getEmptyView()).setText("Producto no existente");
        //else if(mode == 3)
        //    ((TextView)lstProductos.getEmptyView()).setText("Producto sin codigo rapido");

    }

    private void RefreshAdapter() {
        ((ProductArrayAdapter) lstProductos.getAdapter()).notifyDataSetChanged();
    }

    private void UpdateList(String filter) {
        if (productos == null) {
            productos = getListProducts(filter);
            lstProductos.setAdapter(new ProductArrayAdapter(this, productos));
        } else {
            productos.clear();
            productos.addAll(getListProducts(filter));

            RefreshAdapter();
        }

        printListButton.setEnabled(productos.size() > 0);
    }

    private List<Product> getListProducts(String filter) {
        List<Product> _temp;
        if (TextUtils.isEmpty(filter)) {//no hay filtro, traemos los que tengan codigo rapido
            _temp = new ProductDao(getContentResolver()).list("removed = 0 and "+ ProductTable.COLUMN_ISP_CODE+" <> '' ", null, ""+ProductTable.COLUMN_ISP_CODE+" ASC");
            if (_temp.size() < 1)
                SetEmptyLabel(1);
        } else {//hay filtro numerico => SKU
            _temp = new ProductDao(getContentResolver()).list("removed = 0 and "+ ProductTable.COLUMN_ISP_CODE +" <> '' and (code like '%" + filter + "%' or "+ProductTable.COLUMN_ISP_CODE+" like '%" + filter + "%')", null, ""+ProductTable.COLUMN_ISP_CODE+" ASC");

            if (_temp.size() < 1) {
                //no hay productos que coincidan con el filtro y tengan codigo rapido
                _temp = new ProductDao(getContentResolver()).list("removed = 0 and IFNULL("+ProductTable.COLUMN_ISP_CODE+",'') = '' and code like '%" + filter + "%' and code not like '%-%'", null, "name ASC");

                if (_temp.size() == 1)
                    IniciarFragmentCodigoRapido(_temp.get(0), false);
                else if (_temp.size() < 1)
                    SetEmptyLabel(2);
            }
        }
        return _temp;

        //
        //filtro numerico => SKU
        //return new ProductDao(getContentResolver()).list("removed = 0 and fast_code <> '' and code like '%" + filter + "%' or fast_code like '%" + filter + "%' and code not like '%-%'", null, "name ASC");
    }

    private void CancelarSearch() {
        searchText.setText("");
        selectedProduct = null;

        ((ImageView) findViewById(R.id.cod_rap_btn_find)).setImageResource(R.drawable.lupa);

        //foco sin teclado
        Window.FocusViewNoSoftKeyboard(CodigosRapidosActivity.this, searchText);

        UpdateList(null);
    }

    //Metodo que se ejecuta al volver del fragment de seleccionar un proveedor
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_PRODUCTOS: {//requerimos un producto
                if (resultCode == RESULT_OK) {
                    Object result = data.getParcelableExtra("result");

                    IniciarFragmentCodigoRapido((Product) result, false);//mandamos a crear el codigo rapido con el producto seleccionado
                }
                if (resultCode == RESULT_CANCELED) {
                    //Write your code if there's no result
                    //SelectProvider(null);
                }
            }
            break;
        }

    }


    class ProductArrayAdapter extends ArrayAdapter<Product> {
        private final Context context;
        private List<Product> products;

        public ProductArrayAdapter(Context context, List<Product> products) {
            super(context, R.layout.list_item_prod, products);
            this.context = context;
            this.products = products;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(context)
                        .inflate(R.layout.list_item_cod_rap, parent, false);
            }

            TextView nameTextView = (TextView) convertView.findViewById(R.id.lblListItem);
            nameTextView.setText(products.get(position).getName());

            TextView codeTextView = (TextView) convertView.findViewById(R.id.lblListItemCode);
            codeTextView.setText(products.get(position).getIspCode());

            if (convertView.isSelected())
                convertView.setBackgroundResource(R.drawable.azul);
            else
                convertView.setBackgroundResource(R.drawable.bg_normal);

            return convertView;
        }

    }


}
