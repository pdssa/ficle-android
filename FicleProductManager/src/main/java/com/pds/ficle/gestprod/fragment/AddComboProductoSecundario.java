package com.pds.ficle.gestprod.fragment;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Formatos;
import com.pds.common.dao.ProductDao;
import com.pds.common.db.ProductTable;
import com.pds.common.model.ComboItem;
import com.pds.common.model.Product;
import com.pds.ficle.gestprod.R;
import com.pds.ficle.gestprod.activity.ProductSearchActivity;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AddComboProductoSecundario.OnComboFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AddComboProductoSecundario#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddComboProductoSecundario extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    private ComboItem itemEdit;
    private Product productEdit;

    private Button btnGrabar;
    private Button btnCancelar;
    private Button btnEliminar;
    //private Button btnAddItem;
    private EditText txtCantidad;
    private EditText txtCodigo;
    private EditText txtDescripcion;
    private ImageButton btnFindProd;
    private boolean flagItemEdit = false;
    //private List<ComboItem> listaItems;

    private OnComboFragmentInteractionListener mListener;

    private Timer timer;

    public void setOnComboFragmentInteractionListener(OnComboFragmentInteractionListener mListener) {
        this.mListener = mListener;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param item Parameter 1.
     * @return A new instance of fragment AddComboProductoSecundario.
     */
    public static AddComboProductoSecundario newInstance(ComboItem item) {
        AddComboProductoSecundario fragment = new AddComboProductoSecundario();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, item);
        fragment.setArguments(args);
        return fragment;
    }

    public AddComboProductoSecundario() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            itemEdit = getArguments().getParcelable(ARG_PARAM1);
            if (itemEdit != null){
                productEdit = itemEdit.getProducto(getActivity().getContentResolver());
                flagItemEdit = true;
            }
        }

        //listaItems = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_combo_producto_secundario, container, false);

        Init_Views(view);

        Init_Eventos(view);

        if (itemEdit != null) {
            btnEliminar.setVisibility(View.VISIBLE);

            CargarDatos(
                    productEdit.getCode(),
                    productEdit.getName(),
                    itemEdit.getCantidad()
            );
        }

        //posamos el foco sobre el codigo, pero quitamos el teclado, para que puedan ingresar el codigo via scanner
        txtCodigo.requestFocus();

        ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(txtCodigo.getWindowToken(), 0);

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 2) {//requerimos un product
            if (resultCode == Activity.RESULT_OK) {
                Object result = data.getParcelableExtra("result");
                SelectProduct((Product) result);
                txtCantidad.setText(String.valueOf(1));
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                //SelectProvider(null);
            }
        }
    }

    private void Init_Views(View view) {
        btnGrabar = (Button) view.findViewById(R.id.frg_add_item_btn_grabar);
        btnCancelar = (Button) view.findViewById(R.id.frg_add_item_btn_cancelar);
        btnEliminar = (Button) view.findViewById(R.id.frg_add_item_btn_eliminar);
        //btnAddItem = (Button) view.findViewById(R.id.frg_add_item_btn_add_item);
        txtCantidad = (EditText) view.findViewById(R.id.frg_add_item_txt_cantidad);

        txtCodigo = (EditText) view.findViewById(R.id.frg_add_item_txt_codigo);
        txtDescripcion = (EditText) view.findViewById(R.id.frg_add_item_txt_descripcion);
        btnFindProd = (ImageButton) view.findViewById(R.id.fc_add_item_btn_find_prod);
    }

    private void Init_Eventos(View view) {
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cerrar();
            }
        });
        btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EliminarItem();
            }
        });
        btnGrabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GrabarItem();
                Cerrar();
            }
        });
        view.findViewById(R.id.img_dummy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Cerrar();
            }
        });
        /*btnAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GrabarItem();
            }
        });*/

        view.findViewById(R.id.frg_add_item_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        btnFindProd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ProductSearchActivity.class);
                intent.putExtra("TypeResultSearch", ProductSearchActivity.TypeResultSearch.SEARCH_PRODUCT_BACK);
                startActivityForResult(intent, 2);
            }
        });

        txtCodigo.setFilters(new InputFilter[]{new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++)
                    if (!Character.isLetter(source.charAt(i)) &&
                            !Character.isSpaceChar(source.charAt(i)) &&
                            !Character.isDigit(source.charAt(i))
                            ) //solo aceptamos: letras, numeros y espacio
                        return "";

                return null;
            }
        }});
        /*txtCodigo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus && itemEdit==null){
                    //esto es para que, en el caso de estar editando un item, no pierda la info en edicion, al salir del foco del codigo ,dado que vuelve a blanquear los campos

                    if(!TextUtils.isEmpty(((EditText) view).getText())) {
                        if (!ObtenerProductoByCode(((EditText) view).getText().toString()))
                            SelectProduct(null);//no encontramos code
                    }
                }
            }
        });*/
        txtCodigo.addTextChangedListener(new TextWatcher() {

            private final long DELAY = 500; // in ms

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if(timer == null)
                    timer = new Timer();
                else {
                    timer.cancel();
                    timer = new Timer();
                }

                final Editable _s = s;

                if (s.length() > 0) {
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (ObtenerProductoByCode(_s.toString()))
                                        txtCantidad.requestFocus();
                                    else
                                        SelectProduct(null);//no encontramos code

                                    timer.cancel();
                                }
                            });
                        }
                    }, DELAY);
                } /*else
                    ((ImageView) findViewById(R.id.prod_search_btn_find)).setImageResource(R.drawable.lupa);*/
            }
        });
        txtCantidad.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean focus) {
                if (focus) {
                    ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(txtCantidad, 0);
                }
            }
        });
    }

    private void CargarDatos(String codigo, String descripcion, double cantidad) {
        txtCantidad.setText(cantidad != 0 ? Integer.toString((int) cantidad) : null);
        txtCodigo.setText(codigo);
        txtDescripcion.setText(descripcion);
    }

    public void EliminarItem() {
        try{

            if (mListener != null) {
                    mListener.onItemDeleted(itemEdit);
            }

            Cerrar();

        } catch (Exception ex) {
            Toast.makeText(AddComboProductoSecundario.this.getActivity(), "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }
    public void GrabarItem() {
        try {

            txtCodigo.setError(null);
            txtCantidad.setError(null);

            if(productEdit == null){
                txtCodigo.setError("Producto obligatorio");
                return;
            }

            if (TextUtils.isEmpty(txtCantidad.getText())){
                txtCantidad.setError("Debe ingresar una cantidad");
                return;
            }

            boolean newItem = itemEdit == null;

            if (newItem)
                itemEdit = new ComboItem();

            double cantidad = TextUtils.isEmpty(txtCantidad.getText()) ? 0 : Double.parseDouble(txtCantidad.getText().toString());

            itemEdit.setProducto_id(productEdit.getId());
            itemEdit.setProducto_code(productEdit.getCode());
            itemEdit.setProducto_precio_vta(productEdit.getSalePrice());
            itemEdit.setCantidad(cantidad);

            //listaItems.add(itemEdit);

            if (mListener != null) {
                if (newItem) {
                    mListener.onItemCreated(itemEdit);

                    //agregamos y nos mantenemos en pantalla para continuar..
                    Toast.makeText(AddComboProductoSecundario.this.getActivity(), "Item agregado correctamente al combo", Toast.LENGTH_SHORT).show();

                    ClearFragment();
                }
                else {
                    mListener.onItemEdited(itemEdit);

                    Cerrar();
                }
            }



        } catch (Exception ex) {
            Toast.makeText(AddComboProductoSecundario.this.getActivity(), "Error: " + ex.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    private void Cerrar() {
        if (getActivity().getCurrentFocus() != null)
            ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        //getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        FragmentManager fm = getActivity().getFragmentManager();

        Fragment fragment = fm.findFragmentById(R.id.frgAddEditItem);

        FragmentTransaction ft = fm.beginTransaction();

        ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);

        ft.hide(fragment);

        ft.commit();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnComboFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnComboFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        if(timer != null)
            timer.cancel();
    }

    private void ClearFragment(){
        itemEdit = null;
        productEdit = null;
        txtDescripcion.setText(null);
        txtCodigo.setText(null);
        txtCantidad.setText(null);

        txtCodigo.requestFocus();

        ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(txtCodigo.getWindowToken(), 0);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnComboFragmentInteractionListener {
        public void onItemCreated(ComboItem new_item);

        public void onItemEdited(ComboItem edited_item);

        public void onItemDeleted(ComboItem deleted_item);
    }

    private boolean ObtenerProductoByCode(String code) {
        try {

            boolean pudoLeer = true;

            List<Product> productos = new ProductDao(getActivity().getContentResolver()).list(ProductTable.COLUMN_CODE + " = '" + code + "' AND removed = 0", null, null);

            if (productos.size() > 0) {
                // en el caso de haber mas de uno, nos quedamos solo con el primero
                Product p = productos.get(0);
                SelectProduct(p);

            } else {
                pudoLeer = false;
            }

            return pudoLeer;

        } catch (Exception ex) {
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public void SelectProduct(Product _product) {
        if (_product != null) {
            productEdit = _product;
            txtCodigo.setError(null);

            if (!flagItemEdit){
            CargarDatos(
                    _product.getCode(),
                    _product.getName(),
                    1
            );}else{
                CargarDatos(
                        _product.getCode(),
                        _product.getName(),
                        itemEdit.getCantidad()
                );
            }
        } else {
            //TODO: product not found
            productEdit = null;
            txtDescripcion.setText(null);
            txtCodigo.setError("Producto no existente");
        }
    }
}
