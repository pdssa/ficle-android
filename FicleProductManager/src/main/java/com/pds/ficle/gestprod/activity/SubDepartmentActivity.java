package com.pds.ficle.gestprod.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.pds.common.activity.TimerActivity;
import com.pds.common.dao.ProductDao;
import com.pds.common.dao.TaxDao;
import com.pds.common.model.Product;
import com.pds.common.model.Tax;
import com.pds.ficle.gestprod.R;
import com.pds.common.dao.DepartmentDao;
import com.pds.common.dao.ProviderDao;
import com.pds.common.dao.SubDepartmentDao;
import com.pds.common.model.Department;
import com.pds.common.model.Provider;
import com.pds.common.model.SubDepartment;
import com.pds.ficle.gestprod.util.SpinnerUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class SubDepartmentActivity extends TimerActivity {
    private final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");

    private Button addButton;
    private Button cancelButton;
    private Button deleteButton;
    private ImageView addDepartmentImage;
    private EditText altaEditText;
    private EditText nameEditText;
    private EditText descriptionEditText;
    private Spinner departmentSpinner;
    private Switch genericSwitch;
    private CheckBox genericPesableCheckBox;
    private RadioButton modoListaOption;
    private RadioButton modoGrillaOption;
    private EditText codigoEditText;
    private EditText codigoPadreEditText;

    private String name;
    private String alta;
    private String description;
    private Department department;
    private boolean edit = false;
    private boolean generico;
    private boolean genericoPesable;
    private int modo_visualizacion;
    private String codigo, codigo_padre;

    private SubDepartmentTask subDepartmentTask = null;

    private SubDepartment subDepartment;

    private ArrayAdapter<Department> departmentArrayAdapter;
    private List<Department> departmentList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_department);

        Intent intent = getIntent();
        subDepartment = intent.getParcelableExtra("subDepartment");
        if (subDepartment == null) {
            subDepartment = new SubDepartment();
            subDepartment.setAutomatic(0);
            alta = DATE_FORMAT.format(new Date());
            generico = false;
            genericoPesable = false;
            modo_visualizacion = 0;
        } else {
            edit = true;
            name = subDepartment.getName();
            alta = DATE_FORMAT.format(subDepartment.getAlta());
            description = subDepartment.getDescription();
            department = subDepartment.getDepartment();
            generico = subDepartment.isGenericSubdept();
            genericoPesable = subDepartment.isGenericPesableSubdept();
            modo_visualizacion = subDepartment.getVisualization_mode();
            codigo = subDepartment.getCodigo();
            codigo_padre = subDepartment.getCodigoPadre();
        }

        altaEditText = (EditText) findViewById(R.id.sub_department_alta_date);
        altaEditText.setText(alta);

        nameEditText = (EditText) findViewById(R.id.sub_department_name);
        nameEditText.setText(name);

        descriptionEditText = (EditText) findViewById(R.id.sub_department_description);
        descriptionEditText.setText(description);

        codigoEditText = (EditText) findViewById(R.id.sub_department_codigo);
        codigoEditText.setText(codigo);

        codigoPadreEditText = (EditText) findViewById(R.id.sub_department_codigo_padre);
        codigoPadreEditText.setText(codigo_padre);

        genericPesableCheckBox = (CheckBox)findViewById(R.id.subdepartment_generic_pesable);
        genericSwitch = (Switch) findViewById(R.id.subdepartment_generic);
        genericSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                genericPesableCheckBox.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            }
        });
        genericSwitch.setChecked(generico);
        genericPesableCheckBox.setVisibility(genericSwitch.isChecked() ? View.VISIBLE : View.GONE);
        genericPesableCheckBox.setChecked(genericoPesable);

        modoListaOption = (RadioButton) findViewById(R.id.subdepartment_rbt_modo_lista);
        modoListaOption.setChecked(modo_visualizacion == 0);
        modoGrillaOption = (RadioButton) findViewById(R.id.subdepartment_rbt_modo_grilla);
        modoGrillaOption.setChecked(modo_visualizacion == 1);

        departmentList = getListDepartments();
        departmentArrayAdapter = new ArrayAdapter<Department>(this, android.R.layout.simple_spinner_item, departmentList);
        departmentArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        departmentSpinner = (Spinner) findViewById(R.id.sub_department_department);
        departmentSpinner.setAdapter(departmentArrayAdapter);

        if (department != null) {
            int index = SpinnerUtils.getIndexForElement(departmentSpinner, new SpinnerUtils.Predicate<Department>() {
                @Override
                public boolean evaluate(Department element) {
                    return department.getId() == element.getId();
                }
            });
            departmentSpinner.setSelection(index);
        }

        addDepartmentImage = (ImageView) findViewById(R.id.sub_department_department_add_icon);
        addDepartmentImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SubDepartmentActivity.this, DepartmentActivity.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });

        cancelButton = (Button) findViewById(R.id.sub_department_cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        addButton = (Button) findViewById(R.id.sub_department_add_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate();
            }
        });

        if (edit) {
            deleteButton = (Button) findViewById(R.id.sub_department_delete);
            deleteButton.setVisibility(View.VISIBLE);
            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    delete();
                }
            });

            adjustEditMode();
        }
    }

    public void adjustEditMode(){
        if(subDepartment.getAutomatic() == 1){
            //no puede grabar
            addButton.setVisibility(View.INVISIBLE);
            //no tiene la opcion de eliminar
            deleteButton.setVisibility(View.INVISIBLE);
            //no puede editar el nombre
            nameEditText.setEnabled(false);
            //no puede editar descripcion
            descriptionEditText.setEnabled(false);
            //no puede editar modo visualizacion
            modoListaOption.setEnabled(false);
            modoGrillaOption.setEnabled(false);
            //no puede editar condicion del subdepto
            genericSwitch.setEnabled(false);
            genericPesableCheckBox.setEnabled(false);
            //no puede cambiar el depto
            departmentSpinner.setEnabled(false);
        }
    }

    @Override
    public void onResume(){
        super.onResume();

        departmentList.clear();
        departmentList.addAll(getListDepartments());
        departmentArrayAdapter.notifyDataSetChanged();
    }

    public List<Department> getListDepartments(){
        List<Department> lst = (new DepartmentDao(getContentResolver())).list();
        lst.add(0,new Department(-1, "Seleccione..."));
        return lst;
    }

    public void validate() {
        if (subDepartmentTask != null) {
            return;
        }

        boolean cancel = false;

        // Reset errors.
        nameEditText.setError(null);
        altaEditText.setError(null);
        descriptionEditText.setError(null);

        name = nameEditText.getText().toString();
        alta = altaEditText.getText().toString();
        description = descriptionEditText.getText().toString();
        department = ((Department) departmentSpinner.getSelectedItem());
        generico = genericSwitch.isChecked();
        genericoPesable = genericPesableCheckBox.isChecked();
        modo_visualizacion = modoGrillaOption.isChecked() ? 1 : 0;

        if (TextUtils.isEmpty(name)) {
            nameEditText.setError(getString(R.string.error_field_required));
            cancel = true;
        }

        if (department == null) {
            Toast.makeText(this
                    , "Error: No existen departamentos. Por favor, registre al menos un departamento."
                    , Toast.LENGTH_SHORT).show();
            cancel = true;
        }
        else if (department.getId() == -1) {
            Toast.makeText(this
                    , "Error: Debe seleccionar un departamento."
                    , Toast.LENGTH_SHORT).show();
            cancel = true;
        }


        /*if (TextUtils.isEmpty(description)) {
            descriptionEditText.setError(getString(R.string.error_field_required));
            cancel = true;
        }*/

        if(generico) {
            //SI ESTA MARCADO COMO GENERICO => BUSCAMOS QUE NO TENGA PRODUCTOS ASOCIADOS
            List<Product> products = new ProductDao(getContentResolver()).list(String.format("subdepartmennt_id = %d and removed = 0 and generic = 0", subDepartment.getId()), null, null);
            if (products.size() > 0) {
                //SI TIENE PRODUCTOS ASOCIADOS => ADVERTIMOS QUE DEBE DESASOCIARLOS PREVIAMENTE
                Toast.makeText(this, "Error: El subdepartamento tiene productos dependientes. Debe desasociarlos previamente para poder convertirlo a SubDepartamento Genérico.", Toast.LENGTH_LONG).show();
                cancel = true;
            }
        }

        if (!cancel) {
            subDepartmentTask = new SubDepartmentTask();
            subDepartmentTask.execute((Void) null);
        }
    }

    public void delete() {
        List<Provider> providers = new ProviderDao(getContentResolver())
                .list(String.format("subdepartment_id = %d", subDepartment.getId()), null, null);
        if (providers.size() == 0) {
            new SubDepartmentDao(getContentResolver()).delete(subDepartment);
            Toast.makeText(getApplicationContext(), R.string.message_delete_success_sub_department, Toast.LENGTH_SHORT).show();
            onBackPressed();
        } else {
            Toast.makeText(getApplicationContext(), R.string.message_delete_denied_sub_department, Toast.LENGTH_SHORT).show();
        }
    }

    public class SubDepartmentTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {
            subDepartment.setDescription(description);
            subDepartment.setDepartment(department);
            subDepartment.setName(name);
            subDepartment.setGeneric_subdept(generico ? (genericoPesable ? 2 : 1) : 0);
            subDepartment.setVisualization_mode(modo_visualizacion);
            try {
                subDepartment.setAlta(DATE_FORMAT.parse(alta));
            } catch (ParseException e) {
                Log.d("SubDepartmentActivity", "Alta date parse error.");
            }

            boolean result =  new SubDepartmentDao(getContentResolver()).saveOrUpdate(subDepartment);

            if(subDepartment.isGenericSubdept()){
                //hay que crear un producto generico
                ProductDao productDao = new ProductDao(getContentResolver());
                List<Product> products = productDao.list(String.format("subdepartmennt_id = %d and removed = 0 and generic = 1" , subDepartment.getId()),null,null );

                Product product ;
                if(products.size() > 0){
                    product = products.get(0); //DEBERIA TENER SOLO UN PRODUCTO, QUE ES EL GENERICO
                }
                else {
                    product = new Product();
                }

                product.setIdDepartment(department.getId());
                product.setIdSubdepartment(subDepartment.getId());
                product.setWeighable(subDepartment.isGenericPesableSubdept());
                product.setName(subDepartment.getName());
                product.setDescription(subDepartment.getDescription());
                product.setGeneric(true);
                product.setAlta(new Date());
                product.setSalePrice(0);
                product.setStock(0);
                product.setPurchasePrice(0);
                product.setMinStock(0);
                product.setContenido(0);
                product.setBrand("");
                product.setCode("");
                product.setProvider(null);
                product.setUnit("");
                product.setPresentation("");
                List<Tax> _tax = new TaxDao(getContentResolver()).list("name = 'Gravado'" , null, null);
                product.setTax(_tax.get(0));
                product.setIva(_tax.get(0).getAmount());

                result = productDao.saveOrUpdate(product);
            }

            return result;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            subDepartmentTask = null;

            CharSequence text;
            if(success)
                text = getString(R.string.message_save_success_sub_department);
            else
                text = getString(R.string.message_save_failed_sub_department);
            Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();

            if(success)
                onBackPressed();
        }

        @Override
        protected void onCancelled() {
            subDepartmentTask = null;
        }
    }

}

