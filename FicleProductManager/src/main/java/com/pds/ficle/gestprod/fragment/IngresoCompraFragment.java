package com.pds.ficle.gestprod.fragment;



import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pds.ficle.gestprod.R;
import com.pds.ficle.gestprod.activity.CargaCompraActivity;
import com.pds.ficle.gestprod.activity.ProductSearchActivity;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class IngresoCompraFragment extends Fragment {


    public IngresoCompraFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ingreso_compra, container, false);

        view.findViewById(R.id.frg_ingreso_compra_por_producto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ProductSearchActivity.class);
                intent.putExtra("TypeResultSearch", ProductSearchActivity.TypeResultSearch.STOCK_PRODUCT_EDIT);
                startActivity(intent);
                getActivity().overridePendingTransition(0, 0);
            }
        });

        view.findViewById(R.id.frg_ingreso_compra_por_proveedor).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CargaCompraActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(0, 0);
            }
        });

        view.findViewById(R.id.img_dummy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { Cerrar(); }
        });

        return view;
    }

    private void Cerrar(){
        FragmentManager fm = getActivity().getFragmentManager();

        Fragment fragment = fm.findFragmentById(R.id.main_fragment);

        FragmentTransaction ft = fm.beginTransaction();

        ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);

        ft.hide(fragment);

        ft.commit();
    }

}
