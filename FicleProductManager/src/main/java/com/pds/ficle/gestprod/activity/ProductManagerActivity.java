package com.pds.ficle.gestprod.activity;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.pds.common.activity.TimerActivity;
import com.pds.common.model.Product;
import com.pds.ficle.gestprod.R;
import com.pds.ficle.gestprod.fragment.ProductListFragment;

public class ProductManagerActivity extends TimerActivity implements ProductListFragment.ProductListFragmentInteractionListener{

    private Button newButton;
    private Button backButton;

    private boolean searchScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_manager);

        searchScreen = false;

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            //vemos si es invocado como screen de búsqueda
            searchScreen =  extras.getBoolean("search", false);
        }

        newButton = (Button) findViewById(R.id.product_manager_add_button);
        newButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProductManagerActivity.this, ProductActivity.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });

        backButton = (Button) findViewById(R.id.product_manager_back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        FragmentManager fm = getFragmentManager();

        if (fm.findFragmentById(android.R.id.content) == null) {
            ProductListFragment list = ProductListFragment.newInstance(searchScreen);
            list.setProductListFragmentInteractionListener(this);
            fm.beginTransaction().add(R.id.product_manager_list_container, list).commit();
        }
    }


    @Override
    public void onProductSelected(Product product) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result",product);
        setResult(RESULT_OK,returnIntent);
        finish();
    }
}
