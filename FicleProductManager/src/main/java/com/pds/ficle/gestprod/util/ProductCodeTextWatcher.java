package com.pds.ficle.gestprod.util;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.ImageView;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Hernan on 01/03/2015.
 */
public class ProductCodeTextWatcher implements TextWatcher {
    private Timer timer=new Timer();
    private final long DELAY = 500; // in ms

    private Activity _activity;

    private OnProductReadingInterfaceListener _interface;

    public void setOnProductReadingInterfaceListener(OnProductReadingInterfaceListener _interface) {
        this._interface = _interface;
    }

    public ProductCodeTextWatcher(Activity activity){
        this._activity = activity;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {}

    @Override
    public void afterTextChanged(Editable s) {
        timer.cancel();
        timer = new Timer();

        final Editable _s = s;

        /*if (s.length() > 0) {
            ((ImageView) findViewById(R.id.prod_search_btn_find)).setImageResource(R.drawable.ic_action_cancel);
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    _activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            FiltrarProductos(_s);
                        }
                    });
                }
            }, DELAY);
        } else
            ((ImageView) findViewById(R.id.prod_search_btn_find)).setImageResource(R.drawable.lupa);*/
    }

    public interface OnProductReadingInterfaceListener{
        public void onCodeReaded(String code);
        public void onNothingReaded();
    }
}
