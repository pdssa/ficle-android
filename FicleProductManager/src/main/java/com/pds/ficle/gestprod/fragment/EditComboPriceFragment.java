/*
package com.pds.ficle.gestprod.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Formato;
import com.pds.common.Logger;
import com.pds.common.model.Combo;
import com.pds.common.util.Window;
import com.pds.ficle.gestprod.R;

import java.util.concurrent.Callable;

*/
/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EditComboPriceFragment.OnEditPriceInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EditComboPriceFragment#newInstance} factory method to
 * create an instance of this fragment.
 *//*

public class EditComboPriceFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "combo";
    private static final String ARG_PARAM2 = "formato";

    private Combo combo;

    private OnEditPriceInteractionListener mListener;
    private Button btnGrabar, btnCancelar;
    private TextView txtPrecioActual, txtTitulo;
    private EditText txtNuevoPrecio;
    private Formato.Decimal_Format DECIMAL_FORMAT;

    public void setOnEditPriceInteractionListener(OnEditPriceInteractionListener mListener) {
        this.mListener = mListener;
    }

    */
/**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param _combo Combo a editar.
     * @return A new instance of fragment EditPriceFragment.
     *//*

    public static EditComboPriceFragment newInstance(Combo _combo, Formato.Decimal_Format _decimal) {
        EditComboPriceFragment fragment = new EditComboPriceFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, _combo);
        args.putSerializable(ARG_PARAM2, _decimal);
        fragment.setArguments(args);
        return fragment;
    }

    public EditComboPriceFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            combo = getArguments().getParcelable(ARG_PARAM1);
            DECIMAL_FORMAT = (Formato.Decimal_Format) getArguments().getSerializable(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_combo_price, container, false);

        Init_Views(view);

        Init_Eventos(view);


        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        Step1();

    }

    private void Init_Views(View view) {
        btnGrabar = (Button) view.findViewById(R.id.frg_edit_price_btn_aceptar);
        btnCancelar = (Button) view.findViewById(R.id.frg_edit_price_btn_cancelar);

        txtNuevoPrecio = (EditText) view.findViewById(R.id.frg_edit_price_nuevo);

        txtPrecioActual = (TextView) view.findViewById(R.id.frg_edit_price_actual);

        txtTitulo = (TextView) view.findViewById(R.id.frg_edit_price_lbl_title);
    }

    private void Init_Eventos(View view) {
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cerrar();
            }
        });
        btnGrabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CerrarItem();
            }
        });

        view.findViewById(R.id.img_dummy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cerrar();
            }
        });
        view.findViewById(R.id.frg_edit_price_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
    }

    double newSalePrice = 0;

    private void Step1() {
        txtTitulo.setText(getString(R.string.edit_price_precio_vta));

        txtNuevoPrecio.setText("");
        txtNuevoPrecio.setError(null);

        txtPrecioActual.setText(Formato.FormateaDecimal(combo.getPrecio(), DECIMAL_FORMAT, Formato.getCurrencySymbol(DECIMAL_FORMAT)));

        Window.FocusViewShowSoftKeyboard(getActivity(), txtNuevoPrecio);

        Window.AddDoneKeyboardAction(txtNuevoPrecio, new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                btnGrabar.performClick();
                return null;
            }
        });

    }

    public void CerrarItem() {

        //obtenemos el precio de venta ingresado
        //validamos que sea obligatorio
        txtNuevoPrecio.setError(null);

        String salePrice = txtNuevoPrecio.getText().toString().trim();

        if (TextUtils.isEmpty(salePrice)) {
            txtNuevoPrecio.setError(getString(R.string.error_field_required));
            txtNuevoPrecio.requestFocus();
        } else {

            double precio = Formato.ParseaDecimal(salePrice, DECIMAL_FORMAT, true);

            if (precio <= 0) {
                txtNuevoPrecio.setError(getString(R.string.error_invalid_value));
                txtNuevoPrecio.requestFocus();
            } else {
                txtNuevoPrecio.setText("");

                //actualizamos los precios del producto y grabamos
                combo.setPrecio(precio);

                boolean saved = new ComboDao(getActivity().getContentResolver()).saveOrUpdate(combo);

                if (saved) {

                    Logger.RegistrarEvento(getActivity().getContentResolver(), "i", "MODIF. PRECIO COMBO",
                            "NOMBRE: " + combo.getNombre(),
                            "Prc.Vta.: " + Formato.FormateaDecimal(combo.getPrecio(), DECIMAL_FORMAT, Formato.getCurrencySymbol(DECIMAL_FORMAT))
                    );

                    if (mListener != null) {
                        mListener.onPriceChanged(combo);
                    }

                    Cerrar();

                } else {
                    Toast.makeText(getActivity(), "No se pudo actualizar el combo", Toast.LENGTH_SHORT).show();
                }
            }

        }

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnEditPriceInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " debe implementar OnEditStockInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void Cerrar() {
        //cerramos el teclado
        Window.CloseSoftKeyboard(getActivity());

        FragmentManager fm = getActivity().getFragmentManager();

        Fragment fragment = fm.findFragmentById(R.id.combo_abm_frg_edit_stock);

        FragmentTransaction ft = fm.beginTransaction();

        ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);

        ft.hide(fragment);

        ft.commit();
    }

    */
/**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     *//*

    public interface OnEditPriceInteractionListener {
        public void onPriceChanged(Combo combo);
    }

}
*/
