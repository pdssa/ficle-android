package com.pds.ficle.gestprod.util;

import android.app.DatePickerDialog;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;

/**
 * Created by raul.lopez on 5/18/2014.
 */
public class DateOnClickListener implements View.OnClickListener {
    private final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");

    private EditText editText;

    private String initialDate;
    private String initialMonth;
    private String initialYear;

    private DatePickerDialog dialog = null;


    public DateOnClickListener(EditText editText) {
        this.editText = editText;
    }

    @Override
    public void onClick(View v) {
        String preExistingDate = (String) editText.getText().toString();

        if (preExistingDate == null || preExistingDate.equals("")) {
            preExistingDate = DATE_FORMAT.format(new Date());
        }

        StringTokenizer st = new StringTokenizer(preExistingDate, "/");
        initialDate = st.nextToken();
        initialMonth = st.nextToken();
        initialYear = st.nextToken();
        if (dialog == null)
            dialog = new DatePickerDialog(v.getContext(),
                    new PickDate(), Integer.parseInt(initialYear),
                    Integer.parseInt(initialMonth) - 1,
                    Integer.parseInt(initialDate));
        dialog.updateDate(Integer.parseInt(initialYear), Integer.parseInt(initialMonth) - 1, Integer.parseInt(initialDate));
        dialog.show();
    }

    private class PickDate implements DatePickerDialog.OnDateSetListener {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            view.updateDate(year, monthOfYear, dayOfMonth);

            Date date;
            try {
                date = DATE_FORMAT.parse(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
            } catch (ParseException e) {
                date = new Date();
            }

            editText.setText(DATE_FORMAT.format(date));
            dialog.hide();
        }
    }

}
