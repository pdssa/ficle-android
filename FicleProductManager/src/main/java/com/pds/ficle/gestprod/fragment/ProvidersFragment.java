package com.pds.ficle.gestprod.fragment;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;

import com.pds.common.dao.ProviderDao;
import com.pds.common.dao.ProviderProductDao;
import com.pds.common.model.Product;
import com.pds.common.model.Provider;
import com.pds.common.model.ProviderProduct;
import com.pds.ficle.gestprod.R;
import com.pds.ficle.gestprod.activity.ProviderManagerActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link ProvidersFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProvidersFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "product";

    private Product producto;
    private List<Provider> _list;
    private ListView lstProviders;
    //private View layoutNewProvider;
    //private TextView txtNewProvider;

    public void setOnProvidersInteractionListener(OnProvidersInteractionListener mListener) {
        this.mListener = mListener;
    }

    private OnProvidersInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param product Parameter 1.
     * @return A new instance of fragment ProvidersFragment.
     */
    public static ProvidersFragment newInstance(Product product) {
        ProvidersFragment fragment = new ProvidersFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, product);
        fragment.setArguments(args);
        return fragment;
    }

    public ProvidersFragment() {
        // Required empty public constructor
    }

    /*public boolean Grabar(){
        //buscamos el proveedor seleccionado, para crear la relacion producto+proveedor en caso de no existir
        ProviderRow row = null;

        for(ProviderRow _row : _list)
            if(_row.isCheched())
                row = _row;

        if(row != null){

            ProviderProductDao providerProductDao = new ProviderProductDao(getActivity().getContentResolver());

            //vemos si existe ya la relacion
            if(providerProductDao.existeRelacion(producto.getId(),row.getId_provider()))
                return true;
            else //no existe => la creamos
                return providerProductDao.save(new ProviderProduct(producto.getId(),row.getId_provider()));
        }
        else
            return true;

    }*/

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {//requerimos un provider
            if (resultCode == Activity.RESULT_OK) {
                Object result = data.getParcelableExtra("result");
                //NewProvider((Provider) result);
                SelectProvider((Provider) result);

                Cerrar();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                //SelectProvider(null);
            }
        }
    }

    public void SelectProvider(Provider provider)
    {
        if(mListener != null)
            mListener.onProviderSeleccionado(provider);

        Cerrar();
    }

    /*public void NewProvider(Provider provider) {
        ProviderArrayAdapter adapter = (ProviderArrayAdapter) lstProviders.getAdapter();
        adapter.clearChoices();

        _list.add(0, new ProviderRow(provider, true));

        adapter.notifyDataSetChanged();

        //layoutNewProvider.setVisibility(View.VISIBLE);
        //txtNewProvider.setText(provider.toString());
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            producto = getArguments().getParcelable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_providers, container, false);

        lstProviders = (ListView) view.findViewById(android.R.id.list);
        _list = getListProviders(producto.getId());
        lstProviders.setAdapter(new ProviderArrayAdapter(getActivity(), _list));
        lstProviders.setEmptyView(view.findViewById(android.R.id.empty));
        lstProviders.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ProviderArrayAdapter adapter = (ProviderArrayAdapter) lstProviders.getAdapter();

                SelectProvider(adapter.getItem(position));

                /*adapter.clearChoices();

                ProviderRow row = adapter.getItem(position);
                row.setCheched(!row.isCheched());*/
            }
        });
        view.findViewById(R.id.fragment_providers_btn_nuevo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ProviderManagerActivity.class);
                intent.putExtra("search", true);
                startActivityForResult(intent, 1);
            }
        });
        view.findViewById(R.id.fragment_providers_btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SelectProvider(null);
            }
        });
        //layoutNewProvider = view.findViewById(R.id.fragment_providers_layout_otro);
        //txtNewProvider = (TextView) view.findViewById(R.id.fragment_providers_txt_new_provider);

        return view;
    }

    private List<Provider> getListProviders(long _idProduct) {
        return new ProviderDao(getActivity().getContentResolver()).getProvidersProduct(_idProduct);
        /*List<ProviderRow> listRow = new ArrayList<ProviderRow>();
        for (Provider prov : listProv) {
            listRow.add(new ProviderRow(prov));
        }
        return listRow;*/
    }

    /*
    class ProviderRow {
        private boolean cheched;
        private long id_provider;
        private String tax_id; //RUT - CUIT
        private String name;

        public boolean isCheched() {
            return cheched;
        }

        public void setCheched(boolean cheched) {
            this.cheched = cheched;
        }

        public String getTax_id() {
            return tax_id;
        }

        public void setTax_id(String tax_id) {
            this.tax_id = tax_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public long getId_provider() {
            return id_provider;
        }

        public void setId_provider(long id_provider) {
            this.id_provider = id_provider;
        }

        public String getTag() {
            return this.name + this.tax_id;
        }

        ProviderRow(Provider provider) {
            this(provider, false);
        }

        ProviderRow(Provider provider, boolean _cheched) {
            this.cheched = _cheched;
            this.id_provider = provider.getId();
            this.tax_id = provider.getTax_id();
            this.name = provider.getName();
        }
    }
*/
    class ProviderArrayAdapter extends ArrayAdapter<Provider> {
        private final Context context;
        private List<Provider> items;

        private LayoutInflater layoutInflater;

        public ProviderArrayAdapter(Context _context, List<Provider> list) {
            super(_context, 0, list);
            layoutInflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            items = list;
            context = _context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null)
                convertView = layoutInflater.inflate(R.layout.list_item_provider_asociados, parent, false);

            final Provider row = items.get(position);

            /*((CheckBox) convertView.findViewById(R.id.list_chk)).setTag(row.getTag());
            ((CheckBox) convertView.findViewById(R.id.list_chk)).setChecked(row.isCheched());
            ((CheckBox) convertView.findViewById(R.id.list_chk)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton view, boolean isChecked) {

                    if (row.getTag().equals(view.getTag().toString()))
                        row.setCheched(isChecked);
                }
            });*/
            ((TextView) convertView.findViewById(R.id.list_txt_codigo)).setText(row.getTax_id());
            ((TextView) convertView.findViewById(R.id.list_txt_nombre)).setText(row.getName());

            return convertView;
        }


        /*public void clearChoices(){
            for(ProviderRow row : items)
                row.setCheched(false);
        }*/
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        /*try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
    }

    private void Cerrar() {
        FragmentManager fm = getActivity().getFragmentManager();

        Fragment fragment = fm.findFragmentById(R.id.frg_stock_edit_provider_fragment);

        FragmentTransaction ft = fm.beginTransaction();

        ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);

        ft.hide(fragment);

        ft.commit();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnProvidersInteractionListener {
        public void onProviderSeleccionado(Provider provider);
    }

}
