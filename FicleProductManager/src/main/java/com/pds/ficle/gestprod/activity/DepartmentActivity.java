package com.pds.ficle.gestprod.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.Toast;

import com.pds.common.activity.TimerActivity;
import com.pds.common.dao.ProductDao;
import com.pds.common.dao.TaxDao;
import com.pds.common.model.Product;
import com.pds.common.model.Tax;
import com.pds.ficle.gestprod.R;
import com.pds.common.dao.DepartmentDao;
import com.pds.common.dao.SubDepartmentDao;
import com.pds.common.model.Department;
import com.pds.common.model.SubDepartment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class DepartmentActivity extends TimerActivity {
    private final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");

    private Button addButton;
    private Button cancelButton;
    private Button deleteButton;
    private EditText altaEditText;
    private EditText nameEditText;
    private EditText descriptionEditText;
    private Switch genericSwitch;
    private CheckBox genericPesableCheckBox;
    private RadioButton modoListaOption;
    private RadioButton modoGrillaOption;
    private EditText codigoEditText;

    private String name;
    private String alta;
    private String description;
    private boolean generico;
    private boolean genericoPesable;
    private int modo_visualizacion;
    private String codigo;

    private Department department;

    private boolean edit = false;

    private DepartmentTask departmentTask = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_department);

        alta = DATE_FORMAT.format(new Date());

        Intent intent = getIntent();
        department = intent.getParcelableExtra("department");
        if (department == null) {
            department = new Department();
            department.setAutomatic(0);
            alta = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
            generico = false;
            genericoPesable = false;
            modo_visualizacion = 0;
        } else {
            edit = true;
            name = department.getName();
            alta = new SimpleDateFormat("dd/MM/yyyy").format(department.getAlta());
            description = department.getDescription();
            generico = department.isGenericDept();
            genericoPesable = department.isGenericPesableDept();
            modo_visualizacion = department.getVisualization_mode();
            codigo = department.getCodigo();
        }

        altaEditText = (EditText) findViewById(R.id.department_alta_date);
        altaEditText.setText(alta);

        nameEditText = (EditText) findViewById(R.id.department_name);
        nameEditText.setText(name);

        descriptionEditText = (EditText) findViewById(R.id.department_description);
        descriptionEditText.setText(description);

        codigoEditText = (EditText) findViewById(R.id.department_codigo);
        codigoEditText.setText(codigo);

        genericPesableCheckBox = (CheckBox)findViewById(R.id.department_generic_pesable);
        genericSwitch = (Switch) findViewById(R.id.department_generic);
        genericSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                genericPesableCheckBox.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            }
        });
        genericSwitch.setChecked(generico);
        genericPesableCheckBox.setVisibility(genericSwitch.isChecked() ? View.VISIBLE : View.GONE);
        genericPesableCheckBox.setChecked(genericoPesable);

        modoListaOption = (RadioButton) findViewById(R.id.department_rbt_modo_lista);
        modoListaOption.setChecked(modo_visualizacion == 0);
        modoGrillaOption = (RadioButton) findViewById(R.id.department_rbt_modo_grilla);
        modoGrillaOption.setChecked(modo_visualizacion == 1);

        cancelButton = (Button) findViewById(R.id.department_cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        addButton = (Button) findViewById(R.id.department_add_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate();
            }
        });

        if (edit) {
            deleteButton = (Button) findViewById(R.id.department_delete);
            deleteButton.setVisibility(View.VISIBLE);
            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    delete();
                }
            });

            adjustEditMode();
        }
    }

    public void adjustEditMode(){
        if(department.getAutomatic() == 1){
            //no puede grabar
            addButton.setVisibility(View.INVISIBLE);
            //no tiene la opcion de eliminar
            deleteButton.setVisibility(View.INVISIBLE);
            //no puede editar el nombre
            nameEditText.setEnabled(false);
            //no puede editar descripcion
            descriptionEditText.setEnabled(false);
            //no puede editar modo visualizacion
            modoListaOption.setEnabled(false);
            modoGrillaOption.setEnabled(false);
            //no puede editar condicion del depto
            genericSwitch.setEnabled(false);
            genericPesableCheckBox.setEnabled(false);
        }
    }

    public void validate() {
        if (departmentTask != null) {
            return;
        }

        boolean cancel = false;

        // Reset errors.
        nameEditText.setError(null);
        descriptionEditText.setError(null);

        name = nameEditText.getText().toString();
        alta = altaEditText.getText().toString();
        description = descriptionEditText.getText().toString();
        generico = genericSwitch.isChecked();
        genericoPesable = genericPesableCheckBox.isChecked();
        modo_visualizacion = modoGrillaOption.isChecked() ? 1 : 0;

        if (TextUtils.isEmpty(name)) {
            nameEditText.setError(getString(R.string.error_field_required));
            cancel = true;
        }

        /*if (TextUtils.isEmpty(description)) {
            descriptionEditText.setError(getString(R.string.error_field_required));
            cancel = true;
        }*/

        if(generico) {
            //SI ESTA MARCADO COMO GENERICO => BUSCAMOS QUE NO TENGA PRODUCTOS ASOCIADOS
            List<Product> products = new ProductDao(getContentResolver()).list(String.format("department_id = %d and removed = 0 and generic = 0", department.getId()), null, null);
            if (products.size() > 0) {
                //SI TIENE PRODUCTOS ASOCIADOS => ADVERTIMOS QUE DEBE DESASOCIARLOS PREVIAMENTE
                Toast.makeText(this, "Error: El departamento tiene productos dependientes. Debe desasociarlos previamente para poder convertirlo a Departamento Genérico.", Toast.LENGTH_LONG).show();
                cancel = true;
            }
            else{

                //BUSCAMOS QUE NO TENGA SUBDEPARTAMENTOS ASOCIADOS
                List<SubDepartment> subdeptos = new SubDepartmentDao(getContentResolver()).list(String.format("department_id = %d", department.getId()), null, null);
                if(subdeptos.size()>0){
                    //SI TIENE SUBDEPTOS ASOCIADOS => ADVERTIMOS QUE DEBE DESASOCIARLOS PREVIAMENTE
                    Toast.makeText(this, "Error: El departamento tiene subdepartamentos dependientes. Debe desasociarlos previamente para poder convertirlo a Departamento Genérico.", Toast.LENGTH_LONG).show();
                    cancel = true;
                }
            }
        }

        if (!cancel) {
            departmentTask = new DepartmentTask();
            departmentTask.execute((Void) null);
        }
    }

    public void delete() {
        List<SubDepartment> subDepartments = new SubDepartmentDao(getContentResolver())
                .list(String.format("department_id = %d", department.getId()), null, null);
        if (subDepartments.size() == 0) {
            new DepartmentDao(getContentResolver()).delete(department);
            Toast.makeText(getApplicationContext(), R.string.message_delete_success_department, Toast.LENGTH_SHORT).show();
            onBackPressed();
        } else {
            Toast.makeText(getApplicationContext(), R.string.message_delete_denied_department, Toast.LENGTH_SHORT).show();
        }
    }

    public class DepartmentTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {
            department.setDescription(description);
            department.setName(name);
            department.setGeneric_dept(generico ? (genericoPesable ? 2 : 1) : 0);
            department.setVisualization_mode(modo_visualizacion);
            try {
                department.setAlta(DATE_FORMAT.parse(alta));
            } catch (ParseException e) {
                Log.d("DepartmentActivity", "Alta date parse error.");
            }
            boolean result = new DepartmentDao(getContentResolver()).saveOrUpdate(department);

            if(department.isGenericDept()){
                //hay que crear un producto generico
                ProductDao productDao = new ProductDao(getContentResolver());
                List<Product> products = productDao.list(String.format("department_id = %d and removed = 0 and generic = 1" , department.getId()),null,null );

                Product product ;
                if(products.size() > 0){
                    product = products.get(0); //DEBERIA TENER SOLO UN PRODUCTO, QUE ES EL GENERICO
                }
                else {
                    product = new Product();
                }

                product.setIdDepartment(department.getId());
                product.setWeighable(department.isGenericPesableDept());
                product.setName(department.getName());
                product.setDescription(department.getDescription());
                product.setGeneric(true);
                product.setAlta(new Date());
                product.setSalePrice(0);
                product.setStock(0);
                product.setPurchasePrice(0);
                product.setMinStock(0);
                product.setContenido(0);
                product.setBrand("");
                product.setCode("");
                product.setProvider(null);
                product.setUnit("");
                product.setPresentation("");
                List<Tax> _tax = new TaxDao(getContentResolver()).list("name = 'Gravado'" , null, null);
                product.setTax(_tax.get(0));
                product.setIva(_tax.get(0).getAmount());

                result = productDao.saveOrUpdate(product);
            }

            return result;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            departmentTask = null;

            CharSequence text;
            if (success)
                text = getString(R.string.message_save_success_department);
            else
                text = getString(R.string.message_save_failed_department);
            Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();

            if (success)
                onBackPressed();
        }

        @Override
        protected void onCancelled() {
            departmentTask = null;
        }
    }

}
