package com.pds.ficle.gestprod.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.pds.common.Config;
import com.pds.common.Formato;
import com.pds.common.Logger;
import com.pds.common.Usuario;
import com.pds.common.activity.Actividad;
import com.pds.common.activity.TimerActivity;
import com.pds.common.dao.DepartmentDao;
import com.pds.common.dao.ProductDao;
import com.pds.common.dao.SubDepartmentDao;
import com.pds.common.dao.TaxDao;
import com.pds.common.db.DepartmentTable;
import com.pds.common.db.ProductTable;
import com.pds.common.db.SubdepartmentTable;
import com.pds.common.fragment.ProductosFragment;
import com.pds.common.hardware.ScannerPOWA;
import com.pds.common.model.Department;
import com.pds.common.model.Product;
import com.pds.common.model.SubDepartment;
import com.pds.common.util.Sku_Utils;
import com.pds.common.util.UriUtils;
import com.pds.common.util.Window;
import com.pds.ficle.gestprod.R;
import com.pds.ficle.gestprod.fragment.WizardStep;
import com.pds.ficle.gestprod.fragment.WizardStepChangeListener;
import com.pds.ficle.gestprod.util.ProductCodeTextWatcher;
import com.pds.ficle.gestprod.util.SkuUtils;
import com.pds.ficle.gestprod.util.SpinnerUtils;
import com.pds.ficle.gestprod.wizard.AltaConSkuWizard;
import com.pds.ficle.gestprod.wizard.AltaSinSkuWizard;
import com.pds.ficle.gestprod.wizard.BaseAltaWizard;
import com.pds.ficle.gestprod.wizard.ProductWizardListener;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

public class ProductWizardActivity extends Actividad implements ProductWizardListener {

    //private List<WizardStep> steps;
    private Product _product;
    private Formato.Decimal_Format FORMATO_DECIMAL;
    private Config config;
    private boolean editMode;
    private Button btnCancelar;
    private Usuario loggedUser;
    private WizardStep stepAltaSinSKU, stepPesables, stepAltaSinSKUOtros, stepAltaSinSKUOtros2, stepAltaSinSKUCateg;
    private BaseAltaWizard altaWizard;

    @Override
    public void openProduct(Product p) {
        //llamamos al ABM de producto
        Intent intent = new Intent(ProductWizardActivity.this, NewProductActivity.class);
        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//con esto, hacemos que el EDIT vuelva al search, no al wizard
        intent.putExtra("product", p);
        intent.putExtra("current_user", loggedUser);
        startActivity(intent);
        overridePendingTransition(0, 0);

        finish();

        //txtCode.setError("Codigo ya asociado");
        //txtCode.requestFocus();
    }

    @Override
    public void restartWizard() {
        if(editMode)
            cancelWizard();
        else {
            //WizardStep step0 = new WizardStep(R.string.prod_wiz_first_step, R.layout.product_wiz_step_start, step0Change, View.GONE);

            //mostramos el primero
            //execWizStep(step0);

            //title
            ((TextView) findViewById(R.id.prod_wiz_lbl_title)).setText(R.string.prod_wiz_first_step);

            findViewById(R.id.prod_wiz_btn_back).setVisibility(View.INVISIBLE);
            findViewById(R.id.prod_wiz_btn_next).setVisibility(View.GONE);

            //inflate view
            LinearLayout container = ((LinearLayout) findViewById(R.id.prod_wiz_lay_step));
            container.removeAllViews();
            View view = getLayoutInflater().inflate(R.layout.product_wiz_step_start, null);
            container.addView(view, 0);

            ((RadioButton) findViewById(R.id.prod_wiz_step_start_grp_sku).findViewById(R.id.prod_wiz_step_start_rbt_con_sku)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    /*MODO_ALTA_SIN_SKU = false;
                    execWizStep(nextStep);*/

                    AltaConSkuWizard altaConSkuWizard = new AltaConSkuWizard(ProductWizardActivity.this, ProductWizardActivity.this, _product, config);
                    altaWizard = altaConSkuWizard;
                    altaConSkuWizard.startWizard();

                }
            });

            ((RadioButton) findViewById(R.id.prod_wiz_step_start_grp_sku).findViewById(R.id.prod_wiz_step_start_rbt_sin_sku)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    /*MODO_ALTA_SIN_SKU = true;
                    execWizStep(stepAltaSinSKU);*/

                    AltaSinSkuWizard altaSinSkuWizard = new AltaSinSkuWizard(ProductWizardActivity.this, ProductWizardActivity.this, _product, config);
                    altaWizard = altaSinSkuWizard;
                    altaSinSkuWizard.startWizard();
                }
            });

            Window.CloseSoftKeyboard(ProductWizardActivity.this);
        }
    }

    @Override
    public boolean saveProduct(boolean updateProd, Product p) {

        boolean success = false;

        if (!updateProd) {
            //crear producto
            success = new ProductDao(getContentResolver()).save(p);

            if (success) {
                Logger.RegistrarEvento(getContentResolver(), "i", "ALTA PRODUCTO MANUAL", "SKU: " + p.getCode(), "id: " + String.valueOf(p.getId()));
                Toast.makeText(ProductWizardActivity.this, getString(R.string.message_create_success_product), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(ProductWizardActivity.this, getString(R.string.message_create_failed_product), Toast.LENGTH_SHORT).show();
            }

        } else {
            //modificar producto
            success = new ProductDao(getContentResolver()).saveOrUpdate(p);

            if (success) {
                Toast.makeText(ProductWizardActivity.this, getString(R.string.message_modified_success_product), Toast.LENGTH_SHORT).show();

                //devolvemos el producto actualizado
                Intent i = new Intent();
                i.putExtra("edit_product", p);
                setResult(1, i);

            } else {
                Toast.makeText(ProductWizardActivity.this, getString(R.string.message_modified_failed_product), Toast.LENGTH_SHORT).show();
            }
        }

        return success;
    }

    @Override
    public void finishWizard() {
        //ocultamos el teclado al ingresar
        Window.CloseSoftKeyboard(ProductWizardActivity.this);

        finish();//salimos
        //volver();//salimos
    }

    private void cancelWizard(){
        //ocultamos el teclado al ingresar
        Window.CloseSoftKeyboard(ProductWizardActivity.this);

        if (editMode) {
            setResult(0);
        }

        //finish();//salimos
        volver();//salimos
    }

    String codigoIngresado = "";

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (event.isPrintingKey() || keyCode == KeyEvent.KEYCODE_ENTER) {
            if (keyCode == KeyEvent.KEYCODE_ENTER) {
                if(altaWizard != null)
                    altaWizard.inputByScannerSkuStep(codigoIngresado);
                codigoIngresado = "";
            } else {
                codigoIngresado += String.valueOf(event.getDisplayLabel());
            }
            return true;
        } else {
            return super.onKeyUp(keyCode, event);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_wizard);

        //cancelar
        btnCancelar = (Button) findViewById(R.id.prod_wiz_btn_cancel);
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelWizard();
                /*
                //ocultamos el teclado al ingresar
                Window.CloseSoftKeyboard(ProductWizardActivity.this);

                if (editMode) {
                    setResult(0);
                }

                //finish();//salimos
                volver();//salimos
                */
            }
        });

        //formato
        config = new Config(this);
        FORMATO_DECIMAL = Formato.getDecimal_Format(config.PAIS);

        //product
        Intent intent = getIntent();
        _product = intent.getParcelableExtra("product");//si se recibe es un edit, sino es un new
        loggedUser = (Usuario) intent.getSerializableExtra("current_user");

        editMode = _product != null;

        if(editMode){
            //edición de un producto
            AltaConSkuWizard altaConSkuWizard = new AltaConSkuWizard(ProductWizardActivity.this, ProductWizardActivity.this, _product, config);
            altaWizard = altaConSkuWizard;
            altaConSkuWizard.startWizard();
        }
        else{
            restartWizard();
        }

        /*editMode = product != null;

        if (editMode) {
            //algunas acciones para el edit...
            //1. si falta alguna entidad relacionada, las cargamos
            if (product.getDepartment() == null || product.getSubDepartment() == null || product.getTax() == null)
                product.LoadDepartmentSubdepartmentTax(getContentResolver());
        }*/




/*

        //armamos los pasos del wizard
        WizardStep step0 = new WizardStep(R.string.prod_wiz_first_step, R.layout.product_wiz_step_start, step0Change, View.GONE);

        stepAltaSinSKU = new WizardStep(R.string.prod_wiz_no_sku, R.layout.product_wiz_step_no_sku_cate_prev, stepSinSKUChange, View.GONE);
        stepAltaSinSKUOtros = new WizardStep(R.string.prod_wiz_no_sku, R.layout.product_wiz_step_no_sku_cate_depto, stepAltaSinSKUOtrosChange, View.GONE);
        stepAltaSinSKUOtros2 = new WizardStep(R.string.prod_wiz_no_sku, R.layout.product_wiz_step_no_sku_cate_subdepto, stepAltaSinSKUOtros2Change, View.GONE);
        stepAltaSinSKUCateg = new WizardStep(R.string.prod_wiz_no_sku_cate_grid, R.layout.product_wiz_step_no_sku_cate_grid, stepAltaSinSKUCategChange, View.GONE);

        WizardStep step1 = new WizardStep(R.string.prod_wiz_cod_barra, R.layout.product_wiz_step_cod_barra, stepCodBarrasChange);
        WizardStep step2 = new WizardStep(R.string.prod_wiz_marca, R.layout.product_wiz_step_marca, stepMarcaChange);
        WizardStep step3 = new WizardStep(R.string.prod_wiz_nombre, R.layout.product_wiz_step_nombre, stepNombreChange);
        WizardStep step4 = new WizardStep(R.string.prod_wiz_pesable, R.layout.product_wiz_step_pesable, stepPesableChange);

        WizardStep step411 = new WizardStep(R.string.prod_wiz_unidad, R.layout.product_wiz_step_unidad, stepUnidadChange);//, View.GONE);
        WizardStep step412 = new WizardStep(R.string.prod_wiz_contenido, R.layout.product_wiz_step_contenido, stepContenidoChange);

        stepPesables = new WizardStep(R.string.prod_wiz_unidad_pesable, R.layout.product_wiz_step_medida_pesable, stepMedidaPesableChange, View.GONE);

        WizardStep step5 = new WizardStep(R.string.prod_wiz_categoria, R.layout.product_wiz_step_categoria, stepCategoriaChange);
        WizardStep step6 = new WizardStep(R.string.prod_wiz_precio_vta, R.layout.product_wiz_step_precio, stepPrecioChange);
        WizardStep step7 = new WizardStep(R.string.prod_wiz_descripcion, R.layout.product_wiz_step_descripcion, stepDescriptionChange);
        //WizardStep step9 = new WizardStep(R.string.prod_wiz_stock_min, R.layout.product_wiz_step_stock_min, stepStockMinChange);
        WizardStep end = new WizardStep(R.string.prod_wiz_precio_valida, R.layout.product_wiz_step_validar, stepEndChange);

        if (editMode && product.getAutomatic() == Product.TYPE_AUTOM.PROD_ALTA_AUTOM) {
            //si estamos editando un producto generado por catalogo, reducimos las opciones a editar
            step7.flows(null, end);
            end.flows(step7, null);

            steps = new ArrayList<WizardStep>();
            steps.add(step7);
            steps.add(end);

            //mostramos el primero
            execWizStep(step7);
        } else {

            //alta o edit normal
            step0.flows(null, step1);

            //flujo alternativo para alta SIN SKU
            stepAltaSinSKU.flows(step0, stepAltaSinSKUCateg);
            stepAltaSinSKUOtros.flows(stepAltaSinSKU, stepAltaSinSKUOtros2);
            stepAltaSinSKUOtros2.flows(stepAltaSinSKUOtros, stepAltaSinSKUCateg);
            stepAltaSinSKUCateg.flows(stepAltaSinSKU, step6);

            step1.flows(step0, step2);
            step2.flows(step1, step3);
            step3.flows(step2, step4);
            step4.flows(step3, step411);//va al 411 porque x def tenemos NO PESABLES

            step411.flows(step4, step412);
            step412.flows(step411, step5);

            stepPesables.flows(step4, step5);

            step5.flows(step4, step6);
            step6.flows(step5, step7);
            step7.flows(step6, end);
            end.flows(step7, null);

            steps = new ArrayList<WizardStep>();
            steps.add(step0);
            steps.add(step1);
            steps.add(step2);
            steps.add(step3);
            steps.add(step4);
            steps.add(step411);
            steps.add(step412);
            steps.add(stepPesables);
            steps.add(step5);
            steps.add(step6);
            steps.add(step7);
            steps.add(end);

            //mostramos el primero
            execWizStep(step0);
        }
*/

    }

    private ScannerPOWA _scanner;

    @Override
    protected void onResume() {
        super.onResume();

        Log.d("Test", "Wiz-Resume");

        try {
            //si tenemos una printer POWA, entonces iniciamos el scanner POWA S10
            if (config.PRINTER_MODEL != null && config.PRINTER_MODEL.equals("POWA")) {
                _scanner = new ScannerPOWA(this);
                _scanner._scannerCallback = new ScannerPOWA.ScannerCallback() {
                    @Override
                    public void onScannerRead(String text) {
                        if(altaWizard != null)
                            altaWizard.inputByScannerSkuStep(text);
                    }
                };
            }
        } catch (Exception ex) {
            Toast.makeText(ProductWizardActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();

        if (altaWizard != null) {
            if(!altaWizard.previousStep())
                volver();
        }
        else
            volver();
    }

    private void volver() {
        super.onBackPressed();//usamos el back pressed del super...
    }

    /*private boolean MODO_ALTA_SIN_SKU = false;
    private boolean STEP_SKU = false;*/
/*

    private WizardStepChangeListener step0Change = new WizardStepChangeListener() {
        //private WizardStep nextStep;

        @Override
        public void onStepBack() {
            //no hay paso anterior...
        }

        @Override
        public void onStepShow(final WizardStep _step) {
            */
/*nextStep = _step.nextStep;
            selectedSinSKUCateCode = "";*//*


            ((RadioButton) findViewById(R.id.prod_wiz_step_start_grp_sku).findViewById(R.id.prod_wiz_step_start_rbt_con_sku)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    */
/*MODO_ALTA_SIN_SKU = false;
                    execWizStep(nextStep);*//*


                    AltaConSkuWizard altaConSkuWizard = new AltaConSkuWizard(ProductWizardActivity.this, ProductWizardActivity.this, _product, config);
                    altaWizard = altaConSkuWizard;
                    altaConSkuWizard.startWizard();

                }
            });

            ((RadioButton) findViewById(R.id.prod_wiz_step_start_grp_sku).findViewById(R.id.prod_wiz_step_start_rbt_sin_sku)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    */
/*MODO_ALTA_SIN_SKU = true;
                    execWizStep(stepAltaSinSKU);*//*


                    AltaSinSkuWizard altaSinSkuWizard = new AltaSinSkuWizard(ProductWizardActivity.this, ProductWizardActivity.this, _product, config);
                    altaWizard = altaSinSkuWizard;
                    altaSinSkuWizard.startWizard();
                }
            });

            Window.CloseSoftKeyboard(ProductWizardActivity.this);
        }

        @Override
        public boolean onStepNext() {
            return true;
        }

    };

    private WizardStep currentStep;

    public void execWizStep(WizardStep step) {
        //aqui deberíamos completar el paso

        currentStep = step;

        //title
        ((TextView) findViewById(R.id.prod_wiz_lbl_title)).setText(step.titleId);

        final WizardStepChangeListener listener = step.stepChangeListener;

        //back
        if (step.previousStep != null) {
            final WizardStep previousStep = step.previousStep;
            findViewById(R.id.prod_wiz_btn_back).setVisibility(View.VISIBLE);
            findViewById(R.id.prod_wiz_btn_back).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        listener.onStepBack();
                    execWizStep(previousStep);
                }
            });
        } else
            findViewById(R.id.prod_wiz_btn_back).setVisibility(View.INVISIBLE);

        //next
        if (step.nextStep != null) {
            final WizardStep nextStep = step.nextStep;
            ((Button) findViewById(R.id.prod_wiz_btn_next)).setText("SIGUIENTE");
            findViewById(R.id.prod_wiz_btn_next).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        if (listener.onStepNext())
                            execWizStep(nextStep);
                    } else
                        execWizStep(nextStep);
                }
            });
            step.nextButton = ((Button) findViewById(R.id.prod_wiz_btn_next));
        } else {
            ((Button) findViewById(R.id.prod_wiz_btn_next)).setText("GRABAR");
            findViewById(R.id.prod_wiz_btn_next).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        if (listener.onStepNext()) {


                            if (editMode) {
                                //devolvemos el producto actualizado
                                Intent i = new Intent();
                                i.putExtra("edit_product", product);

                                setResult(1, i);
                            }

                            finish();//salimos
                        }
                }
            });
        }
        ((Button) findViewById(R.id.prod_wiz_btn_next)).setVisibility(step.nextButtonViewMode);


        //inflate view
        LinearLayout container = ((LinearLayout) findViewById(R.id.prod_wiz_lay_step));
        container.removeAllViews();

        if (step.viewId != -1) {

            View view = getLayoutInflater().inflate(step.viewId, null);
            container.addView(view, 0);

        }

        //ejecutamos el evento
        if (listener != null)
            listener.onStepShow(step);
    }
*/





    @Override
    protected void onPause() {
        if (_scanner != null) {
            _scanner.end();
            _scanner = null;
        }

        Log.d("Test", "Wiz-Pause");

        super.onPause();
    }

}
