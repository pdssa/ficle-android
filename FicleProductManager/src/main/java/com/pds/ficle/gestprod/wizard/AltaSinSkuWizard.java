package com.pds.ficle.gestprod.wizard;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Space;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Formato;
import com.pds.common.dao.DepartmentDao;
import com.pds.common.dao.ProductDao;
import com.pds.common.dao.SubDepartmentDao;
import com.pds.common.dao.TaxDao;
import com.pds.common.db.DepartmentTable;
import com.pds.common.db.ProductTable;
import com.pds.common.db.SubdepartmentTable;
import com.pds.common.model.Department;
import com.pds.common.model.Product;
import com.pds.common.model.SubDepartment;
import com.pds.common.util.UriUtils;
import com.pds.common.util.Window;
import com.pds.ficle.gestprod.R;
import com.pds.ficle.gestprod.fragment.WizardStep;
import com.pds.ficle.gestprod.fragment.WizardStepChangeListener;
import com.pds.ficle.gestprod.util.SkuUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Hernan on 05/12/2016.
 */
public class AltaSinSkuWizard extends BaseAltaWizard {

    private WizardStep stepAltaSinSKUCateg, stepAltaSinSKUOtros, stepAltaSinSKUOtros2;
    private Product product;
    private WizardStep stepAltNombre, stepAltPesables;
    private Department selectedSinSKUDepto;
    private String selectedSinSKUCateCode;
    private boolean editMode;

    public AltaSinSkuWizard(Activity context, ProductWizardListener productWizardListener, Product p, Config config) {
        super(context, productWizardListener, config);


        WizardStep stepAltaSinSKU = new WizardStep(R.string.prod_wiz_no_sku, R.layout.product_wiz_step_no_sku_cate_prev, stepSinSKUChange, View.GONE);
        stepAltaSinSKUOtros = new WizardStep(R.string.prod_wiz_no_sku, R.layout.product_wiz_step_no_sku_cate_depto, stepAltaSinSKUOtrosChange, View.GONE);
        stepAltaSinSKUOtros2 = new WizardStep(R.string.prod_wiz_no_sku, R.layout.product_wiz_step_no_sku_cate_subdepto, stepAltaSinSKUOtros2Change, View.GONE);
        stepAltaSinSKUCateg = new WizardStep(R.string.prod_wiz_no_sku_cate_grid, R.layout.product_wiz_step_no_sku_cate_grid, stepAltaSinSKUCategChange, View.GONE);
        WizardStep stepPrecioVenta = new WizardStep(R.string.prod_wiz_precio_vta, R.layout.product_wiz_step_precio, stepPrecioChange);
        WizardStep stepDescripcion = new WizardStep(R.string.prod_wiz_descripcion, R.layout.product_wiz_step_descripcion, stepDescriptionChange);
        WizardStep end = new WizardStep(R.string.prod_wiz_precio_valida, R.layout.product_wiz_step_validar, stepEndChange);


        stepAltaSinSKU.flows(null, stepAltaSinSKUCateg);
        stepAltaSinSKUOtros.flows(stepAltaSinSKU, stepAltaSinSKUOtros2);
        stepAltaSinSKUOtros2.flows(stepAltaSinSKUOtros, stepAltaSinSKUCateg);
        stepAltaSinSKUCateg.flows(stepAltaSinSKU, stepPrecioVenta);
        stepPrecioVenta.flows(stepAltaSinSKUCateg, stepDescripcion);
        stepDescripcion.flows(stepPrecioVenta, end);
        end.flows(stepDescripcion, null);


        steps = new ArrayList<WizardStep>();
        steps.add(stepAltaSinSKU);
        steps.add(stepAltaSinSKUOtros);
        steps.add(stepAltaSinSKUOtros2);
        steps.add(stepAltaSinSKUCateg);
        steps.add(stepPrecioVenta);
        steps.add(stepDescripcion);
        steps.add(end);


        //flujo de alta sin SKU
        //stepAltaSinSKUCateg -> puedo saltar a estos flujos
        stepAltNombre = new WizardStep(R.string.prod_wiz_nombre, R.layout.product_wiz_step_nombre, stepNombreChange);
        WizardStep stepAltPesableAfecto = new WizardStep(R.string.prod_wiz_pesable, R.layout.product_wiz_step_pesable, stepPesableChange);

        WizardStep stepAltPesableAfectoUnidad = new WizardStep(R.string.prod_wiz_unidad, R.layout.product_wiz_step_unidad, stepUnidadChange);//, View.GONE);
        WizardStep stepAltPesableAfectoContenido = new WizardStep(R.string.prod_wiz_contenido, R.layout.product_wiz_step_contenido, stepContenidoChange);

        WizardStep stepAltPrecioVta = new WizardStep(R.string.prod_wiz_precio_vta, R.layout.product_wiz_step_precio, stepPrecioChange);
        WizardStep stepAltDescripcion = new WizardStep(R.string.prod_wiz_descripcion, R.layout.product_wiz_step_descripcion, stepDescriptionChange);


        stepAltPesables = new WizardStep(R.string.prod_wiz_unidad_pesable, R.layout.product_wiz_step_medida_pesable, stepMedidaPesableChange, View.GONE);

        stepAltNombre.flows(stepAltaSinSKUCateg, stepAltPesableAfecto);
        stepAltPesableAfecto.flows(stepAltNombre, stepAltPesableAfectoUnidad);//va al stepAltPesableAfectoUnidad porque x def tenemos NO PESABLES
        stepAltPesableAfectoUnidad.flows(stepAltPesableAfecto, stepAltPesableAfectoContenido);
        stepAltPesableAfectoContenido.flows(stepAltPesableAfectoUnidad, stepAltPrecioVta);

        stepAltPesables.flows(stepAltPesableAfecto, stepAltPrecioVta);

        stepAltPrecioVta.flows(stepAltPesableAfecto, stepAltDescripcion);
        stepAltDescripcion.flows(stepAltPrecioVta, end);

    }

    private WizardStepChangeListener stepSinSKUChange = new WizardStepChangeListener() {

        private int[] options = new int[]{
                R.id.prod_wiz_step_start_no_sku_cate_prev_c_otro,
                R.id.prod_wiz_step_start_no_sku_cate_prev_c_fruta,
                R.id.prod_wiz_step_start_no_sku_cate_prev_c_verd,
                R.id.prod_wiz_step_start_no_sku_cate_prev_c_panad,
                R.id.prod_wiz_step_start_no_sku_cate_prev_c_huevo,
                R.id.prod_wiz_step_start_no_sku_cate_prev_c_env
        };

        @Override
        public void onStepBack() {
            //no hay paso anterior...
        }

        @Override
        public void onStepShow(WizardStep _step) {
            for (int opt : options) {
                ((RadioButton) findViewById(R.id.prod_wiz_step_start_no_sku_cate_prev_cates).findViewById(opt)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked)
                            optionSelected(buttonView.getId());
                    }
                });
            }

            Window.CloseSoftKeyboard(activity);
        }

        @Override
        public boolean onStepNext() {
            return true;
        }

        private void optionSelected(int categoriaSelected) {
            //revisamos la opcion de categoria seleccionada
            switch (categoriaSelected) {
                case R.id.prod_wiz_step_start_no_sku_cate_prev_c_otro: {
                    //tenemos que ir al paso para mostrar los deptos
                    execWizStep(stepAltaSinSKUOtros);
                    return;
                }
                default: {
                    //usamos el codigo de categoria
                    String codCate = String.valueOf((findViewById(categoriaSelected)).getTag());
                    showCategoryGrid(codCate);
                    return;
                }
            }

        }

    };

    private void showCategoryGrid(String codCategory) {
        selectedSinSKUCateCode = codCategory;

        execWizStep(stepAltaSinSKUCateg);
    }

    private WizardStepChangeListener stepAltaSinSKUOtrosChange = new WizardStepChangeListener() {

        private int[] options = new int[]{
                R.id.prod_wiz_step_start_no_sku_cate_d_abar,
                R.id.prod_wiz_step_start_no_sku_cate_d_alim,
                R.id.prod_wiz_step_start_no_sku_cate_d_ambi,
                R.id.prod_wiz_step_start_no_sku_cate_d_artm,
                R.id.prod_wiz_step_start_no_sku_cate_d_bebid,
                R.id.prod_wiz_step_start_no_sku_cate_d_cuid,
                R.id.prod_wiz_step_start_no_sku_cate_d_limp,
                R.id.prod_wiz_step_start_no_sku_cate_d_taba
                //,R.id.prod_wiz_step_start_no_sku_cate_d_vari
        };

        @Override
        public void onStepBack() {
            //no hay paso anterior...
        }

        @Override
        public void onStepShow(WizardStep _step) {
            selectedSinSKUDepto = null;

            for (int opt : options) {
                ((RadioButton) findViewById(R.id.prod_wiz_step_start_no_sku_cate_deptos).findViewById(opt)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked)
                            optionSelected(buttonView.getId());
                    }
                });
            }

            Window.CloseSoftKeyboard(activity);
        }

        @Override
        public boolean onStepNext() {
            return true;
        }

        private void optionSelected(int categoriaSelected) {
            //usamos el codigo de categoria
            String codCate = String.valueOf((findViewById(categoriaSelected)).getTag());

            selectedSinSKUDepto = new DepartmentDao(activity.getContentResolver()).first(DepartmentTable.COLUMN_CODIGO + "='" + codCate + "' and " + DepartmentTable.COLUMN_GENERIC + "= 0", null, null);

            if (selectedSinSKUDepto != null)
                execWizStep(stepAltaSinSKUOtros2);
            else
                Toast.makeText(activity, "Ha ocurrido un error, no es posible continuar", Toast.LENGTH_SHORT).show();
        }
    };

    private WizardStepChangeListener stepAltaSinSKUOtros2Change = new WizardStepChangeListener() {

        @Override
        public void onStepBack() {
            //no hay paso anterior...
        }

        @Override
        public void onStepShow(WizardStep _step) {

            List<SubDepartment> subDepartments = new SubDepartmentDao(activity.getContentResolver()).list(SubdepartmentTable.COLUMN_DEPARTMENT_ID + "=" + String.valueOf(selectedSinSKUDepto.getId()) + " and " + SubdepartmentTable.COLUMN_GENERIC + "= 0", null, SubdepartmentTable.COLUMN_NAME);

            com.pds.common.ui.RadioGridGroup group = (com.pds.common.ui.RadioGridGroup) findViewById(R.id.prod_wiz_step_start_no_sku_cate_subdeptos);
            TableRow currentRow = null;

            for (int i = 0; i < subDepartments.size(); i++) {

                if (i % 2 == 0) {
                    //es par, tenemos que agregar un TableRow
                    currentRow = new TableRow(activity);

                    //ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) group.getLayoutParams();
                    TableLayout.LayoutParams lp = new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    lp.setMargins(0, 0, 0, 5);
                    currentRow.setLayoutParams(lp);

                    group.addView(currentRow);
                }

                RadioButton radioButton = new RadioButton(activity);
                radioButton.setText(subDepartments.get(i).getName().toUpperCase());
                radioButton.setMaxLines(1);
                radioButton.setEllipsize(TextUtils.TruncateAt.END);
                radioButton.setTag(subDepartments.get(i).getCodigo());
                radioButton.setChecked(false);
                radioButton.setButtonDrawable(new StateListDrawable());//idem android:button="@null"
                radioButton.setBackgroundResource(R.drawable.custom_radio_button);
                radioButton.setTypeface(Typeface.DEFAULT_BOLD);
                radioButton.setTextSize(30);
                radioButton.setTextColor(activity.getResources().getColorStateList(R.color.custom_radio_color));
                //radioButton.setTextColor(activity.getResources().getColor(R.color.custom_radio_color));
                radioButton.setGravity(Gravity.CENTER);
                TableRow.LayoutParams lp = new TableRow.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 0.5f);
                if (i % 2 == 0)
                    lp.setMargins(0, 0, 20, 0);
                else
                    lp.setMargins(20, 0, 0, 0);
                radioButton.setLayoutParams(lp);
                radioButton.setPadding(40, 20, 40, 20);
                radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked)
                            optionSelected(String.valueOf(buttonView.getTag()));
                    }
                });

                currentRow.addView(radioButton);
            }

            if (subDepartments.size() % 2 != 0) {
                //cantidad impar, rellenamos el ultimo
                Space space = new Space(activity);
                TableRow.LayoutParams lp = new TableRow.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 0.5f);
                lp.setMargins(20, 0, 0, 0);
                space.setLayoutParams(lp);
                currentRow.addView(space);
            }

            Window.CloseSoftKeyboard(activity);
        }

        @Override
        public boolean onStepNext() {
            return true;
        }

        private void optionSelected(String categoriaSelected) {
            //usamos el codigo de subcategoria
            showCategoryGrid(categoriaSelected);
        }

    };

    private WizardStepChangeListener stepAltaSinSKUCategChange = new WizardStepChangeListener() {

        @Override
        public void onStepBack() {
            //no hay paso anterior...
        }

        @Override
        public void onStepShow(final WizardStep _step) {

            Window.CloseSoftKeyboard(activity);

            GridView grdProductos = (GridView) findViewById(R.id.product_wiz_step_no_sku_cate_grd);

            List<Product> productList = new ProductDao(activity.getContentResolver()).list(ProductTable.COLUMN_CODIGO_PADRE + "= '" + selectedSinSKUCateCode + "' AND removed = 0 and generic = 0 and code not like '%-%'", null, ProductTable.COLUMN_NAME);

            Product defaultProduct = new Product();
            defaultProduct.setName("OTRO");
            defaultProduct.setId(-1L);
            productList.add(defaultProduct);

            grdProductos.setAdapter(new GridAdapter(activity, productList));
            grdProductos.setOnItemClickListener(
                    new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Product p = (Product) parent.getItemAtPosition(position);

                            if (p.getId() != -1) {
                                //levantamos un producto existente => vamos a permitir editar algunas pocas caracteristicas
                                editMode = true;
                                product = p;

                                //algunas acciones para el edit...
                                //1. si falta alguna entidad relacionada, las cargamos
                                if (product.getDepartment() == null || product.getSubDepartment() == null || product.getTax() == null)
                                    product.LoadDepartmentSubdepartmentTax(activity.getContentResolver());

                                execWizStep(_step.nextStep);
                            } else {
                                //llamamos al primer step del flujo alternativo
                                editMode = false;
                                execWizStep(stepAltNombre);
                            }
                        }
                    }
            );

        }

        @Override
        public boolean onStepNext() {
            return true;
        }

    };

    private WizardStepChangeListener stepPrecioChange = new WizardStepChangeListener() {


        class PrecioWatcher implements TextWatcher {
            public String label;


            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String salePrice = s.toString().trim();

                double precio_por_decena = 0;

                if (!TextUtils.isEmpty(s))
                    precio_por_decena = Formato.ParseaDecimal(salePrice, getDecimalFormat(), true) / 10;
                else
                    precio_por_decena = 0;

                //mostramos el precio por gr / ml
                ((TextView) findViewById(R.id.prod_wiz_step_precio_lbl_precio_decena)).setText(String.format("%s: %s", label, Formato.FormateaDecimal(precio_por_decena, getDecimalFormat(), Formato.getCurrencySymbol(getDecimalFormat()))));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        }

        private PrecioWatcher precioChanged = new PrecioWatcher();

        private void asignarWatcher(boolean asignar, String label) {
            ((EditText) findViewById(R.id.prod_wiz_step_precio_txt_precio)).removeTextChangedListener(precioChanged);

            if (asignar) {
                precioChanged.label = label;
                ((EditText) findViewById(R.id.prod_wiz_step_precio_txt_precio)).addTextChangedListener(precioChanged);
            }
        }

        @Override
        public void onStepBack() {
        }

        @Override
        public void onStepShow(WizardStep _step) {
            if (product.isWeighable()) {
                //si es pesable, pedimos precio por KILO o por LITRO
                //TODO: check unit in gs1
                if (product.getUnit().equalsIgnoreCase("kg") || product.getUnit().equalsIgnoreCase("gr")) {
                    ((TextView) findViewById(R.id.prod_wiz_step_precio_lbl_precio)).setText("Precio por KILOGRAMO:");

                    asignarWatcher(true, "Precio por 100 gr");
                } else if (product.getUnit().equalsIgnoreCase("lt") || product.getUnit().equalsIgnoreCase("ml") || product.getUnit().equalsIgnoreCase("cc")) {
                    ((TextView) findViewById(R.id.prod_wiz_step_precio_lbl_precio)).setText("Precio por LITRO:");

                    asignarWatcher(true, "Precio por 100 ml");
                } else {
                    ((TextView) findViewById(R.id.prod_wiz_step_precio_lbl_precio)).setText(String.format("Precio por %s:", product.getUnit().toUpperCase()));

                    asignarWatcher(false, "");
                }

            } else {
                ((TextView) findViewById(R.id.prod_wiz_step_precio_lbl_precio)).setText("Precio por UNIDAD:");

                asignarWatcher(false, "");
            }


            //mostramos el precio de venta que teniamos
            if (product != null && product.getSalePrice() != 0)
                ((EditText) findViewById(R.id.prod_wiz_step_precio_txt_precio)).setText(Formato.FormateaDecimal(product.getSalePrice(), getDecimalFormat()));

            Window.FocusViewShowSoftKeyboard(activity, findViewById(R.id.prod_wiz_step_precio_txt_precio));

            Window.AddDoneKeyboardAction(((EditText) findViewById(R.id.prod_wiz_step_precio_txt_precio)), _step.doneAction());

            updateProductCreation(6, (TextView) findViewById(R.id.prod_wiz_step_precio_lbl_prdtemp), product);


        }

        @Override
        public boolean onStepNext() {

            //tomamos el campo precio venta ingresado

            EditText txtPrecioVta = ((EditText) findViewById(R.id.prod_wiz_step_precio_txt_precio));

            //validamos que sea obligatorio
            //txtPrecioVta.setError(null);

            String salePrice = txtPrecioVta.getText().toString().trim();

            if (TextUtils.isEmpty(salePrice)) {
                //txtPrecioVta.setError(getString(R.string.error_field_required));
                //txtPrecioVta.requestFocus();

                //ahora es opcional
                product.setSalePrice(0);

                return true;
            } else {
                product.setSalePrice(Formato.ParseaDecimal(salePrice, getDecimalFormat(), true));

                return true;
            }
        }
    };

    private WizardStepChangeListener stepDescriptionChange = new WizardStepChangeListener() {
        @Override
        public void onStepBack() {
        }

        @Override
        public void onStepShow(WizardStep _step) {
            boolean propuesto = false;

            //mostramos la descripcion que teniamos
            if (product != null) {
                if (!TextUtils.isEmpty(product.getDescription()))
                    ((EditText) findViewById(R.id.prod_wiz_step_descripcion_txt_descripcion)).setText(product.getDescription());
                else {
                    //armamos un nombre corto formado por la marca + nombre + contenido + unidad
                    ((EditText) findViewById(R.id.prod_wiz_step_descripcion_txt_descripcion)).setText(product.buildShortName());

                    propuesto = true;
                }
            }
            updateProductCreation(7, (TextView) findViewById(R.id.prod_wiz_step_descripcion_lbl_prdtemp), product);

            Window.FocusViewShowSoftKeyboard(activity, findViewById(R.id.prod_wiz_step_descripcion_txt_descripcion));

            Window.AddDoneKeyboardAction(((EditText) findViewById(R.id.prod_wiz_step_descripcion_txt_descripcion)), _step.doneAction());

            if (propuesto) {
                //((EditText) findViewById(R.id.prod_wiz_step_descripcion_txt_descripcion)).setSelection(0);
                ((EditText) findViewById(R.id.prod_wiz_step_descripcion_txt_descripcion)).selectAll();
            }
        }

        @Override
        public boolean onStepNext() {
            //tomamos el campo descripcion ingresado

            EditText txtDescript = ((EditText) findViewById(R.id.prod_wiz_step_descripcion_txt_descripcion));

            String descrip = txtDescript.getText().toString().trim();

            //validamos que sea obligatorio
            txtDescript.setError(null);

            if (TextUtils.isEmpty(descrip)) {
                txtDescript.setError(getString(R.string.error_field_required));
                txtDescript.requestFocus();
                return false;
            } else {
                product.setDescription(descrip);
                return true;
            }

        }
    };

    private WizardStepChangeListener stepEndChange = new WizardStepChangeListener() {
        @Override
        public void onStepBack() {
        }

        @Override
        public void onStepShow(WizardStep _step) {
            //cerramos el teclado
            Window.CloseSoftKeyboard(activity);

            if (TextUtils.isEmpty(product.getCode())) {
                //calculamos un SKU en base al codigo del comercio
                product.setCode(SkuUtils.GenerateSKUv2(getConfig().ID_COMERCIO, product, new ProductDao(activity.getContentResolver())));
                //calculamos un SKU en base a la categoria del producto
                //product.setCode(SkuUtils.GenerateSku(product, new ProductDao(getContentResolver())));
            }

            final String precio_formateado = Formato.FormateaDecimal(product.getSalePrice(), getDecimalFormat(), Formato.getCurrencySymbol(getDecimalFormat()));
            String precio_decenas = "";

            if (product.isWeighable()) {
                if (product.getUnit().equalsIgnoreCase("kg") || product.getUnit().equalsIgnoreCase("gr"))
                    precio_decenas = String.format("Precio x 100gr: %s", Formato.FormateaDecimal(product.getSalePrice() / 10, getDecimalFormat(), Formato.getCurrencySymbol(getDecimalFormat())));
                if (product.getUnit().equalsIgnoreCase("lt") || product.getUnit().equalsIgnoreCase("ml"))
                    precio_decenas = String.format("Precio x 100ml: %s", Formato.FormateaDecimal(product.getSalePrice() / 10, getDecimalFormat(), Formato.getCurrencySymbol(getDecimalFormat())));
            }


            final String precio_decenas_f = precio_decenas;

            findViewById(R.id.prod_wiz_step_valida_btn_print).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SkuUtils.ImprimirEtiqueta(activity, getConfig().PRINTER_MODEL, product.getCode(), product.toString(), "", false, "");
                }
            });

            findViewById(R.id.prod_wiz_step_valida_btn_print_precio).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SkuUtils.ImprimirEtiqueta(activity, getConfig().PRINTER_MODEL, product.getCode(), product.toString(), precio_formateado, true, precio_decenas_f);
                }
            });


            //mostramos los datos del producto
            if (product != null) {
                ((TextView) findViewById(R.id.prod_wiz_step_valida_lbl_nombre)).setText(product.getName());
                ((TextView) findViewById(R.id.prod_wiz_step_valida_lbl_desc_corta)).setText(product.getPrintingName());
                ((TextView) findViewById(R.id.prod_wiz_step_valida_lbl_precio)).setText(precio_formateado);
                ((Button) findViewById(R.id.prod_wiz_step_valida_btn_pesable)).setText(Html.fromHtml(
                        (product.isWeighable() ? "PESABLE" : "NO PESABLE") + "<br/>" +
                                product.getTax().getName().toUpperCase().replace("GRAVADO", "AFECTO")
                ));
                ((TextView) findViewById(R.id.prod_wiz_step_valida_lbl_sku)).setText(product.getCode());
                ((TextView) findViewById(R.id.prod_wiz_step_valida_lbl_depto)).setText(product.getDepartment().getName());
                ((TextView) findViewById(R.id.prod_wiz_step_valida_lbl_subdepto)).setText(product.getSubDepartment().getName());
                ((TextView) findViewById(R.id.prod_wiz_step_valida_lbl_marca)).setText(product.getBrand());
                ((TextView) findViewById(R.id.prod_wiz_step_valida_lbl_contenido)).setText(product.isWeighable() ? product.getUnit() : product.getContenidoFormatted() + product.getUnit());

            }

        }

        @Override
        public boolean onStepNext() {

            if (!editMode) {
                product.setAlta(new Date());
                product.setStock(0);
                product.setMinStock(0);
                product.setPurchasePrice(0);

                //ajustamos el tipo de producto
                product.setAutomatic(Product.TYPE_AUTOM.PROD_ALTA_SIN_SKU);
            }

            return productWizardListener.saveProduct(editMode, product);
        }
    };

    private WizardStepChangeListener stepNombreChange = new WizardStepChangeListener() {
        @Override
        public void onStepBack() {
            product = null;
        }

        @Override
        public void onStepShow(WizardStep _step) {
            //mostramos el nombre que teniamos
            if (product != null)
                ((EditText) findViewById(R.id.prod_wiz_step_nombre_txt_nombre)).setText(product.getName());
            else {
                product = new Product();//instanciamos un objeto producto

                //asignamos la categoria seleccionada en el paso anterior
                SubDepartment subDepartment = new SubDepartmentDao(activity.getContentResolver()).first(SubdepartmentTable.COLUMN_CODIGO + "='" + selectedSinSKUCateCode + "' ", null, null);
                if (subDepartment == null) {
                    Toast.makeText(activity, "Se ha producido un error con la categoria seleccionada...", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    product.setDepartment(subDepartment.getDepartment());//le asignamos la categoria seleccionada antes
                    product.setSubDepartment(subDepartment);
                    product.setCodigoPadre(subDepartment.getCodigo());
                }
            }

            ((TextView) findViewById(R.id.prod_wiz_step_nombre_lbl_example)).setText(Html.fromHtml(getString(R.string.prod_wiz_nombre_examp)));

            updateProductCreation(2, (TextView) findViewById(R.id.prod_wiz_step_nombre_lbl_prdtemp), product);

            Window.FocusViewShowSoftKeyboard(activity, findViewById(R.id.prod_wiz_step_nombre_txt_nombre));

            Window.AddDoneKeyboardAction(((EditText) findViewById(R.id.prod_wiz_step_nombre_txt_nombre)), _step.doneAction());

        }

        @Override
        public boolean onStepNext() {
            //tomamos el campo nombre ingresado

            EditText txtNombre = ((EditText) findViewById(R.id.prod_wiz_step_nombre_txt_nombre));

            String nombre = txtNombre.getText().toString().trim();

            //validamos que sea obligatorio
            txtNombre.setError(null);

            if (TextUtils.isEmpty(nombre)) {
                txtNombre.setError(getString(R.string.error_field_required));
                txtNombre.requestFocus();
                return false;
            } else {
                product.setName(nombre);
                return true;
            }

        }
    };

    private WizardStepChangeListener stepPesableChange = new WizardStepChangeListener() {

        @Override
        public void onStepBack() {
        }

        @Override
        public void onStepShow(WizardStep _step) {
            //mostramos los datos que teniamos
            if (product != null && product.getTax() != null) {
                //seleccionamos la opcion de pesable
                int tipo_pesable = product.isWeighable() ? R.id.prod_wiz_step_pesable_rbt_pesable : R.id.prod_wiz_step_pesable_rbt_no_pesable;

                ((RadioButton) findViewById(R.id.prod_wiz_step_pesable_grp_pesable).findViewById(tipo_pesable)).setChecked(true);

                //seleccionamos la opcion de tax
                if (product.getTax() != null) {
                    if (product.getTax().getName().toUpperCase().equalsIgnoreCase("Gravado"))
                        ((RadioButton) findViewById(R.id.prod_wiz_step_pesable_grp_iva).findViewById(R.id.prod_wiz_step_pesable_rbt_afecto)).setChecked(true);

                    else if (product.getTax().getName().toUpperCase().equalsIgnoreCase("Exento"))
                        ((RadioButton) findViewById(R.id.prod_wiz_step_pesable_grp_iva).findViewById(R.id.prod_wiz_step_pesable_rbt_no_afecto)).setChecked(true);
                }
            }

            //cerramos el teclado
            Window.CloseSoftKeyboard(activity);

            //asignamos los eventos a las opciones
            /*Button btnNoPesable = ((Button) findViewById(R.id.prod_wiz_step_pesable_btn_no_pesable));
            Button btnPesable = ((Button) findViewById(R.id.prod_wiz_step_pesable_btn_pesable));

            btnNoPesable.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PesableTypeSelected(false);
                }
            });
            btnPesable.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PesableTypeSelected(true);
                }
            });*/

            updateProductCreation(3, (TextView) findViewById(R.id.prod_wiz_step_pesable_lbl_prdtemp), product);

        }

        @Override
        public boolean onStepNext() {

            int tipo_pesable = ((RadioGroup) findViewById(R.id.prod_wiz_step_pesable_grp_pesable)).getCheckedRadioButtonId();
            int tipo_iva = ((RadioGroup) findViewById(R.id.prod_wiz_step_pesable_grp_iva)).getCheckedRadioButtonId();

            //revisamos la opcion de pesable seleccionada
            switch (tipo_pesable) {
                case R.id.prod_wiz_step_pesable_rbt_pesable: {
                    product.setWeighable(true);
                }
                break;
                case R.id.prod_wiz_step_pesable_rbt_no_pesable: {
                    product.setWeighable(false);
                }
                break;
                default: {
                    //default NO PESABLE
                    product.setWeighable(false);

                    //Toast.makeText(ProductWizardActivity.this, "Error: Debe seleccionar una opcion: PESABLE / NO PESABLE", Toast.LENGTH_SHORT).show();
                    //return false;
                }
            }

            //revisamos la opcion de IVA seleccionada
            switch (tipo_iva) {
                case R.id.prod_wiz_step_pesable_rbt_afecto: {
                    product.setIvaEnabled(true);
                    product.setTax(new TaxDao(activity.getContentResolver()).list("name = 'Gravado'", null, null).get(0));
                    product.setIva(product.getTax().getAmount());
                }
                break;
                case R.id.prod_wiz_step_pesable_rbt_no_afecto: {
                    product.setIvaEnabled(true);
                    product.setTax(new TaxDao(activity.getContentResolver()).list("name = 'Exento'", null, null).get(0));
                    product.setIva(product.getTax().getAmount());
                }
                break;
                default: {
                    //default AFECTO
                    product.setIvaEnabled(true);
                    product.setTax(new TaxDao(activity.getContentResolver()).list("name = 'Gravado'", null, null).get(0));
                    product.setIva(product.getTax().getAmount());

                    //Toast.makeText(ProductWizardActivity.this, "Error: Debe seleccionar una opcion: AFECTO / EXENTO", Toast.LENGTH_SHORT).show();
                    //return false;
                }
            }

            if (!product.isWeighable())
                return true; //vamos al paso siguiente
            else {
                //tenemos que ir al paso para productos pesables
                execWizStep(stepAltPesables);
                return false;
            }
        }



        /*private void PesableTypeSelected(boolean pesable) {
            //asignamos la opcion
            product.setWeighable(pesable);

            execWizStep(this.step.nextStep);//llamamos al siguiente step..
        }*/
    };

    private WizardStepChangeListener stepUnidadChange = new WizardStepChangeListener() {
        private WizardStep _step;

        private String _unitProd;

        @Override
        public void onStepBack() {
        }

        @Override
        public void onStepShow(WizardStep __step) {

            if (product != null)
                _unitProd = product.getUnit();

            _step = __step;

            findViewById(R.id.prod_wiz_step_contenido_btn_u_gr).setOnClickListener(new UnitClickListener("gr"));
            findViewById(R.id.prod_wiz_step_contenido_btn_u_kg).setOnClickListener(new UnitClickListener("kg"));
            findViewById(R.id.prod_wiz_step_contenido_btn_u_lt).setOnClickListener(new UnitClickListener("lt"));
            findViewById(R.id.prod_wiz_step_contenido_btn_u_ml).setOnClickListener(new UnitClickListener("ml"));
            findViewById(R.id.prod_wiz_step_contenido_btn_u_un).setOnClickListener(new UnitClickListener("un"));

            ((TextView) findViewById(R.id.prod_wiz_step_contenido_lbl_example)).setText(Html.fromHtml(getString(R.string.prod_wiz_contenido_examp)));

            updateProductCreation(4, (TextView) findViewById(R.id.prod_wiz_step_contenido_lbl_prdtemp), product);

            Window.CloseSoftKeyboard(activity);

        }

        @Override
        public boolean onStepNext() {
            //por default unidad especial (asi el sigte paso es omitido)
            if (TextUtils.isEmpty(_unitProd))
                _unitProd = "_un_";

            product.setUnit(_unitProd);

            return true;
        }

        private void assignUnit(String unit) {
            _unitProd = unit;

            _step.nextButton.performClick();

            //updateLabelContenido();
        }

        /*private void updateLabelContenido() {

            //unidad
            String contenido = ((EditText) findViewById(R.id.prod_wiz_step_contenido_txt_contenido)).getText().toString().trim();
            String lbl = String.format("%s%s", contenido, product.getUnit());

            ((TextView) findViewById(R.id.prod_wiz_step_contenido_lbl_contenido)).setText(lbl);
        }*/

        class UnitClickListener implements View.OnClickListener {

            private String unit;

            public UnitClickListener(String _unit) {
                this.unit = _unit;
            }

            @Override
            public void onClick(View v) {
                assignUnit(unit);
            }
        }
    };

    private WizardStepChangeListener stepContenidoChange = new WizardStepChangeListener() {
        @Override
        public void onStepBack() {
        }

        @Override
        public void onStepShow(WizardStep _step) {
            //mostramos el contenido que teniamos
            if (product != null) {
                if (product.getUnit().equalsIgnoreCase("_un_")) {
                    //por default una unidad
                    product.setUnit("un");
                    product.setContenido(1);
                    //saltamos al sgte paso
                    execWizStep(_step.nextStep);

                    return;
                } else if (product.getContenido() != 0) {
                    ((EditText) findViewById(R.id.prod_wiz_step_contenido_txt_contenido)).setText(product.getContenidoFormatted());
                }
            }

            ((TextView) findViewById(R.id.prod_wiz_step_contenido_lbl_contenido)).setText(String.format("Ingrese cantidad de %s:", product.getUnit().toUpperCase()));

            /*
            ((EditText) findViewById(R.id.prod_wiz_step_contenido_txt_contenido)).addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    updateLabelContenido();
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            */

            ((TextView) findViewById(R.id.prod_wiz_step_contenido_lbl_example)).setText(Html.fromHtml(getString(R.string.prod_wiz_contenido_examp)));

            updateProductCreation(4, (TextView) findViewById(R.id.prod_wiz_step_contenido_lbl_prdtemp), product);

            Window.FocusViewShowSoftKeyboard(activity, findViewById(R.id.prod_wiz_step_contenido_txt_contenido));

            Window.AddDoneKeyboardAction(((EditText) findViewById(R.id.prod_wiz_step_contenido_txt_contenido)), _step.doneAction());

        }

        @Override
        public boolean onStepNext() {

            //tomamos el campo contenido ingresado

            EditText txtContenido = ((EditText) findViewById(R.id.prod_wiz_step_contenido_txt_contenido));

            //validamos que sea obligatorio
            txtContenido.setError(null);

            String contenido = txtContenido.getText().toString().trim();

            if (TextUtils.isEmpty(contenido)) {
                //por default, contenido en 1
                product.setContenido(1);

                return true;

                //txtContenido.setError(getString(R.string.error_field_required));
                //txtContenido.requestFocus();
                //return false;
            } else {
                product.setContenido(Formato.ParseaDecimal(contenido, Formato.Decimal_Format.US));

                return true;
            }
        }

        /*private void assignUnit(String unit) {
            product.setUnit(unit);

            updateLabelContenido();
        }*/

        /*private void updateLabelContenido() {

            //unidad
            String contenido = ((EditText) findViewById(R.id.prod_wiz_step_contenido_txt_contenido)).getText().toString().trim();
            String lbl = String.format("%s%s", contenido, product.getUnit());

            ((TextView) findViewById(R.id.prod_wiz_step_contenido_lbl_contenido)).setText(lbl);
        }*/


    };

    private WizardStepChangeListener stepMedidaPesableChange = new WizardStepChangeListener() {
        private WizardStep _step;

        private String _unitProd;

        @Override
        public void onStepBack() {
        }

        @Override
        public void onStepShow(WizardStep __step) {

            if (product != null)
                _unitProd = product.getUnit();

            _step = __step;

            findViewById(R.id.prod_wiz_step_contenido_btn_u_kg).setOnClickListener(new UnitClickListener("gr"));
            findViewById(R.id.prod_wiz_step_contenido_btn_u_lt).setOnClickListener(new UnitClickListener("lt"));

            ((TextView) findViewById(R.id.prod_wiz_step_contenido_lbl_example)).setText(Html.fromHtml(getString(R.string.prod_wiz_contenido_examp)));

            updateProductCreation(4, (TextView) findViewById(R.id.prod_wiz_step_contenido_lbl_prdtemp), product);

            Window.CloseSoftKeyboard(activity);

        }

        @Override
        public boolean onStepNext() {
            product.setUnit(_unitProd);
            product.setContenido(0);//solo interesa la unidad

            return true;
        }

        private void assignUnit(String unit) {
            _unitProd = unit;

            _step.nextButton.performClick();

            //updateLabelContenido();
        }

        class UnitClickListener implements View.OnClickListener {

            private String unit;

            public UnitClickListener(String _unit) {
                this.unit = _unit;
            }

            @Override
            public void onClick(View v) {
                assignUnit(unit);
            }
        }
    };

    @Override
    public void inputByScannerSkuStep(String input) {
       return;
    }

    class GridAdapter extends BaseAdapter {
        private final Context context;
        private List<Product> products;

        public GridAdapter(Context context, List<Product> products) {
            this.context = context;
            this.products = products;
        }

        @Override
        public int getCount() {
            return products.size();
        }

        @Override
        public Object getItem(int position) {
            return products.get(position);
        }

        @Override
        public long getItemId(int position) {
            return products.get(position).getId();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(context)
                        .inflate(com.pds.common.R.layout.grid_item_prod_2, parent, false);
            }

            Product p = products.get(position);

            TextView nameTextView = (TextView) convertView.findViewById(com.pds.common.R.id.lblProdName);
            TextView priceTextView = (TextView) convertView.findViewById(com.pds.common.R.id.lblProdPrice);
            ImageView imagen = (ImageView) convertView.findViewById(com.pds.common.R.id.imgProdPic);

            convertView.findViewById(com.pds.common.R.id.vwProdLay).setBackgroundResource(com.pds.common.R.drawable.frg_prod_selectable_item);

            if (p.getId() == -1) {
                imagen.setVisibility(View.GONE);
                nameTextView.setText(p.getName());
                priceTextView.setVisibility(View.GONE);

                FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) ((View) nameTextView.getParent()).getLayoutParams();
                lp.gravity = Gravity.CENTER_VERTICAL;
                ((View) nameTextView.getParent()).setLayoutParams(lp);

                return convertView;
            }

            Uri uri = UriUtils.resolveUri(context, p.getImage_uri());
            imagen.setImageURI(uri);

            //verificamos si se encontró una imagen o no
            if (imagen.getDrawable() == null) {
                imagen.setVisibility(View.GONE);
            }
            else{
                //usamos font más claro
                nameTextView.setTextColor(activity.getResources().getColor(R.color.marron_claro));
                priceTextView.setTextColor(activity.getResources().getColor(R.color.marron_claro));
            }

            nameTextView.setText(p.getName());
            if (p.isWeighable()) {
                String formatPrice = String.format("%s/%s",
                        Formato.FormateaDecimal(p.getSalePrice(), getDecimalFormat(), Formato.getCurrencySymbol(getDecimalFormat())),
                        p.getUnit()
                );

                priceTextView.setText(formatPrice);
            } else
                priceTextView.setText(Formato.FormateaDecimal(p.getSalePrice(), getDecimalFormat(), Formato.getCurrencySymbol(getDecimalFormat())));

            //convertView.setBackgroundResource(com.pds.common.R.drawable.bg_normal);
            /*if (convertView.isSelected() || productosSelected.containsKey((int) p.getId()))
                convertView.setBackgroundResource(com.pds.common.R.drawable.azul);
            else
                convertView.setBackgroundResource(com.pds.common.R.drawable.bg_normal);*/

            return convertView;
        }


    }
}
