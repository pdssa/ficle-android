package com.pds.ficle.gestprod.util;

import com.pds.common.Usuario;

public class UserHolder {

    private static UserHolder instance = new UserHolder();

    private Usuario user;

    public void setUser(Usuario user) {
        this.user = user;
    }

    public Usuario getUser() {
        return user;
    }

    public static UserHolder getInstance() {
        return instance;
    }
}
