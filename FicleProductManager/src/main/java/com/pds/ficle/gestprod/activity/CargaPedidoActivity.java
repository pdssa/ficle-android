package com.pds.ficle.gestprod.activity;

import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Formatos;
import com.pds.common.Usuario;
import com.pds.common.activity.Actividad;
import com.pds.common.dao.PedidoDao;
import com.pds.common.dao.PedidoDetalleDao;
import com.pds.common.dao.ProviderProductDao;
import com.pds.common.db.PedidoDetalleTable;
import com.pds.common.db.PedidoTable;
import com.pds.common.model.Pedido;
import com.pds.common.model.PedidoDetalle;
import com.pds.common.model.Product;
import com.pds.common.model.Provider;
import com.pds.common.model.ProviderProduct;
import com.pds.ficle.gestprod.R;
import com.pds.ficle.gestprod.fragment.AddPedidoItemFragment;
import com.pds.ficle.gestprod.util.PedidoMail;
import com.pds.ficle.gestprod.util.Resources;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android_serialport_api.Printer;

/**
 * Created by EMEKA on 20/02/2015.
 */
public class CargaPedidoActivity extends Actividad implements AddPedidoItemFragment.PedidoItemFragmentInteractionListener {

    private View btnFindProveedor;
    private Button btnAddItem;
    private EditText txtTaxId;
    private TextView txtRazonSocial;
    private Button btnCancelar;
    private Button btnGrabar;
    private Button btnCerrarPedido;
    private Button btnPrintPedido;
    private Pedido _cabecera;
    private List<PedidoDetalle> _items;
    private ItemsArrayAdapter listAdapter;
    private ListView lstDetallePedido;
    private int editPosition;
    private Config config;
    private Usuario _user;
    private List<PedidoDetalle> _itemsActuales;


    private final int REQUEST_PROVEEDORES = 1;
    private final int REQUEST_PRODUCTOS = 3;

    /*
        @Override
        protected void onSaveInstanceState(Bundle outState) {
            //_provider    //_cabecera
            if (_cabecera != null)
                outState.putParcelable("Pedido", _cabecera);

            super.onSaveInstanceState(outState);
        }
    */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carga_pedidos);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            //tomamos el id de usuario
            //_id_user_login = extras.getInt("_id_user_login");
            this._user = (Usuario) extras.getSerializable("current_user");
        }

        //no tenemos user logueado...volvemos a EntryPoint
        if (_user == null) {
            Toast.makeText(this, R.string.unknown_user, Toast.LENGTH_LONG).show();
            this.finish();
        } else {

            asignarViews();

            asignarEventos();

            config = new Config(this);

            Init_CustomConfigPais(config.PAIS);

            btnAddItem.setEnabled(false);//lo deshabilitamos por default
            btnCerrarPedido.setVisibility(View.GONE); //no lo muestro por default
            btnPrintPedido.setVisibility(View.GONE); //no lo muestro por default

            Intent i = getIntent();
            long idPedido = i.getLongExtra("id_pedido", 0);
            if (idPedido != 0) {
                ViewPedido(idPedido);
            } else {
                NuevoPedido();
            }

            /*if (savedInstanceState != null) {
                if (savedInstanceState.containsKey("Pedido")) {
                    _cabecera = savedInstanceState.getParcelable("Pedido");

                    SelectProvider(_cabecera.getProveedor());
                }
            }*/
        }
    }

    //Metodo que se ejecuta al volver del fragment de seleccionar un proveedor
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_PROVEEDORES: {//requerimos un provider
                if (resultCode == RESULT_OK) {
                    Object result = data.getParcelableExtra("result");
                    SelectProvider((Provider) result);
                }
                if (resultCode == RESULT_CANCELED) {
                    //Write your code if there's no result
                    //SelectProvider(null);
                }
            }
            break;
            case PedidoMail.REQUEST_ENVIO_MAIL: { //enviamos un mail
                ConsultarMailEnviado();
                /*if (resultCode == RESULT_OK) {
                    PedidoDao pedidoDao = new PedidoDao(getContentResolver());

                    _cabecera.setEstado("E"); //estado E de Enviado (mail al proveedor)
                    pedidoDao.update(_cabecera);
                    btnCerrarPedido.setText("CERRAR PEDIDO");
                    Toast.makeText(CargaPedidoActivity.this, "El Pedido se ha Enviado correctamente", Toast.LENGTH_SHORT).show();
                }
                if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(CargaPedidoActivity.this, "El Pedido NO se haEnviado", Toast.LENGTH_SHORT).show();
                }*/
            }
            break;
            case REQUEST_PRODUCTOS: {//requerimos un producto
                if (resultCode == RESULT_OK) {
                    Object result = data.getParcelableExtra("result");

                    IniciarFragmentAddEditItem((Product) result, null);//mandamos a crear el item con el producto seleccionado
                }
                if (resultCode == RESULT_CANCELED) {
                    //Write your code if there's no result
                    //SelectProvider(null);
                }
            }
            break;
        }

    }

    private void ConsultarMailEnviado() {
        new AlertDialog.Builder(CargaPedidoActivity.this)
                .setIcon(android.R.drawable.ic_menu_help)
                .setTitle("ENVIAR PEDIDO POR E-MAIL")
                .setMessage("¿Pudo enviar el e-mail correctamente?")
                .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        PedidoDao pedidoDao = new PedidoDao(getContentResolver());

                        _cabecera.setEstado("E"); //estado E de Enviado (mail al proveedor)
                        pedidoDao.update(_cabecera);
                        btnCerrarPedido.setText("CERRAR PEDIDO");
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setCancelable(false)
                .show();
    }

    public void SelectProvider(Provider _provider) {
        if (_provider != null) {
            _cabecera.setProveedor(_provider);
            txtRazonSocial.setText(_provider.getName());
            txtTaxId.setError(null);
            txtTaxId.setText(_provider.getTax_id());
            btnAddItem.setEnabled(true);//lo habilitamos para ingresar items
        } else {
            //TODO: provider not found
            txtRazonSocial.setText("");
            txtTaxId.setError("Proveedor no existente");
            btnAddItem.setEnabled(true);//lo deshabilitamos para que no puedan ingresar items
        }
    }

    private void Init_CustomConfigPais(String codePais) {
        ((TextView) findViewById(R.id.fc_lbl_tax_id)).setText(Resources.getStringFromResource(this, codePais, "label_tax_id"));
        txtTaxId.setHint(Resources.getStringFromResource(this, codePais, "prompt_tax_id"));
    }


    private void ViewPedido(long idPedido) {
        this._cabecera = new PedidoDao(getContentResolver()).first(PedidoTable.PEDIDO_ID + " = " + String.valueOf(idPedido), null, null);

        txtTaxId.setText(this._cabecera.getProveedor().getTax_id());
        txtRazonSocial.setText(this._cabecera.getProveedor().getName());

        this._items = new PedidoDetalleDao(getContentResolver()).list(PedidoDetalleTable.PEDIDO_DET_PEDIDO_ID + " = " + String.valueOf(idPedido), null, null);

        btnGrabar.setVisibility(View.GONE);
        listAdapter = new ItemsArrayAdapter(this, _items);
        lstDetallePedido.setAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();
        btnFindProveedor.setEnabled(false);

        btnCancelar.setText("VOLVER");

        btnPrintPedido.setVisibility(View.VISIBLE);

        if (_cabecera.getEstado().startsWith("A")) {
            btnCerrarPedido.setVisibility(View.VISIBLE);
            btnCerrarPedido.setText("ENVIAR PEDIDO");
            btnAddItem.setEnabled(true);
            btnGrabar.setVisibility(View.VISIBLE);
        } else {
            if (_cabecera.getEstado().startsWith("E")) {//Estado E de Enviado
                btnCerrarPedido.setVisibility(View.VISIBLE);
                btnCerrarPedido.setText("CERRAR PEDIDO");
                btnAddItem.setEnabled(true);
                btnGrabar.setVisibility(View.VISIBLE);
            } else {
                lstDetallePedido.setOnItemClickListener(null);
            }

        }

        txtTaxId.setEnabled(false);

        //lstDetallePedido.setOnItemClickListener(null);
    }

    private void asignarViews() {
        btnFindProveedor = (ImageButton) findViewById(R.id.fc_ibtn_find_prov);
        txtTaxId = (EditText) findViewById(R.id.fc_txt_tax_id);
        txtRazonSocial = (TextView) findViewById(R.id.fc_txt_proveedor);
        btnAddItem = (Button) findViewById(R.id.fc_btn_add_item);
        btnCancelar = (Button) findViewById(R.id.carga_pedido_back_button);
        btnGrabar = (Button) findViewById(R.id.carga_pedido_btn_grabar);
        lstDetallePedido = (ListView) findViewById(R.id.list_item_pedidos_cantidad);
        btnCerrarPedido = (Button) findViewById(R.id.carga_pedido_btn_cerrar);
        btnPrintPedido = (Button) findViewById(R.id.carga_pedido_btn_print);
    }

    private void asignarEventos() {
        btnFindProveedor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CargaPedidoActivity.this, ProviderManagerActivity.class);
                intent.putExtra("search", true);
                startActivityForResult(intent, REQUEST_PROVEEDORES);
                overridePendingTransition(0, 0);
            }
        });

        btnAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IniciarBusquedaProductos(_cabecera.getProveedor());
            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        lstDetallePedido.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PedidoDetalle det = (PedidoDetalle) parent.getItemAtPosition(position);

                editPosition = position;

                IniciarFragmentAddEditItem(null, det);//mandamos a editar el item
            }
        });

        btnGrabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Confirmar();
                Validar();
            }
        });

        btnCerrarPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    PedidoDao pedidoDao = new PedidoDao(getContentResolver());

                    ControlarNuevosItemsInsertados();

                    if (_cabecera.getEstado().startsWith("A")) {

                        if (TextUtils.isEmpty(_cabecera.getProveedor().getEmail()))
                            Toast.makeText(CargaPedidoActivity.this, "El proveedor no posee un mail asignado, por favor asignarle dicho dato en la pantalla Proveedores", Toast.LENGTH_LONG).show();
                        else
                            PedidoMail.EnviarEmailProveedor(CargaPedidoActivity.this, _cabecera.getProveedor().getEmail(),_cabecera.getProveedor().getName(), config.COMERCIO, config.CLAVEFISCAL, config.DIRECCION, _items);

                    } else {
                        _cabecera.setEstado("C"); //estado C de Cerrado
                        _cabecera.setSync("N");
                        pedidoDao.update(_cabecera);

                        Toast.makeText(CargaPedidoActivity.this, "El Pedido se ha CERRADO correctamente", Toast.LENGTH_SHORT).show();
                        btnCerrarPedido.setVisibility(View.GONE);
                        btnAddItem.setEnabled(false);
                        btnGrabar.setVisibility(View.GONE);
                    }

                } catch (Exception ex) {
                    Toast.makeText(CargaPedidoActivity.this, "El Pedido no se pudo CERRAR correctamente", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnPrintPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImprimirPedido();
            }
        });
    }


    @Override
    protected void onDestroy() {

        super.onDestroy();
    }

    private Printer _printer;

    private void ImprimirPedido() {
        try {
            if (_printer == null)
                _printer = Printer.getInstance(config.PRINTER_MODEL, CargaPedidoActivity.this);

            _printer.printerCallback = new Printer.PrinterCallback() {
                @Override
                public void onPrinterReady() {
                    _printer.lineFeed();

                    _printer.printLine(String.format("PEDIDO # : %d", _cabecera.getId()));
                    _printer.lineFeed();

                    _printer.printOpposite(Formatos.FormateaDate(_cabecera.getFecha(), Formatos.DateFormat), Formatos.FormateaDate(_cabecera.getFecha(), Formatos.ShortTimeFormat)); //fecha ocupa 10, hora ocupa 5, restan 17

                    _printer.printLine("Proveedor: ");
                    _printer.printLine(_cabecera.getProveedor().getTax_id());
                    _printer.printLine(_cabecera.getProveedor().getName());

                    _printer.sendSeparadorHorizontal();

                    //recorremos el detalle
                    for (PedidoDetalle det : _items) {

                        String cantidad = "    " + String.valueOf(det.getCantidad()); //hacemos un padding con 4 espacios (izquierda)
                        cantidad = cantidad.substring(cantidad.length() - 4);

                        String code = det.getProducto().getCode().trim();

                        String descripcion = det.getProducto().getPrintingName().trim(); // + _printer.getPadding(_printer.get_longitudLinea()); //hacemos un padding de relleno

                        int len = 4 + 1 + 1 + code.length();//q + " " + ____________ + " " + code

                        descripcion = descripcion.substring(0, Math.min(descripcion.length(), _printer.get_longitudLinea() - len)); //cortamos en 15


                        //descripcion = descripcion.trim() + " (" + code + ")" + "         "; //hacemos un padding de 9 (derecha)
                        //descripcion = descripcion.substring(0, 27); //cortamos en 27


                        //_printer.print(cantidad + " " + descripcion); //completamos 32
                        _printer.printOpposite(cantidad + " " + descripcion, " " + code);
                    }

                    _printer.sendSeparadorHorizontal();

                    _printer.printLine("Estado del pedido : " + _cabecera.getEstadoDescripcion());

                    _printer.sendSeparadorHorizontal();

                    _printer.footer();
                }
            };

            if (!_printer.initialize()) {
                Toast.makeText(CargaPedidoActivity.this, "Error al inicializar", Toast.LENGTH_SHORT).show();

                _printer.end();
            }
        } catch (Exception ex) {
            Toast.makeText(CargaPedidoActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        if (_cabecera == null)
            super.onBackPressed();
        else if (TextUtils.isEmpty(_cabecera.getEstado()))
            super.onBackPressed();
        else if (!_cabecera.getEstado().startsWith("C"))
            super.onBackPressed();
        else
            finish();
    }


    public void ControlarNuevosItemsInsertados() {
        for (PedidoDetalle det : _items) {
            PedidoDetalleDao dao = new PedidoDetalleDao(getContentResolver());
            det.setSync("N");
            det.setPedido(_cabecera);
            dao.saveOrUpdate(det);
        }
    }

    public void IniciarBusquedaProductos(Provider _provider) {
        Intent intent = new Intent(this, ProductSearchActivity.class);
        intent.putExtra("TypeResultSearch", ProductSearchActivity.TypeResultSearch.SEARCH_PRODUCT_BACK);
        intent.putExtra("Filter_Provider", _provider);
        startActivityForResult(intent, REQUEST_PRODUCTOS);
    }

    public void IniciarFragmentAddEditItem(Product product_add, PedidoDetalle item_edit) {
        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.frgAddEditItem) != null) {

            // Create a new Fragment to be placed in the activity layout
            AddPedidoItemFragment fragment = new AddPedidoItemFragment().newInstance(product_add, item_edit);

            //AddFcItemFragment fragment = new AddFcItemFragment();
            fragment.setPedidoItemFragmentInteractionListener(this);

            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.frgAddEditItem, fragment);
            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            ft.commit();
        }
    }

    private void NuevoPedido() {
        this._cabecera = new Pedido();

        editPosition = 0;

        if (_items == null) {
            _items = new ArrayList<PedidoDetalle>();
            listAdapter = new ItemsArrayAdapter(this, _items);
            lstDetallePedido.setAdapter(listAdapter);
        } else {
            this._items.clear();
            listAdapter.notifyDataSetChanged();
        }

        txtTaxId.setText(null);
        txtTaxId.setError(null);
        txtRazonSocial.setText(null);


    }

    public void Confirmar() {
        super.onSave(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Validar();
            }
        });
    }

    public void Validar() {

        txtTaxId.setError(null);

        if (_cabecera.getProveedor() == null) {
            txtTaxId.setError("El Proveedor es obligatorio");
            return;
        }

        if (lstDetallePedido.getCount() == 0) {
            Toast.makeText(CargaPedidoActivity.this, "Debe ingresar al menos un Item", Toast.LENGTH_SHORT).show();
            return;
        }


        Grabar();

        finish();
    }

    public void Grabar() {
        try {

            //primero vamos a grabar las relaciones entre productos cargados y proveedor
            if (RevisaGrabaRelaciones()) {

                boolean nuevo = false;

                if (TextUtils.isEmpty(_cabecera.getEstado())) {
                    //La fecha del pedido es la del mismo que que se realiza
                    _cabecera.setFecha(new Date());

                    //Siempre que se crea un pedido está en estado ABIERTO
                    _cabecera.setEstado("A");
                    _cabecera.setUserid(_user.getId());

                    nuevo = true;
                }

                _cabecera.setSync("N");

                if (!new PedidoDao(getContentResolver()).saveOrUpdate(_cabecera)) {
                    Toast.makeText(CargaPedidoActivity.this, "Error al generar el pedido", Toast.LENGTH_SHORT).show();
                } else {

                    PedidoDetalleDao pedidoDetalleDao = new PedidoDetalleDao(getContentResolver());

                    for (PedidoDetalle det : _items) {

                        //asociamos la cabecera y grabamos
                        det.setPedido(_cabecera);
                        det.setSync("N");

                        pedidoDetalleDao.saveOrUpdate(det);
                    }

                    if (nuevo)
                        Toast.makeText(CargaPedidoActivity.this, "Pedido generado correctamente!", Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(CargaPedidoActivity.this, "Pedido actualizado correctamente!", Toast.LENGTH_SHORT).show();

                    NuevoPedido();
                }
            }

        } catch (Exception ex) {
            Toast.makeText(CargaPedidoActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public boolean RevisaGrabaRelaciones() {

        ProviderProductDao providerProductDao = new ProviderProductDao(getContentResolver());
        for (PedidoDetalle det : _items) {
            //vemos si existe ya la relacion, sino la creamos
            if (!providerProductDao.existeRelacion(det.getProducto().getId(), _cabecera.getProveedor().getId())) {

                providerProductDao.save(new ProviderProduct(det.getProducto().getId(), _cabecera.getProveedor().getId()));
            }
        }

        return true;//ya no quedan relaciones por crear
    }

    public class ItemsArrayAdapter extends ArrayAdapter<PedidoDetalle> {
        private final Context context;
        private List<PedidoDetalle> items;

        public ItemsArrayAdapter(Context context, List<PedidoDetalle> items) {
            super(context, R.layout.list_item_compra, items);
            this.context = context;
            this.items = items;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View rowView = inflater.inflate(R.layout.list_item_pedido_detalle, parent, false);

            ((TextView) rowView.findViewById(R.id.list_item_producto_cod)).setText(TextUtils.isEmpty(items.get(position).getProducto().getCode()) ? "S/C" : items.get(position).getProducto().getCode());
            ((TextView) rowView.findViewById(R.id.list_item_pedidos_producto_det)).setText(items.get(position).getProducto().getName());
            ((TextView) rowView.findViewById(R.id.list_item_pedidos_cantidad)).setText(String.valueOf(items.get(position).getCantidad()));

            return rowView;
        }

    }


    @Override
    public void onItemCreated(PedidoDetalle new_item) {
        this._items.add(new_item);
        //listAdapter = new ItemsArrayAdapter(this, _items);
        listAdapter.notifyDataSetChanged();

        //agregamos y nos mantenemos en pantalla para continuar..
        Toast.makeText(this, "Item agregado correctamente al pedido", Toast.LENGTH_SHORT).show();

        IniciarBusquedaProductos(_cabecera.getProveedor());//iniciamos nueva busqueda
    }

    @Override
    public void onItemEdited(PedidoDetalle edited_item) {
        _items.set(editPosition, edited_item);

        listAdapter.notifyDataSetChanged();
    }
}
