package com.pds.ficle.gestprod.wizard;

import com.pds.common.model.Product;

/**
 * Created by Hernan on 05/12/2016.
 */
public interface ProductWizardListener {
    void openProduct(Product p);
    boolean saveProduct(boolean updateProd,Product p);
    void restartWizard();
    void finishWizard();
}
