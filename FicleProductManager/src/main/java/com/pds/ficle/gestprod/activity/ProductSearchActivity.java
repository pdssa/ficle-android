package com.pds.ficle.gestprod.activity;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Formatos;
import com.pds.common.Usuario;
import com.pds.common.activity.TimerActivity;
import com.pds.common.dao.ProductDao;
import com.pds.common.dao.ProviderProductDao;
import com.pds.common.fragment.GenericFragmentListener;
import com.pds.common.fragment.ProductoFragmentListener;
import com.pds.common.fragment.ProductosFragment;
import com.pds.common.hardware.ScannerPOWA;
import com.pds.common.model.Combo;
import com.pds.common.model.Product;
import com.pds.common.model.Provider;
import com.pds.common.util.Window;
import com.pds.ficle.gestprod.R;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Callable;

public class ProductSearchActivity extends TimerActivity implements ProductoFragmentListener, GenericFragmentListener {

    private Timer timer = new Timer();
    private boolean continueSearching = true;

    //private Button newButton;
    private Button withoutPriceButton;
    private Button backButton;
    private EditText searchText;
    ListView lstProductos;
    private ProductArrayAdapter adapter;
    private List<Product> productos;

    private Usuario loggedUser;
    private ScannerPOWA _scanner;

    private Provider filter_provider;

  /*  @Override
    public void onCodeAsociado(String code) {
        backButton.performClick();
    }*/

    @Override
    public void onFragmentAttached() {
        //filtramos por proveedor en caso de haber recibido el filtro
        if (filter_provider != null) {
            FiltrarProductos(filter_provider, "");
        }
    }



    public enum TypeResultSearch {
        FULL_PRODUCT_EDIT, //vamos al ABM completo de productos/combos
        STOCK_PRODUCT_EDIT, //vamos a editar opciones simples de stock del producto
        SEARCH_PRODUCT_BACK, //vamos a retornar el producto seleccionado a la actividad que llamó la busqueda
        FAST_CODE_ASSOC //vamos a llamar al fragment de asociacion de productos
    }

    private TypeResultSearch resultType;
    private boolean INCLUIR_PRODS_SIN_PRECIO;

    private void setTypeResultSearch() {
        Serializable extra = getIntent().getSerializableExtra("TypeResultSearch");

        if (extra != null)
            resultType = (TypeResultSearch) extra;
        else
            resultType = TypeResultSearch.FULL_PRODUCT_EDIT; //por default

        Serializable user = getIntent().getSerializableExtra("current_user");

        if (user != null)
            loggedUser = (Usuario) user;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_search);
        try {
            //según el intent
            setTypeResultSearch();

            INCLUIR_PRODS_SIN_PRECIO = false; //SOLO PRODUCTOS CON PRECIO

            ///FIXME: HZ: Le saque esta linea, dado que no tenemos relacionados los productos con los proveedores,
            ///y en el ingreso de compras, no encontraba productos para el proveedor
            ///FIXME: filter_provider = getIntent().getParcelableExtra("Filter_Provider");


           /* newButton = (Button) findViewById(R.id.prod_search_btn_nuevo);
            newButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(ProductSearchActivity.this, ProductWizardActivity.class);
                    //intent.putExtra("searchString", searchText.getText().toString());
                    intent.putExtra("current_user", loggedUser);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                }
            });*/



            backButton = (Button) findViewById(R.id.prod_search_btn_back);
            backButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });

            withoutPriceButton = (Button) findViewById(R.id.prod_search_btn_without_price);
            withoutPriceButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    INCLUIR_PRODS_SIN_PRECIO = !INCLUIR_PRODS_SIN_PRECIO;

                    ReloadList(INCLUIR_PRODS_SIN_PRECIO);
                }
            });

            setupNumericFilter();
            setupAlphaFilter();

            findViewById(R.id.prod_search_btn_find).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CancelarSearch();
                }
            });
            findViewById(R.id.prod_search_txt_depart).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CancelarSearch();
                }
            });
            findViewById(R.id.prod_search_txt_subdepart).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CancelarSearch();
                }
            });

            config = new Config(this);
            Init_CustomConfigPais(config.PAIS);

            Window.AddEnterKeyboardAction(searchText, new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    return null;
                }
            });


        } catch (Exception ex) {
            Toast.makeText(this, "Error: " + ex.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    private void setupNumericFilter() {
        searchText = (EditText) findViewById(R.id.prod_search_txt_criteria);
        searchText.setFilters(new InputFilter[]{new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++)
                    if (!Character.isLetter(source.charAt(i)) &&
                            !Character.isSpaceChar(source.charAt(i)) &&
                            !Character.isDigit(source.charAt(i))
                            ) //solo aceptamos: letras, numeros y espacio
                        return "";

                return null;
            }
        }});

        searchText.addTextChangedListener(new TextWatcher() {

            private final long DELAY = 500; // in ms

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                timer.cancel();
                if (continueSearching) {
                    timer = new Timer();

                    final Editable _s = s;

                    if (s.length() > 0) {
                        ((ImageView) findViewById(R.id.prod_search_btn_find)).setImageResource(R.drawable.ic_action_cancel);
                        timer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        FiltrarProductos(filter_provider, _s);
                                    }
                                });
                            }
                        }, DELAY);
                    } else
                        ((ImageView) findViewById(R.id.prod_search_btn_find)).setImageResource(R.drawable.lupa);
                }
            }
        });
    }

    private void setupAlphaFilter() {
        EditText alphaFilter = (EditText) findViewById(R.id.prod_search_txt_alpha_criteria);
        alphaFilter.setFilters(new InputFilter[]{new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++)
                    if (!Character.isLetter(source.charAt(i)) &&
                            !Character.isSpaceChar(source.charAt(i)) &&
                            !Character.isDigit(source.charAt(i))
                            ) //solo aceptamos: letras, numeros y espacio
                        return "";

                return null;
            }
        }});

        alphaFilter.addTextChangedListener(new TextWatcher() {

            private final long DELAY = 500; // in ms

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                timer.cancel();
                if (continueSearching) {
                    timer = new Timer();

                    final Editable _s = s;

                    if (s.length() > 0) {
                        ((ImageView) findViewById(R.id.prod_search_btn_find)).setImageResource(R.drawable.ic_action_cancel);
                        timer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        filterProductsByalphaCriteria(filter_provider, _s);
                                    }
                                });
                            }
                        }, DELAY);
                    } else
                        ((ImageView) findViewById(R.id.prod_search_btn_find)).setImageResource(R.drawable.lupa);
                }
            }
        });
    }

    private Config config;
    private DecimalFormat FORMATO_DECIMAL;
    private String MONEDA;

    @Override
    protected void onResume() {
        try {
            super.onResume();

            AttachFragment();

            //limpiamos por si quedó una busqueda activa
            searchText.setText("");
            continueSearching = true;
            adapter = null;
            lstProductos = null;

            ((ImageView) findViewById(R.id.prod_search_btn_find)).setImageResource(R.drawable.lupa);
            //...

            //CancelarSearch();

            Log.d("Test", "Search-Resume");

            //si tenemos una printer POWA, entonces iniciamos el scanner POWA S10
            if (config.PRINTER_MODEL != null && config.PRINTER_MODEL.equals("POWA")) {
                _scanner = new ScannerPOWA(this);
                _scanner._scannerCallback = new ScannerPOWA.ScannerCallback() {
                    @Override
                    public void onScannerRead(String text) {
                        searchText.setText(text);
                    }
                };
            }

        } catch (Exception ex) {
            Toast.makeText(this, "Error: " + ex.toString(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onPause() {
        if (_scanner != null) {
            _scanner.end();
            _scanner = null;
        }

        Log.d("Test", "Search-Pause");

        super.onPause();
    }

    private void Init_CustomConfigPais(String codePais) {

        FORMATO_DECIMAL = Formatos.getDecimalFormat(config.PAIS);
        MONEDA = Formatos.getCurrencySymbol(config.PAIS);
    }

    private ProductosFragment frgProductos;
    private void ReloadList(boolean incluirSinPrecio){
        if(frgProductos != null)
            frgProductos.RefreshList(incluirSinPrecio);
        else
            AttachFragment();

        //seteamos el texto del boton según la accion que realiza
        withoutPriceButton.setText(getString(incluirSinPrecio ?  R.string.btn_with_price: R.string.btn_with_no_price));
    }

    private void AttachFragment() {
        if (findViewById(R.id.prod_search_frg_productos) != null) {

            // Create a new Fragment to be placed in the activity layout
            boolean takeListViewControl = filter_provider != null;
            frgProductos = ProductosFragment.newInstance(false, new HashMap<Integer, Integer>(), FORMATO_DECIMAL, MONEDA, false, takeListViewControl, INCLUIR_PRODS_SIN_PRECIO);
            frgProductos.setProductoListener(this);
            frgProductos.setListener(this);

            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.prod_search_frg_productos, frgProductos);
            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            ft.commit();

        }
    }


    private void filterProductsByalphaCriteria(Provider provider, CharSequence filter)
    {


        if (lstProductos == null)
            lstProductos = (ListView) findViewById(R.id.lstProductos);

        //ocultamos las listas
        findViewById(R.id.lstDepartamentos).setVisibility(View.INVISIBLE);
        findViewById(R.id.lstSubdepartamentos).setVisibility(View.INVISIBLE);

        //cargamos los productos resultantes del filtro de texto
        List<Product> result_list = getListProductsAlphaFiltered(provider, filter);

        if (result_list.size() == 1) {
            //si solo se encontró un resultado => seleccionamos dicho producto
            //timer.cancel();
            continueSearching = false;
            onProductoSeleccionado(result_list.get(0));
        } else {
            if (adapter == null) {
                productos = result_list;
                adapter = new ProductArrayAdapter(ProductSearchActivity.this, productos);
                lstProductos.setAdapter(adapter);
            } else {
                productos.clear();
                productos.addAll(result_list);
                adapter.notifyDataSetChanged();
            }


            //si no hay resultados:
            if (result_list.size() == 0)
                findViewById(R.id.prod_search_txt_empty).setVisibility(View.VISIBLE);
            else
                findViewById(R.id.prod_search_txt_empty).setVisibility(View.GONE);
        }
    }

    private void FiltrarProductos(Provider provider, CharSequence filter) {
        if (lstProductos == null)
            lstProductos = (ListView) findViewById(R.id.lstProductos);

        //ocultamos las listas
        findViewById(R.id.lstDepartamentos).setVisibility(View.INVISIBLE);
        findViewById(R.id.lstSubdepartamentos).setVisibility(View.INVISIBLE);

        //cargamos los productos resultantes del filtro de texto
        List<Product> result_list = getListProducts(provider, filter);

        if (result_list.size() == 1) {
            //si solo se encontró un resultado => seleccionamos dicho producto
            //timer.cancel();
            continueSearching = false;
            onProductoSeleccionado(result_list.get(0));
        } else {
            if (adapter == null) {
                productos = result_list;
                adapter = new ProductArrayAdapter(ProductSearchActivity.this, productos);
                lstProductos.setAdapter(adapter);
            } else {
                productos.clear();
                productos.addAll(result_list);
                adapter.notifyDataSetChanged();
            }


            //si no hay resultados:
            if (result_list.size() == 0)
                findViewById(R.id.prod_search_txt_empty).setVisibility(View.VISIBLE);
            else
                findViewById(R.id.prod_search_txt_empty).setVisibility(View.GONE);
        }
    }

    private void CancelarSearch() {
        searchText.setText("");

        adapter = null;

        findViewById(R.id.prod_search_txt_empty).setVisibility(View.GONE);

        ((ImageView) findViewById(R.id.prod_search_btn_find)).setImageResource(R.drawable.lupa);

        //cerramos el teclado
        Window.CloseSoftKeyboard(ProductSearchActivity.this);

        if (filter_provider != null) {
            FiltrarProductos(filter_provider, "");
        } else {
            //mostramos las listas
            ListView lstDeptos = (ListView) findViewById(R.id.lstDepartamentos);
            if (lstDeptos != null) {
                lstDeptos.setVisibility(View.VISIBLE);
                lstDeptos.performItemClick(
                        lstDeptos.getAdapter().getView(0, null, null),
                        0,
                        lstDeptos.getAdapter().getItemId(0));
                //lstDeptos.getAdapter().getView(0, null, null).setBackgroundResource(R.drawable.azul);
            }
        }
    }

    private List<Product> getListProducts(Provider provider, CharSequence filter) {
        String cond_filter = getFilterProvider(provider);

        //return new ProductDao(getContentResolver()).list("removed = 0 and " + cond_filter + " (upper(name) like '%" + filter.toString().toUpperCase() + "%' or code like '%" + filter + "%' or fast_code like '%" + filter + "%') and code not like '%-%'", null, "name ASC");
        //filtro numerico => SKU
        return new ProductDao(getContentResolver()).list("removed = 0 and " + cond_filter + " code like '%" + filter + "%' and code not like '%-%'", null, "name ASC");
    }

    private List<Product> getListProductsAlphaFiltered(Provider provider, CharSequence filter) {
        String cond_filter = getFilterProvider(provider);

        //return new ProductDao(getContentResolver()).list("removed = 0 and " + cond_filter + " (upper(name) like '%" + filter.toString().toUpperCase() + "%' or code like '%" + filter + "%' or fast_code like '%" + filter + "%') and code not like '%-%'", null, "name ASC");
        //filtro numerico => SKU
        return new ProductDao(getContentResolver()).list("removed = 0 and (" + cond_filter + " name like '%" + filter + "%') and code not like '%-%'", null, "name ASC");
    }

    private String getFilterProvider(Provider provider) {
        if (provider != null) {
            List<Long> prods = new ProviderProductDao(getContentResolver()).getProductsList(provider.getId());

            String s_prods = "";

            for (Long p : prods) {
                s_prods += p.toString() + ",";
            }

            if (s_prods.length() > 0)
                s_prods = s_prods.substring(0, s_prods.length() - 1);

            return " _id in (" + s_prods + ") and ";
        } else
            return "";

        //return new ProductDao(getContentResolver()).list("removed = 0 and _id in (" + s_prods + ")", null, "name ASC");
    }

    @Override
    public void onProductoSeleccionado(Product selectedProduct) {
        if (!selectedProduct.isGeneric()) {

            //según el comportamiento definido:

            if (resultType == TypeResultSearch.FULL_PRODUCT_EDIT) {
                //llamamos al ABM de producto
                Intent intent = new Intent(this, NewProductActivity.class);
                intent.putExtra("product", selectedProduct);
                intent.putExtra("current_user", loggedUser);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }

            if (resultType == TypeResultSearch.STOCK_PRODUCT_EDIT) {
                //llamamos al ABM simplificado
                Intent intent = new Intent(this, StockEditActivity.class);
                intent.putExtra("product_edit", selectedProduct);
                startActivity(intent);
                overridePendingTransition(0, 0);
                /*if (findViewById(R.id.prod_search_frg_stock_edit) != null) {

                    // Create a new Fragment to be placed in the activity layout
                    StockEditFragment frgStockEdit = StockEditFragment.newInstance(selectedProduct);

                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.replace(R.id.prod_search_frg_stock_edit, frgStockEdit);
                    ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
                    ft.commit();
                }*/
            }

            if (resultType == TypeResultSearch.SEARCH_PRODUCT_BACK) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result", selectedProduct);
                setResult(RESULT_OK, returnIntent);
                finish();
            }

            if (resultType == TypeResultSearch.FAST_CODE_ASSOC) {
                if (findViewById(R.id.prod_search_frg_stock_edit) != null) {

                  /*  // Create a new Fragment to be placed in the activity layout
                    AsociarCodeFragment fragment = AsociarCodeFragment.newInstance(selectedProduct, false);

                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.replace(R.id.prod_search_frg_stock_edit, fragment);
                    ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
                    ft.commit();*/
                }
            }

        }
    }

   /* @Override
    public void onComboSeleccionado(Combo c) {
        //según el comportamiento definido:

        if (resultType == TypeResultSearch.FULL_PRODUCT_EDIT) {
            //llamamos al ABM de combos
            //Intent intent = new Intent(this, CargaComboActivity.class);
            Intent intent = new Intent(this, NewComboActivity.class);
            intent.putExtra("combo_edit", c);
            startActivity(intent);
            overridePendingTransition(0, 0);
        }

        if (resultType == TypeResultSearch.SEARCH_PRODUCT_BACK || resultType == TypeResultSearch.STOCK_PRODUCT_EDIT || resultType == TypeResultSearch.FAST_CODE_ASSOC) {
            Toast.makeText(ProductSearchActivity.this, "Funcion no implementada para combos", Toast.LENGTH_SHORT).show();
        }
    }*/

    @Override
    public void onFragmentProductoClose() {

    }

    public class ProductArrayAdapter extends ArrayAdapter<Product> {
        private final Context context;
        private List<Product> products;

        public ProductArrayAdapter(Context context, List<Product> products) {
            super(context, R.layout.list_item_prod, products);
            this.context = context;
            this.products = products;

            lstProductos.setVisibility(this.products.size() > 0 ? View.VISIBLE : View.INVISIBLE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(context)
                        .inflate(R.layout.list_item_prod, parent, false);
            }

            //if (position % 2 != 0)
            //    rowView.setBackgroundColor(Color.LTGRAY);
            //else
            //rowView.setBackgroundColor(Color.WHITE);

            Product p = products.get(position);

            TextView nameTextView = (TextView) convertView.findViewById(R.id.lblListItem);
            nameTextView.setText(p.getName());

            //ImageView imagen = (ImageView) convertView.findViewById(R.id.imgListItem);
            //imagen.setImageResource(R.drawable.objects_icon);

            TextView priceTextView = (TextView) convertView.findViewById(R.id.lblListItemPrice);
            priceTextView.setText(Formatos.FormateaDecimal(p.getSalePrice(), FORMATO_DECIMAL, MONEDA));

            //ajustes visuales...
            if (FORMATO_DECIMAL == Formatos.DecimalFormat) {
                priceTextView.setTextSize(20);
            } else if (FORMATO_DECIMAL == Formatos.DecimalFormat_PE) {
                priceTextView.setTextSize(18);
            } else {
                priceTextView.setTextSize(25);
            }

            TextView stockTextView = (TextView) convertView.findViewById(R.id.lblListItemStock);
            stockTextView.setVisibility(View.VISIBLE);
            stockTextView.setText(Formatos.FormateaDecimal(p.getStock(), Formatos.DecimalFormat_US));

            if (p.getStock() <= p.getMinStock())
                stockTextView.setTextColor(getResources().getColor(com.pds.common.R.color.boton_rojo));
            else
                stockTextView.setTextColor(getResources().getColor(com.pds.common.R.color.texto_black));


            if (convertView.isSelected())
                convertView.setBackgroundResource(R.drawable.azul);
            else
                convertView.setBackgroundResource(R.drawable.bg_normal);

            return convertView;
        }

    }

}
