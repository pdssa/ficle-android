package com.pds.ficle.gestprod.util;

import android.app.Activity;
import android.content.Intent;

import com.pds.common.model.PedidoDetalle;

import java.util.List;

/**
 * Created by Hernan on 16/07/2015.
 */
public abstract class PedidoMail {

    public static final int REQUEST_ENVIO_MAIL = 9999;


    public static void EnviarEmailProveedor(Activity activity, String emailProveedor, String proveedor, String comercio, String clavefiscal, String direccion, List<PedidoDetalle> items) {
        //obtenemos los datos para el envío del correo
        String etEmail = emailProveedor;
        String asunto = "Solucitud de Mercaderías";
        String cuerpoMail = PedidoMail.ArmaCuerpoMail(proveedor, comercio, clavefiscal, direccion, items);

        //es necesario un intent que levante la actividad deseada
        Intent itSend = new Intent(android.content.Intent.ACTION_SEND);

        //vamos a enviar texto plano a menos que el checkbox esté marcado
        itSend.setType("plain/text");

        //colocamos los datos para el envío
        itSend.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{etEmail});
        itSend.putExtra(android.content.Intent.EXTRA_SUBJECT, asunto);
        itSend.putExtra(android.content.Intent.EXTRA_TEXT, cuerpoMail);

        //iniciamos la actividad
        activity.startActivityForResult(itSend, REQUEST_ENVIO_MAIL);
    }

    private static String ArmaCuerpoMail(String proveedor, String comercio, String clavefiscal, String direccion, List<PedidoDetalle> items){
        String pedido = "Estimado " + proveedor;
        pedido += "\n\n"; //Salto de línea
        pedido += "Me comunico desde el punto de venta "+comercio+", RUT número "+clavefiscal+", ubicado en "+direccion+", para solicitarle el envío de los siguientes productos:\n";
        pedido += "\n"; //Salto de línea
        for (PedidoDetalle p : items) {
            pedido += p.getCantidad() + "     " + p.getProducto().getDescription() + "\n";
        }
        pedido += "\nSaludos cordiales";

        return pedido;
    }
}
