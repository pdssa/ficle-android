package com.pds.ficle.gestprod.activity;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.pds.common.activity.TimerActivity;
import com.pds.ficle.gestprod.R;
import com.pds.ficle.gestprod.fragment.SubDepartmentListFragment;

public class SubDepartmentManagerActivity extends TimerActivity {

    private Button newButton;
    private Button backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_department_manager);

        newButton = (Button) findViewById(R.id.sub_department_manager_add_button);
        newButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SubDepartmentManagerActivity.this, SubDepartmentActivity.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });

        backButton = (Button) findViewById(R.id.department_manager_back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        FragmentManager fm = getFragmentManager();

        if (fm.findFragmentById(android.R.id.content) == null) {
            SubDepartmentListFragment list = new SubDepartmentListFragment();
            fm.beginTransaction().add(R.id.sub_department_manager_list_container, list).commit();
        }
    }


}
