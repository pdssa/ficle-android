package com.pds.ficle.gestprod.util;

import android.widget.Spinner;

/**
 * Created by raul.lopez on 6/1/2014.
 */
public class SpinnerUtils {
    public interface Predicate<T> {
        boolean evaluate(T t);
    }
    public static <T> int getIndexForElement(Spinner spinner, Predicate<T> predicate) {
        for (int i=0;i<spinner.getCount();i++){
            if(predicate.evaluate((T)spinner.getItemAtPosition(i))) {
                return i;
            }
        }
        return 0;
    }
}
