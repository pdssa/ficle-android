package com.pds.promos;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Formato;
import com.pds.common.Logger;
import com.pds.common.activity.Actividad;
import com.pds.common.dao.ComboDao;
import com.pds.common.dao.ComboItemDao;
import com.pds.common.dao.ProductDao;
import com.pds.common.dialog.Dialog;
import com.pds.common.hardware.ScannerPOWA;
import com.pds.common.model.Combo;
import com.pds.common.model.ComboItem;
import com.pds.common.model.Product;
import com.pds.common.model.Promocion;
import com.pds.common.util.Sku_Utils;
import com.pds.common.util.Window;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Hernan on 07/06/2016.
 */
public class ComboPromoWizardActivity extends Actividad {

    private Button btnCancelar;
    private List<WizardStep> steps;
    private WizardStep currentStep;
    private Combo combo;
    private List<ComboItem> _items;
    private String COMBO_PREFIX = "PROMO";
    public static final String PROMOCION_KEY = "__PROMO";
    private Promocion promocionOrigen;
    private Config config;
    private Formato.Decimal_Format FORMATO_DECIMAL;
    private Product productoPpal;
    private WizardStep stepProdsSecundAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_combo_promo_wizard);

        if (!getIntent().hasExtra(PROMOCION_KEY)) {
            Toast.makeText(ComboPromoWizardActivity.this, "Parametros no implementados", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        promocionOrigen = getIntent().getExtras().getParcelable(PROMOCION_KEY);

        //cancelar
        btnCancelar = (Button) findViewById(R.id.combo_wiz_btn_cancel);
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ocultamos el teclado al salir
                Window.CloseSoftKeyboard(ComboPromoWizardActivity.this);

                volver();//salimos
            }
        });

        //formato
        config = new Config(this);
        FORMATO_DECIMAL = Formato.getDecimal_Format(config.PAIS);

        //armamos los pasos del wizard
        WizardStep step1 = new WizardStep(R.string.combo_wiz_validation, R.layout.combo_wiz_step_validation, stepValidationChange);
        WizardStep step2 = new WizardStep(R.string.combo_wiz_nombre, R.layout.combo_wiz_step_nombre, stepNombreChange);
        WizardStep step3 = new WizardStep(R.string.combo_wiz_descripcion, R.layout.combo_wiz_step_descripcion, stepDescripcionChange);

        stepProdsSecundAdd = new WizardStep(R.string.combo_wiz_prod_secund_add, R.layout.combo_wiz_step_prod_secund_add, stepProdSecundAdd);


        //WizardStep step4 = new WizardStep(R.string.combo_wiz_prod_secund, R.layout.combo_wiz_step_prodsecunds, stepProdSecund, View.GONE);


        WizardStep step5 = new WizardStep(R.string.combo_wiz_prod_secund_cant, R.layout.combo_wiz_step_cantidad_secund, stepProdSecundCantidadChange);

        WizardStep end = new WizardStep(R.string.combo_wiz_precio_valida, R.layout.combo_wiz_step_validar, stepEndChange);

        //alta
        step1.flows(null, step2);
        step2.flows(step1, step3);
        step3.flows(step2, stepProdsSecundAdd);
        stepProdsSecundAdd.flows(step3, step5);

        //step4.flows(step3, end);
        step5.flows(stepProdsSecundAdd, end);

        end.flows(stepProdsSecundAdd, null);


        steps = new ArrayList<WizardStep>();
        steps.add(step1);
        steps.add(step2);
        steps.add(step3);
        //steps.add(step4);
        steps.add(stepProdsSecundAdd);
        steps.add(step5);
        steps.add(end);

        //mostramos el primero
        execWizStep(step1);

    }

    private ScannerPOWA _scanner;

    @Override
    protected void onResume() {
        super.onResume();

        try {
            //si tenemos una printer POWA, entonces iniciamos el scanner POWA S10
            if (config.PRINTER_MODEL != null && config.PRINTER_MODEL.equals("POWA")) {
                _scanner = new ScannerPOWA(this);
                _scanner._scannerCallback = new ScannerPOWA.ScannerCallback() {
                    @Override
                    public void onScannerRead(String text) {
                        //si está activado el step SKU PPAL
                        if(STEP_SKU == 1 || STEP_SKU == 2)
                            ((EditText) findViewById(R.id.combo_wiz_step_cod_barra_txt_code)).setText(text);
                    }
                };
            }
        } catch (Exception ex) {
            Toast.makeText(ComboPromoWizardActivity.this, ex.getMessage(),Toast.LENGTH_SHORT ).show();
        }
    }

    @Override
    protected void onPause() {
        if (_scanner != null){
            _scanner.end();
            _scanner = null;
        }

        super.onPause();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();

        if (currentStep != null) {
            if (currentStep.previousStep != null)
                execWizStep(currentStep.previousStep);
            else
                super.onBackPressed();//btnCancelar.performClick();
        }
    }

    private void volver() {
        super.onBackPressed();//asi llamamos al super...
    }

    private int STEP_SKU = 0;
    private Product productoSecundTemp;

    //private static final int REQ_BUSQ_PROD_PPAL = 1;
    //private static final int REQ_BUSQ_PROD_SECD = 2;
    /*private void IniciarBusquedaProductoManual(int requestcode) {
        Intent intent = new Intent();

        intent.setClassName("com.pds.ficle.gestprod", "com.pds.ficle.gestprod.activity.ProductSearchActivity");

        intent.putExtra("TypeResultSearch", 2);

        startActivityForResult(intent, requestcode);
    }*/

    private WizardStepChangeListener stepValidationChange = new WizardStepChangeListener() {
        @Override
        public void onStepBack() {
            //no hay paso anterior...
        }

        @Override
        public void onStepShow(WizardStep _step) {
            //ocultamos teclado
            Window.CloseSoftKeyboard(ComboPromoWizardActivity.this);

            //1. validamos si existe el producto asociado a la promocion
            List<Product> products = new ProductDao(getContentResolver()).getByCode(promocionOrigen.getSkuProductoAsociado());
            if (products.size() < 1) {
                ShowMessageAndFinish("Producto promocional no válido", "Producto de la promoción desconocido");

                return;
            }

            //2. validamos que tenga precio de venta (asi el combo vendido fuera de vigencia no queda con precio cero)
            productoPpal = products.get(0);

            if (productoPpal.getSalePrice() == 0) {
                ShowMessageAndFinish("Producto promocional sin precio de venta", "Por favor, asigne el precio de venta al producto promocional para poder continuar");

                return;
            }

            //3. validamos que tenga precio de compra
            //  para la promo de precio fijo no es necesario exigir un precio de compra en los productos
            if (productoPpal.getPurchasePrice() == 0 && promocionOrigen.getTipoPromocion() != Promocion.TIPO_PROMOCION.PRECIO_FIJO_VENTA) {
                ShowMessageAndFinish("Producto promocional sin precio de compra", "Por favor, asigne el precio de compra al producto promocional para poder continuar");

                return;
            }

            //3. asignamos el producto ppal al combo
            if (combo == null)
                combo = new Combo();//instanciamos un objeto combo

            combo.setPromocion_id(promocionOrigen.getId());
            combo.setProducto_ppal_code(productoPpal.getCode());
            combo.setProducto_ppal_id(productoPpal.getId());
            combo.setCodigo("");
            combo.setFecha_alta(new Date());
            combo.setCantidad(1);
            combo.setSync("N");

            execWizStep(_step.nextStep);//saltamos al sgte paso, solicitar NOMBRE COMBO

        }

        @Override
        public boolean onStepNext() {
            //solo avanzamos
            return true;
        }
    };

    private WizardStepChangeListener stepNombreChange = new WizardStepChangeListener() {
        @Override
        public void onStepBack() {
        }

        @Override
        public void onStepShow(WizardStep _step) {
            //mostramos el nombre que teniamos
            if (combo != null)
                ((EditText) findViewById(R.id.combo_wiz_step_nombre_txt_nombre)).setText(combo.getNombre());

            Window.FocusViewShowSoftKeyboard(ComboPromoWizardActivity.this, findViewById(R.id.combo_wiz_step_nombre_txt_nombre));

            Window.AddDoneKeyboardAction(((EditText) findViewById(R.id.combo_wiz_step_nombre_txt_nombre)), _step.doneAction());
        }

        @Override
        public boolean onStepNext() {
            //tomamos el campo nombre ingresado

            EditText txtNombre = ((EditText) findViewById(R.id.combo_wiz_step_nombre_txt_nombre));

            String nombre = txtNombre.getText().toString().trim();

            //validamos que sea obligatorio
            txtNombre.setError(null);

            if (TextUtils.isEmpty(nombre)) {
                txtNombre.setError(getString(R.string.error_field_required));
                txtNombre.requestFocus();
                return false;
            } else {
                //agregamos el prefijo "COMBO"
                if (!nombre.startsWith(COMBO_PREFIX)) {
                    nombre = COMBO_PREFIX + " " + nombre;
                }

                combo.setNombre(nombre);
                return true;
            }
        }
    };

    private WizardStepChangeListener stepDescripcionChange = new WizardStepChangeListener() {
        @Override
        public void onStepBack() {
        }

        @Override
        public void onStepShow(WizardStep _step) {
            //mostramos el nombre que teniamos
            if (combo != null)
                ((EditText) findViewById(R.id.combo_wiz_step_descripcion_txt_descripcion)).setText(combo.getDescripcion());

            if (TextUtils.isEmpty(combo.getDescripcion())) {
                //si no tengo nombre corto, y el nombre ingresado, es menor a 15, entonces lo usamos como nombre corto
                if (combo.getNombre().length() <= 14)
                    ((EditText) findViewById(R.id.combo_wiz_step_descripcion_txt_descripcion)).setText(combo.getNombre());
            }

            Window.FocusViewShowSoftKeyboard(ComboPromoWizardActivity.this, findViewById(R.id.combo_wiz_step_descripcion_txt_descripcion));

            Window.AddDoneKeyboardAction(((EditText) findViewById(R.id.combo_wiz_step_descripcion_txt_descripcion)), _step.doneAction());

        }

        @Override
        public boolean onStepNext() {
            //tomamos el campo nombre ingresado

            EditText txtDescripcion = ((EditText) findViewById(R.id.combo_wiz_step_descripcion_txt_descripcion));

            String descripcion = txtDescripcion.getText().toString().trim();

            //validamos que sea obligatorio
            txtDescripcion.setError(null);

            if (TextUtils.isEmpty(descripcion)) {
                txtDescripcion.setError(getString(R.string.error_field_required));
                txtDescripcion.requestFocus();
                return false;
            } else {
                combo.setDescripcion(descripcion);
                return true;
            }

        }
    };

    private WizardStepChangeListener stepProdSecund = new WizardStepChangeListener() {
        @Override
        public void onStepBack() {
        }

        @Override
        public void onStepShow(final WizardStep _step) {
            //ocultamos teclado
            Window.CloseSoftKeyboard(ComboPromoWizardActivity.this);

            findViewById(R.id.combo_wiz_step_prodsecunds_si).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    execWizStep(stepProdsSecundAdd);
                }
            });

            findViewById(R.id.combo_wiz_step_prodsecunds_no).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    execWizStep(_step.nextStep);//saltamos al sgte paso, solicitar PRECIO COMBO
                }
            });

            //mostramos info del producto principal
            ((TextView) findViewById(R.id.combo_wiz_step_prodsecunds_ppal_cod)).setText(TextUtils.isEmpty(productoPpal.getCode()) ? "S/C" : productoPpal.getCode());
            ((TextView) findViewById(R.id.combo_wiz_step_prodsecunds_ppal_desc)).setText(productoPpal.getName());
            ((TextView) findViewById(R.id.combo_wiz_step_prodsecunds_ppal_cant)).setText(Integer.toString((int) combo.getCantidad()));

            //actualizamos los items
            if (_items != null) {
                ListView lstItems = (ListView) findViewById(R.id.combo_wiz_step_prodsecunds_lst_items);
                lstItems.setEmptyView(findViewById(android.R.id.empty));
                ItemsArrayAdapter listAdapter = new ItemsArrayAdapter(ComboPromoWizardActivity.this, _items);
                lstItems.setAdapter(listAdapter);

                if (_items.size() > 0)
                    ((TextView) findViewById(R.id.combo_wiz_step_prodsecunds_lbl_msj)).setText(getString(R.string.combo_wiz_prod_secund_add_lbl_2));
                else
                    ((TextView) findViewById(R.id.combo_wiz_step_prodsecunds_lbl_msj)).setText(getString(R.string.combo_wiz_prod_secund_add_lbl_1));
            } else
                ((TextView) findViewById(R.id.combo_wiz_step_prodsecunds_lbl_msj)).setText(getString(R.string.combo_wiz_prod_secund_add_lbl_1));
        }

        @Override
        public boolean onStepNext() {
            //solo avanzamos
            return true;
        }
    };

    private WizardStepChangeListener stepProdSecundAdd = new WizardStepChangeListener() {
        private void skuStep(boolean t) {
            STEP_SKU = (t ? 2 : 0);
        }

        @Override
        public void onStepBack() {
            productoSecundTemp = null;

            skuStep(false);
        }

        @Override
        public void onStepShow(final WizardStep _step) {
            productoSecundTemp = null;

            skuStep(true);

            /*findViewById(R.id.combo_wiz_step_cod_barra_btn_manual).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    IniciarBusquedaProductoManual(REQ_BUSQ_PROD_SECD);
                }
            });*/

            Window.FocusViewShowSoftKeyboard(ComboPromoWizardActivity.this, findViewById(R.id.combo_wiz_step_cod_barra_txt_code));

            Window.AddDoneKeyboardAction(((EditText) findViewById(R.id.combo_wiz_step_cod_barra_txt_code)), _step.doneAction());
        }

        @Override
        public boolean onStepNext() {
            //creamos el producto y tomamos el campo codigo

            EditText txtCode = ((EditText) findViewById(R.id.combo_wiz_step_cod_barra_txt_code));

            //validamos que sea obligatorio
            txtCode.setError(null);

            String code = txtCode.getText().toString().trim();

            if (TextUtils.isEmpty(code)) {
                txtCode.setError(getString(R.string.error_field_required));
                txtCode.requestFocus();
                return false;
            } else {

                List<Product> secund = new ProductDao(getContentResolver()).getByCode(code);

                if (secund != null && secund.size() > 0) {

                    if(!promocionOrigen.checkProductCondition(secund.get(0))){

                        //el producto seleccionado no cumple las condiciones
                        ShowMessage("Producto secundario no válido", "El producto seleccionado no cumple las condiciones de la promoción");
                        txtCode.setText("");
                        return false;
                    }
                    else if(secund.get(0).getSalePrice() == 0 && promocionOrigen.getTipoPromocion() != Promocion.TIPO_PROMOCION.PRECIO_FIJO_VENTA) {
                        //el producto seleccionado no tiene precio asociado (para los precio fijo no es necesario)
                        ShowMessage("Producto secundario sin precio", "Por favor, asigne precio de venta al producto para incluirlo a la promo");
                        txtCode.setText("");
                        return false;
                    }
                    else{
                        productoSecundTemp = secund.get(0);

                        skuStep(false);
                        return true;
                    }
                } else {
                    txtCode.setError("Producto desconocido");
                    txtCode.requestFocus();
                    txtCode.setText("");

                    return false;
                }

            }

        }
    };

    private WizardStepChangeListener stepProdSecundCantidadChange = new WizardStepChangeListener() {
        @Override
        public void onStepBack() {
        }

        @Override
        public void onStepShow(WizardStep _step) {
            //mostramos la cantidad que teniamos
            Window.FocusViewShowSoftKeyboard(ComboPromoWizardActivity.this, findViewById(R.id.combo_wiz_step_cantidad_txt_cantidad));

            Window.AddDoneKeyboardAction(((EditText) findViewById(R.id.combo_wiz_step_cantidad_txt_cantidad)), _step.doneAction());

            //if(promocionOrigen.getTipoPromocion() == Promocion.TIPO_PROMOCION.PRECIO_FIJO_VENTA){
                //asumimos cantidad 1 y avanzamos
                EditText txtCantidad = ((EditText) findViewById(R.id.combo_wiz_step_cantidad_txt_cantidad));
                txtCantidad.setText("1");

                _step.nextButton.performClick();
            //}
        }

        @Override
        public boolean onStepNext() {

            //tomamos el campo cantidad ingresado
            EditText txtCantidad = ((EditText) findViewById(R.id.combo_wiz_step_cantidad_txt_cantidad));

            //validamos que sea obligatorio
            txtCantidad.setError(null);

            String cantidad = txtCantidad.getText().toString().trim();

            if (TextUtils.isEmpty(cantidad)) {
                txtCantidad.setError(getString(R.string.error_field_required));
                txtCantidad.requestFocus();
                return false;
            } else {
                double cant = Formato.ParseaDecimal(cantidad, Formato.Decimal_Format.US);

                if (cant <= 0) {
                    txtCantidad.setError(getString(R.string.error_invalid_value));
                    txtCantidad.requestFocus();
                    return false;
                } else {
                    //creamos un item de combo
                    ComboItem item = new ComboItem();
                    item.setCantidad(cant);
                    item.setProducto_id(productoSecundTemp.getId());
                    item.setProducto_code(productoSecundTemp.getCode());
                    item.setProducto_precio_vta(productoSecundTemp.getSalePrice());
                    item.setSync("N");

                    //inicializamos la lista de items
                    if (_items == null)
                        _items = new ArrayList<ComboItem>();

                    _items.clear();
                    _items.add(item);//solo nos quedamos con un solo item

                    productoSecundTemp = null;

                    return true;
                }
            }
        }



        /*private void assignUnit(String unit) {
            product.setUnit(unit);

            updateLabelContenido();
        }*/

        /*private void updateLabelContenido() {

            //unidad
            String contenido = ((EditText) findViewById(R.id.prod_wiz_step_contenido_txt_contenido)).getText().toString().trim();
            String lbl = String.format("%s%s", contenido, product.getUnit());

            ((TextView) findViewById(R.id.prod_wiz_step_contenido_lbl_contenido)).setText(lbl);
        }*/


    };

    private WizardStepChangeListener stepEndChange = new WizardStepChangeListener() {
        @Override
        public void onStepBack() {
        }

        @Override
        public void onStepShow(WizardStep _step) {
            //cerramos el teclado
            Window.CloseSoftKeyboard(ComboPromoWizardActivity.this);

            //calculamos el precio del combo promocional durante la vigencia
            double precioComboPromocional = promocionOrigen.calcularPrecioVenta(
                    getContentResolver(),
                    combo,
                    productoPpal,
                    _items,
                    true //asumimos vigencia de la promo
            );


            //calculamos el precio del combo promocional actual
            double precioComboActual = promocionOrigen.calcularPrecioVenta(
                    getContentResolver(),
                    combo,
                    productoPpal,
                    _items
            );

            String precio_actual = Formato.FormateaDecimal(
                    precioComboActual,
                    FORMATO_DECIMAL, Formato.getCurrencySymbol(FORMATO_DECIMAL));

            final String precio_formateado = Formato.FormateaDecimal(
                    precioComboPromocional,
                    FORMATO_DECIMAL, Formato.getCurrencySymbol(FORMATO_DECIMAL));

            findViewById(R.id.combo_wiz_step_valida_btn_print).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Sku_Utils.ImprimirEtiqueta(ComboPromoWizardActivity.this, config.PRINTER_MODEL, combo.getProducto_ppal_code(), combo.getNombre(), "", false, "");
                }
            });

            findViewById(R.id.combo_wiz_step_valida_btn_print_precio).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Sku_Utils.ImprimirEtiqueta(ComboPromoWizardActivity.this, config.PRINTER_MODEL, combo.getProducto_ppal_code(), combo.getNombre(), precio_formateado, true, "");
                }
            });


            //mostramos los datos del combo
            if (combo != null) {

                //guardamos como referencia el precio actual promocional del combo (para luego poder calcular el descuento aplicado)
                combo.setPrecio(precioComboPromocional);

                ((TextView) findViewById(R.id.combo_wiz_step_valida_lbl_nombre)).setText(combo.getNombre());
                ((TextView) findViewById(R.id.combo_wiz_step_valida_lbl_desc_corta)).setText(combo.getDescripcion());
                ((TextView) findViewById(R.id.combo_wiz_step_valida_lbl_precio)).setText(precio_actual);
                ((TextView) findViewById(R.id.combo_wiz_step_valida_lbl_precio_prom)).setText(precio_formateado);
                ((TextView) findViewById(R.id.combo_wiz_step_valida_lbl_sku)).setText(combo.getProducto_ppal_code());

                //actualizamos los items
                List<ComboItem> _itemsMasPrincipal = new ArrayList<ComboItem>();

                //creamos un ComboItem temporalmente para el producto principal
                ComboItem itemPpalTemp = new ComboItem();
                itemPpalTemp.setProducto_code(productoPpal.getCode());
                itemPpalTemp.setProducto_id(productoPpal.getId());
                itemPpalTemp.setCantidad(combo.getCantidad());

                _itemsMasPrincipal.add(itemPpalTemp);

                if (_items != null)
                    _itemsMasPrincipal.addAll(_items);

                ListView lstItems = (ListView) findViewById(R.id.combo_wiz_step_valida_lst_items);
                lstItems.setEmptyView(findViewById(android.R.id.empty));
                ItemsArrayAdapter listAdapter = new ItemsArrayAdapter(ComboPromoWizardActivity.this, _itemsMasPrincipal);
                lstItems.setAdapter(listAdapter);

            }

        }

        @Override
        public boolean onStepNext() {

            try {

                //crear combo
                boolean success = new ComboDao(getContentResolver()).saveOrUpdate(combo);

                if (success) {

                    //grabamos el producto como principal de un combo
                    productoPpal.setType("P"); //lo marcamos como producto ppal de un combo

                    new ProductDao(getContentResolver()).saveOrUpdate(productoPpal);

                    //grabamos los items, si los tiene
                    if (_items != null) {
                        ComboItemDao comboItemDao = new ComboItemDao(getContentResolver());

                        for (ComboItem det : _items) {
                            //asociamos la cabecera y grabamos
                            det.setCombo_id(combo.getId());
                            det.setSync("N");

                            comboItemDao.saveOrUpdate(det);
                        }
                    }

                    Logger.RegistrarEvento(getContentResolver(), "i", "ALTA COMBO PROMOCIONAL", String.format("COD.PROMO: %s - SKU PPAL: %s", promocionOrigen.getCodPromocion(), combo.getProducto_ppal_code()), "id: " + String.valueOf(combo.getId()));

                    AlertDialog.Builder builder = new AlertDialog.Builder(ComboPromoWizardActivity.this);
                    builder.setIcon(android.R.drawable.ic_dialog_info);
                    builder.setTitle("NUEVA PROMO");
                    builder.setMessage(Html.fromHtml(String.format("La promo <b>\"%s\"</b> fue generada correctamente!", combo.getNombre())));
                    builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface _dialog, int which) {
                            _dialog.dismiss();

                            finish();
                        }
                    });
                    builder.setCancelable(false);
                    builder.show();

                    return false;
                } else {
                    Toast.makeText(getApplicationContext(), "Error al generar la promo", Toast.LENGTH_SHORT).show();
                    return false;
                }

            } catch (Exception ex) {
                Toast.makeText(ComboPromoWizardActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();

                return false;

            }
        }
    };

    public void execWizStep(WizardStep step) {
        //aqui deberíamos completar el paso

        currentStep = step;

        //title
        ((TextView) findViewById(R.id.combo_wiz_lbl_title)).setText(step.titleId);

        final WizardStepChangeListener listener = step.stepChangeListener;

        //back
        if (step.previousStep != null) {
            final WizardStep previousStep = step.previousStep;
            findViewById(R.id.combo_wiz_btn_back).setVisibility(View.VISIBLE);
            findViewById(R.id.combo_wiz_btn_back).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        listener.onStepBack();
                    execWizStep(previousStep);
                }
            });
        } else
            findViewById(R.id.combo_wiz_btn_back).setVisibility(View.INVISIBLE);

        //next
        if (step.nextStep != null) {
            final WizardStep nextStep = step.nextStep;
            ((Button) findViewById(R.id.combo_wiz_btn_next)).setText("SIGUIENTE");
            findViewById(R.id.combo_wiz_btn_next).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        if (listener.onStepNext())
                            execWizStep(nextStep);
                    } else
                        execWizStep(nextStep);
                }
            });
            step.nextButton = ((Button) findViewById(R.id.combo_wiz_btn_next));
        } else {
            ((Button) findViewById(R.id.combo_wiz_btn_next)).setText("GRABAR");
            findViewById(R.id.combo_wiz_btn_next).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        if (listener.onStepNext()) {
                            finish();//salimos
                        }
                }
            });
        }
        ((Button) findViewById(R.id.combo_wiz_btn_next)).setVisibility(step.nextButtonViewMode);


        //inflate view
        LinearLayout container = ((LinearLayout) findViewById(R.id.combo_wiz_lay_step));
        container.removeAllViews();

        if (step.viewId != -1) {
            View view = getLayoutInflater().inflate(step.viewId, null);
            container.addView(view, 0);
        }

        //ejecutamos el evento
        if (listener != null)
            listener.onStepShow(step);
    }

    public class ItemsArrayAdapter extends ArrayAdapter<ComboItem> {
        private final Context context;
        private List<ComboItem> items;

        public ItemsArrayAdapter(Context context, List<ComboItem> items) {
            super(context, R.layout.list_item_combo_detalle, items);
            this.context = context;
            this.items = items;
        }

        @Override
        public View getView(int position, View rowView, ViewGroup parent) {

            if (rowView == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                rowView = inflater.inflate(R.layout.list_item_combo_detalle, parent, false);
            }

            ComboItem item = items.get(position);

            Product p = item.getProducto(getContentResolver());

            ((TextView) rowView.findViewById(R.id.list_item_producto_cod)).setText(TextUtils.isEmpty(p.getCode()) ? "S/C" : p.getCode());
            ((TextView) rowView.findViewById(R.id.list_item_pedidos_producto_det)).setText(p.getName());
            ((TextView) rowView.findViewById(R.id.list_item_pedidos_cantidad)).setText(Integer.toString((int) item.getCantidad()));

            return rowView;
        }

    }

    private void ShowMessageAndFinish(String title, String message) {
        Dialog.Alert(ComboPromoWizardActivity.this, title, message, android.R.drawable.ic_dialog_alert, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
    }

    private void ShowMessage(String title, String message) {
        Dialog.Alert(ComboPromoWizardActivity.this, title, message, android.R.drawable.ic_dialog_alert);
    }
}
