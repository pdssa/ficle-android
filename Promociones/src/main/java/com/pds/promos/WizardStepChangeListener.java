package com.pds.promos;

/**
 * Created by Hernan on 15/02/2016.
 */
public interface WizardStepChangeListener {

    void onStepBack();

    boolean onStepNext();

    void onStepShow(WizardStep step);

}
