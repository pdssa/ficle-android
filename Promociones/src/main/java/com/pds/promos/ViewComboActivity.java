package com.pds.promos;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Formato;
import com.pds.common.Logger;
import com.pds.common.activity.TimerActivity;
import com.pds.common.dao.ComboDao;
import com.pds.common.dao.ProductDao;
import com.pds.common.dialog.ConfirmDialog;
import com.pds.common.model.Combo;
import com.pds.common.model.ComboItem;
import com.pds.common.model.Product;
import com.pds.common.util.Sku_Utils;

import java.util.ArrayList;
import java.util.List;

public class ViewComboActivity extends TimerActivity  {

    public static final String KEY_COMBO_VIEW = "__COMBO_VIEW";

    private Combo combo;
    private Product productoPpal;
    private Config config;
    private Formato.Decimal_Format FORMATO_DECIMAL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_combo);

        //cargamos el producto
        Intent intent = getIntent();
        combo = intent.getParcelableExtra(KEY_COMBO_VIEW);

        if (combo == null) {
            Toast.makeText(this, "Error en parametros recibidos", Toast.LENGTH_SHORT).show();
            finish();
        } else {
            //formato
            config = new Config(this);
            FORMATO_DECIMAL = Formato.getDecimal_Format(config.PAIS);

            LoadCombo(combo);
        }

        findViewById(R.id.combo_btn_delete_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ConfirmDialog(ViewComboActivity.this, "ELIMINAR PROMO", "¿Esta seguro que desea eliminar la promo?")
                        .set_positiveButtonText("ACEPTAR")
                        .set_positiveButtonAction(new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Eliminar(combo);
                            }
                        })
                        .set_negativeButtonText("CANCELAR")
                        .set_negativeButtonAction(new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });

        findViewById(R.id.combo_btn_back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //finish();
                volver();
            }
        });


        findViewById(R.id.combo_btn_print).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Sku_Utils.ImprimirEtiqueta(ViewComboActivity.this, config.PRINTER_MODEL, combo.getProducto_ppal_code(), combo.getNombre(), "", false, "");
            }
        });

        findViewById(R.id.combo_btn_print_precio).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String precio_formateado = Formato.FormateaDecimal(combo.getPrecio(getContentResolver()), FORMATO_DECIMAL, Formato.getCurrencySymbol(FORMATO_DECIMAL));

                Sku_Utils.ImprimirEtiqueta(ViewComboActivity.this, config.PRINTER_MODEL, combo.getProducto_ppal_code(), combo.getNombre(), precio_formateado, true, "");
            }
        });

    }

    private void LoadCombo(Combo _combo) {
        //datos combo
        ((TextView) findViewById(R.id.combo_wiz_step_valida_lbl_nombre)).setText(_combo.getNombre());
        ((TextView) findViewById(R.id.combo_wiz_step_valida_lbl_desc_corta)).setText(_combo.getDescripcion());
        ((TextView) findViewById(R.id.combo_wiz_step_valida_lbl_precio_prom)).setText(Formato.FormateaDecimal(_combo.getPrecioPromocionalForzado(getContentResolver()), FORMATO_DECIMAL, Formato.getCurrencySymbol(FORMATO_DECIMAL)));
        ((TextView) findViewById(R.id.combo_wiz_step_valida_lbl_precio)).setText(Formato.FormateaDecimal(_combo.getPrecio(getContentResolver()), FORMATO_DECIMAL, Formato.getCurrencySymbol(FORMATO_DECIMAL)));
        ((TextView) findViewById(R.id.combo_wiz_step_valida_lbl_sku)).setText(_combo.getProducto_ppal_code());

        ((TextView) findViewById(R.id.combo_lbl_name)).setText(TextUtils.isEmpty(_combo.getDescripcion()) ? _combo.getNombre() : _combo.getDescripcion());


        //producto ppal
        productoPpal = _combo.getProductoPrincipal(getContentResolver());

        if(productoPpal == null)
            return ;//TODO : check for null...

        //datos de los items
        List<ComboItem> _itemsMasPrincipal = new ArrayList<ComboItem>();

        //creamos un ComboItem temporalmente para el producto principal
        ComboItem itemPpalTemp = new ComboItem();
        itemPpalTemp.setProducto_code(productoPpal.getCode());
        itemPpalTemp.setProducto_id(productoPpal.getId());
        itemPpalTemp.setCantidad(_combo.getCantidad());

        _itemsMasPrincipal.add(itemPpalTemp);

        List<ComboItem> _items = _combo.getItemsCombo(getContentResolver());

        if (_items != null)
            _itemsMasPrincipal.addAll(_items);

        ListView lstItems = (ListView) findViewById(R.id.combo_wiz_step_valida_lst_items);
        lstItems.setEmptyView(findViewById(android.R.id.empty));
        ItemsArrayAdapter listAdapter = new ItemsArrayAdapter(ViewComboActivity.this, _itemsMasPrincipal);
        lstItems.setAdapter(listAdapter);

    }

    private void Eliminar(Combo _combo) {
        try {
            if (_combo != null) {
                _combo.setBaja(true);
                _combo.setSync("N");

                ComboDao comboDao = new ComboDao(getContentResolver());

                boolean result = comboDao.saveOrUpdate(_combo);

                //tengo que desmarcar el producto como ppal si era el unico combo que tenia asociado
                boolean updateProduct = (comboDao.first(String.format("producto_ppal_id = %d and baja = 0", productoPpal.getId()), null, null) == null);

                if (updateProduct) {
                    //ya no quedan combos habilitados con ese producto como principal
                    //grabamos el producto como principal de un combo
                    productoPpal.setType(""); //lo desmarcamos como producto ppal de un combo

                    new ProductDao(getContentResolver()).saveOrUpdate(productoPpal);
                }

                if (result) {
                    Logger.RegistrarEvento(getContentResolver(), "i", "BAJA COMBO", "SKU PPAL: " + _combo.getProducto_ppal_code(), "id: " + String.valueOf(_combo.getId()));

                    AlertDialog.Builder builder = new AlertDialog.Builder(ViewComboActivity.this);
                    builder.setIcon(android.R.drawable.ic_dialog_info);
                    builder.setTitle("ELIMINAR PROMO");
                    builder.setMessage(Html.fromHtml(String.format("El combo <b>\"%s\"</b> fue eliminado correctamente!", _combo.getNombre())));
                    builder.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface _dialog, int which) {
                            _dialog.dismiss();

                            volver();
                        }
                    });
                    builder.setCancelable(false);
                    builder.show();


                } else
                    Toast.makeText(ViewComboActivity.this, "No se ha podido eliminar la promo", Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(ViewComboActivity.this, "No se ha podido eliminar la promo", Toast.LENGTH_SHORT).show();

        } catch (Exception ex) {
            Toast.makeText(ViewComboActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        volver();
    }

    private void volver() {
        finish();
    }

    public class ItemsArrayAdapter extends ArrayAdapter<ComboItem> {
        private final Context context;
        private List<ComboItem> items;

        public ItemsArrayAdapter(Context context, List<ComboItem> items) {
            super(context, R.layout.list_item_combo_detalle, items);
            this.context = context;
            this.items = items;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View rowView = inflater.inflate(R.layout.list_item_combo_detalle, parent, false);

            ComboItem item = items.get(position);

            Product p = item.getProducto(getContentResolver());

            ((TextView) rowView.findViewById(R.id.list_item_producto_cod)).setText(TextUtils.isEmpty(p.getCode()) ? "S/C" : p.getCode());
            ((TextView) rowView.findViewById(R.id.list_item_pedidos_producto_det)).setText(p.getName());
            ((TextView) rowView.findViewById(R.id.list_item_pedidos_cantidad)).setText(Integer.toString((int) item.getCantidad()));

            return rowView;
        }

    }
}

