package com.pds.promos;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pds.common.model.Promocion;
import com.pds.common.util.UriUtils;

import java.util.List;

/**
 * Created by Hernan on 06/06/2016.
 */
public class PromocionesAdapter extends RecyclerView.Adapter implements View.OnClickListener {

    private Context context;
    private List<Promocion> listaDatos;

    public PromocionesAdapter(Context context, List<Promocion> promociones){
        this.context = context;
        this.listaDatos = promociones;
    }

    private PromocionesListener listener;

    public interface PromocionesListener{
        void onPromoSelected(View view);
    }

    public void setPromocionesListener(PromocionesListener listener){
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vistaPromo = LayoutInflater.from(this.context).inflate(R.layout.promo_item_view, parent, false );

        PromocionViewHolder promocionViewHolder = new PromocionViewHolder(vistaPromo);

        vistaPromo.setOnClickListener(this);

        return promocionViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((PromocionViewHolder)holder).bindPromocion(listaDatos.get(position));
    }

    @Override
    public int getItemCount() {
        return listaDatos.size();
    }

    @Override
    public void onClick(View v) {
        if(this.listener != null)
            this.listener.onPromoSelected(v);
    }


    private class PromocionViewHolder extends RecyclerView.ViewHolder{
        private TextView txtNombre;
        private TextView txtProducto;
        private TextView txtLeyendaPromoNoVigente;
        private ImageView imgProducto;

        public PromocionViewHolder(View view){
            super(view);
            this.txtNombre = (TextView)view.findViewById(R.id.promo_txt_nombre);
            this.txtProducto = (TextView)view.findViewById(R.id.promo_txt_producto);
            this.txtLeyendaPromoNoVigente = (TextView)view.findViewById(R.id.promo_txt_leyenda_no_vigente);
            this.imgProducto = (ImageView) view.findViewById(R.id.promo_img_imagen);
        }

        public void bindPromocion(Promocion promocion){
            this.txtNombre.setText(promocion.getNombrePromocion());
            this.txtProducto.setText("SKU: " + promocion.getSkuProductoAsociado());
            //this.imgProducto.setImageResource(promocion.getImgProductoAsociado());
            this.imgProducto.setImageURI(UriUtils.resolveUri(context, promocion.getUriImgProductoAsociado()));

            if(promocion.promocionExpirada())
                txtLeyendaPromoNoVigente.setVisibility(View.VISIBLE);
            else
                txtLeyendaPromoNoVigente.setVisibility(View.GONE);

        }
    }

}
