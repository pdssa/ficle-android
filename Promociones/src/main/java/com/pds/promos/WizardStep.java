package com.pds.promos;

import android.view.View;
import android.widget.Button;

import java.util.concurrent.Callable;

/**
 * Created by Hernan on 15/02/2016.
 */
public class WizardStep {

    public int titleId;
    public int viewId;
    public WizardStep nextStep;
    public WizardStep previousStep;
    public WizardStepChangeListener stepChangeListener;
    public int nextButtonViewMode;
    public Button nextButton;

    public WizardStep(int _titleId, int _viewId, WizardStepChangeListener _stepChangeListener) {
        this.titleId = _titleId;
        this.viewId = _viewId;
        this.stepChangeListener = _stepChangeListener;
        this.nextButtonViewMode = View.VISIBLE;
    }

    public WizardStep(int _titleId, int _viewId, WizardStepChangeListener _stepChangeListener, int _nextButtonViewMode) {
        this(_titleId, _viewId, _stepChangeListener);

        this.nextButtonViewMode = _nextButtonViewMode;
    }

    public void flows(WizardStep _previous, WizardStep _next) {
        this.nextStep = _next;
        this.previousStep = _previous;
    }

    public Callable<Void> doneAction() {
        if (nextButton != null) {
            return new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    nextButton.performClick();
                    return null;
                }
            };
        } else
            return null;
    }


}

