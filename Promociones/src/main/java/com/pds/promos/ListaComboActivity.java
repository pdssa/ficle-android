package com.pds.promos;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Formatos;
import com.pds.common.activity.TimerActivity;
import com.pds.common.fragment.CombosListFragment;
import com.pds.common.model.Combo;

import java.text.DecimalFormat;

public class ListaComboActivity extends TimerActivity implements CombosListFragment.ComboListFragmentInteractionListener {

    private Button btnVolver;
    private DecimalFormat FORMATO_DECIMAL;
    private Config config;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_combo);

        InitViews();

        InitEvents();

        config = new Config(this);

        Init_CustomConfigPais(config.PAIS);
    }

    @Override
    protected void onResume() {
        super.onResume();

        CargarListaCombos();
    }

    private void Init_CustomConfigPais(String codePais) {
        FORMATO_DECIMAL = codePais.equals("AR") ? Formatos.DecimalFormat : Formatos.DecimalFormat_CH;
    }

    private void CargarListaCombos(){
        CombosListFragment combosListFragment = CombosListFragment.newInstance(FORMATO_DECIMAL, true);

        combosListFragment.setComboListFragmentInteractionListener(this);

        getFragmentManager().beginTransaction().replace(R.id.lista_combo_fragment_container, combosListFragment).commit();
    }

    private void InitViews() {
        btnVolver = (Button) findViewById(R.id.lista_combo_back_button);
    }

    private void InitEvents() {
        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onComboSelected(Combo combo) {
        Intent intent = new Intent(ListaComboActivity.this, ViewComboActivity.class);
        intent.putExtra(ViewComboActivity.KEY_COMBO_VIEW, combo);
        startActivity(intent);
        overridePendingTransition(0, 0);
    }
}
