package com.pds.promos;

import android.app.Fragment;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pds.common.model.Promocion;
import com.pds.common.util.UriUtils;

/**
 * Created by Hernan on 06/06/2016.
 */
public class DetailPromoFragment extends Fragment {

    private static final String KEY_PROMO = "_PROMO";

    private DetailPromoInterface detailPromoInterface;

    public interface DetailPromoInterface{
        void onPromoAccept(Promocion promo);
        void onPromoCancel();
    }

    public void setDetailPromoInterface(DetailPromoInterface detailPromoInterface) {
        this.detailPromoInterface = detailPromoInterface;
    }

    public static DetailPromoFragment newInstance(Promocion promo){
        DetailPromoFragment detailPromoFragment = new DetailPromoFragment();

        Bundle bundle = new Bundle();

        bundle.putParcelable(KEY_PROMO, promo);

        detailPromoFragment.setArguments(bundle);

        return detailPromoFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_promo_detail, container, false);

        final Promocion promo = (Promocion) getArguments().getParcelable(KEY_PROMO);

        view.findViewById(R.id.prom_det_btn_volver).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(detailPromoInterface != null)
                    detailPromoInterface.onPromoCancel();
            }
        });

        view.findViewById(R.id.prom_det_btn_ingresar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(detailPromoInterface != null)
                    detailPromoInterface.onPromoAccept(promo);
            }
        });

        ShowPromoDetail(view, promo);

        return view;
    }

    private void ShowPromoDetail(View view, Promocion promo){

        ((TextView)view.findViewById(R.id.prom_det_txt_nombprod)).setText(promo.getNombrePromocion());
        ((TextView)view.findViewById(R.id.prom_det_txt_codprod)).setText("SKU: " + promo.getSkuProductoAsociado());
        ((TextView)view.findViewById(R.id.prom_det_txt_codprom)).setText(String.format("PROMOCION: [%s]", promo.getCodPromocion()));
        ((TextView)view.findViewById(R.id.prom_det_txt_promocion)).setText(Html.fromHtml(promo.getTextoPromocion()));
        ((TextView)view.findViewById(R.id.prom_det_txt_vigencia)).setText("VIGENCIA: " + promo.getVigenciaString());
        //((ImageView)view.findViewById(R.id.prom_det_img_producto)).setImageResource(promo.getImgProductoAsociado());
        ((ImageView)view.findViewById(R.id.prom_det_img_producto)).setImageURI(UriUtils.resolveUri(getActivity(), promo.getUriImgProductoAsociado()));

        if(promo.promocionExpirada()){
            //si la promo expiró, no dejamos crear combos y mostramos leyenda de promo expirada
            view.findViewById(R.id.prom_det_txt_no_vigente).setVisibility(View.VISIBLE);
            view.findViewById(R.id.prom_det_btn_ingresar).setVisibility(View.GONE);
        }
        else{
            //si la promo no expiró, dejamos crear combos, y no mostramos leyenda de promo expirada
            view.findViewById(R.id.prom_det_txt_no_vigente).setVisibility(View.GONE);
            view.findViewById(R.id.prom_det_btn_ingresar).setVisibility(View.VISIBLE);
        }

    }
}
