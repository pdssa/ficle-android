package com.pds.promos;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Formato;
import com.pds.common.Logger;
import com.pds.common.dao.PromocionDao;
import com.pds.common.db.PromocionTable;
import com.pds.common.model.Promocion;
import com.pds.common.model.PromocionCondicion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity implements PromocionesAdapter.PromocionesListener, DetailPromoFragment.DetailPromoInterface {

    private List<Promocion> promocionesCrear;
    private RecyclerView recyclerview;
    private DetailPromoFragment detailFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            ((TextView) findViewById(R.id.txtVersion)).setText("Version:" + getPackageManager().getPackageInfo(this.getPackageName().toString(), 0).versionName);

            findViewById(R.id.prom_main_btn_back).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });

            findViewById(R.id.prom_main_btn_combos).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MainActivity.this, ListaComboActivity.class));
                }
            });


            promocionesCrear = new ArrayList<>();

            crearPromociones();

            PromocionesAdapter promocionesAdapter = new PromocionesAdapter(MainActivity.this,promocionesCrear);
            promocionesAdapter.setPromocionesListener(this);

            recyclerview = (RecyclerView)findViewById(R.id.prom_main_rec_view_promos);
            recyclerview.setAdapter(promocionesAdapter);
            recyclerview.setHasFixedSize(true);
            recyclerview.setLayoutManager(new GridLayoutManager(MainActivity.this, 4));

            if (promocionesCrear.isEmpty()) {
                recyclerview.setVisibility(View.GONE);
                findViewById(R.id.empty_view).setVisibility(View.VISIBLE);
            }
            else {
                recyclerview.setVisibility(View.VISIBLE);
                findViewById(R.id.empty_view).setVisibility(View.GONE);
            }

        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, "Error al inicializar app", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPromoSelected(View view) {
        Promocion promo = promocionesCrear.get(recyclerview.getChildAdapterPosition(view));

        ShowPromoDetail(promo);
    }

    private void crearPromociones(){
        //List<Promocion> promocionesCrear = new ArrayList<>();

        List<PromocionCondicion> condicionesBotillerias = new ArrayList<>();
        condicionesBotillerias.add(new PromocionCondicion(PromocionCondicion.TIPO_CONDICION.SUBDEPARTAMENTO_ESPECIFICO, "002001"));

        List<PromocionCondicion> condicionesAlmacenes = new ArrayList<>();
        condicionesAlmacenes.add(new PromocionCondicion(PromocionCondicion.TIPO_CONDICION.PRODUCTO_ESPECIFICO, "7801610002049"));
        condicionesAlmacenes.add(new PromocionCondicion(PromocionCondicion.TIPO_CONDICION.PRODUCTO_ESPECIFICO, "7801610005040"));
        condicionesAlmacenes.add(new PromocionCondicion(PromocionCondicion.TIPO_CONDICION.PRODUCTO_ESPECIFICO, "7801610001042"));
        condicionesAlmacenes.add(new PromocionCondicion(PromocionCondicion.TIPO_CONDICION.PRODUCTO_ESPECIFICO, "7801610350980"));

        String desde1 = "2016-07-15 18:00:00", hasta1 = "2016-07-17 23:59:59";
        String desde2 = "2016-07-29 18:00:00", hasta2 = "2016-07-31 23:59:59";

        String desde = desde1;
        String hasta = hasta1;

        //si se venció el primer periodo de vigencia de las promos, usamos el segundo
        try {
            if(new Date().compareTo(Formato.DbDate(hasta)) > 0){
                desde = desde2;
                hasta = hasta2;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Config config = new Config(this);

        if(config.RUBRO.equalsIgnoreCase("ALM")) {

            //PROMO ALMACENES
            String leyendaAlmacenes = "Promo <font color='red'><b>2x $990</b></font> combinando este producto<br/>con cualquier otra FANTA, COCA, SPRITE, COCA-ZERO de 1lt RETORNABLE";

            promocionesCrear.add(new Promocion(1, "FANTA 1 lt RETORNABLE", "ALM0001-1", leyendaAlmacenes, "7801610002049", "android.resource://com.pds.promos/drawable/fanta_logo", desde, hasta, Promocion.TIPO_PROMOCION.PRECIO_FIJO_VENTA, 990, condicionesAlmacenes));
            promocionesCrear.add(new Promocion(2, "SPRITE 1 lt RETORNABLE", "ALM0001-2", leyendaAlmacenes, "7801610005040", "android.resource://com.pds.promos/drawable/sprite_logo", desde, hasta, Promocion.TIPO_PROMOCION.PRECIO_FIJO_VENTA, 990, condicionesAlmacenes));
            promocionesCrear.add(new Promocion(3, "COCA-COLA 1 lt RETORNABLE", "ALM0001-3", leyendaAlmacenes, "7801610001042", "android.resource://com.pds.promos/drawable/coca_logo", desde, hasta, Promocion.TIPO_PROMOCION.PRECIO_FIJO_VENTA, 990, condicionesAlmacenes));
            promocionesCrear.add(new Promocion(4, "COCA-COLA ZERO 1 lt RETORNABLE", "ALM0001-4", leyendaAlmacenes, "7801610350980", "android.resource://com.pds.promos/drawable/coca_zero_logo", desde, hasta, Promocion.TIPO_PROMOCION.PRECIO_FIJO_VENTA, 990, condicionesAlmacenes));

        }
        else if(config.RUBRO.equalsIgnoreCase("BOT")){

            //PROMO BOTILLERIAS
            String leyendaBotilleria = "Promo con <font color='red'><b>15% de descuento</b></font> sobre el precio<br/>de compra combinando este producto con algún licor";

            promocionesCrear.add(new Promocion(5,"COCA-COLA 1,5 DESECHABLE", "BOT0001-1", leyendaBotilleria,"7801610001622", "android.resource://com.pds.promos/drawable/coca_logo", desde, hasta, Promocion.TIPO_PROMOCION.DESCUENTO_PROD_PPAL, 15, condicionesBotillerias));
            promocionesCrear.add(new Promocion(6,"SPRITE  1,5 DESECHABLE","BOT0001-2", leyendaBotilleria, "7801610005262", "android.resource://com.pds.promos/drawable/sprite_logo", desde, hasta, Promocion.TIPO_PROMOCION.DESCUENTO_PROD_PPAL, 15, condicionesBotillerias));
            promocionesCrear.add(new Promocion(7,"SPRITE ZERO 1,5 DESECHABLE","BOT0001-3", leyendaBotilleria, "7801610223727", "android.resource://com.pds.promos/drawable/sprite_zero_logo", desde, hasta, Promocion.TIPO_PROMOCION.DESCUENTO_PROD_PPAL, 15, condicionesBotillerias));
            promocionesCrear.add(new Promocion(8,"FANTA 1,5 DESECHABLE","BOT0001-4", leyendaBotilleria, "7801610002261", "android.resource://com.pds.promos/drawable/fanta_logo", desde, hasta, Promocion.TIPO_PROMOCION.DESCUENTO_PROD_PPAL, 15, condicionesBotillerias));
            promocionesCrear.add(new Promocion(9,"FANTA ZERO 1,5 DESECHABLE", "BOT0001-5", leyendaBotilleria, "7801610671030", "android.resource://com.pds.promos/drawable/fanta_zero_logo", desde, hasta, Promocion.TIPO_PROMOCION.DESCUENTO_PROD_PPAL, 15, condicionesBotillerias));
            promocionesCrear.add(new Promocion(10,"NORDIC GINGER ALE 1,5 DESECHABLE","BOT0001-6", leyendaBotilleria, "7801610019269", "android.resource://com.pds.promos/drawable/nordic_ginger_ale_logo", desde, hasta, Promocion.TIPO_PROMOCION.DESCUENTO_PROD_PPAL, 15, condicionesBotillerias));
            promocionesCrear.add(new Promocion(11,"NORDIC TONICA 1,5 DESECHABLE","BOT0001-7", leyendaBotilleria, "7801610020265", "android.resource://com.pds.promos/drawable/nordic_logo", desde, hasta, Promocion.TIPO_PROMOCION.DESCUENTO_PROD_PPAL, 15, condicionesBotillerias));
            promocionesCrear.add(new Promocion(12, "NORDIC ZERO 1,5 DESECHABLE", "BOT0001-8", leyendaBotilleria, "7801610271056", "android.resource://com.pds.promos/drawable/nordic_zero_logo", desde, hasta, Promocion.TIPO_PROMOCION.DESCUENTO_PROD_PPAL, 15, condicionesBotillerias));

        }

        PromocionDao promocionDao = new PromocionDao(getContentResolver());

        for (Promocion p : promocionesCrear) {
            promocionDao.saveOrUpdate(p);
        }

        Logger.RegistrarEvento(MainActivity.this, "i", "Promos", "Fechas: " + desde + " " + hasta);
    }

    private List<Promocion> cargarPromociones(){
        return new PromocionDao(getContentResolver()).list(PromocionTable.COLUMN_BAJA + " = 0", null, PromocionTable.COLUMN_ID + " ASC");
    }

    private void ShowPromoDetail(Promocion promo){
        detailFragment = DetailPromoFragment.newInstance(promo);

        detailFragment.setDetailPromoInterface(this);

        getFragmentManager().beginTransaction().add(R.id.main_fragment, detailFragment).commit();
    }

    @Override
    public void onPromoAccept(Promocion promocion) {
        closePromoDetail();

        startWizardNewCombo(promocion);
    }

    @Override
    public void onPromoCancel() {
        closePromoDetail();
    }

    private void closePromoDetail(){
        if(detailFragment != null)
            getFragmentManager().beginTransaction().remove(detailFragment).commit();
    }

    private void startWizardNewCombo(Promocion promocionOrigen){
        Intent intent = new Intent(MainActivity.this, ComboPromoWizardActivity.class);

        intent.putExtra(ComboPromoWizardActivity.PROMOCION_KEY , promocionOrigen);

        startActivity(intent);
    }
}
