package android_serialport_api;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

import hdx.HdxUtil;
import hdx.pwm.PWMControl;


/**
 * Created by Hernan on 19/06/2014.
 */
public class PrinterGrande extends Printer {

    private SerialPort _serialPort;
    private int _longitudLinea = 32;

    public PrinterGrande(SerialPort serialPort) {
        cashDrawerActivated = true;
        _serialPort = serialPort;
    }

    //CONSTANTES DE SETEOS
    public static final int CMD_LINE_FEED = 0x0a;
    public static final int CMD_RESET_PRINT = 0x40;
    public static final int CMD_FORMAT = 0x1b;
    public static final int CMD_FORMAT_WIDTH = 0x56;//0x55;
    public static final int CMD_FORMAT_HEIGHT = 0x55;//0x56;
    public static final int CMD_FORMAT_ALIGNMENT = 0x6c;//0x61;
    public static final int CMD_FORMAT_ALIGNMENT_LEFT = 0;//0x48;
    public static final int CMD_FORMAT_ALIGNMENT_CENTER = 3;//0x49;
    public static final int CMD_FORMAT_ALIGNMENT_RIGHT = 0x50;
    public static final int CMD_FORMAT_ALIGNMENT_CANCEL = 0;//por ahora el cancel es el left 0x51;
    public static final int CMD_BARCODE = 0x1d;
    public static final int CMD_BARCODE_PRINT = 0x6b;
    public static final int CMD_BARCODE_EAN8 = 3;
    public static final int CMD_BARCODE_EAN13 = 2;
    public static final int[] CLEAR_BUFFER = {0x10, 0x14, 0x08, 0x01,0x03,0x20,0x01,0x06,0x02,0x08};

    public static final String ERROR_SIN_TINTA = "no ink";
    public static final String ERROR_TEMPERATURA = "over heat";
    public static final String ERROR_SIN_PAPEL = "no paper";

    public void setDefaultFormat() {
        _serialPort.write(0x1d, 0x4f, 0x01, 0x0a, 0x1b, 0x21, 0x00);
    }

    public void cancelCurrentFormat() {
        _serialPort.write(CMD_FORMAT, CMD_FORMAT_HEIGHT, 1);
        _serialPort.write(CMD_FORMAT, CMD_FORMAT_WIDTH, 1);
        _serialPort.write(CMD_FORMAT, CMD_FORMAT_ALIGNMENT, CMD_FORMAT_ALIGNMENT_CANCEL);

        /*buffer.add((byte)CMD_FORMAT);
        buffer.add((byte)CMD_FORMAT_HEIGHT);
        buffer.add((byte)1);
        buffer.add((byte)CMD_FORMAT);
        buffer.add((byte)CMD_FORMAT_WIDTH);
        buffer.add((byte)1);
        buffer.add((byte)CMD_FORMAT);
        buffer.add((byte)CMD_FORMAT_ALIGNMENT);
        buffer.add((byte)CMD_FORMAT_ALIGNMENT_CANCEL);*/


        current_width = 1;
        delay();


    }

    public void lineFeed() {
        _serialPort.write(CMD_LINE_FEED);

        //buffer.add((byte)CMD_LINE_FEED);

        delay();
    }

    //retorna null
    public static String EsError(String respuesta) {
        if (respuesta.equals(ERROR_SIN_TINTA))
            return "ATENCION: IMPRESORA SIN TINTA";
        if (respuesta.equals(ERROR_TEMPERATURA))
            return "ATENCION: IMPRESORA CON TEMPERATURA ELEVADA";
        if (respuesta.equals(ERROR_SIN_PAPEL))
            return "ATENCION: IMPRESORA SIN PAPEL";

        return null;
    }

    //imprimimos el buffer
    List<Byte> buffer = new ArrayList<Byte>();

    public Boolean initialize() {
        _serialPort.write(CMD_FORMAT, CMD_RESET_PRINT);

        //buffer.add((byte)CMD_FORMAT);
        //buffer.add((byte)CMD_RESET_PRINT);

        if (printerCallback != null) {
            //si estamos usando el nuevo callback => definimos callback al serial port
            if(printerCallback instanceof PrinterCallback2){
                _serialPort.setSerialPortCallback(new SerialPort.SerialPortCallback() {
                    @Override
                    public void onSerialPortError() {
                        ((PrinterCallback2)printerCallback).onPrinterError();
                    }
                });
            }

            printerCallback.onPrinterReady();
        }

        return true;
    }

    private String clearChars(String input){
        String output = "";

        output = input.replace("á", "a")
                .replace("à", "a")
                .replace("é", "e")
                .replace("è", "e")
                .replace("í", "i")
                .replace("ì", "i")
                .replace("ó", "o")
                .replace("ò", "o")
                .replace("ú", "u")
                .replace("ù", "u")
                .replace("Á", "A")
                .replace("À", "A")
                .replace("É", "E")
                .replace("È", "E")
                .replace("Í", "I")
                .replace("Ì", "I")
                .replace("Ó", "O")
                .replace("Ò", "O")
                .replace("Ú", "U")
                .replace("Ù", "U")
                .replace("Ñ", "N")
                .replace("ñ", "n");

        return output;
    }

    public void print(String linea) {

        linea = clearChars(linea);

        _serialPort.write(linea);

        /*try {
            byte[] linea_b = linea.getBytes("GB2312");
            for(int i = 0; i < linea_b.length ; i++ ) {
                buffer.add(linea_b[i]);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }*/

        delay();
    }

    public void printLine(String text) {
        print(text);
        lineFeed();
    }

    private void delay(){
        try {
            if(delayActivated && toogleDelay)
                Thread.sleep(100);

            toogleDelay = !toogleDelay;

        } catch (InterruptedException e) {
        }
    }

    @Override
    public void printBarCode(BARCODE_TYPE type, String input) {
/*
        int barcode_type = type == BARCODE_TYPE.EAN8 ? CMD_BARCODE_EAN8 : CMD_BARCODE_EAN13;

        //_serialPort.write(CMD_BARCODE, 0x48,0, CMD_BARCODE, 0x68,80, CMD_BARCODE, CMD_BARCODE_PRINT, barcode_type, 3,3,3,3,3,3,3,3,3,3,3,3);//0x07,0x06,0x02,0x02,0x03,0x00,0x00,0x08,0x02,0x09,0x06,0x04 );

        _serialPort.write(CMD_BARCODE, 0x68,60,CMD_BARCODE, CMD_BARCODE_PRINT, barcode_type, 1,1,1,1,1,1,1,1,1,1,1,1);//0x07,0x06,0x02,0x02,0x03,0x00,0x00,0x08,0x02,0x09,0x06,0x04 );

        //_serialPort.write(CMD_BARCODE, 0x68,100, CMD_BARCODE, CMD_BARCODE_PRINT, barcode_type, 2,2,2,2,2,2,2,2,2,2,2,2);//0x07,0x06,0x02,0x02,0x03,0x00,0x00,0x08,0x02,0x09,0x06,0x04 );

        //_serialPort.write(CMD_BARCODE, 0x68,100, CMD_BARCODE, CMD_BARCODE_PRINT, barcode_type, 4,4,4,4,4,4,4,4,4,4,4,4);//0x07,0x06,0x02,0x02,0x03,0x00,0x00,0x08,0x02,0x09,0x06,0x04 );

        //_serialPort.write(CMD_BARCODE, 0x68,100, CMD_BARCODE, CMD_BARCODE_PRINT, barcode_type, 5,5,5,5,5,5,5,5,5,5,5,5);//0x07,0x06,0x02,0x02,0x03,0x00,0x00,0x08,0x02,0x09,0x06,0x04 );

        //_serialPort.write(CMD_BARCODE, 0x68,100, CMD_BARCODE, CMD_BARCODE_PRINT, CMD_BARCODE_EAN8, 0,4,2,1,0,0,0,9);
*/
    }

    @Override
    public void printOpposite(String text1, String text2) {
        int longText = 0;

        longText += text1.length();

        longText += text2.length();

        /*for(String text : texts){
            longText += text.length();
        }*/

        int longPadding = get_longitudLinea() - longText;

        String padding = new String(new char[longPadding]).replace("\0", " ");

        print(text1 + padding + text2);
    }

    int current_width = 1;
    public void format(int alto, int ancho, ALINEACION alineacion) {
        //cancelCurrentFormat();

        _serialPort.write(CMD_FORMAT, CMD_FORMAT_HEIGHT, alto);
        _serialPort.write(CMD_FORMAT, CMD_FORMAT_WIDTH, ancho);

        /*buffer.add((byte)CMD_FORMAT);
        buffer.add((byte)CMD_FORMAT_HEIGHT);
        buffer.add((byte)alto);
        buffer.add((byte)CMD_FORMAT_HEIGHT);
        buffer.add((byte)CMD_FORMAT_WIDTH);
        buffer.add((byte)ancho);*/

        //enviamos la alineacion
        switch (alineacion) {
            case CENTER: {
                _serialPort.write(CMD_FORMAT, CMD_FORMAT_ALIGNMENT, CMD_FORMAT_ALIGNMENT_CENTER);
                /*buffer.add((byte)CMD_FORMAT);
                buffer.add((byte)CMD_FORMAT_ALIGNMENT);
                buffer.add((byte)CMD_FORMAT_ALIGNMENT_CENTER);*/
            }
            break;
            case LEFT: {
                _serialPort.write(CMD_FORMAT, CMD_FORMAT_ALIGNMENT, CMD_FORMAT_ALIGNMENT_LEFT);
                /*buffer.add((byte)CMD_FORMAT);
                buffer.add((byte)CMD_FORMAT_ALIGNMENT);
                buffer.add((byte)CMD_FORMAT_ALIGNMENT_LEFT);*/
            }
            break;
            case RIGHT: {
                _serialPort.write(CMD_FORMAT, CMD_FORMAT_ALIGNMENT, CMD_FORMAT_ALIGNMENT_RIGHT);
                /*buffer.add((byte)CMD_FORMAT);
                buffer.add((byte)CMD_FORMAT_ALIGNMENT);
                buffer.add((byte)CMD_FORMAT_ALIGNMENT_RIGHT);*/
            }
            break;
            case NORMAL: {
                _serialPort.write(CMD_FORMAT, CMD_FORMAT_ALIGNMENT, CMD_FORMAT_ALIGNMENT_CANCEL);
                /*buffer.add((byte)CMD_FORMAT);
                buffer.add((byte)CMD_FORMAT_ALIGNMENT);
                buffer.add((byte)CMD_FORMAT_ALIGNMENT_CANCEL);*/
            }
            break;
        }

        current_width = ancho;

        delay();
    }

    public void end() {
        try {
            delayActivated = false;
            toogleDelay = false;

            //Byte[] bufferArr = new Byte[buffer.size()];

            //_serialPort.write(buffer.toArray(bufferArr));

            _serialPort.write(CLEAR_BUFFER);

            _serialPort.closeSerialPort();

        } catch (Exception ex) {
            Log.e("printer", "Error;" + ex.getMessage());
        }

    }

    @Override
    public int get_longitudLinea() {
        return _longitudLinea / current_width;
    }


    public void test() {
        _serialPort.write("IMPRESION DE TEST");
        _serialPort.write(CMD_LINE_FEED);
        _serialPort.write("-------------------------");
        for (int i = 0; i < 5; i++) {
            lineFeed();
        }

    }

    @Override
    public void printBitmap(Bitmap bitmap) {
        int[] command = {0x1B, 0x4B, 0x80, 0x01};

        try {

            //PWMControl.PrinterEnable(1);

            //_serialPort.resetPort();
            //Thread.sleep(2000); //esperamos 2seg en la mitad

            int width = bitmap.getWidth();
            int height = bitmap.getHeight();

            //byte xL = (byte) (width % 256);
            //byte xH = (byte) (width / 256);
            int n1 = width & 0xFF;
            byte n2 = (byte) ((width >> 8) & 0xFF);

            command[2] = n1;
            command[3] = n2;


            // Adjust to adapt to integer multiples of printer
            int adjustedHeight = (height + 7) / 8 * 8; // 8 Multiples, one time print height is 8
            // Cycle of image pixel print pictures
            // Cycle height, one time print 8 pixel height
            int loops = adjustedHeight / 8;

            for (int i = 0; i < loops; i++) {
                /*if (i == loops / 2)
                    _serialPort.resetPort();//Thread.sleep(2000); //esperamos 2seg en la mitad
                */
                byte[] data = new byte[width];

                _serialPort.write(true, command);
                Log.d("printBitmap", "init line");

                //fill line with zeros
                for (int x = 0; x < width; x++)
                    data[x] = 0;
                for (int k = 0; k < 8; k++) {
                    // Loop width
                    for (int x = 0; x < width; x++) {
                        int y = (i * 8) + k;
                        // insure it is in image area
                        if (y < height) {
                            int pixel = bitmap.getPixel(x, y);
                            if (Color.red(pixel) == 0 || Color.green(pixel) == 0 || Color.blue(pixel) == 0) {
                                // High on the left, so the use of 128 right move
                                data[x] += (byte) (128 >> (y % 8));
                            }
                        }
                    }
                }

                _serialPort.write(true, data);
                Log.d("printBitmap", "end line");

                //delay();
            }

            Log.d("printBitmap", "end");


        } catch (Exception ex) {
            Log.e("PrinterError", ex.getMessage());
        }

    }

    @Override
    public void openCashDrawer() {
        HdxUtil.SetV12Power(1);//para abrirlo
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        HdxUtil.SetV12Power(0);//para apagarlo, sino no deja cerrar el cajon
    }

    @Override
    public void printQR(String qr) {

    }

    @Override
    public boolean isQrPrintSupported() {
        return false;
    }
}
