package android_serialport_api;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.pds.common.Config;
import com.pds.common.Formatos;
import com.pds.common.R;
import com.pds.common.model.VentaAbstract;
import com.pds.common.model.VentaDetalleAbstract;

import java.util.ArrayList;
import java.util.List;

import android_serialport_api.Printer.ALINEACION;

/**
 * Created by Hernan on 10/12/13.
 */
public class TicketBuilder extends Ticket_Builder {
    private String _header;
    private String _subheader;
    private String _comercio;
    private String _terminal;
    private String _direccion;
    private String _clavefiscal;
    private String _fecha;
    private String _hora;
    private List<LineaTicket> _lineas;
    private String _total;
    private String _medio_pago;
    private String _usuario;
    private String _serie;
    private String _leyenda_pie;
    private String _numero;
    private Bitmap _timbre;
    private String _vuelto;

    public String get_numero() {
        return _numero;
    }

    public void set_numero(String _numero) {
        this._numero = _numero;
    }

    public void set_total(String _total) {
        this._total = _total;
    }

    public void set_clavefiscal(String _clavefiscal) {
        this._clavefiscal = _clavefiscal;
    }

    public void set_comercio(String _comercio) {
        this._comercio = _comercio;
    }

    public void set_direccion(String _direccion) {
        this._direccion = _direccion;
    }

    public void set_fecha(String _fecha) {
        this._fecha = _fecha;
    }

    public void set_hora(String _hora) {
        this._hora = _hora;
    }

    public void set_leyenda_pie(String _leyenda_pie) {
        this._leyenda_pie = _leyenda_pie;
    }

    public void add_linea(LineaTicket _linea) {
        this._lineas.add(_linea);
    }

    public void set_medio_pago(String _medio_pago) {
        this._medio_pago = _medio_pago;
    }

    public void set_serie(String _serie) {
        this._serie = _serie;
    }

    public void set_usuario(String _usuario) {
        this._usuario = _usuario;
    }

    public void set_header(String _header) {
        this._header = _header;
    }

    public void set_subheader(String _subheader) {
        this._subheader = _subheader;
    }

    public List<LineaTicket> get_lineas() {
        return _lineas;
    }

    public String get_clavefiscal() {
        return _clavefiscal;
    }

    public String get_comercio() {
        return _comercio;
    }

    public String get_direccion() {
        return _direccion;
    }

    public String get_fecha() {
        return _fecha;
    }

    public String get_hora() {
        return _hora;
    }

    public String get_leyenda_pie() {
        return _leyenda_pie;
    }

    public String get_medio_pago() {
        return _medio_pago;
    }

    public String get_serie() {
        return _serie;
    }

    public String get_total() {
        return _total;
    }

    public String get_usuario() {
        return _usuario;
    }

    public String get_header() {
        return _header;
    }

    public String get_subheader() {
        return _subheader;
    }

    public Bitmap get_timbre() {
        return _timbre;
    }

    public void set_timbre(Bitmap _timbre) {
        this._timbre = _timbre;
    }

    public String get_vuelto() {
        return _vuelto;
    }

    public void set_vuelto(String _vuelto) {
        this._vuelto = _vuelto;
    }

    public void set_Venta(VentaAbstract venta) {
        this._numero = venta.getNroVenta();
        this._fecha = venta.getFecha();
        this._hora = venta.getHora();
        this._total = Formatos.FormateaDecimal(venta.getTotal(), Formatos.DecimalFormat_CH);
    }

    public void set_Detalles(List<VentaDetalleAbstract> detalles) {
        for (VentaDetalleAbstract detalle : detalles) {
            add_linea(new LineaTicket(
                    Formatos.FormateaDecimal(detalle.getCantidad(), Formatos.DecimalFormat_US),
                    detalle.getDescripcion(),
                    Formatos.FormateaDecimal(detalle.getPrecio(), Formatos.DecimalFormat_CH),
                    Formatos.FormateaDecimal(detalle.getTotal(), Formatos.DecimalFormat_CH)
            ));
        }
    }

    public TicketBuilder(Config config) {
        this();

        this._comercio = config.COMERCIO;
        this._direccion = config.DIRECCION;
        this._clavefiscal = config.CLAVEFISCAL;
        this._serie = config.IMEI();
        this._terminal = config.ID_COMERCIO;
    }

    public TicketBuilder() {
        this._header = "";
        this._subheader = "";
        this._comercio = "";
        this._direccion = "";
        this._clavefiscal = "";
        this._fecha = "";
        this._hora = "";
        this._numero = "";
        this._lineas = new ArrayList<LineaTicket>();
        this._total = "";
        this._medio_pago = "";
        this._usuario = "";
        this._serie = "";
        this._leyenda_pie = "";
        this._vuelto = "";
    }


    @Override
    public void Print(Printer _printer) {

        String leyenda_no_fiscal = "** SIN VALIDEZ FISCAL **";

        _printer.lineFeed();
        _printer.lineFeed();

            /*if(_printer instanceof PrinterPOWA && _timbre != null) {
                _printer.printBitmap(_timbre);
                _printer.lineFeed();
            }*/

        _printer.format(2, 1, ALINEACION.CENTER);
        _printer.printLine(leyenda_no_fiscal);

        //si tiene header
        if (!this._header.equals("")) {

            _printer.format(1, 1, ALINEACION.CENTER);
            _printer.printLine(this._header);
            if (!this._subheader.equals("")) {
                _printer.printLine(this._subheader);
            }
            _printer.cancelCurrentFormat();
            _printer.sendSeparadorHorizontal();
        }

        //nombre comercio
        _printer.format(2, 1, ALINEACION.CENTER);
        _printer.printLine(this._comercio);
        _printer.lineFeed();

        //direccion
        _printer.cancelCurrentFormat();
        _printer.printLine(this._direccion);

        //clave fiscal
        _printer.printLine("RUT: " + this._clavefiscal);
        _printer.sendSeparadorHorizontal();

        //fecha y hora
        /*
        mSerialPort.setCurrentFormat(1, 1, SerialPort.CMD_FORMAT_ALIGNMENT_LEFT);
        mSerialPort.sendString("Fecha");
        mSerialPort.setCurrentFormat(1, 1, SerialPort.CMD_FORMAT_ALIGNMENT_RIGHT);
        mSerialPort.sendString("Hora");
        mSerialPort.sendLineFeed();
        */
        _printer.format(1, 1, ALINEACION.LEFT);
        _printer.printOpposite("Terminal:  ", this._terminal);
        _printer.printOpposite("Nro.Ticket:", this._numero);
        //_printer.print(this._fecha + "              " + this._hora); //fecha ocupa 10, hora ocupa 8, restan 14
        _printer.printOpposite(this._fecha, this._hora);
        _printer.sendSeparadorHorizontal();

        //items
        _printer.format(1, 1, ALINEACION.LEFT);

        for (int i = 0; i < this._lineas.size(); i++) {

            if (i % 3 == 0) {
                //pausamos cada 5
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //Log.d("pdv", String.valueOf(i));
            }

            LineaTicket linea = this._lineas.get(i);

            /*String cantidad = "    " + linea.get_cantidad(); //hacemos un padding con 4 espacios (izquierda)
            cantidad = cantidad.substring(cantidad.length() - 4);

            String descripcion = linea.get_descripcion() + "              "; //hacemos un padding de 14 (derecha)
            descripcion = descripcion.substring(0, 14);

            String subtotal = "          " + linea.get_subtotal() ; //hacemos un padding de 10
            subtotal = subtotal.substring(subtotal.length() - 10);

            // 4 + 3 + 14 + 1 + 10 = 32
            _printer.print(cantidad + " x " + descripcion + " " + subtotal); //completamos 32*/

            String cantidad = "    " + linea.get_cantidad(); //hacemos un padding con 4 espacios (izquierda)
            cantidad = cantidad.substring(cantidad.length() - 4);

            int k = _printer.get_longitudLinea() - 18; //4 + 3 (" x ") + 10 + 1 (" ")

            String descripcion = linea.get_descripcion().replace("\r\n", " ").replace("\n", " ").trim() + _printer.getPadding(k); //hacemos un padding para cortar el nombre del producto en caso de excederse
            descripcion = descripcion.substring(0, k);

            String subtotal = "          " + linea.get_subtotal(); //hacemos un padding de 10
            subtotal = subtotal.substring(subtotal.length() - 10);

            _printer.printOpposite(cantidad + " x " + descripcion, subtotal);


        }

        //total
        _printer.lineFeed();
        _printer.format(2, 1, ALINEACION.CENTER);
        _printer.printLine(this._total);

        _printer.cancelCurrentFormat();
        _printer.sendSeparadorHorizontal();

        //pie
        if (!this._medio_pago.equals("")) {
            _printer.printLine(this._medio_pago);
        }

        if (!this._vuelto.equals("")) {
            _printer.printLine(this._vuelto);
        }

        if (!this._usuario.equals("")) {
            _printer.printLine(this._usuario);
        }

        //_printer.printLine(this._serie);
        _printer.lineFeed();

        _printer.format(2, 1, ALINEACION.CENTER);
        _printer.printLine(leyenda_no_fiscal);
        _printer.printLine(this._leyenda_pie);

        _printer.cancelCurrentFormat();

        _printer.footer();

    }

}