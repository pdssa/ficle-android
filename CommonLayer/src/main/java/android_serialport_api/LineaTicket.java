package android_serialport_api;

/**
 * Created by Hernan on 10/12/13.
 */
public class LineaTicket {
    private String _cantidad;
    private String _descripcion;
    private String _precio;
    private String _subtotal;


    public LineaTicket(String cantidad,String descripcion,String precio,String subtotal ){
        this._cantidad = cantidad;
        this._descripcion=descripcion;
        this._precio=precio;
        this._subtotal = subtotal;

    }

    public void set_precio(String _precio) {
        this._precio = _precio;
    }

    public void set_descripcion(String _descripcion) {
        this._descripcion = _descripcion;
    }

    public void set_cantidad(String _cantidad) {
        this._cantidad = _cantidad;
    }

    public String get_descripcion() {
        return _descripcion;
    }

    public String get_cantidad() {
        return _cantidad;
    }

    public String get_precio() {
        return _precio;
    }

    public String get_subtotal() {
        return _subtotal;
    }

    public void set_subtotal(String _subtotal) {
        this._subtotal = _subtotal;
    }

}