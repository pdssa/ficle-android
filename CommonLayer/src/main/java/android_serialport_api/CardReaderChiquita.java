package android_serialport_api;

import com.ctrl.gpio.Ioctl;

/**
 * Created by Hernan on 21/06/2014.
 */
public class CardReaderChiquita extends CardReader {
    private SerialPort _serialPort;
    private OnReadSerialPortDataListener onReadSerialPortDataListener;
    private Boolean _read;

    public CardReaderChiquita(SerialPort serialPort) {
        _serialPort = serialPort;
    }

    public void setOnReadDataListener(OnReadSerialPortDataListener _onReadSerialPortDataListener) {
        this.onReadSerialPortDataListener = _onReadSerialPortDataListener;
    }

    public String MSR_shangDian() {
        try {
            //上电
            Ioctl.activate(14, 1);
            //Thread.sleep(100);
            //切换串口
            Ioctl.convertMagcard();

            //Thread.sleep(100);
            //发送上电指令
            byte[] shangdian = {0x68, 0x02, 0x01, 0x00, 0x00, 0x6B, 0x16};
            _serialPort.write(shangdian);

            if (_serialPort.getInputStream() == null) {
                return "power_on_error";
            }
            int cout = _serialPort.getInputStream().available();
            byte[] buffer = new byte[cout];

            int size = _serialPort.getInputStream().read(buffer);

            if (size == 0) {
                return "power_on_error";
            }

            byte result = buffer[4];
            if (result == 0x00) {
                return "power_on_success";
            } else {
                return "power_on_failure";
            }
        } catch (Exception e) {

        }
        return "power_on_success";
    }

    public String MSR_xiaDian() {
        try {
            byte[] xiadian = {0x68, 0x02, 0x01, 0x00, 0x01, 0x6C, 0x16};
            _serialPort.write(xiadian);
            //Thread.sleep(50);
            if (_serialPort.getInputStream() == null) {
                return "power_off_error";
            }
            int cout = _serialPort.getInputStream().available();
            byte[] buffer = new byte[cout];
            int size = _serialPort.getInputStream().read(buffer);
            //Log.i("info", "byte == "+bytesToHexString1234(buffer));
            if (size == 0) {
                return "power_off_error";
            }
            byte result = buffer[4];
            if (result == 0x00) {
                return "power_off_success";
            } else {
                return "pwer_off_failure";
            }
        } catch (Exception e) {
            //Log.i("info", " e == "+e.getMessage().toString());
        }
        try {
            //Thread.sleep(50);
            Ioctl.activate(14, 0);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return "power_off_error";
    }

    public void initialize() {
        MSR_shangDian();
    }

    public void read() {
        _read = true;

        ReadThread _thread = new ReadThread();
        _thread.start();
    }

    private class ReadThread extends Thread {
        public void run() {
            doRead();
        }
    }

    private void doRead() {
        while (_read) {

            int size;

            if (_serialPort.getInputStream() == null) {
                return;
            }

            int cout;
            try {
                cout = _serialPort.getInputStream().available();
                byte[] buffer1 = new byte[cout];
                if (buffer1.length >= 16) {
                    cout = 0;
                    Thread.sleep(800);
                    cout = _serialPort.getInputStream().available();
                    buffer1 = new byte[cout];
                } else {
                    Thread.sleep(50);
                }
                size = _serialPort.getInputStream().read(buffer1);

                if (size > 0) {
//					Log.i("info", "buffer1 ===  "+bytesToHexString(buffer1));
                    SerialPortData data = new SerialPortData(buffer1, size);
                    if (onReadSerialPortDataListener != null) {
                        onReadSerialPortDataListener.onReadSerialPortData(data);
                    }
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
    }

    public void end() {

        try {

            _read = false;

            MSR_xiaDian();
            Ioctl.activate(14, 0);

            _serialPort.closeSerialPort();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
