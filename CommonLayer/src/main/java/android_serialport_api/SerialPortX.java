package android_serialport_api;

/**
 * Created by Hernan on 01/12/13.
 */

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import android.util.Log;

public class SerialPortX {

    private static final String TAG = "SerialPort";

    /*
     * Do not remove or rename the field mFd: it is used by native method close();
     */
    private FileDescriptor mFd;
    private FileInputStream mFileInputStream;
    private FileOutputStream mFileOutputStream;

    public static final int CMD_LINE_FEED = 0x0a;
    public static final int CMD_RESET_PRINT = 0x40;
    public static final int CMD_FORMAT = 0x1b;
    public static final int CMD_FORMAT_WIDTH = 0x56;//0x55;
    public static final int CMD_FORMAT_HEIGHT = 0x55;//0x56;
    public static final int CMD_FORMAT_ALIGNMENT = 0x6c;//0x61;
    public static final int CMD_FORMAT_ALIGNMENT_LEFT = 0;//0x48;
    public static final int CMD_FORMAT_ALIGNMENT_CENTER = 3;//0x49;
    public static final int CMD_FORMAT_ALIGNMENT_RIGHT = 0x50;
    public static final int CMD_FORMAT_ALIGNMENT_CANCEL = 0;//por ahora el cancel es el left 0x51;

    public static final String ERROR_SIN_TINTA = "no ink";
    public static final String ERROR_TEMPERATURA = "over heat";
    public static final String ERROR_SIN_PAPEL = "no paper";

    private List<Integer> comandosEnviados = new ArrayList<Integer>();

    //retorna null
    public static String EsError(String respuesta) {
        if (respuesta.equals(ERROR_SIN_TINTA))
            return "ATENCION: IMPRESORA SIN TINTA";
        if (respuesta.equals(ERROR_TEMPERATURA))
            return "ATENCION: IMPRESORA CON TEMPERATURA ELEVADA";
        if (respuesta.equals(ERROR_SIN_PAPEL))
            return "ATENCION: IMPRESORA SIN PAPEL";

        return null;
    }

    public SerialPortX() throws Exception {

        //String path = ;//sp.getString("DEVICE", "");
        //int baudrate = 115200;//Integer.decode(sp.getString("BAUDRATE", "-1"));

		/* Open the serial port */
        this(new File("/dev/ttyS0"), 115200, 0);
    }

    public SerialPortX(File device, int baudrate, int flags) throws SecurityException, IOException, Exception {
        try {
        /* Check access permission */
            if (!device.canRead() || !device.canWrite()) {
                try {
                /* Missing read/write permission, trying to chmod the file */
                    Process su;
                    su = Runtime.getRuntime().exec("/system/bin/su");
                    String cmd = "chmod 666 " + device.getAbsolutePath() + "\n"
                            + "exit\n";
                    su.getOutputStream().write(cmd.getBytes());
                    if ((su.waitFor() != 0) || !device.canRead()
                            || !device.canWrite()) {
                        throw new SecurityException();
                    }

                    comandosEnviados = new ArrayList<Integer>();

                } catch (Exception e) {
                    e.printStackTrace();
                    throw new SecurityException();
                }
            }

            mFd = open(device.getAbsolutePath(), baudrate, flags);
            if (mFd == null) {
                Log.e(TAG, "native open returns null");
                throw new IOException();
            }
            mFileInputStream = new FileInputStream(mFd);
            mFileOutputStream = new FileOutputStream(mFd);
        } catch (Exception ex) {
            throw ex;
        }
    }

    // Getters and setters
    public InputStream getInputStream() {
        return mFileInputStream;
    }

    public OutputStream getOutputStream() {
        return mFileOutputStream;
    }

    // Metodos de escritura
    public void sendCommand(int... command) {
        try {
            for (int i = 0; i < command.length; i++) {
                this.mFileOutputStream.write(command[i]);
                //Log.e(TAG,"command["+i+"] = "+Integer.toHexString(command[i]));

                this.comandosEnviados.add(command[i]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //esta version no guarda el comando para reenvio
    public void reSendCommand(int... command) {
        try {
            for (int i = 0; i < command.length; i++) {
                this.mFileOutputStream.write(command[i]);
                //Log.e(TAG,"command["+i+"] = "+Integer.toHexString(command[i]));

                //this.comandosEnviados.add(command[i]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void endTicket() {
        for (int i = 0; i < 5; i++) {
            this.sendLineFeed();
        }
    }

    public void setDefaultFormat() {
        sendCommand(0x1d, 0x4f, 0x01, 0x0a, 0x1b, 0x21, 0x00);
    }

    public void initializePrinter() {

        comandosEnviados = new ArrayList<Integer>();

        sendCommand(CMD_FORMAT, CMD_RESET_PRINT);
    }

    public void cancelCurrentFormat() {
        sendCommand(CMD_FORMAT, CMD_FORMAT_HEIGHT, 1);
        sendCommand(CMD_FORMAT, CMD_FORMAT_WIDTH, 1);
        sendCommand(CMD_FORMAT, CMD_FORMAT_ALIGNMENT, CMD_FORMAT_ALIGNMENT_CANCEL);
    }

    public void setCurrentFormat(int height, int width, int alignment) {
        cancelCurrentFormat();

        sendCommand(CMD_FORMAT, CMD_FORMAT_HEIGHT, height);
        sendCommand(CMD_FORMAT, CMD_FORMAT_WIDTH, width);
        sendCommand(CMD_FORMAT, CMD_FORMAT_ALIGNMENT, alignment);
    }

    public void sendSeparadorHorizontal() {
        sendString("--------------------------------");//32 caracteres en la linea
        sendLineFeed();
    }

    public void sendString(String string) {

        //String string = null;
        //String string = UnicodeToGBK(tString);
        try {
            for (int i = 0; i < string.length(); i++) {
                //Log.e(TAG, "111.i="+i+":"+(string.charAt(i)));
                //Log.e(TAG, "222.(int)i="+i+":"+Integer.toHexString(string.charAt(i)));
                //Log.e(TAG, "333.(char)i="+i+":"+(char)(string.charAt(i)));
                Log.e(TAG, "sendString," + i + " = " + Integer.toHexString(string.charAt(i)));
                this.mFileOutputStream.write((int) string.charAt(i));
                this.comandosEnviados.add((int) string.charAt(i));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendLineFeed() {
        sendCommand(CMD_LINE_FEED);
    }

    public void rePrint() {
        if (this.comandosEnviados.size() > 0) {

            for (int comando : comandosEnviados) {
                reSendCommand(comando);
            }

        }
    }

    private void sendCharacterDemo() {
        sendString("×Ö·û´®²âÊÔ£º1234567890£±£²£³£´£µ£¶£·");
        sendCommand(0x0a);
        sendString("ÐÐ¼ä¾à²âÊÔ±¾ÐÐÓëÏÂÐÐ¼ä¾àÎª200µã");
        sendCommand(0x1b, 0x33, 0xc8, 0x0a);
        sendString("ÐÐ¼ä¾à²âÊÔ±¾ÐÐÓëÏÂÐÐ¼ä¾àÎª100µã");
        sendCommand(0x1b, 0x33, 0x64, 0x0a);
        sendString("ÐÐ¼ä¾à²âÊÔ±¾ÐÐÓëÏÂÐÐ¼ä¾àÎª50µã");
        sendCommand(0x1b, 0x33, 0x32, 0x0a);
        sendString("ÐÐ¼ä¾à²âÊÔ±¾ÐÐÓëÏÂÐÐ¼ä¾àÎª30µã");
        sendCommand(0x1b, 0x33, 0x14, 0x0a);
        sendString("ÐÐ¼ä¾à²âÊÔ±¾ÐÐÓëÏÂÐÐ¼ä¾àÎª30µã");
        sendCommand(0x0a, 0x1b, 0x0e);
        sendString("±¶¿íÃüÁî±¶¿íÃüÁî123456ABCabc");
        sendCommand(0x0a, 0x1b, 0x14);
        sendString("È¡Ïû±¶¿íÃüÁî123456ABCabc");
        sendCommand(0x0a, 0x1b, 0x21, 0x30);
        sendString("±¶¸ßÃüÁî123456ABCabc");
        sendCommand(0x0a, 0x1b, 0x21, 0x00);
        sendCommand(0x1d, 0x4d, 0x0f);
        sendString("²âÊÔ½áÊøÍùÏÂ×ßÖ½50µãÐÐ");
        sendCommand(0x1b, 0x4a, 0xc8);
    }

    public static String UnicodeToGBK(String s) {
        try {
            String newstring = null;
            //Log.e(TAG, "->s : "+s);
            newstring = new String(s.getBytes("GBK"), "ISO-8859-1");
            //Log.e(TAG, "->GBKToUnicode : "+newstring);
            return newstring;
        } catch (UnsupportedEncodingException e) {
            return s;
        }
    }


    public void closeSerialPort() throws Exception {
        mFileInputStream.close();
        mFileOutputStream.close();
        this.close();
    }

    // JNI
    private native static FileDescriptor open(String path, int baudrate, int flags);

    public native void close();

    static {
        System.loadLibrary("serial_port");
    }


}
