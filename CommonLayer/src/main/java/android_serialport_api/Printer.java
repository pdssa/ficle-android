package android_serialport_api;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.pds.common.util.ConvertUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import hdx.HdxUtil;

/**
 * Created by Hernan on 19/06/2014.
 */
public abstract class Printer {

    private int _linesEndFeed = 5;

    public boolean delayActivated = false;
    public boolean toogleDelay = true;
    protected boolean cashDrawerActivated;

    public enum Printer_Model {CHICA, GRANDE, TP210}

    private static PrinterPOWA printerPOWA = null;

    public static Printer getInstance(String modelo, Context context) throws Exception {

        if (modelo.equals("JP762A")) {
            return new PrinterChiquita(new SerialPort("/dev/ttyS3", 115200, 0));
        }
        if (modelo.equals("TP210")) {
            return new PrinterTP210(new SerialPort("/dev/ttyS0", 115200, 0));
        }
        if(modelo.equals("POWA")){
            if(printerPOWA == null ) //guardamos la instancia para no perderla en cada inicializacion
                printerPOWA = new PrinterPOWA(context);
            else
                printerPOWA.setContext(context);

            return printerPOWA;
        }

        if (modelo.equals("BANANA")) {
            return new PrinterGrande(new SerialPort("/dev/ttyS0", 115200, 0));///mnt/sdcard/download/print_fm.txt
        }
        if (modelo.equals("BANANA2")) {

            HdxUtil.SwitchSerialFunction(HdxUtil.SERIAL_FUNCTION_PRINTER);
            HdxUtil.SetPrinterPower(1);

            Thread.sleep(500);

            return new PrinterGrande(new SerialPort("/dev/ttyS1", 115200, 0));
        }
        if (modelo.equals("BANANA3")) {

            HdxUtil.SwitchSerialFunction(HdxUtil.SERIAL_FUNCTION_PRINTER);
            HdxUtil.SetPrinterPower(1);

            Thread.sleep(500);

            return new PrinterBPOS4(new SerialPort("/dev/ttyS1", 115200, 0));
        }

        if(modelo.equals("BPOS3")){
            return new PrinterBPOS3();
        }
        else {
            return new PrinterGrande(new SerialPort("/dev/ttyS0", 115200, 0));
        }

    }

    public static Printer getInstance(String modelo) throws Exception {

        if (modelo.equals("JP762A")) {
            return new PrinterChiquita(new SerialPort("/dev/ttyS3", 115200, 0));
        }
        if (modelo.equals("TP210")) {
            return new PrinterTP210(new SerialPort("/dev/ttyS0", 115200, 0));
        }
        if(modelo.equals("POWA")){
            return new PrinterPOWA(null);
        }

        if (modelo.equals("BANANA")) {
            return new PrinterGrande(new SerialPort("/dev/ttyS0", 115200, 0));///mnt/sdcard/download/print_fm.txt
        }
        if (modelo.equals("BANANA2")) {

            HdxUtil.SwitchSerialFunction(HdxUtil.SERIAL_FUNCTION_PRINTER);
            HdxUtil.SetPrinterPower(1);

            Thread.sleep(500);//wait ready

            return new PrinterGrande(new SerialPort("/dev/ttyS1", 115200, 0));
        }
        if (modelo.equals("BANANA3")) {

            HdxUtil.SwitchSerialFunction(HdxUtil.SERIAL_FUNCTION_PRINTER);
            HdxUtil.SetPrinterPower(1);

            Thread.sleep(500);//wait ready

            return new PrinterBPOS4(new SerialPort("/dev/ttyS1", 115200, 0));
        }
        if(modelo.equals("BPOS3")){
            return new PrinterBPOS3();
        }
        else {
            return new PrinterGrande(new SerialPort("/dev/ttyS0", 115200, 0));
        }

    }

    /*public static Printer getInstance() throws Exception {

        Printer_Model tipo = Printer_Model.TP210;

        if (tipo == Printer_Model.CHICA) {
            return new PrinterChiquita(new SerialPort("/dev/ttyS3", 115200, 0));
        }
        if (tipo == Printer_Model.TP210) {
            return new PrinterTP210(new SerialPort("/dev/ttyS0", 115200, 0));
        } else {
            return new PrinterGrande(new SerialPort("/dev/ttyS0", 115200, 0));
        }

    }*/

    public void footer() {
        //enviamos lineas al pie para el corte del ticket
        for (int i = 0; i < _linesEndFeed; i++) {
            lineFeed();
        }
    }

    public void sendSeparadorHorizontal(boolean cancel_current_format) {
        sendSeparadorHorizontal('-', cancel_current_format);
    }
    public void sendSeparadorHorizontal() {
        sendSeparadorHorizontal(true);
    }
    public void sendSeparadorHorizontal(char caracter) {
        sendSeparadorHorizontal(caracter, true);
    }
    public void sendSeparadorHorizontal(char caracter, boolean cancel_current_format) {
        if(cancel_current_format)
            cancelCurrentFormat();
        print(new String(new char[get_longitudLinea()]).replace("\0", String.valueOf(caracter)));
    }
    public void sendSeparadorHorizontalMasLineFeed() {
        cancelCurrentFormat();
        printLine(new String(new char[get_longitudLinea()]).replace("\0", "-"));
    }


    public enum ALINEACION {LEFT, CENTER, RIGHT, NORMAL}

    protected String alignText(ALINEACION align_mode, String text, int longLine){
        //cortamos el texto por si excede el max de la linea
        text = text.substring(0, Math.min(text.length(), longLine));

        int longText = text.length();

        if(align_mode == ALINEACION.CENTER){
            //centramos el texto
            int longPadding = (longLine - longText) / 2;

            String padding = getPadding(longPadding);

            return padding + text;
        }

        if(align_mode == ALINEACION.RIGHT ){
            //alineamos a derecha
            int longPadding = longLine - longText;

            String padding = getPadding(longPadding);

            return padding + text;
        }

        return text;
    }

    public String getPadding(int longPadding){
        return getPadding(longPadding, " ");
    }

    public String getPadding(int longPadding, String _char){
        return new String(new char[longPadding]).replace("\0", _char);
    }

    //public enum FORMATO{}

    public abstract Boolean initialize();

    public abstract void print(String linea);

    public abstract void printLine(String linea);

    //public abstract void printOpposite(String... texts);

    public abstract void printOpposite(String text1, String text2);

    public abstract void format(int alto, int ancho, ALINEACION alineacion);

    public abstract void cancelCurrentFormat();

    public abstract void lineFeed();

    public abstract void end();

    public abstract void test();

    public abstract int get_longitudLinea();

    public enum BARCODE_TYPE{
        EAN8,
        EAN13
    }

    public abstract void printBarCode(BARCODE_TYPE type, String input);

    public abstract void printBitmap(Bitmap bmp);

    public static byte[] decodeBitmap(Bitmap bmp){
        int bmpWidth = bmp.getWidth();
        int bmpHeight = bmp.getHeight();

        List<String> list = new ArrayList<String>(); //binaryString list
        StringBuffer sb;

        // 姣忚瀛楄妭鏁�闄や互8锛屼笉瓒宠ˉ0)
        int bitLen = bmpWidth / 8;
        int zeroCount = bmpWidth % 8;
        // 姣忚闇�琛ュ厖鐨�
        String zeroStr = "";
        if (zeroCount > 0) {
            bitLen = bmpWidth / 8 + 1;
            for (int i = 0; i < (8 - zeroCount); i++) {
                zeroStr = zeroStr + "0";
            }
        }
        // 閫愪釜璇诲彇鍍忕礌棰滆壊锛屽皢闈炵櫧鑹叉敼涓洪粦鑹�
        for (int i = 0; i < bmpHeight; i++) {
            sb = new StringBuffer();
            for (int j = 0; j < bmpWidth; j++) {
                int color = bmp.getPixel(j, i); // 鑾峰緱Bitmap 鍥剧墖涓瘡涓�釜鐐圭殑color棰滆壊鍊�
                //棰滆壊鍊肩殑R G B
                int r = (color >> 16) & 0xff;
                int g = (color >> 8) & 0xff;
                int b = color & 0xff;

                // if color close to white锛宐it='0', else bit='1'
                if (r > 160 && g > 160 && b > 160)
                    sb.append("0");
                else
                    sb.append("1");
            }
            // 姣忎竴琛岀粨鏉熸椂锛岃ˉ鍏呭墿浣欑殑0
            if (zeroCount > 0) {
                sb.append(zeroStr);
            }
            list.add(sb.toString());
        }
        // binaryStr姣�浣嶈皟鐢ㄤ竴娆¤浆鎹㈡柟娉曪紝鍐嶆嫾鍚�
        List<String> bmpHexList = ConvertUtil.binaryListToHexStringList(list);
        String commandHexString = "1B4B8001";//0x1B,0x4B,0x80,0x01
        // 瀹藉害鎸囦护
        String widthHexString = Integer
                .toHexString(bmpWidth % 8 == 0 ? bmpWidth / 8
                        : (bmpWidth / 8 + 1));
        if (widthHexString.length() > 2) {
            Log.e("decodeBitmap error", "瀹藉害瓒呭嚭 width is too large");
            return null;
        } else if (widthHexString.length() == 1) {
            widthHexString = "0" + widthHexString;
        }
        widthHexString = widthHexString + "00";

        // 楂樺害鎸囦护
        String heightHexString = Integer.toHexString(bmpHeight);
        if (heightHexString.length() > 2) {
            Log.e("decodeBitmap error", "楂樺害瓒呭嚭 height is too large");
            return null;
        } else if (heightHexString.length() == 1) {
            heightHexString = "0" + heightHexString;
        }
        heightHexString = heightHexString + "00";

        List<String> commandList = new ArrayList<String>();
        commandList.add(commandHexString+widthHexString+heightHexString);
        commandList.addAll(bmpHexList);

        return ConvertUtil.hexList2Byte(commandList);
    }

    public PrinterCallback printerCallback;

    public interface PrinterCallback{
        void onPrinterReady();
        //void onPrinterOutOfPaper();
        //void onPrinterError(String error);
    }

    public interface PrinterCallback2 extends PrinterCallback{
        void onPrinterError();
    }

    protected void _end(){
        printerPOWA = null;
    }

    public abstract void openCashDrawer();

    public boolean isCashDrawerActivated(){
        return cashDrawerActivated;
    }

    public abstract void printQR(String qr);

    public abstract boolean isQrPrintSupported();
}
