package android_serialport_api;

import android_serialport_api.Printer.ALINEACION;

/**
 * Created by Hernan on 06/01/14.
 */
public class TicketRecargaBuilder {
    //encabezado
    private String _header;
    private String _titulo1;
    private String _titulo2;
    //cuerpo 1
    private String _producto;
    private String _nroTelefono;
    private String _montoRecarga;
    private String _codAutoriz;
    //cuerpo 2
    private String _razonSocialComercio;
    private String _nroComercio;
    private String _idTransaccion;
    private String _idControl;
    private String _idTerminal;
    private String _fechaHora;
    //cuerpo 3
    private String _nuevoSaldo;
    private String _validezRecarga;
    private String _bonosAcumulados;
    private String _descAcumulados;
    //cuerpo 4
    private String _mensajeLinea1;
    private String _mensajeLinea2;
    //pie
    private String _leyendaPie;

    public String get_header() {
        return _header;
    }

    public void set_header(String _header) {
        this._header = _header;
    }

    public String get_titulo1() {
        return _titulo1;
    }

    public void set_titulo1(String _titulo1) {
        this._titulo1 = _titulo1;
    }

    public String get_titulo2() {
        return _titulo2;
    }

    public void set_titulo2(String _titulo2) {
        this._titulo2 = _titulo2;
    }

    public String get_producto() {
        return _producto;
    }

    public void set_producto(String _producto) {
        this._producto = _producto;
    }

    public String get_nroTelefono() {
        return _nroTelefono;
    }

    public void set_nroTelefono(String _nroTelefono) {
        this._nroTelefono = _nroTelefono;
    }

    public String get_montoRecarga() {
        return _montoRecarga;
    }

    public void set_montoRecarga(String _montoRecarga) {
        this._montoRecarga = _montoRecarga;
    }

    public String get_codAutoriz() {
        return _codAutoriz;
    }

    public void set_codAutoriz(String _codAutoriz) {
        this._codAutoriz = _codAutoriz;
    }

    public String get_razonSocialComercio() {
        return _razonSocialComercio;
    }

    public void set_razonSocialComercio(String _razonSocialComercio) {
        this._razonSocialComercio = _razonSocialComercio;
    }

    public String get_nroComercio() {
        return _nroComercio;
    }

    public void set_nroComercio(String _nroComercio) {
        this._nroComercio = _nroComercio;
    }

    public String get_idTransaccion() {
        return _idTransaccion;
    }

    public void set_idTransaccion(String _idTransaccion) {
        this._idTransaccion = _idTransaccion;
    }

    public String get_idControl() {
        return _idControl;
    }

    public void set_idControl(String _idControl) {
        this._idControl = _idControl;
    }

    public String get_idTerminal() {
        return _idTerminal;
    }

    public void set_idTerminal(String _idTerminal) {
        this._idTerminal = _idTerminal;
    }

    public String get_fechaHora() {
        return _fechaHora;
    }

    public void set_fechaHora(String _fechaHora) {
        this._fechaHora = _fechaHora;
    }

    public String get_nuevoSaldo() {
        return _nuevoSaldo;
    }

    public void set_nuevoSaldo(String _nuevoSaldo) {
        this._nuevoSaldo = _nuevoSaldo;
    }

    public String get_validezRecarga() {
        return _validezRecarga;
    }

    public void set_validezRecarga(String _validezRecarga) {
        this._validezRecarga = _validezRecarga;
    }

    public String get_bonosAcumulados() {
        return _bonosAcumulados;
    }

    public void set_bonosAcumulados(String _bonosAcumulados) {
        this._bonosAcumulados = _bonosAcumulados;
    }

    public String get_descAcumulados() {
        return _descAcumulados;
    }

    public void set_descAcumulados(String _descAcumulados) {
        this._descAcumulados = _descAcumulados;
    }

    public String get_mensajeLinea1() {
        return _mensajeLinea1;
    }

    public void set_mensajeLinea1(String _mensajeLinea1) {
        this._mensajeLinea1 = _mensajeLinea1;
    }

    public String get_mensajeLinea2() {
        return _mensajeLinea2;
    }

    public void set_mensajeLinea2(String _mensajeLinea2) {
        this._mensajeLinea2 = _mensajeLinea2;
    }

    public String get_leyendaPie() {
        return _leyendaPie;
    }

    public void set_leyendaPie(String _leyendaPie) {
        this._leyendaPie = _leyendaPie;
    }

    public TicketRecargaBuilder() {
        this._header = "";
        this._titulo1 = "";
        this._titulo2 = "";
        this._producto = "";
        this._nroTelefono = "";
        this._montoRecarga = "";
        this._codAutoriz = "";
        this._razonSocialComercio = "";
        this._nroComercio = "";
        this._idTransaccion = "";
        this._idControl = "";
        this._idTerminal = "";
        this._fechaHora = "";
        this._nuevoSaldo = "";
        this._validezRecarga = "";
        this._bonosAcumulados = "";
        this._descAcumulados = "";
        this._mensajeLinea1 = "";
        this._mensajeLinea2 = "";
        this._leyendaPie = "";
    }


    public void PrintTicket(Printer _printer) {

        _printer.lineFeed();
        _printer.lineFeed();

        //si tiene header
        if (!this._header.equals("")) {

            _printer.format(1, 1, ALINEACION.CENTER);
            _printer.printLine(this._header);
            _printer.cancelCurrentFormat();
            _printer.sendSeparadorHorizontal();
        }

        //COMPROBANTE RECARGA
        _printer.format(2, 2, ALINEACION.CENTER);
        _printer.printLine(this._titulo1);
        _printer.cancelCurrentFormat();

        _printer.format(2, 2, ALINEACION.CENTER);
        _printer.printLine(this._titulo2);
        _printer.cancelCurrentFormat();
        _printer.sendSeparadorHorizontal();

        //-*-CUERPO 1-*-

        //producto
        _printer.format(2, 1, ALINEACION.CENTER);
        _printer.printLine(this._producto);
        _printer.cancelCurrentFormat();
        _printer.lineFeed();

        //nroTelefono
        _printer.printLine("Nro. Telefono : " + this._nroTelefono);

        //montoRecarga
        _printer.printLine("Monto Recarga : $ " + this._montoRecarga);
        _printer.lineFeed();
        _printer.cancelCurrentFormat();

        //codAutor
        _printer.printLine("Cod. Aut. : " + this._codAutoriz);
        _printer.sendSeparadorHorizontal();

        //-*-CUERPO 2-*-
        //razonSocialComercio
        _printer.format(2, 1, ALINEACION.CENTER);
        _printer.printLine(this._razonSocialComercio);
        _printer.cancelCurrentFormat();
        //nroComercio
        _printer.printLine("Nro comercio    : " + this._nroComercio);
        //id transaccion
        _printer.printLine("ID de control   : " + this._idControl);
        //id control
        _printer.printLine("ID Transaccion  : " + this._idTransaccion);
        //id terminal
        _printer.printLine("ID Terminal     : " + this._idTerminal);
        //fecha y hora
        _printer.printLine("Fecha y hora    : " + this._fechaHora);
        _printer.sendSeparadorHorizontal();

        //-*-CUERPO 3-*-

        //nuevoSaldo
        _printer.printLine("Nuevo saldo     : $ " + this._nuevoSaldo.replaceFirst("^0+(?!$)", "")); //remueve ceros iniciales
        //validez recarga
        _printer.printLine("Validez recarga : " + this._validezRecarga);
        //bonosAcumulados
        _printer.printLine("Bonos acumulados: " + this._bonosAcumulados);
        //descAcumulados
        _printer.printLine("Desc. acumulados: " + this._descAcumulados);
        _printer.sendSeparadorHorizontal();

        //-*-CUERPO 4-*-
        //mensajeLinea1
        _printer.printLine(this._mensajeLinea1);
        //mensajeLinea2
        _printer.format(1, 1, ALINEACION.CENTER);
        _printer.printLine(this._mensajeLinea2);
        _printer.lineFeed();

        //-*-PIE-*-

        _printer.format(1, 1, ALINEACION.CENTER);
        _printer.printLine(this._leyendaPie);
        _printer.cancelCurrentFormat();

        _printer.footer();

    }

}
