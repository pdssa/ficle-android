package android_serialport_api;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;

import com.telpo.tps550.api.TelpoException;
import com.telpo.tps550.api.printer.ThermalPrinter;


/**
 * Created by PDS S.A. on 17/01/2017
 */
public class PrinterBPOS3 extends Printer {

    private int _longitudLinea = 32;

    public PrinterBPOS3() {
        cashDrawerActivated = false;
    }


    public void cancelCurrentFormat() {

        try {
            ThermalPrinter.setAlgin(ThermalPrinter.ALGIN_LEFT);
            ThermalPrinter.enlargeFontSize(1, 1);

        } catch (TelpoException e) {
            Log.e("PrinterError", e.getMessage());
        }

        current_width = 1;
        delay();

    }

    public void lineFeed() {
        try {
            ThermalPrinter.addString(" \n");
        } catch (TelpoException e) {
            Log.e("PrinterError", e.getMessage());
        }

        delay();
    }

    public Boolean initialize() {

        try {
            ThermalPrinter.start();
            ThermalPrinter.reset();
            ThermalPrinter.clearString();
            ThermalPrinter.setHighlight(false);
            ThermalPrinter.setFontSize(2);//384 / (12 * 24) = 32 x line
            ThermalPrinter.setGray(10);

            if (printerCallback != null) {
                printerCallback.onPrinterReady();
            }
            return true;

        } catch (TelpoException ex) {
            Log.e("PrinterError", ex.getMessage());
        }

        return false;
    }

    public void print(String linea) {
        try {
            linea = clearChars(linea);

            ThermalPrinter.addString(linea);

            delay();
        } catch (TelpoException e) {
            Log.e("PrinterError", e.getMessage());
        }
    }

    public void printLine(String text) {
        print(text);
        lineFeed();
    }

    private void delay() {
        try {
            if (delayActivated && toogleDelay)
                Thread.sleep(100);

            toogleDelay = !toogleDelay;

        } catch (InterruptedException e) {
        }
    }

    @Override
    public void printBarCode(BARCODE_TYPE type, String input) {

    }

    @Override
    public void printOpposite(String text1, String text2) {
        int longText = 0;

        longText += text1.length();

        longText += text2.length();

        int longPadding = get_longitudLinea() - longText;

        String padding = new String(new char[longPadding]).replace("\0", " ");

        //print(text1 + padding + text2);
        printLine(text1 + padding + text2);
    }

    int current_width = 1;

    public void format(int alto, int ancho, ALINEACION alineacion) {
        try {
            //cancelCurrentFormat();

            ThermalPrinter.enlargeFontSize(ancho, alto);

            //enviamos la alineacion
            switch (alineacion) {
                case CENTER: {
                    ThermalPrinter.setAlgin(ThermalPrinter.ALGIN_MIDDLE);
                }
                break;
                case LEFT: {
                    ThermalPrinter.setAlgin(ThermalPrinter.ALGIN_LEFT);
                }
                break;
                case RIGHT: {
                    ThermalPrinter.setAlgin(ThermalPrinter.ALGIN_RIGHT);
                }
                break;
                case NORMAL: {
                    ThermalPrinter.setAlgin(ThermalPrinter.ALGIN_LEFT);
                }
                break;
            }

            current_width = ancho;

            delay();
        } catch (TelpoException e) {
            Log.e("PrinterError", e.getMessage());
        }

    }

    public void end() {
        try {
            delayActivated = false;
            toogleDelay = false;

            ThermalPrinter.printString();
            ThermalPrinter.clearString();

            ThermalPrinter.stop();

        } catch (Exception ex) {
            Log.e("printer", "Error;" + ex.getMessage());
        }

    }

    @Override
    public int get_longitudLinea() {
        return _longitudLinea / current_width;
    }


    public void test() {
        try {
            ThermalPrinter.addString("IMPRESION DE TEST\n");
            ThermalPrinter.addString("-------------------------");
            for (int i = 0; i < 5; i++) {
                lineFeed();
            }
            ThermalPrinter.printString();
            ThermalPrinter.clearString();
        } catch (TelpoException e) {
            Log.e("PrinterError", e.getMessage());
        }
    }

    @Override
    public void printQR(String qr) {

    }

    @Override
    public void printBitmap(Bitmap bitmap) {
        try {
            ThermalPrinter.printLogo(bitmap);
        } catch (Exception ex) {
            Log.e("PrinterError", ex.getMessage());
        }
    }

    @Override
    public void openCashDrawer() {

    }

    @Override
    public boolean isQrPrintSupported() {
        return false;
    }

    private String clearChars(String input) {
        String output = "";

        output = input.replace("á", "a")
                .replace("à", "a")
                .replace("é", "e")
                .replace("è", "e")
                .replace("í", "i")
                .replace("ì", "i")
                .replace("ó", "o")
                .replace("ò", "o")
                .replace("ú", "u")
                .replace("ù", "u")
                .replace("Á", "A")
                .replace("À", "A")
                .replace("É", "E")
                .replace("È", "E")
                .replace("Í", "I")
                .replace("Ì", "I")
                .replace("Ó", "O")
                .replace("Ò", "O")
                .replace("Ú", "U")
                .replace("Ù", "U")
                .replace("Ñ", "N")
                .replace("ñ", "n");

        return output;
    }
}
