package android_serialport_api;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.Toast;

//NEW SDK IMPORTS
//import com.mpowa.android.sdk.common.base.PowaEnums;
import com.mpowa.android.sdk.powapos.*;
import com.mpowa.android.sdk.powapos.common.base.PowaEnums;
import com.mpowa.android.sdk.powapos.core.PowaPOSEnums;
import com.mpowa.android.sdk.powapos.core.abstracts.PowaScanner;
import com.mpowa.android.sdk.powapos.core.callbacks.PowaPOSCallback;
import com.mpowa.android.sdk.powapos.core.dataobjects.PowaDeviceInfo;
import com.mpowa.android.sdk.powapos.drivers.s10.PowaS10Scanner;
import com.mpowa.android.sdk.powapos.drivers.tseries.PowaTSeries;

/*//OLD SDK IMPORTS
import com.mpowa.android.powapos.peripherals.PowaPOS;
import com.mpowa.android.powapos.peripherals.drivers.s10.PowaS10Scanner;
import com.mpowa.android.powapos.peripherals.drivers.tseries.PowaTSeries;
import com.mpowa.android.powapos.peripherals.platform.base.PowaPOSEnums;
import com.mpowa.android.powapos.peripherals.platform.base.PowaPeripheralCallback;
import com.mpowa.android.powapos.peripherals.platform.base.PowaScanner;*/

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Hernan on 26/11/2014.
 */
public class PrinterPOWA extends Printer {

    private int _longitudLinea = 48;
    //private PowaPOSSimulator powaPOS;
    private PowaPOS powaPOS;
    private Context context;

    private PowaTSeries powaTSeries;

    //CONSTANTES DE SETEOS
    public static final String CMD_LINE_FEED = "";

    //private List<String> buffer_to_print;

    /*private void toast(String message){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }*/

    private PowaPOSCallback powaPOSCallback;//NEW VERSION
    //private PowaPeripheralCallback powaPOSCallback;//OLD VERSION

    private boolean printerReady = false;

    public void setContext(Context context) {
        this.context = context;
    }

    public PrinterPOWA(final Context _context) throws Exception {

        cashDrawerActivated = true;

        setContext(_context);
        //toast("create peripheralCallback");
        powaPOSCallback = new PowaPOSCallback() {
            @Override
            public void onMCUInitialized(PowaPOSEnums.InitializedResult initializedResult) {
                //toast("onMCUInitialized()");
                //Toast.makeText(context, "onMCUInitialized()", Toast.LENGTH_SHORT).show();

                if (initializedResult.equals(PowaPOSEnums.InitializedResult.SUCCESSFUL)) {
                    //toast("onMCUInitialized() SUCCESSFUL");

                    /*
                    //loginFragment.showOption("mcu");
                    powaPOS.printText("TEST LINEA 1");
                    powaPOS.printText("TEST LINEA 2");
                    powaPOS.printText("TEST LINEA 3");
                    powaPOS.printText("TEST LINEA 4");

                    powaPOS.printText("DESPUES DE VACIO");
                    powaPOS.printText("------------------------------------------------");
                    powaPOS.printText("DESPUES DE SEPARADOR");
                    */
                    if (printerCallback != null) {

                        printerReady = true;

                        //toast("printerCallback() -> START");
                        powaPOS.startReceipt();
                        printerCallback.onPrinterReady();
                        powaPOS.printReceipt();
                        //toast("printerCallback() -> END");
                    } else {
                        //toast("printerCallback() -> NULL");
                    }

                } else {
                    //toast("onMCUInitialized() FAILURE");

                    Toast.makeText(context, "The app could not connect to MCU.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onMCUConnectionStateChanged(PowaEnums.ConnectionState connectionState) {
                //Toast.makeText(context, "onMCUConnectionStateChanged()", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onMCUFirmwareUpdateStarted() {
                //toast("onMCUFirmwareUpdateStarted()");
            }

            @Override
            public void onMCUFirmwareUpdateProgress(int i) {
                //toast("onMCUFirmwareUpdateProgress()");

            }

            @Override
            public void onMCUFirmwareUpdateFinished() {
                //toast("onMCUFirmwareUpdateFinished()");

            }

            @Override
            public void onMCUBootloaderUpdateStarted() {
                //toast("onMCUBootloaderUpdateStarted()");

            }

            @Override
            public void onMCUBootloaderUpdateProgress(int i) {
                //toast("onMCUBootloaderUpdateProgress()");
            }

            @Override
            public void onMCUBootloaderUpdateFinished() {
                //toast("onMCUBootloaderUpdateFinished");
            }

            @Override
            public void onMCUBootloaderUpdateFailed(PowaPOSEnums.BootloaderUpdateError bootloaderUpdateError) {
                //toast("onMCUBootloaderUpdateFailed");

            }

            @Override
            public void onMCUSystemConfiguration(Map<String, String> stringStringMap) {
                //toast("onMCUSystemConfiguration");

            }

            @Override
            public void onUSBDeviceAttached(PowaPOSEnums.PowaUSBCOMPort powaUSBCOMPort) {
                //toast("onUSBDeviceAttached");

            }

            @Override
            public void onUSBDeviceDetached(PowaPOSEnums.PowaUSBCOMPort powaUSBCOMPort) {
                //toast("onUSBDeviceDetached");

            }

            @Override
            public void onCashDrawerStatus(PowaPOSEnums.CashDrawerStatus cashDrawerStatus) {
                //toast("onCashDrawerStatus");
                //Toast.makeText(context, cashDrawerStatus., Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onRotationSensorStatus(PowaPOSEnums.RotationSensorStatus rotationSensorStatus) {
                //toast("onRotationSensorStatus");

            }

            @Override
            public void onScannerInitialized(PowaPOSEnums.InitializedResult initializedResult) {
                //toast("onScannerInitialized");

            }

            @Override
            public void onScannerRead(String s) {
                if (_scannerCallback != null)
                    _scannerCallback.onScannerRead(s);

            }

            /*@Override
            public void onPrintJobCompleted(PowaPOSEnums.PrintJobResult printJobResult) {

            }*/

            @Override
            public void onPrintJobResult(PowaPOSEnums.PrintJobResult printJobResult) {
                //toast("onPrintJobResult");
            }

            @Override
            public void onPrinterOutOfPaper() {
                //toast("onPrinterOutOfPaper");
            }

            @Override
            public void onUSBReceivedData(PowaPOSEnums.PowaUSBCOMPort powaUSBCOMPort, byte[] bytes) {
                //toast("onUSBReceivedData");

            }
        };

        //toast("create PowaPOS");
        powaPOS = new PowaPOS(context, powaPOSCallback);
    }

    @Override
    public Boolean initialize() {
        try {

            if (powaTSeries == null) {
                //toast("create PowaTSeries");
                powaTSeries = new PowaTSeries(context, true);

                //toast("addPeripheral");

                powaPOS.addPeripheral(powaTSeries);
            }


            setDefaultFormat();

            //si ya está iniciado, solo verificamos
            if (isConnected()) {
                powaPOS.startReceipt();
                printerCallback.onPrinterReady();
                powaPOS.printReceipt();
            }


        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private ScannerCallback _scannerCallback;

    public interface ScannerCallback {
        void onScannerRead(String text);
    }

    public void addScanner(ScannerCallback scannerCallback) {
        try {

            _scannerCallback = scannerCallback;

            PowaScanner scanner = new PowaS10Scanner(context);

            powaPOS.addPeripheral(scanner);

            if (powaPOS.getAvailableScanners().size() > 0) {
                powaPOS.selectScanner(powaPOS.getAvailableScanners().get(0));

                //Thread.sleep(2000); //dormimos 2 seg para esperar a que el scanner esté disponible
            }


        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void setDefaultFormat() {
        currentAlignMode = ALINEACION.NORMAL;
        currentMagnificationMode = PowaPOSEnums.PowaPrintFormat.MAGNIFICATION_NONE;
    }

    @Override
    public void print(String linea) {

        switch (currentAlignMode) {
            case CENTER: {
                String text = alignText(ALINEACION.CENTER, linea, get_longitudLinea());

                powaPOS.getPrinter().printText(text);
            }
            break;
            case RIGHT: {
                String text = alignText(ALINEACION.RIGHT, linea, get_longitudLinea());

                powaPOS.getPrinter().printText(text);
            }
            break;
            default: {
                powaPOS.getPrinter().printText(linea);
            }
            break;
        }

        //toast("print()");
    }

    @Override
    public void printLine(String text) {
        print(text);
    }

    @Override
    public void printOpposite(String text1, String text2) {
        int longText = 0;

        longText += text1.length();

        longText += text2.length();

        int longPadding = get_longitudLinea() - longText;

        String padding = new String(new char[longPadding]).replace("\0", " ");

        print(text1 + padding + text2);
    }

    private ALINEACION currentAlignMode;
    private PowaPOSEnums.PowaPrintFormat currentMagnificationMode;

    @Override
    public void format(int alto, int ancho, ALINEACION alineacion) {

        cancelCurrentFormat();

        if (alto == 1 && ancho == 1) {
            currentMagnificationMode = PowaPOSEnums.PowaPrintFormat.MAGNIFICATION_NONE;
        }
        if (alto == 2 || ancho == 2) {
            currentMagnificationMode = PowaPOSEnums.PowaPrintFormat.MAGNIFICATION_X2;
        }
        if (alto == 3 || ancho == 3) {
            currentMagnificationMode = PowaPOSEnums.PowaPrintFormat.MAGNIFICATION_X3;
        }
        if (alto == 4 || ancho == 4) {
            currentMagnificationMode = PowaPOSEnums.PowaPrintFormat.MAGNIFICATION_X4;
        }

        powaPOS.setFormat(currentMagnificationMode);

        currentAlignMode = alineacion;


    }

    @Override
    public void cancelCurrentFormat() {

        currentMagnificationMode = PowaPOSEnums.PowaPrintFormat.MAGNIFICATION_NONE;

        powaPOS.setFormat(currentMagnificationMode);

        currentAlignMode = ALINEACION.NORMAL;
    }


    @Override
    public void lineFeed() {
        powaPOS.getPrinter().printText(CMD_LINE_FEED);
    }

    @Override
    public void end() {
        if (powaPOS != null) {
            powaPOS.dispose();
            powaPOS = null;
        }

        super._end();
    }

    @Override
    public void test() {

        powaPOS.getPrinter().printText("SDK: " + powaPOS.getPowaPOSSDKVersion());

        List<PowaDeviceInfo> info = powaPOS.getMCUInformation();
        for (PowaDeviceInfo deviceInfo : info) {
            powaPOS.getPrinter().printText("Device Type: " + deviceInfo.deviceType );
            powaPOS.getPrinter().printText("Firmware Version: " + deviceInfo.firmwareVersion );
            powaPOS.getPrinter().printText("Hardware Version: " + deviceInfo.hardwareVersion );
            powaPOS.getPrinter().printText("Serial Number: " + deviceInfo.serialNumber );
        }

        powaPOS.getPrinter().printText("------------------------------------------------");

        powaPOS.getPrinter().printText("IMPRESION DE TEST LINEA 1");
        powaPOS.getPrinter().printText("IMPRESION DE TEST LINEA 2");
        powaPOS.getPrinter().printText("IMPRESION DE TEST LINEA 3");
        powaPOS.getPrinter().printText("IMPRESION DE TEST LINEA 4");

        powaPOS.getPrinter().printText("------------------------------------------------");

        for (int i = 0; i < 5; i++) {
            lineFeed();
        }
    }

    @Override
    public int get_longitudLinea() {

        if (currentMagnificationMode == PowaPOSEnums.PowaPrintFormat.MAGNIFICATION_X2) {
            return _longitudLinea / 2;
        }

        if (currentMagnificationMode == PowaPOSEnums.PowaPrintFormat.MAGNIFICATION_X3) {
            return _longitudLinea / 3;
        }

        if (currentMagnificationMode == PowaPOSEnums.PowaPrintFormat.MAGNIFICATION_X4) {
            return _longitudLinea / 4;
        }

        return _longitudLinea;
    }

    @Override
    public void printBarCode(BARCODE_TYPE type, String input) {

    }

    @Override
    public void printBitmap(Bitmap bmp) {
        powaPOS.printImage(bmp);
    }

    @Override
    public void openCashDrawer() {
        //Toast.makeText(context, "open", Toast.LENGTH_SHORT).show();
        powaPOS.openCashDrawer();
    }

    private boolean isConnected() {
        if (powaTSeries != null && printerCallback != null) {
            return powaPOS.getMCU().getConnectionState().equals(PowaEnums.ConnectionState.CONNECTED);
        } else
            return false;
    }

    @Override
    public void printQR(String qr) {

    }

    @Override
    public boolean isQrPrintSupported() {
        return false;
    }
}
