package android_serialport_api;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import hdx.HdxUtil;


/**
 * Created by PDS S.A. on 16/12/2015.
 */
public class PrinterBPOS4 extends Printer {

    private SerialPort _serialPort;
    private int _longitudLinea = 32;

    public PrinterBPOS4(SerialPort serialPort) {
        cashDrawerActivated = true;
        _serialPort = serialPort;
    }

    //CONSTANTES DE SETEOS
    public static final int CMD_LINE_FEED = 0x0a;
    public static final int CMD_RESET_PRINT = 0x40;
    public static final int CMD_FORMAT = 0x1b;
    public static final int CMD_FORMAT_SIZE = 29;//0x1d;
    //public static final int CMD_FORMAT_WIDTH = 33;//0x21;
    //public static final int CMD_FORMAT_HEIGHT = 33;//0x21;
    public static final int CMD_FORMAT_CHAR_SIZE = 33;
    public static final int CMD_FORMAT_ALIGNMENT = 0x61;
    public static final int CMD_FORMAT_ALIGNMENT_LEFT = 0;//0x48;
    public static final int CMD_FORMAT_ALIGNMENT_CENTER = 1;//0x49;
    public static final int CMD_FORMAT_ALIGNMENT_RIGHT = 2;//0x50;
    public static final int CMD_FORMAT_ALIGNMENT_CANCEL = CMD_FORMAT_ALIGNMENT_LEFT;//por ahora el cancel es el left
    //public static final int CMD_BARCODE = 0x1d;
    //public static final int CMD_BARCODE_PRINT = 0x6b;
    //public static final int CMD_BARCODE_EAN8 = 3;
    //public static final int CMD_BARCODE_EAN13 = 2;

    public void cancelCurrentFormat() {
        _serialPort.write(CMD_FORMAT_SIZE, CMD_FORMAT_CHAR_SIZE, 0);
        _serialPort.write(CMD_FORMAT, CMD_FORMAT_ALIGNMENT, CMD_FORMAT_ALIGNMENT_CANCEL);

        current_width = 1;
        delay();

    }

    public void lineFeed() {
        _serialPort.write(CMD_LINE_FEED);

        delay();
    }

    public Boolean initialize() {
        _serialPort.write(CMD_FORMAT, CMD_RESET_PRINT);

        if (printerCallback != null) {
            printerCallback.onPrinterReady();
        }
        return true;
    }

    public void print(String linea) {

        linea = clearChars(linea);

        _serialPort.write(linea);

        delay();
    }

    public void printLine(String text) {
        print(text);
        lineFeed();
    }

    private void delay() {
        try {
            if (delayActivated && toogleDelay)
                Thread.sleep(100);

            toogleDelay = !toogleDelay;

        } catch (InterruptedException e) {
        }
    }

    @Override
    public void printBarCode(BARCODE_TYPE type, String input) {
/*
        int barcode_type = type == BARCODE_TYPE.EAN8 ? CMD_BARCODE_EAN8 : CMD_BARCODE_EAN13;

        //_serialPort.write(CMD_BARCODE, 0x48,0, CMD_BARCODE, 0x68,80, CMD_BARCODE, CMD_BARCODE_PRINT, barcode_type, 3,3,3,3,3,3,3,3,3,3,3,3);//0x07,0x06,0x02,0x02,0x03,0x00,0x00,0x08,0x02,0x09,0x06,0x04 );

        _serialPort.write(CMD_BARCODE, 0x68,60,CMD_BARCODE, CMD_BARCODE_PRINT, barcode_type, 1,1,1,1,1,1,1,1,1,1,1,1);//0x07,0x06,0x02,0x02,0x03,0x00,0x00,0x08,0x02,0x09,0x06,0x04 );

        //_serialPort.write(CMD_BARCODE, 0x68,100, CMD_BARCODE, CMD_BARCODE_PRINT, barcode_type, 2,2,2,2,2,2,2,2,2,2,2,2);//0x07,0x06,0x02,0x02,0x03,0x00,0x00,0x08,0x02,0x09,0x06,0x04 );

        //_serialPort.write(CMD_BARCODE, 0x68,100, CMD_BARCODE, CMD_BARCODE_PRINT, barcode_type, 4,4,4,4,4,4,4,4,4,4,4,4);//0x07,0x06,0x02,0x02,0x03,0x00,0x00,0x08,0x02,0x09,0x06,0x04 );

        //_serialPort.write(CMD_BARCODE, 0x68,100, CMD_BARCODE, CMD_BARCODE_PRINT, barcode_type, 5,5,5,5,5,5,5,5,5,5,5,5);//0x07,0x06,0x02,0x02,0x03,0x00,0x00,0x08,0x02,0x09,0x06,0x04 );

        //_serialPort.write(CMD_BARCODE, 0x68,100, CMD_BARCODE, CMD_BARCODE_PRINT, CMD_BARCODE_EAN8, 0,4,2,1,0,0,0,9);
*/
    }

    @Override
    public void printOpposite(String text1, String text2) {
        int longText = 0;

        longText += text1.length();

        longText += text2.length();

        int longPadding = get_longitudLinea() - longText;

        String padding = new String(new char[longPadding]).replace("\0", " ");

        //print(text1 + padding + text2);
        printLine(text1 + padding + text2);
    }

    int current_width = 1;

    public void format(int alto, int ancho, ALINEACION alineacion) {
        //cancelCurrentFormat();

        // Height: 0..7
        // Width: 0..7
        // Size: (H-1) + (W-1) * 16
        _serialPort.write(CMD_FORMAT_SIZE, CMD_FORMAT_CHAR_SIZE, (alto - 1) + (ancho - 1) * 16);

        //enviamos la alineacion
        switch (alineacion) {
            case CENTER: {
                _serialPort.write(CMD_FORMAT, CMD_FORMAT_ALIGNMENT, CMD_FORMAT_ALIGNMENT_CENTER);
            }
            break;
            case LEFT: {
                _serialPort.write(CMD_FORMAT, CMD_FORMAT_ALIGNMENT, CMD_FORMAT_ALIGNMENT_LEFT);
            }
            break;
            case RIGHT: {
                _serialPort.write(CMD_FORMAT, CMD_FORMAT_ALIGNMENT, CMD_FORMAT_ALIGNMENT_RIGHT);
            }
            break;
            case NORMAL: {
                _serialPort.write(CMD_FORMAT, CMD_FORMAT_ALIGNMENT, CMD_FORMAT_ALIGNMENT_CANCEL);
            }
            break;
        }

        current_width = ancho;

        delay();
    }

    public void end() {
        try {
            delayActivated = false;
            toogleDelay = false;

            _serialPort.closeSerialPort();

            //HdxUtil.SetPrinterPower(0);

        } catch (Exception ex) {
            Log.e("printer", "Error;" + ex.getMessage());
        }

    }

    @Override
    public int get_longitudLinea() {
        return _longitudLinea / current_width;
    }


    public void test() {
        _serialPort.write("IMPRESION DE TEST");
        _serialPort.write(CMD_LINE_FEED);
        _serialPort.write("-------------------------");
        for (int i = 0; i < 5; i++) {
            lineFeed();
        }

    }

    @Override
    public void printQR(String qr) {

        char[] chars = qr.toCharArray();
        int[] qrData = new int[chars.length];

        for(int i = 0; i< chars.length; i++){
            qrData[i] = (int)chars[i];
        }

        //****************try center code : tests*********************************
        //_serialPort.write(0x1b, 0x61, 0x01); //test 1 -> result: QR on the right
        //_serialPort.write(0x1b, 0x61, 0x1);  //test 2 -> result: QR on the right
        //_serialPort.write(0x1b, 0x61, 1);    //test 3 -> result: QR on the right
        //_serialPort.write(0x1b, 0x61, 0x48); //test 4 -> result: QR on the left
        //_serialPort.write(0x1b, 0x61, 0x49); //test 5 -> result: QR on the left
        //_serialPort.write(0x1b, 0x61, 0x50); //test 6 -> result: QR on the left
        //************************************************************************

        //enable barcode mode
        _serialPort.write(0x1d, 0x45, 0x43, 0x1);

        //init buffer qr data
        _serialPort.write(0x1d, 0x28, 0x6b, qrData.length + 3, 0x00, 0x31, 0x50, 0x30);

        //send qr data to the buffer
        _serialPort.write(qrData);

        //print barcode cache
        _serialPort.write(0x1d, 0x28, 0x6b, 0x03, 0x00, 0x31, 0x51, 0x30);

        //end barcode mode
        _serialPort.write(0x1d, 0x45, 0x43, 0x0);

        //line feed
        _serialPort.write(0x1b, 0x4a, 0x30);
    }

    @Override
    public void printBitmap(Bitmap bitmap) {

        //int[] command = {0x1D, 0x76, 0x30, 0x01};
        int startx = 0;//padding
        byte[] command = {0x1D, 0x76, 0x30, 0x30, 0x00, 0x00, 0x01, 0x00};

        try {

            //_serialPort.resetPort();
            //Thread.sleep(2000); //esperamos 2seg en la mitad

            int width = bitmap.getWidth() + startx;
            int height = bitmap.getHeight();

            if (width > 384)
                width = 384;

            int tmp = (width + 7) / 8;
            byte[] data = new byte[tmp];
            byte xL = (byte) (tmp % 256);
            byte xH = (byte) (tmp / 256);
            command[4] = xL;
            command[5] = xH;
            command[6] = (byte) (height % 256);
            command[7] = (byte) (height / 256);

            _serialPort.write(true, command);

            for (int i = 0; i < height; i++) {
                Log.d("printBitmap", "init line");

                for (int x = 0; x < tmp; x++)
                    data[x] = 0;
                for (int x = startx; x < width; x++) {
                    int pixel = bitmap.getPixel(x - startx, i);
                    if (Color.red(pixel) == 0 || Color.green(pixel) == 0 || Color.blue(pixel) == 0) {
                        data[x / 8] += 128 >> (x % 8);// (byte) (128 >> (y % 8));
                    }
                }

                _serialPort.write(true, data);
                Log.d("printBitmap", "end line");
                //delay();
            }

        } catch (Exception ex) {
            Log.e("PrinterError", ex.getMessage());
        }

    }

    @Override
    public void openCashDrawer() {
        HdxUtil.SetV12Power(1);//para abrirlo

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        HdxUtil.SetV12Power(0);//para apagarlo, sino no deja cerrar el cajon
    }

    @Override
    public boolean isQrPrintSupported() {
        return true;
    }

    private String clearChars(String input) {
        String output = "";

        output = input.replace("á", "a")
                .replace("à", "a")
                .replace("é", "e")
                .replace("è", "e")
                .replace("í", "i")
                .replace("ì", "i")
                .replace("ó", "o")
                .replace("ò", "o")
                .replace("ú", "u")
                .replace("ù", "u")
                .replace("Á", "A")
                .replace("À", "A")
                .replace("É", "E")
                .replace("È", "E")
                .replace("Í", "I")
                .replace("Ì", "I")
                .replace("Ó", "O")
                .replace("Ò", "O")
                .replace("Ú", "U")
                .replace("Ù", "U")
                .replace("Ñ", "N")
                .replace("ñ", "n");

        return output;
    }
}
