package android_serialport_api;


import android.graphics.Bitmap;

import java.io.File;

/**
 * Created by Hernan on 10/07/2014.
 */
public class PrinterTP210
        extends Printer {

    private int _longitudLinea = 32;

    @Override
    public int get_longitudLinea() {
        return _longitudLinea;
    }

    @Override
    public void printBarCode(BARCODE_TYPE type, String input) {

    }

    private SerialPort _serialPort;


    public PrinterTP210(SerialPort serialPort) {
        cashDrawerActivated = false;
        _serialPort = serialPort;
    }

    public static final byte HT = 0x9; // Horizontal tab
    public static final byte LF = 0x0A; // Printing and paper feeding
    public static final byte CR = 0x0D; // Enter
    public static final byte ESC = 0x1B;
    public static final byte DLE = 0x10;
    public static final byte GS = 0x1D;
    public static final byte FS = 0x1C;
    public static final byte STX = 0x02;
    public static final byte US = 0x1F;
    public static final byte CAN = 0x18;
    public static final byte CLR = 0x0C;
    public static final byte EOT = 0x04;
    public static final byte CMD_FORMAT_ALIGNMENT = 0x61; //'a'
    public static final byte CMD_FORMAT = 0x21;

    /* 榛樿棰滆壊瀛椾綋鎸囦护 */
    public static final byte[] ESC_FONT_COLOR_DEFAULT = new byte[]{ESC, 'r', 0x00};
    /* 鏍囧噯澶у皬 */
    public static final byte[] FS_FONT_ALIGN = new byte[]{FS, CMD_FORMAT, 1, ESC, CMD_FORMAT, 1};
    /* 鍙栨秷瀛椾綋鍔犵矖 */
    public static final byte[] ESC_CANCEL_BOLD = new byte[]{ESC, 0x45, 0};

    // 杩涚焊
    public static final byte[] ESC_ENTER = new byte[]{ESC, 0x4A, 0x40};
    public static final byte[] ENTER = new byte[]{CR, LF};

    // 鑷
    public static final byte[] PRINT_RESET = new byte[]{ 0x40, 0x1B };
    public static final byte[] PRINT_TEST = new byte[]{GS, 0x28, 0x41};
    public static final byte[] SET_LEFT = new byte[]{ESC, CMD_FORMAT_ALIGNMENT, 0x00};
    public static final byte[] SET_CENTER = new byte[]{ESC, CMD_FORMAT_ALIGNMENT, 0x01};
    public static final byte[] SET_RIGHT = new byte[]{ESC, CMD_FORMAT_ALIGNMENT, 0x02};

    //formatos
    public static final byte[] FORMAT_NORMAL = new byte[]{ESC, CMD_FORMAT, 0};
    public static final byte[] FORMAT_ITALIC = new byte[]{ESC, CMD_FORMAT, 1};
    public static final byte[] FORMAT_BOLD = new byte[]{ESC, CMD_FORMAT, 3};
    public static final byte[] FORMAT_ALTO = new byte[]{ESC, CMD_FORMAT, 16};
    public static final byte[] FORMAT_ANCHO = new byte[]{ESC, CMD_FORMAT, 32};
    public static final byte[] FORMAT_ANCHO_ALTO = new byte[]{ESC, CMD_FORMAT, 48};

    public static final byte[] UNICODE_TEXT = new byte[]{0x00, 0x50, 0x00,
            0x72, 0x00, 0x69, 0x00, 0x6E, 0x00, 0x74, 0x00, 0x20, 0x00, 0x20,
            0x00, 0x20, 0x00, 0x4D, 0x00, 0x65, 0x00, 0x73, 0x00, 0x73, 0x00,
            0x61, 0x00, 0x67, 0x00, 0x65};
    //public static final byte[] BOLD = new byte[]{ESC, 0x6D, 0x04};


    public void resetPrint() {

        _serialPort.write(ESC_FONT_COLOR_DEFAULT);
        _serialPort.write(FS_FONT_ALIGN);
        _serialPort.write(SET_LEFT);
        _serialPort.write(ESC_CANCEL_BOLD);
        _serialPort.write(LF);
    }

    public void lineFeed() {
        _serialPort.write(ENTER);
    }

    public Boolean initialize() {

        try {

            Boolean result = true;

            _serialPort.write(PRINT_RESET);

            return result;
        } catch (Exception e) {
            return false;
        }

    }

    public void print(String linea) {
        _serialPort.write(linea);
    }

    public void printLine(String text) {
        print(text);
        lineFeed();
    }

    @Override
    public void printOpposite(String text1, String text2) {    }


    public void format(int alto, int ancho, ALINEACION alineacion) {

        _serialPort.write(FORMAT_NORMAL);

        //si es los dos, enviamos el doble ancho y alto
        if (alto > 1 && alto > 1) {
            _serialPort.write(FORMAT_ANCHO_ALTO);
        } else {
            //DOBLE ALTO
            if (alto > 1) {
                _serialPort.write(FORMAT_ALTO);
            }

            //DOBLE ANCHO
            if (ancho > 1) {
                _serialPort.write(FORMAT_ANCHO);
            }
        }
        //enviamos la alineacion
        switch (alineacion) {
            case CENTER: {
                _serialPort.write(SET_CENTER);
            }
            break;
            case LEFT: {
                _serialPort.write(SET_LEFT);
            }
            break;
            case RIGHT: {
                _serialPort.write(SET_RIGHT);
            }
            break;
            case NORMAL: {
                _serialPort.write(SET_LEFT);
            }
            break;
        }
    }

    public void cancelCurrentFormat() {
        format(1, 1, ALINEACION.NORMAL);
    }


    public void test() {
        _serialPort.write(PRINT_TEST);
        _serialPort.write(ESC_ENTER);
        _serialPort.write(ESC_ENTER);
    }

    @Override
    public void printBitmap(Bitmap bmp) {

    }

    @Override
    public void openCashDrawer() {

    }

    @Override
    public boolean isQrPrintSupported() {
        return false;
    }

    public void end() {
        try {

            _serialPort.closeSerialPort();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void printQR(String qr) {

    }

}
