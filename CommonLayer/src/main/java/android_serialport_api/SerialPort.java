package android_serialport_api;

/**
 * Created by Hernan on 01/12/13.
 */

import android.util.Log;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class SerialPort {

    private static final String TAG = "SerialPort";

    public String _port;
    public int _baudrate;
    public int _flags;

    /*
     * Do not remove or rename the field mFd: it is used by native method close();
     */
    private FileDescriptor mFd;
    private FileInputStream mFileInputStream;
    private FileOutputStream mFileOutputStream;

    private ReadThread mReadThread;

    private int printer_status = 0;

    private SerialPortCallback serialPortCallback;

    public interface SerialPortCallback {
        void onSerialPortError();
    }

    public void setSerialPortCallback(SerialPortCallback serialPortCallback) {
        this.serialPortCallback = serialPortCallback;
    }

    public SerialPort() throws Exception {

        //String path = ;//sp.getString("DEVICE", "");
        //int baudrate = 115200;//Integer.decode(sp.getString("BAUDRATE", "-1"));

		/* Open the serial port */
        this("/dev/ttyS0", 115200, 0);
    }

    public SerialPort(String ttyPort, int baudrate, int flags) throws Exception {
        try {

            _port = ttyPort;
            _baudrate = baudrate;
            _flags = flags;

            startPort(ttyPort, baudrate, 0, ' ', 0, flags);

        } catch (Exception ex) {
            throw ex;
        }
    }

    public SerialPort(String ttyPort, int baudrate, int bits, char event, int stop, int flags) throws Exception {
        try {

            _port = ttyPort;
            _baudrate = baudrate;
            _flags = flags;

            startPort(ttyPort, baudrate, bits, event, stop, flags);

        } catch (Exception ex) {
            throw ex;
        }
    }

    private void startPort(String ttyPort, int baudrate, int bits, char event, int stop, int flags) throws Exception {
        try {

            File device = new File(ttyPort);
            /*if(!device.exists())
                device.createNewFile();*/
            /* Check access permission */
            if (!device.canRead() || !device.canWrite()) {
                try {
                    /* Missing read/write permission, trying to chmod the file */
                    Process su;
                    su = Runtime.getRuntime().exec("/system/bin/su");
                    String cmd = "chmod 777 " + device.getAbsolutePath() + "\n"
                            + "exit\n";
                    su.getOutputStream().write(cmd.getBytes());
                    if ((su.waitFor() != 0) || !device.canRead()
                            || !device.canWrite()) {
                        throw new SecurityException();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new SecurityException();
                }
            }

            mFd = open(device.getAbsolutePath(), baudrate, bits, event, stop, flags);
            if (mFd == null) {
                Log.e(TAG, "native open returns null");
                throw new IOException();
            }
            mFileInputStream = new FileInputStream(mFd);
            mFileOutputStream = new FileOutputStream(mFd);

			/* Create a receiving thread */
            mReadThread = new ReadThread();
            mReadThread.start();
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void resetPort() throws Exception {

        Thread.sleep(4000);//wait 3 sec

        closeSerialPort();

        startPort(_port, _baudrate, 0, ' ', 0, _flags);
    }

    // Getters and setters
    public InputStream getInputStream() {
        return mFileInputStream;
    }

    public OutputStream getOutputStream() {
        return mFileOutputStream;
    }

    class ReadThread extends Thread {

        @Override
        public void run() {
            super.run();
            while (!isInterrupted()) {
                int size;
                try {
                    sleep(100);

                    if (mFileInputStream == null) return;

                    int available = mFileInputStream.available();

                    if (available > 0) {
                        byte[] buffer = new byte[64];

                        size = mFileInputStream.read(buffer);
                        if (size > 0) {

                            String readString = new String(buffer, 0, size);

                            for (int i = 0; i < readString.length(); i++) {
                                Log.e("serial_port", " error: " + readString.charAt(i));
                                printer_status = (int) readString.charAt(i);
                                //readString += buffer[i];
                            }

                        }
                    }
                } catch (InterruptedException e) {
                    return;//sleep
                } catch (NullPointerException e) {
                    return;//if inputstream was null
                } catch (IOException e) {
                    return;//
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            }
        }
    }

    // Metodos de escritura
    public void write(int... command) {
        write(false, command);
    }

    public void write(boolean checkstatus, int... command) {
        try {

            if (mFileOutputStream == null)
                return;

            if (checkstatus) {
                Log.e(TAG, "printer_status=" + printer_status);
                int loops = 0;
                //printer_status = 5; //forced error
                while ((printer_status & 0x13) != 0) {
                    try {

                        if (loops > 20) {
                            if (serialPortCallback != null) {
                                serialPortCallback.onSerialPortError();
                                return;
                            } else
                                printer_status = 0;
                        }

                        Thread.sleep(50);
                        loops++;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Log.e(TAG, "printer_status=" + printer_status);
                }
            }

            for (int i = 0; i < command.length; i++) {
                this.mFileOutputStream.write(command[i]);
            }
            //this.mFileOutputStream.flush();//x
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void write(String string) {

        try {

            if (mFileOutputStream == null)
                return;

            /*
            for (int i = 0; i < string.length(); i++) {
                Log.e(TAG, "sendString," + i + " = " + Integer.toHexString(string.charAt(i)));
                this.mFileOutputStream.write((int) string.charAt(i));
            }
            */
            this.mFileOutputStream.write(string.getBytes("GB2312"));
            this.mFileOutputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void write(byte[] b) {
        write(false, b);
    }

    public void write(boolean checkstatus, byte[] b) {
        try {

            if (b == null)
                return;

            if (mFileOutputStream == null)
                return;

            if (checkstatus) {
                Log.e(TAG, "printer_status=" + printer_status);
                int loops = 0;
                while ((printer_status & 0x13) != 0) {
                    try {

                        if (loops > 20) {
                            if (serialPortCallback != null) {
                                serialPortCallback.onSerialPortError();
                                return;
                            } else
                                printer_status = 0;
                        }

                        Thread.sleep(50);
                        loops++;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Log.e(TAG, "printer_status=" + printer_status);
                }
            }

            for (int i = 0; i < b.length; i++) {

                if (i % 2000 == 0)
                    Thread.sleep(100);//wait 0.5 sec

                this.mFileOutputStream.write(b[i]);
            }

            //this.mFileOutputStream.flush(); //x

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void write(byte b) {
        try {
            if (mFileOutputStream != null) {
                this.mFileOutputStream.write(b);
                this.mFileOutputStream.flush(); // 1
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void closeSerialPort() throws Exception {
        //Thread.sleep(1000);
        if (mFileOutputStream != null) {
            mFileOutputStream.close();
            mFileOutputStream = null;
        }

        if (mFileInputStream != null) {
            mFileInputStream.close();
            mFileInputStream = null;
        }

        if (mReadThread != null) {
            mReadThread.interrupt();
            mReadThread = null;
        }

        if(mFd != null){
            //mFd.sync();
            this.close();
            mFd = null;
            System.gc();
        }
    }

    // JNI
    private native static FileDescriptor open(String path, int baudrate, int flags);

    private native static FileDescriptor open(String path, int baudrate, int bits, char event, int stop, int flags);

    public native void close();

    static {
        System.loadLibrary("serial_port");
    }


}
