package android_serialport_api;

/**
 * Created by Hernan on 21/06/2014.
 */
public abstract class CardReader {

    public static CardReader getInstance() throws Exception {
        return new CardReaderChiquita(new SerialPort("/dev/ttyS1", 115200, 8, 'N', 1, 0));
    }

    public abstract void initialize();

    public abstract void setOnReadDataListener(OnReadSerialPortDataListener _onReadSerialPortDataListener);

    public abstract void read();

    public abstract void end();

    public interface OnReadSerialPortDataListener {
        public void onReadSerialPortData(SerialPortData serialPortData);
    }

    public class SerialPortData {
        private byte[] dataByte;
        private int size;

        public SerialPortData(byte[] _dataByte, int _size) {
            this.setDataByte(_dataByte);
            this.setSize(_size);
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public byte[] getDataByte() {
            return dataByte;
        }

        public void setDataByte(byte[] dataByte) {
            this.dataByte = dataByte;
        }

        public String getCardNumber()
        {
            String str = new String(dataByte);
            if(str.contains("^")) {
                str = str.split("\\^")[0];
                str = str.replace("%B", "");
            }

            return str;
        }
    }
}
