package android_serialport_api;

import java.io.IOException;
import java.security.InvalidParameterException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;

import com.pds.common.R;


/**
 * Created by Hernan on 02/12/13.
 */
public abstract class SerialPortActivity extends Activity {

    protected SerialPortX mSerialPort;
    private ReadThread mReadThread;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {

            mSerialPort = new SerialPortX();

			/* Create a receiving thread */
            mReadThread = new ReadThread();
            mReadThread.start();

        } catch (SecurityException e) {
            DisplayError(R.string.error_security);
        } catch (IOException e) {
            DisplayError(R.string.error_unknown);
        } catch (InvalidParameterException e) {
            DisplayError(R.string.error_configuration);
        } catch (Exception e) {
            DisplayError(R.string.error_configuration);
        }
    }

    protected abstract void onDataReceived(final byte[] buffer, final int size);

    @Override
    protected void onDestroy() {
        try {
            if (mReadThread != null)
                mReadThread.interrupt();

            mSerialPort.closeSerialPort();

            super.onDestroy();
        } catch (IOException e) {
        } catch (Exception ex) {
            String error = ex.getMessage();
        }
    }

    private class ReadThread extends Thread {

        @Override
        public void run() {
            super.run();
            while (!isInterrupted()) {
                int size;
                try {
                    byte[] buffer = new byte[64];
                    if (mSerialPort.getInputStream() == null) return;

                    size = mSerialPort.getInputStream().read(buffer);

                    if (size > 0) {
                        onDataReceived(buffer, size);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                }
            }
        }
    }

    private void DisplayError(int resourceId) {
        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setTitle("Error");
        b.setMessage(resourceId);
        b.setPositiveButton("OK", new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                SerialPortActivity.this.finish();
            }
        });
        b.show();
    }

}