package com.pds.common;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.pds.common.db.AlicuotasTable;
import com.pds.common.db.CategoriaTable;
import com.pds.common.db.CierreTable;
import com.pds.common.db.ClienteTable;
import com.pds.common.db.ComboItemTable;
import com.pds.common.db.ComboTable;
import com.pds.common.db.CompraDetalleTable;
import com.pds.common.db.CompraTable;
import com.pds.common.db.ComprobanteDetalleTable;
import com.pds.common.db.ComprobanteTable;
import com.pds.common.db.CuentaTable;
import com.pds.common.db.DepartmentTable;
import com.pds.common.db.HistMovStockTable;
import com.pds.common.db.PedidoDetalleTable;
import com.pds.common.db.PedidoTable;
import com.pds.common.db.ProductProvidersTable;
import com.pds.common.db.ProductTable;
import com.pds.common.db.PromocionTable;
import com.pds.common.db.ProviderTable;
import com.pds.common.db.RangosFoliosTable;
import com.pds.common.db.SubdepartmentTable;
import com.pds.common.db.TaxTable;
import com.pds.common.db.TipoDocTable;
import com.pds.common.db.VentaAnuladaDetalleTable;
import com.pds.common.db.VentaAnuladaTable;
import com.pds.common.model.Cliente;
import com.pds.common.model.Compra;
import com.pds.common.model.Department;


/**
 * Created by Hernan on 25/10/13.
 */
public class DbHelper extends SQLiteOpenHelper {

    //database properties
    private static final String DATABASE_NAME = "pds_data.db";
    //TODO: siempre agregar rutina en onUpgrade para realizar los updates
    //TODO: siempre agregar TABLAS nuevas en onCreate
    private static final int DATABASE_VERSION = 7;  ///HZ 09-04-2019 SII_MERCHANT_ID
    private static final String TAG = DbHelper.class.getSimpleName();

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        SQLiteDatabase _db;

        //si fue abierta para consulta y no estaba creada, se abre en modo read y no permite la creacion
        //del modelo, entonces forzamos una apertura write
        if (db.isReadOnly())
            _db = this.getWritableDatabase();
        else
            _db = db;

        //-------------TABLAS Y VALORES POR DEFECTO-------------
        _db.execSQL(UsuarioHelper.USUARIOS_CREATE_SCRIPT);
        _db.execSQL(UsuarioHelper.USUARIO_ADMIN_CREATE_SCRIPT);
        _db.execSQL(ProductoHelper.PRODUCTO_CREATE_SCRIPT);
        _db.execSQL(VentasHelper.VENTA_CREATE_SCRIPT);
        _db.execSQL(VentaDetalleHelper.VENTA_DET_CREATE_SCRIPT);
        _db.execSQL(LogHelper.LOG_CREATE_SCRIPT);
        _db.execSQL(ConfigHelper.CONFIG_CREATE_SCRIPT);
        _db.execSQL(ConfigHelper.CONFIG_DEFAULT_CREATE_SCRIPT);
        _db.execSQL(CajaHelper.CAJA_CREATE_SCRIPT);
        _db.execSQL(HorarioHelper.HORARIO_CREATE_SCRIPT);
        _db.execSQL(AppHelper.APPS_CREATE_SCRIPT);
        _db.execSQL(AppHelper.APPS_INIT_ALTA_SCRIPT);
        DepartmentTable.onCreate(_db);
        SubdepartmentTable.onCreate(_db);
        ProviderTable.onCreate(_db);
        ProductTable.onCreate(_db);
        TaxTable.onCreate(_db);
        AlicuotasTable.onCreate(_db);
        _db.execSQL(ClienteTable.TABLE_CREATE);
        _db.execSQL(CuentaTable.TABLE_CREATE_SCRIPT);
        _db.execSQL(ComprobanteTable.COMPROBANTE_CREATE_SCRIPT);
        _db.execSQL(ComprobanteDetalleTable.COMPROBANTE_DET_CREATE_SCRIPT);
        _db.execSQL(ProductProvidersTable.CREATE_SCRIPT);
        _db.execSQL(CompraTable.COMPRA_CREATE_SCRIPT);
        _db.execSQL(CompraDetalleTable.COMPRA_DET_CREATE_SCRIPT);
        _db.execSQL(CierreTable.TABLE_CREATE);
        _db.execSQL(TipoDocTable.TABLE_CREATE);
        _db.execSQL(RangosFoliosTable.TABLE_CREATE);
        _db.execSQL(HistMovStockTable.TABLE_CREATE_SCRIPT);
        _db.execSQL(CategoriaTable.TABLE_CREATE);
        _db.execSQL(PedidoTable.PEDIDO_CREATE_SCRIPT);
        _db.execSQL(PedidoDetalleTable.PEDIDO_DET_CREATE_SCRIPT);
        _db.execSQL(VentaAnuladaTable.VENTA_ANULADA_CREATE_SCRIPT);
        _db.execSQL(VentaAnuladaDetalleTable.VENTA_ANULADA_DETALLE_CREATE_SCRIPT );
        _db.execSQL(ComboTable.TABLE_CREATE);
        _db.execSQL(ComboItemTable.TABLE_CREATE);
        _db.execSQL(PromocionTable.TABLE_CREATE);

        //----------------VIEWS-----------------------
        _db.execSQL(CajaHelper.CAJA_VIEW_CREATE_SCRIPT);
        _db.execSQL(HorarioHelper.HORARIO_CREATE_VIEW);
        _db.execSQL(HorarioHelper.HORARIO_CREATE_VIEW_2);
        _db.execSQL(VentasHelper.VENTA_VW_DIA_APP_CREATE_SCRIPT);
        _db.execSQL(VentasHelper.VENTA_VW_DIA_VDDOR_CREATE_SCRIPT);
        _db.execSQL(VentasHelper.VENTA_VW_MES_APP_CREATE_SCRIPT);
        _db.execSQL(VentasHelper.VENTA_VW_MES_VDDOR_CREATE_SCRIPT);
        _db.execSQL(ProductTable.CREATE_VIEW_TOP_VENTAS);
        _db.execSQL(CuentaTable.VIEW_CREATE_SCRIPT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG, String.format("Upgrade DB from %d to %d", oldVersion, newVersion));

        SQLiteDatabase _db;

        //si fue abierta para consulta y no estaba creada, se abre en modo read y no permite la creacion
        //del modelo, entonces forzamos una apertura write
        if (db.isReadOnly())
            _db = this.getWritableDatabase();
        else
            _db = db;

        if(isColExists(_db, ConfigHelper.TABLE_NAME, ConfigHelper.CONFIG_SII_VALE_MERCHANT_ID) == false)
        {
            _db.execSQL(ConfigHelper.CONFIG_ADD_SII_VALE_MERCHANT_ID);
        }


    }

    public long insert(String table_name, ContentValues cv) throws Exception {
        try {
            return this.getWritableDatabase().insert(table_name, null, cv);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public boolean update(String table_name, ContentValues cv, long _id) throws Exception {
        try {
            int count = this.getWritableDatabase().update(
                    table_name,
                    cv,
                    " _id = ?",
                    new String[]{String.valueOf(_id)}
            );

            return (count > 0);//retornamos si pudo actualizar algun registro
        } catch (Exception ex) {
            throw ex;
        }
    }

    public boolean delete(String table_name, long _id) throws Exception {
        try {
            int count = this.getWritableDatabase().delete(
                    table_name,
                    " _id = ?",
                    new String[]{String.valueOf(_id)}
            );

            return (count > 0);//retornamos si pudo eliminar algun registro
        } catch (Exception ex) {
            throw ex;
        }
    }

    public Cursor query(String query) throws Exception {

        try {


            return this.getWritableDatabase().rawQuery(query, null);


        } catch (Exception ex) {
            throw ex;
        }

    }

    private boolean isTableExists(SQLiteDatabase db, String tableName) {
        if (tableName == null || db == null || !db.isOpen()) {
            return false;
        }
        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM sqlite_master WHERE type = ? AND name = ?", new String[]{"table", tableName});
        if (!cursor.moveToFirst()) {
            return false;
        }
        int count = cursor.getInt(0);
        cursor.close();
        return count > 0;
    }

    //CUIDADO DE QUE LOS NOMBRES DE LAS COLUMNAS NO ESTEN INCLUIDOS EN EL NOMBRE DE LA TABLA
    private boolean isColExists(SQLiteDatabase db, String tableName, String columnName) {
        if (tableName == null || db == null || !db.isOpen()) {
            return false;
        }
        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM sqlite_master WHERE type = ? AND name = ? AND sql like '%" + columnName + "%'", new String[]{"table", tableName});
        if (!cursor.moveToFirst()) {
            return false;
        }
        int count = cursor.getInt(0);
        cursor.close();
        return count > 0;
    }
}
