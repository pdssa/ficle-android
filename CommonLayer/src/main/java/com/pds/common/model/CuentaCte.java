package com.pds.common.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.pds.common.Formatos;

import java.text.ParseException;
import java.util.Date;

public class CuentaCte implements Parcelable{

    private long id;
    private int tipoMov;
    private Date fechaHora;
    private Double monto;
    private long idVenta;
    private Venta venta;
    private Cliente cliente;
    private String observacion;
    private String tipoVenta;
    private String syncStatus;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getTipoMov() {
        return tipoMov;
    }

    public void setTipoMov(int tipoMov) {
        this.tipoMov = tipoMov;
    }

    public long getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(long idVenta) {
        this.idVenta = idVenta;
    }

    public void setTipoMov(TipoMovimientoCtaCte tipoMov) {
        this.tipoMov = tipoMov.ordinal();
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    public Venta getVenta() {
        return venta;
    }

    public void setVenta(Venta venta) {
        this.venta = venta;
        this.idVenta = venta != null ? venta.getId() : 0;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getTipoVenta() {
        return tipoVenta;
    }

    public void setTipoVenta(String tipoVenta) {
        this.tipoVenta = tipoVenta;
    }

    public String getSyncStatus() {
        return syncStatus;
    }

    public void setSyncStatus(String syncStatus) {
        this.syncStatus = syncStatus;
    }

    public static enum TipoMovimientoCtaCte {
        DEUDA, //0
        COBRO //1
    }

    public static final int ID_PRESTAMO = -2;

    public CuentaCte(){
        idVenta = 0;
    }

    @Override
    public String toString() {
        return "";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel target, int flags) {
        target.writeLong(id);
        target.writeInt(tipoMov);
        target.writeString(Formatos.DbDateTimeFormat.format(fechaHora));
        target.writeDouble(monto);
        target.writeLong(idVenta);
        target.writeParcelable(venta, 0);
        target.writeParcelable(cliente, 0);
        target.writeString(observacion);
        target.writeString(tipoVenta);
    }

    public static final Creator<CuentaCte> CREATOR = new Creator<CuentaCte>() {
        public CuentaCte createFromParcel(Parcel in) {
            return new CuentaCte(in);
        }

        public CuentaCte[] newArray(int size) {
            return new CuentaCte[size];
        }
    };

    private CuentaCte(Parcel in) {
        id = in.readLong();
        tipoMov = in.readInt();
        try {
            fechaHora = Formatos.DbDateTimeFormat.parse(in.readString());
        } catch (ParseException e) {
            Log.e("CuentaCte", "cannot parse fechaHora", e);
        }
        monto = in.readDouble();
        idVenta = in.readLong();
        venta = in.readParcelable(Venta.class.getClassLoader());
        cliente = in.readParcelable(Cliente.class.getClassLoader());
        observacion = in.readString();
        tipoVenta = in.readString();
    }
}
