package com.pds.common.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Hernan on 05/12/13.
 */
public class ComprobanteDetalle extends VentaDetalleAbstract implements Parcelable {

    private long id;
    private int idComprobante;
    private int idProducto;
    private double cantidad;
    private double precio;
    private double total;
    private String sku;
    private double neto;
    private double iva;

    public double getNeto() {
        return neto;
    }

    public void setNeto(double neto) {
        this.neto = neto;
    }

    public double getIva() {
        return iva;
    }

    public void setIva(double iva) {
        this.iva = iva;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    private Comprobante comprobante;
    private Product producto;

    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getIdComprobante() {
        return idComprobante;
    }

    public void setIdComprobante(int idComprobante) {
        this.idComprobante = idComprobante;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Comprobante getComprobante() {
        return comprobante;
    }

    public void setComprobante(Comprobante comprobante) {
        this.comprobante = comprobante;
    }

    public Product getProducto() {
        return producto;
    }

    public void setProducto(Product producto) {
        this.producto = producto;
    }
    public ComprobanteDetalle(){

    }
    public ComprobanteDetalle(int id, double precio, double cantidad, double total, int comprobanteid, int productoid, String sku){
        this.id = id;
        this.total = total;
        this.cantidad = cantidad;
        this.idComprobante= comprobanteid;
        this.idProducto = productoid;
        this.precio = precio;
        this.sku = sku;
    }

    @Override
    public String toString() {
        return "";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel target, int flags) {
        target.writeLong(id);
        target.writeParcelable(comprobante, 0);
        target.writeParcelable(producto, 0);
        target.writeDouble(cantidad);
        target.writeDouble(precio);
        target.writeDouble(total);
        target.writeString(sku);
        target.writeDouble(neto);
        target.writeDouble(iva);
    }

    public static final Creator<ComprobanteDetalle> CREATOR = new Creator<ComprobanteDetalle>() {
        public ComprobanteDetalle createFromParcel(Parcel in) {
            return new ComprobanteDetalle(in);
        }

        public ComprobanteDetalle[] newArray(int size) {
            return new ComprobanteDetalle[size];
        }
    };

    private ComprobanteDetalle(Parcel in) {
        this.id = in.readLong();
        this.comprobante = in.readParcelable(Comprobante.class.getClassLoader());
        this.producto = in.readParcelable(Product.class.getClassLoader());
        this.cantidad = in.readDouble();
        this.precio = in.readDouble();
        this.total = in.readDouble();
        this.sku = in.readString();
        this.neto = in.readDouble();
        this.iva = in.readDouble();
    }

    public boolean esItemPesable(){
        if(producto != null)
            return producto.isWeighable();
        else
            return false;
    }
    public boolean esItemGenerico(){
        if(idProducto != -1){
            if(producto != null){
                return producto.isGeneric() || precio == 0;
            }
            else
                return false;
        }
        else
            return true;
    }
}
