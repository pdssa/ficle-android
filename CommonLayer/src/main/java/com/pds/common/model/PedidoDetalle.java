package com.pds.common.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by EMEKA on 13/01/2015.
 */
public class PedidoDetalle implements Parcelable {
    private long id;
    private long id_pedido;
    private long idProducto;
    private int cantidad;
    private String sync;

    private Product producto;
    private Pedido pedido;

    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getId_pedido() {
        return id_pedido;
    }

    public void setId_pedido(long id_pedido) {
        this.id_pedido = id_pedido;
    }

    public long getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(long idProducto) {
        this.idProducto = idProducto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }

    public PedidoDetalle(){

    }

    public Product getProducto() {
        return producto;
    }

    public void setProducto(Product producto) {
        this.producto = producto;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel target, int flags) {
        target.writeLong(id);
        target.writeLong(id_pedido);
        target.writeLong(idProducto);
        target.writeInt(cantidad);
        target.writeString(sync);
        target.writeParcelable(producto, 0);
    }

    public static final Creator<PedidoDetalle> CREATOR = new Creator<PedidoDetalle>() {
        public PedidoDetalle createFromParcel(Parcel in) {
            return new PedidoDetalle(in);
        }

        public PedidoDetalle[] newArray(int size) {
            return new PedidoDetalle[size];
        }
    };

    private PedidoDetalle(Parcel in) {
        this.id = in.readLong();
        this.id_pedido = in.readLong();
        this.idProducto = in.readLong();
        this.cantidad = in.readInt();
        this.sync = in.readString();
        this.producto = in.readParcelable(Product.class.getClassLoader());
    }
}
