package com.pds.common.model;

import android.content.ContentResolver;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.pds.common.Formato;
import com.pds.common.Formatos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Hernan on 06/06/2016.
 */
public class Promocion implements Parcelable {

    protected Promocion(Parcel in) {
        id = in.readLong();
        tipoPromocion = TIPO_PROMOCION.fromValue(in.readInt());
        valorAsociado = in.readDouble();

        try {
            vigenciaDesde = Formatos.DbDate(in.readString());
        } catch (Exception e) {
            Log.e("Promocion", "cannot parse fecha desde", e);
        }
        try {
            vigenciaHasta = Formatos.DbDate(in.readString());
        } catch (Exception e) {
            Log.e("Promocion", "cannot parse fecha hasta", e);
        }

        codPromocion = in.readString();
        nombrePromocion = in.readString();
        textoPromocion = in.readString();
        skuProductoAsociado = in.readString();
        imgUriProductoAsociado = in.readString();
        baja = in.readByte() != 0;
        sync = in.readString();
        condiciones = new ArrayList<PromocionCondicion>();
        in.readTypedList(condiciones, PromocionCondicion.CREATOR);
    }

    public static final Creator<Promocion> CREATOR = new Creator<Promocion>() {
        @Override
        public Promocion createFromParcel(Parcel in) {
            return new Promocion(in);
        }

        @Override
        public Promocion[] newArray(int size) {
            return new Promocion[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeInt(tipoPromocion.ordinal());
        dest.writeDouble(valorAsociado);
        dest.writeString(Formatos.DbDate(vigenciaDesde));
        dest.writeString(Formatos.DbDate(vigenciaHasta));
        dest.writeString(codPromocion);
        dest.writeString(nombrePromocion);
        dest.writeString(textoPromocion);
        dest.writeString(skuProductoAsociado);
        dest.writeString(imgUriProductoAsociado);
        dest.writeByte((byte) (baja ? 1 : 0));
        dest.writeString(sync);
        dest.writeTypedList(condiciones);
    }

    public enum TIPO_PROMOCION {
        //Descuento sobre el precio de venta (sin IVA) al producto principal, más la suma de los precios de venta de los productos secundarios que se hayan asociado
        DESCUENTO_PROD_PPAL,
        //Aplicación de un precio fijo de venta al pack, independiente del precio de venta de los productos
        PRECIO_FIJO_VENTA;

        public static TIPO_PROMOCION fromValue(int idTipoPromocion)
                throws IllegalArgumentException {
            try {
                return TIPO_PROMOCION.values()[idTipoPromocion];
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new IllegalArgumentException("Unknown enum value :" + idTipoPromocion);
            }
        }
    }

    private long id;
    private TIPO_PROMOCION tipoPromocion;
    private double valorAsociado;
    private Date vigenciaDesde;
    private Date vigenciaHasta;
    private String codPromocion;
    private String nombrePromocion;
    private String textoPromocion;
    private String skuProductoAsociado;
    private String imgUriProductoAsociado;
    private boolean baja;
    private String sync;
    private List<PromocionCondicion> condiciones;

    public Promocion() {
    }

    public Promocion(long id, String nombrePromocion, String codPromocion, String textoPromocion, String skuProductoAsociado, String imgUriProductoAsociado, String desde, String hasta, TIPO_PROMOCION tipo, double valorAsociado, List<PromocionCondicion> condiciones) {
        this.id = id;
        this.nombrePromocion = nombrePromocion;
        this.codPromocion = codPromocion;
        this.skuProductoAsociado = skuProductoAsociado;
        this.textoPromocion = textoPromocion;
        this.imgUriProductoAsociado = imgUriProductoAsociado;
        try {
            this.vigenciaDesde = Formatos.DbDate(desde);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            this.vigenciaHasta = Formatos.DbDate(hasta);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.tipoPromocion = tipo;
        this.valorAsociado = valorAsociado;
        this.condiciones = condiciones;
        baja = false;
        sync = "N";
    }

    public TIPO_PROMOCION getTipoPromocion() {
        return tipoPromocion;
    }

    public void setTipoPromocion(TIPO_PROMOCION tipoPromocion) {
        this.tipoPromocion = tipoPromocion;
    }

    public boolean isBaja() {
        return baja;
    }

    public void setBaja(boolean baja) {
        this.baja = baja;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }

    public double getValorAsociado() {
        return valorAsociado;
    }

    public void setValorAsociado(double valorAsociado) {
        this.valorAsociado = valorAsociado;
    }

    public Date getVigenciaDesde() {
        return vigenciaDesde;
    }

    public void setVigenciaDesde(Date vigenciaDesde) {
        this.vigenciaDesde = vigenciaDesde;
    }

    public Date getVigenciaHasta() {
        return vigenciaHasta;
    }

    public void setVigenciaHasta(Date vigenciaHasta) {
        this.vigenciaHasta = vigenciaHasta;
    }

    public String getCodPromocion() {
        return codPromocion;
    }

    public void setCodPromocion(String codPromocion) {
        this.codPromocion = codPromocion;
    }

    public String getTextoPromocion() {
        return textoPromocion;
    }

    public void setTextoPromocion(String textoPromocion) {
        this.textoPromocion = textoPromocion;
    }

    public String getSkuProductoAsociado() {
        return skuProductoAsociado;
    }

    public void setSkuProductoAsociado(String skuProductoAsociado) {
        this.skuProductoAsociado = skuProductoAsociado;
    }

    public void setImgUriProductoAsociado(String imgUriProductoAsociado) {
        this.imgUriProductoAsociado = imgUriProductoAsociado;
    }

    public String getUriImgProductoAsociado() {
        return imgUriProductoAsociado;
    }

    public String getNombrePromocion() {
        return nombrePromocion;
    }

    public void setNombrePromocion(String nombrePromocion) {
        this.nombrePromocion = nombrePromocion;
    }

    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVigenciaString() {
        return Formato.FormateaDate(this.vigenciaDesde, Formato.DateFormatSlashAndTime) + " al " + Formato.FormateaDate(this.vigenciaHasta, Formato.DateFormatSlashAndTime);
    }

    public double calcularPrecioVenta(ContentResolver resolver, Combo combo) {
        return calcularPrecioVenta(
                resolver,
                combo,
                combo.getProductoPrincipal(resolver),
                combo.getItemsCombo(resolver)
        );
    }

    public double calcularPrecioVenta(ContentResolver resolver, Combo combo, boolean promocionVigente) {
        return calcularPrecioVenta(
                resolver,
                combo,
                combo.getProductoPrincipal(resolver),
                combo.getItemsCombo(resolver),
                promocionVigente
        );
    }

    public double calcularPrecioVenta(ContentResolver resolver, Combo combo, Product prodPpal, List<ComboItem> comboItems) {
        return calcularPrecioVenta(
                resolver,
                combo,
                prodPpal,
                comboItems,
                promocionVigente()
        );
    }

    public double calcularPrecioVenta(ContentResolver resolver, Combo combo, Product prodPpal, List<ComboItem> comboItems, boolean promocionVigente) {
        if (promocionVigente) {
            //si está vigente, tenemos que aplicar la promo según el tipo

            switch (tipoPromocion) {

                case DESCUENTO_PROD_PPAL: {

                    //descuento sobre precio actual del prod ppal (menos IVA)
                    double descuentoProdPpal = combo.getCantidad() * prodPpal.getPurchasePrice() / (1 + prodPpal.getIva() / 100 ) * (valorAsociado / 100);
                    double precioVentaCombo = combo.getCantidad() * prodPpal.getSalePrice() - descuentoProdPpal;

                    //sumamos el precio actual de los prods secundarios
                    if(comboItems != null) {
                        for (ComboItem comboItem : comboItems) {
                            Product prodSecund = comboItem.getProducto(resolver);

                            precioVentaCombo += (comboItem.getCantidad() * prodSecund.getSalePrice());
                        }
                    }

                    return Formato.RedondeaDecimal(precioVentaCombo);
                }

                case PRECIO_FIJO_VENTA: {
                    //aplica un valor fijo , independiente de los precios de los productos que lo conforman
                    return valorAsociado;
                }
            }
        }

        //si la promo no está vigente, o no es un tipo de promo conocida
        //PRECIO COMBO = SUMA PRECIOS ACTUALES DE LOS PRODUCTOS

        //sobre precio actual del prod ppal
        double precioVentaCombo = combo.getCantidad() * prodPpal.getSalePrice();

        //sumamos el precio actual de los prods secundarios
        if(comboItems != null) {
            for (ComboItem comboItem : comboItems) {
                Product prodSecund = comboItem.getProducto(resolver);

                precioVentaCombo += (comboItem.getCantidad() * prodSecund.getSalePrice());
            }
        }

        return Formato.RedondeaDecimal(precioVentaCombo);
    }


    private boolean promocionVigente() {

        Date now = new Date();

        return now.compareTo(vigenciaDesde) >= 0 && now.compareTo(vigenciaHasta) <= 0;

    }

    public boolean checkProductCondition(Product product){
        for (PromocionCondicion condicion : condiciones) {
            if(condicion.checkCondition(product))
                return true;
        }

        return false;
    }


    public boolean promocionExpirada() {

        Date now = new Date();

        return now.compareTo(vigenciaHasta) > 0;

    }
}
