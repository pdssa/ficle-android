package com.pds.common.model;

import android.content.ContentResolver;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.pds.common.dao.TaxDao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Department implements Parcelable{
    private static final SimpleDateFormat ALTA_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private long id;
    private String name;
    private Date alta;
    private String description;
    private int visualization_mode;
    private int generic_dept;
    private int automatic;
    private String codigo;

    public int getAutomatic() {
        return automatic;
    }

    public void setAutomatic(int automatic) {
        this.automatic = automatic;
    }

    public Department(long id, String name){
        this.name = name;
        this.id = id;
    }
    public Department(){
    }

    public Department(long id){
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Date getAlta() {
        return alta;
    }

    public void setAlta(Date alta) {
        this.alta = alta;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isGenericDept(){
        return generic_dept == 1 || isGenericPesableDept();
    }

    public boolean isGenericPesableDept(){
        return generic_dept == 2;
    }

    public boolean isVistaLista(){
        return visualization_mode == 0;
    }

    public boolean isVistaGrilla(){
        return visualization_mode == 1;
    }

    public int getGeneric_dept() {
        return generic_dept;
    }

    public void setGeneric_dept(int generic_dept) {
        this.generic_dept = generic_dept;
    }

    public int getVisualization_mode() {
        return visualization_mode;
    }

    public void setVisualization_mode(int visualization_mode) {
        this.visualization_mode = visualization_mode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel target, int flags) {
        target.writeLong(id);
        target.writeString(name);
        target.writeString(description);
        target.writeString(ALTA_FORMAT.format(alta));
        target.writeInt(visualization_mode);
        target.writeInt(generic_dept);
        target.writeInt(automatic);
        target.writeString(codigo);
    }

    public static final Creator<Department> CREATOR = new Creator<Department>() {
        public Department createFromParcel(Parcel in) {
            return new Department(in);
        }

        public Department[] newArray(int size) {
            return new Department[size];
        }
    };

    private Department(Parcel in) {
        id = in.readLong();
        name = in.readString();
        description = in.readString();
        try {
            alta = ALTA_FORMAT.parse(in.readString());
        } catch (ParseException e) {
            Log.e("Provider", "cannot parse alta", e);
        }
        visualization_mode = in.readInt();
        generic_dept = in.readInt();
        automatic = in.readInt();
        codigo = in.readString();
    }

    public Department(String fromImport){
        //TYPEREG;DEPARTAMENTO;GENERIC;VISTA;CODIGO
        //D;Abarrotes Dulces;0;0;0104000000
        String[] datos = fromImport.split(";",-1);

        name = datos[1].toUpperCase();
        description = "";
        alta = new Date();
        generic_dept = Integer.parseInt(datos[2]);
        visualization_mode = Integer.parseInt(datos[3]);
        codigo = datos[4];
        automatic = 1;
    }

//    public Product exportDepartmentGenericProduct(Tax _tax){
//
//        //hay que crear un producto generico
//        Product product  = new Product();
//
//        product.setIdDepartment(this.getId());
//        product.setWeighable(this.isGenericPesableDept());
//        product.setName(this.getName());
//        product.setDescription(this.getDescription());
//        product.setGeneric(true);
//        product.setAlta(new Date());
//        product.setSalePrice(0);
//        product.setStock(0);
//        product.setPurchasePrice(0);
//        product.setMinStock(0);
//        product.setContenido(0);
//        product.setBrand("");
//        product.setCode("");
//        product.setProvider(null);
//        product.setUnit("");
//        product.setPresentation("");
//        product.setTax(_tax);
//        product.setIva(_tax.getAmount());
//        product.setCodigoPadre(this.getCodigo());//ponemos el codigo de padre
//        product.setQuickAccess(false);
//        product.setAutomatic(Product.TYPE_AUTOM.PROD_ALTA_AUTOM);
//
//        return product;
//    }
}
