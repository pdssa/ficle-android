package com.pds.common.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Alicuota implements Parcelable{

    private long id;
    private String name;
    private Double amount;

    public Alicuota(){
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return this.name;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel target, int flags) {
        target.writeLong(id);
        target.writeString(name);
        target.writeDouble(amount);
    }

    public static final Creator<Alicuota> CREATOR = new Creator<Alicuota>() {
        public Alicuota createFromParcel(Parcel in) {
            return new Alicuota(in);
        }

        public Alicuota[] newArray(int size) {
            return new Alicuota[size];
        }
    };

    private Alicuota(Parcel in) {
        id = in.readLong();
        name = in.readString();
        amount = in.readDouble();
    }
}
