package com.pds.common.model;

import android.content.ContentResolver;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.pds.common.dao.ProductDao;

/**
 * Created by Hernan on 25/02/2015.
 */
public class ComboItem implements Parcelable {

    private long id;
    private long combo_id;
    private long producto_id;
    private String producto_code;
    private double cantidad;
    private String sync;
    private double producto_precio_vta;

    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getCombo_id() {
        return combo_id;
    }

    public void setCombo_id(long combo_id) {
        this.combo_id = combo_id;
    }

    public long getProducto_id() {
        return producto_id;
    }

    public void setProducto_id(long producto_id) {
        this.producto_id = producto_id;
    }

    public String getProducto_code() {
        return producto_code;
    }

    public void setProducto_code(String producto_code) {
        this.producto_code = producto_code;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }

    public double getProducto_precio_vta() {
        return producto_precio_vta;
    }

    public void setProducto_precio_vta(double producto_precio_vta) {
        this.producto_precio_vta = producto_precio_vta;
    }

    public ComboItem(){

    }

    @Override
    public String toString() {
        return "";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel target, int flags) {
        target.writeLong(id);
        target.writeLong(combo_id);
        target.writeLong(producto_id);
        target.writeString(producto_code);
        target.writeDouble(cantidad);
        target.writeString(sync);
        target.writeDouble(producto_precio_vta);
    }

    public static final Creator<ComboItem> CREATOR = new Creator<ComboItem>() {
        public ComboItem createFromParcel(Parcel in) {
            return new ComboItem(in);
        }

        public ComboItem[] newArray(int size) {
            return new ComboItem[size];
        }
    };

    private ComboItem(Parcel in) {
        this.id = in.readLong();
        this.combo_id = in.readLong();
        this.producto_id = in.readLong();
        this.producto_code = in.readString();
        this.cantidad = in.readDouble();
        this.sync = in.readString();
        this.producto_precio_vta = in.readDouble();
    }

    public Product getProducto(ContentResolver contentResolver) {
        return new ProductDao(contentResolver).find(this.producto_id);
    }
}

