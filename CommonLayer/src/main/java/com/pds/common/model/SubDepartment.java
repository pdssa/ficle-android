package com.pds.common.model;

import android.content.ContentResolver;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.pds.common.dao.DepartmentDao;
import com.pds.common.dao.TaxDao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class SubDepartment implements Parcelable {
    private static final SimpleDateFormat ALTA_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private long id;
    private Department department;
    private String name;
    private Date alta;
    private String description;
    private int visualization_mode;
    private int generic_subdept;
    private int automatic;
    private String codigo;
    private String codigo_padre;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigoPadre() {
        return codigo_padre;
    }

    public void setCodigoPadre(String codigo_padre) {
        this.codigo_padre = codigo_padre;
    }


    public int getAutomatic() {
        return automatic;
    }

    public void setAutomatic(int automatic) {
        this.automatic = automatic;
    }

    public SubDepartment(long id, String name) {
        this.id = id;
        this.name = name;
    }


    public SubDepartment() {
    }

    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public Date getAlta() {
        return alta;
    }

    public void setAlta(Date alta) {
        this.alta = alta;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public boolean isGenericSubdept() {
        return generic_subdept == 1 || isGenericPesableSubdept();
    }

    public boolean isGenericPesableSubdept() {
        return generic_subdept == 2;
    }

    public boolean isVistaLista() {
        return visualization_mode == 0;
    }

    public boolean isVistaGrilla() {
        return visualization_mode == 1;
    }

    public int getGeneric_subdept() {
        return generic_subdept;
    }

    public void setGeneric_subdept(int generic_subdept) {
        this.generic_subdept = generic_subdept;
    }

    public int getVisualization_mode() {
        return visualization_mode;
    }

    public void setVisualization_mode(int visualization_mode) {
        this.visualization_mode = visualization_mode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel target, int flags) {
        target.writeLong(id);
        target.writeParcelable(department, 0);
        target.writeString(name);
        target.writeString(description);
        target.writeString(ALTA_FORMAT.format(alta));
        target.writeInt(visualization_mode);
        target.writeInt(generic_subdept);
        target.writeInt(automatic);
        target.writeString(codigo);
        target.writeString(codigo_padre);
    }

    public static final Creator<SubDepartment> CREATOR = new Creator<SubDepartment>() {
        public SubDepartment createFromParcel(Parcel in) {
            return new SubDepartment(in);
        }

        public SubDepartment[] newArray(int size) {
            return new SubDepartment[size];
        }
    };

    private SubDepartment(Parcel in) {
        id = in.readLong();
        department = in.readParcelable(Department.class.getClassLoader());
        name = in.readString();
        description = in.readString();
        try {
            alta = ALTA_FORMAT.parse(in.readString());
        } catch (ParseException e) {
            Log.e("Provider", "cannot parse alta", e);
        }
        visualization_mode = in.readInt();
        generic_subdept = in.readInt();
        automatic = in.readInt();
        codigo = in.readString();
        codigo_padre = in.readString();
    }


    public SubDepartment(String fromImport) {
        //TYPEREG;SUBDEPARTAMENTO;GENERIC;VISTAGRILLA;CODIGO;CODIGO_PADRE
        //S;Bizcochos;0;0;0104000000;0100000000
        String[] datos = fromImport.split(";", -1);

        name = datos[1].toUpperCase();
        description = "";
        alta = new Date();
        //department = new DepartmentDao(contentResolver).find(Integer.parseInt(datos[3]) + deptoIdDelta);
        generic_subdept = Integer.parseInt(datos[2]);
        visualization_mode = Integer.parseInt(datos[3]);
        codigo = datos[4];
        codigo_padre = datos[5];
        automatic = 1;

        /*
        List<Department> result = new DepartmentDao(contentResolver).list("codigo=? and generic = 0", new String[]{codigo_padre}, null);
        if (result.size() > 0)
            department = result.get(0);
        else
            department = null;
        */
    }

//    public Product exportSubDepartmentGenericProduct(Tax _tax) {
//
//        //hay que crear un producto generico
//        Product product = new Product();
//
//        product.setIdDepartment(department.getId());
//        product.setIdSubdepartment(this.getId());
//        product.setWeighable(this.isGenericPesableSubdept());
//        product.setName(this.getName());
//        product.setDescription(this.getDescription());
//        product.setGeneric(true);
//        product.setAlta(new Date());
//        product.setSalePrice(0);
//        product.setStock(0);
//        product.setPurchasePrice(0);
//        product.setMinStock(0);
//        product.setContenido(0);
//        product.setBrand("");
//        product.setCode("");
//        product.setProvider(null);
//        product.setUnit("");
//        product.setPresentation("");
//        product.setTax(_tax);
//        product.setIva(_tax.getAmount());
//
//        return product;
//    }
}
