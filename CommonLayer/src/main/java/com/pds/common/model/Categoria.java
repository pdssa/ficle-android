package com.pds.common.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.pds.common.Formatos;

import java.text.ParseException;
import java.util.Date;

/**
 * Created by EMEKA on 22/01/2015.
 */
public class Categoria implements Parcelable {

    private long id;
    private String nombre;
    private String descripcion;
    private String codigo;
    private String codigo_padre;
    private boolean ultimo_nivel;
    private int numero_nivel;

    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo_padre() {
        return codigo_padre;
    }

    public void setCodigo_padre(String codigo_padre) {
        this.codigo_padre = codigo_padre;
    }

    public boolean isUltimo_nivel() {
        return ultimo_nivel;
    }

    public void setUltimo_nivel(boolean ultimo_nivel) {
        this.ultimo_nivel = ultimo_nivel;
    }

    public int getNumero_nivel() {
        return numero_nivel;
    }

    public void setNumero_nivel(int numero_nivel) {
        this.numero_nivel = numero_nivel;
    }

    public Categoria() {
    }

    public Categoria(long _id, String _name, int nivel, String cod_padre) {
        id = _id;
        nombre = _name;
        codigo = "...";
        codigo_padre = cod_padre;
        numero_nivel = nivel;
    }

    public Categoria(Parcel in) {
        id = in.readLong();
        nombre = in.readString();
        descripcion= in.readString();
        codigo= in.readString();
        codigo_padre= in.readString();
        ultimo_nivel= in.readInt() > 0;
        numero_nivel= in.readInt();
    }

    public Categoria(String fromImport){
        //TYPEREG;NIVEL;NOMBRE;CODIGO;CODIGO_PADRE;DESCRIPCION;ULTIMO
        //C;2;Abarrotes Dulces;0104000000;0100000000;Abarr. Dulces;0
        String[] datos = fromImport.split(";",-1);

        numero_nivel = Integer.parseInt(datos[1]);
        nombre = datos[2].toUpperCase();
        codigo = datos[3];
        codigo_padre =datos[4];
        descripcion = datos[5].toUpperCase();
        ultimo_nivel =  Integer.parseInt(datos[6]) > 0;
    }

    public static final Creator<Categoria> CREATOR = new Creator<Categoria>() {
        public Categoria createFromParcel(Parcel in) {
            return new Categoria(in);
        }

        public Categoria[] newArray(int size) {
            return new Categoria[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel target, int flags) {
        target.writeLong(id);
        target.writeString(nombre );
        target.writeString(descripcion);
        target.writeString(codigo);
        target.writeString(codigo_padre);
        target.writeInt(ultimo_nivel ? 1 : 0);
        target.writeInt(numero_nivel);
    }

    @Override
    public String toString() {
        return this.nombre;
    }
}
