package com.pds.common.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Hernan on 05/11/2014.
 */
public class ProviderProduct implements Parcelable {
    private long id;
    private long idProduct;
    private long idProvider;

    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(long idProduct) {
        this.idProduct = idProduct;
    }

    public long getIdProvider() {
        return idProvider;
    }

    public void setIdProvider(long idProvider) {
        this.idProvider = idProvider;
    }

    public ProviderProduct() {
    }

    public ProviderProduct(long idProduct, long idProvider) {
        this.idProduct = idProduct;
        this.idProvider = idProvider;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel target, int flags) {
        target.writeLong(id);
        target.writeLong(idProduct);
        target.writeLong(idProvider);
    }

    public static final Creator<ProviderProduct> CREATOR = new Creator<ProviderProduct>() {
        public ProviderProduct createFromParcel(Parcel in) {
            return new ProviderProduct(in);
        }

        public ProviderProduct[] newArray(int size) {
            return new ProviderProduct[size];
        }
    };

    private ProviderProduct(Parcel in) {
        id = in.readLong();
        idProduct = in.readLong();
        idProvider = in.readLong();
    }
}
