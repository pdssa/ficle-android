package com.pds.common.model;

import android.content.ContentResolver;
import android.os.Parcel;
import android.os.Parcelable;

import com.pds.common.dao.ComprobanteDetalleDao;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hernan on 05/12/13.
 */
public class Comprobante extends VentaAbstract implements Parcelable {

    public static enum TipoMedioPago {
        EFECTIVO, //0
        CHEQUE, //1
        TARJETACREDITO, //2
        TARJETADEBITO //3
    }

    private long id;
    private double total;
    private int cantidad;
    private String fecha;
    private String hora;
    private String codigo;
    private int userid;
    private String medio_pago;
    private double neto;
    private double iva;
    private double vuelto;
    private boolean anulada;
    private String sync;

    @Override
    public double getVuelto() {
        return vuelto;
    }

    public void setVuelto(double vuelto) {
        this.vuelto = vuelto;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }

    public boolean isAnulada() {
        return anulada;
    }

    public void setAnulada(boolean anulada) {
        this.anulada = anulada;
    }

    public double getNeto() {
        return neto;
    }

    public void setNeto(double neto) {
        this.neto = neto;
    }

    public double getIva() {
        return iva;
    }

    public void setIva(double iva) {
        this.iva = iva;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public long getId() {
        return id;
    }

    public double getTotal() {
        return total;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getFecha() {
        return fecha;
    }

    public String getHora() {
        return hora;
    }

    public String getFechaHoraCorta() {
        return fecha.substring(0, 6) + fecha.substring(8) + " " + hora.substring(0, 5);
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getCantidad() {
        return cantidad;
    }

    public int getUserid() {
        return userid;
    }

    public String getMedio_pago() {
        return medio_pago;
    }

    public void setMedio_pago(String medio_pago) {
        this.medio_pago = medio_pago;
    }

    public String getNroVenta(){
        return getNroComprobante();
    }
    public String getNroComprobante(){
        String nroVenta = "00000000" + String.valueOf(this.id);
        return nroVenta.substring( nroVenta.length() - 8 );
    }

    //agregamos los detalles
    public void addDetalles(ContentResolver contentResolver) {
        setDetalles(new ComprobanteDetalleDao(contentResolver).list(String.format("id_comprobante = %d", getId()), null, null));
    }

    public List<ComprobanteDetalle> getDetalles() {
        return detalles;
    }

    public List<VentaDetalleAbstract> getDetallesAbstract() {
        List<VentaDetalleAbstract> list =  new ArrayList<VentaDetalleAbstract>();
        list.addAll(detalles);
        return list;
    }

    public void setDetalles(List<ComprobanteDetalle> detalles) {
        this.detalles = detalles;
    }

    private List<ComprobanteDetalle> detalles;

    public Comprobante(){

    }
    public Comprobante(long id, double total, int cantidad, String fecha, String hora, String codigo, int userid, String medio_pago, double _neto, double _iva){
        this.id = id;
        this.fecha = fecha;
        this.hora = hora;
        this.total = total;
        this.cantidad = cantidad;
        this.codigo = codigo;
        this.userid = userid;
        this.medio_pago = medio_pago;
        this.neto = _neto;
        this.iva = _iva;
    }

    @Override
    public String toString() {
        return "";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel target, int flags) {
        target.writeLong(id);
        target.writeString(fecha);
        target.writeString(hora);
        target.writeDouble(total);
        target.writeInt(cantidad);
        target.writeString(codigo);
        target.writeInt(userid);
        target.writeString(medio_pago);
        target.writeDouble(neto);
        target.writeDouble(iva);
        target.writeInt(anulada ? 1 : 0);
        target.writeString(sync);
    }

    public static final Creator<Comprobante> CREATOR = new Creator<Comprobante>() {
        public Comprobante createFromParcel(Parcel in) {
            return new Comprobante(in);
        }

        public Comprobante[] newArray(int size) {
            return new Comprobante[size];
        }
    };

    private Comprobante(Parcel in) {
        this.id = in.readLong();
        this.fecha = in.readString();
        this.hora = in.readString();
        this.total = in.readDouble();
        this.cantidad = in.readInt();
        this.codigo = in.readString();
        this.userid = in.readInt();
        this.medio_pago = in.readString();
        this.neto = in.readDouble();
        this.iva = in.readDouble();
        this.anulada = in.readInt() == 1 ;
        this.sync = in.readString();
    }

    public String getType(String pais){
        return pais.equals("AR") ? "PRESUP." : "COMPROB";
    }

    @Override
    public String getDbType() {return "CPB"; }
}
