package com.pds.common.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Tax implements Parcelable{

    private long id;
    private String name;
    private Integer amount;

    public Tax(){
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return this.name;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel target, int flags) {
        target.writeLong(id);
        target.writeString(name);
        target.writeInt(amount);
    }

    public static final Creator<Tax> CREATOR = new Creator<Tax>() {
        public Tax createFromParcel(Parcel in) {
            return new Tax(in);
        }

        public Tax[] newArray(int size) {
            return new Tax[size];
        }
    };

    private Tax(Parcel in) {
        id = in.readLong();
        name = in.readString();
        amount = in.readInt();
    }
}
