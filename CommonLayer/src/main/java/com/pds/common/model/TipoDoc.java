package com.pds.common.model;

import android.content.ContentResolver;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import org.json.JSONObject;

/**
 * Created by Hernan on 17/12/2014.
 */
public class TipoDoc implements Parcelable {

    private Long id;
    private String nombre;
    private String codigo;
    private boolean habilitado;

    public TipoDoc(String i, String s) {
        this.nombre = s;
        this.codigo  = i;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public boolean isHabilitado() {
        return habilitado;
    }

    public void setHabilitado(boolean habilitado) {
        this.habilitado = habilitado;
    }

    //JSON properties
    public void setTipoDocNombre(String tipoDocNombre){
        this.nombre = tipoDocNombre;
    }
    public void setTipoDocCodigo(String tipoDocCodigo){
        this.codigo = tipoDocCodigo;
    }
            //tipoDocHabilitado
    //

    public TipoDoc() {}

    public TipoDoc(Parcel in) {
        id = in.readLong();
        nombre = in.readString();
        codigo = in.readString();
        habilitado = in.readInt() == 1;
    }

    public static final Creator<TipoDoc> CREATOR = new Creator<TipoDoc>() {
        public TipoDoc createFromParcel(Parcel in) {
            return new TipoDoc(in);
        }

        public TipoDoc[] newArray(int size) {
            return new TipoDoc[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel target, int flags) {
        target.writeLong(id);
        target.writeString(nombre);
        target.writeString(codigo);
        target.writeInt(habilitado ? 1 : 0);
    }

    @Override
    public String toString() {
        return this.nombre;
    }

    public TipoDoc(JSONObject jsonObject) {
        try {
            nombre = jsonObject.getString("tipoDocNombre");
            codigo = jsonObject.getString("tipoDocCodigo");
            habilitado = jsonObject.getBoolean("tipoDocHabilitado");
        }
        catch (Exception ex){
            Log.e("TipoDoc", "Read from JSON", ex);
        }
    }
}
