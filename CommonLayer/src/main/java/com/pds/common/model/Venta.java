package com.pds.common.model;

import android.content.ContentResolver;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;

import com.pds.common.SII.vale.util.ValeConst;
import com.pds.common.dao.VentaDao;
import com.pds.common.dao.VentaDetalleDao;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hernan on 05/12/13.
 */
public class Venta extends VentaAbstract implements Parcelable {

    public static enum TipoMedioPago {
        EFECTIVO, //0
        CHEQUE, //1
        TARJETACREDITO, //2
        TARJETADEBITO //3
    }

    private long id;
    private double total;
    private int cantidad;
    private String fecha;
    private String hora;
    private String codigo;
    private int userid;
    private String medio_pago;

    private String fc_type;
    private long fc_id;
    private String fc_folio;
    private String fc_validation;
    private double neto;
    private double iva;
    private double vuelto;
    private String sync;
    private boolean anulada;

    private String fc_timbre;

    @Override
    public double getVuelto() {
        return vuelto;
    }

    public void setVuelto(double vuelto) {
        this.vuelto = vuelto;
    }

    public boolean isAnulada() {
        return anulada;
    }

    public void setAnulada(boolean anulada) {
        this.anulada = anulada;
    }

    public double getNeto() {
        return neto;
    }

    public void setNeto(double neto) {
        this.neto = neto;
    }

    public double getIva() {
        return iva;
    }

    public void setIva(double iva) {
        this.iva = iva;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }

    //agregamos los detalles
    public void addDetalles(ContentResolver contentResolver) {
        setDetalles(new VentaDetalleDao(contentResolver).list(String.format("id_venta = %d", getId()), null, null));
    }

    public List<VentaDetalle> getDetalles() {
        return detalles;
    }

    public List<VentaDetalleAbstract> getDetallesAbstract() {
        List<VentaDetalleAbstract> list = new ArrayList<VentaDetalleAbstract>();
        list.addAll(detalles);
        return list;
    }

    public void setDetalles(List<VentaDetalle> detalles) {
        this.detalles = detalles;
    }

    private List<VentaDetalle> detalles;

    public void setId(Long id) {
        this.id = id;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public long getId() {
        return id;
    }

    public double getTotal() {
        return total;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getFecha() {
        return fecha;
    }

    public String getHora() {
        return hora;
    }

    public String getFechaHoraCorta() {
        return fecha.substring(0, 6) + fecha.substring(8) + " " + hora.substring(0, 5);
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getCantidad() {
        return cantidad;
    }

    public int getUserid() {
        return userid;
    }

    public String getMedio_pago() {
        return medio_pago;
    }

    public void setMedio_pago(String medio_pago) {
        this.medio_pago = medio_pago;
    }

    public String getFc_type() {
        return fc_type;
    }

    public void setFc_type(String fc_type) {
        this.fc_type = fc_type;
    }

    public long getFc_id() {
        return fc_id;
    }

    public void setFc_id(long fc_id) {
        this.fc_id = fc_id;
    }

    public String getFc_folio() {
        return fc_folio;
    }

    public void setFc_folio(String fc_folio) {
        this.fc_folio = fc_folio;
    }

    public String getFc_validation() {
        return fc_validation;
    }

    public void setFc_validation(String fc_validation) {
        this.fc_validation = fc_validation;
    }

    public void setFC(long fc_id, String fc_type, String fc_folio, String fc_validation, String fc_stamp) {
        this.fc_id = fc_id;
        this.fc_type = fc_type;
        this.fc_folio = fc_folio;
        this.fc_validation = fc_validation;
        this.fc_timbre = fc_stamp;
    }

    public String getFc_Timbre() {
        return fc_timbre;
    }

    public void setFc_Timbre(String timbre) {
        this.fc_timbre = timbre;
    }

    public String getNroVenta() {
        String nroVenta = "00000000" + String.valueOf(this.id);

        if (!TextUtils.isEmpty(this.fc_folio)) {
            if (tieneValeAsociado())//si es un VALE, ya tenemos el numero completo para mostar
                return this.fc_folio;
            else
                //si tenemos folio, lo usamos como nro de venta
                nroVenta = "00000000" + String.valueOf(this.fc_folio);
        }

        return nroVenta.substring(nroVenta.length() - 8);
    }

    public Venta() {
    }

    public Venta(long id, double total, int cantidad, String fecha, String hora, String codigo, int userid, String medio_pago, double _neto, double _iva) {
        this.id = id;
        this.fecha = fecha;
        this.hora = hora;
        this.total = total;
        this.cantidad = cantidad;
        this.codigo = codigo;
        this.userid = userid;
        this.medio_pago = medio_pago;
        this.neto = _neto;
        this.iva = _iva;
    }

    @Override
    public String toString() {
        return "";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel target, int flags) {
        target.writeLong(id);
        target.writeString(fecha);
        target.writeString(hora);
        target.writeDouble(total);
        target.writeInt(cantidad);
        target.writeString(codigo);
        target.writeInt(userid);
        target.writeString(medio_pago);
        target.writeString(fc_type);
        target.writeLong(fc_id);
        target.writeString(fc_folio);
        target.writeString(fc_validation);
        target.writeTypedList(detalles);
        target.writeDouble(neto);
        target.writeDouble(iva);
        target.writeString(sync);
        target.writeString(fc_timbre);
        target.writeInt(anulada ? 1 : 0);
    }

    public static final Creator<Venta> CREATOR = new Creator<Venta>() {
        public Venta createFromParcel(Parcel in) {
            return new Venta(in);
        }

        public Venta[] newArray(int size) {
            return new Venta[size];
        }
    };

    private Venta(Parcel in) {
        this.id = in.readLong();
        this.fecha = in.readString();
        this.hora = in.readString();
        this.total = in.readDouble();
        this.cantidad = in.readInt();
        this.codigo = in.readString();
        this.userid = in.readInt();
        this.medio_pago = in.readString();
        this.fc_type = in.readString();
        this.fc_id = in.readLong();
        this.fc_folio = in.readString();
        this.fc_validation = in.readString();
        in.readTypedList(this.detalles, VentaDetalle.CREATOR);
        this.neto = in.readDouble();
        this.iva = in.readDouble();
        this.sync = in.readString();
        this.fc_timbre = in.readString();
        this.anulada = in.readInt() == 1;
    }

    public String getType(String pais) {
        return "VENTA";
    }

    @Override
    public String getDbType() {
        return "VTA";
    }


    public boolean tieneValeAsociado() {
        return getFc_type().equals(ValeConst.TIPO_DOCUMENTO);
    }
}
