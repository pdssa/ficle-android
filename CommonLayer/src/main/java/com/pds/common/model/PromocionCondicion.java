package com.pds.common.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Hernan on 10/06/2016.
 */
public class PromocionCondicion implements Parcelable {

    protected PromocionCondicion(Parcel in) {
        tipoCondicion = TIPO_CONDICION.fromValue(in.readInt());
        valorAsociado = in.readString();
    }

    public static final Creator<PromocionCondicion> CREATOR = new Creator<PromocionCondicion>() {
        @Override
        public PromocionCondicion createFromParcel(Parcel in) {
            return new PromocionCondicion(in);
        }

        @Override
        public PromocionCondicion[] newArray(int size) {
            return new PromocionCondicion[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(tipoCondicion.ordinal());
        dest.writeString(valorAsociado);
    }

    public enum TIPO_CONDICION{
        PRODUCTO_ESPECIFICO,

        SUBDEPARTAMENTO_ESPECIFICO,

        MARCA_ESPECIFICA;

        public static TIPO_CONDICION fromValue(int idTipoCondicion)
                throws IllegalArgumentException {
            try {
                return TIPO_CONDICION.values()[idTipoCondicion];
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new IllegalArgumentException("Unknown enum value :" + idTipoCondicion);
            }
        }
    }

    private TIPO_CONDICION tipoCondicion;
    private String valorAsociado;

    public PromocionCondicion(TIPO_CONDICION tipoCondicion, String valorAsociado) {
        this.tipoCondicion = tipoCondicion;
        this.valorAsociado = valorAsociado;
    }

    public TIPO_CONDICION getTipoCondicion() {
        return tipoCondicion;
    }

    public void setTipoCondicion(TIPO_CONDICION tipoCondicion) {
        this.tipoCondicion = tipoCondicion;
    }

    public String getValorAsociado() {
        return valorAsociado;
    }

    public void setValorAsociado(String valorAsociado) {
        this.valorAsociado = valorAsociado;
    }

    public boolean checkCondition(Product product){
        switch (tipoCondicion){
            case PRODUCTO_ESPECIFICO:{
                return product.getCode().equalsIgnoreCase(valorAsociado);
            }
            case SUBDEPARTAMENTO_ESPECIFICO:{
                return product.getCodigoPadre().equalsIgnoreCase(valorAsociado);
            }
            case MARCA_ESPECIFICA:{
                return product.getBrand().equalsIgnoreCase(valorAsociado);
            }
        }

        return false;
    }
}
