package com.pds.common.model;

import android.content.ContentResolver;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hernan on 24/02/2015.
 */
public class VentaAnulada implements Parcelable {

    private long id;
    private long id_original;
    private String type_original;
    private double total;
    private int cantidad;
    private String fecha;
    private String hora;
    private String codigo;
    private int userid;
    private String medio_pago;
    private String fc_type;
    private long fc_id;
    private String fc_folio;
    private String fc_validation;
    private String fc_timbre;
    private double neto;
    private double iva;
    private String sync;

    public long getId_original() {
        return id_original;
    }

    public void setId_original(long id_original) {
        this.id_original = id_original;
    }

    public String getType_original() {
        return type_original;
    }

    public void setType_original(String type_original) {
        this.type_original = type_original;
    }

    public double getNeto() {
        return neto;
    }

    public void setNeto(double neto) {
        this.neto = neto;
    }

    public double getIva() {
        return iva;
    }

    public void setIva(double iva) {
        this.iva = iva;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public long getId() {
        return id;
    }

    public double getTotal() {
        return total;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getFecha() {
        return fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getCantidad() {
        return cantidad;
    }

    public int getUserid() {
        return userid;
    }

    public String getMedio_pago() {
        return medio_pago;
    }

    public void setMedio_pago(String medio_pago) {
        this.medio_pago = medio_pago;
    }

    public String getFc_type() {
        return fc_type;
    }

    public void setFc_type(String fc_type) {
        this.fc_type = fc_type;
    }

    public long getFc_id() {
        return fc_id;
    }

    public void setFc_id(long fc_id) {
        this.fc_id = fc_id;
    }

    public String getFc_folio() {
        return fc_folio;
    }

    public void setFc_folio(String fc_folio) {
        this.fc_folio = fc_folio;
    }

    public String getFc_validation() {
        return fc_validation;
    }

    public void setFc_validation(String fc_validation) {
        this.fc_validation = fc_validation;
    }

    public String getFc_Timbre() {
        return fc_timbre;
    }

    public void setFc_Timbre(String timbre) {
        this.fc_timbre = timbre;
    }

    public VentaAnulada() {}

    public VentaAnulada(Venta vta) {
        this.id_original = vta.getId();
        this.type_original = "VTA";
        this.fecha = vta.getFecha();
        this.hora = vta.getHora();
        this.total = vta.getTotal();
        this.cantidad = vta.getCantidad();
        this.codigo = vta.getCodigo();
        this.userid = vta.getUserid();
        this.medio_pago = vta.getMedio_pago();
        this.neto = vta.getNeto();
        this.iva = vta.getIva();
        this.fc_folio = vta.getFc_folio();
        this.fc_id = vta.getFc_id();
        this.fc_timbre = vta.getFc_Timbre();
        this.fc_type = vta.getFc_type();
        this.fc_validation = vta.getFc_validation();
    }

    public VentaAnulada(Comprobante cmp) {
        this.id_original = cmp.getId();
        this.type_original = "CPB";
        this.fecha = cmp.getFecha();
        this.hora = cmp.getHora();
        this.total = cmp.getTotal();
        this.cantidad = cmp.getCantidad();
        this.codigo = cmp.getCodigo();
        this.userid = cmp.getUserid();
        this.medio_pago = cmp.getMedio_pago();
        this.neto = cmp.getNeto();
        this.iva = cmp.getIva();
    }

    @Override
    public String toString() {
        return "";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel target, int flags) {
        target.writeLong(id);
        target.writeString(fecha);
        target.writeString(hora);
        target.writeDouble(total);
        target.writeInt(cantidad);
        target.writeString(codigo);
        target.writeInt(userid);
        target.writeString(medio_pago);
        target.writeString(fc_type);
        target.writeLong(fc_id);
        target.writeString(fc_folio);
        target.writeString(fc_validation);
        target.writeString(fc_timbre);
        target.writeDouble(neto);
        target.writeDouble(iva);
        target.writeString(sync);
    }

    public static final Creator<VentaAnulada> CREATOR = new Creator<VentaAnulada>() {
        public VentaAnulada createFromParcel(Parcel in) {
            return new VentaAnulada(in);
        }

        public VentaAnulada[] newArray(int size) {
            return new VentaAnulada[size];
        }
    };

    private VentaAnulada(Parcel in) {
        this.id = in.readLong();
        this.fecha = in.readString();
        this.hora = in.readString();
        this.total = in.readDouble();
        this.cantidad = in.readInt();
        this.codigo = in.readString();
        this.userid = in.readInt();
        this.medio_pago = in.readString();
        this.fc_type = in.readString();
        this.fc_id = in.readLong();
        this.fc_folio = in.readString();
        this.fc_validation = in.readString();
        this.fc_timbre = in.readString();
        this.neto = in.readDouble();
        this.iva = in.readDouble();
        this.sync = in.readString();
    }

}
