package com.pds.common.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.pds.common.Formatos;

import java.text.ParseException;
import java.util.Date;

/**
 * Created by Hernan on 05/12/13.
 */
public class Compra implements Parcelable {

    private long id;
    private String numero;
    private long providerid;
    private Provider proveedor;
    private Date fecha;
    private double subtotal_exento;
    private double subtotal_neto_grav;
    private double subtotal_impuesto;
    private double total;
    private String codigo;
    private int userid;
    private String medio_pago;

    public void setId(Long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public long getProviderid() {
        return providerid;
    }

    public void setProviderid(long providerid) {
        this.providerid = providerid;
    }

    public Provider getProveedor() {
        return proveedor;
    }

    public void setProveedor(Provider proveedor) {
        this.proveedor = proveedor; this.providerid = proveedor.getId();
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public double getSubtotal_exento() {
        return subtotal_exento;
    }

    public void setSubtotal_exento(double subtotal_exento) {
        this.subtotal_exento = subtotal_exento;
    }

    public double getSubtotal_neto_grav() {
        return subtotal_neto_grav;
    }

    public void setSubtotal_neto_grav(double subtotal_neto_grav) {
        this.subtotal_neto_grav = subtotal_neto_grav;
    }

    public double getSubtotal_impuesto() {
        return subtotal_impuesto;
    }

    public void setSubtotal_impuesto(double subtotal_impuesto) {
        this.subtotal_impuesto = subtotal_impuesto;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getMedio_pago() {
        return medio_pago;
    }

    public void setMedio_pago(String medio_pago) {
        this.medio_pago = medio_pago;
    }

    public Compra() {

    }

    public Compra(long id, String numero, long providerid, Date fecha, double subtotal_exento, double subtotal_neto_grav, double subtotal_impuesto, double total, String codigo, int userid, String medio_pago) {
        this.id = id;
        this.numero = numero;
        this.providerid = providerid;
        this.fecha = fecha;
        this.subtotal_exento = subtotal_exento;
        this.subtotal_neto_grav = subtotal_neto_grav;
        this.subtotal_impuesto = subtotal_impuesto;
        this.total = total;
        this.codigo = codigo;
        this.userid = userid;
        this.medio_pago = medio_pago;
    }

    @Override
    public String toString() {
        return "";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel target, int flags) {
        target.writeLong(id);
        target.writeString(numero);
        target.writeLong(providerid);
        target.writeString(Formatos.DbDate(fecha));
        target.writeDouble(subtotal_exento);
        target.writeDouble(subtotal_neto_grav);
        target.writeDouble(subtotal_impuesto);
        target.writeDouble(total);
        target.writeString(medio_pago);
        target.writeString(codigo);
        target.writeInt(userid);
    }

    public static final Creator<Compra> CREATOR = new Creator<Compra>() {
        public Compra createFromParcel(Parcel in) {
            return new Compra(in);
        }

        public Compra[] newArray(int size) {
            return new Compra[size];
        }
    };

    private Compra(Parcel in) {
        this.id = in.readLong();
        this.numero = in.readString();
        this.providerid = in.readLong();
        try {
            this.fecha = Formatos.DbDate(in.readString());
        } catch (Exception e) {
            Log.e("Compra", "cannot parse fecha", e);
        }
        this.subtotal_exento = in.readDouble();
        this.subtotal_neto_grav = in.readDouble();
        this.subtotal_impuesto = in.readDouble();
        this.total = in.readDouble();
        this.medio_pago = in.readString();
        this.codigo = in.readString();
        this.userid = in.readInt();
    }

}
