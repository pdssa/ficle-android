package com.pds.common.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.pds.common.Formatos;

import org.json.JSONObject;

import java.util.Date;

/**
 * Created by Hernan on 17/12/2014.
 */
public class RangoFolio implements Parcelable {

    private Long id;
    private String tipo_doc;
    private long desde;
    private long hasta;
    private long ultimo;
    private String caf;
    private Date fec_ref;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipo_doc() {
        return tipo_doc;
    }

    public void setTipo_doc(String tipo_doc) {
        this.tipo_doc = tipo_doc;
    }

    public long getDesde() {
        return desde;
    }

    public void setDesde(long desde) {
        this.desde = desde;
    }

    public long getHasta() {
        return hasta;
    }

    public void setHasta(long hasta) {
        this.hasta = hasta;
    }

    public long getUltimo() {
        return ultimo;
    }

    public void setUltimo(long ultimo) {
        this.ultimo = ultimo;
    }

    public String getCaf() {
        return caf;
    }

    public void setCaf(String caf) {
        this.caf = caf;
    }

    public Date getFec_ref() {
        return fec_ref;
    }

    public void setFec_ref(Date fec_ref) {
        this.fec_ref = fec_ref;
    }

    public long getRestantes(){
        if(this.getUltimo() > 0)
            return this.getHasta() - this.getUltimo(); //si ya consumi alguno, vemos los restantes
        else
            return this.getHasta() - this.getDesde() + 1;//si aún no consumi ninguno, vemos los disponibles
    }

    public boolean estaCompleto(){
        return this.getHasta() == this.getUltimo();
    }

    private double getTolerancia(){return 10;}

    public boolean estaPorCompletar(){
        double restantes = getRestantes();

        //si la cantidad de restantes es menor a la tolerancia (supongamos 10%)
        return ((restantes * 100) / this.getHasta()) < getTolerancia();
    }

    public long getProximo(){
        if(this.getUltimo() > 0)
            return this.getUltimo() + 1;
        else
            return this.getDesde();
    }

    public RangoFolio() {

    }

    public RangoFolio(Parcel in) {
        id = in.readLong();
        tipo_doc = in.readString();
        desde = in.readLong();
        hasta = in.readLong();
        ultimo = in.readLong();
        caf = in.readString();
        try {
            fec_ref = Formatos.DbDate(in.readString());
        } catch (Exception e) {
            Log.e("RangoFolio", "cannot parse fecha", e);
        }
    }

    public RangoFolio(JSONObject jsonObject) {
        try {
            tipo_doc = jsonObject.getString("rangosFoliosCodTipoDoc");
            desde = jsonObject.getLong("rangosFoliosDesde");
            hasta = jsonObject.getLong("rangosFoliosHasta");
            caf = jsonObject.getString("rangosFoliosCaf");
            try {
                fec_ref = Formatos.ObtieneDate(jsonObject.getString("rangosFoliosFecha"), Formatos.DbDate_Format);
            } catch (Exception e) {
                Log.e("RangoFolio", "cannot parse fecha", e);
            }
        } catch (Exception ex) {
            Log.e("RangoFolio", "Read from JSON", ex);
        }
    }

    public static final Creator<RangoFolio> CREATOR = new Creator<RangoFolio>() {
        public RangoFolio createFromParcel(Parcel in) {
            return new RangoFolio(in);
        }

        public RangoFolio[] newArray(int size) {
            return new RangoFolio[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel target, int flags) {
        target.writeLong(id);
        target.writeString(tipo_doc);
        target.writeLong(desde);
        target.writeLong(hasta);
        target.writeLong(ultimo);
        target.writeString(caf);
        target.writeString(Formatos.DbDate(fec_ref));
    }


}