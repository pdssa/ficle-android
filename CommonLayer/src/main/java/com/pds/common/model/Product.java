package com.pds.common.model;

import android.content.ContentResolver;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;

import com.pds.common.Formato;
import com.pds.common.Formatos;
import com.pds.common.dao.CategoriaDao;
import com.pds.common.dao.DepartmentDao;
import com.pds.common.dao.SubDepartmentDao;
import com.pds.common.dao.TaxDao;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Product implements Parcelable {

    //flag de automatic, se utiliza:
    public static class TYPE_AUTOM {
        public static final int PROD_ALTA_MANUAL = 0;//PRODUCTO CREADO MANUALMENTE CON SKU
        public static final int PROD_ALTA_AUTOM = 1;//PRODUCTO CREADO AUTOMATICAMENTE VIA CATALOGO
        public static final int PROD_ALTA_SIN_SKU = 2;//PRODUCTO CREADO MANUALMENTE "SIN SKU"
    }

    private long id;
    private String name;
    private String code;
    private long idDepartment;
    private Department department;
    private long idSubdepartment;
    private SubDepartment subDepartment;
    private long idProvider;
    private Provider provider;
    private long idTax;
    private Tax tax;
    private String description;
    private Date alta;
    private double stock;
    private String brand;
    private String presentation;
    private double purchasePrice;
    private double salePrice;
    private double iva;
    private boolean ivaEnabled;
    private double minStock;
    private boolean quickAccess;
    private boolean weighable;
    private String unit;
    private boolean removed;
    private double contenido;
    private boolean generic;
    private String image_uri;
    private String mIspCode;
    private int automatic;
    private String codigo_padre;
    private String type;

    public String getCodigoPadre() {
        return codigo_padre;
    }

    public void setCodigoPadre(String codigo_padre) {
        this.codigo_padre = codigo_padre;
    }

    public int getAutomatic() {
        return automatic;
    }

    public void setAutomatic(int automatic) {
        this.automatic = automatic;
    }

    public String getIspCode() {
        return mIspCode;
    }

    public void setIspCode(String ispCode) {
        this.mIspCode = ispCode;
    }

    public double getContenido() {
        return contenido;
    }


    public String getContenidoFormatted() {
        return Formato.FormateaDecimal(contenido, Formato.Decimal_Format.US);
    }



    public void setContenido(double contenido) {
        this.contenido = contenido;
    }

    public boolean isGeneric() {
        return generic;
    }

    public void setGeneric(boolean generic) {
        this.generic = generic;
    }

    public String getImage_uri() {
        return image_uri;
    }

    public void setImage_uri(String image_uri) {
        this.image_uri = image_uri;
    }

    public String getType() {
        return type == null ? "" : type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Product() {
        alta = new Date();

        idDepartment = -1;
        idSubdepartment = -1;
        idProvider = -1;
        name = "";
        description = "";
        weighable = false;
        brand = "";
        contenido = 0;
        unit = "";
        presentation = "";
        code = "";
        image_uri = "";
        salePrice = 0;
        minStock = 0;
        purchasePrice = 0;
        ivaEnabled = false;
        stock = 0;
        generic = false;
        quickAccess = false;
        tax = null;
        iva = 0;
        automatic = TYPE_AUTOM.PROD_ALTA_MANUAL;
        type = "";
    }

    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getPrintingName() {
        return !TextUtils.isEmpty(this.getDescription()) ? this.getDescription() : this.getName();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public SubDepartment getSubDepartment() {
        return subDepartment;
    }

    public void setSubDepartment(SubDepartment subDepartment) {
        this.subDepartment = subDepartment;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getAlta() {
        return alta;
    }

    public void setAlta(Date alta) {
        this.alta = alta;
    }

    public double getStock() {
        return stock;
    }

    public void setStock(double stock) {
        this.stock = stock;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getPresentation() {
        return presentation;
    }

    public void setPresentation(String presentation) {
        this.presentation = presentation;
    }

    public double getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(double purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public double getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(double salePrice) {
        this.salePrice = salePrice;
    }

    public double getIva() {
        return iva;
    }

    public void setIva(double iva) {
        this.iva = iva;
    }

    public boolean isIvaEnabled() {
        return ivaEnabled;
    }

    public void setIvaEnabled(boolean ivaEnabled) {
        this.ivaEnabled = ivaEnabled;
    }

    public double getMinStock() {
        return minStock;
    }

    public void setMinStock(double minStock) {
        this.minStock = minStock;
    }

    public boolean isQuickAccess() {
        return quickAccess;
    }

    public void setQuickAccess(boolean quickAccess) {
        this.quickAccess = quickAccess;
    }

    public boolean isWeighable() {
        return weighable;
    }

    public void setWeighable(boolean weighable) {
        this.weighable = weighable;
    }

    public Tax getTax() {
        return tax;
    }

    public void setTax(Tax tax) {
        this.tax = tax;
    }

    public String getUnit() {
        return unit == null ? "" : unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setRemoved(boolean removed) {
        this.removed = removed;
    }

    public boolean isRemoved() {
        return this.removed;
    }

    public long getIdDepartment() {
        return idDepartment;
    }

    public void setIdDepartment(long idDepartment) {
        this.idDepartment = idDepartment;
    }

    public long getIdSubdepartment() {
        return idSubdepartment;
    }

    public void setIdSubdepartment(long idSubdepartment) {
        this.idSubdepartment = idSubdepartment;
    }

    public long getIdProvider() {
        return idProvider;
    }

    public void setIdProvider(long idProvider) {
        this.idProvider = idProvider;
    }

    public long getIdTax() {
        return idTax;
    }

    public void setIdTax(long idTax) {
        this.idTax = idTax;
    }

    @Override
    public String toString() {
        if(isWeighable())
            return String.format("%s %s %s", this.brand , this.name, !TextUtils.isEmpty(getUnit()) ? getUnit() : "");
        else
            return String.format("%s %s %s%s", this.brand , this.name, getContenidoFormatted(), this.unit);
    }

    public String buildShortName(){
        //armamos un nombre corto formado por la marca + nombre + contenido + unidad
        String _marca = getBrand().substring(0, Math.min(getBrand().length(), 4)) + " ";
        String _nombre = getName().substring(0, Math.min(getName().length(), 4 + 5 - _marca.length())) + " ";
        String _contenido ;

        if(isWeighable()) {
            _contenido = !TextUtils.isEmpty(getUnit()) ? getUnit() : "";
        }
        else {
            _contenido =
                    getContenidoFormatted().substring(0, Math.min(getContenidoFormatted().length(), 2)) +
                            (!TextUtils.isEmpty(getUnit()) ? getUnit().charAt(0) : "");
        }

        return String.format("%s%s%s", _marca, _nombre, _contenido);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel target, int flags) {
        target.writeLong(id);
        target.writeString(name);
        target.writeString(code);
        target.writeLong(idDepartment);
        target.writeLong(idSubdepartment);
        target.writeLong(idProvider);
        //target.writeParcelable(department, 0);
        //target.writeParcelable(subDepartment, 0);
        //target.writeParcelable(provider, 0);
        target.writeString(description);
        target.writeString(Formatos.DbDateTimeFormat.format(alta));
        target.writeDouble(stock);
        target.writeString(brand);
        target.writeString(presentation);
        target.writeDouble(purchasePrice);
        target.writeDouble(salePrice);
        target.writeDouble(iva);
        target.writeByte((byte) ((ivaEnabled) ? 1 : 0));
        target.writeDouble(minStock);
        target.writeByte((byte) ((quickAccess) ? 1 : 0));
        target.writeByte((byte) ((weighable) ? 1 : 0));
        target.writeLong(idTax);
        //target.writeParcelable(tax, 0);
        target.writeString(unit);
        target.writeDouble(contenido);
        target.writeByte((byte) (generic ? 1 : 0));
        target.writeString(image_uri);
        target.writeString(mIspCode);
        target.writeInt(automatic);
        target.writeString(codigo_padre);
        target.writeString(type);
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    private Product(Parcel in) {
        id = in.readLong();
        name = in.readString();
        code = in.readString();

        idDepartment = in.readLong();
        idSubdepartment = in.readLong();
        idProvider = in.readLong();
        //department = in.readParcelable(Department.class.getClassLoader());
        //subDepartment = in.readParcelable(SubDepartment.class.getClassLoader());
        //provider = in.readParcelable(Provider.class.getClassLoader());
        description = in.readString();
        try {
            alta = Formatos.DbDateTimeFormat.parse(in.readString());
        } catch (ParseException e) {
            Log.e("Product", "cannot parse alta", e);
        }
        stock = in.readDouble();
        brand = in.readString();
        presentation = in.readString();
        purchasePrice = in.readDouble();
        salePrice = in.readDouble();
        iva = in.readDouble();
        ivaEnabled = in.readByte() == 1 ? true : false;
        minStock = in.readDouble();
        quickAccess = in.readByte() == 1 ? true : false;
        weighable = in.readByte() == 1 ? true : false;
        idTax = in.readLong();
        //tax = (Tax) in.readParcelable(Tax.class.getClassLoader());
        unit = in.readString();
        contenido = in.readDouble();
        generic = in.readByte() == 1;
        image_uri = in.readString();
        mIspCode = in.readString();
        automatic = in.readInt();
        codigo_padre = in.readString();
        type = in.readString();
    }

   /* public Product(String fromImport) {
        //TYPEREG;NOMBRE;PESABLE;Marca;Contenido;UNIDAD;PRESENTACION;CODE;IMAGE;PRICE;CODIGO_PADRE;FAST_CODE;NOMBRE_CORTO
        //P;PAPEL HIGIENICO 26 METROS-PREFERIDO-4-Unidades;0;PREFERIDO;4;Unidades;BOLSA;7806500501602;file://mnt/sdcard/Productos/papel.png;100;0208030246;123;aaa
        String[] datos = fromImport.split(";", -1);

        //idDepartment = Integer.parseInt(datos[1]) + deptoIdDelta;
        //idSubdepartment = Integer.parseInt(datos[2]) + subdeptoIdDelta;
        idProvider = -1;
        name = datos[1];
        //description = name.length() > 30 ? name.substring(0, 30) : name;
        //description = ""; //por default, sin nombre corto
        alta = new Date();
        weighable = (TextUtils.isEmpty(datos[2]) ? 0 : Integer.parseInt(datos[2])) == 1;
        brand = datos[3];
        contenido = TextUtils.isEmpty(datos[4]) ? 0 : Double.parseDouble(datos[4]);
        unit = datos[5];
        presentation = datos[6];
        code = datos[7];
        image_uri = datos[8];
        salePrice = TextUtils.isEmpty(datos[9]) ? 0 : Double.parseDouble(datos[9]);
        codigo_padre = datos[10];
        minStock = 0;
        purchasePrice = 0;
        ivaEnabled = true;
        stock = 0;
        generic = false;
        quickAccess = false;
        automatic = TYPE_AUTOM.PROD_ALTA_AUTOM;
        mIspCode = datos.length > 11 ? datos[11] : "";
        description = datos.length > 12 ? datos [12] : "";//en las nuevas versiones de catalogo, enviamos la descripcion


    }*/

  /*  public void ProductoCombo(Combo c, ContentResolver contentResolver){
        name = c.getNombre();
        description = c.getDescripcion();
        salePrice = c.getPrecio();
        code = c.getProductoComboSKU();
        type = "C";
        removed = c.isBaja() ;
        tax = new TaxDao(contentResolver).list("name = 'Gravado'", null, null).get(0);
        iva = tax.getAmount();
    }

    public boolean esProductoCombo(){
        //return code.contains("-");
        return code.matches("\\S*-\\S+");
    }

    public long getProductoComboId(){
        return Long.parseLong(code.split("-")[1]);
    }
*/
    public void LoadDepartmentSubdepartmentTax(ContentResolver contentResolver){
        if(getIdTax() != -1)
            tax = new TaxDao(contentResolver).find(getIdTax());

        department = new DepartmentDao(contentResolver).find(getIdDepartment());
        subDepartment = new SubDepartmentDao(contentResolver).find(getIdSubdepartment());
    }
}
