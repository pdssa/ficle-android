package com.pds.common.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.pds.common.Formatos;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Provider implements Parcelable{

    private static final SimpleDateFormat ALTA_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private long id;
    private String name;
    private Date alta;
    private String description;
    private String contact;
    private long idDepartment;
    private Department department;
    private long idSubdepartment;
    private SubDepartment subDepartment;
    private String email;
    private String phoneNumber;
    private String address;
    private String city;
    private String tax_id; //RUT - CUIT

    public Provider(long id, String name){
        this.id = id;
        this.name = name;
    }

    public Provider() {
    }

    public long getIdDepartment() {
        return idDepartment;
    }

    public void setIdDepartment(long idDepartment) {
        this.idDepartment = idDepartment;
    }

    public long getIdSubdepartment() {
        return idSubdepartment;
    }

    public void setIdSubdepartment(long idSubdepartment) {
        this.idSubdepartment = idSubdepartment;
    }

    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getAlta() {
        return alta;
    }

    public void setAlta(Date alta) {
        this.alta = alta;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public SubDepartment getSubDepartment() {
        return subDepartment;
    }

    public void setSubDepartment(SubDepartment subDepartment) {
        this.subDepartment = subDepartment;
    }

    public String getTax_id() {
        return tax_id;
    }

    public void setTax_id(String tax_id) {
        this.tax_id = tax_id;
    }

    @Override
    public String toString() {
        return this.name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel target, int flags) {
        target.writeLong(id);
        target.writeString(name);
        target.writeString(ALTA_FORMAT.format(alta));
        target.writeString(description);
        target.writeString(contact);
        target.writeParcelable(department, 0);
        target.writeParcelable(subDepartment, 0);
        target.writeString(email);
        target.writeString(phoneNumber);
        target.writeString(address);
        target.writeString(city);
        target.writeString(phoneNumber);
        target.writeString(tax_id);
    }

    public static final Creator<Provider> CREATOR = new Creator<Provider>() {
        public Provider createFromParcel(Parcel in) {
            return new Provider(in);
        }

        public Provider[] newArray(int size) {
            return new Provider[size];
        }
    };

    private Provider(Parcel in) {
        id = in.readLong();
        name = in.readString();
        try {
            alta = ALTA_FORMAT.parse(in.readString());
        } catch (ParseException e) {
            Log.e("Provider", "cannot parse alta", e);
        }
        description = in.readString();
        contact = in.readString();
        department = in.readParcelable(Department.class.getClassLoader());
        subDepartment = in.readParcelable(SubDepartment.class.getClassLoader());
        email = in.readString();
        phoneNumber = in.readString();
        address = in.readString();
        city = in.readString();
        phoneNumber = in.readString();
        tax_id = in.readString();
    }

    public Provider(String fromImport){
        //TYPEREG;RAZON_SOCIAL;TAX_ID;NOMBRE_FANTASIA;MAIL
        //X;DISTRIBUIDORA DEL PACIFICO LTDA.;76462500-5;DIPAC;aa@aa.com
        String[] datos = fromImport.split(";",-1);

        name = datos[1];
        tax_id = datos[2].toUpperCase().trim();
        description = datos[3];
        alta = new Date();
        department = null;
        subDepartment = null;
        email = datos.length > 4 ? datos[4] : ""; //esto es por si no tenemos el mail
    }
}
