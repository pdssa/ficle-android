package com.pds.common.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Hernan on 24/02/2015.
 */
public class VentaAnuladaDetalle implements Parcelable {
    private long id;
    private long idVentaAnulada;
    private long idProducto;
    private double cantidad;
    private double precio;
    private double total;
    private String sku;
    private double neto;
    private double iva;
    private VentaAnulada ventaAnulada;
    private Product producto;

    public double getNeto() {
        return neto;
    }

    public void setNeto(double neto) {
        this.neto = neto;
    }

    public double getIva() {
        return iva;
    }

    public void setIva(double iva) {
        this.iva = iva;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getIdVentaAnulada() {
        return idVentaAnulada;
    }

    public void setIdVentaAnulada(Long idVenta) {
        this.idVentaAnulada = idVenta;
    }

    public long getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Long idProducto) {
        this.idProducto = idProducto;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public VentaAnulada getVentaAnulada() {
        return ventaAnulada;
    }

    public void setVentaAnulada(VentaAnulada ventaAnulada) {
        this.ventaAnulada = ventaAnulada;
    }

    public Product getProducto() {
        return producto;
    }

    public void setProducto(Product producto) {
        this.producto = producto;
    }

    public VentaAnuladaDetalle(){

    }

    public VentaAnuladaDetalle(long _idVentaAnulada, VentaDetalle det){
        this.idVentaAnulada = _idVentaAnulada;
        this.total = det.getTotal();
        this.cantidad = det.getCantidad();
        this.idProducto = det.getIdProducto();
        this.precio = det.getPrecio();
        this.sku = det.getSku();
        this.neto = det.getNeto();
        this.iva = det.getIva();
    }

    public VentaAnuladaDetalle(long _idVentaAnulada, ComprobanteDetalle det){
        this.idVentaAnulada = _idVentaAnulada;
        this.ventaAnulada = null;
        this.total = det.getTotal();
        this.cantidad = det.getCantidad();
        this.idProducto = det.getIdProducto();
        this.precio = det.getPrecio();
        this.sku = det.getSku();
        this.neto = det.getNeto();
        this.iva = det.getIva();
    }

    @Override
    public String toString() {
        return "";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel target, int flags) {
        target.writeLong(id);
        target.writeParcelable(ventaAnulada, 0);
        target.writeLong(idVentaAnulada);
        //target.writeParcelable(producto, 0);
        target.writeLong(idProducto);
        target.writeDouble(cantidad);
        target.writeDouble(precio);
        target.writeDouble(total);
        target.writeString(sku);
        target.writeDouble(neto);
        target.writeDouble(iva);
    }

    public static final Creator<VentaAnuladaDetalle> CREATOR = new Creator<VentaAnuladaDetalle>() {
        public VentaAnuladaDetalle createFromParcel(Parcel in) {
            return new VentaAnuladaDetalle(in);
        }

        public VentaAnuladaDetalle[] newArray(int size) {
            return new VentaAnuladaDetalle[size];
        }
    };

    private VentaAnuladaDetalle(Parcel in) {
        this.id = in.readLong();
        this.ventaAnulada = in.readParcelable(VentaAnulada.class.getClassLoader());
        this.idVentaAnulada = in.readLong();
        //this.producto = in.readParcelable(Product.class.getClassLoader());
        this.idProducto= in.readLong();
        this.cantidad = in.readDouble();
        this.precio = in.readDouble();
        this.total = in.readDouble();
        this.sku = in.readString();
        this.neto = in.readDouble();
        this.iva = in.readDouble();
    }
}
