package com.pds.common.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.pds.common.Formatos;

import java.text.ParseException;
import java.util.Date;

/**
 * Created by Hernan on 10/12/2014.
 */
public class Cierre implements Parcelable {

    private long id;
    private Date fechaCierre;
    private int afectas_cant;
    private double afectas_total;
    private int exentas_cant;
    private double exentas_total;
    private int menores_cant;
    private double menores_total;
    private int otros_cant;
    private double otros_total;
    private double iva;
    private Date fechaDesde;

    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFecha() {
        return fechaCierre;
    }

    public void setFecha(Date fecha) {
        this.fechaCierre = fecha;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fecha) {
        this.fechaDesde = fecha;
    }

    //vamos a reutilizar el campo afectas_cant para contabilizar el total de operaciones
    public int getOperaciones_cant() {
        return afectas_cant;
    }

    public void setOperaciones_cant(int afectas_cant) {
        this.afectas_cant = afectas_cant;
    }

    public int getAfectas_cant() {
        return afectas_cant;
    }

    public void setAfectas_cant(int afectas_cant) {
        this.afectas_cant = afectas_cant;
    }

    public double getAfectas_total() {
        return afectas_total;
    }

    public void setAfectas_total(double afectas_total) {
        this.afectas_total = afectas_total;
    }

    public int getExentas_cant() {
        return exentas_cant;
    }

    public void setExentas_cant(int exentas_cant) {
        this.exentas_cant = exentas_cant;
    }

    public double getExentas_total() {
        return exentas_total;
    }

    public void setExentas_total(double exentas_total) {
        this.exentas_total = exentas_total;
    }

    public int getMenores_cant() {
        return menores_cant;
    }

    public void setMenores_cant(int menores_cant) {
        this.menores_cant = menores_cant;
    }

    public double getMenores_total() {
        return menores_total;
    }

    public void setMenores_total(double menores_total) {
        this.menores_total = menores_total;
    }

    public int getOtros_cant() {
        return otros_cant;
    }

    public void setOtros_cant(int otros_cant) {
        this.otros_cant = otros_cant;
    }

    public double getOtros_total() {
        return otros_total;
    }

    public void setOtros_total(double otros_total) {
        this.otros_total = otros_total;
    }

    public double getIva() {
        return iva;
    }

    public void setIva(double iva) {
        this.iva = iva;
    }



    public Cierre(Date _fechaDesde, Date _fechaCierre) {
        this();
        fechaDesde = _fechaDesde;
        fechaCierre = _fechaCierre;
    }
    public Cierre() {
        afectas_cant = 0;
        afectas_total = 0;
        exentas_cant = 0;
        exentas_total = 0;
        menores_cant = 0;
        menores_total = 0;
        otros_cant = 0;
        otros_total = 0;
        iva = 0;
    }

    public Cierre(Parcel in) {
        id = in.readLong();
        try {
            fechaCierre = Formatos.ObtieneDate(in.readString(), Formatos.DbDateTimeFormat);
        } catch (Exception e) {
            Log.e("Cierre", "cannot parse fechaCierre", e);
        }
        afectas_cant = in.readInt();
        afectas_total = in.readDouble();
        exentas_cant = in.readInt();
        exentas_total = in.readDouble();
        menores_cant = in.readInt();
        menores_total = in.readDouble();
        otros_cant = in.readInt();
        otros_total = in.readDouble();
        iva = in.readDouble();
        try {
            fechaDesde = Formatos.ObtieneDate(in.readString(), Formatos.DbDateTimeFormat);
        } catch (Exception e) {
            Log.e("Cierre", "cannot parse fechaDesde", e);
        }
    }

    public static final Creator<Cierre> CREATOR = new Creator<Cierre>() {
        public Cierre createFromParcel(Parcel in) {
            return new Cierre(in);
        }

        public Cierre[] newArray(int size) {
            return new Cierre[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel target, int flags) {
        target.writeLong(id);
        target.writeString(Formatos.FormateaDate(fechaCierre, Formatos.DbDateTimeFormat));
        target.writeInt(afectas_cant);
        target.writeDouble(afectas_total);
        target.writeInt(exentas_cant);
        target.writeDouble(exentas_total);
        target.writeInt(menores_cant);
        target.writeDouble(menores_total);
        target.writeInt(otros_cant);
        target.writeDouble(otros_total);
        target.writeDouble(iva);
        target.writeString(Formatos.FormateaDate(fechaDesde, Formatos.DbDateTimeFormat));
    }
}
