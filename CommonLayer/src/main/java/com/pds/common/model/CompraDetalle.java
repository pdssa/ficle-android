package com.pds.common.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Hernan on 05/12/13.
 */
public class CompraDetalle implements Parcelable {

    private long id;
    private long idCompra;
    private long idProducto;
    private int cantidad;
    private double precio;
    private double subtotal_exento;
    private double subtotal_neto_grav;
    private double subtotal_impuesto;
    private double total;
    private double precio_vta; //no va a la tabla este dato

    private Compra compra;
    private Product producto;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(long idProducto) {
        this.idProducto = idProducto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Product getProducto() {
        return producto;
    }

    public void setProducto(Product producto) {
        this.producto = producto;
    }

    public long getIdCompra() {
        return idCompra;
    }

    public void setIdCompra(long idCompra) {
        this.idCompra = idCompra;
    }

    public double getSubtotal_exento() {
        return subtotal_exento;
    }

    public void setSubtotal_exento(double subtotal_exento) {
        this.subtotal_exento = subtotal_exento;
    }

    public double getSubtotal_neto_grav() {
        return subtotal_neto_grav;
    }

    public void setSubtotal_neto_grav(double subtotal_neto_grav) {
        this.subtotal_neto_grav = subtotal_neto_grav;
    }

    public double getSubtotal_impuesto() {
        return subtotal_impuesto;
    }

    public void setSubtotal_impuesto(double subtotal_impuesto) {
        this.subtotal_impuesto = subtotal_impuesto;
    }

    public Compra getCompra() {
        return compra;
    }

    public void setCompra(Compra compra) {
        this.compra = compra;
    }

    public double getPrecio_vta() {
        return precio_vta;
    }

    public void setPrecio_vta(double precio_vta) {
        this.precio_vta = precio_vta;
    }

    public CompraDetalle(){

    }
    public CompraDetalle(int id, double precio, int cantidad, double subtotal_exento, double subtotal_neto_grav, double subtotal_impuesto, double total, long compraid, long productoid ){
        this.id = id;
        this.idCompra= compraid;
        this.idProducto = productoid;
        this.cantidad = cantidad;
        this.precio = precio;
        this.subtotal_exento = subtotal_exento;
        this.subtotal_neto_grav = subtotal_neto_grav;
        this.subtotal_impuesto = subtotal_impuesto;
        this.total = total;
    }



    @Override
    public String toString() {
        return "";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel target, int flags) {
        target.writeLong(id);
        target.writeLong(idCompra);
        target.writeParcelable(compra, 0);
        target.writeLong(idProducto);
        target.writeParcelable(producto, 0);
        target.writeInt(cantidad);
        target.writeDouble(precio);
        target.writeDouble(subtotal_exento);
        target.writeDouble(subtotal_neto_grav);
        target.writeDouble(subtotal_impuesto);
        target.writeDouble(total);
    }

    public static final Creator<CompraDetalle> CREATOR = new Creator<CompraDetalle>() {
        public CompraDetalle createFromParcel(Parcel in) {
            return new CompraDetalle(in);
        }

        public CompraDetalle[] newArray(int size) {
            return new CompraDetalle[size];
        }
    };

    private CompraDetalle(Parcel in) {
        this.id = in.readLong();
        this.idCompra = in.readLong();
        this.compra = in.readParcelable(Compra.class.getClassLoader());
        this.idProducto = in.readLong();
        this.producto = in.readParcelable(Product.class.getClassLoader());
        this.cantidad = in.readInt();
        this.precio = in.readDouble();
        this.subtotal_exento = in.readDouble();
        this.subtotal_neto_grav = in.readDouble();
        this.subtotal_impuesto = in.readDouble();
        this.total = in.readDouble();
    }
}
