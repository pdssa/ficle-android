package com.pds.common.model;

/**
 * Created by Hernan on 12/12/2014.
 */
public abstract class VentaDetalleAbstract {
    public abstract double getNeto();

    public abstract double getIva();

    public abstract String getSku();

    public abstract long getId();

    public abstract int getIdProducto();

    public abstract double getCantidad();

    public abstract double getPrecio();

    public abstract double getTotal();

    public abstract Product getProducto();

    public String getDescripcion() {
        return getProducto() != null ? getProducto().getPrintingName() : "GENERICO";
    }

    public String getCode() {
        return getProducto() != null ? getProducto().getCode() : "0";
    }

    public abstract boolean esItemPesable();

    public abstract boolean esItemGenerico();

    public abstract void setProducto(Product producto);

    public abstract void setIdProducto(int idProducto);


}