package com.pds.common.model;

import android.content.ContentResolver;

import com.pds.common.Formatos;
import com.pds.common.dao.ComprobanteDao;
import com.pds.common.dao.VentaDao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by Hernan on 01/09/2014.
 */
public abstract class VentaAbstract implements Comparable<VentaAbstract> {
    public abstract long getId();

    public abstract double getTotal();

    public abstract String getCodigo();

    public abstract String getFecha();

    public abstract String getHora();

    public abstract String getFechaHoraCorta();

    public abstract int getCantidad();

    public abstract int getUserid();

    public abstract String getMedio_pago();

    public abstract String getNroVenta();

    public abstract String getType(String pais);

    public abstract String getDbType();

    public abstract boolean isAnulada();

    public abstract List<VentaDetalleAbstract> getDetallesAbstract() ;

    public abstract void addDetalles(ContentResolver contentResolver) ;

    private Cliente _cliente;

    public void setCliente(Cliente cliente){
        _cliente = cliente;
    };

    public Cliente getCliente(){
        return _cliente != null ? _cliente : new Cliente();
    };

    public Date getFechaHora_Date(){
        String fecha_String = getFecha();
        String hora_String = getHora();

        try {
            return Formatos.ObtieneDate(fecha_String + " " +  hora_String, Formatos.PDVDateTimeFormat);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Date();
    }

    /*public int compareTo(Object obj) {
        Date d1 = this.getFechaHora_Date();
        Date d2 = ((VentaAbstract) obj).getFechaHora_Date();

        //an int < 0 if d1 is less than the d2, 0 if they are equal, and an int > 0 if d1 is greater.
        return (-1 * d1.compareTo(d2)); //multiplicamos por -1 para invertir el resultado del orden (descendiente)

    }*/

    @Override
    public int compareTo(VentaAbstract vta) {
        Date d1 = this.getFechaHora_Date();
        Date d2 = vta.getFechaHora_Date();

        //an int < 0 if d1 is less than the d2, 0 if they are equal, and an int > 0 if d1 is greater.
        return (-1 * d1.compareTo(d2)); //multiplicamos por -1 para invertir el resultado del orden (descendiente)
    }

    public abstract double getVuelto();

    public static List<VentaAbstract> ventasList(ContentResolver cr, String selection, String[] selectionArgs, String sortOrder, String groupBy, String having, int limit){
        String _limit = limit > 0 ? String.valueOf(limit) : null;

        List<Venta> ventas = new VentaDao(cr).list(selection, selectionArgs, sortOrder, groupBy, having, _limit);
        List<Comprobante> comprobantes = new ComprobanteDao(cr).list(selection, selectionArgs, sortOrder, groupBy, having, _limit);

        List<VentaAbstract> ventasList = new ArrayList<VentaAbstract>();
        ventasList.addAll(ventas);
        ventasList.addAll(comprobantes);

        Collections.sort(ventasList);//ordenamos vtas desc (en vtaabstract está el comparator)

        if(limit > 0 && ventasList.size() > limit)
            return ventasList.subList(0, limit);
        else
            return ventasList;
    }

}
