package com.pds.common.model;

import android.content.ContentResolver;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.pds.common.Formatos;
import com.pds.common.dao.CuentaCteTotalesDao;

import java.text.ParseException;
import java.util.Date;

public class Cliente implements Parcelable{

    private long id;
    private String nombre;
    private String apellido;
    private String telefono;
    private Double limite;
    private String observacion;
    private int baja;
    private String pathPhoto;
    private String claveFiscal;
    private String sync;

    public Cliente(){
    }

    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Double getLimite() {
        return limite;
    }

    public void setLimite(Double limite) {
        this.limite = limite;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public int getBaja() {
        return baja;
    }

    public void setBaja(int baja) {
        this.baja = baja;
    }

    public String getPathPhoto() {
        return pathPhoto;
    }

    public void setPathPhoto(String pathPhoto) {
        this.pathPhoto = pathPhoto;
    }

    public String getClaveFiscal() {
        return claveFiscal;
    }

    public void setClaveFiscal(String claveFiscal) {
        this.claveFiscal = claveFiscal;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }

    @Override
    public String toString() {
        if(!this.apellido.equals(""))
            return (this.apellido + ", " + this.nombre).toUpperCase();
        else
            return this.nombre.toUpperCase();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel target, int flags) {
        target.writeLong(id);
        target.writeString(nombre);
        target.writeString(apellido);
        target.writeString(telefono);
        target.writeString(observacion);
        target.writeDouble(limite);
        target.writeInt(baja);
        target.writeString(pathPhoto);
        target.writeString(claveFiscal);
        target.writeString(sync);
    }

    public static final Creator<Cliente> CREATOR = new Creator<Cliente>() {
        public Cliente createFromParcel(Parcel in) {
            return new Cliente(in);
        }

        public Cliente[] newArray(int size) {
            return new Cliente[size];
        }
    };

    private Cliente(Parcel in) {
        id = in.readLong();
        nombre = in.readString();
        apellido = in.readString();
        telefono = in.readString();
        observacion = in.readString();
        limite = in.readDouble();
        baja = in.readInt();
        pathPhoto = in.readString();
        claveFiscal = in.readString();
        sync = in.readString();
    }

    public boolean tieneSaldoDisponible(double monto, ContentResolver contentResolver){
        //info del cliente : VALORES DEFAULT
        double disponible = getLimite(); //con limite disponible

        CuentaCteTotal _total = new CuentaCteTotalesDao(contentResolver).find(getId());
        if (_total != null) { //si hay registros del cliente
            disponible = _total.getDisponible();
        }

        return monto <= disponible;
    }

}
