package com.pds.common.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;

import com.pds.common.Formatos;

import org.w3c.dom.Text;

import java.util.Date;

/**
 * Created by EMEKA on 13/01/2015.
 */
public class Pedido implements Parcelable {

    private long id;
    private long providerid;
    private String estado;
    private Date fecha;
    private int userid;
    private String sync;
    private Provider proveedor;

    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getProviderid() {
        return providerid;
    }

    public void setProviderid(long providerid) {
        this.providerid = providerid;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEstadoDescripcion() {
        if (estado.startsWith("A"))
            return "ABIERTO";
        else if (estado.startsWith("E"))
            return "ENVIADO";
        else if (estado.startsWith("C"))
            return "CERRADO";
        else
            return "";
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }

    public Pedido() {

    }

    public Provider getProveedor() {
        return proveedor;
    }

    public void setProveedor(Provider proveedor) {
        this.proveedor = proveedor;
        this.providerid = proveedor.getId();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel target, int flags) {
        target.writeLong(id);
        target.writeLong(providerid);
        target.writeString(estado);

        target.writeString(Formatos.DbDate(fecha));

/*        if (fecha != null)
            target.writeString(Formatos.DbDate(fecha));
        else
            target.writeString("");*/

        target.writeInt(userid);
        target.writeString(sync);
    }

    public static final Creator<Pedido> CREATOR = new Creator<Pedido>() {
        public Pedido createFromParcel(Parcel in) {
            return new Pedido(in);
        }

        public Pedido[] newArray(int size) {
            return new Pedido[size];
        }
    };

    private Pedido(Parcel in) {
        this.id = in.readLong();
        this.providerid = in.readLong();
        this.estado = in.readString();
        String _fecha = in.readString();
        /*if (!TextUtils.isEmpty(_fecha)) {
            try {
                this.fecha = Formatos.DbDate(_fecha);
            } catch (Exception e) {
                Log.e("Pedido", "cannot parse fecha", e);
            }
        }*/
        try {
            this.fecha = Formatos.DbDate(_fecha);
        } catch (Exception e) {
            Log.e("Pedido", "cannot parse fecha", e);
        }
        this.userid = in.readInt();
        this.sync = in.readString();
    }
}
