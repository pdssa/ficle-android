package com.pds.common.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.pds.common.Formatos;

import java.text.ParseException;
import java.util.Date;

public class CuentaCteTotal implements Parcelable{

    private Cliente cliente;
    private Double saldo;
    private Double monto_ult_pago;
    private Date fecha_ult_pago;

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public Double getMonto_ult_pago() {
        return monto_ult_pago;
    }

    public void setMonto_ult_pago(Double monto_ult_pago) {
        this.monto_ult_pago = monto_ult_pago;
    }

    public Date getFecha_ult_pago() {
        return fecha_ult_pago;
    }

    public void setFecha_ult_pago(Date fecha_ult_pago) {
        this.fecha_ult_pago = fecha_ult_pago;
    }

    public Double getDisponible(){return this.cliente.getLimite() - this.saldo;}

    public CuentaCteTotal(){
    }

    @Override
    public String toString() {
        return "";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel target, int flags) {
        target.writeParcelable(cliente, 0);
        target.writeDouble(saldo);
        target.writeString(Formatos.DbDateTimeFormat.format(fecha_ult_pago));
        target.writeDouble(monto_ult_pago);
    }

    public static final Creator<CuentaCteTotal> CREATOR = new Creator<CuentaCteTotal>() {
        public CuentaCteTotal createFromParcel(Parcel in) {
            return new CuentaCteTotal(in);
        }

        public CuentaCteTotal[] newArray(int size) {
            return new CuentaCteTotal[size];
        }
    };

    private CuentaCteTotal(Parcel in) {
        cliente = in.readParcelable(Cliente.class.getClassLoader());
        saldo = in.readDouble();
        try {
            fecha_ult_pago = Formatos.DbDateTimeFormat.parse(in.readString());
        } catch (ParseException e) {
            Log.e("CuentaCteTotal", "cannot parse fecha_ult_pago", e);
        }
        monto_ult_pago = in.readDouble();
    }
}
