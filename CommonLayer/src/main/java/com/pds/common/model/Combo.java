package com.pds.common.model;

import android.content.ContentResolver;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.pds.common.Formato;
import com.pds.common.Formatos;
import com.pds.common.dao.ComboItemDao;
import com.pds.common.dao.ProductDao;
import com.pds.common.dao.PromocionDao;

import java.util.Date;
import java.util.List;

/**
 * Created by Hernan on 25/02/2015.
 */
public class Combo implements Parcelable {

    private long id;
    private String nombre;
    private String descripcion;
    private long producto_ppal_id;
    private String producto_ppal_code;
    private double cantidad;
    private double precio;
    private String codigo;
    private Date fecha_alta;
    private boolean baja;
    private String sync;
    private long promocion_id;

    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public long getProducto_ppal_id() {
        return producto_ppal_id;
    }

    public void setProducto_ppal_id(long producto_ppal_id) {
        this.producto_ppal_id = producto_ppal_id;
    }

    public String getProducto_ppal_code() {
        return producto_ppal_code;
    }

    public void setProducto_ppal_code(String producto_ppal_code) {
        this.producto_ppal_code = producto_ppal_code;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Date getFecha_alta() {
        return fecha_alta;
    }

    public void setFecha_alta(Date fecha_alta) {
        this.fecha_alta = fecha_alta;
    }

    public boolean isBaja() {
        return baja;
    }

    public void setBaja(boolean baja) {
        this.baja = baja;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }

    public Combo() { }

    @Override
    public String toString() {
        return "";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel target, int flags) {
        target.writeLong(id);
        target.writeString(nombre);
        target.writeString(descripcion);
        target.writeLong(producto_ppal_id);
        target.writeString(producto_ppal_code);
        target.writeDouble(cantidad);
        target.writeDouble(precio);
        target.writeString(codigo);
        target.writeString(Formatos.DbDate(fecha_alta));
        target.writeInt(baja ? 1 : 0);
        target.writeString(sync);
        target.writeLong(promocion_id);
    }

    public static final Creator<Combo> CREATOR = new Creator<Combo>() {
        public Combo createFromParcel(Parcel in) {
            return new Combo(in);
        }

        public Combo[] newArray(int size) {
            return new Combo[size];
        }
    };

    private Combo(Parcel in) {
        this.id = in.readLong();
        this.nombre = in.readString();
        this.descripcion = in.readString();
        this.producto_ppal_id = in.readLong();
        this.producto_ppal_code = in.readString();
        this.cantidad = in.readDouble();
        this.precio = in.readDouble();
        this.codigo = in.readString();

        try {
            this.fecha_alta = Formatos.DbDate(in.readString());
        } catch (Exception e) {
            Log.e("Combo", "cannot parse fecha", e);
        }

        this.baja = in.readInt() == 1;
        this.sync = in.readString();
        this.promocion_id = in.readLong();

    }

    public Product getProductoPrincipal(ContentResolver contentResolver) {
        return new ProductDao(contentResolver).find(this.producto_ppal_id);
    }

    public List<ComboItem> getItemsCombo(ContentResolver contentResolver) {
        return new ComboItemDao(contentResolver).list(String.format("combo_id = %d", this.id), null, null);
    }

    public Product getProductoCombo(ContentResolver contentResolver) {
        return new ProductDao(contentResolver).first("code = '" + getProductoComboSKU() + "'", null, null);
    }

    public String getProductoComboSKU(){
        //SKUPPAL-IDCOMBO
        return this.producto_ppal_code + "-" + String.valueOf(this.id);
    }

    public long getPromocion_id() {
        return promocion_id;
    }

    public void setPromocion_id(long promocion_id) {
        this.promocion_id = promocion_id;
    }

    public boolean isComboPromocion(){
        return this.promocion_id != 0;
    }

    public double getPrecio(ContentResolver contentResolver) {
        if(isComboPromocion()){
            return calcularPrecioVentaPromocion(contentResolver);
        }

        return precio;
    }

    public double getPrecioPromocionalForzado(ContentResolver contentResolver) {
        if(isComboPromocion()){
            return calcularPrecioVentaPromocionForzado(contentResolver);
        }

        return precio;
    }

    private double calcularPrecioVentaPromocion(ContentResolver contentResolver){

        Promocion p = new PromocionDao(contentResolver).find(this.promocion_id);

        return p.calcularPrecioVenta(contentResolver, this);

    }

    private double calcularPrecioVentaPromocionForzado(ContentResolver contentResolver){

        Promocion p = new PromocionDao(contentResolver).find(this.promocion_id);

        return p.calcularPrecioVenta(contentResolver, this, true);

    }
}

