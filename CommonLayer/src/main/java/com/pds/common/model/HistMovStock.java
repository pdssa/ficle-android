package com.pds.common.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.pds.common.Formatos;

import java.util.Date;

/**
 * Created by EMEKA on 21/01/2015.
 */
public class HistMovStock implements Parcelable{

    private long id;
    private long productoid;
    private int cantidad;
    private double monto;
    private String operacion;
    private int stock;
    private String observacion;
    private Date fecha;
    private String usuario;

    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getProductoid() {
        return productoid;
    }

    public void setProductoid(long productoid) {
        this.productoid = productoid;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public HistMovStock() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel target, int flags) {
        target.writeLong(id);
        target.writeLong(productoid);
        target.writeInt(cantidad);
        target.writeDouble(monto);
        target.writeString(operacion);
        target.writeInt(stock);
        target.writeString(observacion);
        target.writeString(Formatos.DbDate(fecha));
        target.writeString(usuario);
    }

    public static final Creator<HistMovStock> CREATOR = new Creator<HistMovStock>() {
        public HistMovStock createFromParcel(Parcel in) {
            return new HistMovStock(in);
        }

        public HistMovStock[] newArray(int size) {
            return new HistMovStock[size];
        }
    };

    public HistMovStock(Parcel in) {
        this.id = in.readLong();
        this.productoid = in.readLong();
        this.cantidad = in.readInt();
        this.monto = in.readDouble();
        this.operacion = in.readString();
        this.stock = in.readInt();
        this.observacion = in.readString();
        try {
            this.fecha = Formatos.DbDate(in.readString());
        } catch (Exception e) {
            Log.e("HistMovStock", "cannot parse fecha", e);
        }
        this.usuario = in.readString();
    }
}
