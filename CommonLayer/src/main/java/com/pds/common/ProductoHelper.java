package com.pds.common;

/**
 * Created by Hernan on 30/10/13.
 */
public class ProductoHelper {

    //************TABLE PRODUCTO***************
    public static final String TABLE_NAME = "productos";
    //columns names
    public static final String PRODUCTO_ID = "_id";
    public static final String PRODUCTO_CATEGORIA = "categoria";
    public static final String PRODUCTO_NOMBRE = "nombre";
    public static final String PRODUCTO_PRECIO = "precio";
    public static final String PRODUCTO_DESCRIPCION = "descripcion";
    public static final String PRODUCTO_IMAGEN = "imagen";
    public static final String PRODUCTO_CODIGO = "codigo";

    //columns indexes
    public static final int PRODUCTO_IX_ID = 0;
    public static final int PRODUCTO_IX_CATEGORIA = 1;
    public static final int PRODUCTO_IX_NOMBRE = 2;
    public static final int PRODUCTO_IX_PRECIO = 3;
    public static final int PRODUCTO_IX_DESCRIPCION = 4;
    public static final int PRODUCTO_IX_IMAGEN = 5;
    public static final int PRODUCTO_IX_CODIGO = 6;

    // script de creacion de tabla
    public static final String PRODUCTO_CREATE_SCRIPT = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
            PRODUCTO_ID + " integer primary key autoincrement, " +
            PRODUCTO_CATEGORIA + " integer not null, " +
            PRODUCTO_NOMBRE + " text, " +
            PRODUCTO_PRECIO + " real, " +
            PRODUCTO_DESCRIPCION + " text, " +
            PRODUCTO_IMAGEN + " text, " +
            PRODUCTO_CODIGO + " text " +
            ");";

    private static final String PRODUCTO_INSERT_SCRIPT =  "INSERT INTO " + TABLE_NAME +
            "(" + PRODUCTO_CATEGORIA + "," + PRODUCTO_NOMBRE + "," + PRODUCTO_PRECIO + "," +
    PRODUCTO_DESCRIPCION + "," + PRODUCTO_IMAGEN + "," + PRODUCTO_CODIGO + ") ";
/*
    public static final String PRODUCTO_GENERICO_ALTA_SCRIPT = PRODUCTO_INSERT_SCRIPT +
             "VALUES (1,'GENERICO',0,'PRODUCTO GENERICO','ic_launcher','000'); " ;

    public static final String PRODUCTOS_INIT_ALTA_SCRIPT = PRODUCTO_INSERT_SCRIPT +
            "SELECT " + "1,'Alfajor-Terrabusi',6,'Alfajor-Terrabusi','" + "alfajor_terrabusi" + "','P0' " +
            "UNION SELECT " + "1,'Beldent',6,'Beldent','" + "beldent" + "','P1' " +
            "UNION SELECT " + "1,'Bonobon',2,'Bonobon','"+ "bonobon" +"','P2' " +
            "UNION SELECT " + "1,'Cabsha',2,'Cabsha','"+ "cabsha" +"','P3' " +
            "UNION SELECT " + "1,'Club Social',6,'Club Social','"+ "club_social" +"','P4' " +
            "UNION SELECT " + "1,'Cofler block',4.5,'Cofler block','"+ "cofler_block" +"','P5' " +
            "UNION SELECT " + "1,'Chicles-Extra',10,'Chicles-Extra','"+ "extra_chicles" +"','P6' " +
            "UNION SELECT " + "1,'Frutigran',15,'Frutigran','"+ "frutigran" +"','P7' " +
            "UNION SELECT " + "1,'Halls',3,'Halls','"+ "halls" +"','P8' " +
            "UNION SELECT " + "1,'Alfajor-Jorgito',6,'Alfajor-Jorgito','"+ "jorgito" +"','P9' " +
            "UNION SELECT " + "1,'Kinder Bueno',8,'Kinder Bueno','"+ "kinder_bueno" +"','P10' " +
            "UNION SELECT " + "1,'Malboro',12,'Malboro','"+ "malboro" +"','P11' " +
            "UNION SELECT " + "1,'Mantecol',15,'Mantecol','"+ "mantecol" +"','P12' " +
            "UNION SELECT " + "1,'Marroc',4,'Marroc','"+ "marroc" +"','P13' " +
            "UNION SELECT " + "1,'Nerds',9.5,'Nerds','"+ "nerds" +"','P14' " +
            "UNION SELECT " + "1,'Nugaton',6.5,'Nugaton','"+ "nugaton" +"','P 15' " +
            "UNION SELECT " + "1,'Oreo',13,'Oreo','"+ "oreo" +"','P16' " +
            "UNION SELECT " + "1,'Rhodesia',4.5,'Rhodesia','"+ "rhodesia" +"','P17' " +
            "UNION SELECT " + "1,'Sugus-Confitados',7.5,'Sugus-Confitados','"+ "sugus_confitados" +"','P18' " +
            "UNION SELECT " + "1,'Tic-tac',8,'Tic-tac','"+ "tictac" +"','P19' ; " ;*/

    public static final String[] columnas = new String[]{
            PRODUCTO_ID,
            PRODUCTO_CATEGORIA,
            PRODUCTO_NOMBRE,
            PRODUCTO_PRECIO,
            PRODUCTO_DESCRIPCION,
            PRODUCTO_IMAGEN,
            PRODUCTO_CODIGO
    };


}
