package com.pds.common.integration.android;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by Hernan on 12/02/14.
 */
public class ScanIntegrator {
    public static final int ACTIVITY_RESULT_QR_DRDROID = 0;

    //Accion de scan
    public static final String SCAN = "la.droid.qr.scan";

    //Parametros
    //SCAN / DECODE
    //public static final String COMPLETE = "la.droid.qr.complete"; //Default: false

    //Resultado
    public static final String RESULT = "la.droid.qr.result";

    //actividad que llama
    private final Activity activity;

    public ScanIntegrator(Activity activity){
        this.activity = activity;
    }

    public final void initiateScan() {

        //Crea un nuevo Intent para llamar a QR Droid
        Intent qrDroid = new Intent( SCAN ); //Set action "la.droid.qr.scan"

        //qrDroid.putExtra( COMPLETE , true); //por si se quiere recibir full info de un QR Code

        try {
            this.activity.startActivityForResult(qrDroid, ACTIVITY_RESULT_QR_DRDROID);
        }
        catch (ActivityNotFoundException activity)
        {
            //avisamos que no está instalado el complemento
            Toast.makeText(this.activity, "No es posible utilizar el SCAN: Complemento QR Droid no encontrado", Toast.LENGTH_LONG).show();
        }


    }

    public static String parseActivityResult(int requestCode, int resultCode, Intent intent) {

        if( ScanIntegrator.ACTIVITY_RESULT_QR_DRDROID == requestCode && intent != null && intent.getExtras()!=null ) {
            //Leemos el resultado de QR Droid (almacenado en la.droid.qr.result)
            String result = intent.getExtras().getString(RESULT);

            return result;
        }
        else
            return null;
    }
}
