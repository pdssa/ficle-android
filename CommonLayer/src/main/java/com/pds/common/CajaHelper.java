package com.pds.common;

/**
 * Created by Hernan on 03/02/14.
 */
public class CajaHelper {

    //************TABLE CAJA***************
    public static final String TABLE_NAME = "caja";
    public static final String VIEW_NAME = "vw_caja_movs";

    //columns names
    public static final String CAJA_ID = "_id";
    public static final String CAJA_TIPO_MOV = "tipoMov";
    public static final String CAJA_FECHA = "fecha";
    public static final String CAJA_HORA = "hora";
    public static final String CAJA_MONTO = "monto";
    public static final String CAJA_VENTA_ID = "idVenta";
    public static final String CAJA_OBSERVACION = "observacion";
    public static final String CAJA_MEDIO_PAGO = "medioPago";
    public static final String CAJA_ACUMULADO = "acumulado";
    public static final String CAJA_VENTA_DESC = "descVenta";
    public static final String CAJA_TIPO_VTA = "tipoVta";
    public static final String CAJA_SYNC_STATUS = "sync";

    //columns indexes
    public static final int CAJA_IX_ID = 0;
    public static final int CAJA_IX_TIPO_MOV = 1;
    public static final int CAJA_IX_FECHA = 2;
    public static final int CAJA_IX_HORA = 3;
    public static final int CAJA_IX_MONTO = 4;
    public static final int CAJA_IX_VENTA_ID = 5;
    public static final int CAJA_IX_OBSERVACION = 6;
    public static final int CAJA_IX_MEDIO_PAGO = 7;
    public static final int CAJA_IX_TIPO_VTA = 8;
    public static final int CAJA_IX_SYNC_STATUS = 9;
    public static final int CAJA_IX_VENTA_DESC = 10; //solo en VIEW
    public static final int CAJA_IX_ACUMULADO = 11; //solo en VIEW


    // script de creacion de tabla
    public static final String CAJA_CREATE_SCRIPT = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
            CAJA_ID + " integer primary key autoincrement, " +
            CAJA_TIPO_MOV + " integer not null, " +
            CAJA_FECHA + " text, " +
            CAJA_HORA + " text, " +
            CAJA_MONTO + " real, " +
            CAJA_VENTA_ID + " integer, " +
            CAJA_OBSERVACION + " text, " +
            CAJA_MEDIO_PAGO + " text, " +
            CAJA_TIPO_VTA + " text default 'VTA', " +
            CAJA_SYNC_STATUS + " text default 'N'" +
            ");";

    //creamos la vista de los movimientos
    public static final String CAJA_VIEW_CREATE_SCRIPT = "CREATE VIEW IF NOT EXISTS " + VIEW_NAME + " AS " +
            "SELECT "+TABLE_NAME+".*, " + VentasHelper.TABLE_NAME + "." + VentasHelper.VENTA_CODIGO + " AS " + CAJA_VENTA_DESC + ", " +
            "(SELECT IFNULL(SUM(CASE " + CAJA_TIPO_MOV + " WHEN 1 THEN " + CAJA_MONTO + " * -1 ELSE " + CAJA_MONTO + " END),0) FROM " + TABLE_NAME + " AS c2 WHERE c2." + CAJA_ID + " <= " + TABLE_NAME + "." + CAJA_ID +
            ") AS " + CAJA_ACUMULADO +
            " FROM " + TABLE_NAME +
            " LEFT OUTER JOIN " + VentasHelper.TABLE_NAME + " ON " + TABLE_NAME + "." +  CAJA_VENTA_ID + "=" + VentasHelper.TABLE_NAME + "." + VentasHelper.VENTA_ID;

    //script de alter
    public static final String ALTER_SYNC_STATUS_SCRIPT = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + CAJA_TIPO_VTA + " text default 'VTA'" ;
    public static final String ALTER_SYNC_STATUS_SCRIPT_2 = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + CAJA_SYNC_STATUS + " text default 'N'" ;
    public static final String ALTER_SYNC_STATUS_SCRIPT_3 = "UPDATE " + TABLE_NAME + " SET " + CAJA_TIPO_VTA + " = 'VTA', " + CAJA_SYNC_STATUS + " = 'N'" ;

    public static final String[] columnas = new String[]{
            CAJA_ID,
            CAJA_TIPO_MOV,
            CAJA_FECHA,
            CAJA_HORA,
            CAJA_MONTO,
            CAJA_VENTA_ID,
            CAJA_OBSERVACION,
            CAJA_MEDIO_PAGO,
            CAJA_TIPO_VTA,
            CAJA_SYNC_STATUS,
            CAJA_VENTA_DESC,
            CAJA_ACUMULADO
    };


}
