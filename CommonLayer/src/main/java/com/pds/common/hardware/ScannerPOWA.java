package com.pds.common.hardware;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.Toast;

//NEW SDK IMPORTS
//import com.mpowa.android.sdk.common.base.PowaEnums;
import com.mpowa.android.sdk.powapos.PowaPOS;
import com.mpowa.android.sdk.powapos.common.base.PowaEnums;
import com.mpowa.android.sdk.powapos.core.PowaPOSEnums;
import com.mpowa.android.sdk.powapos.core.abstracts.PowaMCU;
import com.mpowa.android.sdk.powapos.core.abstracts.PowaScanner;
import com.mpowa.android.sdk.powapos.core.callbacks.PowaPOSCallback;
import com.mpowa.android.sdk.powapos.drivers.s10.PowaS10Scanner;
import com.mpowa.android.sdk.powapos.drivers.tseries.PowaTSeries;
import com.pds.common.R;
import com.pds.common.dialog.ConfirmDialog;

/*//OLD SDK IMPORTS
import com.mpowa.android.powapos.peripherals.PowaPOS;
import com.mpowa.android.powapos.peripherals.drivers.s10.PowaS10Scanner;
import com.mpowa.android.powapos.peripherals.platform.base.PowaPOSEnums;
import com.mpowa.android.powapos.peripherals.platform.base.PowaPeripheralCallback;
import com.mpowa.android.powapos.peripherals.platform.base.PowaScanner;*/

import java.util.Map;


/**
 * Created by Hernan on 04/03/2015.
 */
public class ScannerPOWA {

    private PowaPOS powaPOS;
    private Context context;
    private PowaPOSCallback powaPOSCallback;//NEW VERSION
    //private PowaPeripheralCallback powaPOSCallback;//OLD VERSION

    public ScannerCallback _scannerCallback;

    public interface ScannerCallback {
        void onScannerRead(String text);
    }

    private void toast(String s) {
        Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
    }

    private void reTry(){
        //preguntamos que tipo de ARQUEO desea realizar

        new ConfirmDialog(context, "LECTOR DE CODIGO DE BARRAS", "Error al intentar conectar el lector. Reintentar?<br/>Nota: Aguarde a que la luz del lector quede fija")
                .set_icon(R.drawable.scanner_icon)
                .set_positiveButtonText("ACEPTAR")
                .set_positiveButtonAction(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //TODO: reintentar
                        powaPOS.selectScanner(powaPOS.getAvailableScanners().get(0));
                    }
                })
                .set_negativeButtonText("CANCELAR")
                .set_negativeButtonAction(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    public ScannerPOWA(final Context _context) throws Exception {
        try {
            context = _context;
            //toast("create peripheralCallback");
            powaPOSCallback = new PowaPOSCallback() {

                @Override
                public void onScannerInitialized(PowaPOSEnums.InitializedResult initializedResult) {
                    //toast("onScannerInitialized()");
                    if(initializedResult != PowaPOSEnums.InitializedResult.SUCCESSFUL) {
                        Log.d("Test", "Scan init result:" + String.valueOf(initializedResult));

                        reTry();
                    }
                }

                @Override
                public void onScannerRead(String s) {
                    //toast("onScannerRead");
                    if (_scannerCallback != null)
                        _scannerCallback.onScannerRead(s);
                }

                /*@Override
                public void onPrintJobCompleted(PowaPOSEnums.PrintJobResult printJobResult) {

                }*/

                @Override
                public void onMCUInitialized(PowaPOSEnums.InitializedResult initializedResult) {
                    //toast("onMCUInitialized()");
                }
                @Override
                public void onMCUConnectionStateChanged(PowaEnums.ConnectionState connectionState) {
                    //toast("onMCUConnectionStateChanged()");
                }
                @Override
                public void onMCUFirmwareUpdateStarted() {
                    //toast("onMCUFirmwareUpdateStarted()");
                }

                @Override
                public void onMCUFirmwareUpdateProgress(int i) {
                    //toast("onMCUFirmwareUpdateProgress()");
                }

                @Override
                public void onMCUFirmwareUpdateFinished() {
                    //toast("onMCUFirmwareUpdateFinished()");
                }

                @Override
                public void onMCUBootloaderUpdateStarted() {
                    //toast("onMCUBootloaderUpdateStarted()");
                }

                @Override
                public void onMCUBootloaderUpdateProgress(int i) {
                    //toast("onMCUBootloaderUpdateProgress()");
                }

                @Override
                public void onMCUBootloaderUpdateFinished() {
                    //toast("onMCUBootloaderUpdateFinished");
                }

                @Override
                public void onMCUBootloaderUpdateFailed(PowaPOSEnums.BootloaderUpdateError bootloaderUpdateError) {
                    //toast("onMCUBootloaderUpdateFailed");
                }

                @Override
                public void onMCUSystemConfiguration(Map<String, String> stringStringMap) {
                    //toast("onMCUSystemConfiguration");
                }

                @Override
                public void onUSBDeviceAttached(PowaPOSEnums.PowaUSBCOMPort powaUSBCOMPort) {
                    //toast("onUSBDeviceAttached");
                }

                @Override
                public void onUSBDeviceDetached(PowaPOSEnums.PowaUSBCOMPort powaUSBCOMPort) {
                    //toast("onUSBDeviceDetached");
                }

                @Override
                public void onCashDrawerStatus(PowaPOSEnums.CashDrawerStatus cashDrawerStatus) {
                    //toast("onCashDrawerStatus");
                }

                @Override
                public void onRotationSensorStatus(PowaPOSEnums.RotationSensorStatus rotationSensorStatus) {
                    //toast("onRotationSensorStatus");
                }

                @Override
                public void onPrintJobResult(PowaPOSEnums.PrintJobResult printJobResult) {
                    //toast("onPrintJobResult");
                }

                @Override
                public void onPrinterOutOfPaper() {
                    //toast("onPrinterOutOfPaper");
                }

                @Override
                public void onUSBReceivedData(PowaPOSEnums.PowaUSBCOMPort powaUSBCOMPort, byte[] bytes) {
                    //toast("onUSBReceivedData");

                }
            };
            //toast("create PowaPOS");
            powaPOS = new PowaPOS(context, powaPOSCallback);
            //powaPOS = Powa_POS.getInstance(context, powaPOSCallback);

            initialize();

        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public Boolean initialize() {
        try {

            PowaScanner scanner = new PowaS10Scanner(context);

            powaPOS.addPeripheral(scanner);
Log.d("Test", "Availab:" + String.valueOf(powaPOS.getAvailableScanners().size()));

            if(powaPOS.getAvailableScanners().size() > 0) {
                powaPOS.selectScanner(powaPOS.getAvailableScanners().get(0));

                Thread.sleep(2000); //dormimos 2 seg para esperar a que el scanner esté disponible
            }
            else{
                throw new Exception("No se encontraron scanners disponibles, verifique bluetooth");
            }

        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    /*public boolean isConnected(){
        return (powaPOS != null && powaPOS.getMCU() != null && powaPOSisDriverConnected());
    }*/

    public void end() {
        if (powaPOS != null) {
            powaPOS.dispose();
            powaPOS = null;
        }
    }
}
