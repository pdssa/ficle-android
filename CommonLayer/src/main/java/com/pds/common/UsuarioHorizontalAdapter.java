package com.pds.common;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Hernan on 03/12/13.
 */
public class UsuarioHorizontalAdapter extends ArrayAdapter<Usuario> {
    private Context context;
    private List<Usuario> datos;
    private Boolean simple;
    private Boolean horario;

    private LayoutInflater mInflater;

    public UsuarioHorizontalAdapter(Context context, List<Usuario> datos, int resource, Boolean simple) {
        this(context, datos, resource, simple, false);
    }

    public UsuarioHorizontalAdapter(Context context, List<Usuario> datos, int resource, Boolean simple, Boolean horario) {
        super(context, resource, datos);

        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.datos = datos;
        this.simple = simple;
        this.horario = horario;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;

        // En primer lugar "inflamos" una nueva vista, que será la que se
        // mostrará en la celda del ListView. Para ello primero creamos el
        // inflater, y después inflamos la vista.

        if (convertView == null) {
            // Inflate the view since it does not exist
            convertView = mInflater.inflate(R.layout.item_horizontal_listview, parent, false);

            // Create and save off the holder in the tag so we get quick access to inner fields
            // This must be done for performance reasons
            holder = new Holder();
            holder.enabled = false;
            holder.imageView = (ImageView) convertView.findViewById(R.id.list_row_icon);
            holder.textView = (TextView) convertView.findViewById(R.id.list_row_name);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        Usuario user = datos.get(position);

        // A partir de la vista, recogeremos los controles que contiene para
        // poder manipularlos.
        // Recogemos el ImageView y le asignamos una foto.
        if(holder.enabled || convertView.isSelected())
            holder.imageView.setImageResource(R.drawable.user_on);
        else
            holder.imageView.setImageResource(R.drawable.user);

        // Recogemos los TextView para mostrar datos
        if (TextUtils.isEmpty(user.getApellido()))
            holder.textView.setText(user.getNombre());
        else
            holder.textView.setText(user.getApellido() + ", " + user.getNombre());

        // Devolvemos la vista para que se muestre en el ListView.
        return convertView;
    }

    /** View holder for the views we need access to */
    public static class Holder {
        public ImageView imageView;
        public TextView textView;
        public boolean enabled;
    }
}