package com.pds.common;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Hernan on 24/11/13.
 */
public abstract class Logger {

    public static void RegistrarEvento(Context context,
                                String tipo,
                                String titulo,
                                String descripcion){

        RegistrarEvento(context, tipo, titulo, descripcion, "");
    }

    public static void RegistrarEvento(Context context,
                                       String tipo,
                                       String titulo,
                                       String descripcion,
                                       String descripcion2){

        RegistrarEvento(context.getContentResolver(),tipo, titulo, descripcion, descripcion2);

    }

    public static void RegistrarEvento(ContentResolver contentResolver,
                                       String tipo,
                                       String titulo,
                                       String descripcion,
                                       String descripcion2){

        ContentValues cv = new ContentValues();

        cv.put(LogHelper.LOG_TIPO, tipo);
        cv.put(LogHelper.LOG_TITULO,titulo);
        cv.put(LogHelper.LOG_DESCRIPCION,descripcion);
        cv.put(LogHelper.LOG_DESCRIPCION2, descripcion2);
        cv.put(LogHelper.LOG_FECHA, new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
        cv.put(LogHelper.LOG_HORA, new SimpleDateFormat("HH:mm:ss").format(new Date()));


        Uri newProdUri = contentResolver.insert(Uri.parse("content://com.pds.ficle.ep.logs.contentprovider/logs"), cv);

    }
}
