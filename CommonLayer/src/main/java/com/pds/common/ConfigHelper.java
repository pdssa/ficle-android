package com.pds.common;

/**
 * Created by Hernan on 06/12/13.
 */
public class ConfigHelper {

    //************TABLE CONFIG************
    public static final String TABLE_NAME = "config";

    //columns names
    public static final String CONFIG_ID = "_id";
    public static final String CONFIG_COMERCIO = "comercio";
    public static final String CONFIG_DIRECCION = "direccion";
    public static final String CONFIG_CLAVEFISCAL = "clavefiscal";
    public static final String CONFIG_REC_NRO_COMERCIO = "rec_nro_comercio";
    public static final String CONFIG_REC_ID_TERMINAL = "rec_id_terminal";
    public static final String CONFIG_PRINTER_MODEL = "printer_model";
    public static final String CONFIG_CLOUD_HOST = "cloud_host";
    public static final String CONFIG_CLOUD_PORT = "cloud_port";
    public static final String CONFIG_ID_COMERCIO = "id_comercio";
    public static final String CONFIG_SCAN_MODEL = "scan_model";
    public static final String CONFIG_SYNC_LOGS = "sync_log";
    public static final String CONFIG_SYNC_CAJA = "sync_caja";
    public static final String CONFIG_SYNC_STOCK = "sync_stock";
    public static final String CONFIG_PERFIL = "perfil";
    public static final String CONFIG_INACTIVITY_TIME = "inactivity_time";
    public static final String CONFIG_PAIS = "pais";
    public static final String CONFIG_SYNC_TIME = "sync_time";
    public static final String CONFIG_HIST_DIAS = "hist_days";
    public static final String CONFIG_SII_GIRO1 = "sii_giro1";
    public static final String CONFIG_SII_GIRO2 = "sii_giro2";
    public static final String CONFIG_SII_GIRO3 = "sii_giro3";
    public static final String CONFIG_SII_RES_VALE = "sii_res_vale";
    public static final String CONFIG_SII_VALE_TERMINAL_ID = "sii_terminal_id";
    public static final String CONFIG_SII_VALE_SERVER_IP = "sii_server_ip";
    public static final String CONFIG_SII_VALE_SERVER_PORT = "sii_server_port";
    public static final String CONFIG_SII_VALE_SERVER_PLATFORM = "sii_server_platform";
    public static final String CONFIG_SII_VALE_SERVER_CONSULTA_QR = "sii_server_consulta_qr_path";
    public static final String CONFIG_RAZON_SOCIAL = "razon_social";
    public static final String CONFIG_RUBRO = "rubro";
    public static final String CONFIG_LOCALIDAD = "localidad";
    public static final String CONFIG_SII_VALE_MODALIDAD = "sii_vale_modalidad";
    public static final String CONFIG_SII_VALE_STATUS = "sii_vale_status";
    public static final String CONFIG_SII_VALE_MERCHANT_ID = "sii_merchant_id";


    //columns indexes
    public static final int CONFIG_IX_ID = 0;
    public static final int CONFIG_IX_COMERCIO = 1;
    public static final int CONFIG_IX_DIRECCION = 2;
    public static final int CONFIG_IX_CLAVEFISCAL = 3;
    public static final int CONFIG_IX_REC_NRO_COMERCIO = 4;
    public static final int CONFIG_IX_REC_ID_TERMINAL = 5;
    public static final int CONFIG_IX_PRINTER_MODEL = 6;
    public static final int CONFIG_IX_CLOUD_HOST = 7;
    public static final int CONFIG_IX_CLOUD_PORT = 8;
    public static final int CONFIG_IX_ID_COMERCIO = 9;
    public static final int CONFIG_IX_SCAN_MODEL = 10;
    public static final int CONFIG_IX_SYNC_LOGS = 11;
    public static final int CONFIG_IX_SYNC_CAJA = 12;
    public static final int CONFIG_IX_SYNC_STOCK = 13;
    public static final int CONFIG_IX_PERFIL = 14;
    public static final int CONFIG_IX_INACTIVITY_TIME = 15;
    public static final int CONFIG_IX_PAIS = 16;
    public static final int CONFIG_IX_SYNC_TIME = 17;
    public static final int CONFIG_IX_HIST_DIAS = 18;
    public static final int CONFIG_IX_SII_GIRO1 = 19;
    public static final int CONFIG_IX_SII_GIRO2 = 20;
    public static final int CONFIG_IX_SII_GIRO3 = 21;
    public static final int CONFIG_IX_SII_RES_VALE = 22;
    public static final int CONFIG_IX_SII_VALE_TERMINAL_ID = 23;
    public static final int CONFIG_IX_SII_VALE_SERVER_IP = 24;
    public static final int CONFIG_IX_SII_VALE_SERVER_PORT = 25;
    public static final int CONFIG_IX_SII_VALE_SERVER_PLATFORM = 26;
    public static final int CONFIG_IX_SII_VALE_SERVER_CONSULTA_QR = 27;
    public static final int CONFIG_IX_RAZON_SOCIAL = 28;
    public static final int CONFIG_IX_RUBRO = 29;
    public static final int CONFIG_IX_LOCALIDAD = 30;
    public static final int CONFIG_IX_SII_VALE_MODALIDAD = 31;
    public static final int CONFIG_IX_SII_VALE_STATUS = 32;
    public static final int CONFIG_IX_SII_VALE_MERCHANT_ID = 33;


    // script de creacion de tabla
    public static final String CONFIG_CREATE_SCRIPT = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
            CONFIG_ID + " integer primary key autoincrement, " +
            CONFIG_COMERCIO + " text, " +
            CONFIG_DIRECCION + " text," +
            CONFIG_CLAVEFISCAL + " text, " +
            CONFIG_REC_NRO_COMERCIO + " text, " +
            CONFIG_REC_ID_TERMINAL + " text, " +
            CONFIG_PRINTER_MODEL + " text, " +
            CONFIG_CLOUD_HOST + " text, " +
            CONFIG_CLOUD_PORT + " text, " +
            CONFIG_ID_COMERCIO + " text," +
            CONFIG_SCAN_MODEL + " text," +
            CONFIG_SYNC_LOGS + " integer default 1, " +
            CONFIG_SYNC_CAJA + " integer default 1, " +
            CONFIG_SYNC_STOCK + " integer default 1, " +
            CONFIG_PERFIL + " text," +
            CONFIG_INACTIVITY_TIME + " integer default 60," +
            CONFIG_PAIS + " text," +
            CONFIG_SYNC_TIME + " integer default 60," +
            CONFIG_HIST_DIAS + " integer default 60," +
            CONFIG_SII_GIRO1 + " text," +
            CONFIG_SII_GIRO2 + " text," +
            CONFIG_SII_GIRO3 + " text," +
            CONFIG_SII_RES_VALE + " text," +
            CONFIG_SII_VALE_TERMINAL_ID + " text," +
            CONFIG_SII_VALE_SERVER_IP + " text," +
            CONFIG_SII_VALE_SERVER_PORT + " text," +
            CONFIG_SII_VALE_SERVER_PLATFORM + " text," +
            CONFIG_SII_VALE_SERVER_CONSULTA_QR + " text," +
            CONFIG_RAZON_SOCIAL + " text," +
            CONFIG_RUBRO + " text," +
            CONFIG_LOCALIDAD + " text," +
            CONFIG_SII_VALE_MODALIDAD + " text," +
            CONFIG_SII_VALE_STATUS + " text," +
            CONFIG_SII_VALE_MERCHANT_ID + " text" +
            ");";

    public static final String CONFIG_ADD_SII_VALE_MERCHANT_ID = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + CONFIG_SII_VALE_MERCHANT_ID + " text " ;


    public static final String CONFIG_DEFAULT_CREATE_SCRIPT = "INSERT INTO " + TABLE_NAME + "(" +
            CONFIG_COMERCIO + "," + CONFIG_DIRECCION + "," + CONFIG_CLAVEFISCAL + ") values (" +
            "'','','')";
            //"'COMERCIO TEST','CALLE 123','111233456')";



    public static final String[] columnas = new String[]{
            CONFIG_ID,
            CONFIG_COMERCIO,
            CONFIG_DIRECCION,
            CONFIG_CLAVEFISCAL,
            CONFIG_REC_NRO_COMERCIO,
            CONFIG_REC_ID_TERMINAL,
            CONFIG_PRINTER_MODEL,
            CONFIG_CLOUD_HOST,
            CONFIG_CLOUD_PORT,
            CONFIG_ID_COMERCIO,
            CONFIG_SCAN_MODEL,
            CONFIG_SYNC_LOGS,
            CONFIG_SYNC_CAJA,
            CONFIG_SYNC_STOCK,
            CONFIG_PERFIL,
            CONFIG_INACTIVITY_TIME,
            CONFIG_PAIS,
            CONFIG_SYNC_TIME,
            CONFIG_HIST_DIAS,
            CONFIG_SII_GIRO1,
            CONFIG_SII_GIRO2,
            CONFIG_SII_GIRO3,
            CONFIG_SII_RES_VALE,
            CONFIG_SII_VALE_TERMINAL_ID,
            CONFIG_SII_VALE_SERVER_IP,
            CONFIG_SII_VALE_SERVER_PORT,
            CONFIG_SII_VALE_SERVER_PLATFORM,
            CONFIG_SII_VALE_SERVER_CONSULTA_QR,
            CONFIG_RAZON_SOCIAL,
            CONFIG_RUBRO,
            CONFIG_LOCALIDAD,
            CONFIG_SII_VALE_MODALIDAD,
            CONFIG_SII_VALE_STATUS,
            CONFIG_SII_VALE_MERCHANT_ID
    };

}
