package com.pds.common;

/**
 * Created by Hernan on 24/11/13.
 */
public class LogHelper {
    
    //************TABLE LOGS************
    public static final String TABLE_NAME = "log";
    //columns names
    public static final String LOG_ID = "_id";
    public static final String LOG_TIPO = "tipo";
    public static final String LOG_FECHA = "fecha";
    public static final String LOG_HORA = "hora";
    public static final String LOG_TITULO = "titulo";
    public static final String LOG_DESCRIPCION = "descripcion";
    public static final String LOG_DESCRIPCION2 = "descripcion2";
    public static final String LOG_SYNC_STATUS = "sync";

    //columns indexes
    public static final int LOG_IX_ID = 0;
    public static final int LOG_IX_TIPO = 1;
    public static final int LOG_IX_FECHA = 2;
    public static final int LOG_IX_HORA = 3;
    public static final int LOG_IX_TITULO = 4;
    public static final int LOG_IX_DESCRIPCION = 5;
    public static final int LOG_IX_DESCRIPCION2 = 6;
    public static final int LOG_IX_SYNC_STATUS = 7;

    // script de creacion de tabla
    public static final String LOG_CREATE_SCRIPT = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
            LOG_ID + " integer primary key autoincrement, " +
            LOG_TIPO + " text, " +
            LOG_FECHA + " text, " +
            LOG_HORA + " text, " +
            LOG_TITULO + " text, " +
            LOG_DESCRIPCION + " text, " +
            LOG_DESCRIPCION2 + " text, " +
            LOG_SYNC_STATUS + " text default 'N'" +
            ");";

    //script de alter
    public static final String LOG_ALTER_SYNC_STATUS_SCRIPT = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + LOG_SYNC_STATUS + " text default 'N'" ;
    public static final String LOG_ALTER_SYNC_STATUS_SCRIPT_2 = "UPDATE " + TABLE_NAME + " SET " + LOG_SYNC_STATUS + " = 'N'" ;

    public static final String[] columnas = new String[]{
            LOG_ID,
            LOG_TIPO,
            LOG_FECHA,
            LOG_HORA,
            LOG_TITULO,
            LOG_DESCRIPCION,
            LOG_DESCRIPCION2,
            LOG_SYNC_STATUS
    };

}
