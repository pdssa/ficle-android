package com.pds.common.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.View;

/**
 * Created by Hernan on 25/08/2014.
 */
public class ConfirmDialog {

    private Context _context;
    private String _titulo;
    private String _mensaje;
    private String _positiveButtonText;
    private DialogInterface.OnClickListener _positiveButtonAction;
    private String _negativeButtonText;
    private DialogInterface.OnClickListener _negativeButtonAction;
    private boolean _cancelable;
    private boolean _cancelableOnTouchOutside;
    private int _icon;

    public ConfirmDialog set_cancelable(boolean _cancelable) {
        this._cancelable = _cancelable;
        return this;
    }

    public ConfirmDialog set_cancelableOnTouchOutside(boolean _cancelableOnTouchOutside) {
        this._cancelableOnTouchOutside = _cancelableOnTouchOutside;
        return this;
    }

    public ConfirmDialog set_titulo(String _titulo) {
        this._titulo = _titulo;
        return this;
    }

    public ConfirmDialog set_mensaje(String _mensaje) {
        this._mensaje = _mensaje;
        return this;
    }

    public ConfirmDialog set_positiveButtonText(String _positiveButtonText) {
        this._positiveButtonText = _positiveButtonText;
        return this;
    }

    public ConfirmDialog set_positiveButtonAction(DialogInterface.OnClickListener _positiveButtonAction) {
        this._positiveButtonAction = _positiveButtonAction;
        return this;
    }

    public ConfirmDialog set_negativeButtonText(String _negativeButtonText) {
        this._negativeButtonText = _negativeButtonText;
        return this;
    }

    public ConfirmDialog set_negativeButtonAction(DialogInterface.OnClickListener _negativeButtonAction) {
        this._negativeButtonAction = _negativeButtonAction;
        return this;
    }

    public ConfirmDialog set_negativeButton(String _negativeButtonText, DialogInterface.OnClickListener _negativeButtonAction) {
        set_negativeButtonText(_negativeButtonText);
        set_negativeButtonAction(_negativeButtonAction);
        return this;
    }

    public ConfirmDialog set_positiveButton(String _positiveButtonText, DialogInterface.OnClickListener _positiveButtonAction) {
        set_positiveButtonText(_positiveButtonText);
        set_positiveButtonAction(_positiveButtonAction);
        return this;
    }

    public ConfirmDialog set_icon(int _icon) {
        this._icon = _icon;
        return this;
    }

    public ConfirmDialog(Context context) {
        _context = context;
        //por default
        _titulo = "";
        _mensaje = "";
        _positiveButtonText = "SI";
        _positiveButtonAction = null;
        _negativeButtonText = "NO";
        _negativeButtonAction = null;
        _icon = android.R.drawable.ic_dialog_alert;
    }

    public ConfirmDialog(Context context, String titulo, String mensaje) {
        _context = context;
        _titulo = titulo;
        _mensaje = mensaje;
        //por default
        _positiveButtonText = "SI";
        _positiveButtonAction = null;
        _negativeButtonText = "NO";
        _negativeButtonAction = null;
        _cancelable = false;
        _cancelableOnTouchOutside = false;
        _icon = android.R.drawable.ic_dialog_alert;
    }

    public ConfirmDialog(Context _context, String _titulo, String _mensaje, String _positiveButtonText, DialogInterface.OnClickListener _positiveButtonAction, String _negativeButtonText, DialogInterface.OnClickListener _negativeButtonAction) {
        this._context = _context;
        this._titulo = _titulo;
        this._mensaje = _mensaje;
        this._positiveButtonText = _positiveButtonText;
        this._positiveButtonAction = _positiveButtonAction;
        this._negativeButtonText = _negativeButtonText;
        this._negativeButtonAction = _negativeButtonAction;
        //por default
        this._cancelable = false;
        this._cancelableOnTouchOutside = false;
        this._icon = android.R.drawable.ic_dialog_alert;
    }

    public void show() {
        AlertDialog c = new AlertDialog.Builder(_context)
                .setIcon(_icon)
                .setTitle(_titulo)
                .setMessage(Html.fromHtml(_mensaje))
                        .setPositiveButton(_positiveButtonText, _positiveButtonAction)
                        .setNegativeButton(_negativeButtonText, _negativeButtonAction)
                        .create();

        c.setCancelable(_cancelable);
        c.setCanceledOnTouchOutside(_cancelableOnTouchOutside);
        c.show();
    }

}
