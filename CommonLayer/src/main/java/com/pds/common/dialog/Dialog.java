package com.pds.common.dialog;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.pds.common.R;

/**
 * Created by Hernan on 26/10/13.
 */
public abstract class Dialog {


    public static void Alert(Context context, String titulo, String mensaje) {
        AlertDialog.Builder builder = new Builder(context);

        builder
                .setTitle(titulo)
                .setMessage(mensaje)
                .setPositiveButton("Aceptar", null)
                .create()
                .show();
    }

    public static void Alert(Context context, String titulo, String mensaje, int icon) {
        Alert(context, titulo, mensaje, icon, null);
    }

    public static void Alert(Context context, String titulo, String mensaje, int icon, DialogInterface.OnClickListener onAccept) {
        AlertDialog.Builder builder = new Builder(context);

        builder
                .setTitle(titulo)
                .setMessage(mensaje)
                .setIcon(icon)
                .setPositiveButton("Aceptar", onAccept)
                .setCancelable(false)
                .create()
                .show();
    }

    public static void ImageDialog(Context context, String imagePath, DialogInterface.OnDismissListener onDismiss){
        final android.app.Dialog imageDialog = new android.app.Dialog(context, R.style.ProgressGreyTransparent);
        imageDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View contentView = LayoutInflater.from(context).inflate(R.layout.image_layout, null);

        ((ImageView)contentView.findViewById(R.id.image_layout_image)).setImageURI(Uri.parse(imagePath));

        contentView.findViewById(R.id.image_layout_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageDialog.dismiss();
            }
        });

        imageDialog.setContentView(contentView);
        imageDialog.setCancelable(false);
        imageDialog.setCanceledOnTouchOutside(false);
        imageDialog.setOnDismissListener(onDismiss);
        imageDialog.show();

        //lo hacemos "full screen"
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = imageDialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);
    }
}
