package com.pds.common.dialog;

import android.content.Context;
import android.content.DialogInterface;

import java.util.Calendar;

/**
 * Created by Hernan on 19/02/2015.
 */
public abstract class TimePickerDialog {
    public static void showTimePickerDialog(Context context, String title, android.app.TimePickerDialog.OnTimeSetListener event) {

        /** Get the current date */
        final Calendar cal = Calendar.getInstance();
        int pHour = cal.get(Calendar.HOUR_OF_DAY);
        int pMinute = cal.get(Calendar.MINUTE);

        CustomTimePickerDialog a = new CustomTimePickerDialog(context, event, pHour, pMinute, true, 15);

        //a.().setSpinnersShown(false);
        //a.getDatePicker().setCalendarViewShown(true);

        a.setTitle(title);

        a.setButton(android.app.TimePickerDialog.BUTTON_POSITIVE, "Seleccionar", a);
        a.setButton(android.app.TimePickerDialog.BUTTON_NEGATIVE, "Cancelar", (DialogInterface.OnClickListener) null);
        a.setCanceledOnTouchOutside(false);
        a.setCancelable(false);
        a.show();
    }


}
