package com.pds.common;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Hernan on 03/02/14.
 */
public abstract class Caja {

    public static enum TipoMovimientoCaja {
        DEPOSITO, //0
        RETIRO, //1
        ARQUEO, //2
        CIERRE //3
    }

    public static void RegistrarMovimientoCaja(Context context,
                                               TipoMovimientoCaja tipoMov,
                                               String monto,
                                               String medioPago,
                                               String observacion) {

        RegistrarMovimientoCaja(context, tipoMov, monto, medioPago, observacion, -1);
    }

    public static void RegistrarMovimientoCaja(Context context,
                                               TipoMovimientoCaja tipoMov,
                                               String monto,
                                               String medioPago,
                                               String observacion,
                                               int ventaId) {

        ContentValues cv = new ContentValues();

        cv.put(CajaHelper.CAJA_TIPO_MOV, tipoMov.ordinal());
        cv.put(CajaHelper.CAJA_MONTO, monto);
        cv.put(CajaHelper.CAJA_OBSERVACION, observacion);
        cv.put(CajaHelper.CAJA_FECHA, Formatos.FormateaDate(new Date(), Formatos.DateFormat));
        cv.put(CajaHelper.CAJA_HORA, Formatos.FormateaDate(new Date(), Formatos.TimeFormat));
        cv.put(CajaHelper.CAJA_MEDIO_PAGO, medioPago);

        if (ventaId != -1)
            cv.put(CajaHelper.CAJA_VENTA_ID, ventaId);


        Uri newProdUri = context.getContentResolver().insert(Uri.parse("content://com.pds.ficle.ep.caja.contentprovider/caja"), cv);

    }

    public static void RegistrarMovimientoCaja(Context context,
                                               TipoMovimientoCaja tipoMov,
                                               double monto,
                                               String medioPago,
                                               String observacion,
                                               long ventaId,
                                               String tipoVta) {

        ContentValues cv = new ContentValues();

        cv.put(CajaHelper.CAJA_TIPO_MOV, tipoMov.ordinal());
        cv.put(CajaHelper.CAJA_MONTO, monto);
        cv.put(CajaHelper.CAJA_OBSERVACION, observacion);
        cv.put(CajaHelper.CAJA_FECHA, Formatos.FormateaDate(new Date(), Formatos.DateFormat));
        cv.put(CajaHelper.CAJA_HORA, Formatos.FormateaDate(new Date(), Formatos.TimeFormat));
        cv.put(CajaHelper.CAJA_MEDIO_PAGO, medioPago);

        if (ventaId != -1) {
            cv.put(CajaHelper.CAJA_VENTA_ID, ventaId);
            cv.put(CajaHelper.CAJA_TIPO_VTA, tipoVta);
        }

        Uri newProdUri = context.getContentResolver().insert(Uri.parse("content://com.pds.ficle.ep.caja.contentprovider/caja"), cv);

    }

}
