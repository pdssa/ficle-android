package com.pds.common.display;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;

import java.util.List;

/**
 * Created by Hernan on 11/07/2014.
 */
public class Message implements Parcelable {

    public static final String MESSAGE_TYPE = "type";
    public static final String MESSAGE_CONTENT = "content";
    public static final String MESSAGE_TIMEOUT = "timeout";

    public String type;
    public String content; //json object of different messages types
    public int timeout; //seconds to finish the message

    @Override
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(type);
        out.writeString(content);
        out.writeInt(timeout);
    }

    public Message(Parcel in){
        type = in.readString();
        content = in.readString();
        timeout = in.readInt();
    }

    // Method to recreate a Message from a Parcel
    public static final Creator<Message> CREATOR = new Creator<Message>() {
        public Message createFromParcel(Parcel source) {return new Message(source);}

        public Message[] newArray(int size) {return new Message[size];}
    };

    public Message(){

    }

    @Override
    public String toString() {
        return new Gson().toJson(this, Message.class);
    }

    public ConnectionRequest getConnectionRequestContent(){
        return new Gson().fromJson(content, Message.ConnectionRequest.class);
    }

    public ShowSplashRequest getShowTextRequestContent(){
        return new Gson().fromJson(content, Message.ShowSplashRequest.class);
    }

    public ShowScreenRequest getShowScreenRequestContent(){
        return new Gson().fromJson(content, Message.ShowScreenRequest.class);
    }


    public static final class MessageType{
        public static final String CONNECT_TYPE = "ConnectionRequest";
        public static final String SHOW_SCREEN_TYPE = "ShowScreenRequest";
        public static final String SHOW_SPLASH_TYPE = "ShowSplashRequest";
        public static final String CLOSE_SCREEN = "CloseScreen";
        //public static final String INPUT_TYPE = "InputResponse";
    }


    //SERIALIZABLES CLASSES (dentro de Message.content)

    public class ConnectionRequest {
        public static final String CONTENT_DEVICE = "device";

        public String device;

        public ConnectionRequest(String _device){
            device = _device;
        }

        @Override
        public String toString() {
            return new Gson().toJson(this, Message.ConnectionRequest.class);
        }
    }

    public class ShowSplashRequest{
        public static final String CONTENT_TEXT = "text";

        public String text;

        public ShowSplashRequest(String _text){
            text = _text;
        }

        @Override
        public String toString() {
            return new Gson().toJson(this, Message.ShowSplashRequest.class);
        }
    }


    public class InputRequest{
        public static final String CONTENT_LABEL = "label";
        public static final String CONTENT_HINT = "hint";
    }

    public class ShowScreenRequest{

        private transient final String CONTENT_SCREEN = "screen";
        private transient final String CONTENT_DATA = "data";

        public String screen;
        public List<ShowScreenViewContent> data;


        @Override
        public String toString() {
            return new Gson().toJson(this, Message.ShowScreenRequest.class);
        }

        public String dataToString() {
            return new Gson().toJson(data, data.getClass());
        }
    }

    public class ShowScreenViewContent{

        private transient final String CONTENT_VIEW_ID = "idView";
        private transient final String CONTENT_VIEW_TYPE = "classTypeView";
        private transient final String CONTENT_VIEW_CONTENT = "viewContent";
        private transient final String CONTENT_TYPE_PROP = "viewContentType";


        public String idView;
        public String classTypeView;
        public String viewContentType;
        public Object viewContent; //public List<ShowScreenListContent> viewContent

        public void setListContent(List<ShowScreenListContent> viewContent){
            this.viewContent = viewContent;
            //this.viewContent = new Gson().toJson(viewContent, viewContent.getClass());
        }
    }

    public class ShowScreenListContent{

        public List<ShowScreenViewContent> item;

    }
}
