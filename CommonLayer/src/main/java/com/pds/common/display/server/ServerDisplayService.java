package com.pds.common.display.server;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceRequest;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Messenger;
import android.util.Log;
import android.widget.Toast;

import com.pds.common.display.Connection;
import com.pds.common.display.Message;
import com.pds.common.display.MessageHandler;
import com.pds.common.display.WiFiDirectBroadcastReceiver;

import java.lang.reflect.Method;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

public class ServerDisplayService extends Service
        implements WifiP2pManager.ConnectionInfoListener, Connection.ConnectionListener, WiFiDirectBroadcastReceiver.WifiDirectListener {

    private static final String DEBUG_TAG = ServerDisplayService.class.getName();
    private static final String TAG = "Wifi-P2P";


    private WifiP2pManager mManager;
    private WifiP2pManager.Channel mChannel;
    private WiFiDirectBroadcastReceiver mReceiver;
    private IntentFilter mIntentFilter;
    private WifiP2pDnsSdServiceRequest mServiceRequest;
    private List<WifiP2pDevice> mPeersList = new ArrayList<WifiP2pDevice>();
    private Connection mConnection;

    private WifiP2pDevice mServerPeer;

    public static final int SERVER_PORT = 4545;


    public ServerDisplayService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appendStatus("starting server...");

        //new WiFiP2pManager
        mManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        //init
        mChannel = mManager.initialize(this, getMainLooper(), null);

        //new WiFiBroadcastReceiver
        mReceiver = new WiFiDirectBroadcastReceiver(mManager, mChannel, this, null, this);

        //filters action interested in
        mIntentFilter = mReceiver.intentFilter();

        /* register the broadcast receiver with the intent values to be matched */
        registerReceiver(mReceiver, mIntentFilter);

        appendStatus("Service Created");
    }

    /* unregister the broadcast receiver and close connection */
    @Override
    public void onDestroy() {

        unregisterReceiver(mReceiver);

        //stopping connection
        if (this.mConnection != null) {
            this.mConnection.closeConnection();
            this.mConnection = null;
        }

        //stop server service
        if (mServerServiceHandler != null) {
            mServerServiceHandler.stopServer();
            mServerServiceHandler = null;
        }

        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(DEBUG_TAG, "onStartCommand");
        return android.app.Service.START_STICKY;
    }


    private ServerServiceHandler mServerServiceHandler;

    private WifiP2pDevice mThisDevice;

    @Override
    public void onWiFiAvailable(WifiP2pDevice thisDevice) {
        mThisDevice = thisDevice;
        //Start Service
        mServerServiceHandler = new ServerServiceHandler(this, mManager, mChannel);
        mServerServiceHandler.startServer();
    }


    /**
     * ESTE EVENTO PROVIENE DE UNA CONEXION ESTABLECIDA
     *
     * @param wifiP2pInfo
     */
    @Override
    public void onConnectionInfoAvailable(WifiP2pInfo wifiP2pInfo) {
        // InetAddress from WifiP2pInfo struct.
        InetAddress groupOwnerAddress = wifiP2pInfo.groupOwnerAddress;

        //Thread handler = null;

        // After the group negotiation, we can determine the group owner.
        if (wifiP2pInfo.groupFormed && wifiP2pInfo.isGroupOwner) {
            // Do whatever tasks are specific to the group owner.
            // One common case is creating a server thread and accepting
            // incoming connections.
            appendStatus("Connected as group owner");
            try {
                mConnection = new Connection(SERVER_PORT, this, mServerHandler);
                mConnection.createServer();
                //handler = new GroupOwnerSocketHandler(((ChatManager.MessageTarget) this).getHandler());
                //handler.start();
            } catch (Exception e) {
                appendStatus("Failed to create a server thread - " + e.getMessage());
                return;
            }
        } else if (wifiP2pInfo.groupFormed) {
            // The other device acts as the client. In this case,
            // you'll want to create a client thread that connects to the group
            // owner.
            appendStatus("Connected as peer");
        }
    }


    private void appendStatus(String status) {
        Log.d(TAG, status);
        Toast.makeText(this, status, Toast.LENGTH_SHORT).show();
    }


    private Handler mServerHandler = new MessageHandler(ServerDisplayService.this) {
        @Override
        public void onMessage(String type, Message message) {
            try {
                if (type.equals(Message.MessageType.CONNECT_TYPE)) {//COMMUNICATION REQUEST

                    Message.ConnectionRequest deviceFrom = message.getConnectionRequestContent();

                    Toast.makeText(getApplicationContext(), "Device: " + deviceFrom.device, Toast.LENGTH_SHORT).show();

                    /*Message msg = new Message();
                    msg.type = Message.MessageType.SHOW_SPLASH_TYPE;
                    msg.content = message.new ShowSplashRequest(String.valueOf(Math.min((int) (Math.random() * 1000), 20))).toString();

                    mConnection.sendMessage(msg.toString());*/

                }
            } catch (Exception e) {
                Log.d(DEBUG_TAG, "JSON parsing exception: " + e);
            }
        }
    };

    @Override
    public void onConnection() {
        /*Message message = new Message();
        message.type = Message.MessageType.CONNECT_TYPE;
        message.content = message.new ConnectionRequest(getHostName(mThisDevice)).toString();

        mConnection.sendMessage(message.toString());*/
    }

    private static String getHostName(WifiP2pDevice device) {
        try {
            if (device != null)
                return device.deviceName;
            else {

                Method getString = Build.class.getDeclaredMethod("getString", String.class);
                getString.setAccessible(true);
                return getString.invoke(null, "net.hostname").toString();
            }
        } catch (Exception ex) {
            return "12345678";
        }
    }

    private void notifyUser(final String msg) {
        Toast.makeText(ServerDisplayService.this, msg, Toast.LENGTH_SHORT).show();
    }


    /**
     * Target we publish for clients to send messages to IncomingHandler.
     */
    final Messenger mMessenger = new Messenger(new IncomingHandler());

    /** Command to the service to display a message */
    public static final int MSG_SHOW_TEXT = 1;
    public static final int MSG_SHOW_SCREEN = 2;
    public static final int MSG_CLOSE_SCREEN = 3;

    /**
     * Handler of incoming messages from clients.
     */
    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case MSG_SHOW_TEXT:
                    showText(msg.getData().getString("text"));
                    break;
                case MSG_SHOW_SCREEN:
                    Bundle data = msg.getData();
                    data.setClassLoader(getClassLoader());

                    showScreen((Message)data.getParcelable("d"));
                    break;
                case MSG_CLOSE_SCREEN:
                    endSession();
                    break;

                default:
                    super.handleMessage(msg);
            }
        }
    }


    // Binder given to clients
    //private final IBinder mBinder = new ServiceBinder();
    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    /*public class ServiceBinder extends Binder {
        public ServerDisplayService getService() {
            // Return this instance of LocalService so clients can call public methods
            return ServerDisplayService.this;
        }
    }*/

    /*
    ** A client is binding to the service with bindService()
    ** We return an interface to our messenger for sending messages to the service
     */
    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }


    //*******************METODOS*********************
    public boolean isOnLine(){
        return mConnection != null;
    }

    private void showScreen(Message msg){
        mConnection.sendMessage(msg.toString());
    }

    public void endSession(){
        Message msg = new Message();
        msg.type = Message.MessageType.CLOSE_SCREEN;

        mConnection.sendMessage(msg.toString());
    }

    private void showText(String text){
        Message msg = new Message();
        msg.type = Message.MessageType.SHOW_SPLASH_TYPE;
        msg.content = msg.new ShowSplashRequest(text).toString();

        mConnection.sendMessage(msg.toString());
    }


}