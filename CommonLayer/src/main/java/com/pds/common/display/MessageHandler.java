package com.pds.common.display;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.google.gson.Gson;


/**
 * Created by Hernan on 11/07/2014.
 */

public abstract class MessageHandler extends Handler {
    private static final String DEBUG_TAG = MessageHandler.class.getName();
    private Context mContext;

    public MessageHandler(Context context) {
        mContext = context;
    }

    @Override
    public void handleMessage(android.os.Message msg) {
        try {
            String data = msg.getData().getString("m");
            //final JSONObject jsonObject = new JSONObject(data); //obtenemos el json del mensaje

            //final String type = jsonObject.getString(Message.MESSAGE_TYPE);
            final Message content = new Gson().fromJson(data, Message.class);

            if (mContext instanceof Activity) {
                ((Activity) mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onMessage(content.type, content);
                    }
                });
            } else {
                onMessage(content.type, content);
            }
        } catch (Exception e) {
            Log.e(DEBUG_TAG, "Invalid message format: " + e);
        }
    }

    abstract public void onMessage(String type, Message message);
}