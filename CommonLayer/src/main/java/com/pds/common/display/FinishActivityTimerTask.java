package com.pds.common.display;

import android.app.Activity;
import android.util.Log;

import java.util.TimerTask;

/**
 * Created by Hernan on 13/04/2015.
 */
public class FinishActivityTimerTask extends TimerTask {

    private Activity mActivity;


    public FinishActivityTimerTask(Activity activity) {
        mActivity = activity;
    }

    @Override
    public void run() {
        mActivity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                Log.d("FinishActivityTimerTask", "Closing screen");

                if(!mActivity.isFinishing()) {
                    mActivity.finish();
                }
            }
        });
    }

}
