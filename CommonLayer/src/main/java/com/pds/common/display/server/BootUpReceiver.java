package com.pds.common.display.server;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.pds.common.display.server.ServerDisplayService;


/**
 * Created by Hernan on 12/04/2014.
 */
public class BootUpReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        //esta clase recibe el aviso que Android termino de iniciar y realiza una serie de acciones
        //1.START MODO PUBLICIDAD
        //StartSureVideo(context);
        //2.START SERVICE
        StartDisplayService(context);
    }

    private void StartDisplayService(Context context) {
        try {
            Intent i = new Intent(context, ServerDisplayService.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startService(i);
        }catch (Exception ex) {
            Log.e("Display",ex.toString());
        }
    }

}
