
package com.pds.common.display;

import android.net.wifi.p2p.WifiP2pDevice;

/**
 * A structure to hold service information.
 */
public class WiFiP2pService {
    public WifiP2pDevice device;
    public String instanceName = null;
    public String serviceRegistrationType = null;
}
