package com.pds.common.display;

/**
 * Created by EMEKA on 20/03/2015.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pManager;
import android.util.Log;
import android.widget.Toast;

/**
 * A BroadcastReceiver that notifies of important Wi-Fi p2p events.
 */
public class WiFiDirectBroadcastReceiver extends BroadcastReceiver {

    private WifiP2pManager mManager;
    private WifiP2pManager.Channel mChannel;
    private Context mContext;
    private WifiP2pManager.PeerListListener mPeerListListener;
    private WifiDirectListener mWifiDirectListener;

    private static final String TAG = "Wifi-P2P";

    public boolean WifiP2pEnabled;

    public IntentFilter intentFilter() {
        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        return mIntentFilter;
    }

    public WiFiDirectBroadcastReceiver(WifiP2pManager manager,
                                       WifiP2pManager.Channel channel,
                                       Context context,
                                       WifiP2pManager.PeerListListener peerListListener,
                                       WifiDirectListener wifiDirectListener) {
        super();
        this.mManager = manager;
        this.mChannel = channel;
        this.mContext = context;
        this.mPeerListListener = peerListListener;
        this.WifiP2pEnabled = true; //por default asumimos ON
        this.mWifiDirectListener = wifiDirectListener;
    }

    public interface WifiDirectListener {
        public void onWiFiAvailable(WifiP2pDevice thisDevice);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
            // Check to see if Wi-Fi is enabled and notify appropriate activity
            int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
            if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
                // Wifi P2P is enabled
                WifiP2pEnabled = true;

                Log.i(TAG, "Enabled");
                Toast.makeText(this.mContext, "Wifi-P2P: Enabled", Toast.LENGTH_SHORT).show();

                WifiP2pDevice thisDevice = intent.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_DEVICE);

                if (mWifiDirectListener != null)
                    mWifiDirectListener.onWiFiAvailable(thisDevice);

            } else {
                // Wi-Fi P2P is not enabled
                WifiP2pEnabled = false;

                Log.i(TAG, "Not Enabled");
                Toast.makeText(this.mContext, "Wifi-P2P: Not Enabled", Toast.LENGTH_SHORT).show();
            }
        } else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {
            // Discovery process succeeds and broadcast intent action indicates that the available peer list has changed.
            // This can be sent as a result of peers being found, lost or updated
            // Call WifiP2pManager.requestPeers() to get a list of current peers
            // This is an asynchronous call and the calling activity is notified
            // with a callback on PeerListListener.onPeersAvailable()
            //
            if (this.mManager != null) {

                NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);

                if (networkInfo.isConnected()) {
                    // we are connected with the other device, request connection
                    // info to find group owner IP
                    Log.d(TAG, "Connected to p2p network. Requesting network details");
                    Toast.makeText(this.mContext, "Connected to p2p network. Requesting network details", Toast.LENGTH_SHORT).show();

                    this.mManager.requestConnectionInfo(this.mChannel, (WifiP2pManager.ConnectionInfoListener) this.mContext);
                } else {
                    // It's a disconnect
                }
            }
        } else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {
            // Respond to this device's wifi state changing
            WifiP2pDevice device = (WifiP2pDevice) intent.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_DEVICE);
            Log.d(TAG, "Device status : " + getDeviceStatus(device.status));
            Toast.makeText(this.mContext, "Device status : " + getDeviceStatus(device.status), Toast.LENGTH_SHORT).show();
        }
    }

    public static String getDeviceStatus(int statusCode) {
        switch (statusCode) {
            case WifiP2pDevice.CONNECTED:
                return "Connected";
            case WifiP2pDevice.INVITED:
                return "Invited";
            case WifiP2pDevice.FAILED:
                return "Failed";
            case WifiP2pDevice.AVAILABLE:
                return "Available";
            case WifiP2pDevice.UNAVAILABLE:
                return "Unavailable";
            default:
                return "Unknown";

        }
    }

    public static String getErrorResultDescription(int errorCode) {
        switch (errorCode) {
            case WifiP2pManager.P2P_UNSUPPORTED:
                return "P2P isn't supported on this device.";
            case WifiP2pManager.BUSY:
                return "The system is to busy to process the request.";
            case WifiP2pManager.ERROR:
                return "The operation failed due to an internal error.";
            case WifiP2pManager.NO_SERVICE_REQUESTS:
                return "No service requests are added.";
            default:
                return "Unknown error code: " + String.valueOf(errorCode);

        }
    }
}