package com.pds.common.display.server;

import android.content.Context;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceInfo;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceRequest;
import android.util.Log;
import android.widget.Toast;

import com.pds.common.display.WiFiDirectBroadcastReceiver;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Hernan on 11/07/2014.
 */
public class ServerServiceHandler {
    private final String DEBUG_TAG = ServerServiceHandler.class.getName();

    // TXT RECORD properties
    public static final String TXTRECORD_PROP_AVAILABLE = "available";
    public static final String SERVICE_INSTANCE = "_PDSSERVER";
    public static final String SERVICE_REG_TYPE = "_presence._tcp";

    private WifiP2pManager mManager;
    private WifiP2pManager.Channel mChannel;
    private WifiP2pDnsSdServiceInfo mService;
    private Context mContext;

    public ServerServiceHandler(Context context, WifiP2pManager manager, WifiP2pManager.Channel channel) {
        mContext = context;
        mManager = manager;
        mChannel = channel;

        try {
            //WifiManager wifi = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
            //WifiInfo wifiInfo = wifi.getConnectionInfo();
            //int intaddr = wifiInfo.getIpAddress();

        } catch (Exception e) {
            Log.d(DEBUG_TAG, "Error in ServerServiceHandler creation: " + e);
        }
    }

    public void appendStatus(String status) {
        Log.d(DEBUG_TAG, status);
        Toast.makeText(mContext, status, Toast.LENGTH_SHORT).show();
    }

    /**
     * starts server
     * you need to use this function only for device you need to register as server
     */
    public void startServer() {

        appendStatus("Adding Local Service...");

        /*new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {*/
        Map<String, String> record = new HashMap<String, String>();
        record.put("listenport", String.valueOf(998));
        record.put("buddyname", "John Doe" + (int) (Math.random() * 1000));
        record.put(TXTRECORD_PROP_AVAILABLE, "visible");

        // Service information.  Pass it an instance name, service type
        // _protocol._transportlayer , and the map containing
        // information other devices will want once they connect to this one.
        mService = WifiP2pDnsSdServiceInfo.newInstance(SERVICE_INSTANCE, SERVICE_REG_TYPE, record);


        // Add the local service, sending the service info, network channel,
        // and listener that will be used to indicate success or failure of
        // the request.
        mManager.addLocalService(mChannel, mService, addServiceActionListener);


        /*
         * Hacemos un discover para poner en modo escucha y que los peers se puedan conectar
         */
        this.mManager.setDnsSdResponseListeners(this.mChannel,
                new WifiP2pManager.DnsSdServiceResponseListener() {

                    @Override
                    public void onDnsSdServiceAvailable(String instanceName, String registrationType, WifiP2pDevice srcDevice) {
                        appendStatus("TEST" );
                    }
                },
                new WifiP2pManager.DnsSdTxtRecordListener() {

                    /* Callback includes:
                     * fullDomain: full domain name: e.g "printer._ipp._tcp.local."
                     * record: TXT record dta as a map of key/value pairs.
                     * device: The device running the advertised service.
                     */
                    @Override
                    public void onDnsSdTxtRecordAvailable(String fullDomainName, Map<String, String> record, WifiP2pDevice device) {
                        appendStatus("TEST 2" );
                    }
                });
        WifiP2pDnsSdServiceRequest mServiceRequest = WifiP2pDnsSdServiceRequest.newInstance();
        this.mManager.addServiceRequest(this.mChannel, mServiceRequest,discoverServiceActionListener);
        mManager.discoverServices(mChannel, discoverServiceActionListener);

        //return null;
        //  }
        //}.execute((Void) null);
    }

    public void stopServer() {

        appendStatus("Disconnecting...");

        /*mManager.requestGroupInfo(mChannel, new WifiP2pManager.GroupInfoListener() {
            @Override
            public void onGroupInfoAvailable(WifiP2pGroup group) {
                if (group != null && mManager != null && mChannel != null && group.isGroupOwner()) {
                    mManager.removeGroup(mChannel, disconnectActionListener);
                }
            }
        });*/

        this.mManager.removeGroup(this.mChannel, disconnectActionListener);

        appendStatus("Removing Local Service...");

        mManager.removeLocalService(mChannel, mService, removeServiceActionListener);


    }

    public void resetServer() {
        stopServer();

        startServer();
    }

    private WifiP2pManager.ActionListener addServiceActionListener = new WifiP2pManager.ActionListener() {
        @Override
        public void onSuccess() {
            appendStatus("Added Local Service");
        }

        @Override
        public void onFailure(int errorCode) {
            appendStatus("Failed to add a service. Error:" + WiFiDirectBroadcastReceiver.getErrorResultDescription(errorCode));
        }
    };

    private WifiP2pManager.ActionListener removeServiceActionListener = new WifiP2pManager.ActionListener() {
        @Override
        public void onSuccess() {
            appendStatus("Removed Local Service");
        }

        @Override
        public void onFailure(int errorCode) {
            appendStatus("Failed to remove a service. Error:" + WiFiDirectBroadcastReceiver.getErrorResultDescription(errorCode));
        }
    };

    private WifiP2pManager.ActionListener disconnectActionListener = new WifiP2pManager.ActionListener() {
        @Override
        public void onSuccess() {
            appendStatus("Disconnect success");
        }

        @Override
        public void onFailure(int reasonCode) {
            appendStatus("Disconnect failed. Error:" + WiFiDirectBroadcastReceiver.getErrorResultDescription(reasonCode));
        }
    };

    private WifiP2pManager.ActionListener discoverServiceActionListener = new WifiP2pManager.ActionListener() {
        @Override
        public void onSuccess() {appendStatus("---");}

        @Override
        public void onFailure(int errorCode) {appendStatus("-. Error:" + WiFiDirectBroadcastReceiver.getErrorResultDescription(errorCode));}
    };
}
