package com.pds.common.db;

/**
 * Created by Hernan on 22/11/13.
 */
public class CompraTable {

    //************TABLE COMPRAS************
    public static final String TABLE_NAME = "compras";

    //columns names
    public static final String COMPRA_ID = "_id";
    public static final String COMPRA_NUMERO_FC = "numero";
    public static final String COMPRA_PROVEEDOR_ID = "providerid";
    public static final String COMPRA_FECHA = "fecha";
    public static final String COMPRA_SUBTOTAL_X = "subt_exento";
    public static final String COMPRA_SUBTOTAL_NG = "subt_neto_grav";
    public static final String COMPRA_SUBTOTAL_I = "subt_tax";
    public static final String COMPRA_TOTAL = "total";
    public static final String COMPRA_MEDIO_PAGO = "medio_pago";
    public static final String COMPRA_CODIGO = "codigo";
    public static final String COMPRA_USER_ID = "userid";
    public static final String COMPRA_SYNC_STATUS = "sync";

    // script de creacion de tabla
    public static final String COMPRA_CREATE_SCRIPT = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
            COMPRA_ID + " integer primary key autoincrement, " +
            COMPRA_NUMERO_FC + " text, "+
            COMPRA_PROVEEDOR_ID + " int, " +
            COMPRA_FECHA + " datetime, " +
            COMPRA_SUBTOTAL_X + " real, " +
            COMPRA_SUBTOTAL_NG + " real, " +
            COMPRA_SUBTOTAL_I + " real, " +
            COMPRA_TOTAL + " real, " +
            COMPRA_MEDIO_PAGO + " text, " +
            COMPRA_CODIGO + " text, " +
            COMPRA_USER_ID + " int, " +
            COMPRA_SYNC_STATUS + " text default 'N'" +
            ");";

    public static final String[] columnas = new String[]{
            COMPRA_ID,
            COMPRA_NUMERO_FC,
            COMPRA_PROVEEDOR_ID,
            COMPRA_FECHA,
            COMPRA_SUBTOTAL_X,
            COMPRA_SUBTOTAL_NG,
            COMPRA_SUBTOTAL_I,
            COMPRA_TOTAL,
            COMPRA_MEDIO_PAGO,
            COMPRA_CODIGO,
            COMPRA_USER_ID,
            COMPRA_SYNC_STATUS
    };

}
