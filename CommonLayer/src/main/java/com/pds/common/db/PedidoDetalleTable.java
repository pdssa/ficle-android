package com.pds.common.db;

/**
 * Created by EMEKA on 13/01/2015.
 */
public class PedidoDetalleTable {

    //************TABLE DETALLES COMPRA************
    public static final String TABLE_NAME = "pedido_det";

    //columns names
    public static final String PEDIDO_DET_ID = "_id";
    public static final String PEDIDO_DET_PEDIDO_ID = "id_pedido";
    public static final String PEDIDO_DET_PRODUCTO_ID = "id_producto";
    public static final String PEDIDO_DET_CANTIDAD = "cantidad";
    public static final String COLUMN_SYNC_STATUS = "sync";

    // script de creacion de tabla
    public static final String PEDIDO_DET_CREATE_SCRIPT = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
            PEDIDO_DET_ID + " integer primary key autoincrement, " +
            PEDIDO_DET_PEDIDO_ID + " integer not null, " +
            PEDIDO_DET_PRODUCTO_ID + " integer not null, " +
            PEDIDO_DET_CANTIDAD + " integer, " +
            COLUMN_SYNC_STATUS + " text default 'N'" +
            ");";

    public static final String[] columnas = new String[]{
            PEDIDO_DET_ID,
            PEDIDO_DET_PEDIDO_ID,
            PEDIDO_DET_PRODUCTO_ID,
            PEDIDO_DET_CANTIDAD,
            COLUMN_SYNC_STATUS
    };
}
