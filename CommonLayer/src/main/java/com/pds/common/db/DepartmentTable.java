package com.pds.common.db;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DepartmentTable {

    public static final String TABLE_NAME = "departments";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_ALTA = "alta";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_MODO_VISUAL = "visualization";
    public static final String COLUMN_GENERIC = "generic";
    public static final String COLUMN_AUTO = "autom";
    public static final String COLUMN_SYNC_STATUS = "sync";
    public static final String COLUMN_CODIGO = "codigo";

    public static final String DATABASE_CREATE = "create table if not exists "
            + TABLE_NAME + "(" + COLUMN_ID
            + " integer primary key autoincrement, "
            + COLUMN_NAME + " text not null, "
            + COLUMN_DESCRIPTION + " text, "
            + COLUMN_ALTA + " datetime, "
            + COLUMN_MODO_VISUAL + " integer default 0,"
            + COLUMN_GENERIC + " integer default 0,"
            + COLUMN_AUTO + " integer default 0,"
            + COLUMN_SYNC_STATUS + " text default 'N',"
            + COLUMN_CODIGO + " text "
            + ");";

    //script de alter
    public static final String ALTER_SYNC_STATUS_SCRIPT = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + COLUMN_SYNC_STATUS + " text default 'N'";
    public static final String ALTER_SYNC_STATUS_SCRIPT_2 = "UPDATE " + TABLE_NAME + " SET " + COLUMN_SYNC_STATUS + " = 'N'";

    public static final String ALTER_MODO_VISUAL_SCRIPT = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + COLUMN_MODO_VISUAL + " integer default 0";
    public static final String ALTER_MODO_VISUAL_SCRIPT_2 = "UPDATE " + TABLE_NAME + " SET " + COLUMN_MODO_VISUAL + " = 0";
    public static final String ALTER_GENERIC_SCRIPT = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + COLUMN_GENERIC + " integer default 0";
    public static final String ALTER_GENERIC_SCRIPT_2 = "UPDATE " + TABLE_NAME + " SET " + COLUMN_GENERIC + " = 0";

    public static final String ALTER_AUTO_SCRIPT = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + COLUMN_AUTO + " integer default 0";
    public static final String ALTER_AUTO_SCRIPT_2 = "UPDATE " + TABLE_NAME + " SET " + COLUMN_AUTO + " = 0";

    public static final String ALTER_ADD_CODIGO = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + COLUMN_CODIGO + " text";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        Log.w(DepartmentTable.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data"
        );
        //database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(database);
    }
}