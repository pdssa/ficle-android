package com.pds.common.db;

import android.database.sqlite.SQLiteDatabase;

import com.pds.common.model.Promocion;

/**
 * Created by Hernan on 06/06/2016.
 */
public class PromocionTable {

    public static final String TABLE_NAME = "promociones";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NOMBRE = "nombre";
    public static final String COLUMN_CODIGO = "codigo";
    public static final String COLUMN_TEXTO = "texto";
    public static final String COLUMN_VIG_DESDE = "desde";
    public static final String COLUMN_VIG_HASTA = "hasta";
    public static final String COLUMN_VALOR = "valor";
    public static final String COLUMN_TIPO = "tipo";
    public static final String COLUMN_PROD_SKU = "prod_sku";
    public static final String COLUMN_PROD_IMG = "prod_img";
    public static final String COLUMN_BAJA = "baja";
    public static final String COLUMN_SYNC_STATUS = "sync";

    public static final String TABLE_CREATE = "create table if not exists "
            + TABLE_NAME + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_NOMBRE + " text not null, "
            + COLUMN_CODIGO + " text not null, "
            + COLUMN_TEXTO + " text not null, "
            + COLUMN_VIG_DESDE + " datetime, "
            + COLUMN_VIG_HASTA + " datetime, "
            + COLUMN_VALOR + " real default 0, "
            + COLUMN_TIPO + " integer default 0, "
            + COLUMN_PROD_SKU + " text, "
            + COLUMN_PROD_IMG + " text, "
            + COLUMN_BAJA + " integer default 0, "
            + COLUMN_SYNC_STATUS + " text default 'N' "
            + ");";


    /*public static void onCreate(SQLiteDatabase database) {
        database.execSQL(TABLE_CREATE);

        //PROMO ALMACENES
        String leyendaAlmacenes = "Promo <font color='red'><b>2x $990</b></font> combinando este producto<br/>con cualquier otra FANTA, COCA, SPRITE, COCA-ZERO de 1lt RETORNABLE";

        database.execSQL("insert into promociones (nombre, codigo, texto, desde, hasta, valor, tipo, prod_sku, prod_img) values('Gravado', 19);");




        promociones.add(new Promocion(1,"FANTA 1 lt RETORNABLE", "ALM0001-1", leyendaAlmacenes, "7801610002049", "android.resource://com.pds.promos/drawable/fanta_logo", "2016-06-17 18:00:00", "2016-06-19 23:59:59", Promocion.TIPO_PROMOCION.PRECIO_FIJO_VENTA, 990, condicionesAlmacenes));
        promociones.add(new Promocion(0,"SPRITE 1 lt RETORNABLE", "ALM0001-2", leyendaAlmacenes, "7801610005040", "android.resource://com.pds.promos/drawable/sprite_logo", "2016-06-17 18:00:00", "2016-06-19 23:59:59", Promocion.TIPO_PROMOCION.PRECIO_FIJO_VENTA, 990, condicionesAlmacenes));
        promociones.add(new Promocion(0, "COCA-COLA 1 lt RETORNABLE", "ALM0001-3", leyendaAlmacenes, "7801610001042", "android.resource://com.pds.promos/drawable/coca_logo", "2016-06-17 18:00:00", "2016-06-19 23:59:59", Promocion.TIPO_PROMOCION.PRECIO_FIJO_VENTA, 990, condicionesAlmacenes));
        promociones.add(new Promocion(0, "COCA-COLA ZERO 1 lt RETORNABLE", "ALM0001-4", leyendaAlmacenes, "7801610350980", "android.resource://com.pds.promos/drawable/coca_zero_logo", "2016-06-17 18:00:00", "2016-06-19 23:59:59", Promocion.TIPO_PROMOCION.PRECIO_FIJO_VENTA, 990, condicionesAlmacenes));

    }*/


    public static String[] COLUMNAS = {
            COLUMN_ID,
            COLUMN_NOMBRE,
            COLUMN_CODIGO,
            COLUMN_TEXTO,
            COLUMN_VIG_DESDE,
            COLUMN_VIG_HASTA,
            COLUMN_VALOR,
            COLUMN_TIPO,
            COLUMN_PROD_SKU,
            COLUMN_PROD_IMG,
            COLUMN_BAJA,
            COLUMN_SYNC_STATUS
    };
}
