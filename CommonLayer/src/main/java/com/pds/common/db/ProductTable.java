package com.pds.common.db;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.pds.common.VentaDetalleHelper;
import com.pds.common.VentasHelper;

public class ProductTable {

    public static final String TABLE_NAME = "products";
    public static final String VW_TOP_VENTAS_NAME = "vw_top_ventas";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_CODE = "code";
    public static final String COLUMN_DEPARTMENT_ID = "department_id";
    public static final String COLUMN_SUBDEPARTMENT_ID = "subdepartmennt_id";
    public static final String COLUMN_PROVIDER_ID = "provider_id";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_ALTA = "creation_date";
    public static final String COLUMN_STOCK = "stock";
    public static final String COLUMN_BRAND = "brand";
    public static final String COLUMN_PRESENTATION = "presentation";
    public static final String COLUMN_PURCHASE_PRICE = "purchase_price";
    public static final String COLUMN_SALE_PRICE = "sale_price";
    public static final String COLUMN_IVA = "iva";
    public static final String COLUMN_IVA_ENABLED = "iva_enabled";
    public static final String COLUMN_MIN_STOCK = "min_stock";
    public static final String COLUMN_QUICK_ACCESS = "quick_access";
    public static final String COLUMN_WEIGHABLE = "weighable";
    public static final String COLUMN_TAX = "tax_id";
    public static final String COLUMN_UNIT = "unit";
    public static final String COLUMN_REMOVED = "removed";
    public static final String COLUMN_GENERIC = "generic";
    public static final String COLUMN_CONTENIDO = "contenido";
    public static final String COLUMN_IMAGE_URI = "image";
    public static final String COLUMN_ISP_CODE = "ispCode";
    public static final String COLUMN_AUTO = "autom";
    public static final String COLUMN_SYNC_STATUS = "sync";
    public static final String COLUMN_CODIGO_PADRE = "codigo_padre";
    public static final String COLUMN_TYPE = "type";

    public static final String DATABASE_CREATE = "create table if not exists "
            + TABLE_NAME + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_NAME + " text not null, "
            + COLUMN_CODE + " text not null, "
            + COLUMN_DEPARTMENT_ID + " integer not null, "
            + COLUMN_SUBDEPARTMENT_ID + " integer not null, "
            + COLUMN_PROVIDER_ID + " integer not null, "
            + COLUMN_DESCRIPTION + " text not null, "
            + COLUMN_ALTA + " datetime, "
            + COLUMN_STOCK + " real, "
            + COLUMN_BRAND + " text not null, "
            + COLUMN_PRESENTATION + " text, "
            + COLUMN_PURCHASE_PRICE + " real, "
            + COLUMN_SALE_PRICE + " real not null, "
            + COLUMN_IVA + " real not null, "
            + COLUMN_IVA_ENABLED + " integer not null, "
            + COLUMN_MIN_STOCK + " real, "
            + COLUMN_QUICK_ACCESS + " integer default 0, "
            + COLUMN_WEIGHABLE + " integer, "
            + COLUMN_TAX + " integer, "
            + COLUMN_UNIT + " text, "
            + COLUMN_REMOVED + " integer default 0, "
            + COLUMN_GENERIC + " integer default 0, "
            + COLUMN_CONTENIDO + " real, "
            + COLUMN_IMAGE_URI + " text, "
            + COLUMN_ISP_CODE + " text,"
            + COLUMN_AUTO + " integer default 0,"
            + COLUMN_SYNC_STATUS + " text default 'N',"
            + COLUMN_CODIGO_PADRE + " text, "
            + COLUMN_TYPE + " text"
            + ");";

    //script de alter
    public static final String CREATE_VIEW_TOP_VENTAS = "create view if not exists " + VW_TOP_VENTAS_NAME + " as "
            + " select p.* from " + TABLE_NAME + " p "
            + " JOIN ( "
            + "   select id_producto, count(*) from "
            + "   (select det.* "
            + "   from " + VentaDetalleHelper.TABLE_NAME + " det "
            + "   inner join " + VentasHelper.TABLE_NAME + " vta on vta._id = det.id_venta "
            + "   where julianday() - julianday(julianday(substr(fecha, 7)||'-'||substr(fecha,4,2)||'-'||substr(fecha,1,2))) < 30 "
            + "   UNION "
            + "   select cdet.* "
            + "   from " + ComprobanteDetalleTable.TABLE_NAME + " cdet "
            + "   inner join " + ComprobanteTable.TABLE_NAME + " cpb on cpb._id = cdet.id_comprobante "
            + "   where julianday() - julianday(julianday(substr(fecha, 7)||'-'||substr(fecha,4,2)||'-'||substr(fecha,1,2))) < 30 "
            + "   ) where id_producto <> -1 " //NO GENERICO
            + "   group by id_producto "
            + "   order by 2 desc "
            + "   limit 10 ) as vent on p._id = vent.id_producto; ";

    public static String[] COLUMNAS = {
            ProductTable.COLUMN_ID,
            ProductTable.COLUMN_NAME,
            ProductTable.COLUMN_CODE,
            ProductTable.COLUMN_DEPARTMENT_ID,
            ProductTable.COLUMN_SUBDEPARTMENT_ID,
            ProductTable.COLUMN_PROVIDER_ID,
            ProductTable.COLUMN_DESCRIPTION,
            ProductTable.COLUMN_ALTA,
            ProductTable.COLUMN_STOCK,
            ProductTable.COLUMN_BRAND,
            ProductTable.COLUMN_PRESENTATION,
            ProductTable.COLUMN_PURCHASE_PRICE,
            ProductTable.COLUMN_SALE_PRICE,
            ProductTable.COLUMN_IVA,
            ProductTable.COLUMN_IVA_ENABLED,
            ProductTable.COLUMN_MIN_STOCK,
            ProductTable.COLUMN_QUICK_ACCESS,
            ProductTable.COLUMN_WEIGHABLE,
            ProductTable.COLUMN_TAX,
            ProductTable.COLUMN_UNIT,
            ProductTable.COLUMN_CONTENIDO,
            ProductTable.COLUMN_GENERIC,
            ProductTable.COLUMN_IMAGE_URI,
            ProductTable.COLUMN_ISP_CODE,
            ProductTable.COLUMN_AUTO,
            ProductTable.COLUMN_CODIGO_PADRE,
            ProductTable.COLUMN_TYPE,
            ProductTable.COLUMN_REMOVED
    };


    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        Log.w(ProductTable.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data"
        );
        //database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(database);
    }


}