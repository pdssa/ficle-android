package com.pds.common.db;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class TaxTable {

    public static final String TABLE_NAME = "taxes";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_AMOUNT = "amount";

    public static final String DATABASE_CREATE = "create table if not exists "
            + TABLE_NAME + "(" + COLUMN_ID
            + " integer primary key autoincrement, "
            + COLUMN_NAME + " text not null, "
            + COLUMN_AMOUNT + " integer not null"
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
        database.execSQL("insert into taxes (name, amount) values('Gravado', 19);");
        database.execSQL("insert into taxes (name, amount) values('No Gravado', 19);");
        database.execSQL("insert into taxes (name, amount) values('Incluido', 19);");
        database.execSQL("insert into taxes (name, amount) values('Exento', 0);");
    }

    public static final String TAX_SCRIPT_DELETE_TAX = "delete from taxes where _id > 4";
    public static final String TAX_SCRIPT_UPDATE_TAX = "update taxes set amount = 19";
    public static final String TAX_SCRIPT_UPDATE_TAX_2 = "update taxes set amount = 0 where _id = 4";


    public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        Log.w(TaxTable.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data"
        );
        //database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        //onCreate(database);
    }
}