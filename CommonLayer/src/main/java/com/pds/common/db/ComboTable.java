package com.pds.common.db;

/**
 * Created by Hernan on 25/02/2015.
 */
public class ComboTable {

    public static final String TABLE_NAME = "combos";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NOMBRE = "nombre";
    public static final String COLUMN_DESCRIPCION = "descripcion";
    public static final String COLUMN_PRODUCTO_PPAL_ID = "producto_ppal_id";
    public static final String COLUMN_PRODUCTO_PPAL_CODE = "producto_ppal_code";
    public static final String COLUMN_CANTIDAD = "cantidad";
    public static final String COLUMN_PRECIO = "precio";
    public static final String COLUMN_CODIGO = "codigo";
    public static final String COLUMN_FECHA_ALTA = "fecha";
    public static final String COLUMN_BAJA = "baja";
    public static final String COLUMN_PROMOCION_ID = "promocion_id";
    public static final String COLUMN_SYNC_STATUS = "sync";

    public static final String TABLE_CREATE = "create table if not exists "
            + TABLE_NAME + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_NOMBRE + " text not null, "
            + COLUMN_DESCRIPCION + " text, "
            + COLUMN_PRODUCTO_PPAL_ID + " integer not null, "
            + COLUMN_PRODUCTO_PPAL_CODE + " text, "
            + COLUMN_CANTIDAD + " real default 0, "
            + COLUMN_PRECIO + " real default 0, "
            + COLUMN_CODIGO + " text,"
            + COLUMN_FECHA_ALTA + " datetime,"
            + COLUMN_BAJA + " integer default 0, "
            + COLUMN_PROMOCION_ID + " integer default 0, "
            + COLUMN_SYNC_STATUS + " text default 'N'"
            + ");";

    //alter
    public static final String ALTER_ADD_PROMOCION_SCRIPT = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + COLUMN_PROMOCION_ID + " integer default 0" ;
    public static final String ALTER_ADD_PROMOCION_SCRIPT_2 = "UPDATE " + TABLE_NAME + " SET " + COLUMN_PROMOCION_ID + " = 0" ;

    public static final String[] COLUMNAS = {
            COLUMN_ID,
            COLUMN_NOMBRE,
            COLUMN_DESCRIPCION,
            COLUMN_PRODUCTO_PPAL_ID,
            COLUMN_PRODUCTO_PPAL_CODE,
            COLUMN_CANTIDAD,
            COLUMN_PRECIO,
            COLUMN_CODIGO,
            COLUMN_BAJA,
            COLUMN_FECHA_ALTA,
            COLUMN_PROMOCION_ID,
            COLUMN_SYNC_STATUS
    };
}
