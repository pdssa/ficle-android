package com.pds.common.db;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by EMEKA on 22/01/2015.
 */
public class CategoriaTable {

    public static final String TABLE_NAME = "categorias";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NOMBRE = "nombre";
    public static final String COLUMN_DESCRIPCION = "descripcion";
    public static final String COLUMN_CODIGO = "codigo";
    public static final String COLUMN_CODIGO_PADRE = "cod_padre";
    public static final String COLUMN_ULT_NIVEL = "ultimo";
    public static final String COLUMN_NUM_NIVEL = "nivel";

    public static final String TABLE_CREATE = "create table if not exists "
            + TABLE_NAME + "("
            + COLUMN_ID + " integer primary key autoincrement,"
            + COLUMN_NOMBRE + " text , "
            + COLUMN_DESCRIPCION + " text, "
            + COLUMN_CODIGO + " text, "
            + COLUMN_CODIGO_PADRE + " text, "
            + COLUMN_ULT_NIVEL + " integer, "
            + COLUMN_NUM_NIVEL + " integer "
            + ");";

    public static final String[] COLUMNAS = {
            COLUMN_ID,
            COLUMN_NOMBRE,
            COLUMN_DESCRIPCION,
            COLUMN_CODIGO,
            COLUMN_CODIGO_PADRE,
            COLUMN_ULT_NIVEL,
            COLUMN_NUM_NIVEL
    };

    public static void deleteTable(SQLiteDatabase db){
        db.execSQL("DELETE FROM " + TABLE_NAME );
    }
}
