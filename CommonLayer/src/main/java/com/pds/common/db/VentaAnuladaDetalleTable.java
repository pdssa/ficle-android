package com.pds.common.db;

/**
 * Created by Hernan on 24/02/2015.
 */
public class VentaAnuladaDetalleTable {

    public static final String TABLE_NAME = "venta_anulada_det";

    //columns names
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_VENTA_ANULADA_ID = "id_venta_anulada";//FK a la tabla venta_anulada
    public static final String COLUMN_PRODUCTO_ID = "id_producto";
    public static final String COLUMN_CANTIDAD = "cantidad";
    public static final String COLUMN_PRECIO = "precio";
    public static final String COLUMN_TOTAL = "total";
    public static final String COLUMN_SKU = "sku";
    public static final String COLUMN_NETO = "neto";
    public static final String COLUMN_IVA = "iva";
    public static final String COLUMN_SYNC_STATUS = "sync";

    // script de creacion de tabla
    public static final String VENTA_ANULADA_DETALLE_CREATE_SCRIPT = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
            COLUMN_ID + " integer primary key autoincrement, " +
            COLUMN_VENTA_ANULADA_ID  + " integer not null, " +
            COLUMN_PRODUCTO_ID + " integer not null, " +
            COLUMN_CANTIDAD + " integer, " +
            COLUMN_PRECIO + " real, " +
            COLUMN_TOTAL + " real, " +
            COLUMN_SKU + " text, " +
            COLUMN_NETO + " real, " +
            COLUMN_IVA + " real," +
            COLUMN_SYNC_STATUS + " text default 'N' " +
            ");";

    public static final String[] columnas = new String[]{
            COLUMN_ID,
            COLUMN_VENTA_ANULADA_ID,
            COLUMN_PRODUCTO_ID,
            COLUMN_CANTIDAD,
            COLUMN_PRECIO,
            COLUMN_TOTAL,
            COLUMN_SKU,
            COLUMN_NETO,
            COLUMN_IVA,
            COLUMN_SYNC_STATUS
    };
}
