package com.pds.common.db;

import android.util.Log;

import com.pds.common.model.Cliente;

/**
 * Created by Hernan on 19/08/2014.
 */
public class ClienteTable {

    public static final String TABLE_NAME = "clientes";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NOMBRE = "nombre";
    public static final String COLUMN_APELLIDO = "apellido";
    public static final String COLUMN_TELEFONO = "telefono";
    public static final String COLUMN_OBSERVACION = "observacion";
    public static final String COLUMN_LIMITE_CREDITO = "limite";
    public static final String COLUMN_BAJA = "baja";
    public static final String COLUMN_FOTO_PATH = "photo";
    public static final String COLUMN_CLAVE_FISCAL = "clave_fiscal";
    public static final String COLUMN_SYNC_STATUS = "sync";

    public static final String TABLE_CREATE = "create table if not exists "
            + TABLE_NAME + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_NOMBRE + " text not null, "
            + COLUMN_APELLIDO + " text not null, "
            + COLUMN_TELEFONO + " text, "
            + COLUMN_OBSERVACION + " text, "
            + COLUMN_LIMITE_CREDITO + " real default 0, "
            + COLUMN_BAJA + " integer default 0, "
            + COLUMN_FOTO_PATH + " text, "
            + COLUMN_CLAVE_FISCAL + " text, "
            + COLUMN_SYNC_STATUS + " text default 'N' "
            + ");";

    //script de alter
    public static final String ALTER_ADD_CLAVE_FISCAL = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + COLUMN_CLAVE_FISCAL + " text";

    public static final String ALTER_SYNC_STATUS_SCRIPT = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + COLUMN_SYNC_STATUS + " text default 'N'" ;
    public static final String ALTER_SYNC_STATUS_SCRIPT_2 = "UPDATE " + TABLE_NAME + " SET " + COLUMN_SYNC_STATUS + " = 'N'" ;


    public static String[] COLUMNAS = {
            ClienteTable.COLUMN_ID,
            ClienteTable.COLUMN_NOMBRE,
            ClienteTable.COLUMN_APELLIDO,
            ClienteTable.COLUMN_TELEFONO,
            ClienteTable.COLUMN_OBSERVACION,
            ClienteTable.COLUMN_LIMITE_CREDITO,
            ClienteTable.COLUMN_BAJA,
            ClienteTable.COLUMN_FOTO_PATH,
            ClienteTable.COLUMN_CLAVE_FISCAL,
            ClienteTable.COLUMN_SYNC_STATUS
    };
}
