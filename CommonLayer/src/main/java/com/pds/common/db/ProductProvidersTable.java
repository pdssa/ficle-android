package com.pds.common.db;

/**
 * Created by Hernan on 04/11/2014.
 */
public class ProductProvidersTable {
    public static final String TABLE_NAME = "product_providers";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_PRODUCT_ID = "product_id";
    public static final String COLUMN_PROVIDER_ID = "provider_id";

    public static final String CREATE_SCRIPT = "create table if not exists "
            + TABLE_NAME + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_PRODUCT_ID + " integer not null, "
            + COLUMN_PROVIDER_ID + " integer not null "
            + ");";

}
