package com.pds.common.db;

/**
 * Created by Hernan on 22/11/13.
 */
public class CompraDetalleTable {

    //************TABLE DETALLES COMPRA************
    public static final String TABLE_NAME = "compra_det";

    //columns names
    public static final String COMPRA_DET_ID = "_id";
    public static final String COMPRA_DET_COMPRA_ID = "id_compra";
    public static final String COMPRA_DET_PRODUCTO_ID = "id_producto";
    public static final String COMPRA_DET_CANTIDAD = "cantidad";
    public static final String COMPRA_DET_PRECIO = "precio";
    public static final String COMPRA_DET_SUBTOTAL_X = "subt_exento";
    public static final String COMPRA_DET_SUBTOTAL_NG = "subt_neto_grav";
    public static final String COMPRA_DET_SUBTOTAL_I = "subt_tax";
    public static final String COMPRA_DET_TOTAL = "total";
    public static final String COLUMN_SYNC_STATUS = "sync";

    // script de creacion de tabla
    public static final String COMPRA_DET_CREATE_SCRIPT = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
            COMPRA_DET_ID + " integer primary key autoincrement, " +
            COMPRA_DET_COMPRA_ID + " integer not null, " +
            COMPRA_DET_PRODUCTO_ID + " integer not null, " +
            COMPRA_DET_CANTIDAD + " integer, " +
            COMPRA_DET_PRECIO + " real, " +
            COMPRA_DET_SUBTOTAL_X + " real, " +
            COMPRA_DET_SUBTOTAL_NG + " real, " +
            COMPRA_DET_SUBTOTAL_I + " real, " +
            COMPRA_DET_TOTAL + " real, " +
            COLUMN_SYNC_STATUS + " text default 'N'" +
            ");";

    //script de alter
    public static final String COMPRA_DET_ALTER_SYNC_STATUS_SCRIPT = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + COLUMN_SYNC_STATUS + " text default 'N'" ;
    public static final String COMPRA_DET_ALTER_SYNC_STATUS_SCRIPT_2 = "UPDATE " + TABLE_NAME + " SET " + COLUMN_SYNC_STATUS + " = 'N'" ;

    public static final String[] columnas = new String[]{
            COMPRA_DET_ID,
            COMPRA_DET_COMPRA_ID,
            COMPRA_DET_PRODUCTO_ID,
            COMPRA_DET_CANTIDAD,
            COMPRA_DET_PRECIO,
            COMPRA_DET_SUBTOTAL_X,
            COMPRA_DET_SUBTOTAL_NG,
            COMPRA_DET_SUBTOTAL_I,
            COMPRA_DET_TOTAL,
            COLUMN_SYNC_STATUS
    };
}
