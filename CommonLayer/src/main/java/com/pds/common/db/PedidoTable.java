package com.pds.common.db;

/**
 * Created by EMEKA on 13/01/2015.
 */
public class PedidoTable {

    //************TABLE PEDIDOS************
    public static final String TABLE_NAME = "pedido";

    //columns names
    public static final String PEDIDO_ID = "_id";
    public static final String PEDIDO_PROVEEDOR_ID = "providerid";
    public static final String PEDIDO_ESTADO = "estado";
    public static final String PEDIDO_FECHA = "fecha";
    public static final String PEDIDO_USER_ID = "userid";
    public static final String PEDIDO_SYNC_STATUS = "sync";

    // script de creacion de tabla
    public static final String PEDIDO_CREATE_SCRIPT = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
            PEDIDO_ID + " integer primary key autoincrement, " +
            PEDIDO_PROVEEDOR_ID + " int, "+
            PEDIDO_ESTADO + " text, " +
            PEDIDO_FECHA + " datetime, " +
            PEDIDO_USER_ID + " integer," +
            PEDIDO_SYNC_STATUS + " text default 'N'" +
            ");";

    public static final String[] columnas = new String[]{
            PEDIDO_ID,
            PEDIDO_PROVEEDOR_ID,
            PEDIDO_ESTADO,
            PEDIDO_FECHA,
            PEDIDO_USER_ID,
            PEDIDO_SYNC_STATUS
    };
}
