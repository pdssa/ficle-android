package com.pds.common.db;

/**
 * Created by EMEKA on 21/01/2015.
 */
public class HistMovStockTable {
    //************TABLE HistMovStockTable************
    public static final String TABLE_NAME = "historial_movimientos_stock";

    //columns names
    public static final String STOCK_ID = "_id";
    public static final String STOCK_PRODUCTO_ID = "productoid";
    public static final String STOCK_CANTIDAD = "cantidad";
    public static final String STOCK_MONTO = "monto";
    public static final String STOCK_OPERACION = "operacion";
    public static final String STOCK_STOCK = "hist_stock";
    public static final String STOCK_OBSERVACION = "observacion";
    public static final String STOCK_FECHA = "fecha";
    public static final String STOCK_USUARIO = "usuario";

    // script de creacion de tabla
    public static final String TABLE_CREATE_SCRIPT = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
            STOCK_ID + " integer primary key autoincrement, " +
            STOCK_PRODUCTO_ID + " int, "+
            STOCK_CANTIDAD + " int, " +
            STOCK_MONTO + " real not null, " +
            STOCK_OPERACION + " text, " +
            STOCK_STOCK + " int," +
            STOCK_OBSERVACION + " text," +
            STOCK_FECHA + " datetime," +
            STOCK_USUARIO + " text" +
            ");";

    public static final String STOCK_ALTER_ADD_STOCK = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + STOCK_STOCK + " int";

    public static final String STOCK_ALTER_ADD_OBSERVACION = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + STOCK_OBSERVACION + " text";
    public static final String STOCK_ALTER_ADD_FECHA = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + STOCK_FECHA + " datetime";
    public static final String STOCK_ALTER_ADD_USUARIO = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + STOCK_USUARIO + " text";

    public static final String[] columnas = new String[]{
            STOCK_ID,
            STOCK_PRODUCTO_ID,
            STOCK_CANTIDAD,
            STOCK_MONTO,
            STOCK_OPERACION,
            STOCK_STOCK,
            STOCK_OBSERVACION,
            STOCK_FECHA,
            STOCK_USUARIO
    };
}
