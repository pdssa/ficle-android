package com.pds.common.db;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class AlicuotasTable {

    public static final String TABLE_NAME = "alicuotas";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_AMOUNT = "amount";

    public static final String DATABASE_CREATE = "create table if not exists "
            + TABLE_NAME + "(" + COLUMN_ID
            + " integer primary key autoincrement, "
            + COLUMN_NAME + " text not null, "
            + COLUMN_AMOUNT + " real not null"
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
        database.execSQL("insert into alicuotas (name, amount) values('21 %', 21);");
        database.execSQL("insert into alicuotas (name, amount) values('10.5 %', 10.5);");
        database.execSQL("insert into alicuotas (name, amount) values('0 %', 0);");
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        Log.w(AlicuotasTable.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data"
        );
        //database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(database);
    }
}