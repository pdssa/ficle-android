package com.pds.common.db;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by raul.lopez on 5/18/2014.
 */
public class ProviderTable {
    public static final String TABLE_NAME = "providers";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_ALTA = "alta";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_DEPARTMENT_ID = "department_id";
    public static final String COLUMN_SUBDEPARTMENT_ID = "subdepartment_id";
    public static final String COLUMN_CONTACT = "contact";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_ADDRESS = "address";
    public static final String COLUMN_CITY = "city";
    public static final String COLUMN_PHONE_NUMBER = "phone_number";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_SYNC_STATUS = "sync";
    public static final String COLUMN_TAXID = "tax_id";

    public static final String DATABASE_CREATE = "create table if not exists "
            + TABLE_NAME + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_NAME + " text not null, "
            + COLUMN_ALTA + " datetime, "
            + COLUMN_EMAIL + " text, "
            + COLUMN_CONTACT + " text, "
            + COLUMN_ADDRESS + " text, "
            + COLUMN_CITY + " text, "
            + COLUMN_DEPARTMENT_ID + " integer not null, "
            + COLUMN_SUBDEPARTMENT_ID + " integer not null, "
            + COLUMN_PHONE_NUMBER + " text, "
            + COLUMN_DESCRIPTION + " text, "
            + COLUMN_SYNC_STATUS + " text default 'N',"
            + COLUMN_TAXID + " text"
            + ");";

    //script de alter
    public static final String ALTER_SYNC_STATUS_SCRIPT = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + COLUMN_SYNC_STATUS + " text default 'N'" ;
    public static final String ALTER_SYNC_STATUS_SCRIPT_2 = "UPDATE " + TABLE_NAME + " SET " + COLUMN_SYNC_STATUS + " = 'N'" ;

    public static final String ALTER_TAX_ID_SCRIPT = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + COLUMN_TAXID + " text " ;

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        Log.w(ProviderTable.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data"
        );
        //database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(database);
    }
}
