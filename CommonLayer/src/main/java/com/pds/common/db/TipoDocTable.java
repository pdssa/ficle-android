package com.pds.common.db;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Hernan on 17/12/2014.
 */
public class TipoDocTable {
    public static final String TABLE_NAME = "tipo_doc";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NOMBRE = "nombre";
    public static final String COLUMN_CODIGO = "codigo";
    public static final String COLUMN_HABILITADO = "habilitado";

    public static final String TABLE_CREATE = "create table if not exists "
            + TABLE_NAME + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_NOMBRE + " text, "
            + COLUMN_CODIGO + " text, "
            + COLUMN_HABILITADO + " integer default 0"
            + ");";

    public static final String[] COLUMNAS = new String[]{
            COLUMN_ID,
            COLUMN_NOMBRE,
            COLUMN_CODIGO,
            COLUMN_HABILITADO
    };

    public static void deleteTable(SQLiteDatabase db){
        db.execSQL("DELETE FROM " + TABLE_NAME );
    }
}
