package com.pds.common.db;

/**
 * Created by Hernan on 25/02/2015.
 */
public class ComboItemTable {
    public static final String TABLE_NAME = "combos_item";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_COMBO_ID = "combo_id";
    public static final String COLUMN_PRODUCTO_ID = "producto_id";
    public static final String COLUMN_PRODUCTO_CODE = "producto_code";
    public static final String COLUMN_CANTIDAD = "cantidad";
    public static final String COLUMN_SYNC_STATUS = "sync";

    public static final String TABLE_CREATE = "create table if not exists "
            + TABLE_NAME + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_COMBO_ID + " integer not null, "
            + COLUMN_PRODUCTO_ID + " integer not null, "
            + COLUMN_PRODUCTO_CODE + " text, "
            + COLUMN_CANTIDAD + " real default 0, "
            + COLUMN_SYNC_STATUS + " text default 'N'"
            + ");";

    public static final String[] COLUMNAS = {
            COLUMN_ID,
            COLUMN_COMBO_ID,
            COLUMN_PRODUCTO_ID,
            COLUMN_PRODUCTO_CODE,
            COLUMN_CANTIDAD,
            COLUMN_SYNC_STATUS
    };
}
