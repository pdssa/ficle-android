package com.pds.common.db;

/**
 * Created by Hernan on 22/08/2014.
 */
public class CuentaTable {

    //columns names
    public static final String TABLE_NAME = "ctacte";
    public static final String VW_TOTALES_NAME = "ctacte_totales";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_TIPO_MOV = "tipomov";
    public static final String COLUMN_FECHA_HORA = "fecha";
    public static final String COLUMN_MONTO = "monto";
    public static final String COLUMN_VENTA_ID = "id_venta";
    public static final String COLUMN_CLIENTE_ID = "id_cliente";
    public static final String COLUMN_OBSERVACION = "observacion";
    public static final String COLUMN_TIPO_VTA = "tipoVta";
    public static final String COLUMN_SYNC_STATUS = "sync";

    public static final String COLUMN_VW_CLIENTE_ID = "id_cliente";
    public static final String COLUMN_VW_SALDO = "saldo";
    public static final String COLUMN_VW_FECHA_ULT_PAGO = "fec_ult_pago";
    public static final String COLUMN_VW_MONTO_ULT_PAGO = "mon_ult_pago";

    // script de creacion de tabla
    public static final String TABLE_CREATE_SCRIPT = "CREATE TABLE IF NOT EXISTS " +
            TABLE_NAME + "(" +
            COLUMN_ID + " integer primary key autoincrement, " +
            COLUMN_TIPO_MOV + " integer not null, " +
            COLUMN_FECHA_HORA + " datetime, " +
            COLUMN_MONTO + " real, " +
            COLUMN_VENTA_ID + " integer, " +
            COLUMN_CLIENTE_ID + " integer, " +
            COLUMN_OBSERVACION + " text, " +
            COLUMN_TIPO_VTA + " text default 'VTA', " +
            COLUMN_SYNC_STATUS + " text default 'N'" +
            ");";

    //script de alter
    public static final String ALTER_SYNC_STATUS_SCRIPT = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + COLUMN_TIPO_VTA + " text default 'VTA'" ;
    public static final String ALTER_SYNC_STATUS_SCRIPT_2 = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + COLUMN_SYNC_STATUS + " text default 'N'" ;
    public static final String ALTER_SYNC_STATUS_SCRIPT_3 = "UPDATE " + TABLE_NAME + " SET " + COLUMN_TIPO_VTA + " = 'VTA', " + COLUMN_SYNC_STATUS + " = 'N'" ;

    //script de creacion de vista de totales
    public static final String VIEW_CREATE_SCRIPT = "CREATE VIEW IF NOT EXISTS " +
            VW_TOTALES_NAME + " AS " +
            " select a.id_cliente, " +
            " (select sum(case z.tipomov when 0 then z.monto else 0 end) - sum(case z.tipomov when 1 then z.monto else 0 end) from ctacte z where a.id_cliente = z.id_cliente) saldo," +
            " b.fec_ult_pago,b.mon_ult_pago from ctacte a " +
            " left outer join (select y.id_cliente, y.fecha fec_ult_pago, y.monto mon_ult_pago from ctacte y where y.tipomov = 1 and " +
            "    y.fecha = (select max(x.fecha) from ctacte x where x.tipomov = 1 and x.id_cliente = y.id_cliente)) b on a.id_cliente = b.id_cliente " +
            " group by a.id_cliente ";

    public static String[] COLUMNAS = {
            CuentaTable.COLUMN_ID,
            CuentaTable.COLUMN_TIPO_MOV,
            CuentaTable.COLUMN_FECHA_HORA,
            CuentaTable.COLUMN_MONTO,
            CuentaTable.COLUMN_VENTA_ID,
            CuentaTable.COLUMN_CLIENTE_ID,
            CuentaTable.COLUMN_OBSERVACION,
            CuentaTable.COLUMN_TIPO_VTA,
            CuentaTable.COLUMN_SYNC_STATUS
    };
}
