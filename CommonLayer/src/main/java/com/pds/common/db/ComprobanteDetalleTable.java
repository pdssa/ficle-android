package com.pds.common.db;

/**
 * Created by Hernan on 22/11/13.
 */
public class ComprobanteDetalleTable {

    //************TABLE COMPROBANTE_DET************
    public static final String TABLE_NAME = "comprobante_det";
    //columns names
    public static final String COMPROBANTE_DET_ID = "_id";
    public static final String COMPROBANTE_DET_COMPROBANTE_ID = "id_comprobante";
    public static final String COMPROBANTE_DET_PRODUCTO_ID = "id_producto";
    public static final String COMPROBANTE_DET_CANTIDAD = "cantidad";
    public static final String COMPROBANTE_DET_PRECIO = "precio";
    public static final String COMPROBANTE_DET_TOTAL = "total";
    public static final String COMPROBANTE_DET_SKU = "sku";
    public static final String COMPROBANTE_DET_NETO = "neto";
    public static final String COMPROBANTE_DET_IVA = "iva";
    public static final String COLUMN_SYNC_STATUS = "sync";

    //columns indexes
    public static final int COMPROBANTE_DET_IX_ID = 0;
    public static final int COMPROBANTE_DET_IX_COMPROBANTE_ID = 1;
    public static final int COMPROBANTE_DET_IX_PRODUCTO_ID = 2;
    public static final int COMPROBANTE_DET_IX_CANTIDAD = 3;
    public static final int COMPROBANTE_DET_IX_PRECIO = 4;
    public static final int COMPROBANTE_DET_IX_TOTAL = 5;
    public static final int COMPROBANTE_DET_IX_SKU = 6;
    public static final int COMPROBANTE_DET_IX_NETO = 7;
    public static final int COMPROBANTE_DET_IX_IVA = 8;

    // script de creacion de tabla
    public static final String COMPROBANTE_DET_CREATE_SCRIPT = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
            COMPROBANTE_DET_ID + " integer primary key autoincrement, " +
            COMPROBANTE_DET_COMPROBANTE_ID  + " integer not null, " +
            COMPROBANTE_DET_PRODUCTO_ID + " integer not null, " +
            COMPROBANTE_DET_CANTIDAD + " integer, " +
            COMPROBANTE_DET_PRECIO + " real, " +
            COMPROBANTE_DET_TOTAL + " real, " +
            COMPROBANTE_DET_SKU + " text, " +
            COMPROBANTE_DET_NETO + " real, " +
            COMPROBANTE_DET_IVA + " real," +
            COLUMN_SYNC_STATUS + " text default 'N' " +
            ");";

    //script de alter
    public static final String COMPROBANTE_DET_ALTER_SYNC_STATUS_SCRIPT = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + COLUMN_SYNC_STATUS + " text default 'N'" ;
    public static final String COMPROBANTE_DET_ALTER_SYNC_STATUS_SCRIPT_2 = "UPDATE " + TABLE_NAME + " SET " + COLUMN_SYNC_STATUS + " = 'N'" ;

    public static final String COMPROBANTE_DET_ALTER_SKU_SCRIPT = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + COMPROBANTE_DET_SKU + " text " ;

    public static final String COMPROBANTE_DET_ALTER_NETO_SCRIPT = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + COMPROBANTE_DET_NETO + " real " ;
    public static final String COMPROBANTE_DET_ALTER_IVA_SCRIPT = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + COMPROBANTE_DET_IVA + " real " ;

    public static final String[] columnas = new String[]{
            COMPROBANTE_DET_ID,
            COMPROBANTE_DET_COMPROBANTE_ID,
            COMPROBANTE_DET_PRODUCTO_ID,
            COMPROBANTE_DET_CANTIDAD,
            COMPROBANTE_DET_PRECIO,
            COMPROBANTE_DET_TOTAL,
            COMPROBANTE_DET_SKU,
            COMPROBANTE_DET_NETO,
            COMPROBANTE_DET_IVA,
            COLUMN_SYNC_STATUS
    };
}
