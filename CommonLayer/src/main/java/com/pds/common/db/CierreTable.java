package com.pds.common.db;

/**
 * Created by Hernan on 10/12/2014.
 */
public class CierreTable {
    public static final String TABLE_NAME = "cierres";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_FECHA_CIERRE = "fecha";
    public static final String COLUMN_AFECTAS_Q = "afectas_q";
    public static final String COLUMN_AFECTAS_T = "afectas_t";
    public static final String COLUMN_EXENTAS_Q = "exentas_q";
    public static final String COLUMN_EXENTAS_T = "exentas_t";
    public static final String COLUMN_MENORES_Q = "menores_q";
    public static final String COLUMN_MENORES_T = "menores_t";
    public static final String COLUMN_OTROS_Q = "otros_q";
    public static final String COLUMN_OTROS_T = "otros_t";
    public static final String COLUMN_IVA = "iva";
    public static final String COLUMN_FECHA_DESDE = "fecha_desde";

    public static final String TABLE_CREATE = "create table if not exists "
            + TABLE_NAME + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_FECHA_CIERRE + " datetime, "
            + COLUMN_AFECTAS_Q + " integer default 0, "
            + COLUMN_AFECTAS_T + " real default 0, "
            + COLUMN_EXENTAS_Q + " integer default 0, "
            + COLUMN_EXENTAS_T + " real default 0, "
            + COLUMN_MENORES_Q + " integer default 0, "
            + COLUMN_MENORES_T + " real default 0 ,"
            + COLUMN_OTROS_Q + " integer default 0,"
            + COLUMN_OTROS_T + " real default 0,"
            + COLUMN_IVA + " real default 0,"
            + COLUMN_FECHA_DESDE + " datetime "
            + ");";

    public static final String CIERRE_ALTER_ADD_FECHA_DESDE = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + COLUMN_FECHA_DESDE + " datetime";

    public static final String CIERRE_FEC_DESDE_UPDATE = "update cierres set fecha_desde = ifnull((select c.fecha from cierres c where c._id = cierres._id - 1),'2014-01-01 00:00:00')";

    public static final String[] COLUMNAS = new String[]{
            COLUMN_ID,
            COLUMN_FECHA_CIERRE,
            COLUMN_AFECTAS_Q,
            COLUMN_AFECTAS_T,
            COLUMN_EXENTAS_Q,
            COLUMN_EXENTAS_T,
            COLUMN_MENORES_Q,
            COLUMN_MENORES_T,
            COLUMN_OTROS_Q,
            COLUMN_OTROS_T,
            COLUMN_IVA,
            COLUMN_FECHA_DESDE
    };
}
