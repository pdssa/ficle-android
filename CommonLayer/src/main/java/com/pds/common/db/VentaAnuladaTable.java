package com.pds.common.db;

/**
 * Created by Hernan on 24/02/2015.
 */
public class VentaAnuladaTable {

    public static final String TABLE_NAME = "venta_anulada";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_ID_ORIGINAL = "id_original";
    public static final String COLUMN_TYPE_ORIGINAL = "type_original";
    public static final String COLUMN_TOTAL = "total";
    public static final String COLUMN_CANTIDAD = "cantidad";
    public static final String COLUMN_FECHA = "fecha";
    public static final String COLUMN_HORA = "hora";
    public static final String COLUMN_MEDIO_PAGO = "medio_pago";
    public static final String COLUMN_CODIGO = "codigo";
    public static final String COLUMN_USERID = "userid";
    public static final String COLUMN_FC_TYPE = "fc_type";
    public static final String COLUMN_FC_ID = "fc_id";
    public static final String COLUMN_FC_FOLIO = "fc_folio";
    public static final String COLUMN_FC_VALIDATION = "fc_v";
    public static final String COLUMN_NETO = "neto";
    public static final String COLUMN_IVA = "iva";
    public static final String COLUMN_TIMBRE = "timbre";
    public static final String COLUMN_SYNC_STATUS = "sync";

    // script de creacion de tabla
    public static final String VENTA_ANULADA_CREATE_SCRIPT = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
            COLUMN_ID + " integer primary key autoincrement, " +
            COLUMN_ID_ORIGINAL + " integer not null, " +
            COLUMN_TYPE_ORIGINAL + " text not null, " +
            COLUMN_TOTAL + " real, " +
            COLUMN_CANTIDAD + " int," +
            COLUMN_FECHA + " text, " +
            COLUMN_HORA + " text, " +
            COLUMN_MEDIO_PAGO + " text, " +
            COLUMN_CODIGO + " text, " +
            COLUMN_USERID + " int, " +
            COLUMN_FC_TYPE + " text, " +
            COLUMN_FC_ID + " int, " +
            COLUMN_FC_FOLIO + " text, " +
            COLUMN_FC_VALIDATION + " text, " +
            COLUMN_NETO + " real, " +
            COLUMN_IVA + " real, " +
            COLUMN_TIMBRE + " text, " +
            COLUMN_SYNC_STATUS + " text default 'N'" +
            ");";

    public static final String[] columnas = new String[]{
            COLUMN_ID,
            COLUMN_TOTAL,
            COLUMN_CANTIDAD,
            COLUMN_FECHA,
            COLUMN_HORA,
            COLUMN_MEDIO_PAGO,
            COLUMN_CODIGO,
            COLUMN_USERID,
            COLUMN_FC_TYPE,
            COLUMN_FC_ID,
            COLUMN_FC_FOLIO,
            COLUMN_FC_VALIDATION,
            COLUMN_NETO,
            COLUMN_IVA,
            COLUMN_SYNC_STATUS,
            COLUMN_TIMBRE
    };
}
