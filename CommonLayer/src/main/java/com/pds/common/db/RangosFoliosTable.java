package com.pds.common.db;

/**
 * Created by Hernan on 17/12/2014.
 */
public class RangosFoliosTable {

    public static final String TABLE_NAME = "rangos_folios";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_TIPO_DOC = "tipo_doc";
    public static final String COLUMN_DESDE = "desde";
    public static final String COLUMN_HASTA = "hasta";
    public static final String COLUMN_ULTIMO = "ultimo";
    public static final String COLUMN_CAF = "caf";
    public static final String COLUMN_FECHA_REFERENCIA = "fec_ref";

    public static final String TABLE_CREATE = "create table if not exists "
            + TABLE_NAME + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_TIPO_DOC + " text, "
            + COLUMN_DESDE + " integer default 0, "
            + COLUMN_HASTA + " integer default 0, "
            + COLUMN_ULTIMO + " integer default 0, "
            + COLUMN_CAF + " text,"
            + COLUMN_FECHA_REFERENCIA + " datetime"
            + ");";

    public static final String ADD_COLUMN_FECHA_REFERENCIA = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + COLUMN_FECHA_REFERENCIA + " datetime";

    public static final String[] COLUMNAS = new String[]{
            COLUMN_ID,
            COLUMN_TIPO_DOC,
            COLUMN_DESDE,
            COLUMN_HASTA,
            COLUMN_ULTIMO,
            COLUMN_CAF,
            COLUMN_FECHA_REFERENCIA
    };
}
