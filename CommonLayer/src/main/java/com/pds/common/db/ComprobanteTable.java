package com.pds.common.db;

/**
 * Created by Hernan on 22/11/13.
 */
public class ComprobanteTable {

    //************TABLE COMPROBANTES************
    public static final String TABLE_NAME = "comprobantes";

    //columns names
    public static final String COMPROBANTE_ID = "_id";
    public static final String COMPROBANTE_TOTAL = "total";
    public static final String COMPROBANTE_CANTIDAD = "cantidad";
    public static final String COMPROBANTE_FECHA = "fecha";
    public static final String COMPROBANTE_HORA = "hora";
    public static final String COMPROBANTE_MEDIO_PAGO = "medio_pago";
    public static final String COMPROBANTE_CODIGO = "codigo";
    public static final String COMPROBANTE_USERID = "userid";
    public static final String COMPROBANTE_NETO = "neto";
    public static final String COMPROBANTE_IVA = "iva";
    public static final String COMPROBANTE_SYNC_STATUS = "sync";
    public static final String COMPROBANTE_ANULADA = "anulada";

    //columns indexes
    public static final int COMPROBANTE_IX_ID = 0;
    public static final int COMPROBANTE_IX_TOTAL = 1;
    public static final int COMPROBANTE_IX_CANTIDAD = 2;
    public static final int COMPROBANTE_IX_FECHA = 3;
    public static final int COMPROBANTE_IX_HORA = 4;
    public static final int COMPROBANTE_IX_MEDIO_PAGO = 5;
    public static final int COMPROBANTE_IX_CODIGO = 6;
    public static final int COMPROBANTE_IX_USERID = 7;
    public static final int COMPROBANTE_IX_NETO = 8;
    public static final int COMPROBANTE_IX_IVA = 9;
    public static final int COMPROBANTE_IX_SYNC_STATUS = 10;
    public static final int COMPROBANTE_IX_ANULADA = 11;

    public static final String COMPROBANTE_ALTER_NETO = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + COMPROBANTE_NETO + " real";
    public static final String COMPROBANTE_ALTER_IVA = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + COMPROBANTE_IVA + " real";
    public static final String COMPROBANTE_ALTER_ANULADA = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + COMPROBANTE_ANULADA + " int default 0";

    // script de creacion de tabla
    public static final String COMPROBANTE_CREATE_SCRIPT = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
            COMPROBANTE_ID + " integer primary key autoincrement, " +
            COMPROBANTE_TOTAL + " real, " +
            COMPROBANTE_CANTIDAD + " int," +
            COMPROBANTE_FECHA + " text, " +
            COMPROBANTE_HORA + " text, " +
            COMPROBANTE_MEDIO_PAGO + " text, " +
            COMPROBANTE_CODIGO + " text, " +
            COMPROBANTE_USERID + " int, " +
            COMPROBANTE_NETO + " real, " +
            COMPROBANTE_IVA + " real, " +
            COMPROBANTE_SYNC_STATUS + " text default 'N'," +
            COMPROBANTE_ANULADA + " int default 0" +
            ");";

    public static final String[] columnas = new String[]{
            COMPROBANTE_ID,
            COMPROBANTE_TOTAL,
            COMPROBANTE_CANTIDAD,
            COMPROBANTE_FECHA,
            COMPROBANTE_HORA,
            COMPROBANTE_MEDIO_PAGO,
            COMPROBANTE_CODIGO,
            COMPROBANTE_USERID,
            COMPROBANTE_NETO,
            COMPROBANTE_IVA,
            COMPROBANTE_SYNC_STATUS,
            COMPROBANTE_ANULADA
    };

}
