package com.pds.common.services;

import android.content.Context;
import android.content.Intent;

/**
 * Created by Hernan on 13/07/2016.
 */
public class PingServiceIntent {

    private static final String PING_SERVICE_ACTION = "com.pds.serv.ping.logged";

    private boolean logged;

    public PingServiceIntent(boolean logged) {
        this.logged = logged;
    }

    public void send(Context context) {
        Intent intent = new Intent(PING_SERVICE_ACTION);
        intent.putExtra("loggedin", logged);
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);

        /*final Intent intent=new Intent();
        intent.setAction(PING_SERVICE_ACTION);
        intent.putExtra("loggedin",logged);
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        intent.setComponent(
                new ComponentName("com.pds.serv.ping","com.pds.serv.ping.receivers.BroadcastReceiverOnLogin"));
        */

        context.sendBroadcast(intent);
    }

}
