package com.pds.common.dao;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;

import com.pds.common.db.SubdepartmentTable;
import com.pds.common.model.SubDepartment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

public class SubDepartmentDao extends GenericDao<SubDepartment> {
    public static final String[] PROJECTION_ATTRIBUTES = {
            SubdepartmentTable.COLUMN_ID,
            SubdepartmentTable.COLUMN_NAME,
            SubdepartmentTable.COLUMN_ALTA,
            SubdepartmentTable.COLUMN_DESCRIPTION,
            SubdepartmentTable.COLUMN_DEPARTMENT_ID,
            SubdepartmentTable.COLUMN_MODO_VISUAL,
            SubdepartmentTable.COLUMN_GENERIC,
            SubdepartmentTable.COLUMN_AUTO,
            SubdepartmentTable.COLUMN_CODIGO,
            SubdepartmentTable.COLUMN_CODIGO_PADRE
    };

    private DepartmentDao departmentDao;

    private final SimpleDateFormat altaDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public SubDepartmentDao(ContentResolver contentResolver) {
        super(contentResolver);
        this.departmentDao = new DepartmentDao(contentResolver);
    }

    @Override
    protected boolean logEvent() {
        return false;
    }
    @Override
    protected String getModelName() {
        return "Sub Departamento";
    }

    @Override
    public String getTableName() {
        return "subdepartments";
    }

    @Override
    protected ContentValues toContentValues(SubDepartment instance) {
        ContentValues subDepartmentValues = new ContentValues();
        subDepartmentValues.put(SubdepartmentTable.COLUMN_NAME, instance.getName());
        subDepartmentValues.put(SubdepartmentTable.COLUMN_ALTA, altaDateFormat.format(instance.getAlta()));
        subDepartmentValues.put(SubdepartmentTable.COLUMN_DESCRIPTION, instance.getDescription());
        long departmentId = instance.getDepartment() != null ? instance.getDepartment().getId() : 0;
        subDepartmentValues.put(SubdepartmentTable.COLUMN_DEPARTMENT_ID, departmentId);
        subDepartmentValues.put(SubdepartmentTable.COLUMN_SYNC_STATUS, "N");
        subDepartmentValues.put(SubdepartmentTable.COLUMN_MODO_VISUAL, instance.getVisualization_mode());
        subDepartmentValues.put(SubdepartmentTable.COLUMN_GENERIC, instance.getGeneric_subdept());
        subDepartmentValues.put(SubdepartmentTable.COLUMN_AUTO, instance.getAutomatic());
        subDepartmentValues.put(SubdepartmentTable.COLUMN_CODIGO, instance.getCodigo());
        subDepartmentValues.put(SubdepartmentTable.COLUMN_CODIGO_PADRE, instance.getCodigoPadre());

        return subDepartmentValues;
    }

    @Override
    protected SubDepartment fromCursor(Cursor cursor) {
        SubDepartment subDepartment = new SubDepartment();
        Long id = cursor.getLong(cursor.getColumnIndex(SubdepartmentTable.COLUMN_ID));
        String name = cursor.getString(cursor.getColumnIndex(SubdepartmentTable.COLUMN_NAME));
        String alta = cursor.getString(cursor.getColumnIndex(SubdepartmentTable.COLUMN_ALTA));
        String description = cursor.getString(cursor.getColumnIndex(SubdepartmentTable.COLUMN_DESCRIPTION));
        Long departmentId = cursor.getLong(cursor.getColumnIndex(SubdepartmentTable.COLUMN_DEPARTMENT_ID));
        subDepartment.setId(id);
        subDepartment.setName(name);
        subDepartment.setDescription(description);
        subDepartment.setDepartment(departmentDao.find(departmentId));

        try {
            subDepartment.setAlta(altaDateFormat.parse(alta));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        subDepartment.setVisualization_mode(cursor.getInt(cursor.getColumnIndex(SubdepartmentTable.COLUMN_MODO_VISUAL)));
        subDepartment.setGeneric_subdept(cursor.getInt(cursor.getColumnIndex(SubdepartmentTable.COLUMN_GENERIC)));
        subDepartment.setAutomatic(cursor.getInt(cursor.getColumnIndex(SubdepartmentTable.COLUMN_AUTO)));
        subDepartment.setCodigo(cursor.getString(cursor.getColumnIndex(SubdepartmentTable.COLUMN_CODIGO)));
        subDepartment.setCodigoPadre(cursor.getString(cursor.getColumnIndex(SubdepartmentTable.COLUMN_CODIGO_PADRE)));

        return subDepartment;
    }

    @Override
    protected String[] getProjectionAttributes() {
        return PROJECTION_ATTRIBUTES;
    }

    public boolean importSubDepartment(SubDepartment instance){

        //USAMOS EL CODIGO PARA CHEQUEAR SI EXISTE
        List<SubDepartment> result = list(SubdepartmentTable.COLUMN_CODIGO +"=?", new String[]{instance.getCodigo()}, null);

        if(result.size() == 0){
            //NO EXISTE => CREAR
            return save(instance);
        }
        else{
            //encontramos subdepto ...entonces reemplazamos para normalizar nombres
            instance.setId(result.get(0).getId());
            instance.setAlta(result.get(0).getAlta());

            return saveOrUpdate(instance);
        }
        /*else {

            result = list(SubdepartmentTable.COLUMN_NAME +"=?", new String[]{instance.getName()}, null);

            if(result.size() != 0){
                //encontramos subdepto por el nombre...entonces reemplazamos con el import
                instance.setId(result.get(0).getId());

                return saveOrUpdate(instance);
            }
            else
                return false;*/
        //}
    }
}