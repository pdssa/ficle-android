package com.pds.common.dao;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;

import com.pds.common.db.TipoDocTable;
import com.pds.common.model.TipoDoc;

/**
 * Created by Hernan on 17/12/2014.
 */
public class TipoDocDao extends GenericDao<TipoDoc> {

    public TipoDocDao(ContentResolver contentResolver) {
        super(contentResolver);
    }
    @Override
    protected boolean logEvent() {
        return true;
    }
    @Override
    protected String getModelName() {
        return "TipoDoc";
    }

    @Override
    public String getTableName() {
        return TipoDocTable.TABLE_NAME;
    }

    @Override
    protected ContentValues toContentValues(TipoDoc instance) {
        ContentValues values = new ContentValues();
        values.put(TipoDocTable.COLUMN_NOMBRE, instance.getNombre());
        values.put(TipoDocTable.COLUMN_CODIGO, instance.getCodigo());
        values.put(TipoDocTable.COLUMN_HABILITADO, instance.isHabilitado() ? 1 : 0);
        return values;
    }

    @Override
    protected TipoDoc fromCursor(Cursor cursor) {
        TipoDoc instance = new TipoDoc();
        int i = 0;

        Long id = cursor.getLong(i++);
        String nombre = cursor.getString(i++);
        String codigo = cursor.getString(i++);
        int habilitado = cursor.getInt(i++);

        instance.setId(id);
        instance.setNombre(nombre);
        instance.setCodigo(codigo);
        instance.setHabilitado(habilitado == 1);

        return instance;
    }

    @Override
    protected String[] getProjectionAttributes() {
        return TipoDocTable.COLUMNAS;
    }
}
