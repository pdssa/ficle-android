package com.pds.common.dao;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;

import com.pds.common.db.ComprobanteDetalleTable;
import com.pds.common.model.ComprobanteDetalle;


public class ComprobanteDetalleDao extends GenericDao<ComprobanteDetalle> {

    private static final String[] PROJECTION_ATTRIBUTES = ComprobanteDetalleTable.columnas;

    private ProductDao productDao;
    private ComprobanteDao comprobanteDao;

    public ComprobanteDetalleDao(ContentResolver contentResolver) {
        super(contentResolver);
        this.productDao = new ProductDao(contentResolver);
        this.comprobanteDao = new ComprobanteDao(contentResolver);
    }

    @Override
    protected boolean logEvent() {
        return false;
    }

    @Override
    protected String getModelName() {
        return "ComprobanteDetalle";
    }

    @Override
    public String getTableName() {
        return ComprobanteDetalleTable.TABLE_NAME;
    }

    @Override
    protected ContentValues toContentValues(ComprobanteDetalle instance) {
        ContentValues detalleValues = new ContentValues();

        long COMPROBANTEId = instance.getComprobante() != null ? instance.getComprobante().getId() : 0;
        //long productoId = instance.getProducto() != null ? instance.getProducto().getId() : -1;

        detalleValues.put(ComprobanteDetalleTable.COMPROBANTE_DET_COMPROBANTE_ID, COMPROBANTEId);
        detalleValues.put(ComprobanteDetalleTable.COMPROBANTE_DET_PRODUCTO_ID, instance.getIdProducto());
        detalleValues.put(ComprobanteDetalleTable.COMPROBANTE_DET_CANTIDAD, instance.getCantidad());
        detalleValues.put(ComprobanteDetalleTable.COMPROBANTE_DET_PRECIO, instance.getPrecio());
        detalleValues.put(ComprobanteDetalleTable.COMPROBANTE_DET_TOTAL, instance.getTotal());
        detalleValues.put(ComprobanteDetalleTable.COMPROBANTE_DET_SKU, instance.getSku());
        detalleValues.put(ComprobanteDetalleTable.COMPROBANTE_DET_NETO, instance.getNeto());
        detalleValues.put(ComprobanteDetalleTable.COMPROBANTE_DET_IVA, instance.getIva());

        return detalleValues;
    }

    @Override
    protected ComprobanteDetalle fromCursor(Cursor cursor) {
        ComprobanteDetalle detalle = new ComprobanteDetalle();

        long id = cursor.getLong(ComprobanteDetalleTable.COMPROBANTE_DET_IX_ID);
        Double total = cursor.getDouble(ComprobanteDetalleTable.COMPROBANTE_DET_IX_TOTAL);
        Double cantidad = cursor.getDouble(ComprobanteDetalleTable.COMPROBANTE_DET_IX_CANTIDAD);
        Double precio = cursor.getDouble(ComprobanteDetalleTable.COMPROBANTE_DET_IX_PRECIO);
        int COMPROBANTEId = cursor.getInt(ComprobanteDetalleTable.COMPROBANTE_DET_IX_COMPROBANTE_ID);
        Long productoId = cursor.getLong(ComprobanteDetalleTable.COMPROBANTE_DET_IX_PRODUCTO_ID);
        String sku = cursor.getString(ComprobanteDetalleTable.COMPROBANTE_DET_IX_SKU);
        Double neto = cursor.getDouble(ComprobanteDetalleTable.COMPROBANTE_DET_IX_NETO);
        Double iva = cursor.getDouble(ComprobanteDetalleTable.COMPROBANTE_DET_IX_IVA);

        detalle.setId(id);
        detalle.setCantidad(cantidad);
        detalle.setPrecio(precio);
        detalle.setTotal(total);
        detalle.setProducto(productoId != -1 ? productDao.find(productoId) : null);
        detalle.setComprobante(comprobanteDao.find(COMPROBANTEId));
        detalle.setSku(sku);
        detalle.setNeto(neto);
        detalle.setIva(iva);

        return detalle;
    }

    @Override
    protected String[] getProjectionAttributes() {
        return PROJECTION_ATTRIBUTES;
    }
}