package com.pds.common.dao;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.pds.common.Formatos;
import com.pds.common.db.ClienteTable;
import com.pds.common.model.Cliente;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class ClienteDao extends GenericDao<Cliente> {

    public static String[] PROJECTION_ATTRIBUTES = ClienteTable.COLUMNAS;

    @Override
    protected boolean logEvent() {
        return true;
    }

    public ClienteDao(ContentResolver contentResolver) {
        super(contentResolver);
    }

    @Override
    protected String getModelName() {
        return "Cliente";
    }

    @Override
    public String getTableName() {
        return ClienteTable.TABLE_NAME;
    }

    @Override
    protected ContentValues toContentValues(Cliente instance) {
        ContentValues clienteValues = new ContentValues();
        clienteValues.put(ClienteTable.COLUMN_NOMBRE, instance.getNombre());
        clienteValues.put(ClienteTable.COLUMN_APELLIDO, instance.getApellido());
        clienteValues.put(ClienteTable.COLUMN_TELEFONO, instance.getTelefono());
        clienteValues.put(ClienteTable.COLUMN_OBSERVACION, instance.getObservacion());
        clienteValues.put(ClienteTable.COLUMN_LIMITE_CREDITO, instance.getLimite());
        clienteValues.put(ClienteTable.COLUMN_BAJA, instance.getBaja());
        clienteValues.put(ClienteTable.COLUMN_FOTO_PATH, instance.getPathPhoto());
        clienteValues.put(ClienteTable.COLUMN_CLAVE_FISCAL, instance.getClaveFiscal());
        clienteValues.put(ClienteTable.COLUMN_SYNC_STATUS, instance.getSync());

        return clienteValues;
    }

    @Override
    protected Cliente fromCursor(Cursor cursor) {
        Cliente cliente = new Cliente();
        Long id = cursor.getLong(0);
        String nombre = cursor.getString(1);
        String apellido = cursor.getString(2);
        String telefono = cursor.getString(3);
        String observacion = cursor.getString(4);
        Double limite = cursor.getDouble(5);
        Integer baja = cursor.getInt(6);
        String path = cursor.getString(7);
        String claveFiscal = cursor.getString(8);
        String sync = cursor.getString(9);

        cliente.setId(id);
        cliente.setNombre(nombre);
        cliente.setApellido(apellido);
        cliente.setTelefono(telefono);
        cliente.setObservacion(observacion);
        cliente.setLimite(limite);
        cliente.setBaja(baja);
        cliente.setPathPhoto(path);
        cliente.setClaveFiscal(claveFiscal);
        cliente.setSync(sync);

        return cliente;
    }

    @Override
    protected String[] getProjectionAttributes() {
        return PROJECTION_ATTRIBUTES;
    }
}