package com.pds.common.dao;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.pds.common.Logger;
import com.pds.common.db.ProductTable;
import com.pds.common.model.Product;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

public class ProductDao extends GenericDao<Product> {

    public static String[] PROJECTION_ATTRIBUTES = ProductTable.COLUMNAS;

    private DepartmentDao departmentDao;
    private SubDepartmentDao subDepartmentDao;
    private ProviderDao providerDao;
    private TaxDao taxDao;
    private AlicuotasDao alicuotasDao;

    private boolean flagMasVendidos;

    private boolean change_sync_status;

    public ProductDao(ContentResolver contentResolver) {
        this(contentResolver, false, true);
    }

    public ProductDao(ContentResolver contentResolver, int change_sync_status) {
        this(contentResolver, false, change_sync_status > 0);
    }

    public ProductDao(ContentResolver contentResolver, boolean masVendidos, boolean change_sync_status) {
        super(contentResolver);
        //this.departmentDao = new DepartmentDao(contentResolver);
        //this.subDepartmentDao = new SubDepartmentDao(contentResolver);
        //this.providerDao = new ProviderDao(contentResolver);
        //this.taxDao = new TaxDao(contentResolver);
        //this.alicuotasDao = new AlicuotasDao(contentResolver);

        this.flagMasVendidos = masVendidos;
        this.change_sync_status = change_sync_status;
    }

    @Override
    protected boolean logEvent() {
        return false;//TODO: hay que implementar una regla acá
    }

    @Override
    protected String getModelName() {
        return "Producto";
    }

    @Override
    public String getTableName() {
        return flagMasVendidos ? ProductTable.VW_TOP_VENTAS_NAME : ProductTable.TABLE_NAME;
    }

    @Override
    protected ContentValues toContentValues(Product instance) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ProductTable.COLUMN_NAME, instance.getName());
        contentValues.put(ProductTable.COLUMN_CODE, instance.getCode());
        long departmentId = instance.getDepartment() != null ? instance.getDepartment().getId() : instance.getIdDepartment();
        long subDepartmentId = instance.getSubDepartment() != null ? instance.getSubDepartment().getId() : instance.getIdSubdepartment();
        long providerId = instance.getProvider() != null ? instance.getProvider().getId() : instance.getIdProvider();
        contentValues.put(ProductTable.COLUMN_DEPARTMENT_ID, departmentId);
        contentValues.put(ProductTable.COLUMN_SUBDEPARTMENT_ID, subDepartmentId);
        contentValues.put(ProductTable.COLUMN_PROVIDER_ID, providerId);
        contentValues.put(ProductTable.COLUMN_DESCRIPTION, instance.getDescription());
        contentValues.put(ProductTable.COLUMN_ALTA, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(instance.getAlta()));
        contentValues.put(ProductTable.COLUMN_STOCK, instance.getStock());
        contentValues.put(ProductTable.COLUMN_BRAND, instance.getBrand());
        contentValues.put(ProductTable.COLUMN_PRESENTATION, instance.getPresentation());
        contentValues.put(ProductTable.COLUMN_PURCHASE_PRICE, instance.getPurchasePrice());
        contentValues.put(ProductTable.COLUMN_SALE_PRICE, instance.getSalePrice());
        contentValues.put(ProductTable.COLUMN_IVA, instance.getIva());
        contentValues.put(ProductTable.COLUMN_IVA_ENABLED, instance.isIvaEnabled());
        contentValues.put(ProductTable.COLUMN_MIN_STOCK, instance.getMinStock());
        contentValues.put(ProductTable.COLUMN_QUICK_ACCESS, instance.isQuickAccess());
        contentValues.put(ProductTable.COLUMN_WEIGHABLE, instance.isWeighable());
        long taxId = instance.getTax() != null ? instance.getTax().getId() : instance.getIdTax();
        contentValues.put(ProductTable.COLUMN_TAX, taxId);
        contentValues.put(ProductTable.COLUMN_UNIT, instance.getUnit());
        contentValues.put(ProductTable.COLUMN_REMOVED, instance.isRemoved());

        if (change_sync_status) contentValues.put(ProductTable.COLUMN_SYNC_STATUS, "N");

        contentValues.put(ProductTable.COLUMN_CONTENIDO, instance.getContenido());
        contentValues.put(ProductTable.COLUMN_GENERIC, instance.isGeneric());
        contentValues.put(ProductTable.COLUMN_IMAGE_URI, instance.getImage_uri());
        contentValues.put(ProductTable.COLUMN_ISP_CODE, instance.getIspCode());
        contentValues.put(ProductTable.COLUMN_AUTO, instance.getAutomatic());
        contentValues.put(ProductTable.COLUMN_CODIGO_PADRE, instance.getCodigoPadre());
        contentValues.put(ProductTable.COLUMN_TYPE, instance.getType());

        return contentValues;
    }

    @Override
    protected Product fromCursor(Cursor cursor) {
        //long providerId;
        //long departmentId;
        //long subDepartmentId;
        Product product = new Product();
        int index = 0;
        product.setId(cursor.getLong(index++));
        product.setName(cursor.getString(index++));
        product.setCode(cursor.getString(index++));
        product.setIdDepartment(cursor.getLong(index++));
        product.setIdSubdepartment(cursor.getLong(index++));
        product.setIdProvider(cursor.getLong(index++));
        product.setDescription(cursor.getString(index++));
        try {
            String alta = cursor.getString(index++);
            product.setAlta(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(alta));
        } catch (ParseException e) {
            Log.e("cannot parse alta", e.getMessage(), e);
        }
        product.setStock(cursor.getDouble(index++));
        product.setBrand(cursor.getString(index++));
        product.setPresentation(cursor.getString(index++));
        product.setPurchasePrice(cursor.getDouble(index++));
        product.setSalePrice(cursor.getDouble(index++));
        product.setIva(cursor.getDouble(index++));
        product.setIvaEnabled(cursor.getInt(index++) > 0);
        product.setMinStock(cursor.getDouble(index++));
        product.setQuickAccess(cursor.getInt(index++) > 0);
        product.setWeighable(cursor.getInt(index++) > 0);
        //product.setTax(taxDao.find(cursor.getInt(index++)));
        product.setIdTax(cursor.getInt(index++));
        product.setUnit(cursor.getString(index++));
        //product.setDepartment(departmentDao.find(departmentId));
        //product.setSubDepartment(subDepartmentDao.find(subDepartmentId));
        //product.setProvider(providerDao.find(providerId));
        product.setContenido(cursor.getDouble(index++));
        product.setGeneric(cursor.getInt(index++) > 0);
        product.setImage_uri(cursor.getString(index++));
        product.setIspCode(cursor.getString(index++));
        product.setAutomatic(cursor.getInt(index++));
        product.setCodigoPadre(cursor.getString(index++));
        product.setType(cursor.getString(index++));

        return product;
    }

    @Override
    protected String[] getProjectionAttributes() {
        return PROJECTION_ATTRIBUTES;
    }

    public void delete(Product instance) {
        instance.setRemoved(true);
        long id = getId(instance);
        Uri uri = Uri.parse(BASE_URL + getTableName() + "/" + id);
        int modifiedRows = contentResolver.update(uri, this.toContentValues(instance), null, null);
        if (modifiedRows > 0) {
            String title = String.format("Baja %s", getModelName());
            String description = String.format("%s id : %d", getModelName(), id);
            Logger.RegistrarEvento(contentResolver, "i", title, description, "");
        }
        Log.d("ProductDao", String.format("Deleted %d for uri: %s", modifiedRows, uri.toString()));
    }

    public List<Product> getByCode(String code) {
        //USAMOS EL CODIGO PARA CHEQUEAR SI EXISTE
        return list(ProductTable.COLUMN_CODE + " = '" + code + "' and removed = 0", null, null);
    }

    public int importProduct(Product instance) {

        //USAMOS EL CODIGO PARA CHEQUEAR SI EXISTE
        List<Product> result = getByCode(instance.getCode());

        if (result.size() == 0) {
            //NO EXISTE => CREAR
            return save(instance) ? 1 : 0;
            //} else if (result.get(0).getAutomatic() == 0) {
        } else {
            //EXISTE: ORIGEN MANUAL => ACTUALIZAR
            Product p = result.get(0);

            p.setCodigoPadre(instance.getCodigoPadre());
            p.setAlta(instance.getAlta());
            p.setAutomatic(Product.TYPE_AUTOM.PROD_ALTA_AUTOM);
            p.setBrand(instance.getBrand());
            p.setCode(instance.getCode());
            p.setContenido(instance.getContenido());
            p.setDepartment(instance.getDepartment());
            p.setGeneric(instance.isGeneric());
            p.setIdDepartment(instance.getIdDepartment());
            p.setIdProvider(instance.getIdProvider());
            p.setIdSubdepartment(instance.getIdSubdepartment());
            p.setIdTax(instance.getIdTax());
            p.setImage_uri(instance.getImage_uri());
            p.setIva(instance.getIva());
            p.setIvaEnabled(instance.isIvaEnabled());
            p.setName(instance.getName());
            p.setPresentation(instance.getPresentation());
            //p.setProvider();
            p.setQuickAccess(instance.isQuickAccess());
            p.setSubDepartment(instance.getSubDepartment());
            p.setTax(instance.getTax());
            p.setUnit(instance.getUnit());
            p.setWeighable(instance.isWeighable());

            //nueva version, solo pisamos el nombre corto sino lo tiene
            if(TextUtils.isEmpty(p.getDescription()))
                p.setDescription(instance.getDescription());

            return saveOrUpdate(p) ? 2 : 0;

        } /*else {
            return 0;
        }*/
    }
}