package com.pds.common.dao;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;

import com.pds.common.db.TaxTable;
import com.pds.common.model.Tax;

public class TaxDao extends GenericDao<Tax>{

    public TaxDao(ContentResolver contentResolver) {
        super(contentResolver);
    }

    @Override
    protected boolean logEvent() {
        return true;
    }
    @Override
    protected String getModelName() {
        return "Impuestos";
    }

    @Override
    public String getTableName() {
        return TaxTable.TABLE_NAME;
    }

    @Override
    protected ContentValues toContentValues(Tax instance) {
        ContentValues taxValues = new ContentValues();
        taxValues.put(TaxTable.COLUMN_NAME, instance.getName());
        taxValues.put(TaxTable.COLUMN_AMOUNT, instance.getName());
        return taxValues;
    }

    @Override
    protected Tax fromCursor(Cursor cursor) {
        Tax tax = new Tax();
        int index = 0;
        tax.setId(cursor.getLong(index++));
        tax.setName(cursor.getString(index++));
        tax.setAmount(cursor.getInt(index));
        return tax;
    }

    @Override
    protected String[] getProjectionAttributes() {
        return new String[] {TaxTable.COLUMN_ID, TaxTable.COLUMN_NAME, TaxTable.COLUMN_AMOUNT};
    }
}