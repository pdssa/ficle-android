package com.pds.common.dao;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;

import com.pds.common.db.AlicuotasTable;
import com.pds.common.model.Alicuota;

public class AlicuotasDao extends GenericDao<Alicuota>{

    public AlicuotasDao(ContentResolver contentResolver) {
        super(contentResolver);
    }
    @Override
    protected boolean logEvent() {
        return true;
    }
    @Override
    protected String getModelName() {
        return "Alicuotas";
    }

    @Override
    public String getTableName() {
        return AlicuotasTable.TABLE_NAME;
    }

    @Override
    protected ContentValues toContentValues(Alicuota instance) {
        ContentValues taxValues = new ContentValues();
        taxValues.put(AlicuotasTable.COLUMN_NAME, instance.getName());
        taxValues.put(AlicuotasTable.COLUMN_AMOUNT, instance.getName());
        return taxValues;
    }

    @Override
    protected Alicuota fromCursor(Cursor cursor) {
        Alicuota alicuota = new Alicuota();
        int index = 0;
        alicuota.setId(cursor.getLong(index++));
        alicuota.setName(cursor.getString(index++));
        alicuota.setAmount(cursor.getDouble(index));
        return alicuota;
    }

    @Override
    protected String[] getProjectionAttributes() {
        return new String[] {AlicuotasTable.COLUMN_ID, AlicuotasTable.COLUMN_NAME, AlicuotasTable.COLUMN_AMOUNT};
    }
}