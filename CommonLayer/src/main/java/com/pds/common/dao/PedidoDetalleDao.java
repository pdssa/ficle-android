package com.pds.common.dao;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;

import com.pds.common.db.PedidoDetalleTable;
import com.pds.common.db.PedidoTable;
import com.pds.common.model.PedidoDetalle;

/**
 * Created by EMEKA on 13/01/2015.
 */
public class PedidoDetalleDao extends GenericDao<PedidoDetalle> {
    private static final String[] PROJECTION_ATTRIBUTES = PedidoDetalleTable.columnas;

    private ProductDao productDao;
    private PedidoDao pedidoDao;

    public PedidoDetalleDao(ContentResolver contentResolver) {
        super(contentResolver);

        this.productDao = new ProductDao(contentResolver);
        this.pedidoDao = new PedidoDao(contentResolver);
    }

    @Override
    protected boolean logEvent() {
        return true;
    }

    @Override
    protected String getModelName()  {
        return "PedidoDetalle";
    }

    @Override
    public String getTableName()  {
        return PedidoDetalleTable.TABLE_NAME;
    }

    @Override
    protected ContentValues toContentValues(PedidoDetalle instance) {
        ContentValues values = new ContentValues();

        long pedidoId = instance.getPedido() != null ? instance.getPedido().getId() : 0;
        //long productoId = instance.getProducto() != null ? instance.getProducto().getId() : -1;

        values.put(PedidoDetalleTable.PEDIDO_DET_PEDIDO_ID, pedidoId);
        values.put(PedidoDetalleTable.PEDIDO_DET_PRODUCTO_ID, instance.getIdProducto());
        values.put(PedidoDetalleTable.PEDIDO_DET_CANTIDAD, instance.getCantidad());
        values.put(PedidoDetalleTable.COLUMN_SYNC_STATUS, instance.getSync());

        return values;
    }

    @Override
    protected PedidoDetalle fromCursor(Cursor cursor) {
        PedidoDetalle instance = new PedidoDetalle();

        long id = cursor.getLong(cursor.getColumnIndex(PedidoDetalleTable.PEDIDO_DET_ID));
        long pedidoId = cursor.getInt(cursor.getColumnIndex(PedidoDetalleTable.PEDIDO_DET_PEDIDO_ID));
        long productoId = cursor.getLong(cursor.getColumnIndex(PedidoDetalleTable.PEDIDO_DET_PRODUCTO_ID));
        int cantidad = cursor.getInt(cursor.getColumnIndex(PedidoDetalleTable.PEDIDO_DET_CANTIDAD));
        String sync = cursor.getString(cursor.getColumnIndex(PedidoDetalleTable.COLUMN_SYNC_STATUS));

        instance.setId(id);
        instance.setId_pedido(pedidoId);
        instance.setIdProducto(productoId);
        instance.setCantidad(cantidad);
        instance.setProducto(productoId != -1 ? productDao.find(productoId) : null);
        instance.setPedido(pedidoDao.find(pedidoId));
        instance.setSync(sync);

        return instance;
    }

    @Override
    protected String[] getProjectionAttributes()  {
        return PROJECTION_ATTRIBUTES;
    }
}
