package com.pds.common.dao;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.pds.common.Formato;
import com.pds.common.Formatos;
import com.pds.common.db.PromocionTable;
import com.pds.common.model.Promocion;

/**
 * Created by Hernan on 06/06/2016.
 */
public class PromocionDao extends GenericDao<Promocion> {

    public static String[] PROJECTION_ATTRIBUTES = PromocionTable.COLUMNAS;

    public PromocionDao(ContentResolver contentResolver) {
        super(contentResolver);
    }

    @Override
    protected String getModelName() {
        return "Promocion";
    }

    @Override
    public String getTableName() {
        return PromocionTable.TABLE_NAME;
    }

    @Override
    protected ContentValues toContentValues(Promocion instance) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(PromocionTable.COLUMN_NOMBRE, instance.getNombrePromocion());
        contentValues.put(PromocionTable.COLUMN_CODIGO, instance.getCodPromocion());
        contentValues.put(PromocionTable.COLUMN_TEXTO, instance.getTextoPromocion());
        contentValues.put(PromocionTable.COLUMN_VIG_DESDE, Formato.DbDate(instance.getVigenciaDesde()));
        contentValues.put(PromocionTable.COLUMN_VIG_HASTA, Formato.DbDate(instance.getVigenciaHasta()));
        contentValues.put(PromocionTable.COLUMN_VALOR, instance.getValorAsociado());
        contentValues.put(PromocionTable.COLUMN_TIPO, instance.getTipoPromocion().ordinal());
        contentValues.put(PromocionTable.COLUMN_PROD_SKU, instance.getSkuProductoAsociado());
        contentValues.put(PromocionTable.COLUMN_PROD_IMG, instance.getUriImgProductoAsociado());
        contentValues.put(PromocionTable.COLUMN_BAJA, instance.isBaja() ? 1 : 0);
        contentValues.put(PromocionTable.COLUMN_SYNC_STATUS, instance.getSync());

        return contentValues;
    }

    @Override
    protected Promocion fromCursor(Cursor cursor) {
        Promocion instance = new Promocion();

        Long id = cursor.getLong(cursor.getColumnIndex(PromocionTable.COLUMN_ID));
        String nombre = cursor.getString(cursor.getColumnIndex(PromocionTable.COLUMN_NOMBRE));
        String codigo = cursor.getString(cursor.getColumnIndex(PromocionTable.COLUMN_CODIGO));
        String texto = cursor.getString(cursor.getColumnIndex(PromocionTable.COLUMN_TEXTO));
        String vigDesde = cursor.getString(cursor.getColumnIndex(PromocionTable.COLUMN_VIG_DESDE));
        String vigHasta = cursor.getString(cursor.getColumnIndex(PromocionTable.COLUMN_VIG_HASTA));
        Double valor = cursor.getDouble(cursor.getColumnIndex(PromocionTable.COLUMN_VALOR));
        Integer tipo = cursor.getInt(cursor.getColumnIndex(PromocionTable.COLUMN_TIPO));
        String prodSKU = cursor.getString(cursor.getColumnIndex(PromocionTable.COLUMN_PROD_SKU));
        String prodIMG = cursor.getString(cursor.getColumnIndex(PromocionTable.COLUMN_PROD_IMG));
        Integer baja = cursor.getInt(cursor.getColumnIndex(PromocionTable.COLUMN_BAJA));
        String sync = cursor.getString(cursor.getColumnIndex(PromocionTable.COLUMN_SYNC_STATUS));

        instance.setId(id);
        instance.setNombrePromocion(nombre);
        instance.setCodPromocion(codigo);
        instance.setTextoPromocion(texto);
        try {
            instance.setVigenciaDesde(Formato.DbDate(vigDesde));
        } catch (Exception e) {
            Log.e("PromocionDao", "cannot parse fecha desde", e);
        }
        try {
            instance.setVigenciaHasta(Formato.DbDate(vigHasta));
        } catch (Exception e) {
            Log.e("PromocionDao", "cannot parse fecha hasta", e);
        }

        instance.setValorAsociado(valor);
        instance.setTipoPromocion(Promocion.TIPO_PROMOCION.fromValue(tipo));
        instance.setSkuProductoAsociado(prodSKU);
        instance.setImgUriProductoAsociado(prodIMG);
        instance.setBaja(baja == 1);
        instance.setSync(sync);

        return instance;
    }

    @Override
    protected String[] getProjectionAttributes() {
        return PROJECTION_ATTRIBUTES;
    }

    @Override
    protected boolean logEvent() {
        return false;
    }
}
