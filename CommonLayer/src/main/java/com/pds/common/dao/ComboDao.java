/*
package com.pds.common.dao;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.pds.common.Formatos;
import com.pds.common.db.ComboTable;
import com.pds.common.model.Combo;
import com.pds.common.model.Product;

*/
/**
 * Created by Hernan on 25/02/2015.
 *//*

public class ComboDao extends GenericDao<Combo> {
    private static final String[] PROJECTION_ATTRIBUTES = ComboTable.COLUMNAS;

    @Override
    protected boolean logEvent() {
        return true;
    }

    public ComboDao(ContentResolver contentResolver) {
        super(contentResolver);
    }

    @Override
    protected String getModelName() {
        return "Combo";
    }

    @Override
    public String getTableName() {
        return ComboTable.TABLE_NAME;
    }

    @Override
    protected ContentValues toContentValues(Combo instance) {
        ContentValues values = new ContentValues();
        
        values.put(ComboTable.COLUMN_CANTIDAD, instance.getCantidad());
        values.put(ComboTable.COLUMN_CODIGO, instance.getCodigo());
        values.put(ComboTable.COLUMN_DESCRIPCION, instance.getDescripcion());
        values.put(ComboTable.COLUMN_NOMBRE, instance.getNombre());
        values.put(ComboTable.COLUMN_PRECIO, instance.getPrecio());
        values.put(ComboTable.COLUMN_PRODUCTO_PPAL_CODE, instance.getProducto_ppal_code());
        values.put(ComboTable.COLUMN_PRODUCTO_PPAL_ID, instance.getProducto_ppal_id());
        values.put(ComboTable.COLUMN_FECHA_ALTA, Formatos.DbDate(instance.getFecha_alta()));
        values.put(ComboTable.COLUMN_BAJA, instance.isBaja() ? 1 : 0);
        values.put(ComboTable.COLUMN_SYNC_STATUS, instance.getSync());
        values.put(ComboTable.COLUMN_PROMOCION_ID, instance.getPromocion_id());

        return values;
    }

    @Override
    protected Combo fromCursor(Cursor cursor) {
        Combo instance = new Combo();

        long id = cursor.getLong(cursor.getColumnIndex(ComboTable.COLUMN_ID));
        long producto_ppal_id = cursor.getLong(cursor.getColumnIndex(ComboTable.COLUMN_PRODUCTO_PPAL_ID));
        String producto_ppal_code = cursor.getString(cursor.getColumnIndex(ComboTable.COLUMN_PRODUCTO_PPAL_CODE));
        String nombre = cursor.getString(cursor.getColumnIndex(ComboTable.COLUMN_NOMBRE));
        String descripcion = cursor.getString(cursor.getColumnIndex(ComboTable.COLUMN_DESCRIPCION));
        Double cantidad = cursor.getDouble(cursor.getColumnIndex(ComboTable.COLUMN_CANTIDAD));
        Double precio = cursor.getDouble(cursor.getColumnIndex(ComboTable.COLUMN_PRECIO));
        String codigo = cursor.getString(cursor.getColumnIndex(ComboTable.COLUMN_CODIGO));
        String fecha = cursor.getString(cursor.getColumnIndex(ComboTable.COLUMN_FECHA_ALTA));
        int baja = cursor.getInt(cursor.getColumnIndex(ComboTable.COLUMN_BAJA));
        String sync = cursor.getString(cursor.getColumnIndex(ComboTable.COLUMN_SYNC_STATUS));
        long promocion_id = cursor.getLong(cursor.getColumnIndex(ComboTable.COLUMN_PROMOCION_ID));

        instance.setId(id);
        instance.setProducto_ppal_id(producto_ppal_id);
        instance.setProducto_ppal_code(producto_ppal_code);
        instance.setNombre(nombre);
        instance.setDescripcion(descripcion);
        instance.setCantidad(cantidad);
        instance.setPrecio(precio);
        instance.setCodigo(codigo);
        try {
            instance.setFecha_alta(Formatos.DbDate(fecha));
        } catch (Exception e) {
            Log.e("ComboDao", "cannot parse fecha", e);
        }
        instance.setCodigo(codigo);
        instance.setBaja(baja == 1);
        instance.setSync(sync);
        instance.setPromocion_id(promocion_id);

        return instance;
    }

    @Override
    protected String[] getProjectionAttributes() {
        return PROJECTION_ATTRIBUTES;
    }

    @Override
    public boolean saveOrUpdate(Combo instance) {
        //primero actualizamos el combo
        boolean result = super.saveOrUpdate(instance);

        if(result) {
            //luego actualizamos el PRODUCTO-COMBO
            Product p_combo = instance.getProductoCombo(super.getContentResolver());

            if(p_combo == null) {
                p_combo = new Product();
            }

            p_combo.ProductoCombo(instance, super.getContentResolver());//cargamos los valores del combo en el producto

            result = new ProductDao(super.getContentResolver()).saveOrUpdate(p_combo);

        }

        return result;
    }
}*/
