package com.pds.common.dao;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;

import com.pds.common.db.VentaAnuladaTable;
import com.pds.common.model.VentaAnulada;

/**
 * Created by Hernan on 24/02/2015.
 */
public class VentaAnuladaDao extends GenericDao<VentaAnulada> {
    private static final String[] PROJECTION_ATTRIBUTES = VentaAnuladaTable.columnas;

    public VentaAnuladaDao(ContentResolver contentResolver) {
        super(contentResolver);
    }
    @Override
    protected boolean logEvent() {
        return false;
    }
    @Override
    protected String getModelName() {
        return "VentaAnulada";
    }

    @Override
    public String getTableName() {
        return VentaAnuladaTable.TABLE_NAME;
    }

    @Override
    protected ContentValues toContentValues(VentaAnulada instance) {
        ContentValues ventaValues = new ContentValues();

        ventaValues.put(VentaAnuladaTable.COLUMN_ID_ORIGINAL, instance.getId_original());
        ventaValues.put(VentaAnuladaTable.COLUMN_TYPE_ORIGINAL, instance.getType_original());
        ventaValues.put(VentaAnuladaTable.COLUMN_TOTAL, instance.getTotal());
        ventaValues.put(VentaAnuladaTable.COLUMN_FECHA, instance.getFecha());
        ventaValues.put(VentaAnuladaTable.COLUMN_HORA, instance.getHora());
        ventaValues.put(VentaAnuladaTable.COLUMN_MEDIO_PAGO, instance.getMedio_pago());
        ventaValues.put(VentaAnuladaTable.COLUMN_CODIGO, instance.getCodigo());
        ventaValues.put(VentaAnuladaTable.COLUMN_CANTIDAD, instance.getCantidad());
        ventaValues.put(VentaAnuladaTable.COLUMN_USERID, instance.getUserid());
        ventaValues.put(VentaAnuladaTable.COLUMN_FC_TYPE, instance.getFc_type());
        ventaValues.put(VentaAnuladaTable.COLUMN_FC_ID, instance.getFc_id());
        ventaValues.put(VentaAnuladaTable.COLUMN_FC_FOLIO, instance.getFc_folio());
        ventaValues.put(VentaAnuladaTable.COLUMN_FC_VALIDATION, instance.getFc_validation());
        ventaValues.put(VentaAnuladaTable.COLUMN_NETO, instance.getNeto());
        ventaValues.put(VentaAnuladaTable.COLUMN_IVA, instance.getIva());
        //ventaValues.put(VentaAnuladaTable.COLUMN_SYNC_STATUS, instance.getSync());
        ventaValues.put(VentaAnuladaTable.COLUMN_TIMBRE, instance.getFc_Timbre());

        return ventaValues;
    }

    @Override
    protected VentaAnulada fromCursor(Cursor cursor) {
        VentaAnulada instance = new VentaAnulada();

        long id = cursor.getLong(cursor.getColumnIndex(VentaAnuladaTable.COLUMN_ID));
        long id_original = cursor.getLong(cursor.getColumnIndex(VentaAnuladaTable.COLUMN_ID_ORIGINAL));
        String type_original = cursor.getString(cursor.getColumnIndex(VentaAnuladaTable.COLUMN_TYPE_ORIGINAL));
        Double total = cursor.getDouble(cursor.getColumnIndex(VentaAnuladaTable.COLUMN_TOTAL));
        int cantidad = cursor.getInt(cursor.getColumnIndex(VentaAnuladaTable.COLUMN_CANTIDAD));
        String fecha = cursor.getString(cursor.getColumnIndex(VentaAnuladaTable.COLUMN_FECHA));
        String hora = cursor.getString(cursor.getColumnIndex(VentaAnuladaTable.COLUMN_HORA));
        String codigo = cursor.getString(cursor.getColumnIndex(VentaAnuladaTable.COLUMN_CODIGO));
        int userId = cursor.getInt(cursor.getColumnIndex(VentaAnuladaTable.COLUMN_USERID));
        String medioPago = cursor.getString(cursor.getColumnIndex(VentaAnuladaTable.COLUMN_MEDIO_PAGO));
        String fc_type = cursor.getString(cursor.getColumnIndex(VentaAnuladaTable.COLUMN_FC_TYPE));
        long fc_id = cursor.getLong(cursor.getColumnIndex(VentaAnuladaTable.COLUMN_FC_ID));
        String fc_folio = cursor.getString(cursor.getColumnIndex(VentaAnuladaTable.COLUMN_FC_FOLIO));
        String fc_validat = cursor.getString(cursor.getColumnIndex(VentaAnuladaTable.COLUMN_FC_VALIDATION));
        Double neto = cursor.getDouble(cursor.getColumnIndex(VentaAnuladaTable.COLUMN_NETO));
        Double iva = cursor.getDouble(cursor.getColumnIndex(VentaAnuladaTable.COLUMN_IVA));
        String sync = cursor.getString(cursor.getColumnIndex(VentaAnuladaTable.COLUMN_SYNC_STATUS));
        String stamp = cursor.getString(cursor.getColumnIndex(VentaAnuladaTable.COLUMN_TIMBRE));

        instance.setId(id);
        instance.setId_original(id_original);
        instance.setType_original(type_original);
        instance.setTotal(total);
        instance.setCantidad(cantidad);
        instance.setFecha(fecha);
        instance.setHora(hora);
        instance.setCodigo(codigo);
        instance.setUserid(userId);
        instance.setMedio_pago(medioPago);
        instance.setFc_type(fc_type);
        instance.setFc_id(fc_id);
        instance.setFc_folio(fc_folio);
        instance.setFc_validation(fc_validat);
        instance.setNeto(neto);
        instance.setIva(iva);
        instance.setSync(sync);
        instance.setFc_Timbre(stamp);

        return instance;
    }

    @Override
    protected String[] getProjectionAttributes() {
        return PROJECTION_ATTRIBUTES;
    }
}
