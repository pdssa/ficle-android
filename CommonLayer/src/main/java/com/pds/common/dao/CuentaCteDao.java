package com.pds.common.dao;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.pds.common.Formatos;
import com.pds.common.db.ClienteTable;
import com.pds.common.db.CuentaTable;
import com.pds.common.model.Cliente;
import com.pds.common.model.CuentaCte;
import com.pds.common.model.Venta;

import java.text.ParseException;
import java.util.Date;

public class CuentaCteDao extends GenericDao<CuentaCte> {

    private static String[] PROJECTION_ATTRIBUTES = CuentaTable.COLUMNAS;

    private final ClienteDao clienteDao;
    private final VentaDao ventaDao;

    public CuentaCteDao(ContentResolver contentResolver) {
        super(contentResolver);
        this.clienteDao = new ClienteDao(contentResolver);
        this.ventaDao = new VentaDao(contentResolver);
    }

    @Override
    protected boolean logEvent() {
        return true;
    }

    @Override
    protected String getModelName() {
        return "CuentaCte";
    }

    @Override
    public String getTableName() {
        return CuentaTable.TABLE_NAME;
    }

    @Override
    protected ContentValues toContentValues(CuentaCte instance) {
        ContentValues ctacteValues = new ContentValues();
        ctacteValues.put(CuentaTable.COLUMN_TIPO_MOV, instance.getTipoMov());
        ctacteValues.put(CuentaTable.COLUMN_FECHA_HORA, Formatos.DbDateTimeFormat.format(instance.getFechaHora()));
        ctacteValues.put(CuentaTable.COLUMN_MONTO, instance.getMonto());
        //long ventaId = instance.getVenta() != null ? instance.getVenta().getId() : 0;
        ctacteValues.put(CuentaTable.COLUMN_VENTA_ID, instance.getIdVenta());
        long clienteId = instance.getCliente() != null ? instance.getCliente().getId() : 0;
        ctacteValues.put(CuentaTable.COLUMN_CLIENTE_ID, clienteId);
        ctacteValues.put(CuentaTable.COLUMN_OBSERVACION, instance.getObservacion());
        ctacteValues.put(CuentaTable.COLUMN_TIPO_VTA, instance.getTipoVenta());
        ctacteValues.put(CuentaTable.COLUMN_SYNC_STATUS,  "N");

        return ctacteValues;
    }

    @Override
    protected CuentaCte fromCursor(Cursor cursor) {
        CuentaCte ctacte = new CuentaCte();

        Long id = cursor.getLong(0);
        int tipoMov = cursor.getInt(1);
        String fechaHora = cursor.getString(2);

        Double monto = cursor.getDouble(3);
        Long idVenta = cursor.getLong(4);
        Venta venta = null;
        if(idVenta != CuentaCte.ID_PRESTAMO)
            venta = ventaDao.find(idVenta);
        Cliente cliente = clienteDao.find(cursor.getLong(5));
        String observacion = cursor.getString(6);
        String tipoVta = cursor.getString(7);

        ctacte.setId(id);
        ctacte.setTipoMov(tipoMov);
        try {
            ctacte.setFechaHora(Formatos.DbDateTimeFormat.parse(fechaHora));
        } catch (ParseException e) {
            Log.e("cannot parse fechaHora", e.getMessage(), e);
        }
        ctacte.setMonto(monto);

        if(venta != null)
            ctacte.setVenta(venta);
        else
            ctacte.setIdVenta(idVenta);

        ctacte.setCliente(cliente);
        ctacte.setObservacion(observacion);
        ctacte.setTipoVenta(tipoVta);
        return ctacte;
    }

    @Override
    protected String[] getProjectionAttributes() {
        return PROJECTION_ATTRIBUTES;
    }
}