package com.pds.common.dao;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;

import com.pds.common.Formatos;
import com.pds.common.db.RangosFoliosTable;
import com.pds.common.model.RangoFolio;

/**
 * Created by Hernan on 17/12/2014.
 */
public class RangoFolioDao extends GenericDao<RangoFolio> {

    public RangoFolioDao(ContentResolver contentResolver) {
        super(contentResolver);
    }
    @Override
    protected boolean logEvent() {
        return true;
    }
    @Override
    protected String getModelName() {
        return "RangoFolio";
    }

    @Override
    public String getTableName() {
        return RangosFoliosTable.TABLE_NAME;
    }

    @Override
    protected ContentValues toContentValues(RangoFolio instance) {
        ContentValues values = new ContentValues();
        values.put(RangosFoliosTable.COLUMN_TIPO_DOC, instance.getTipo_doc());
        values.put(RangosFoliosTable.COLUMN_DESDE, instance.getDesde());
        values.put(RangosFoliosTable.COLUMN_HASTA, instance.getHasta());
        values.put(RangosFoliosTable.COLUMN_ULTIMO, instance.getUltimo());
        values.put(RangosFoliosTable.COLUMN_CAF, instance.getCaf());
        values.put(RangosFoliosTable.COLUMN_FECHA_REFERENCIA, Formatos.DbDate(instance.getFec_ref()));

        return values;
    }

    @Override
    protected RangoFolio fromCursor(Cursor cursor) {
        RangoFolio instance = new RangoFolio();
        int i = 0;

        Long id = cursor.getLong(i++);
        String tipoDoc = cursor.getString(i++);
        long desde = cursor.getLong(i++);
        long hasta= cursor.getLong(i++);
        long ultimo= cursor.getLong(i++);
        String caf = cursor.getString(i++);
        String fecha = cursor.getString(i++);

        instance.setId(id);
        instance.setTipo_doc(tipoDoc);
        instance.setDesde(desde);
        instance.setHasta(hasta);
        instance.setUltimo(ultimo);
        instance.setCaf(caf);
        try {
            instance.setFec_ref(Formatos.DbDate(fecha));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return instance;
    }

    @Override
    protected String[] getProjectionAttributes() {
        return RangosFoliosTable.COLUMNAS;
    }

}
