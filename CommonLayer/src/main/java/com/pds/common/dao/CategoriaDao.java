package com.pds.common.dao;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.pds.common.Formatos;
import com.pds.common.db.CategoriaTable;
import com.pds.common.db.CierreTable;
import com.pds.common.db.ClienteTable;
import com.pds.common.model.Categoria;
import com.pds.common.model.Cierre;

import java.util.Date;
import java.util.List;

/**
 * Created by EMEKA on 22/01/2015.
 */
public class CategoriaDao extends GenericDao<Categoria> {

    public CategoriaDao(ContentResolver contentResolver) {
        super(contentResolver);
    }
    @Override
    protected boolean logEvent() {
        return false;
    }
    @Override
    protected String getModelName() {
        return "Categoria";
    }

    @Override
    public String getTableName() {
        return CategoriaTable.TABLE_NAME;
    }

    @Override
    protected ContentValues toContentValues(Categoria instance) {
        ContentValues values = new ContentValues();

        values.put(CategoriaTable.COLUMN_NOMBRE, instance.getNombre());
        values.put(CategoriaTable.COLUMN_DESCRIPCION, instance.getDescripcion());
        values.put(CategoriaTable.COLUMN_CODIGO, instance.getCodigo());
        values.put(CategoriaTable.COLUMN_CODIGO_PADRE, instance.getCodigo_padre());
        values.put(CategoriaTable.COLUMN_ULT_NIVEL, instance.isUltimo_nivel() ? 1 : 0);
        values.put(CategoriaTable.COLUMN_NUM_NIVEL, instance.getNumero_nivel());

        return values;
    }

    @Override
    protected Categoria fromCursor(Cursor cursor) {
        Categoria instance = new Categoria();
        int i = 0;

        long id = cursor.getLong(i++);
        String nombre = cursor.getString(i++);
        String descripcion = cursor.getString(i++);
        String codigo = cursor.getString(i++);
        String codigo_padre = cursor.getString(i++);
        int ultimo = cursor.getInt(i++);
        int nivel = cursor.getInt(i++);

        instance.setId(id);
        instance.setNombre(nombre);
        instance.setDescripcion(descripcion);
        instance.setCodigo(codigo);
        instance.setCodigo_padre(codigo_padre);
        instance.setUltimo_nivel(ultimo > 0);
        instance.setNumero_nivel(nivel);

        return instance;
    }

    @Override
    protected String[] getProjectionAttributes() {
        return CategoriaTable.COLUMNAS;
    }

    public Categoria findPadreByLevel(int level, String codigo_padre) {
        //obtengo el nivel padre
        List<Categoria> result = list(CategoriaTable.COLUMN_CODIGO +"= '" + codigo_padre+"'", null, null);

        if (result.size() > 0) {
            Categoria padre = result.get(0);

            if (padre.getNumero_nivel() == level)
                return padre; //encontramos el nivel buscado
            else if(padre.getNumero_nivel() != 1){
                //buscamos en el padre del nivel actual a ver si es el nivel buscado
                return findPadreByLevel(level, padre.getCodigo_padre());
            }
            else{
                return null;//no hay mas niveles donde buscar
            }

        } else
            return null;
    }
}
