package com.pds.common.dao;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;

import com.pds.common.Formatos;
import com.pds.common.db.CompraTable;
import com.pds.common.model.Compra;
import com.pds.common.model.Comprobante;

import java.text.ParseException;
import java.util.Date;


public class CompraDao extends GenericDao<Compra> {
    private static final String[] PROJECTION_ATTRIBUTES = CompraTable.columnas;

    private ProviderDao providerDao;

    public CompraDao(ContentResolver contentResolver) {
        super(contentResolver);

        providerDao = new ProviderDao(contentResolver);
    }

    @Override
    protected boolean logEvent() {
        return true;
    }

    @Override
    protected String getModelName() {
        return "Compra";
    }

    @Override
    public String getTableName() {
        return CompraTable.TABLE_NAME;
    }

    @Override
    protected ContentValues toContentValues(Compra instance) {
        ContentValues values = new ContentValues();

        values.put(CompraTable.COMPRA_PROVEEDOR_ID, instance.getProviderid());
        values.put(CompraTable.COMPRA_FECHA, Formatos.DbDate(instance.getFecha()));
        values.put(CompraTable.COMPRA_NUMERO_FC, instance.getNumero());
        values.put(CompraTable.COMPRA_SUBTOTAL_X, instance.getSubtotal_exento());
        values.put(CompraTable.COMPRA_SUBTOTAL_NG, instance.getSubtotal_neto_grav());
        values.put(CompraTable.COMPRA_SUBTOTAL_I, instance.getSubtotal_impuesto());
        values.put(CompraTable.COMPRA_TOTAL, instance.getTotal());
        values.put(CompraTable.COMPRA_MEDIO_PAGO, instance.getMedio_pago());
        values.put(CompraTable.COMPRA_CODIGO, instance.getCodigo());
        values.put(CompraTable.COMPRA_USER_ID, instance.getUserid());

        return values;
    }

    @Override
    protected Compra fromCursor(Cursor cursor) {
        Compra instance = new Compra();

        long id = cursor.getLong(cursor.getColumnIndex(CompraTable.COMPRA_ID));
        long providerId = cursor.getLong(cursor.getColumnIndex(CompraTable.COMPRA_PROVEEDOR_ID));
        Double subt_exento = cursor.getDouble(cursor.getColumnIndex(CompraTable.COMPRA_SUBTOTAL_X));
        Double subt_neto_g = cursor.getDouble(cursor.getColumnIndex(CompraTable.COMPRA_SUBTOTAL_NG));
        Double subt_imp = cursor.getDouble(cursor.getColumnIndex(CompraTable.COMPRA_SUBTOTAL_I));
        Double total = cursor.getDouble(cursor.getColumnIndex(CompraTable.COMPRA_TOTAL));
        String numero = cursor.getString(cursor.getColumnIndex(CompraTable.COMPRA_NUMERO_FC));
        String fecha = cursor.getString(cursor.getColumnIndex(CompraTable.COMPRA_FECHA));
        String codigo = cursor.getString(cursor.getColumnIndex(CompraTable.COMPRA_CODIGO));
        int userId = cursor.getInt(cursor.getColumnIndex(CompraTable.COMPRA_USER_ID));
        String medioPago = cursor.getString(cursor.getColumnIndex(CompraTable.COMPRA_MEDIO_PAGO));

        instance.setId(id);
        instance.setProviderid(providerId);
        instance.setProveedor(providerDao.find(providerId));
        instance.setNumero(numero);
        instance.setSubtotal_exento(subt_exento);
        instance.setSubtotal_neto_grav(subt_neto_g);
        instance.setSubtotal_impuesto(subt_imp);
        instance.setTotal(total);
        try {
            instance.setFecha(Formatos.DbDate(fecha));
        } catch (Exception e) {
            e.printStackTrace();
        }
        instance.setCodigo(codigo);
        instance.setUserid(userId);
        instance.setMedio_pago(medioPago);

        return instance;
    }

    @Override
    protected String[] getProjectionAttributes() {
        return PROJECTION_ATTRIBUTES;
    }
}