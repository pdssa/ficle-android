package com.pds.common.dao;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;

import com.pds.common.db.ProductProvidersTable;
import com.pds.common.model.ProviderProduct;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hernan on 05/11/2014.
 */
public class ProviderProductDao extends GenericDao<ProviderProduct> {
    public static final String[] PROJECTION_ATTRIBUTES = {
            ProductProvidersTable.COLUMN_ID,
            ProductProvidersTable.COLUMN_PRODUCT_ID,
            ProductProvidersTable.COLUMN_PROVIDER_ID
    };

    public ProviderProductDao(ContentResolver contentResolver) {
        super(contentResolver);
    }

    @Override
    protected boolean logEvent() {
        return false;
    }
    @Override
    protected String getModelName() {
        return "ProviderProduct";
    }

    @Override
    public String getTableName() {
        return ProductProvidersTable.TABLE_NAME;
    }

    @Override
    protected ContentValues toContentValues(ProviderProduct instance) {
        ContentValues values = new ContentValues();
        values.put(ProductProvidersTable.COLUMN_PRODUCT_ID, instance.getIdProduct());
        values.put(ProductProvidersTable.COLUMN_PROVIDER_ID, instance.getIdProvider());

        return values;
    }

    @Override
    protected ProviderProduct fromCursor(Cursor cursor) {
        ProviderProduct providerProduct = new ProviderProduct();
        providerProduct.setId(cursor.getLong(cursor.getColumnIndex(ProductProvidersTable.COLUMN_ID)));
        providerProduct.setIdProduct(cursor.getLong(cursor.getColumnIndex(ProductProvidersTable.COLUMN_PRODUCT_ID)));
        providerProduct.setIdProvider(cursor.getLong(cursor.getColumnIndex(ProductProvidersTable.COLUMN_PROVIDER_ID)));

        return providerProduct;
    }

    @Override
    protected String[] getProjectionAttributes() {
        return PROJECTION_ATTRIBUTES;
    }

    public boolean existeRelacion(long id_product, long id_provider) {
        return list(String.format("product_id = %d and provider_id = %d", id_product, id_provider), null, null).size() > 0;
    }

    public List<Long> getProductsList(long id_provider){
        List<ProviderProduct> lst = list(String.format("provider_id = %d", id_provider), null, null);

        List<Long> productsId = new ArrayList<Long>();

        if(lst != null) {
            for (ProviderProduct prov : lst) {
                productsId.add(prov.getIdProduct());
            }
        }

        return productsId;
    }
}
