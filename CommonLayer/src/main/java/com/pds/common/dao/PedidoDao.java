package com.pds.common.dao;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;

import com.pds.common.Formatos;
import com.pds.common.db.PedidoTable;
import com.pds.common.model.Pedido;

/**
 * Created by EMEKA on 13/01/2015.
 */

public class PedidoDao extends GenericDao<Pedido> {
    private static final String[] PROJECTION_ATTRIBUTES = PedidoTable.columnas;

    private ProviderDao providerDao;

    public PedidoDao(ContentResolver contentResolver) {
        super(contentResolver);

        providerDao = new ProviderDao(contentResolver);
    }

    @Override
    protected boolean logEvent() {
        return true;
    }

    @Override
    protected String getModelName() {
        return "Pedido";
    }

    @Override
    public String getTableName() {
        return PedidoTable.TABLE_NAME;
    }

    @Override
    protected ContentValues toContentValues(Pedido instance) {
        ContentValues values = new ContentValues();

        values.put(PedidoTable.PEDIDO_PROVEEDOR_ID, instance.getProviderid());
        values.put(PedidoTable.PEDIDO_ESTADO, instance.getEstado());
        values.put(PedidoTable.PEDIDO_FECHA, Formatos.DbDate(instance.getFecha()));
        values.put(PedidoTable.PEDIDO_USER_ID, instance.getUserid());
        values.put(PedidoTable.PEDIDO_SYNC_STATUS, instance.getSync());

        return values;
    }

    @Override
    protected Pedido fromCursor(Cursor cursor) {
        Pedido instance = new Pedido();

        long id = cursor.getLong(cursor.getColumnIndex(PedidoTable.PEDIDO_ID));
        long providerId = cursor.getLong(cursor.getColumnIndex(PedidoTable.PEDIDO_PROVEEDOR_ID));
        String estado = cursor.getString(cursor.getColumnIndex(PedidoTable.PEDIDO_ESTADO));
        String fecha = cursor.getString(cursor.getColumnIndex(PedidoTable.PEDIDO_FECHA));
        int userId = cursor.getInt(cursor.getColumnIndex(PedidoTable.PEDIDO_USER_ID));
        String sync = cursor.getString(cursor.getColumnIndex(PedidoTable.PEDIDO_SYNC_STATUS));

        instance.setId(id);
        instance.setProviderid(providerId);
        instance.setProveedor(providerDao.find(providerId));
        instance.setEstado(estado);

        try {
            instance.setFecha(Formatos.DbDate(fecha));
        } catch (Exception e) {
            e.printStackTrace();
        }
        instance.setUserid(userId);
        instance.setSync(sync);

        return instance;
    }

    @Override
    protected String[] getProjectionAttributes()  {
        return PROJECTION_ATTRIBUTES;
    }
}
