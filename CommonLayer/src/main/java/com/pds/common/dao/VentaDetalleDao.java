package com.pds.common.dao;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.pds.common.VentaDetalleHelper;
import com.pds.common.db.DepartmentTable;
import com.pds.common.model.Department;
import com.pds.common.model.VentaDetalle;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class VentaDetalleDao extends GenericDao<VentaDetalle> {

    private static final String[] PROJECTION_ATTRIBUTES = VentaDetalleHelper.columnas;

    private ProductDao productDao;
    private VentaDao ventaDao;

    public VentaDetalleDao(ContentResolver contentResolver) {
        super(contentResolver);
        this.productDao = new ProductDao(contentResolver);
        this.ventaDao = new VentaDao(contentResolver);
    }
    @Override
    protected boolean logEvent() {
        return false;
    }
    @Override
    protected String getModelName() {
        return "VentaDetalle";
    }

    @Override
    public String getTableName() {
        return "venta_detalles";//return VentaDetalleHelper.TABLE_NAME;
    }

    @Override
    protected ContentValues toContentValues(VentaDetalle instance) {
        ContentValues detalleValues = new ContentValues();

        long ventaId = instance.getVenta() != null ? instance.getVenta().getId() : 0;
        long productoId = instance.getProducto() != null ? instance.getProducto().getId() : -1;

        detalleValues.put(VentaDetalleHelper.VENTA_DET_VENTA_ID, ventaId);
        detalleValues.put(VentaDetalleHelper.VENTA_DET_PRODUCTO_ID, productoId);
        detalleValues.put(VentaDetalleHelper.VENTA_DET_CANTIDAD, instance.getCantidad());
        detalleValues.put(VentaDetalleHelper.VENTA_DET_PRECIO, instance.getPrecio());
        detalleValues.put(VentaDetalleHelper.VENTA_DET_TOTAL, instance.getTotal());
        detalleValues.put(VentaDetalleHelper.VENTA_DET_SKU, instance.getSku());
        detalleValues.put(VentaDetalleHelper.VENTA_DET_NETO, instance.getNeto());
        detalleValues.put(VentaDetalleHelper.VENTA_DET_IVA, instance.getIva());

        return detalleValues;
    }

    @Override
    protected VentaDetalle fromCursor(Cursor cursor) {
        VentaDetalle detalle = new VentaDetalle();

        long id = cursor.getLong(VentaDetalleHelper.VENTA_DET_IX_ID);
        Double total = cursor.getDouble(VentaDetalleHelper.VENTA_DET_IX_TOTAL);
        Double cantidad = cursor.getDouble(VentaDetalleHelper.VENTA_DET_IX_CANTIDAD);
        Double precio = cursor.getDouble(VentaDetalleHelper.VENTA_DET_IX_PRECIO);
        int ventaId = cursor.getInt(VentaDetalleHelper.VENTA_DET_IX_VENTA_ID);
        long productoId = cursor.getLong(VentaDetalleHelper.VENTA_DET_IX_PRODUCTO_ID);
        String sku = cursor.getString(VentaDetalleHelper.VENTA_DET_IX_SKU);
        Double neto = cursor.getDouble(VentaDetalleHelper.VENTA_DET_IX_NETO);
        Double iva = cursor.getDouble(VentaDetalleHelper.VENTA_DET_IX_IVA);

        detalle.setId(id);
        detalle.setCantidad(cantidad);
        detalle.setPrecio(precio);
        detalle.setTotal(total);
        detalle.setProducto(productoId != -1 ? productDao.find(productoId) : null);
        detalle.setVenta(ventaDao.find(ventaId));
        detalle.setSku(sku);
        detalle.setNeto(neto);
        detalle.setIva(iva);

        return detalle;
    }

    @Override
    protected String[] getProjectionAttributes() {
        return PROJECTION_ATTRIBUTES;
    }
}