package com.pds.common.dao;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;
import android.util.Log;

import com.pds.common.Formatos;
import com.pds.common.db.CuentaTable;
import com.pds.common.model.Cliente;
import com.pds.common.model.CuentaCte;
import com.pds.common.model.CuentaCteTotal;
import com.pds.common.model.Venta;

import java.text.ParseException;

public class CuentaCteTotalesDao extends GenericDao<CuentaCteTotal> {

    private static String[] PROJECTION_ATTRIBUTES = {
            CuentaTable.COLUMN_VW_CLIENTE_ID,
            CuentaTable.COLUMN_VW_SALDO,
            CuentaTable.COLUMN_VW_FECHA_ULT_PAGO,
            CuentaTable.COLUMN_VW_MONTO_ULT_PAGO
    };

    private final ClienteDao clienteDao;

    public CuentaCteTotalesDao(ContentResolver contentResolver) {
        super(contentResolver);
        this.clienteDao = new ClienteDao(contentResolver);
    }

    @Override
    protected boolean logEvent() {
        return true;
    }

    @Override
    protected String getModelName() {
        return "CuentaCteTotal";
    }

    @Override
    public String getTableName() {
        return CuentaTable.VW_TOTALES_NAME;
    }

    @Override
    protected ContentValues toContentValues(CuentaCteTotal instance) {
        ContentValues ctacteValues = new ContentValues();

        long clienteId = instance.getCliente() != null ? instance.getCliente().getId() : 0;
        ctacteValues.put(CuentaTable.COLUMN_VW_CLIENTE_ID, clienteId);
        ctacteValues.put(CuentaTable.COLUMN_VW_SALDO, instance.getSaldo());
        ctacteValues.put(CuentaTable.COLUMN_VW_FECHA_ULT_PAGO, Formatos.DbDateTimeFormat.format(instance.getFecha_ult_pago()));
        ctacteValues.put(CuentaTable.COLUMN_VW_MONTO_ULT_PAGO, instance.getMonto_ult_pago());

        return ctacteValues;
    }

    @Override
    protected CuentaCteTotal fromCursor(Cursor cursor) {
        CuentaCteTotal ctacte = new CuentaCteTotal();

        Cliente cliente = clienteDao.find(cursor.getLong(0));
        Double saldo = cursor.getDouble(1);
        String fecha_ult_pago = cursor.getString(2);
        Double monto_ult_pago = cursor.getDouble(3);

        ctacte.setCliente(cliente);
        ctacte.setSaldo(saldo);
        try {
            ctacte.setFecha_ult_pago(!TextUtils.isEmpty(fecha_ult_pago) ? Formatos.DbDateTimeFormat.parse(fecha_ult_pago) : null);
        } catch (ParseException e) {
            Log.e("parse fecha_ult_pago", e.getMessage(), e);
        }
        ctacte.setMonto_ult_pago(monto_ult_pago);

        return ctacte;
    }

    @Override
    protected String[] getProjectionAttributes() {
        return PROJECTION_ATTRIBUTES;
    }
}