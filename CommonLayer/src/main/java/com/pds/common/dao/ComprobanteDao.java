package com.pds.common.dao;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;

import com.pds.common.db.ComprobanteTable;
import com.pds.common.model.Comprobante;

import java.util.Date;


public class ComprobanteDao extends GenericDao<Comprobante> {
    private static final String[] PROJECTION_ATTRIBUTES = ComprobanteTable.columnas;

    public ComprobanteDao(ContentResolver contentResolver) {
        super(contentResolver);
    }

    @Override
    protected boolean logEvent() {
        return false;
    }

    @Override
    protected String getModelName() {
        return "Comprobante";
    }

    @Override
    public String getTableName() {
        return ComprobanteTable.TABLE_NAME;
    }

    @Override
    protected ContentValues toContentValues(Comprobante instance) {
        ContentValues COMPROBANTEValues = new ContentValues();

        COMPROBANTEValues.put(ComprobanteTable.COMPROBANTE_TOTAL, instance.getTotal());
        COMPROBANTEValues.put(ComprobanteTable.COMPROBANTE_FECHA, instance.getFecha());
        COMPROBANTEValues.put(ComprobanteTable.COMPROBANTE_HORA, instance.getHora());
        COMPROBANTEValues.put(ComprobanteTable.COMPROBANTE_MEDIO_PAGO, instance.getMedio_pago());
        COMPROBANTEValues.put(ComprobanteTable.COMPROBANTE_CODIGO, instance.getCodigo());
        COMPROBANTEValues.put(ComprobanteTable.COMPROBANTE_CANTIDAD, instance.getCantidad());
        COMPROBANTEValues.put(ComprobanteTable.COMPROBANTE_USERID, instance.getUserid());
        COMPROBANTEValues.put(ComprobanteTable.COMPROBANTE_NETO, instance.getNeto());
        COMPROBANTEValues.put(ComprobanteTable.COMPROBANTE_IVA, instance.getIva());
        COMPROBANTEValues.put(ComprobanteTable.COMPROBANTE_ANULADA, instance.isAnulada() ? 1 : 0);
        if(instance.getSync() != null && !TextUtils.isEmpty(instance.getSync()))
            COMPROBANTEValues.put(ComprobanteTable.COMPROBANTE_SYNC_STATUS, instance.getSync());

        return COMPROBANTEValues;
    }

    @Override
    protected Comprobante fromCursor(Cursor cursor) {
        Comprobante COMPROBANTE = new Comprobante();

        long id = cursor.getLong(ComprobanteTable.COMPROBANTE_IX_ID);
        Double total = cursor.getDouble(ComprobanteTable.COMPROBANTE_IX_TOTAL);
        int cantidad = cursor.getInt(ComprobanteTable.COMPROBANTE_IX_CANTIDAD);
        String fecha = cursor.getString(ComprobanteTable.COMPROBANTE_IX_FECHA);
        String hora = cursor.getString(ComprobanteTable.COMPROBANTE_IX_HORA);
        String codigo = cursor.getString(ComprobanteTable.COMPROBANTE_IX_CODIGO);
        int userId = cursor.getInt(ComprobanteTable.COMPROBANTE_IX_USERID);
        String medioPago = cursor.getString(ComprobanteTable.COMPROBANTE_IX_MEDIO_PAGO);
        Double neto = cursor.getDouble(ComprobanteTable.COMPROBANTE_IX_NETO);
        Double iva = cursor.getDouble(ComprobanteTable.COMPROBANTE_IX_IVA);
        int anulada = cursor.getInt(ComprobanteTable.COMPROBANTE_IX_ANULADA);
        String sync = cursor.getString(ComprobanteTable.COMPROBANTE_IX_SYNC_STATUS);

        COMPROBANTE.setId(id);
        COMPROBANTE.setTotal(total);
        COMPROBANTE.setCantidad(cantidad);
        COMPROBANTE.setFecha(fecha);
        COMPROBANTE.setHora(hora);
        COMPROBANTE.setCodigo(codigo);
        COMPROBANTE.setUserid(userId);
        COMPROBANTE.setMedio_pago(medioPago);
        COMPROBANTE.setNeto(neto);
        COMPROBANTE.setIva(iva);
        COMPROBANTE.setAnulada(anulada == 1);
        COMPROBANTE.setSync(sync);

        return COMPROBANTE;
    }

    @Override
    protected String[] getProjectionAttributes() {
        return PROJECTION_ATTRIBUTES;
    }

    public boolean ControlaCorrelatividadEmision(Date fechaEmision){
        Comprobante ultimo = first("codigo in ('PDV','STK') and anulada = 0", null, ComprobanteTable.COMPROBANTE_ID + " DESC");

        if(ultimo != null )
            return fechaEmision.after( ultimo.getFechaHora_Date());//la fecha de registracion debe ser posterior
        else
            return true; //como no hay con quien comparar damos el OK
    }
}