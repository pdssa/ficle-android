package com.pds.common.dao;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.pds.common.db.DepartmentTable;
import com.pds.common.model.Department;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

public class DepartmentDao extends GenericDao<Department> {

    public static String[] PROJECTION_ATTRIBUTES = {
            DepartmentTable.COLUMN_ID,
            DepartmentTable.COLUMN_NAME,
            DepartmentTable.COLUMN_ALTA,
            DepartmentTable.COLUMN_DESCRIPTION,
            DepartmentTable.COLUMN_MODO_VISUAL,
            DepartmentTable.COLUMN_GENERIC,
            DepartmentTable.COLUMN_AUTO,
            DepartmentTable.COLUMN_CODIGO
    };

    public DepartmentDao(ContentResolver contentResolver) {
        super(contentResolver);
    }

    @Override
    protected boolean logEvent() {
        return false;
    }

    @Override
    protected String getModelName() {
        return "Departamento";
    }

    @Override
    public String getTableName() {
        return DepartmentTable.TABLE_NAME;
    }

    @Override
    protected ContentValues toContentValues(Department instance) {
        ContentValues departmentValues = new ContentValues();
        departmentValues.put(DepartmentTable.COLUMN_NAME, instance.getName());
        departmentValues.put(DepartmentTable.COLUMN_ALTA, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(instance.getAlta()));
        departmentValues.put(DepartmentTable.COLUMN_DESCRIPTION, instance.getDescription());
        departmentValues.put(DepartmentTable.COLUMN_SYNC_STATUS, "N");
        departmentValues.put(DepartmentTable.COLUMN_MODO_VISUAL, instance.getVisualization_mode());
        departmentValues.put(DepartmentTable.COLUMN_GENERIC, instance.getGeneric_dept());
        departmentValues.put(DepartmentTable.COLUMN_AUTO, instance.getAutomatic());
        departmentValues.put(DepartmentTable.COLUMN_CODIGO, instance.getCodigo());

        return departmentValues;
    }

    @Override
    protected Department fromCursor(Cursor cursor) {
        Department department = new Department();
        int i = 0;
        Long id = cursor.getLong(i++);
        String name = cursor.getString(i++);
        String alta = cursor.getString(i++);
        String description = cursor.getString(i++);
        department.setId(id);
        department.setName(name);
        department.setDescription(description);
        try {
            department.setAlta(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(alta));
        } catch (ParseException e) {
            Log.e("cannot parse alta", e.getMessage(), e);
        }
        department.setVisualization_mode(cursor.getInt(i++));
        department.setGeneric_dept(cursor.getInt(i++));
        department.setAutomatic(cursor.getInt(i++));
        department.setCodigo(cursor.getString(i++));

        return department;
    }

    @Override
    protected String[] getProjectionAttributes() {
        return PROJECTION_ATTRIBUTES;
    }

    public boolean importDepartment(Department instance) {

        //USAMOS EL CODIGO PARA CHEQUEAR SI EXISTE
        List<Department> result = list(DepartmentTable.COLUMN_CODIGO + "='" + instance.getCodigo() + "' and " + DepartmentTable.COLUMN_GENERIC + "=" + String.valueOf(instance.getGeneric_dept()), null, null);

        if (result.size() == 0) {
            //NO EXISTE => CREAR
            return save(instance);
        }
        else {
            //si encontramos depto ...lo reemplazamos con el import para aprovechar y normalizar los nombres
            instance.setId(result.get(0).getId());
            instance.setAlta(result.get(0).getAlta());

            return saveOrUpdate(instance);
        }
        /*} else {
            //USAMOS EL NOMBRE PARA CHEQUEAR SI EXISTE
            result = list(DepartmentTable.COLUMN_NAME + "='" + instance.getName() + "' and " + DepartmentTable.COLUMN_GENERIC + "=" + String.valueOf(instance.getGeneric_dept()), null, null);

            if (result.size() != 0) {
                //si encontramos depto con el nombre...lo reemplazamos con el import
                instance.setId(result.get(0).getId());

                return saveOrUpdate(instance);
            } else
                return false;*/
        //}
    }
}