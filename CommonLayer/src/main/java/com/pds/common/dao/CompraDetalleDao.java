package com.pds.common.dao;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;

import com.pds.common.db.CompraDetalleTable;
import com.pds.common.model.CompraDetalle;


public class CompraDetalleDao extends GenericDao<CompraDetalle> {

    private static final String[] PROJECTION_ATTRIBUTES = CompraDetalleTable.columnas;

    private ProductDao productDao;
    private CompraDao compraDao;

    public CompraDetalleDao(ContentResolver contentResolver) {
        super(contentResolver);
        this.productDao = new ProductDao(contentResolver);
        this.compraDao = new CompraDao(contentResolver);
    }

    @Override
    protected boolean logEvent() {
        return true;
    }

    @Override
    protected String getModelName() {
        return "CompraDetalle";
    }

    @Override
    public String getTableName() {
        return CompraDetalleTable.TABLE_NAME;
    }

    @Override
    protected ContentValues toContentValues(CompraDetalle instance) {
        ContentValues values = new ContentValues();

        long compraId = instance.getCompra() != null ? instance.getCompra().getId() : 0;
        //long productoId = instance.getProducto() != null ? instance.getProducto().getId() : -1;

        values.put(CompraDetalleTable.COMPRA_DET_COMPRA_ID, compraId);
        values.put(CompraDetalleTable.COMPRA_DET_PRODUCTO_ID, instance.getIdProducto());
        values.put(CompraDetalleTable.COMPRA_DET_CANTIDAD, instance.getCantidad());
        values.put(CompraDetalleTable.COMPRA_DET_PRECIO, instance.getPrecio());
        values.put(CompraDetalleTable.COMPRA_DET_SUBTOTAL_X, instance.getSubtotal_exento());
        values.put(CompraDetalleTable.COMPRA_DET_SUBTOTAL_NG, instance.getSubtotal_neto_grav());
        values.put(CompraDetalleTable.COMPRA_DET_SUBTOTAL_I, instance.getSubtotal_impuesto());
        values.put(CompraDetalleTable.COMPRA_DET_TOTAL, instance.getTotal());

        return values;
    }

    @Override
    protected CompraDetalle fromCursor(Cursor cursor) {
        CompraDetalle instance = new CompraDetalle();

        long id = cursor.getLong(cursor.getColumnIndex(CompraDetalleTable.COMPRA_DET_ID));
        int compraId = cursor.getInt(cursor.getColumnIndex(CompraDetalleTable.COMPRA_DET_COMPRA_ID));
        long productoId = cursor.getLong(cursor.getColumnIndex(CompraDetalleTable.COMPRA_DET_PRODUCTO_ID));
        int cantidad = cursor.getInt(cursor.getColumnIndex(CompraDetalleTable.COMPRA_DET_CANTIDAD));
        double precio = cursor.getDouble(cursor.getColumnIndex(CompraDetalleTable.COMPRA_DET_PRECIO));
        double subt_exento = cursor.getDouble(cursor.getColumnIndex(CompraDetalleTable.COMPRA_DET_SUBTOTAL_X));
        double subt_netograv = cursor.getDouble(cursor.getColumnIndex(CompraDetalleTable.COMPRA_DET_SUBTOTAL_NG));
        double subt_impuesto = cursor.getDouble(cursor.getColumnIndex(CompraDetalleTable.COMPRA_DET_SUBTOTAL_I));
        double total = cursor.getDouble(cursor.getColumnIndex(CompraDetalleTable.COMPRA_DET_TOTAL));

        instance.setId(id);
        instance.setIdCompra(compraId);
        instance.setIdProducto(productoId);
        instance.setCantidad(cantidad);
        instance.setPrecio(precio);
        instance.setSubtotal_exento(subt_exento);
        instance.setSubtotal_neto_grav(subt_netograv);
        instance.setSubtotal_impuesto(subt_impuesto);
        instance.setTotal(total);
        instance.setProducto(productoId != -1 ? productDao.find(productoId) : null);
        instance.setCompra(compraDao.find(compraId));

        return instance;
    }

    @Override
    protected String[] getProjectionAttributes() {
        return PROJECTION_ATTRIBUTES;
    }
}