package com.pds.common.dao;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;

import com.pds.common.db.ProviderTable;
import com.pds.common.model.Provider;
import com.pds.common.model.ProviderProduct;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProviderDao extends GenericDao<Provider>{

    private final SimpleDateFormat altaDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static final String[] PROJECTION_ATTRIBUTES = {
            ProviderTable.COLUMN_ID,
            ProviderTable.COLUMN_NAME,
            ProviderTable.COLUMN_ALTA,
            ProviderTable.COLUMN_DEPARTMENT_ID,
            ProviderTable.COLUMN_SUBDEPARTMENT_ID,
            ProviderTable.COLUMN_CONTACT,
            ProviderTable.COLUMN_EMAIL,
            ProviderTable.COLUMN_ADDRESS,
            ProviderTable.COLUMN_CITY,
            ProviderTable.COLUMN_PHONE_NUMBER,
            ProviderTable.COLUMN_DESCRIPTION,
            ProviderTable.COLUMN_TAXID};

    //private DepartmentDao departmentDao;
    //private SubDepartmentDao subDepartmentDao;
    private ProviderProductDao providerProductDao;

    public ProviderDao(ContentResolver contentResolver) {
        super(contentResolver);
        //this.departmentDao = new DepartmentDao(contentResolver);
        //this.subDepartmentDao = new SubDepartmentDao(contentResolver);
        this.providerProductDao = new ProviderProductDao(contentResolver);
    }

    public List<Provider> getProvidersProduct(long _idProduct){
        List<ProviderProduct> rel_list = this.providerProductDao.list(String.format("product_id = %d", _idProduct),null,null);
        List<Provider> list = new ArrayList<Provider>();
        if(rel_list != null){
            for(ProviderProduct rel : rel_list){
                list.add(this.find(rel.getIdProvider()));
            }
        }

        return list;
    }
    @Override
    protected boolean logEvent() {
        return false;
    }
    @Override
    protected String getModelName() {
        return "Proveedor";
    }

    @Override
    public String getTableName() {
        return ProviderTable.TABLE_NAME;
    }

    @Override
    protected ContentValues toContentValues(Provider instance) {
        ContentValues providerValues = new ContentValues();
        providerValues.put(ProviderTable.COLUMN_NAME, instance.getName());
        providerValues.put(ProviderTable.COLUMN_ALTA, altaDateFormat.format(new Date()));
        long departmentId = instance.getDepartment() != null? instance.getDepartment().getId() : instance.getIdDepartment();
        long subDepartmentId = instance.getSubDepartment() != null? instance.getSubDepartment().getId() : instance.getIdSubdepartment();
        providerValues.put(ProviderTable.COLUMN_DEPARTMENT_ID, departmentId);
        providerValues.put(ProviderTable.COLUMN_SUBDEPARTMENT_ID, subDepartmentId);
        providerValues.put(ProviderTable.COLUMN_CONTACT, instance.getContact());
        providerValues.put(ProviderTable.COLUMN_EMAIL, instance.getEmail());
        providerValues.put(ProviderTable.COLUMN_ADDRESS, instance.getAddress());
        providerValues.put(ProviderTable.COLUMN_CITY, instance.getCity());
        providerValues.put(ProviderTable.COLUMN_PHONE_NUMBER, instance.getPhoneNumber());
        providerValues.put(ProviderTable.COLUMN_DESCRIPTION, instance.getDescription());
        providerValues.put(ProviderTable.COLUMN_SYNC_STATUS, "N");
        providerValues.put(ProviderTable.COLUMN_TAXID, instance.getTax_id());
        return providerValues;
    }

    @Override
    protected Provider fromCursor(Cursor cursor) {
        Provider provider = new Provider();
        provider.setId(cursor.getLong(cursor.getColumnIndex(ProviderTable.COLUMN_ID)));
        provider.setName(cursor.getString(cursor.getColumnIndex(ProviderTable.COLUMN_NAME)));
        String alta = cursor.getString(cursor.getColumnIndex(ProviderTable.COLUMN_ALTA));
        provider.setIdDepartment(cursor.getLong(cursor.getColumnIndex(ProviderTable.COLUMN_DEPARTMENT_ID)));
        provider.setIdSubdepartment(cursor.getLong(cursor.getColumnIndex(ProviderTable.COLUMN_SUBDEPARTMENT_ID)));
        provider.setContact(cursor.getString(cursor.getColumnIndex(ProviderTable.COLUMN_CONTACT)));
        provider.setEmail(cursor.getString(cursor.getColumnIndex(ProviderTable.COLUMN_EMAIL)));
        provider.setAddress(cursor.getString(cursor.getColumnIndex(ProviderTable.COLUMN_ADDRESS)));
        provider.setCity(cursor.getString(cursor.getColumnIndex(ProviderTable.COLUMN_CITY)));
        provider.setPhoneNumber(cursor.getString(cursor.getColumnIndex(ProviderTable.COLUMN_PHONE_NUMBER)));
        provider.setDescription(cursor.getString(cursor.getColumnIndex(ProviderTable.COLUMN_DESCRIPTION)));
        try {
            provider.setAlta(altaDateFormat.parse(alta));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        provider.setTax_id(cursor.getString(cursor.getColumnIndex(ProviderTable.COLUMN_TAXID)));

        //provider.setDepartment(departmentDao.find(departmentId));
        //provider.setSubDepartment(subDepartmentDao.find(subDepartmentId));

        return provider;
    }

    @Override
    protected String[] getProjectionAttributes() {
        return PROJECTION_ATTRIBUTES;
    }

    public boolean importProvider(Provider instance) {

        String tax_id = instance.getTax_id().replace("-", "").replace(".","").toUpperCase().trim();

        //USAMOS EL RUT/CUIT PARA CHEQUEAR SI EXISTE
        List<Provider> result = list("upper(replace(replace(" + ProviderTable.COLUMN_TAXID + ",'-',''),'.','')) = upper('" + tax_id + "') " , null, null);

        if (result.size() == 0) {
            //NO EXISTE => CREAR
            return save(instance);
        } else {
            //si encontramos lo reemplazamos con el import
            instance.setId(result.get(0).getId());

            return saveOrUpdate(instance);
        }
    }
}
