package com.pds.common.dao;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;

import com.pds.common.Formatos;
import com.pds.common.db.HistMovStockTable;
import com.pds.common.model.HistMovStock;

import java.util.Date;

/**
 * Created by EMEKA on 21/01/2015.
 */
public class HistMovStockDao extends GenericDao<HistMovStock>{
    private static final String[] PROJECTION_ATTRIBUTES = HistMovStockTable.columnas;

    //private HistMovStockDao histMovStockDao;

    public HistMovStockDao(ContentResolver contentResolver) {
        super(contentResolver);

        //histMovStockDao = new HistMovStockDao(contentResolver);
    }

    @Override
    protected boolean logEvent() {
        return false;
    }

    @Override
    protected String getModelName() {
        return "HistMovStock";
    }

    @Override
    public String getTableName() {
        return HistMovStockTable.TABLE_NAME;
    }

    @Override
    protected ContentValues toContentValues(HistMovStock instance) {
        ContentValues values = new ContentValues();

        //values.put(HistMovStockTable.STOCK_ID, instance.getId());
        values.put(HistMovStockTable.STOCK_PRODUCTO_ID, instance.getProductoid());
        values.put(HistMovStockTable.STOCK_CANTIDAD, instance.getCantidad());
        values.put(HistMovStockTable.STOCK_MONTO, instance.getMonto());
        values.put(HistMovStockTable.STOCK_OPERACION, instance.getOperacion());
        values.put(HistMovStockTable.STOCK_STOCK, instance.getStock());
        values.put(HistMovStockTable.STOCK_OBSERVACION, instance.getObservacion());
        values.put(HistMovStockTable.STOCK_FECHA, Formatos.DbDate(instance.getFecha()));
        values.put(HistMovStockTable.STOCK_USUARIO, instance.getUsuario());

        return values;
    }

    @Override
    protected HistMovStock fromCursor(Cursor cursor) {
        HistMovStock instance = new HistMovStock();

        long id = cursor.getLong(cursor.getColumnIndex(HistMovStockTable.STOCK_ID));
        long productoId = cursor.getLong(cursor.getColumnIndex(HistMovStockTable.STOCK_PRODUCTO_ID));
        int cantidad = cursor.getInt(cursor.getColumnIndex(HistMovStockTable.STOCK_CANTIDAD));
        double monto = cursor.getDouble(cursor.getColumnIndex(HistMovStockTable.STOCK_MONTO));
        String operacion = cursor.getString(cursor.getColumnIndex(HistMovStockTable.STOCK_OPERACION));
        int stock = cursor.getInt(cursor.getColumnIndex(HistMovStockTable.STOCK_STOCK));
        String observacion = cursor.getString(cursor.getColumnIndex(HistMovStockTable.STOCK_OBSERVACION));
        String fecha = cursor.getString(cursor.getColumnIndex(HistMovStockTable.STOCK_FECHA));
        String usuario = cursor.getString(cursor.getColumnIndex(HistMovStockTable.STOCK_USUARIO));

        instance.setId(id);
        instance.setProductoid(productoId);
        instance.setCantidad(cantidad);
        instance.setMonto(monto);
        instance.setOperacion(operacion);
        instance.setStock(stock);
        instance.setObservacion(observacion);
        try {
            instance.setFecha(Formatos.DbDate(fecha));
        } catch (Exception e) {
            e.printStackTrace();
        }
        instance.setUsuario(usuario);

        return instance;
    }

    @Override
    protected String[] getProjectionAttributes()  {
        return PROJECTION_ATTRIBUTES;
    }
}
