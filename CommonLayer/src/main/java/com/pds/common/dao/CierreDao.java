package com.pds.common.dao;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.pds.common.Formatos;
import com.pds.common.db.CierreTable;
import com.pds.common.db.ClienteTable;
import com.pds.common.model.Cierre;

import java.util.Date;

/**
 * Created by Hernan on 10/12/2014.
 */
public class CierreDao extends GenericDao<Cierre> {

    public CierreDao(ContentResolver contentResolver) {
        super(contentResolver);
    }
    @Override
    protected boolean logEvent() {
        return true;
    }
    @Override
    protected String getModelName() {
        return "Cierre";
    }

    @Override
    public String getTableName() {
        return CierreTable.TABLE_NAME;
    }

    @Override
    protected ContentValues toContentValues(Cierre instance) {
        ContentValues clienteValues = new ContentValues();
        clienteValues.put(CierreTable.COLUMN_FECHA_CIERRE, Formatos.FormateaDate(instance.getFecha(), Formatos.DbDateTimeFormat));
        clienteValues.put(CierreTable.COLUMN_AFECTAS_Q, instance.getAfectas_cant());
        clienteValues.put(CierreTable.COLUMN_AFECTAS_T, instance.getAfectas_total());
        clienteValues.put(CierreTable.COLUMN_EXENTAS_Q, instance.getExentas_cant());
        clienteValues.put(CierreTable.COLUMN_EXENTAS_T, instance.getExentas_total());
        clienteValues.put(CierreTable.COLUMN_MENORES_Q, instance.getMenores_cant());
        clienteValues.put(CierreTable.COLUMN_MENORES_T, instance.getMenores_total());
        clienteValues.put(CierreTable.COLUMN_OTROS_Q, instance.getOtros_cant());
        clienteValues.put(CierreTable.COLUMN_OTROS_T, instance.getOtros_total());
        clienteValues.put(CierreTable.COLUMN_IVA, instance.getIva());
        clienteValues.put(CierreTable.COLUMN_FECHA_DESDE, Formatos.FormateaDate(instance.getFechaDesde(), Formatos.DbDateTimeFormat));
        return clienteValues;
    }

    @Override
    protected Cierre fromCursor(Cursor cursor) {
        Cierre cierre = new Cierre();
        int i = 0;

        Long id = cursor.getLong(i++);
        Date fechaCierre = null;
        try {
            fechaCierre = Formatos.ObtieneDate(cursor.getString(i++), Formatos.DbDateTimeFormat);
        } catch (Exception e) {
            Log.e("CierreDao", "cannot parse fecha cierre", e);
        }
        int afectas_q = cursor.getInt(i++);
        double afectas_t = cursor.getDouble(i++);
        int exentas_q = cursor.getInt(i++);
        double exentas_t = cursor.getDouble(i++);
        int menores_q = cursor.getInt(i++);
        double menores_t = cursor.getDouble(i++);
        int otros_q = cursor.getInt(i++);
        double otros_t = cursor.getDouble(i++);
        double iva = cursor.getDouble(i++);

        Date fechaDesde = null;
        try {
            fechaDesde = Formatos.ObtieneDate(cursor.getString(i++), Formatos.DbDateTimeFormat);
        } catch (Exception e) {
            Log.e("CierreDao", "cannot parse fecha desde", e);
        }

        cierre.setId(id);
        cierre.setFecha(fechaCierre);
        cierre.setAfectas_cant(afectas_q);
        cierre.setAfectas_total(afectas_t);
        cierre.setExentas_cant(exentas_q);
        cierre.setExentas_total(exentas_t);
        cierre.setMenores_cant(menores_q);
        cierre.setMenores_total(menores_t);
        cierre.setOtros_cant(otros_q);
        cierre.setOtros_total(otros_t);
        cierre.setIva(iva);
        cierre.setFechaDesde(fechaDesde);

        return cierre;
    }

    @Override
    protected String[] getProjectionAttributes() {
        return CierreTable.COLUMNAS;
    }
}
