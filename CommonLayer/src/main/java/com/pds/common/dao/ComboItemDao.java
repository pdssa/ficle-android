package com.pds.common.dao;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.pds.common.Formatos;
import com.pds.common.db.ComboItemTable;
import com.pds.common.db.ComboItemTable;
import com.pds.common.model.ComboItem;

/**
 * Created by Hernan on 25/02/2015.
 */
public class ComboItemDao extends GenericDao<ComboItem> {
    private static final String[] PROJECTION_ATTRIBUTES = ComboItemTable.COLUMNAS;

    @Override
    protected boolean logEvent() {
        return false;
    }

    public ComboItemDao(ContentResolver contentResolver) {
        super(contentResolver);
    }

    @Override
    protected String getModelName() {
        return "ComboItem";
    }

    @Override
    public String getTableName() {
        return ComboItemTable.TABLE_NAME;
    }

    @Override
    protected ContentValues toContentValues(ComboItem instance) {
        ContentValues values = new ContentValues();

        values.put(ComboItemTable.COLUMN_CANTIDAD, instance.getCantidad());
        values.put(ComboItemTable.COLUMN_PRODUCTO_CODE, instance.getProducto_code());
        values.put(ComboItemTable.COLUMN_PRODUCTO_ID, instance.getProducto_id());
        values.put(ComboItemTable.COLUMN_COMBO_ID, instance.getCombo_id());
        values.put(ComboItemTable.COLUMN_SYNC_STATUS, instance.getSync());

        return values;
    }

    @Override
    protected ComboItem fromCursor(Cursor cursor) {
        ComboItem instance = new ComboItem();

        long id = cursor.getLong(cursor.getColumnIndex(ComboItemTable.COLUMN_ID));
        long combo_id = cursor.getLong(cursor.getColumnIndex(ComboItemTable.COLUMN_COMBO_ID));
        long producto_id = cursor.getLong(cursor.getColumnIndex(ComboItemTable.COLUMN_PRODUCTO_ID));
        String producto_code = cursor.getString(cursor.getColumnIndex(ComboItemTable.COLUMN_PRODUCTO_CODE));
        Double cantidad = cursor.getDouble(cursor.getColumnIndex(ComboItemTable.COLUMN_CANTIDAD));
        String sync = cursor.getString(cursor.getColumnIndex(ComboItemTable.COLUMN_SYNC_STATUS));

        instance.setId(id);
        instance.setCombo_id(combo_id);
        instance.setProducto_id(producto_id);
        instance.setProducto_code(producto_code);
        instance.setCantidad(cantidad);
        instance.setSync(sync);

        return instance;
    }

    @Override
    protected String[] getProjectionAttributes() {
        return PROJECTION_ATTRIBUTES;
    }
}