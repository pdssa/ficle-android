package com.pds.common.dao;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.pds.common.Logger;
import com.pds.common.VentasHelper;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by raul.lopez on 5/26/2014.
 */
public abstract class GenericDao<T> {

    public String BASE_URL = "content://com.pds.ficle.ep.provider/";

    protected final ContentResolver contentResolver;

    public ContentResolver getContentResolver() {
        return contentResolver;
    }

    public GenericDao(ContentResolver contentResolver) {
        this.contentResolver = contentResolver;

        if (this.getTableName().equals(VentasHelper.TABLE_NAME)) {
            BASE_URL = "content://com.pds.ficle.ep.ventas.contentprovider/";
        } else if (this.getTableName().equals("venta_detalles")) {
            BASE_URL = "content://com.pds.ficle.ep.venta_detalles.contentprovider/";
        }
    }

    /*public int bulkInsert(T[] instances){
        ContentValues[] values = new ContentValues[instances.length];

        for(int i=0; i< instances .length; i++){
            values[i] = this.toContentValues(instances[i]);
        }

        Uri uri = Uri.parse(BASE_URL + getTableName());
        int createdObjects = contentResolver.bulkInsert(uri, values);

        if (createdObjects > 0) {
            String title = String.format("Alta Masiva %s", getModelName());
            String description = String.format("Total : %s", createdObjects);
            Logger.RegistrarEvento(contentResolver, "i", title, description, "");
        }

        return createdObjects;
    }*/

    public boolean save(T instance) {
        ContentValues values = this.toContentValues(instance);
        Uri uri = Uri.parse(BASE_URL + getTableName());
        Uri createdObject = contentResolver.insert(uri, values);

        if (createdObject == null) {
            return false;
        }
        String id = createdObject.getLastPathSegment();
        this.setId(instance, Long.parseLong(id));
        if(logEvent()) {
            String title = String.format("Alta %s", getModelName());
            String description = String.format("%s id : %s", getModelName(), id);
            Logger.RegistrarEvento(contentResolver, "i", title, description, "");
        }
        return true;
    }

    public boolean saveOrUpdate(T instance) {
        boolean found = this.find(this.getId(instance)) != null;
        return (found) ? update(instance) : this.save(instance);
    }

    public boolean update(T instance) {
        long id = this.getId(instance);
        Uri uri = Uri.parse(BASE_URL + getTableName() + "/" + id);
        int updatedRows = contentResolver.update(uri, this.toContentValues(instance), null, null);
        if (updatedRows <= 0) {
            return false;
        }
        if(logEvent()) {
            String title = String.format("Modificacion %s", getModelName());
            String description = String.format("%s id : %d", getModelName(), id);
            Logger.RegistrarEvento(contentResolver, "i", title, description, "");
        }
        return true;
    }

    public T find(long id) {
        Uri uri = Uri.parse(BASE_URL + getTableName() + "/" + id);
        Cursor cursor = contentResolver.query(uri, getProjectionAttributes(), null, null, null);
        try {
            if (cursor.moveToNext()) {
                return this.fromCursor(cursor);
            }
            return null;
        } finally {
            cursor.close();
        }
    }

    public List<T> list() {
        return this.list(null, null, null);
    }

    public List<T> list(String selection, String[] selectionArgs, String sortOrder) {
        Uri uri = Uri.parse(BASE_URL + getTableName());
        Cursor cursor = contentResolver.query(
                uri,
                getProjectionAttributes(),
                selection,
                selectionArgs,
                sortOrder);
        List<T> elements = new ArrayList<T>(cursor.getCount());
        while (cursor.moveToNext()) {
            T t = this.fromCursor(cursor);
            elements.add(t);
        }
        cursor.close();
        return elements;
    }

    public List<T> list(String selection, String[] selectionArgs, String sortOrder, String groupBy, String having, String limit) {
        Uri uri = Uri.parse(BASE_URL + getTableName());
        String _limit = limit != null ? (" LIMIT " + limit) : "";
        String _lastParam = sortOrder != null ? sortOrder + _limit : _limit;

        Cursor cursor = contentResolver.query(
                uri,
                getProjectionAttributes(),
                selection,
                selectionArgs,
                _lastParam); //agregamos el limit al final del ultimo parametro

        List<T> elements = new ArrayList<T>(cursor.getCount());
        while (cursor.moveToNext()) {
            T t = this.fromCursor(cursor);
            elements.add(t);
        }
        cursor.close();
        return elements;
    }

    public T first(String selection, String[] selectionArgs, String sortOrder) {
        List<T> elements = this.list(selection, selectionArgs, sortOrder);

        if (elements.size() > 0)
            return elements.get(0);
        else
            return null;
    }

    public void delete(T instance) {
        long id = getId(instance);
        Uri uri = Uri.parse(BASE_URL + getTableName() + "/" + id);
        int deletedRows = contentResolver.delete(uri, null, null);
        if (deletedRows > 0 && logEvent()) {
            String title = String.format("Baja %s", getModelName());
            String description = String.format("%s id : %d", getModelName(), id);
            Logger.RegistrarEvento(contentResolver, "i", title, description, "");
        }
        Log.d("GenericDao", String.format("Deleted %d for uri: %s", deletedRows, uri.toString()));
    }

    protected abstract String getModelName();

    protected long getId(T instance) {
        try {
            Method method = instance.getClass().getMethod("getId");
            return (Long) method.invoke(instance);

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {

            e.printStackTrace();
        }
        return 0;
    }

    protected void setId(T instance, Long id) {
        try {
            Method method = instance.getClass().getMethod("setId", id.getClass());
            method.invoke(instance, id);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    public abstract String getTableName();

    protected abstract ContentValues toContentValues(T instance);

    protected abstract T fromCursor(Cursor cursor);

    protected abstract String[] getProjectionAttributes();

    protected abstract boolean logEvent();
}
