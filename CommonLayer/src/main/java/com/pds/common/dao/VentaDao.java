package com.pds.common.dao;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;

import com.pds.common.VentasHelper;
import com.pds.common.model.Venta;

import java.util.Date;

public class VentaDao extends GenericDao<Venta> {
    private static final String[] PROJECTION_ATTRIBUTES = VentasHelper.columnas;

    public VentaDao(ContentResolver contentResolver) {
        super(contentResolver);
    }

    @Override
    protected boolean logEvent() {
        return false;
    }

    @Override
    protected String getModelName() {
        return "Venta";
    }

    @Override
    public String getTableName() {
        return VentasHelper.TABLE_NAME;
    }

    @Override
    protected ContentValues toContentValues(Venta instance) {
        ContentValues ventaValues = new ContentValues();

        ventaValues.put(VentasHelper.VENTA_TOTAL, instance.getTotal());
        ventaValues.put(VentasHelper.VENTA_FECHA, instance.getFecha());
        ventaValues.put(VentasHelper.VENTA_HORA, instance.getHora());
        ventaValues.put(VentasHelper.VENTA_MEDIO_PAGO, instance.getMedio_pago());
        ventaValues.put(VentasHelper.VENTA_CODIGO, instance.getCodigo());
        ventaValues.put(VentasHelper.VENTA_CANTIDAD, instance.getCantidad());
        ventaValues.put(VentasHelper.VENTA_USERID, instance.getUserid());
        ventaValues.put(VentasHelper.VENTA_FC_TYPE, instance.getFc_type());
        ventaValues.put(VentasHelper.VENTA_FC_ID, instance.getFc_id());
        ventaValues.put(VentasHelper.VENTA_FC_FOLIO, instance.getFc_folio());
        ventaValues.put(VentasHelper.VENTA_FC_VALIDATION, instance.getFc_validation());
        ventaValues.put(VentasHelper.VENTA_NETO, instance.getNeto());
        ventaValues.put(VentasHelper.VENTA_IVA, instance.getIva());
        if(instance.getSync() != null && !TextUtils.isEmpty(instance.getSync()))
            ventaValues.put(VentasHelper.VENTA_SYNC_STATUS, instance.getSync());
        ventaValues.put(VentasHelper.VENTA_TIMBRE, instance.getFc_Timbre());
        ventaValues.put(VentasHelper.VENTA_ANULADA, instance.isAnulada() ? 1 : 0);

        return ventaValues;
    }

    @Override
    protected Venta fromCursor(Cursor cursor) {
        Venta venta = new Venta();

        long id = cursor.getLong(VentasHelper.VENTA_IX_ID);
        Double total = cursor.getDouble(VentasHelper.VENTA_IX_TOTAL);
        int cantidad = cursor.getInt(VentasHelper.VENTA_IX_CANTIDAD);
        String fecha = cursor.getString(VentasHelper.VENTA_IX_FECHA);
        String hora = cursor.getString(VentasHelper.VENTA_IX_HORA);
        String codigo = cursor.getString(VentasHelper.VENTA_IX_CODIGO);
        int userId = cursor.getInt(VentasHelper.VENTA_IX_USERID);
        String medioPago = cursor.getString(VentasHelper.VENTA_IX_MEDIO_PAGO);
        String fc_type = cursor.getString(VentasHelper.VENTA_IX_FC_TYPE);
        long fc_id = cursor.getLong(VentasHelper.VENTA_IX_FC_ID);
        String fc_folio = cursor.getString(VentasHelper.VENTA_IX_FC_FOLIO);
        String fc_validat = cursor.getString(VentasHelper.VENTA_IX_FC_VALIDATION);
        Double neto = cursor.getDouble(VentasHelper.VENTA_IX_NETO);
        Double iva = cursor.getDouble(VentasHelper.VENTA_IX_IVA);
        String sync = cursor.getString(VentasHelper.VENTA_IX_SYNC_STATUS);
        String stamp = cursor.getString(VentasHelper.VENTA_IX_TIMBRE);
        int anulada = cursor.getInt(VentasHelper.VENTA_IX_ANULADA);

        venta.setId(id);
        venta.setTotal(total);
        venta.setCantidad(cantidad);
        venta.setFecha(fecha);
        venta.setHora(hora);
        venta.setCodigo(codigo);
        venta.setUserid(userId);
        venta.setMedio_pago(medioPago);
        venta.setFc_type(fc_type);
        venta.setFc_id(fc_id);
        venta.setFc_folio(fc_folio);
        venta.setFc_validation(fc_validat);
        venta.setNeto(neto);
        venta.setIva(iva);
        venta.setSync(sync);
        venta.setFc_Timbre(stamp);
        venta.setAnulada(anulada == 1);

        return venta;
    }

    @Override
    protected String[] getProjectionAttributes() {
        return PROJECTION_ATTRIBUTES;
    }

    public boolean ControlaCorrelatividadEmision(Date fechaEmision){
        Venta ultimo = first("codigo in ('PDV','STK') and anulada = 0", null, VentasHelper.VENTA_ID + " DESC");

        if(ultimo != null )
            return fechaEmision.after( ultimo.getFechaHora_Date());//la fecha de registracion debe ser posterior
        else
            return true; //como no hay con quien comparar damos el OK
    }

}