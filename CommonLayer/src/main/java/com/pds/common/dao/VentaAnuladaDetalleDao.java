package com.pds.common.dao;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;

import com.pds.common.db.VentaAnuladaDetalleTable;
import com.pds.common.model.VentaAnuladaDetalle;

/**
 * Created by Hernan on 24/02/2015.
 */
public class VentaAnuladaDetalleDao extends GenericDao<VentaAnuladaDetalle> {

    private static final String[] PROJECTION_ATTRIBUTES = VentaAnuladaDetalleTable.columnas;

    private VentaAnuladaDao ventaAnuladaDao;

    public VentaAnuladaDetalleDao(ContentResolver contentResolver) {
        super(contentResolver);
        this.ventaAnuladaDao = new VentaAnuladaDao(contentResolver);
    }
    @Override
    protected boolean logEvent() {
        return false;
    }
    @Override
    protected String getModelName() {
        return "VentaAnuladaDetalle";
    }

    @Override
    public String getTableName() {
        return VentaAnuladaDetalleTable.TABLE_NAME;
    }

    @Override
    protected ContentValues toContentValues(VentaAnuladaDetalle instance) {
        ContentValues detalleValues = new ContentValues();

        long ventaId = instance.getVentaAnulada() != null ? instance.getVentaAnulada().getId() : instance.getIdVentaAnulada();
        //long productoId = instance.getProducto() != null ? instance.getProducto().getId() : -1;

        detalleValues.put(VentaAnuladaDetalleTable.COLUMN_VENTA_ANULADA_ID, ventaId);
        detalleValues.put(VentaAnuladaDetalleTable.COLUMN_PRODUCTO_ID, instance.getIdProducto());
        detalleValues.put(VentaAnuladaDetalleTable.COLUMN_CANTIDAD, instance.getCantidad());
        detalleValues.put(VentaAnuladaDetalleTable.COLUMN_PRECIO, instance.getPrecio());
        detalleValues.put(VentaAnuladaDetalleTable.COLUMN_TOTAL, instance.getTotal());
        detalleValues.put(VentaAnuladaDetalleTable.COLUMN_SKU, instance.getSku());
        detalleValues.put(VentaAnuladaDetalleTable.COLUMN_NETO, instance.getNeto());
        detalleValues.put(VentaAnuladaDetalleTable.COLUMN_IVA, instance.getIva());

        return detalleValues;
    }

    @Override
    protected VentaAnuladaDetalle fromCursor(Cursor cursor) {
        VentaAnuladaDetalle instance = new VentaAnuladaDetalle();

        long id = cursor.getLong(cursor.getColumnIndex(VentaAnuladaDetalleTable.COLUMN_ID));
        Double total = cursor.getDouble(cursor.getColumnIndex(VentaAnuladaDetalleTable.COLUMN_TOTAL));
        Double cantidad = cursor.getDouble(cursor.getColumnIndex(VentaAnuladaDetalleTable.COLUMN_CANTIDAD));
        Double precio = cursor.getDouble(cursor.getColumnIndex(VentaAnuladaDetalleTable.COLUMN_PRECIO));
        long ventaAnuladaId = cursor.getLong(cursor.getColumnIndex(VentaAnuladaDetalleTable.COLUMN_VENTA_ANULADA_ID));
        long productoId = cursor.getLong(cursor.getColumnIndex(VentaAnuladaDetalleTable.COLUMN_PRODUCTO_ID));
        String sku = cursor.getString(cursor.getColumnIndex(VentaAnuladaDetalleTable.COLUMN_SKU));
        Double neto = cursor.getDouble(cursor.getColumnIndex(VentaAnuladaDetalleTable.COLUMN_NETO));
        Double iva = cursor.getDouble(cursor.getColumnIndex(VentaAnuladaDetalleTable.COLUMN_IVA));

        instance.setId(id);
        instance.setCantidad(cantidad);
        instance.setPrecio(precio);
        instance.setTotal(total);
        //instance.setProducto(productoId != -1 ? productDao.find(productoId) : null);
        instance.setIdProducto(productoId);
        instance.setVentaAnulada(ventaAnuladaDao.find(ventaAnuladaId));
        instance.setSku(sku);
        instance.setNeto(neto);
        instance.setIva(iva);

        return instance;
    }

    @Override
    protected String[] getProjectionAttributes() {
        return PROJECTION_ATTRIBUTES;
    }
}
