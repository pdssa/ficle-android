package com.pds.common;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Hernan on 14/02/14.
 */
public abstract class Formatos {

    private static final Locale locale = new Locale("es", "CL");
    private static final Locale localDefault = Locale.US;
    private static final Locale localPE = new Locale("es", "PE");

    public static final SimpleDateFormat DateFormat = new SimpleDateFormat("dd-MM-yyyy");
    public static final SimpleDateFormat MonthYearFormat = new SimpleDateFormat("MM-yyyy");
    public static final SimpleDateFormat YearMonthFormat = new SimpleDateFormat("yyyy-MM");
    public static final SimpleDateFormat ShortDateFormat = new SimpleDateFormat("dd-MM-yy");
    public static final SimpleDateFormat DayMonthFormat = new SimpleDateFormat("dd-MM");
    public static final SimpleDateFormat DbDateFormat = new SimpleDateFormat("yyyyMMdd");
    public static final SimpleDateFormat DbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final SimpleDateFormat DbDateTimeFormatTimeZone = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
    public static final SimpleDateFormat DbDate_Format = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat TimeFormat = new SimpleDateFormat("HH:mm:ss");
    public static final SimpleDateFormat ShortTimeFormat = new SimpleDateFormat("HH:mm");
    public static final SimpleDateFormat PDVDateTimeFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    public static final SimpleDateFormat FullDateTimeFormat = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
    public static final SimpleDateFormat FullDateTimeFormatNoSeconds = new SimpleDateFormat("dd-MM-yy HH:mm");
    public static final SimpleDateFormat ShortDateFormatNameOfDay = new SimpleDateFormat("EEE dd/MM", locale);
    public static final SimpleDateFormat DateFormatSlash = new SimpleDateFormat("dd/MM/yyyy");
    public static final SimpleDateFormat DateFormatSlashAndTime = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    public static final SimpleDateFormat DateFormatSlashAndTimeSec = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    public static final SimpleDateFormat SIIDateTimeFormat = new SimpleDateFormat("yyMMddHHmmss");
    public static final SimpleDateFormat DateTwoDigFormatSlash = new SimpleDateFormat("dd/MM/yy");
    public static final SimpleDateFormat DayMonthShortTimeFormat = new SimpleDateFormat("dd-MM HH:mm");


    public static double RedondeaDecimal(double valor) {
        return RedondeaDecimal(valor, Formatos.DecimalFormat_CH);
    }
    public static double RedondeaDecimal(double valor, DecimalFormat format) {
        return Formatos.ParseaDecimal(Formatos.FormateaDecimal(valor, format),format);
    }

    public static String FormateaDate(Date date, SimpleDateFormat formato) {
        return formato.format(date);
    }

    public static Date ObtieneDate(String date, SimpleDateFormat formato) throws Exception {
        return formato.parse(date);
    }

    public static Date DbDate(String date) throws Exception {
        return ObtieneDate(date, DbDateTimeFormat);
    }

    public static String DbDate(Date date) {
        return FormateaDate(date, DbDateTimeFormat);
    }

    public static final DecimalFormat DecimalFormat = (DecimalFormat) NumberFormat.getNumberInstance(locale);
    public static final DecimalFormat DecimalFormat_US = (DecimalFormat) NumberFormat.getNumberInstance(localDefault);
    public static final DecimalFormat DecimalFormat_CH = (DecimalFormat) NumberFormat.getNumberInstance(localDefault);
    public static final DecimalFormat DecimalFormat_PE = (DecimalFormat) NumberFormat.getNumberInstance(localPE);

    public static final String CurrencySymbol = "$";
    public static final String CurrencySymbol_PE = "S/.";

    private static void PreparaFormatos() {
        //US (Default)
        DecimalFormat_US.setGroupingSize(0);
        DecimalFormat.setMinimumFractionDigits(2);
        DecimalFormat.setMaximumFractionDigits(2);
        //CH
        DecimalFormatSymbols CH_Symbols = new DecimalFormatSymbols(localDefault);
        CH_Symbols.setGroupingSeparator('.');
        CH_Symbols.setDecimalSeparator(',');//Fix kitkat parse..
        DecimalFormat_CH.setDecimalFormatSymbols(CH_Symbols);
        DecimalFormat_CH.setGroupingSize(3);
        DecimalFormat_CH.setMaximumFractionDigits(0);
        DecimalFormat_CH.setRoundingMode(RoundingMode.HALF_UP);
        //AR (1.500,90)
        DecimalFormat.setMinimumFractionDigits(2);
        DecimalFormat.setMaximumFractionDigits(2);
        DecimalFormat.setRoundingMode(RoundingMode.HALF_UP);
        //PE (1,500.90)
        DecimalFormatSymbols PE_Symbols = new DecimalFormatSymbols(localDefault);
        PE_Symbols.setDecimalSeparator('.');
        PE_Symbols.setGroupingSeparator(',');
        DecimalFormat_PE.setDecimalFormatSymbols(PE_Symbols);
        DecimalFormat_PE.setGroupingSize(3);
        DecimalFormat_PE.setMinimumFractionDigits(2);
        DecimalFormat_PE.setMaximumFractionDigits(2);
        DecimalFormat_PE.setRoundingMode(RoundingMode.HALF_UP);
        DecimalFormat_PE.setCurrency(Currency.getInstance(localPE));
    }

    public static String FormateaDecimal(String valor, DecimalFormat formato, String moneda) {
        return moneda + " " + FormateaDecimal(ParseaDecimal(valor), formato);
    }

    public static String FormateaDecimal(String valor, DecimalFormat formato) {
        return FormateaDecimal(ParseaDecimal(valor), formato);
    }

    public static String FormateaDecimal(double valor, DecimalFormat formato, String moneda) {
        return moneda + " " + FormateaDecimal(valor, formato);
    }

    public static String FormateaDecimal(double valor, DecimalFormat formato) {

        //formato.applyPattern(pattern);
        PreparaFormatos();

        return formato.format(valor);
    }


    public static String ParseaFormateaDecimal(String valor, DecimalFormat formatoOrigen, DecimalFormat formatoDestino) {
        double doubleParseado = ParseaDecimal(valor, formatoOrigen);

        return FormateaDecimal(doubleParseado, formatoDestino);
    }

    public static String ParseaFormateaDecimal(String valor, DecimalFormat formatoOrigen, DecimalFormat formatoDestino, String moneda) {
        return moneda + " " + ParseaFormateaDecimal(valor, formatoOrigen, formatoDestino);
    }

    public static double ParseaDecimal(String valor, DecimalFormat formato) {
        try {
            PreparaFormatos();

            return formato.parse(valor).doubleValue();
        } catch (ParseException e) {
            e.printStackTrace();
            return 0.0;
        }
    }

    public static double ParseaDecimal(String valor) {
        return ParseaDecimal(valor, DecimalFormat_US);
    }

    public static boolean isNumeric(String s) {
        return s.matches("\\d+");
    }

    public static DecimalFormat getDecimalFormat(String codPais){
        if(codPais.equals("AR"))
            return Formatos.DecimalFormat ;
        else if(codPais.equals("PE"))
            return Formatos.DecimalFormat_PE ;
        else
            return Formatos.DecimalFormat_CH;
    }

    public static String getCurrencySymbol(String codPais){
        if(codPais.equals("PE"))
            return Formatos.CurrencySymbol_PE;
        else
            return Formatos.CurrencySymbol;
    }
}
