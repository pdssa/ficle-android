package com.pds.common.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;

import java.util.Map;
import java.util.Set;

/**
 * Created by Hernan on 16/12/2014.
 */
public class SharedPrefProvider extends ContentProvider {

    private String _sharedPrefName;


    public SharedPrefProvider(String sharedPrefName) {
        this._sharedPrefName = sharedPrefName;
    }

    private SharedPreferences getSharedPreferences() {
        return getContext().getSharedPreferences(_sharedPrefName, 0);
    }

    @Override
    public boolean onCreate() {
        //Context context = getContext();
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        Set<? extends Map.Entry<String, ?>> values = getSharedPreferences().getAll().entrySet();

        String[] _columns = new String[values.size()];
        String[] _values = new String[values.size()];

        int i = 0;
        for (Map.Entry<String, ?> entry : values) {

            _columns[i] = entry.getKey();

            Object val = entry.getValue();
            _values[i++] = val != null ? String.valueOf(val) : "";
        }

        MatrixCursor matrixCursor = new MatrixCursor(_columns);
        matrixCursor.addRow(_values);

        return matrixCursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SharedPreferences.Editor edit = getSharedPreferences().edit();

        edit.commit();

        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
