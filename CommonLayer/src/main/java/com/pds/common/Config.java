package com.pds.common;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.Build;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.inputmethod.InputMethodManager;

import com.pds.common.SII.vale.util.ValeConst;
import com.pds.common.SII.vale.util.ValeUtils;
import com.pds.common.pref.ValeConfig;

import org.apache.http.conn.util.InetAddressUtils;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Hernan on 06/12/13.
 */

public class Config {

    public String COMERCIO;
    public String DIRECCION;
    public String CLAVEFISCAL;
    private String _IMEI;
    public String IMSI;
    public String SIM_SERIALNUMBER;
    public String ANDROID_ID;
    public String WIFI_MAC;
    public String LAN_MAC;
    public String GOOGLE_ACCOUNT;
    public String PRINTER_MODEL;
    public String SCAN_MODEL;
    public String CLOUD_SERVER_HOST;
    public String ID_COMERCIO;
    public String RAZON_SOCIAL;
    public String RUBRO;
    public String LOCALIDAD;

    public boolean SYNC_LOGS;
    public boolean SYNC_CAJA;
    public boolean SYNC_STOCK;

    public String PERFIL;

    public int INACTIVITY_TIME;//minutes
    public String PAIS;
    public int SYNC_TIME; //minutes
    public int HIST_DAYS; //days

    public String LOCATION_LATITUDE;
    public String LOCATION_LONGITUDE;

    public String SIM_OPERATOR_CODE;
    public String SIM_OPERATOR_NAME;
    public String SIM_COUNTRY;
    public String SIM_STATE;
    public String SIM_VOICE_MAIL;
    public String SIM_VOICE_MAIL_NRO;
    public String SIM_PHONE_TYPE;
    public String SIM_NETWORK_COUNTRY;
    public String SIM_NETWORK_OPERATOR;
    public String SIM_NETWORK_OPERATOR_NAME;
    public String SIM_NETWORK_TYPE;
    public String SIM_LINE1_NRO;

    //*****SII - DATOS********
    public String SII_STATUS;
    public String SII_GIRO1;
    public String SII_GIRO2;
    public String SII_GIRO3;
    public String SII_RESOLUCION_VALE;
    public String SII_TERMINAL_ID;
    public String SII_SERVIDOR_IP;
    public String SII_SERVIDOR_PORT;
    public String SII_SERVIDOR_PLATFORM;
    public String SII_SERVIDOR_CONSULTA_QR;
    public String SII_MODALIDAD;
    public String SII_MERCHANT_ID;  //FIXME HZ, ver como llenar esto bien
    //************************

    public String IMEI() {
        if (TextUtils.isEmpty(_IMEI)) //si no llegamos a tener el IMEI disponible, retornamos en null para que no rompas las aplicaciones
            return "FFFFFFFFFF";
        //esto es para el emulador...sacar
        /*if (_IMEI.equals("000000000000000")) //si es 15 veces 0 => usamos un IMEI de test
            return "60026082";
        else*/
        return _IMEI;
    }

    public String getNET_ID() {
        //usamos el NET_ID para VALE, si no tengo IMEI, uso el SERIAL
        if (TextUtils.isEmpty(_IMEI))
            return ANDROID_ID;
        else
            return _IMEI;
    }

    private Context _context;

    public Config(Context context) {
        this(context, false);
    }

    public Config(Context context, boolean advancedConfig) {

        _context = context;

        InitializeProperties();

        //DbHelper db = new DbHelper(context);

        try {
            GetBasicConfig();

            if (advancedConfig)
                GetAdvancedConfig();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void InitializeProperties() {
        COMERCIO = "";
        DIRECCION = "";
        CLAVEFISCAL = "";
        _IMEI = "";
        IMSI = "";
        SIM_SERIALNUMBER = "";
        ANDROID_ID = "";
        WIFI_MAC = "";
        LAN_MAC = "";
        GOOGLE_ACCOUNT = "";
        PRINTER_MODEL = "";
        SCAN_MODEL = "";
        CLOUD_SERVER_HOST = "";
        SYNC_LOGS = true;
        SYNC_CAJA = true;
        SYNC_STOCK = true;

        PERFIL = "";

        RAZON_SOCIAL = "";
        RUBRO = "";
        LOCALIDAD = "";

        INACTIVITY_TIME = 60; //default 60'

        SYNC_TIME = 60; //default 60'

        HIST_DAYS = 60; //default 60 dias

        LOCATION_LATITUDE = "";
        LOCATION_LONGITUDE = "";
        ID_COMERCIO = "";
        SIM_OPERATOR_CODE = "";
        SIM_OPERATOR_NAME = "";
        SIM_COUNTRY = "";
        SIM_STATE = "";
        SIM_VOICE_MAIL = "";
        SIM_VOICE_MAIL_NRO = "";
        SIM_PHONE_TYPE = "";
        SIM_NETWORK_COUNTRY = "";
        SIM_NETWORK_OPERATOR = "";
        SIM_NETWORK_OPERATOR_NAME = "";
        SIM_NETWORK_TYPE = "";
        SIM_LINE1_NRO = "";

        //datos SII
        SII_STATUS = "";
        SII_GIRO1 = "";
        SII_GIRO2 = "";
        SII_GIRO3 = "";
        SII_RESOLUCION_VALE = "";
        SII_SERVIDOR_IP = "";
        SII_SERVIDOR_PORT = "";
        SII_TERMINAL_ID = "";
        SII_SERVIDOR_PLATFORM = "";
        SII_SERVIDOR_CONSULTA_QR = "";
        SII_MODALIDAD = "";
    }

    private void GetBasicConfig() throws Exception {

        Cursor c = _context.getContentResolver().query(
                Uri.parse("content://com.pds.ficle.ep.config.contentprovider/configs"),
                ConfigHelper.columnas,
                "",
                new String[]{},
                "");

        //Cursor c = db.query(ConfigHelper.CONFIG_SELECT_SCRIPT);

        // recorremos los items
        if (c.moveToFirst()) {
            do {

                this.COMERCIO = c.getString(ConfigHelper.CONFIG_IX_COMERCIO);
                //this.RECARGA_NRO_COMERCIO = c.getString(ConfigHelper.CONFIG_IX_REC_NRO_COMERCIO);
                //this.RECARGA_ID_TERMINAL = c.getString(ConfigHelper.CONFIG_IX_REC_ID_TERMINAL);
                this.DIRECCION = c.getString(ConfigHelper.CONFIG_IX_DIRECCION);
                this.CLAVEFISCAL = c.getString(ConfigHelper.CONFIG_IX_CLAVEFISCAL);
                this.PRINTER_MODEL = c.getString(ConfigHelper.CONFIG_IX_PRINTER_MODEL);
                String host = c.getString(ConfigHelper.CONFIG_IX_CLOUD_HOST);
                this.CLOUD_SERVER_HOST = TextUtils.isEmpty(host) ? "http://tas.valeelectronico.cl:7112/" : host;
                this.ID_COMERCIO = c.getString(ConfigHelper.CONFIG_IX_ID_COMERCIO);
                this.SCAN_MODEL = c.getString(ConfigHelper.CONFIG_IX_SCAN_MODEL);
                this.SYNC_LOGS = c.getInt(ConfigHelper.CONFIG_IX_SYNC_LOGS) == 1;
                this.SYNC_CAJA = c.getInt(ConfigHelper.CONFIG_IX_SYNC_CAJA) == 1;
                this.SYNC_STOCK = c.getInt(ConfigHelper.CONFIG_IX_SYNC_STOCK) == 1;
                String perfil = c.getString(ConfigHelper.CONFIG_IX_PERFIL);
                this.PERFIL = TextUtils.isEmpty(perfil) ? "BASICO" : perfil;
                this.INACTIVITY_TIME = c.getInt(ConfigHelper.CONFIG_IX_INACTIVITY_TIME);
                String pais = c.getString(ConfigHelper.CONFIG_IX_PAIS);
                this.PAIS = TextUtils.isEmpty(pais) ? "CH" : pais;
                this.SYNC_TIME = c.getInt(ConfigHelper.CONFIG_IX_SYNC_TIME);
                this.HIST_DAYS = c.getInt(ConfigHelper.CONFIG_IX_HIST_DIAS);

                this.SII_STATUS = replaceNull(c.getString(ConfigHelper.CONFIG_IX_SII_VALE_STATUS));
                this.SII_GIRO1 = replaceNull(c.getString(ConfigHelper.CONFIG_IX_SII_GIRO1));
                this.SII_GIRO2 = replaceNull(c.getString(ConfigHelper.CONFIG_IX_SII_GIRO2));
                this.SII_GIRO3 = replaceNull(c.getString(ConfigHelper.CONFIG_IX_SII_GIRO3));
                this.SII_RESOLUCION_VALE = replaceNull(c.getString(ConfigHelper.CONFIG_IX_SII_RES_VALE));
                this.SII_TERMINAL_ID = replaceNull(c.getString(ConfigHelper.CONFIG_IX_SII_VALE_TERMINAL_ID));
                this.SII_MERCHANT_ID = replaceNull(c.getString(ConfigHelper.CONFIG_IX_SII_VALE_MERCHANT_ID));
                this.SII_SERVIDOR_IP = replaceNull(c.getString(ConfigHelper.CONFIG_IX_SII_VALE_SERVER_IP));
                this.SII_SERVIDOR_PORT = replaceNull(c.getString(ConfigHelper.CONFIG_IX_SII_VALE_SERVER_PORT));
                this.SII_SERVIDOR_PLATFORM = replaceNull(c.getString(ConfigHelper.CONFIG_IX_SII_VALE_SERVER_PLATFORM));
                this.SII_SERVIDOR_CONSULTA_QR = replaceNull(c.getString(ConfigHelper.CONFIG_IX_SII_VALE_SERVER_CONSULTA_QR));
                this.SII_MODALIDAD = replaceNull(c.getString(ConfigHelper.CONFIG_IX_SII_VALE_MODALIDAD));

                this.RAZON_SOCIAL = replaceNull(c.getString(ConfigHelper.CONFIG_IX_RAZON_SOCIAL));
                this.RUBRO = replaceNull(c.getString(ConfigHelper.CONFIG_IX_RUBRO));
                this.LOCALIDAD = replaceNull(c.getString(ConfigHelper.CONFIG_IX_LOCALIDAD));

                TelephonyManager telephonyManager = ((TelephonyManager) _context.getSystemService(_context.TELEPHONY_SERVICE));

                this._IMEI = telephonyManager.getDeviceId();

                String imsi = telephonyManager.getSubscriberId();
                this.IMSI = imsi != null ? imsi : "";

                String simserialnumber = telephonyManager.getSimSerialNumber();
                this.SIM_SERIALNUMBER = simserialnumber != null ? simserialnumber : "";

                String androidId = Secure.getString(_context.getContentResolver(), Secure.ANDROID_ID);
                this.ANDROID_ID = androidId != null ? androidId : "";

                Location location = getLocation();
                if (location != null) {
                    LOCATION_LONGITUDE = String.valueOf(location.getLongitude());
                    LOCATION_LATITUDE = String.valueOf(location.getLatitude());
                }

            } while (c.moveToNext());
        }

        c.close();
    }

    private void GetAdvancedConfig() throws Exception {
        String googleAccount = getGoogleMainAccount();
        this.GOOGLE_ACCOUNT = googleAccount != null ? googleAccount : "";

        TelephonyManager telephonyManager = ((TelephonyManager) _context.getSystemService(_context.TELEPHONY_SERVICE));

        String simOperatorCode = telephonyManager.getSimOperator(); //*OPERATOR CODE*
        this.SIM_OPERATOR_CODE = simOperatorCode != null ? simOperatorCode : "";

        String simOperatorName = telephonyManager.getSimOperatorName(); //*OPERATOR NAME*
        this.SIM_OPERATOR_NAME = simOperatorName != null ? simOperatorName : "";

        String simCountry = telephonyManager.getSimCountryIso(); //*COUNTRY ISO*
        this.SIM_COUNTRY = simCountry != null ? simCountry : "";

        int simState = telephonyManager.getSimState(); //*STATE*
        switch (simState) {
            case TelephonyManager.SIM_STATE_READY:
                this.SIM_STATE = "READY";
                break;
            case TelephonyManager.SIM_STATE_ABSENT:
                this.SIM_STATE = "ABSENT";
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                this.SIM_STATE = "NETWORK_LOCKED";
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                this.SIM_STATE = "PIN_REQUIRED";
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                this.SIM_STATE = "PUK_REQUIRED";
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                this.SIM_STATE = "UNKNOWN";
                break;
            default:
                this.SIM_STATE = "";
                break;
        }

        String simVoiceMail = telephonyManager.getVoiceMailAlphaTag(); //*VOICE MAIL TEXT ID*
        this.SIM_VOICE_MAIL = simVoiceMail != null ? simVoiceMail : "";

        String simVoiceMailNro = telephonyManager.getVoiceMailNumber(); //*VOICE MAIL NUMBER*
        this.SIM_VOICE_MAIL_NRO = simVoiceMailNro != null ? simVoiceMailNro : "";

        int simSystemService = telephonyManager.getPhoneType(); //*PHONE TYPE*
        switch (simSystemService) {
            case TelephonyManager.PHONE_TYPE_CDMA:
                this.SIM_PHONE_TYPE = "CDMA";
                break;
            case TelephonyManager.PHONE_TYPE_GSM:
                this.SIM_PHONE_TYPE = "GSM";
                break;
            case TelephonyManager.PHONE_TYPE_SIP:
                this.SIM_PHONE_TYPE = "SIP";
                break;
            case TelephonyManager.PHONE_TYPE_NONE:
                this.SIM_PHONE_TYPE = "NONE";
                break;
            default:
                this.SIM_PHONE_TYPE = "";
                break;
        }

        String simNetworkCountry = telephonyManager.getNetworkCountryIso(); //*NETWORK COUNTRY ISO*
        this.SIM_NETWORK_COUNTRY = simNetworkCountry != null ? simNetworkCountry : "";

        String simNetworkOperator = telephonyManager.getNetworkOperator(); //*NETWORK OPERATOR CODE*
        this.SIM_NETWORK_OPERATOR = simNetworkOperator != null ? simNetworkOperator : "";

        String simNetworkOperatorName = telephonyManager.getNetworkOperatorName(); //*NETWORK OPERATOR NAME*
        this.SIM_NETWORK_OPERATOR_NAME = simNetworkOperatorName != null ? simNetworkOperatorName : "";

        int simNetworkType = telephonyManager.getNetworkType(); //*NETWORK TYPE*
        switch (simNetworkType) {
            case TelephonyManager.NETWORK_TYPE_1xRTT:
                this.SIM_NETWORK_TYPE = "1XRTT";
                break;
            case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                this.SIM_NETWORK_TYPE = "UNKNOWN";
                break;
            case TelephonyManager.NETWORK_TYPE_GPRS:
                this.SIM_NETWORK_TYPE = "GPRS";
                break;
            case TelephonyManager.NETWORK_TYPE_EDGE:
                this.SIM_NETWORK_TYPE = "EDGE";
                break;
            case TelephonyManager.NETWORK_TYPE_UMTS:
                this.SIM_NETWORK_TYPE = "UMTS";
                break;
            case TelephonyManager.NETWORK_TYPE_CDMA:
                this.SIM_NETWORK_TYPE = "CMA";
                break;
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
                this.SIM_NETWORK_TYPE = "EVDO_0";
                break;
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
                this.SIM_NETWORK_TYPE = "EVDO_A";
                break;
            case TelephonyManager.NETWORK_TYPE_HSDPA:
                this.SIM_NETWORK_TYPE = "HSDPA";
                break;
            case TelephonyManager.NETWORK_TYPE_HSUPA:
                this.SIM_NETWORK_TYPE = "HSUPA";
                break;
            case TelephonyManager.NETWORK_TYPE_HSPA:
                this.SIM_NETWORK_TYPE = "HSPA";
                break;
            case TelephonyManager.NETWORK_TYPE_IDEN:
                this.SIM_NETWORK_TYPE = "IDEN";
                break;
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
                this.SIM_NETWORK_TYPE = "EVDO_B";
                break;
            case TelephonyManager.NETWORK_TYPE_LTE:
                this.SIM_NETWORK_TYPE = "LTE";
                break;
            case TelephonyManager.NETWORK_TYPE_EHRPD:
                this.SIM_NETWORK_TYPE = "EHRPD";
                break;
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                this.SIM_NETWORK_TYPE = "HSPAP";
                break;
            default:
                this.SIM_NETWORK_TYPE = "";
                break;
        }

        String simLine1Nro = telephonyManager.getLine1Number(); //*PHONE NUMBER*
        this.SIM_LINE1_NRO = simLine1Nro != null ? simLine1Nro : "";

        WifiInfo wifiInf = ((WifiManager) _context.getSystemService(Context.WIFI_SERVICE)).getConnectionInfo();
        String wifiMac = wifiInf.getMacAddress();
        this.WIFI_MAC = wifiMac != null ? wifiMac : "";


        String lanMac = getMACAddress("eth0");
        this.LAN_MAC = lanMac != null ? lanMac : "";
    }

    public float GetBatteryLevel() {
        Intent batteryIntent = this._context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

        // What level does it have?
        int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        // Error checking that probably isn't needed but I added just in case.
        if (level == -1 || scale == -1) {
            return 50.0f;
        }

        return ((float) level / (float) scale) * 100.0f;
    }

    public String GetBatteryStatus() {
        Intent batteryIntent = this._context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

        // Are we charging / charged?
        int status = batteryIntent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        boolean isCharging = (status == BatteryManager.BATTERY_STATUS_CHARGING ||
                status == BatteryManager.BATTERY_STATUS_FULL);

        // How are we charging?
        int chargePlug = batteryIntent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        boolean usbCharge = (chargePlug == BatteryManager.BATTERY_PLUGGED_USB);
        boolean acCharge = (chargePlug == BatteryManager.BATTERY_PLUGGED_AC);

        String _status = "";

        if (isCharging)
            _status = "C";//charging /connected
        else
            _status = "X";//not charging / disconnected

        if (usbCharge)
            _status += "U";//USB
        else if (acCharge)
            _status += "A";//AC
        else
            _status += "N";//none

        return _status;
    }

    public String getAndroidVers() {
        try {
            return Build.VERSION.RELEASE;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

    public String getModelName() {
        try {
            String manufacturer = Build.MANUFACTURER;
            String model = Build.MODEL;

            if (model.startsWith(manufacturer)) {
                return capitalize(model);
            } else
                return capitalize(manufacturer) + " " + capitalize(model);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return "";
    }

    /**
     * Returns MAC address of the given interface name.
     *
     * @param interfaceName eth0, wlan0 or NULL=use first interface
     * @return mac address or empty string
     */
    public static String getMACAddress(String interfaceName) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                if (interfaceName != null) {
                    if (!intf.getName().equalsIgnoreCase(interfaceName)) continue;
                }
                byte[] mac = intf.getHardwareAddress();
                if (mac == null) return "";
                StringBuilder buf = new StringBuilder();
                for (int idx = 0; idx < mac.length; idx++)
                    buf.append(String.format("%02X:", mac[idx]));
                if (buf.length() > 0) buf.deleteCharAt(buf.length() - 1);

                String result = buf.toString();

                return (result != null ? result : "");
            }
        } catch (Exception ex) {
        } // for now eat exceptions
        return "";
        /*try {
            // this is so Linux hack
            return loadFileAsString("/sys/class/net/" +interfaceName + "/address").toUpperCase().trim();
        } catch (IOException ex) {
            return null;
        }*/
    }

    /**
     * Get IP address from first non-localhost interface
     *
     * @param useIPv4 true=return ipv4, false=return ipv6
     * @return address or empty string
     */
    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress().toUpperCase();
                        boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 port suffix
                                return delim < 0 ? sAddr : sAddr.substring(0, delim);
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
        } // for now eat exceptions
        return "";
    }

    private String getGoogleMainAccount() {

        try {

            //************* Get Registered Gmail Account *************
            //Account[] accounts = AccountManager.get(this._context).getAccountsByType("com.gmail");
            //com.google
            //com.gmail
            //com.android.email
            Account[] accounts = AccountManager.get(this._context).getAccounts();
            for (Account account : accounts) {
                return account.name;
            }
        } catch (Exception ex) {
        }

        return null;
    }

    /**
     * *
     * GPS Process
     * <p/>
     * First of all call listener of Location
     * then checking for GPS_PROVIDER
     * if not available then check for NETWORK_PROVIDER
     * and if its also not available then pass 0.00,0.00 to longitude and latitude
     * <p/>
     * ***
     */
    private Location getLocation() {

        /** PROCESS for Get Longitude and Latitude **/
        LocationManager locationManager = (LocationManager) _context.getSystemService(Context.LOCATION_SERVICE);

        List<String> providers = locationManager.getProviders(true);

        /* Loop over the array backwards, and if you get an accurate location, then break out the loop*/
        Location l = null;

        for (int i = providers.size() - 1; i >= 0; i--) {
            l = locationManager.getLastKnownLocation(providers.get(i));
            if (l != null) break;
        }
        return l;
        /*
        // getting GPS status
        boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        // check if GPS enabled
        if (isGPSEnabled) {

            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (location != null) {
                return location;
            } else {
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                if (location != null) {
                    return location;
                } else {
                    return null;
                }
            }
        }
        else{
            return null;
        }*/
    }

    public String getConnectivityType() {
        ConnectivityManager cm = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();

        if (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI) {
            return "LAN";
        } else if (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_MOBILE) {
            return "GPRS";
        } else
            return "";
    }


    private String replaceNull(String input) {
        return TextUtils.isEmpty(input) ? "" : input;
    }

    public String getValeModalidadLabel() {
        if (this.SII_MODALIDAD.equalsIgnoreCase("3"))
            return "SIMULACION";
        if (this.SII_MODALIDAD.equalsIgnoreCase("2"))
            return "ONLINE";
        if (this.SII_MODALIDAD.equalsIgnoreCase("1"))
            return "OFFLINE";

        return this.SII_MODALIDAD;
    }

    public boolean isValeModalidadDemo() {
        return this.SII_MODALIDAD.equalsIgnoreCase("3");
    }

    public boolean isValeHabilitado() {
        return
                this.SII_STATUS.equalsIgnoreCase(ValeConst.VALE_STATUS_HABILITADO) ||
                        this.SII_STATUS.equalsIgnoreCase(ValeConst.VALE_STATUS_INICIALIZADO);
    }

    public boolean isValeInicializado() {
        return this.SII_STATUS.equalsIgnoreCase(ValeConst.VALE_STATUS_INICIALIZADO);
    }


    private String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase.append(c);
        }
        return phrase.toString();
    }
}
