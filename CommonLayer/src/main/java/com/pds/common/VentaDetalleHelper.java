package com.pds.common;

/**
 * Created by Hernan on 22/11/13.
 */
public class VentaDetalleHelper {

    //************TABLE VENTA_DET************
    public static final String TABLE_NAME = "venta_det";
    //columns names
    public static final String VENTA_DET_ID = "_id";
    public static final String VENTA_DET_VENTA_ID = "id_venta";
    public static final String VENTA_DET_PRODUCTO_ID = "id_producto";
    public static final String VENTA_DET_CANTIDAD = "cantidad";
    public static final String VENTA_DET_PRECIO = "precio";
    public static final String VENTA_DET_TOTAL = "total";
    public static final String VENTA_DET_SKU = "sku";
    public static final String VENTA_DET_NETO = "neto";
    public static final String VENTA_DET_IVA = "iva";
    public static final String COLUMN_SYNC_STATUS = "sync";

    //columns indexes
    public static final int VENTA_DET_IX_ID = 0;
    public static final int VENTA_DET_IX_VENTA_ID = 1;
    public static final int VENTA_DET_IX_PRODUCTO_ID = 2;
    public static final int VENTA_DET_IX_CANTIDAD = 3;
    public static final int VENTA_DET_IX_PRECIO = 4;
    public static final int VENTA_DET_IX_TOTAL = 5;
    public static final int VENTA_DET_IX_SKU = 6;
    public static final int VENTA_DET_IX_NETO = 7;
    public static final int VENTA_DET_IX_IVA = 8;

    // script de creacion de tabla
    public static final String VENTA_DET_CREATE_SCRIPT = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
            VENTA_DET_ID + " integer primary key autoincrement, " +
            VENTA_DET_VENTA_ID  + " integer not null, " +
            VENTA_DET_PRODUCTO_ID + " integer not null, " +
            VENTA_DET_CANTIDAD + " integer, " +
            VENTA_DET_PRECIO + " real, " +
            VENTA_DET_TOTAL + " real, " +
            VENTA_DET_SKU + " text, " +
            VENTA_DET_NETO + " real, " +
            VENTA_DET_IVA + " real," +
            COLUMN_SYNC_STATUS + " text default 'N' " +
            ");";

    //script de alter
    public static final String VENTA_DET_ALTER_SYNC_STATUS_SCRIPT = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + COLUMN_SYNC_STATUS + " text default 'N'" ;
    public static final String VENTA_DET_ALTER_SYNC_STATUS_SCRIPT_2 = "UPDATE " + TABLE_NAME + " SET " + COLUMN_SYNC_STATUS + " = 'N'" ;

    public static final String VENTA_DET_ALTER_SKU_SCRIPT = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + VENTA_DET_SKU + " text " ;

    public static final String VENTA_DET_ALTER_NETO_SCRIPT = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + VENTA_DET_NETO + " real " ;
    public static final String VENTA_DET_ALTER_IVA_SCRIPT = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + VENTA_DET_IVA + " real " ;

    public static final String[] columnas = new String[]{
            VENTA_DET_ID,
            VENTA_DET_VENTA_ID,
            VENTA_DET_PRODUCTO_ID,
            VENTA_DET_CANTIDAD,
            VENTA_DET_PRECIO,
            VENTA_DET_TOTAL,
            VENTA_DET_SKU,
            VENTA_DET_NETO,
            VENTA_DET_IVA,
            COLUMN_SYNC_STATUS
    };
}
