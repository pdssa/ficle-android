package com.pds.common.util;

/**
 * Created by Hernan on 03/12/2014.
 */
public abstract class Perfiles {
    public static String PERFIL_BASICO = "BASICO";
    public static String PERFIL_JJD = "PILOTO JJD";
    public static String PERFIL_PILOTO_CHILE = "PILOTO CHILE";
    public static String PERFIL_FACTURA_MOVIL = "DEMO FACTURAMOVIL";
    public static String PERFIL_PILOTO_PYA = "PILOTO PYA";
    public static String PERFIL_PILOTO_ARG = "PILOTO ARG";
    public static String PERFIL_DEMO_POWAPOS = "DEMO POWAPOS";
    public static String PERFIL_IQCORP = "IQCORP TEST";
    public static String PERFIL_VALE_ELECTRONICO = "VALE ELECTRONICO";
    public static String PERFIL_DEMO_FULL_CH = "DEMO_FULL_CH";
    public static String PERFIL_PILOTO_EMBONOR = "PILOTO EMBONOR";
    public static String PERFIL_DEMO_CCU = "DEMO_CCU";
    public static String PERFIL_DEMO_AL_GRAMO = "DEMO_AL_GRAMO";
    public static String PERFIL_DEMO_ALMACENES_CHILE = "DEMO_ALMACENES_CHILE";

    /*public boolean esAR(String perfil){
        return perfil.equals(PERFIL_PILOTO_ARG) || perfil.equals(PERFIL_PILOTO_PYA);
    }
    public boolean esCH(String perfil){
        return perfil.equals(PERFIL_PILOTO_CHILE) || perfil.equals(PERFIL_JJD) || perfil.equals(PERFIL_FACTURA_MOVIL) || perfil.equals(PERFIL_BASICO);
    }*/
}
