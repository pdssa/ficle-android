package com.pds.common.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.EAN13Writer;
import com.google.zxing.oned.EAN8Writer;

import java.util.Hashtable;

import android_serialport_api.Printer;

/**
 * Created by Hernan on 17/02/2016.
 */
public class ImprimirEtiquetaTask extends AsyncTask<Void, Void, Boolean> {
    private ProgressDialog dialog;
    private Context context;

    private String PRINTER_MODEL, sku, nombre, precio, precioSecund;
    private boolean incluirPrecio;

    public ImprimirEtiquetaTask(Context _context, String _PRINTER_MODEL, String _sku, String _nombre, String _precio, boolean _incluirPrecio, String _precioSecund){
        context = _context;
        PRINTER_MODEL = _PRINTER_MODEL;
        sku = _sku;
        nombre = _nombre;
        precio = _precio;
        incluirPrecio = _incluirPrecio;
        precioSecund = _precioSecund;
    }

    @Override
    protected Boolean doInBackground(Void... params) {

        printLabel(context,PRINTER_MODEL, sku, nombre, precio, incluirPrecio, precioSecund );

        return true;
    }

    public static void printLabel(final Context _context, String _PRINTER_MODEL, final String _sku, final String _nombre, final String _precio,final boolean _incluirPrecio,final String _precioSecund){
        try {

            final Printer _printer = Printer.getInstance(_PRINTER_MODEL, _context);

            _printer.delayActivated = true;
            _printer.printerCallback = new Printer.PrinterCallback() {
                @Override
                public void onPrinterReady() {

                    final int WHITE = 0xFFFFFFFF;
                    final int BLACK = 0xFF000000;

                    _printer.sendSeparadorHorizontalMasLineFeed();


                    if (_incluirPrecio) {
                        _printer.format(1, 2, Printer.ALINEACION.CENTER);
                        _printer.printLine(_precio);

                    }

                    _printer.cancelCurrentFormat();

                    if (_nombre.length() > _printer.get_longitudLinea())
                        _printer.print(_nombre.substring(0, _printer.get_longitudLinea()));
                    else
                        _printer.printLine(_nombre);

                    if (_incluirPrecio && !TextUtils.isEmpty(_precioSecund)) {
                        _printer.printLine(_precioSecund);
                    }

                    _printer.lineFeed();

                    //EAN13
                    try {

                        String SKU = Sku_Utils.FormatSkuLabel(_sku);

                        String _SKU = SKU.replace(" ", "").trim();

                        //"1111111111116";
                        BitMatrix result = null;

                        int len = _SKU.length();

                        if (len > 8) {

                            SKU = " " + SKU;

                            EAN13Writer barcode = new EAN13Writer();
                            Hashtable hintsTable = new Hashtable();
                            hintsTable.put(EncodeHintType.MARGIN, 0);

                            result = barcode.encode(_SKU, BarcodeFormat.EAN_13, 350, 75, hintsTable);

                        } else {

                            SKU = "        " + SKU;

                            EAN8Writer barcode = new EAN8Writer();
                            Hashtable hintsTable = new Hashtable();
                            hintsTable.put(EncodeHintType.MARGIN, 0);

                            result = barcode.encode(_SKU, BarcodeFormat.EAN_8, 350, 75, hintsTable);
                        }

                        int width = result.getWidth();
                        int height = result.getHeight();
                        int[] pixels = new int[width * height];
                        for (int y = 0; y < height; y++) {
                            int offset = y * width;
                            for (int x = 0; x < width; x++) {
                                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
                            }
                        }

                        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);

                        _printer.printBitmap(bitmap);

                        _printer.printLine(SKU);

                    } catch (Exception ex) {
                        Log.e("EANWriter", ex.getMessage());
                    }

                    _printer.sendSeparadorHorizontal();

                    _printer.footer();
                }
            };

            //iniciamos la printer
            if (!_printer.initialize()) {
                Toast.makeText(_context, "Error al inicializar", Toast.LENGTH_SHORT).show();

                _printer.end();
            }

        } catch (Exception ex) {
            Log.e("printCode", ex.getMessage());
        }
    }

    @Override
    protected void onPostExecute(Boolean result) {
        if(dialog.isShowing())
            dialog.hide();
    }

    @Override
    protected void onPreExecute() {
        dialog = new ProgressDialog(context);
        dialog.setMessage("Imprimiendo etiqueta, aguarde por favor...");
        dialog.setIndeterminate(false);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    @Override
    protected void onProgressUpdate(Void... values) {}
}

