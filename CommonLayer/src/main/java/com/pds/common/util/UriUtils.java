package com.pds.common.util;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;

/**
 * Created by Hernan on 07/06/2016.
 */
public abstract class UriUtils {

    private static final String ANDROID_RESOURCE = "android.resource://";
    private static final String FORESLASH = "/";

    public static Uri resolveUri(Context context, String uri) {

        if (TextUtils.isEmpty(uri))
            return null;

        if (uri.startsWith(ANDROID_RESOURCE)) {
            //es un recurso local del package => ubicamos el id con el nombre

            //obtenemos el path de resources:
            //EJEMPLO: android.resource://your.package.here/drawable/image_name
            String[] components = uri.split("/");
            //debiera quedar: drawable/image_name
            String folder = components[components.length - 2];
            String resName = components[components.length - 1];
            //quitamos la extension, si la tuviera (image_name.png --> image_name)
            if (resName.contains(".")) {
                resName = resName.substring(0, resName.lastIndexOf("."));
            }
            String path = folder + FORESLASH + resName;
            //obtenemos el id del resource
            int resId = context.getResources().getIdentifier(path, null, context.getPackageName());

            //armamos el uri al resource del package
            //EJEMPLO: android.resource://your.package.here/drawable/2225
            return Uri.parse(ANDROID_RESOURCE + context.getPackageName() + FORESLASH + folder + FORESLASH + resId);
        } else {
            //lo interpretamos como un uri directo
            return Uri.parse(uri);
        }
    }
}
