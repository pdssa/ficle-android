package com.pds.common.util;

import android.content.ContentProvider;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.util.Log;

/**
 * Created by Hernan on 17/02/2015.
 */
public abstract class ContentProviders {

    private static final String TAG = ContentProvider.class.getSimpleName();

    public static boolean ProviderIsAvailable(Context _context, String cpTarget) {
        //buscamos si el content provider existe entre los disponibles en el sistema

        Log.i(TAG, "ProviderIsAvailable: TARGET IS "+cpTarget);
        for (PackageInfo pack : _context.getPackageManager().getInstalledPackages(PackageManager.GET_PROVIDERS)) {
            Log.i(TAG, "ProviderIsAvailable: PACKAGE "+pack);
            ProviderInfo[] providers = pack.providers;
            if (providers != null) {
                for (ProviderInfo provider : providers) {
                    Log.i(TAG, "ProviderIsAvailable: PROVIDER: "+provider);
                    if (provider.authority != null)
                    {
                        Log.i(TAG, "ProviderIsAvailable: PROVIDER AUTHORITY: "+provider.authority+" vs "+cpTarget);
                        if (provider.authority.equalsIgnoreCase(cpTarget))
                        {
                            Log.i(TAG, "ProviderIsAvailable: FOUND!");
                            return true;
                        }

                    }
                    else
                        {
                            Log.i(TAG, "ProviderIsAvailable: PROVIDER is null");
                        }
                }
            }
            else
            {
                Log.i(TAG, "ProviderIsAvailable: Providers NULL, next....");
            }
        }


        Log.i(TAG, "ProviderIsAvailable: NOT FOUND!");
        return false;
    }
}
