package com.pds.common.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Hernan on 04/12/2014.
 */
public abstract class ConnectivityUtils {

    public static final String SIN_CONEXION = "No hay una conexion disponible";
    public static final String SIN_CONEXION_UP = "SIN CONEXION";
    public static final String SIN_RESPUESTA = "NO FUE POSIBLE COMUNICARSE CON EL SERVIDOR";

    //tiene conectividad?
    public static boolean isOnline(Context _context) {
        ConnectivityManager connMgr = (ConnectivityManager)
                _context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        return (networkInfo != null && networkInfo.isConnected());

        //si queres saber que tipo de conexion está activa
        //NetworkInfo networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        //boolean isWifiConn = networkInfo.isConnected();
        //networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        //boolean isMobileConn = networkInfo.isConnected();
    }

}
