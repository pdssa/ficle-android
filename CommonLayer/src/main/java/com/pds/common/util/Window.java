package com.pds.common.util;

import android.app.Activity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import java.util.concurrent.Callable;

/**
 * Created by Hernan on 29/01/2016.
 */
public abstract class Window {
    public static void CloseSoftKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void FocusViewShowSoftKeyboard(Activity activity, View view) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);

        if(view != null) {
            //Set focus to the view
            view.requestFocus();
            //Force show softkeyboard
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
        else{
            //show forced
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }
    }

    public static void FocusViewNoSoftKeyboard(Activity activity, View view) {

        if(view != null) {
            //Set focus to the view
            view.requestFocus();
        }

        CloseSoftKeyboard(activity);
    }

    public static void AddDoneKeyboardAction(EditText view, final Callable<Void> action) {
        if(view != null) {
            view.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT) {
                        try {
                            action.call();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        return true;//keep the keyboard up
                    }
                    return false;
                }
            });
        }
    }

    public static void AddEnterKeyboardAction(EditText view, final Callable<Void> action) {
        if(view != null) {
            view.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                    if(event == null) return false;

                    if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                        try {
                            action.call();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        return true;//keep the keyboard up
                    }
                    return false;
                }
            });
        }
    }
}
