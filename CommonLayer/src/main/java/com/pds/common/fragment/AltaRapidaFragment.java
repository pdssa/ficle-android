/*
package com.pds.common.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.pds.common.Formatos;
import com.pds.common.R;
import com.pds.common.dao.DepartmentDao;
import com.pds.common.dao.ProductDao;
import com.pds.common.dao.SubDepartmentDao;
import com.pds.common.dao.TaxDao;
import com.pds.common.db.DepartmentTable;
import com.pds.common.db.SubdepartmentTable;
import com.pds.common.model.Department;
import com.pds.common.model.Product;
import com.pds.common.model.SubDepartment;
import com.pds.common.model.Tax;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


public class AltaRapidaFragment extends Fragment {

    private List<Product> candidatos;
    private ListView listView;
    private AltaRapidaAdapter adapter;
    private int fragment_id;
    private DecimalFormat FORMATO;

    private static final String ARG_PARAM1 = "candidatos";
    private static final String ARG_PARAM2 = "fragment_id";
    private static final String ARG_PARAM3 = "formato";

    public static AltaRapidaFragment newInstance(ArrayList<Product> _candidatos, int _fragment_id, DecimalFormat _formato) {
        AltaRapidaFragment fragment = new AltaRapidaFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM1, _candidatos);
        args.putInt(ARG_PARAM2, _fragment_id);
        args.putSerializable(ARG_PARAM3, _formato);
        fragment.setArguments(args);
        return fragment;
    }

    public AltaRapidaFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            //tomamos los productos candidatos a ser dados de alta
            candidatos = getArguments().getParcelableArrayList(ARG_PARAM1);
            fragment_id = getArguments().getInt(ARG_PARAM2);
            FORMATO = (DecimalFormat) getArguments().getSerializable(ARG_PARAM3);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_alta_rapida, container, false);

        view.findViewById(R.id.btnCancelar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        view.findViewById(R.id.btnCrear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {

                    boolean error = false;

                    //validamos que todos los seleccionados tengan nombre, depto y subdepto
                    //quitamos obligatoriedad para agilidad
                    */
/*for (Product p : ((AltaRapidaAdapter) listView.getAdapter()).items) {
                        //recorremos los productos a dar de alta
                        if (p.getAutomatic() == 1) {
                            String nombre = p.getName();
                            long idDepto = p.getIdDepartment();
                            long idSubDepto = p.getIdSubdepartment();

                            if (TextUtils.isEmpty(nombre)) {
                                error = true;
                                throw new Exception("Debe asignar un nombre para el producto " + p.getCode());
                            }
                            if (idDepto == -1) {
                                error = true;
                                throw new Exception("Debe asociar un departamento al producto " + p.getCode());
                            }
                            if (idSubDepto == -1) {
                                error = true;
                                throw new Exception("Debe asociar un subdepartamento al producto " + p.getCode());
                            }
                        }
                    }*//*



                    if (!error) {
                        int q = 0;
                        ProductDao productDao = null;

                        for (Product p : ((AltaRapidaAdapter) listView.getAdapter()).items) {
                            //recorremos los productos a dar de alta
                            //if (p.getAutomatic() == 2) {

                                if (productDao == null) {
                                    productDao = new ProductDao(getActivity().getContentResolver());
                                }

                                //seteamos valores por default previo a grabar
                                p.setIdProvider(-1);
                                p.setWeighable(false);
                                p.setMinStock(0);
                                p.setPurchasePrice(0);
                                p.setStock(0);
                                p.setAutomatic(Product.TYPE_AUTOM.PROD_ALTA_MANUAL);

                                if (!productDao.saveOrUpdate(p))
                                    throw new Exception("Error al grabar");

                                q++;
                            //}
                        }
                        if (q > 0) {
                            Toast.makeText(getActivity(), String.format("%d productos dados de alta", q), Toast.LENGTH_SHORT).show();

                            finish();

                        }
                    }

                } catch (Exception ex) {
                    Toast.makeText(getActivity(), "Se ha producido un error al dar de alta alguno de los productos: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
                }


            }
        });

        listView = (ListView) view.findViewById(R.id.lstProductos);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);

        listView.setItemsCanFocus(true);
        adapter = new AltaRapidaAdapter(getActivity(), candidatos);
        listView.setAdapter(adapter);
    }

    private void finish() {
        FragmentManager fm = getActivity().getFragmentManager();
        Fragment fragment = fm.findFragmentById(fragment_id);
        if (fragment != null) {
            //cerramos el fragment
            fm.beginTransaction()
                    .remove(fragment)
                    .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                    .commit();
        }
    }


    class AltaRapidaAdapter extends BaseAdapter {
        private LayoutInflater inflater;
        public List<Product> items = new ArrayList<Product>();
        private Context context;
        private List<Department> departmentList;

        public AltaRapidaAdapter(Context _context, List<Product> _items) {
            inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            items = _items;
            context = _context;
            departmentList = getDepartmentList();
        }

        public int getCount() {
            return items.size();
        }

        public Object getItem(int position) {
            return items.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        private int lookUpDepartmentById(long id, ArrayAdapter<Department> departmentArrayAdapter) {
            for (int i = 0; i < departmentArrayAdapter.getCount(); i++) {
                if(departmentArrayAdapter.getItem(i).getId() == id)
                    return i;
            }

            return 0;
        }

        private int lookUpSubDepartmentById(long id, ArrayAdapter<SubDepartment> subdepartmentArrayAdapter) {
            for (int i = 0; i < subdepartmentArrayAdapter.getCount(); i++) {
                if(subdepartmentArrayAdapter.getItem(i).getId() == id)
                    return i;
            }

            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.alta_rapida_item, null);
                holder.selected = (CheckBox) convertView.findViewById(R.id.alta_chk_selected);
                holder.selected.setChecked(true);
                holder.nombre = (EditText) convertView.findViewById(R.id.alta_txt_nombre);
                holder.codigo = (EditText) convertView.findViewById(R.id.alta_txt_codigo);
                holder.precio = (EditText) convertView.findViewById(R.id.alta_txt_precio);
                holder.deptos = (Spinner) convertView.findViewById(R.id.alta_spn_depto);
                holder.subdeptos = (Spinner) convertView.findViewById(R.id.alta_spn_subdepto);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }


            final Product p = items.get(position);

            //Fill controls with the value you have in data source
            holder.codigo.setText(items.get(position).getCode());
            holder.precio.setText(Formatos.FormateaDecimal(items.get(position).getSalePrice(), FORMATO, "$"));
            holder.nombre.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    p.setName(s.toString());
                }
            });
            holder.selected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    //p.setAutomatic(isChecked ? 2 : 0);
                    p.setAutomatic(Product.TYPE_AUTOM.PROD_ALTA_MANUAL);
                }
            });

            ArrayAdapter<Department> departmentArrayAdapter = new ArrayAdapter<Department>(context, android.R.layout.simple_spinner_item, departmentList);
            departmentArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            holder.deptos.setAdapter(departmentArrayAdapter);
            holder.deptos.setSelection(lookUpDepartmentById(p.getIdDepartment(), departmentArrayAdapter));

            ArrayAdapter<SubDepartment> subdepartmentArrayAdapter = new ArrayAdapter<SubDepartment>(context, android.R.layout.simple_spinner_item, getSubDepartmentList(((Department) holder.deptos.getSelectedItem()).getId()));
            subdepartmentArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            holder.subdeptos.setAdapter(subdepartmentArrayAdapter);
            holder.subdeptos.setSelection(lookUpSubDepartmentById(p.getIdSubdepartment(), subdepartmentArrayAdapter));

            final Spinner departmentSpinner = holder.deptos;
            final Spinner subdepartmentSpinner = holder.subdeptos;
            final ArrayAdapter<SubDepartment> _subdepartmentArrayAdapter = subdepartmentArrayAdapter;

            holder.deptos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Department d = (Department) departmentSpinner.getItemAtPosition(position);

                    p.setIdDepartment(d.getId());
                    p.setDepartment(d);

                    //traemos los subdepartamentos del departamento seleccionado
                    _subdepartmentArrayAdapter.clear();
                    _subdepartmentArrayAdapter.addAll(getSubDepartmentList(d.getId()));
                    _subdepartmentArrayAdapter.notifyDataSetChanged();

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

            holder.subdeptos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    SubDepartment s = (SubDepartment) subdepartmentSpinner.getItemAtPosition(position);

                    p.setIdSubdepartment(s.getId());
                    p.setSubDepartment(s);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

            //holder.codigo.setId(position);

            //we need to update adapter once we finish with editing
            */
/*holder.caption.setOnFocusChangeListener(new OnFocusChangeListener() {
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus){
                        final int position = v.getId();
                        final EditText Caption = (EditText) v;
                        myItems.get(position).caption = Caption.getText().toString();
                    }
                }
            });*//*


            return convertView;
        }

        private List<Department> getDepartmentList() {
            List<Department> lst = (new DepartmentDao(context.getContentResolver())).list("generic = 0", null, DepartmentTable.COLUMN_NAME);
            lst.add(0, new Department(-1, "Seleccione..."));
            return lst;
        }

        private List<SubDepartment> getSubDepartmentList(long idDepartment) {
            List<SubDepartment> lst = new SubDepartmentDao(context.getContentResolver()).list(String.format("department_id = %d", idDepartment), null, SubdepartmentTable.COLUMN_NAME);
            lst.add(0, new SubDepartment(-1, "Seleccione..."));
            return lst;
        }
    }

    class ViewHolder {
        CheckBox selected;
        EditText nombre;
        EditText precio;
        EditText codigo;
        Spinner deptos;
        Spinner subdeptos;
    }


}
*/
