package com.pds.common.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Formatos;
import com.pds.common.R;
import com.pds.common.dao.DepartmentDao;
import com.pds.common.dao.ProductDao;
import com.pds.common.dao.SubDepartmentDao;
import com.pds.common.db.DepartmentTable;
import com.pds.common.db.ProductTable;
import com.pds.common.db.SubdepartmentTable;
import com.pds.common.model.Department;
import com.pds.common.model.Product;
import com.pds.common.model.SubDepartment;
import com.pds.common.util.UriUtils;
import com.pds.common.util.Window;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class ProductosFragment extends BaseProductosFragment {

    private Timer mTimer = null;
    private boolean mContinueSearching = true;

    ListView lstDepartamentos;
    ListView lstSubdepartamentos;
    ListView lstProductos;
    GridView grdProductos;
//    ListView lstCombos;

    GenericFragmentListener frg_listener;

    private static final String ARG_PARAM1 = "compor_vtas";
    private static final String ARG_PARAM2 = "prods";
    private static final String ARG_PARAM3 = "formato";
    private static final String ARG_PARAM4 = "productos_sin_precio";
    private static final String ARG_PARAM5 = "moneda";
    private static final String ARG_PARAM6 = "solo_productos_sin_codigo";
    private static final String ARG_PARAM7 = "combos_habilitados";
    private static final String ARG_PARAM8 = "control_externo_lisview";
    private static final String ARG_PARAM9 = "solo_productos_propios";
    private static final String ARG_PARAM10 = "solo_productos_catalogo";
    private static final String ARG_PARAM11 = "solo_combos";

    boolean comportamientoVenta, mostrarProdSinPrecio, mostrarSoloProdSinCod, combosHabilitados, controlExternoListView;
    boolean mostrarSoloProductosPropios, mostrarSoloProductosCatalogo;
    private boolean SOLO_COMBOS;

    private Long MAS_VENDIDOS_ID = Long.parseLong("-2");
    private Long COMBOS_ID = Long.parseLong("-3");
    private Department MAS_VENDIDOS;
    private Department COMBOS;
    private TextView lblDepartamentos;
    private View vwDeptoHeader;
    private TextView lblSubdepartamentos;
    private View vwSubdeptoHeader;

    //private List<Long> productosIdsSelected ;
    private DecimalFormat formato;
    private String moneda;
    private HashMap<Integer, Integer> productosSelected;
    private View vwProductHeader;
    private TextView lblProductos;

    private boolean esVistaGrilla;

    private MODO_PESABLE modoActivo;

    private enum MODO_PESABLE {
        PRODUCTO,
        SUBDEPARTMENTO_GENERICO,
        DEPARTAMENTO_GENERICO
    }

    public ListView getLstProductos() {
        return lstProductos;
    }


    //este metodo se usa para continuar con las funciones de seleccion de producto luego de haber
    //ingresado el peso en una aplicacion para un producto pesable
    public void continueAfterWeightableProduct() {
        finalizarVistaProductoPesable();
    }

    public void decreaseProduct(Product p) {
        if (comportamientoVenta) {
            if (productosSelected.containsKey((int) p.getId())) {

                int cant = productosSelected.get((int) p.getId()) - 1;
                productosSelected.put((int) p.getId(), cant);

                if (cant == 0)
                    productosSelected.remove((int) p.getId());

                ((ArrayAdapter) lstProductos.getAdapter()).notifyDataSetChanged();
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();

        mContinueSearching = true;
    }

    public void setListener(GenericFragmentListener listener) {
        this.frg_listener = listener;
    }

    /*public static ProductosFragment newInstance(boolean _comportamientoVentas) {
        ProductosFragment fragment = new ProductosFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_PARAM1, _comportamientoVentas);
        args.putSerializable(ARG_PARAM2, new HashMap<Integer, Integer>());
        args.putSerializable(ARG_PARAM3, Formatos.DecimalFormat_US);
        args.putBoolean(ARG_PARAM4, true);
        fragment.setArguments(args);
        return fragment;
    }*/

    //long[] _productosIdsSelected,
    public static ProductosFragment newInstance(boolean _comportamientoVentas, HashMap<Integer, Integer> _productosSelected, DecimalFormat _formato, String _moneda, boolean _combosHabilitados, boolean _externalListControl, boolean _productosSinPrecio) {
        ProductosFragment fragment = new ProductosFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_PARAM1, _comportamientoVentas);
        //args.putLongArray(ARG_PARAM2, _productosIdsSelected);
        args.putSerializable(ARG_PARAM2, _productosSelected);
        args.putSerializable(ARG_PARAM3, _formato);
        args.putBoolean(ARG_PARAM4, _productosSinPrecio);
        args.putString(ARG_PARAM5, _moneda);
        args.putBoolean(ARG_PARAM6, false);
        args.putBoolean(ARG_PARAM7, _combosHabilitados);
        args.putBoolean(ARG_PARAM8, _externalListControl);
        args.putBoolean(ARG_PARAM9, false);
        args.putBoolean(ARG_PARAM10, false);
        args.putBoolean(ARG_PARAM11, false);
        fragment.setArguments(args);
        return fragment;
    }

    public static BaseProductosFragment newInstance(boolean _comportamientoVentas, HashMap<Integer, Integer> _productosSelected, DecimalFormat _formato, boolean _productosSinPrecio, String _moneda, boolean _soloProductosSinCodigo,
                                                    boolean _solo_productos_propios, boolean _solo_productos_catalogo, boolean _solo_combos) {
        Bundle args = new Bundle();
        args.putBoolean(ARG_PARAM1, _comportamientoVentas);
        //args.putLongArray(ARG_PARAM2, _productosIdsSelected);
        args.putSerializable(ARG_PARAM2, _productosSelected);
        args.putSerializable(ARG_PARAM3, _formato);
        args.putBoolean(ARG_PARAM4, _productosSinPrecio);
        args.putString(ARG_PARAM5, _moneda);
        args.putBoolean(ARG_PARAM6, _soloProductosSinCodigo);
        args.putBoolean(ARG_PARAM7, true);
        args.putBoolean(ARG_PARAM8, false);
        args.putBoolean(ARG_PARAM9, _solo_productos_propios);
        args.putBoolean(ARG_PARAM10, _solo_productos_catalogo);
        args.putBoolean(ARG_PARAM11, _solo_combos);

        if (_solo_productos_propios) {
            ProductosPropiosFragment fragment = new ProductosPropiosFragment();
            fragment.setArguments(args);
            return fragment;
        } else {
            ProductosFragment fragment = new ProductosFragment();
            fragment.setArguments(args);
            return fragment;
        }

    }

    public ProductosFragment() {
        // Required empty public constructor
        esVistaGrilla = false;
    }

    public void RefreshList(boolean _productosSinPrecio) {
        mostrarProdSinPrecio = _productosSinPrecio;

        //recargamos la lista
        onActivityCreated(new Bundle());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            comportamientoVenta = getArguments().getBoolean(ARG_PARAM1);
            productosSelected = (HashMap<Integer, Integer>) getArguments().getSerializable(ARG_PARAM2);
            formato = (DecimalFormat) getArguments().getSerializable(ARG_PARAM3);
            mostrarProdSinPrecio = getArguments().getBoolean(ARG_PARAM4);
            moneda = getArguments().getString(ARG_PARAM5);
            mostrarSoloProdSinCod = getArguments().getBoolean(ARG_PARAM6);
            combosHabilitados = getArguments().getBoolean(ARG_PARAM7);
            controlExternoListView = getArguments().getBoolean(ARG_PARAM8);
            mostrarSoloProductosPropios = getArguments().getBoolean(ARG_PARAM9);
            mostrarSoloProductosCatalogo = getArguments().getBoolean(ARG_PARAM10);
            SOLO_COMBOS = getArguments().getBoolean(ARG_PARAM11);
            /*long[] ids = getArguments().getLongArray(ARG_PARAM2);
            productosIdsSelected = new ArrayList<Long>();
            for(long id : ids){
                productosIdsSelected.add(id);
            }*/

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (comportamientoVenta) {
            return inflater.inflate(R.layout.fragment_productos_ext, container, false);
        } else
            return inflater.inflate(R.layout.fragment_productos, container, false);
    }


    private EndlessScrollListener endlessScrollListener;

    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);

        mTimer = new Timer();

        lstDepartamentos = (ListView) getView().findViewById(R.id.lstDepartamentos);
        lstSubdepartamentos = (ListView) getView().findViewById(R.id.lstSubdepartamentos);
        lstProductos = (ListView) getView().findViewById(R.id.lstProductos);
//        lstCombos = (ListView) getView().findViewById(R.id.lstCombos);
        grdProductos = (GridView) getView().findViewById(R.id.grdProductos);
        lblDepartamentos = (TextView) getView().findViewById(R.id.lblListDeptoHeader);
        lblSubdepartamentos = (TextView) getView().findViewById(R.id.lblListSubdeptoHeader);
        lblProductos = (TextView) getView().findViewById(R.id.lblListProductHeader);

        View btnBack = getView().findViewById(R.id.frag_prod_ext_search_btn_back);
        if (btnBack != null) {
            btnBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onFragmentProductoClose();
                }
            });
        }

        final EditText txtFilter = (EditText) getView().findViewById(R.id.frag_prod_ext_search_txt_criteria);

        if(txtFilter != null)
        {
            ///FIXME
            ///HZ: Nueva busqueda al tocar
            txtFilter.setFilters(new InputFilter[]{new InputFilter() {
                @Override
                public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                    for (int i = start; i < end; i++)
                        if (!Character.isLetter(source.charAt(i)) &&
                                !Character.isSpaceChar(source.charAt(i)) &&
                                !Character.isDigit(source.charAt(i))
                                ) //solo aceptamos: letras, numeros y espacio
                            return "";

                    return null;
                }
            }});

            txtFilter.addTextChangedListener(new TextWatcher() {

                private final long DELAY = 500; // in ms

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    mTimer.cancel();
                    if (mContinueSearching) {
                        mTimer = new Timer();
                        if (txtFilter != null) {
                            final String filter = txtFilter.getText().toString().trim();


                            final Editable _s = s;

                            if(s.length() == 0)
                            {
                                lstDepartamentos.setVisibility(View.VISIBLE);
                                lstSubdepartamentos.setVisibility(View.VISIBLE);
                                lstProductos.setAdapter(new ProductArrayAdapter(getActivity(), new ArrayList<Product>()));
                            }
                            else if (s.length() > 2) {
                                ((ImageView) getView().findViewById(R.id.frag_prod_ext_search_btn_find)).setImageResource(R.drawable.ic_action_cancel);
                                mTimer.schedule(new TimerTask() {
                                    @Override
                                    public void run() {
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                List<Product> productList = getProductsList(filter);


                                                lstProductos.setAdapter(new ProductArrayAdapter(getActivity(), productList));

                                                //ocultamos las listas al filtrar por texto
                                                lstDepartamentos.setVisibility(View.INVISIBLE);
                                                lstSubdepartamentos.setVisibility(View.INVISIBLE);


                                            }
                                        });
                                    }
                                }, DELAY);
                            } else
                                ((ImageView) getView().findViewById(R.id.frag_prod_ext_search_btn_find)).setImageResource(R.drawable.lupa);
                        }

                    }
                }
            });
            //HZ: Hasta aqui
            ///FIXME
        }


        final ImageView btnFind = (ImageView) getView().findViewById(R.id.frag_prod_ext_search_btn_find);
        if (btnFind != null) {
            btnFind.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (btnFind.getTag() != null && btnFind.getTag().equals("cancel")) {
                        //cancelar busqueda
                        btnFind.setImageResource(R.drawable.lupa);
                        btnFind.setTag("");

                        //cerramos el teclado por si quedó abierto en una busqueda de producto x nombre
                        Window.CloseSoftKeyboard(getActivity());

                        if (txtFilter != null) {
                            txtFilter.setText("");
                            startView();
                        }
                    } else {
                        //realizar busqueda
                        if (txtFilter != null) {

                            String filter = txtFilter.getText().toString().trim();

                            if (filter.length() <= 3) {
                                Toast.makeText(getActivity(), "Ingrese un termino superior a 3 letras", Toast.LENGTH_SHORT).show();
                            } else {
                                btnFind.setImageResource(R.drawable.ic_action_cancel);
                                btnFind.setTag("cancel");

                                List<Product> productList = getProductsList(filter);
                                lstProductos.setAdapter(new ProductArrayAdapter(getActivity(), productList));

                                //ocultamos las listas al filtrar por texto
                                lstDepartamentos.setVisibility(View.INVISIBLE);
                                lstSubdepartamentos.setVisibility(View.INVISIBLE);
                            }
                        }
                    }
                }
            });
        }

        vwDeptoHeader = getView().findViewById(R.id.lytListDeptoHeader);
        vwSubdeptoHeader = getView().findViewById(R.id.lytListSubdeptoHeader);
        vwProductHeader = getView().findViewById(R.id.lytListProductHeader);


        //estado inicial OCULTO, luego se mostraran si tienen items
        lstDepartamentos.setVisibility(View.INVISIBLE);
        lstSubdepartamentos.setVisibility(View.INVISIBLE);
        lstProductos.setVisibility(View.INVISIBLE);

        //add the footer before adding the adapter, else the footer will not load!
        /*View footerView = getActivity().getLayoutInflater().inflate(R.layout.footer, null, false);
        lstProductos.addFooterView(footerView);*/

        if (!controlExternoListView) {
            endlessScrollListener = new EndlessScrollListener();
            lstProductos.setOnScrollListener(endlessScrollListener);//TODO: esto hace que la lista se complete al scrollearla
        }


        //asignamos los eventos
        lstDepartamentos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                view.setSelected(true);

                //al seleccionar un departamento traemos los subdepartamentos
                Department d = (Department) lstDepartamentos.getItemAtPosition(position);

                //si el departamento es GENERICO, enviamos directo su producto GENERICO asociado
                if (d.isGenericDept()) {
                    List<Product> products = new ProductDao(ProductosFragment.this.getActivity().getContentResolver()).list(String.format("department_id = %d and removed = 0 and generic = 1", d.getId()), null, null);

                    if (products.size() > 0) {
                        //DEBERIA TENER SOLO UN PRODUCTO, QUE ES EL GENERICO
                        if (listener != null)
                            listener.onProductoSeleccionado(products.get(0));

                        //si es pesable, tenemos que activar el modo "pesable"
                        if (d.isGenericPesableDept()) {
                            iniciarVistaProductoPesable(products.get(0), MODO_PESABLE.DEPARTAMENTO_GENERICO);
                        }
                    }


                } else {

                    if (comportamientoVenta) {
                        /*HC 25-10
                        //cargamos el header de departamentos
                        vwDeptoHeader.setVisibility(View.VISIBLE);
                        vwSubdeptoHeader.setVisibility(View.GONE);
                        lblDepartamentos.setText(d.getName());
                        lstDepartamentos.setVisibility(View.GONE);
                        */
                    }
                }

                //cargamos los subdepartamentos
                lstSubdepartamentos.setAdapter(new SubDepartmentArrayAdapter(getActivity(), getSubdepartmentList(d.getId())));

                //cargamos los productos del departamento
                if (d.isVistaLista()) {
                    esVistaGrilla = false;
                    //lstProductos.setAdapter(new ProductArrayAdapter(getActivity(), getProductsList(d.getId(), -1)));
                    //loadProductosList(d);
                    lastDepId = d.getId();
                    lastSubDepId = -1;
                    new LoadProductsTask().execute(PAGE_SIZE);

                    grdProductos.setVisibility(View.GONE);
//                        lstCombos.setVisibility(View.GONE);
                }
                if (d.isVistaGrilla()) {
                    esVistaGrilla = true;
                    //grdProductos.setAdapter(new GridAdapter(getActivity(), getProductsList(d.getId(), -1)));
                    loadProductosGrid(d);
                    lstProductos.setVisibility(View.GONE);
//                        lstCombos.setVisibility(View.GONE);
                }
               /* else {
                    //cargamos los combos
                    loadCombosList();
                    grdProductos.setVisibility(View.GONE);
                    lstProductos.setVisibility(View.GONE);
                    lstProductos.setVisibility(View.GONE);
                    esVistaGrilla = false;

                }*/


            }
        });

        lstSubdepartamentos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                view.setSelected(true);

                //al seleccionar un subdepartamento traemos los productos
                SubDepartment d = (SubDepartment) lstSubdepartamentos.getItemAtPosition(position);

                //cargamos los productos del subdepartamento
                if (d.isVistaLista()) {
                    esVistaGrilla = false;
                    //lstProductos.setAdapter(new ProductArrayAdapter(getActivity(), getProductsList(-1, d.getId())));
                    //loadProductosList(d);
                    lastDepId = -1;
                    lastSubDepId = d.getId();
                    new LoadProductsTask().execute(PAGE_SIZE);

                    grdProductos.setVisibility(View.GONE);
                }
                if (d.isVistaGrilla()) {
                    esVistaGrilla = true;
                    //grdProductos.setAdapter(new GridAdapter(getActivity(), getProductsList(-1, d.getId())));
                    loadProductosGrid(d);
                    lstProductos.setVisibility(View.GONE);
                }

                //si el subdepartamento es GENERICO, enviamos directo su producto GENERICO asociado
                if (d.isGenericSubdept()) {
                    List<Product> products = new ProductDao(ProductosFragment.this.getActivity().getContentResolver()).list(String.format("subdepartmennt_id = %d and removed = 0 and generic = 1", d.getId()), null, null);

                    if (products.size() > 0) {
                        //DEBERIA TENER SOLO UN PRODUCTO, QUE ES EL GENERICO
                        if (listener != null)
                            listener.onProductoSeleccionado(products.get(0));

                        //si es pesable, tenemos que activar el modo "pesable"
                        if (d.isGenericPesableSubdept()) {
                            iniciarVistaProductoPesable(products.get(0), MODO_PESABLE.SUBDEPARTMENTO_GENERICO);
                        }
                    }
                } else {
                    /*HC 25-10
                    if (comportamientoVenta) {
                        //cargamos el header de subdepartamentos
                        vwSubdeptoHeader.setVisibility(View.VISIBLE);
                        lblSubdepartamentos.setText(d.getName());
                        lstSubdepartamentos.setVisibility(View.GONE);
                    }
                    */
                }
            }
        });

        lstProductos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                view.setSelected(true);

                Product p = (Product) lstProductos.getItemAtPosition(position);

                if (comportamientoVenta) {
                    if (productosSelected.containsKey((int) p.getId()))
                        productosSelected.put((int) p.getId(), productosSelected.get((int) p.getId()) + 1);
                    else
                        productosSelected.put((int) p.getId(), 1);
                }

                ((ArrayAdapter) lstProductos.getAdapter()).notifyDataSetChanged();

                if (p.isWeighable())
                    iniciarVistaProductoPesable(p, MODO_PESABLE.PRODUCTO);

                if (listener != null) {
                    /*if (p.esProductoCombo()) {
                        //si tenemos un producto en mas vendidos, que es productoCombo => retornamos el combo
                        Combo c = new ComboDao(getActivity().getContentResolver()).find(p.getProductoComboId());
                        if (c != null)
                            listener.onComboSeleccionado(c);
                    } else */
                    listener.onProductoSeleccionado(p);
                }
            }
        });

        grdProductos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                view.setSelected(true);

                Product p = (Product) grdProductos.getItemAtPosition(position);

                if (comportamientoVenta) {
                    if (productosSelected.containsKey((int) p.getId()))
                        productosSelected.put((int) p.getId(), productosSelected.get((int) p.getId()) + 1);
                    else
                        productosSelected.put((int) p.getId(), 1);
                }

                ((GridAdapter) grdProductos.getAdapter()).notifyDataSetChanged();

                if (p.isWeighable())
                    iniciarVistaProductoPesable(p, MODO_PESABLE.PRODUCTO);

                if (listener != null) {
                    listener.onProductoSeleccionado(p);
                }
            }
        });

//        lstCombos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                view.setSelected(true);
//
//                Combo c = (Combo) lstCombos.getItemAtPosition(position);
//
//                /*if (comportamientoVenta) {
//                    if (productosSelected.containsKey((int) p.getId()))
//                        productosSelected.put((int) p.getId(), productosSelected.get((int) p.getId()) + 1);
//                    else
//                        productosSelected.put((int) p.getId(), 1);
//                }*/
//
//                ((ArrayAdapter) lstCombos.getAdapter()).notifyDataSetChanged();
//
//                if (listener != null) {
//                    listener.onComboSeleccionado(c);
//                }
//            }
//        });

        lblDepartamentos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vwDeptoHeader.setVisibility(View.GONE);
                lstDepartamentos.setVisibility(View.VISIBLE);
            }
        });

        lblSubdepartamentos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vwSubdeptoHeader.setVisibility(View.GONE);
                lstSubdepartamentos.setVisibility(View.VISIBLE);
            }
        });

        lblProductos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vwProductHeader.setVisibility(View.GONE);
                if (esVistaGrilla)
                    grdProductos.setVisibility(View.VISIBLE);
                else
                    lstProductos.setVisibility(View.VISIBLE);
            }
        });

        startView();
    }

    private void startView() {
        if (mostrarSoloProdSinCod) {
            //comportamiento alternativo: no mostramos deptos/subdeptos, solo una lista con los productos sin codigo
            //cargamos los productos sin codigo
            lstProductos.setAdapter(new ProductArrayAdapter(getActivity(), getProductsList(-1, -1)));
        }
        /*else if (SOLO_COMBOS) {
            //solo debemos mostrar la lista de COMBOS
            loadCombosList();
            grdProductos.setVisibility(View.GONE);
            lstProductos.setVisibility(View.GONE);
            lstProductos.setVisibility(View.GONE);
            esVistaGrilla = false;
        }*/
        else if (mostrarSoloProductosPropios) {
            //comportamiento alternativo: no mostramos deptos/subdeptos, solo una lista con los productos propios
            //cargamos los productos propios
            lstProductos.setAdapter(new ProductArrayAdapter(getActivity(), getProductsList(-1, -1)));
        } else {

            //cargamos el DEPTO MAS VENDIDOS
            GetMasVendidos();

            //cargamos el DEPTO COMBOS
            GetCombos();

            //iniciamos la carga de los departamentos
            lstDepartamentos.setAdapter(new DepartmentArrayAdapter(this.getActivity(), getDepartmentList()));
            lstDepartamentos.getAdapter().getView(0, null, null).setBackgroundResource(R.drawable.azul);
            //lstDepartamentos.setSelector(R.drawable.frg_product_list_i_selector);
            //cargamos los productos del departamento MAS VENDIDOS por DEFAULT
            //lstProductos.setAdapter(new ProductArrayAdapter(getActivity(), getProductsList(this.MAS_VENDIDOS.getId(), -1)));

            if (!controlExternoListView) {
                lastDepId = this.MAS_VENDIDOS.getId();
                lastSubDepId = -1;
                new LoadProductsTask().execute(PAGE_SIZE);
            }
        }

        //avisamos al activity que está listo el fragment
        if (frg_listener != null)
            frg_listener.onFragmentAttached();
    }

    private long lastDepId = -1, lastSubDepId = -1;

    private void loadProductosList(final Department d) {
        lastDepId = d.getId();

        Thread t = new Thread() {
            @Override
            public void run() {

                final List<Product> productList = getProductsList(d.getId(), -1, 10);

                getActivity().runOnUiThread(
                        new Runnable() {
                            @Override
                            public void run() {
                                lstProductos.setAdapter(new ProductArrayAdapter(getActivity(), productList));
                            }
                        }
                );
            }
        };
        t.start();
    }

    private void loadProductosGrid(final Department d) {
        Thread t = new Thread() {
            @Override
            public void run() {

                final List<Product> productList = getProductsList(d.getId(), -1);

                getActivity().runOnUiThread(
                        new Runnable() {
                            @Override
                            public void run() {
                                grdProductos.setAdapter(new GridAdapter(getActivity(), productList));
                            }
                        }
                );
            }
        };
        t.start();
    }

    private void loadProductosList(final SubDepartment s) {
        Thread t = new Thread() {
            @Override
            public void run() {

                final List<Product> productList = getProductsList(-1, s.getId());

                getActivity().runOnUiThread(
                        new Runnable() {
                            @Override
                            public void run() {
                                lstProductos.setAdapter(new ProductArrayAdapter(getActivity(), productList));
                            }
                        }
                );
            }
        };
        t.start();
    }

    private void loadProductosGrid(final SubDepartment s) {
        Thread t = new Thread() {
            @Override
            public void run() {

                final List<Product> productList = getProductsList(-1, s.getId());

                getActivity().runOnUiThread(
                        new Runnable() {
                            @Override
                            public void run() {
                                grdProductos.setAdapter(new GridAdapter(getActivity(), productList));
                            }
                        }
                );
            }
        };
        t.start();
    }

    /*private void loadCombosList() {
        Thread t = new Thread() {
            @Override
            public void run() {

                final List<Combo> combosList = getCombosList();

                getActivity().runOnUiThread(
                        new Runnable() {
                            @Override
                            public void run() {
                                lstCombos.setAdapter(new ComboArrayAdapter(getActivity(), combosList));
                            }
                        }
                );
            }
        };
        t.start();
    }
*/
    private void iniciarVistaProductoPesable(Product p, MODO_PESABLE modo) {
        //este comportamiento, aplica solo para modalidad VENTAS
        if (comportamientoVenta) {

            modoActivo = modo;//seteamos el modo, para luego restaurarlo

            if (modo == MODO_PESABLE.DEPARTAMENTO_GENERICO) {
                //cargamos el header de departamentos
                vwDeptoHeader.setVisibility(View.VISIBLE);
                lblDepartamentos.setText(p.getName());
                lstDepartamentos.setVisibility(View.GONE);
            }

            if (modo == MODO_PESABLE.SUBDEPARTMENTO_GENERICO) {

                //cargamos el header de subdepartamentos
                vwSubdeptoHeader.setVisibility(View.VISIBLE);
                lblSubdepartamentos.setText(p.getName());
                lstSubdepartamentos.setVisibility(View.GONE);

                //por las dudas, voy a volver a ocultar la lista de departamentos...
                //puede pasar que esté visible porque se tocó de nuevo, luego de la seleccion de subdeptos
                lstDepartamentos.setVisibility(View.GONE);
            }

            if (modo == MODO_PESABLE.PRODUCTO) {
                //cargamos el header de producto (dado que es pesable)
                vwProductHeader.setVisibility(View.VISIBLE);
                lblProductos.setText(p.getName());
                //ocultamos grilla o lista
                lstProductos.setVisibility(View.GONE);
                grdProductos.setVisibility(View.GONE);

                //por las dudas, voy a volver a ocultar las listas de departamentos|subdepartamentos...
                //puede pasar que esté visible porque usé directo el producto pesable desde MAS VENDIDOS
                lstDepartamentos.setVisibility(View.GONE);
                //puede pasar que está visible porque no filtre por un subdepto
                lstSubdepartamentos.setVisibility(View.GONE);
            }
        }
    }

    private void finalizarVistaProductoPesable() {
        if (modoActivo == MODO_PESABLE.DEPARTAMENTO_GENERICO) {
            lblDepartamentos.performClick();
        }
        if (modoActivo == MODO_PESABLE.SUBDEPARTMENTO_GENERICO) {
            lblSubdepartamentos.performClick();

            //volvemos a mostrar la listas de deptos (la habíamo ocultado por las dudas...)
            //si está GONE el header, es porque no estaba cerrada la lista
            if (vwDeptoHeader.getVisibility() == View.GONE)
                lstDepartamentos.setVisibility(View.VISIBLE);
        }
        if (modoActivo == MODO_PESABLE.PRODUCTO) {
            lblProductos.performClick();

            //volvemos a mostrar las listas de deptos|subdeptos (las habíamos ocultado por las dudas...)
            //si está GONE el header, es porque no estaba cerrada la lista
            if (vwDeptoHeader.getVisibility() == View.GONE)
                lstDepartamentos.setVisibility(View.VISIBLE);
            //si está GONE el header, es porque no estaba cerrada la lista
            if (vwSubdeptoHeader.getVisibility() == View.GONE)
                lstSubdepartamentos.setVisibility(View.VISIBLE);
        }
    }


    private List<Department> getDepartmentList() {
        if (SOLO_COMBOS) {
            List<Department> lst = new ArrayList<Department>();
            lst.add(0, this.COMBOS);//agregamos el depto COMBOS

            return lst;
        } else {
            String condicion = "exists (select 1 from products where department_id = departments._id and removed = 0 and generic = 0)";
            List<Department> lst = new DepartmentDao(this.getActivity().getContentResolver()).list(condicion, null, DepartmentTable.COLUMN_NAME);

            if (this.COMBOS != null)
                lst.add(0, this.COMBOS);//agregamos el depto COMBOS
            if (this.MAS_VENDIDOS != null)
                lst.add(0, this.MAS_VENDIDOS);//agregamos el depto MAS VENDIDOS
            return lst;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //cerramos el teclado
        Window.CloseSoftKeyboard(getActivity());

    }

    private List<SubDepartment> getSubdepartmentList(long idDepartment) {
        String condicion = "exists (select 1 from products where subdepartmennt_id = subdepartments._id and removed = 0 and generic = 0) and " + String.format("department_id = %d", idDepartment);

        return new SubDepartmentDao(this.getActivity().getContentResolver()).list(condicion, null, SubdepartmentTable.COLUMN_NAME);
    }

    private List<Product> getProductsList(long idDepartment, long idSubDepartment) {

        String cond_extra = mostrarProdSinPrecio ? "" : " and sale_price <> 0";

        cond_extra += mostrarSoloProdSinCod ? " and code = ''" : "";

        //para que no figuren los combos
        //cond_extra += mostrarSoloProductosPropios ? " and code not like '%-%' and autom <> " + String.valueOf(Product.TYPE_AUTOM.PROD_ALTA_AUTOM) : "";
        cond_extra += mostrarSoloProductosPropios ? " and code not like '%-%' and autom = " + String.valueOf(Product.TYPE_AUTOM.PROD_ALTA_SIN_SKU) : "";

        //cond_extra += mostrarSoloProductosCatalogo ? " and autom = " + String.valueOf(Product.TYPE_AUTOM.PROD_ALTA_AUTOM) : "";
        cond_extra += mostrarSoloProductosCatalogo ? " and autom <> " + String.valueOf(Product.TYPE_AUTOM.PROD_ALTA_SIN_SKU) : "";

        //cond_extra += " and code not like '%-%'" ; //para que no figuren los combos

        if (idDepartment != -1) {
            if (idDepartment == MAS_VENDIDOS_ID) //seleccionamos los productos mas vendidos
                return new ProductDao(this.getActivity().getContentResolver(), true, true).list("removed = 0 and generic = 0" + cond_extra, null, ProductTable.COLUMN_NAME);
            else
                return new ProductDao(this.getActivity().getContentResolver()).list(String.format("department_id = %d AND removed = 0 and generic = 0", idDepartment) + cond_extra, null, ProductTable.COLUMN_NAME);
        }
        if (idSubDepartment != -1)
            return new ProductDao(this.getActivity().getContentResolver()).list(String.format("subdepartmennt_id = %d AND removed = 0 and generic = 0", idSubDepartment) + cond_extra, null, ProductTable.COLUMN_NAME);

        return new ProductDao(this.getActivity().getContentResolver()).list("removed = 0" + cond_extra, null, ProductTable.COLUMN_NAME);
    }

    private List<Product> getProductsList(long idDepartment, long idSubDepartment, int top) {

        String cond_extra = mostrarProdSinPrecio ? "" : " and sale_price <> 0";

        cond_extra += mostrarSoloProdSinCod ? " and code = ''" : "";

        //para que no figuren los combos
        //cond_extra += mostrarSoloProductosPropios ? " and code not like '%-%' and autom <> " + String.valueOf(Product.TYPE_AUTOM.PROD_ALTA_AUTOM) : "";
        cond_extra += mostrarSoloProductosPropios ? " and code not like '%-%' and autom = " + String.valueOf(Product.TYPE_AUTOM.PROD_ALTA_SIN_SKU) : "";

        //cond_extra += mostrarSoloProductosCatalogo ? " and autom = " + String.valueOf(Product.TYPE_AUTOM.PROD_ALTA_AUTOM) : "";
        cond_extra += mostrarSoloProductosCatalogo ? " and autom <> " + String.valueOf(Product.TYPE_AUTOM.PROD_ALTA_SIN_SKU) : "";

        //cond_extra += " and code not like '%-%'" ; //para que no figuren los combos

        if (idDepartment != -1) {
            if (idDepartment == MAS_VENDIDOS_ID) //seleccionamos los productos mas vendidos
                return new ProductDao(this.getActivity().getContentResolver(), true, true).list("removed = 0 and generic = 0" + cond_extra, null, ProductTable.COLUMN_NAME, "", "", String.valueOf(top));
            else
                return new ProductDao(this.getActivity().getContentResolver()).list(String.format("department_id = %d AND removed = 0 and generic = 0", idDepartment) + cond_extra, null, ProductTable.COLUMN_NAME, "", "", String.valueOf(top));
        }
        if (idSubDepartment != -1)
            return new ProductDao(this.getActivity().getContentResolver()).list(String.format("subdepartmennt_id = %d AND removed = 0 and generic = 0", idSubDepartment) + cond_extra, null, ProductTable.COLUMN_NAME, "", "", String.valueOf(top));

        return new ProductDao(this.getActivity().getContentResolver()).list("removed = 0" + cond_extra, null, ProductTable.COLUMN_NAME, "", "", String.valueOf(top));
    }

    private List<Product> getProductsList(String filter) {

        String cond_extra = mostrarProdSinPrecio ? "" : " and sale_price <> 0";

        cond_extra += mostrarSoloProdSinCod ? " and code = ''" : "";

        //para que no figuren los combos
        //cond_extra += mostrarSoloProductosPropios ? " and code not like '%-%' and autom <> " + String.valueOf(Product.TYPE_AUTOM.PROD_ALTA_AUTOM) : "";
        cond_extra += mostrarSoloProductosPropios ? " and code not like '%-%' and autom = " + String.valueOf(Product.TYPE_AUTOM.PROD_ALTA_SIN_SKU) : "";

        //cond_extra += mostrarSoloProductosCatalogo ? " and autom = " + String.valueOf(Product.TYPE_AUTOM.PROD_ALTA_AUTOM) : "";
        cond_extra += mostrarSoloProductosCatalogo ? " and autom <> " + String.valueOf(Product.TYPE_AUTOM.PROD_ALTA_SIN_SKU) : "";

        return new ProductDao(this.getActivity().getContentResolver()).list("removed = 0 AND (upper(name) like '%" + filter.toUpperCase() + "%') and code not like '%-%'" + cond_extra, null, "name ASC");
    }

   /* private List<Combo> getCombosList() {
        return new ComboDao(this.getActivity().getContentResolver()).list("baja = 0", null, ComboTable.COLUMN_NOMBRE);
    }*/

    /*private List<Product> ComboListAsProductList(List<Combo> combos){
        List<Product> products = new ArrayList<Product>();
        for(Combo c : combos){
            products.add(new Product(c));
        }
        return products;
    }*/

    private void GetMasVendidos() {
        this.MAS_VENDIDOS = new Department();
        this.MAS_VENDIDOS.setId(MAS_VENDIDOS_ID);
        this.MAS_VENDIDOS.setDescription("MAS VENDIDOS");
        this.MAS_VENDIDOS.setName("MAS VENDIDOS");
    }

    private void GetCombos() {
        if (combosHabilitados) {
            this.COMBOS = new Department();
            this.COMBOS.setId(COMBOS_ID);
            this.COMBOS.setDescription("COMBOS");
            this.COMBOS.setName("COMBOS");
        }
    }

    public class DepartmentArrayAdapter extends ArrayAdapter<Department> {
        private final Context context;
        private List<Department> departments;

        public DepartmentArrayAdapter(Context context, List<Department> departments) {
            super(context, R.layout.list_item, departments);
            this.context = context;
            this.departments = departments;

            lstDepartamentos.setVisibility(this.departments.size() > 0 ? View.VISIBLE : View.INVISIBLE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(context)
                        .inflate(R.layout.list_item, parent, false);
            }

            //LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            //View rowView = inflater.inflate(R.layout.list_item, parent, false);
            //if (position % 2 != 0)
            //    rowView.setBackgroundColor(Color.LTGRAY);
            //else
            //rowView.setBackgroundColor(Color.WHITE);

            TextView nameTextView = (TextView) convertView.findViewById(R.id.lblListItem);
            nameTextView.setText(departments.get(position).getName());

            convertView.setBackgroundResource(R.drawable.frg_prod_selectable_item);

            /*if (convertView.isSelected())
                convertView.setBackgroundResource(R.drawable.azul);
            else
                convertView.setBackgroundResource(R.drawable.bg_normal);*/

            if (departments.get(position).getId() == MAS_VENDIDOS_ID) {
                nameTextView.setTextColor(getResources().getColor(R.color.boton_rojo));
                nameTextView.setTypeface(null, Typeface.BOLD);
            } else if (departments.get(position).getId() == COMBOS_ID) {
                nameTextView.setTextColor(getResources().getColor(R.color.boton_verde));
                nameTextView.setTypeface(null, Typeface.BOLD);
            } else {
                nameTextView.setTextColor(getResources().getColor(R.color.texto_black));
                nameTextView.setTypeface(null, Typeface.NORMAL);
            }

            return convertView;
        }

    }

    public class SubDepartmentArrayAdapter extends ArrayAdapter<SubDepartment> {
        private final Context context;
        private List<SubDepartment> subdepartments;

        public SubDepartmentArrayAdapter(Context context, List<SubDepartment> subdepartments) {
            super(context, R.layout.list_item, subdepartments);
            this.context = context;
            this.subdepartments = subdepartments;

            lstSubdepartamentos.setVisibility(this.subdepartments.size() > 0 ? View.VISIBLE : View.INVISIBLE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(context)
                        .inflate(R.layout.list_item, parent, false);
            }

            //if (position % 2 != 0)
            //    rowView.setBackgroundColor(Color.LTGRAY);
            //else
            //convertView.setBackgroundColor(Color.WHITE);

            TextView nameTextView = (TextView) convertView.findViewById(R.id.lblListItem);
            nameTextView.setText(subdepartments.get(position).getName());

            convertView.setBackgroundResource(R.drawable.frg_prod_selectable_item);

            /*if (convertView.isSelected())
                convertView.setBackgroundResource(R.drawable.azul);
            else
                convertView.setBackgroundResource(R.drawable.bg_normal);*/

            return convertView;
        }

    }

    public class ProductArrayAdapter extends ArrayAdapter<Product> {
        private final Context context;
        private List<Product> products;

        public ProductArrayAdapter(Context context, List<Product> products) {
            super(context, R.layout.list_item_prod, products);
            this.context = context;
            this.products = products;

            lstProductos.setVisibility(this.products.size() > 0 ? View.VISIBLE : View.INVISIBLE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(context)
                        .inflate(R.layout.list_item_prod, parent, false);
            }

            //if (position % 2 != 0)
            //    rowView.setBackgroundColor(Color.LTGRAY);
            //else
            //rowView.setBackgroundColor(Color.WHITE);

            Product p = products.get(position);

            TextView nameTextView = (TextView) convertView.findViewById(R.id.lblListItem);
            nameTextView.setText(p.getName());

            //ImageView imagen = (ImageView) convertView.findViewById(R.id.imgListItem);
            //imagen.setImageResource(R.drawable.objects_icon);

            TextView priceTextView = (TextView) convertView.findViewById(R.id.lblListItemPrice);
            priceTextView.setText(Formatos.FormateaDecimal(p.getSalePrice(), formato, moneda));

            //ajustes visuales...
            if (formato == Formatos.DecimalFormat) {
                priceTextView.setTextSize(20);
            } else if (formato == Formatos.DecimalFormat_PE) {
                priceTextView.setTextSize(18);
            } else {
                priceTextView.setTextSize(25);
            }

            if (priceTextView.getText().length() > 5) {
                priceTextView.setTextSize(18);
            } else {
                priceTextView.setTextSize(20);
            }

            TextView stockTextView = (TextView) convertView.findViewById(R.id.lblListItemStock);
            stockTextView.setText(Formatos.FormateaDecimal(p.getStock(), Formatos.DecimalFormat_US));

            if (p.getStock() <= p.getMinStock())
                stockTextView.setTextColor(getResources().getColor(R.color.boton_rojo));
            else
                stockTextView.setTextColor(getResources().getColor(R.color.texto_black));

            if (comportamientoVenta) {
                TextView cantTextView = (TextView) convertView.findViewById(R.id.lblListItemCant);
                cantTextView.setVisibility(View.VISIBLE);
                cantTextView.setText(Formatos.FormateaDecimal(
                        productosSelected.containsKey((int) p.getId()) ? productosSelected.get((int) p.getId()) : 0
                        , Formatos.DecimalFormat_US));
            }

            if (convertView.isSelected() || productosSelected.containsKey((int) p.getId()))
                convertView.setBackgroundResource(R.drawable.azul);
            else
                convertView.setBackgroundResource(R.drawable.bg_normal);

            return convertView;
        }

    }

    public class GridAdapter extends BaseAdapter {
        private final Context context;
        private List<Product> products;

        public GridAdapter(Context context, List<Product> products) {
            this.context = context;
            this.products = products;

            grdProductos.setVisibility(this.products.size() > 0 ? View.VISIBLE : View.INVISIBLE);
        }

        @Override
        public int getCount() {
            return products.size();
        }

        @Override
        public Object getItem(int position) {
            return products.get(position);
        }

        @Override
        public long getItemId(int position) {
            return products.get(position).getId();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(context)
                        .inflate(R.layout.grid_item_prod, parent, false);
            }

            Product p = products.get(position);

            TextView nameTextView = (TextView) convertView.findViewById(R.id.lblListItem);
            ImageView imagen = (ImageView) convertView.findViewById(R.id.imgListItem);

            Uri uri = UriUtils.resolveUri(ProductosFragment.this.getActivity(), p.getImage_uri());
            imagen.setImageURI(uri);

            //verificamos si se encontró una imagen o no
            if (imagen.getDrawable() != null) {
                nameTextView.setVisibility(View.GONE);
                imagen.setVisibility(View.VISIBLE);
                //imagen.setImageURI(uri);
                //imagen.setImageResource(R.drawable.objects_icon);
            } else {
                nameTextView.setVisibility(View.VISIBLE);
                nameTextView.setText(p.getName());
                imagen.setVisibility(View.GONE);
            }

            if (convertView.isSelected() || productosSelected.containsKey((int) p.getId()))
                convertView.setBackgroundResource(R.drawable.azul);
            else
                convertView.setBackgroundResource(R.drawable.bg_normal);

            return convertView;
        }


    }

   /* public class ComboArrayAdapter extends ArrayAdapter<Combo> {
        private final Context context;
        private List<Combo> combos;

        public ComboArrayAdapter(Context context, List<Combo> combos) {
            super(context, R.layout.list_item_prod, combos);
            this.context = context;
            this.combos = combos;

            lstCombos.setVisibility(this.combos.size() > 0 ? View.VISIBLE : View.INVISIBLE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(context)
                        .inflate(R.layout.list_item_prod, parent, false);
            }

            Combo c = combos.get(position);

            TextView nameTextView = (TextView) convertView.findViewById(R.id.lblListItem);
            nameTextView.setText(c.getNombre());

            TextView priceTextView = (TextView) convertView.findViewById(R.id.lblListItemPrice);
            priceTextView.setText(Formatos.FormateaDecimal(c.getPrecio(getActivity().getContentResolver()), formato, moneda));

            //ajustes visuales...
            if (formato == Formatos.DecimalFormat) {
                priceTextView.setTextSize(20);
            } else {
                priceTextView.setTextSize(25);
            }

            if (priceTextView.getText().length() > 5) {
                priceTextView.setTextSize(18);
            } else {
                priceTextView.setTextSize(20);
            }

            TextView stockTextView = (TextView) convertView.findViewById(R.id.lblListItemStock);
            stockTextView.setText(Formatos.FormateaDecimal(0, Formatos.DecimalFormat_US));

            *//*if (p.getStock() <= p.getMinStock())
                stockTextView.setTextColor(getResources().getColor(R.color.boton_rojo));
            else
                stockTextView.setTextColor(getResources().getColor(R.color.texto_black));*//*

     *//*if (comportamientoVenta) {
                TextView cantTextView = (TextView) convertView.findViewById(R.id.lblListItemCant);
                cantTextView.setVisibility(View.VISIBLE);
                cantTextView.setText(Formatos.FormateaDecimal(
                        productosSelected.containsKey((int) p.getId()) ? productosSelected.get((int) p.getId()) : 0
                        , Formatos.DecimalFormat_US));
            }*//*

            if (convertView.isSelected() || productosSelected.containsKey((int) c.getId()))
                convertView.setBackgroundResource(R.drawable.azul);
            else
                convertView.setBackgroundResource(R.drawable.bg_normal);

            return convertView;
        }

    }*/


    class LoadProductsTask extends AsyncTask<Integer, CharSequence, String> {
        private int position = -1;

        public LoadProductsTask() {

        }

        public LoadProductsTask(int _position) {
            position = _position;
        }

        private ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            loading = true;

            //iniciamos un mensaje para el usuario
            dialog = new ProgressDialog(getActivity(), R.style.ProgressTransparent);
            dialog.setMessage("");//"Aguarde por favor...");
            dialog.setIndeterminate(false);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            loading = false;

            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
        }

        @Override
        protected String doInBackground(Integer... params) {

            final List<Product> productList = getProductsList(lastDepId, lastSubDepId, params[0]);

            getActivity().runOnUiThread(
                    new Runnable() {
                        @Override
                        public void run() {
                            lstProductos.setAdapter(new ProductArrayAdapter(getActivity(), productList));
                            //endlessScrollListener.reset();
                            lstProductos.setSelection(position);
                        }
                    }
            );

            return null;
        }
    }

    int PAGE_SIZE = 50;

    //listener para scrollear y traer mas datos al final de la lista
    /*class EndlessScrollListener implements AbsListView.OnScrollListener {
        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            //what is the bottom iten that is visible
            int lastInScreen = firstVisibleItem + visibleItemCount;
            //is the bottom item visible & not loading more already ? Load more !
            if((lastInScreen == totalItemCount) && !(loadingMore)){
                //Thread thread =  new Thread(null, loadMoreListItems);
                //thread.start();

                new LoadProductsTask(lastScrollPosition).execute((currentPage + 1 ) * PAGE_SIZE);
            }

        }
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }
    }*/

    private boolean loading = false;

    class EndlessScrollListener implements AbsListView.OnScrollListener {

        private int visibleThreshold = 5;
        private int currentPage = 0;
        private int previousTotal = 0;
        //private boolean loading = true;

        private int lastScrollPosition = 0;

        public EndlessScrollListener() {
        }
        /*public EndlessScrollListener(int visibleThreshold) {
            this.visibleThreshold = visibleThreshold;
        }

        public void reset(){
            currentPage = 0;
            previousTotal = 0;
            loading = true;
            lastScrollPosition = 0;
        }*/

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            int lastInScreen = firstVisibleItem + visibleItemCount;
            if (totalItemCount > 0 && lastInScreen == totalItemCount && totalItemCount % PAGE_SIZE == 0 && !loading) {
                //ultimo => cargamos
                new LoadProductsTask(lastInScreen - 1).execute(totalItemCount + PAGE_SIZE);

            }

            /*if (loading && totalItemCount > previousTotal) {
                loading = false;
                previousTotal = totalItemCount;
                currentPage++;
            }

            boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount-1;

            if(loadMore && !loading) {
                lastScrollPosition = firstVisibleItem;
                new LoadProductsTask(lastScrollPosition).execute((currentPage + 1 ) * PAGE_SIZE);
                loading = true;
            }*/
        }

        //@Override
        public void onScroll_(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (loading) {
                if (totalItemCount > previousTotal) {
                    loading = false;
                    previousTotal = totalItemCount;
                    currentPage++;

                    //view.setSelection(lastScrollPosition);//TODO: tratar de que no se vuelva al comienzo
                }
            }
            if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
                // I load the next page of gigs using a background task,
                // but you can call any function here.
                lastScrollPosition = firstVisibleItem;

                new LoadProductsTask(lastScrollPosition).execute((currentPage + 1) * PAGE_SIZE);
                loading = true;
            }
        }

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }
    }
}
