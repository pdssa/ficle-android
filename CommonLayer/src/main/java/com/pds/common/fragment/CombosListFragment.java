/*
package com.pds.common.fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.pds.common.Formato;
import com.pds.common.Formatos;
import com.pds.common.dao.ComboDao;
import com.pds.common.db.ComboTable;
import com.pds.common.model.Combo;

import java.text.DecimalFormat;
import java.util.List;
import com.pds.common.R;
*/
/*

public class CombosListFragment extends Fragment {

    private ComboListFragmentInteractionListener mListener;
    private static final String KEY_FORMATO_DECIMAL = "_FORMATO_DECIMAL";
    private static final String KEY_COMBOS_PROMOCIONALES = "_COMBOS_PROMOS";
    private ListView listaCombos;
    private DecimalFormat FORMATO_DECIMAL;
    private boolean COMBOS_PROMOCIONALES;

    public static CombosListFragment newInstance(DecimalFormat FORMATO_DECIMAL, boolean combosPromocionales){
        CombosListFragment combosListFragment = new CombosListFragment();

        Bundle bundle = new Bundle();

        bundle.putSerializable(KEY_FORMATO_DECIMAL, FORMATO_DECIMAL);
        bundle.putBoolean(KEY_COMBOS_PROMOCIONALES, combosPromocionales);

        combosListFragment.setArguments(bundle);

        return combosListFragment;

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FORMATO_DECIMAL = (DecimalFormat) getArguments().getSerializable(KEY_FORMATO_DECIMAL);
        COMBOS_PROMOCIONALES = getArguments().getBoolean(KEY_COMBOS_PROMOCIONALES);

        View view = inflater.inflate(R.layout.fragment_list_combo, container, false);

        listaCombos = (ListView) view.findViewById(R.id.lista_combo_list_combos);
        listaCombos.setEmptyView(view.findViewById(android.R.id.empty));

        listaCombos.setAdapter(new ListaCombosAdapter(getActivity(), getList()));

        listaCombos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Combo c = (Combo) listaCombos.getAdapter().getItem(position);

                if(mListener != null){
                    mListener.onComboSelected(c);
                }
            }
        });

        return view;
    }

    private List<Combo> getList() {

        if(COMBOS_PROMOCIONALES){
            return new ComboDao(getActivity().getContentResolver()).list(
                    ComboTable.COLUMN_PROMOCION_ID + " <> 0 AND " + ComboTable.COLUMN_BAJA + " = 0",
                    null,
                    ComboTable.COLUMN_ID + " ASC");
        }
        else {
            return new ComboDao(getActivity().getContentResolver()).list(
                    ComboTable.COLUMN_PROMOCION_ID + " = 0 AND " + ComboTable.COLUMN_BAJA + " = 0",
                    null,
                    ComboTable.COLUMN_ID + " ASC");
        }
    }

    class ListaCombosAdapter extends ArrayAdapter<Combo> {
        private Context context;
        private List<Combo> datos;

        public ListaCombosAdapter(Context context, List<Combo> datos) {
            super(context, android.R.layout.simple_list_item_1, datos);

            this.context = context;
            this.datos = datos;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // En primer lugar "inflamos" una nueva vista, que será la que se
            // mostrará en la celda del ListView. Para ello primero creamos el
            // inflater, y después inflamos la vista.
            if (convertView == null) {
                convertView = LayoutInflater.from(context)
                        .inflate(R.layout.list_item_lista_combos, parent, false);
            }

            Combo row = datos.get(position);

            // A partir de la vista, recogeremos los controles que contiene para
            // poder manipularlos.
            // Recogemos los TextView para mostrar datos
            ((TextView) convertView.findViewById(R.id.list_item_lista_combos_nombre_combo)).setText(row.getNombre());
            ((TextView) convertView.findViewById(R.id.list_item_lista_combos_precios)).setText(Formatos.FormateaDecimal(row.getPrecio(getActivity().getContentResolver()), FORMATO_DECIMAL, "$"));
            ((TextView) convertView.findViewById(R.id.list_item_lista_combos_producto_principal)).setText(row.getProductoPrincipal(getActivity().getContentResolver()).getName());
            ((TextView) convertView.findViewById(R.id.list_item_lista_combos_codigo)).setText(row.getProductoPrincipal(getActivity().getContentResolver()).getCode());
            ((TextView) convertView.findViewById(R.id.list_item_lista_combos_codigo_combo)).setText(Formato.FormateaDate(row.getFecha_alta(), Formato.DateTwoDigFormatSlash));

            return convertView;
        }
    }

    public interface ComboListFragmentInteractionListener {
        void onComboSelected(Combo combo);
    }

    public void setComboListFragmentInteractionListener(ComboListFragmentInteractionListener mListener) {
        this.mListener = mListener;
    }
}
*/
