package com.pds.common.fragment;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Formatos;
import com.pds.common.R;
import com.pds.common.dao.DepartmentDao;
import com.pds.common.dao.ProductDao;
import com.pds.common.dao.SubDepartmentDao;
import com.pds.common.db.ComboTable;
import com.pds.common.db.DepartmentTable;
import com.pds.common.db.ProductTable;
import com.pds.common.db.SubdepartmentTable;
import com.pds.common.model.Combo;
import com.pds.common.model.Department;
import com.pds.common.model.Product;
import com.pds.common.model.SubDepartment;
import com.pds.common.util.UriUtils;
import com.pds.common.util.Window;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class ProductosPropiosFragment extends BaseProductosFragment {

    ListView lstProductos;
    GridView grdProductos;

    GenericFragmentListener frg_listener;

    private static final String ARG_PARAM1 = "compor_vtas";
    private static final String ARG_PARAM2 = "prods";
    private static final String ARG_PARAM3 = "formato";
    private static final String ARG_PARAM4 = "productos_sin_precio";
    private static final String ARG_PARAM5 = "moneda";
    private static final String ARG_PARAM6 = "solo_productos_sin_codigo";
    private static final String ARG_PARAM7 = "combos_habilitados";
    private static final String ARG_PARAM8 = "control_externo_lisview";
    private static final String ARG_PARAM9 = "solo_productos_propios";
    private static final String ARG_PARAM10 = "solo_productos_catalogo";
    private static final String ARG_PARAM11 = "solo_combos";

    boolean comportamientoVenta, mostrarProdSinPrecio, combosHabilitados, controlExternoListView;
    boolean mostrarSoloProductosPropios;
    //, mostrarSoloProductosCatalogo;
    //private boolean SOLO_COMBOS;


    private DecimalFormat formato;
    private String moneda;
    private HashMap<Integer, Integer> productosSelected;

    private boolean esVistaGrilla;

    private MODO_PESABLE modoActivo;

    private enum MODO_PESABLE {
        PRODUCTO,
        SUBDEPARTMENTO_GENERICO,
        DEPARTAMENTO_GENERICO
    }

    public ListView getLstProductos() {
        return lstProductos;
    }


    public void setListener(GenericFragmentListener listener) {
        this.frg_listener = listener;
    }

    /*public static ProductosFragment newInstance(boolean _comportamientoVentas) {
        ProductosFragment fragment = new ProductosFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_PARAM1, _comportamientoVentas);
        args.putSerializable(ARG_PARAM2, new HashMap<Integer, Integer>());
        args.putSerializable(ARG_PARAM3, Formatos.DecimalFormat_US);
        args.putBoolean(ARG_PARAM4, true);
        fragment.setArguments(args);
        return fragment;
    }*/


    public static BaseProductosFragment newInstance(boolean _comportamientoVentas, HashMap<Integer, Integer> _productosSelected, DecimalFormat _formato,
                                                    boolean _productosSinPrecio, String _moneda, boolean _soloProductosSinCodigo,
                                                    boolean _solo_productos_propios, boolean _solo_productos_catalogo, boolean _solo_combos) {

        ProductosPropiosFragment fragment = new ProductosPropiosFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_PARAM1, _comportamientoVentas);
        //args.putLongArray(ARG_PARAM2, _productosIdsSelected);
        args.putSerializable(ARG_PARAM2, _productosSelected);
        args.putSerializable(ARG_PARAM3, _formato);
        args.putBoolean(ARG_PARAM4, _productosSinPrecio);
        args.putString(ARG_PARAM5, _moneda);
        //args.putBoolean(ARG_PARAM6, _soloProductosSinCodigo);
        args.putBoolean(ARG_PARAM7, true);
        args.putBoolean(ARG_PARAM8, false);
        args.putBoolean(ARG_PARAM9, true);
        //args.putBoolean(ARG_PARAM10, _solo_productos_catalogo);
        //args.putBoolean(ARG_PARAM11, _solo_combos);

        fragment.setArguments(args);
        return fragment;
    }

    public ProductosPropiosFragment() {
        // Required empty public constructor
        esVistaGrilla = false;
    }

    public void RefreshList(boolean _productosSinPrecio) {
        mostrarProdSinPrecio = _productosSinPrecio;

        //recargamos la lista
        onActivityCreated(new Bundle());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            comportamientoVenta = getArguments().getBoolean(ARG_PARAM1);
            productosSelected = (HashMap<Integer, Integer>) getArguments().getSerializable(ARG_PARAM2);
            formato = (DecimalFormat) getArguments().getSerializable(ARG_PARAM3);
            mostrarProdSinPrecio = getArguments().getBoolean(ARG_PARAM4);
            moneda = getArguments().getString(ARG_PARAM5);
            //mostrarSoloProdSinCod = getArguments().getBoolean(ARG_PARAM6);
            combosHabilitados = getArguments().getBoolean(ARG_PARAM7);
            controlExternoListView = getArguments().getBoolean(ARG_PARAM8);
            mostrarSoloProductosPropios = getArguments().getBoolean(ARG_PARAM9);
            //mostrarSoloProductosCatalogo = getArguments().getBoolean(ARG_PARAM10);
            //SOLO_COMBOS = getArguments().getBoolean(ARG_PARAM11);
            /*long[] ids = getArguments().getLongArray(ARG_PARAM2);
            productosIdsSelected = new ArrayList<Long>();
            for(long id : ids){
                productosIdsSelected.add(id);
            }*/

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_productos_propios, container, false);
    }


    private EndlessScrollListener endlessScrollListener;

    private int[] btnSubdeptos = new int[]{
            R.id.frag_prod_prop_btn_sd_frutas,
            R.id.frag_prod_prop_btn_sd_panaderia,
            R.id.frag_prod_prop_btn_sd_verd,
            R.id.frag_prod_prop_btn_sd_envases
    };

    private int[] btnDeptos = new int[]{
            R.id.frag_prod_prop_btn_d_abarr,
            R.id.frag_prod_prop_btn_d_alim,
            R.id.frag_prod_prop_btn_d_amb,
            R.id.frag_prod_prop_btn_d_bebest,
            R.id.frag_prod_prop_btn_d_cuidad,
            R.id.frag_prod_prop_btn_d_limpi,
            R.id.frag_prod_prop_btn_d_masc,
            R.id.frag_prod_prop_btn_d_tabac,
            R.id.frag_prod_prop_btn_d_varios
    };


    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);

        lstProductos = (ListView) getView().findViewById(R.id.lstProductos);
        grdProductos = (GridView) getView().findViewById(R.id.grdProductos);

        View btnBack = getView().findViewById(R.id.frag_prod_prop_btn_back);
        View btnBack2 = getView().findViewById(R.id.frag_prod_prop_search_btn_back);
        if (btnBack != null) {
            btnBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onFragmentProductoClose();
                }
            });
        }
        if (btnBack2 != null) {
            btnBack2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //listener.onFragmentProductoClose();
                    //cerramos la lista
                    changeListProdsVisibility(false);
                }
            });
        }

        for (int btndepto : btnDeptos) {
            getView().findViewById(btndepto).setOnClickListener(btnDeptoClickListener);
        }

        for (int btnsubdepto : btnSubdeptos) {
            getView().findViewById(btnsubdepto).setOnClickListener(btnSubdeptoClickListener);
        }

        //estado inicial OCULTO, luego se mostraran si tienen items
        changeListProdsVisibility(false);
        //lstProductos.setVisibility(View.GONE);

        //add the footer before adding the adapter, else the footer will not load!
        /*View footerView = getActivity().getLayoutInflater().inflate(R.layout.footer, null, false);
        lstProductos.addFooterView(footerView);*/

        if (!controlExternoListView) {
            endlessScrollListener = new EndlessScrollListener();
            lstProductos.setOnScrollListener(endlessScrollListener);//TODO: esto hace que la lista se complete al scrollearla
        }

        lstProductos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                view.setSelected(true);

                Product p = (Product) lstProductos.getItemAtPosition(position);

                if (comportamientoVenta) {
                    if (productosSelected.containsKey((int) p.getId()))
                        productosSelected.put((int) p.getId(), productosSelected.get((int) p.getId()) + 1);
                    else
                        productosSelected.put((int) p.getId(), 1);
                }

                ((ArrayAdapter) lstProductos.getAdapter()).notifyDataSetChanged();

                if (p.isWeighable())
                    Toast.makeText(getActivity(), "Pesable!!", Toast.LENGTH_SHORT).show();
                //iniciarVistaProductoPesable(p, MODO_PESABLE.PRODUCTO);

                if (listener != null) {
                    /*if (p.esProductoCombo()) {
                        //si tenemos un producto en mas vendidos, que es productoCombo => retornamos el combo
                        Combo c = new ComboDao(getActivity().getContentResolver()).find(p.getProductoComboId());
                        if (c != null)
                            listener.onComboSeleccionado(c);
                    } else */
                        listener.onProductoSeleccionado(p);
                }
            }
        });

        grdProductos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                view.setSelected(true);

                Product p = (Product) grdProductos.getItemAtPosition(position);

                if (comportamientoVenta) {
                    if (productosSelected.containsKey((int) p.getId()))
                        productosSelected.put((int) p.getId(), productosSelected.get((int) p.getId()) + 1);
                    else
                        productosSelected.put((int) p.getId(), 1);
                }

                ((GridAdapter) grdProductos.getAdapter()).notifyDataSetChanged();

                if (p.isWeighable())
                    Toast.makeText(getActivity(), "Pesable!!", Toast.LENGTH_SHORT).show();
                //iniciarVistaProductoPesable(p, MODO_PESABLE.PRODUCTO);

                if (listener != null) {
                    listener.onProductoSeleccionado(p);
                }
            }
        });

        startView();
    }

    private void changeListProdsVisibility(boolean visible) {

        if (visible) {
            lstProductos.setVisibility(View.VISIBLE);
            getView().findViewById(R.id.frag_prod_prop_lay_container).setVisibility(View.VISIBLE);
            getView().findViewById(R.id.frag_prod_prop_lay_d_container).setVisibility(View.GONE);
        } else {
            lstProductos.setVisibility(View.GONE);
            getView().findViewById(R.id.frag_prod_prop_lay_container).setVisibility(View.GONE);
            getView().findViewById(R.id.frag_prod_prop_lay_d_container).setVisibility(View.VISIBLE);
        }
    }

    private void startView() {
        if (mostrarSoloProductosPropios) {
            //comportamiento alternativo: no mostramos deptos/subdeptos, solo una lista con los productos propios
            //cargamos los productos propios
            //lstProductos.setAdapter(new ProductArrayAdapter(getActivity(), getProductsList(-1, -1)));
        } else {

            /*if (!controlExternoListView) {
                lastDepId = this.MAS_VENDIDOS.getId();
                lastSubDepId = -1;
                new LoadProductsTask().execute(PAGE_SIZE);
            }*/
        }

        //avisamos al activity que está listo el fragment
        if (frg_listener != null)
            frg_listener.onFragmentAttached();
    }

    private View.OnClickListener btnSubdeptoClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String subdeptoSelected = String.valueOf(v.getTag());

            SubDepartment sd = getSubdepartmentByCode(subdeptoSelected);

            if (sd != null) {
                Toast.makeText(getActivity(), "Subdepto: " + subdeptoSelected, Toast.LENGTH_SHORT).show();

                //cargamos los productos del subdepartamento
                if (sd.isVistaLista()) {
                    esVistaGrilla = false;
                    //lstProductos.setAdapter(new ProductArrayAdapter(getActivity(), getProductsList(-1, d.getId())));
                    //loadProductosList(d);
                    lastDepId = -1;
                    lastSubDepId = sd.getId();
                    new LoadProductsTask().execute(PAGE_SIZE);

                    grdProductos.setVisibility(View.GONE);
                }
                if (sd.isVistaGrilla()) {
                    esVistaGrilla = true;
                    //grdProductos.setAdapter(new GridAdapter(getActivity(), getProductsList(-1, d.getId())));
                    loadProductosGrid(sd);
                    lstProductos.setVisibility(View.GONE);
                }

            } else {
                Toast.makeText(getActivity(), "Unknown subdepto", Toast.LENGTH_SHORT).show();
            }
        }
    };

    private View.OnClickListener btnDeptoClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String deptoSelected = String.valueOf(v.getTag());

            Department d = getDepartmentByCode(deptoSelected);
            if (d != null) {
                Toast.makeText(getActivity(), "Depto: " + deptoSelected, Toast.LENGTH_SHORT).show();


                if (d.isVistaLista()) {
                    esVistaGrilla = false;
                    //lstProductos.setAdapter(new ProductArrayAdapter(getActivity(), getProductsList(d.getId(), -1)));
                    //loadProductosList(d);
                    lastDepId = d.getId();
                    lastSubDepId = -1;
                    new LoadProductsTask().execute(PAGE_SIZE);

                    grdProductos.setVisibility(View.GONE);
                }
                if (d.isVistaGrilla()) {
                    esVistaGrilla = true;
                    //grdProductos.setAdapter(new GridAdapter(getActivity(), getProductsList(d.getId(), -1)));
                    loadProductosGrid(d);
                    lstProductos.setVisibility(View.GONE);
                }

            } else {
                Toast.makeText(getActivity(), "Unknown depto", Toast.LENGTH_SHORT).show();
            }

        }
    };

    private long lastDepId = -1, lastSubDepId = -1;

    private void loadProductosList(final Department d) {
        lastDepId = d.getId();

        Thread t = new Thread() {
            @Override
            public void run() {

                final List<Product> productList = getProductsList(d.getId(), -1, 10);

                getActivity().runOnUiThread(
                        new Runnable() {
                            @Override
                            public void run() {
                                lstProductos.setAdapter(new ProductArrayAdapter(getActivity(), productList));
                            }
                        }
                );
            }
        };
        t.start();
    }

    private void loadProductosGrid(final Department d) {
        Thread t = new Thread() {
            @Override
            public void run() {

                final List<Product> productList = getProductsList(d.getId(), -1);

                getActivity().runOnUiThread(
                        new Runnable() {
                            @Override
                            public void run() {
                                grdProductos.setAdapter(new GridAdapter(getActivity(), productList));
                            }
                        }
                );
            }
        };
        t.start();
    }

    private void loadProductosList(final SubDepartment s) {
        Thread t = new Thread() {
            @Override
            public void run() {

                final List<Product> productList = getProductsList(-1, s.getId());

                getActivity().runOnUiThread(
                        new Runnable() {
                            @Override
                            public void run() {
                                lstProductos.setAdapter(new ProductArrayAdapter(getActivity(), productList));
                            }
                        }
                );
            }
        };
        t.start();
    }

    private void loadProductosGrid(final SubDepartment s) {
        Thread t = new Thread() {
            @Override
            public void run() {

                final List<Product> productList = getProductsList(-1, s.getId());

                getActivity().runOnUiThread(
                        new Runnable() {
                            @Override
                            public void run() {
                                grdProductos.setAdapter(new GridAdapter(getActivity(), productList));
                            }
                        }
                );
            }
        };
        t.start();
    }


    /*private void iniciarVistaProductoPesable(Product p, MODO_PESABLE modo) {
        //este comportamiento, aplica solo para modalidad VENTAS
        if (comportamientoVenta) {

            modoActivo = modo;//seteamos el modo, para luego restaurarlo

            if (modo == MODO_PESABLE.DEPARTAMENTO_GENERICO) {
                //cargamos el header de departamentos
                vwDeptoHeader.setVisibility(View.VISIBLE);
                lblDepartamentos.setText(p.getName());
                lstDepartamentos.setVisibility(View.GONE);
            }

            if (modo == MODO_PESABLE.SUBDEPARTMENTO_GENERICO) {

                //cargamos el header de subdepartamentos
                vwSubdeptoHeader.setVisibility(View.VISIBLE);
                lblSubdepartamentos.setText(p.getName());
                lstSubdepartamentos.setVisibility(View.GONE);

                //por las dudas, voy a volver a ocultar la lista de departamentos...
                //puede pasar que esté visible porque se tocó de nuevo, luego de la seleccion de subdeptos
                lstDepartamentos.setVisibility(View.GONE);
            }

            if (modo == MODO_PESABLE.PRODUCTO) {
                //cargamos el header de producto (dado que es pesable)
                vwProductHeader.setVisibility(View.VISIBLE);
                lblProductos.setText(p.getName());
                //ocultamos grilla o lista
                lstProductos.setVisibility(View.GONE);
                grdProductos.setVisibility(View.GONE);

                //por las dudas, voy a volver a ocultar las listas de departamentos|subdepartamentos...
                //puede pasar que esté visible porque usé directo el producto pesable desde MAS VENDIDOS
                lstDepartamentos.setVisibility(View.GONE);
                //puede pasar que está visible porque no filtre por un subdepto
                lstSubdepartamentos.setVisibility(View.GONE);
            }
        }
    }*/

    /*private void finalizarVistaProductoPesable() {
        if (modoActivo == MODO_PESABLE.DEPARTAMENTO_GENERICO) {
            lblDepartamentos.performClick();
        }
        if (modoActivo == MODO_PESABLE.SUBDEPARTMENTO_GENERICO) {
            lblSubdepartamentos.performClick();

            //volvemos a mostrar la listas de deptos (la habíamo ocultado por las dudas...)
            //si está GONE el header, es porque no estaba cerrada la lista
            if (vwDeptoHeader.getVisibility() == View.GONE)
                lstDepartamentos.setVisibility(View.VISIBLE);
        }
        if (modoActivo == MODO_PESABLE.PRODUCTO) {
            lblProductos.performClick();

            //volvemos a mostrar las listas de deptos|subdeptos (las habíamos ocultado por las dudas...)
            //si está GONE el header, es porque no estaba cerrada la lista
            if (vwDeptoHeader.getVisibility() == View.GONE)
                lstDepartamentos.setVisibility(View.VISIBLE);
            //si está GONE el header, es porque no estaba cerrada la lista
            if (vwSubdeptoHeader.getVisibility() == View.GONE)
                lstSubdepartamentos.setVisibility(View.VISIBLE);
        }
    }*/


    private Department getDepartmentByCode(String code) {

        //String condicion = "exists (select 1 from products where department_id = departments._id and removed = 0 and generic = 0)";
        Department d = new DepartmentDao(this.getActivity().getContentResolver()).first(DepartmentTable.COLUMN_CODIGO + " = '" + code + "'", null, null);

        return d;
    }

    private SubDepartment getSubdepartmentByCode(String code) {

        //String condicion = "exists (select 1 from products where subdepartmennt_id = subdepartments._id and removed = 0 and generic = 0) and " + String.format("department_id = %d", idDepartment);
        SubDepartment sd = new SubDepartmentDao(this.getActivity().getContentResolver()).first(SubdepartmentTable.COLUMN_CODIGO + " = '" + code + "'", null, null);

        return sd;
    }

    private List<Product> getProductsList(long idDepartment, long idSubDepartment) {
        return getProductsList(idDepartment, idSubDepartment, 0);
    }

    private List<Product> getProductsList(long idDepartment, long idSubDepartment, int top) {

        String cond_extra = mostrarProdSinPrecio ? "" : " and sale_price <> 0";

        //para que no figuren los combos
        cond_extra += mostrarSoloProductosPropios ? " and code not like '%-%' and autom = " + String.valueOf(Product.TYPE_AUTOM.PROD_ALTA_SIN_SKU) : "";
        //cond_extra += mostrarSoloProductosCatalogo ? " and autom <> " + String.valueOf(Product.TYPE_AUTOM.PROD_ALTA_SIN_SKU) : "";

        if (idDepartment != -1) {

            String cond = String.format("department_id = %d AND removed = 0 and generic = 0", idDepartment) + cond_extra;

            if(top > 0)
                return new ProductDao(this.getActivity().getContentResolver()).list(cond, null, ProductTable.COLUMN_NAME, "", "", String.valueOf(top));
            else
                return new ProductDao(this.getActivity().getContentResolver()).list(cond, null, ProductTable.COLUMN_NAME);
        }
        if (idSubDepartment != -1) {

            String cond = String.format("subdepartmennt_id = %d AND removed = 0 and generic = 0", idSubDepartment) + cond_extra;

            if(top > 0)
                return new ProductDao(this.getActivity().getContentResolver()).list(cond, null, ProductTable.COLUMN_NAME, "", "", String.valueOf(top));
            else
                return new ProductDao(this.getActivity().getContentResolver()).list(cond, null, ProductTable.COLUMN_NAME);
        }

        return new ProductDao(this.getActivity().getContentResolver()).list("removed = 0" + cond_extra, null, ProductTable.COLUMN_NAME, "", "", String.valueOf(top));
    }

    public class ProductArrayAdapter extends ArrayAdapter<Product> {
        private final Context context;
        private List<Product> products;

        public ProductArrayAdapter(Context context, List<Product> products) {
            super(context, R.layout.list_item_prod, products);
            this.context = context;
            this.products = products;

            changeListProdsVisibility(this.products.size() > 0);
            //lstProductos.setVisibility(this.products.size() > 0 ? View.VISIBLE : View.INVISIBLE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(context)
                        .inflate(R.layout.list_item_prod, parent, false);
            }

            //if (position % 2 != 0)
            //    rowView.setBackgroundColor(Color.LTGRAY);
            //else
            //rowView.setBackgroundColor(Color.WHITE);

            Product p = products.get(position);

            TextView nameTextView = (TextView) convertView.findViewById(R.id.lblListItem);
            nameTextView.setText(p.getName());

            //ImageView imagen = (ImageView) convertView.findViewById(R.id.imgListItem);
            //imagen.setImageResource(R.drawable.objects_icon);

            TextView priceTextView = (TextView) convertView.findViewById(R.id.lblListItemPrice);
            priceTextView.setText(Formatos.FormateaDecimal(p.getSalePrice(), formato, moneda));

            //ajustes visuales...
            if (formato == Formatos.DecimalFormat) {
                priceTextView.setTextSize(20);
            } else if (formato == Formatos.DecimalFormat_PE) {
                priceTextView.setTextSize(18);
            } else {
                priceTextView.setTextSize(25);
            }

            if (priceTextView.getText().length() > 5) {
                priceTextView.setTextSize(18);
            } else {
                priceTextView.setTextSize(20);
            }

            TextView stockTextView = (TextView) convertView.findViewById(R.id.lblListItemStock);
            stockTextView.setText(Formatos.FormateaDecimal(p.getStock(), Formatos.DecimalFormat_US));

            if (p.getStock() <= p.getMinStock())
                stockTextView.setTextColor(getResources().getColor(R.color.boton_rojo));
            else
                stockTextView.setTextColor(getResources().getColor(R.color.texto_black));

            if (comportamientoVenta) {
                TextView cantTextView = (TextView) convertView.findViewById(R.id.lblListItemCant);
                cantTextView.setVisibility(View.VISIBLE);
                cantTextView.setText(Formatos.FormateaDecimal(
                        productosSelected.containsKey((int) p.getId()) ? productosSelected.get((int) p.getId()) : 0
                        , Formatos.DecimalFormat_US));
            }

            if (convertView.isSelected() || productosSelected.containsKey((int) p.getId()))
                convertView.setBackgroundResource(R.drawable.azul);
            else
                convertView.setBackgroundResource(R.drawable.bg_normal);

            return convertView;
        }

    }

    public class GridAdapter extends BaseAdapter {
        private final Context context;
        private List<Product> products;

        public GridAdapter(Context context, List<Product> products) {
            this.context = context;
            this.products = products;

            grdProductos.setVisibility(this.products.size() > 0 ? View.VISIBLE : View.INVISIBLE);
        }

        @Override
        public int getCount() {
            return products.size();
        }

        @Override
        public Object getItem(int position) {
            return products.get(position);
        }

        @Override
        public long getItemId(int position) {
            return products.get(position).getId();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(context)
                        .inflate(R.layout.grid_item_prod, parent, false);
            }

            Product p = products.get(position);

            TextView nameTextView = (TextView) convertView.findViewById(R.id.lblListItem);
            ImageView imagen = (ImageView) convertView.findViewById(R.id.imgListItem);

            Uri uri = UriUtils.resolveUri(ProductosPropiosFragment.this.getActivity(), p.getImage_uri());
            imagen.setImageURI(uri);

            //verificamos si se encontró una imagen o no
            if (imagen.getDrawable() != null) {
                nameTextView.setVisibility(View.GONE);
                imagen.setVisibility(View.VISIBLE);
                //imagen.setImageURI(uri);
                //imagen.setImageResource(R.drawable.objects_icon);
            } else {
                nameTextView.setVisibility(View.VISIBLE);
                nameTextView.setText(p.getName());
                imagen.setVisibility(View.GONE);
            }

            if (convertView.isSelected() || productosSelected.containsKey((int) p.getId()))
                convertView.setBackgroundResource(R.drawable.azul);
            else
                convertView.setBackgroundResource(R.drawable.bg_normal);

            return convertView;
        }


    }


    class LoadProductsTask extends AsyncTask<Integer, CharSequence, String> {
        private int position = -1;

        public LoadProductsTask() {

        }

        public LoadProductsTask(int _position) {
            position = _position;
        }

        private ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            loading = true;

            //iniciamos un mensaje para el usuario
            dialog = new ProgressDialog(getActivity(), R.style.ProgressTransparent);
            dialog.setMessage("");//"Aguarde por favor...");
            dialog.setIndeterminate(false);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            loading = false;

            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
        }

        @Override
        protected String doInBackground(Integer... params) {

            final List<Product> productList = getProductsList(lastDepId, lastSubDepId, params[0]);

            getActivity().runOnUiThread(
                    new Runnable() {
                        @Override
                        public void run() {
                            lstProductos.setAdapter(new ProductArrayAdapter(getActivity(), productList));
                            //endlessScrollListener.reset();
                            lstProductos.setSelection(position);
                        }
                    }
            );

            return null;
        }
    }

    int PAGE_SIZE = 50;

    //listener para scrollear y traer mas datos al final de la lista
    /*class EndlessScrollListener implements AbsListView.OnScrollListener {
        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            //what is the bottom iten that is visible
            int lastInScreen = firstVisibleItem + visibleItemCount;
            //is the bottom item visible & not loading more already ? Load more !
            if((lastInScreen == totalItemCount) && !(loadingMore)){
                //Thread thread =  new Thread(null, loadMoreListItems);
                //thread.start();

                new LoadProductsTask(lastScrollPosition).execute((currentPage + 1 ) * PAGE_SIZE);
            }

        }
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }
    }*/

    private boolean loading = false;

    class EndlessScrollListener implements AbsListView.OnScrollListener {

        private int visibleThreshold = 5;
        private int currentPage = 0;
        private int previousTotal = 0;
        //private boolean loading = true;

        private int lastScrollPosition = 0;

        public EndlessScrollListener() {
        }
        /*public EndlessScrollListener(int visibleThreshold) {
            this.visibleThreshold = visibleThreshold;
        }

        public void reset(){
            currentPage = 0;
            previousTotal = 0;
            loading = true;
            lastScrollPosition = 0;
        }*/

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            int lastInScreen = firstVisibleItem + visibleItemCount;
            if (totalItemCount > 0 && lastInScreen == totalItemCount && totalItemCount % PAGE_SIZE == 0 && !loading) {
                //ultimo => cargamos
                new LoadProductsTask(lastInScreen - 1).execute(totalItemCount + PAGE_SIZE);

            }

            /*if (loading && totalItemCount > previousTotal) {
                loading = false;
                previousTotal = totalItemCount;
                currentPage++;
            }

            boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount-1;

            if(loadMore && !loading) {
                lastScrollPosition = firstVisibleItem;
                new LoadProductsTask(lastScrollPosition).execute((currentPage + 1 ) * PAGE_SIZE);
                loading = true;
            }*/
        }

        //@Override
        public void onScroll_(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (loading) {
                if (totalItemCount > previousTotal) {
                    loading = false;
                    previousTotal = totalItemCount;
                    currentPage++;

                    //view.setSelection(lastScrollPosition);//TODO: tratar de que no se vuelva al comienzo
                }
            }
            if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
                // I load the next page of gigs using a background task,
                // but you can call any function here.
                lastScrollPosition = firstVisibleItem;

                new LoadProductsTask(lastScrollPosition).execute((currentPage + 1) * PAGE_SIZE);
                loading = true;
            }
        }

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }
    }
}
