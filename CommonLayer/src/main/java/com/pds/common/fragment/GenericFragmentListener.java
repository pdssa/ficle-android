package com.pds.common.fragment;

/**
 * Created by Hernan on 20/07/2015.
 */
public interface GenericFragmentListener {
    void onFragmentAttached();
}
