package com.pds.common.fragment;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.pds.common.R;


public class PadFragment extends Fragment {

    private Button btnCantidad;
//    private Button btnPrecio;
    private Button btnEnter;
    private ImageButton btnClear;
    private Button btn0;
    private Button btn1;
    private Button btn2;
    private Button btn3;
    private Button btn4;
    private Button btn5;
    private Button btn6;
    private Button btn7;
    private Button btn8;
    private Button btn9;
    private Button btnPunto;

    private char decimalPoint;

    public static enum FnButton {
        FUNCTION_1, //CANTIDAD
        FUNCTION_2, //PRECIO
        FUNCTION_3, //BACKSPACE
        FUNCTION_4 //ENTER
    }

    public interface PadListener {
        void onNumberPressed(String numberPressed);

        void onFunction1Pressed();

        void onFunction2Pressed();

        void onFunction3Pressed();

        void onFunction4Pressed();
    }

    PadListener listener;

    public void setPadListener(PadListener listener) {
        this.listener = listener;
    }

    public void setFunctionConfig(FnButton button, String text) {
        setFunctionConfig(button, text, true, R.drawable.botones_teclado);
    }

    public void setFunctionConfig(FnButton button, String text, boolean enabled) {
        setFunctionConfig(button, text, enabled, R.drawable.botones_teclado);
    }

    public void setFunctionConfig(FnButton button, String text, boolean enabled, int backgroundResource) {
        setFunctionConfig(button, text,  enabled,  backgroundResource, button == FnButton.FUNCTION_4 ? 40 : 30);
    }

    public void setFunctionConfig(FnButton button, String text, boolean enabled, int backgroundResource, int textSize) {
        Button _button = null;

        if (button == FnButton.FUNCTION_1)
            _button = btnCantidad;
//        if (button == FnButton.FUNCTION_2)
//            _button = btnPrecio;
        if (button == FnButton.FUNCTION_3)
            _button = null; //el backspace es un ImageButton
        if (button == FnButton.FUNCTION_4)
            _button = btnEnter;

        if (_button != null) {
            _button.setText(text);
            _button.setEnabled(enabled);
            _button.setBackgroundResource(backgroundResource);
            _button.setTextSize(textSize);
        }
    }

    public char getDecimalPoint() {
        return decimalPoint;
    }

    public void setDecimalPoint(char decimalPoint) {
        this.decimalPoint = decimalPoint;
    }

    public PadFragment() {
        // Required empty public constructor
        this.decimalPoint = '.';
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pad, container, false);

        // Inicializamos vistas
        InitViews(view);

        // Inicializamos eventos
        InitEvents();

        return view;
    }

    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);

    }

    private void InitViews(View view) {
        //controles del pad
        this.btnCantidad = (Button) view.findViewById(R.id.pad_btnCantidad);
//        this.btnPrecio = (Button) view.findViewById(R.id.pad_btnPrecio);
        this.btnEnter = (Button) view.findViewById(R.id.pad_btnEnter);
        this.btnClear = (ImageButton) view.findViewById(R.id.pad_btnClear);
        this.btn0 = (Button) view.findViewById(R.id.pad_btnNro0);
        this.btn1 = (Button) view.findViewById(R.id.pad_btnNro1);
        this.btn2 = (Button) view.findViewById(R.id.pad_btnNro2);
        this.btn3 = (Button) view.findViewById(R.id.pad_btnNro3);
        this.btn4 = (Button) view.findViewById(R.id.pad_btnNro4);
        this.btn5 = (Button) view.findViewById(R.id.pad_btnNro5);
        this.btn6 = (Button) view.findViewById(R.id.pad_btnNro6);
        this.btn7 = (Button) view.findViewById(R.id.pad_btnNro7);
        this.btn8 = (Button) view.findViewById(R.id.pad_btnNro8);
        this.btn9 = (Button) view.findViewById(R.id.pad_btnNro9);
        this.btnPunto = (Button) view.findViewById(R.id.pad_btnPunto);
    }

    private void InitEvents() {
        this.btn0.setOnClickListener(numberClickListener);
        this.btn1.setOnClickListener(numberClickListener);
        this.btn2.setOnClickListener(numberClickListener);
        this.btn3.setOnClickListener(numberClickListener);
        this.btn4.setOnClickListener(numberClickListener);
        this.btn5.setOnClickListener(numberClickListener);
        this.btn6.setOnClickListener(numberClickListener);
        this.btn7.setOnClickListener(numberClickListener);
        this.btn8.setOnClickListener(numberClickListener);
        this.btn9.setOnClickListener(numberClickListener);
        this.btnPunto.setOnClickListener(numberClickListener);
        this.btnCantidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null)
                    listener.onFunction1Pressed();
            }
        });
//        this.btnPrecio.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (listener != null)
//                    listener.onFunction2Pressed();
//            }
//        });
        this.btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null)
                    listener.onFunction3Pressed();
            }
        });
        this.btnEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null)
                    listener.onFunction4Pressed();
            }
        });
    }

    private View.OnClickListener numberClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            String _char = ((Button) view).getText().toString();

            //si es el punto, tomamos el punto decimal
            if(view.getId() == btnPunto.getId())
                _char = String.valueOf(getDecimalPoint());

            if (listener != null) {
                listener.onNumberPressed(_char);
            }
        }
    };

}




