package com.pds.common.fragment;

import com.pds.common.model.Combo;
import com.pds.common.model.Product;

/**
 * Created by Hernan on 05/12/2016.
 */
public interface ProductoFragmentListener {
    void onProductoSeleccionado(Product p);

//    void onComboSeleccionado(Combo c);

    void onFragmentProductoClose();
}
