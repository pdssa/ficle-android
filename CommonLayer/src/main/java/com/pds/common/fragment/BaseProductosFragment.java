package com.pds.common.fragment;

import android.app.Fragment;

import java.text.DecimalFormat;
import java.util.HashMap;

/**
 * Created by Hernan on 05/12/2016.
 */
public abstract class BaseProductosFragment extends Fragment{
    protected ProductoFragmentListener listener;

    public void setProductoListener(ProductoFragmentListener listener) {
        this.listener = listener;
    }

}
