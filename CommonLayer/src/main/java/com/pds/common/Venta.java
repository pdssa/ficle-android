package com.pds.common;

/**
 * Created by Hernan on 05/12/13.
 */
public class Venta {

    public static enum TipoMedioPago {
        EFECTIVO, //0
        CHEQUE, //1
        TARJETACREDITO, //2
        TARJETADEBITO //3
    }

    private int id;
    private double total;
    private int cantidad;
    private String fecha;
    private String hora;
    private String codigo;
    private int userid;
    private String medio_pago;

    public void setId(int id) {
        this.id = id;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public int getId() {
        return id;
    }

    public double getTotal() {
        return total;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getFecha() {
        return fecha;
    }

    public String getHora() {
        return hora;
    }

    public String getFechaHoraCorta() {
        return fecha.substring(0, 6) + fecha.substring(8) + " " + hora.substring(0, 5);
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getCantidad() {
        return cantidad;
    }

    public int getUserid() {
        return userid;
    }

    public String getMedio_pago() {
        return medio_pago;
    }

    public void setMedio_pago(String medio_pago) {
        this.medio_pago = medio_pago;
    }

    public String getNroVenta() {
        String nroVenta = "00000000" + String.valueOf(this.id);
        return nroVenta.substring(nroVenta.length() - 8);
    }

    public Venta(int id, double total, int cantidad, String fecha, String hora, String codigo, int userid, String medio_pago) {
        this.id = id;
        this.fecha = fecha;
        this.hora = hora;
        this.total = total;
        this.cantidad = cantidad;
        this.codigo = codigo;
        this.userid = userid;
        this.medio_pago = medio_pago;
    }

}
