package com.pds.common;

import com.pds.common.db.ComprobanteTable;

/**
 * Created by Hernan on 22/11/13.
 */
public class VentasHelper {

    //************TABLE VENTAS************
    public static final String TABLE_NAME = "ventas";
    public static final String VW_VENTAS_DIARIAS_APP = "vw_ventas_dia_app";
    public static final String VW_VENTAS_DIARIAS_VDDOR = "vw_ventas_dia_vddor";
    public static final String VW_VENTAS_MES_APP = "vw_ventas_mes_app";
    public static final String VW_VENTAS_MES_VDDOR = "vw_ventas_mes_vddor";

    //columns names
    public static final String VENTA_ID = "_id";
    public static final String VENTA_TOTAL = "total";
    public static final String VENTA_CANTIDAD = "cantidad";
    public static final String VENTA_FECHA = "fecha";
    public static final String VENTA_HORA = "hora";
    public static final String VENTA_MEDIO_PAGO = "medio_pago";
    public static final String VENTA_CODIGO = "codigo";
    public static final String VENTA_USERID = "userid";
    public static final String VENTA_VW_CANTIDAD = "cantidad"; //se usa en la vista
    public static final String VENTA_VW_TOTAL = "total";
    public static final String VENTA_VW_USER = "user";
    public static final String VENTA_SYNC_STATUS = "sync";
    public static final String VENTA_FC_TYPE = "fc_type";
    public static final String VENTA_FC_ID = "fc_id";
    public static final String VENTA_FC_FOLIO = "fc_folio";
    public static final String VENTA_FC_VALIDATION = "fc_v";
    public static final String VENTA_NETO = "neto";
    public static final String VENTA_IVA = "iva";
    public static final String VENTA_TIMBRE = "timbre";
    public static final String VENTA_ANULADA = "anulada";


    //columns indexes
    public static final int VENTA_IX_ID = 0;
    public static final int VENTA_IX_TOTAL = 1;
    public static final int VENTA_IX_CANTIDAD = 2;
    public static final int VENTA_IX_FECHA = 3;
    public static final int VENTA_IX_HORA = 4;
    public static final int VENTA_IX_MEDIO_PAGO = 5;
    public static final int VENTA_IX_CODIGO = 6;
    public static final int VENTA_IX_USERID = 7;
    public static final int VENTA_IX_FC_TYPE = 8;
    public static final int VENTA_IX_FC_ID = 9;
    public static final int VENTA_IX_FC_FOLIO = 10;
    public static final int VENTA_IX_FC_VALIDATION = 11;
    public static final int VENTA_IX_NETO = 12;
    public static final int VENTA_IX_IVA = 13;
    public static final int VENTA_IX_SYNC_STATUS = 14;
    public static final int VENTA_IX_TIMBRE = 15;
    public static final int VENTA_IX_ANULADA = 16;



    //indexes of view (dia / aplicacion)
    public static final int VENTA_IX_VW_FECHA = 0;
    public static final int VENTA_IX_VW_CODIGO = 1;
    public static final int VENTA_IX_VW_CANTIDAD = 2;
    public static final int VENTA_IX_VW_TOTAL = 3;

    //indexes of view (dia / vendedor)
    public static final int VENTA_IX_VW_V_FECHA = 0;
    public static final int VENTA_IX_VW_V_USER = 1;
    public static final int VENTA_IX_VW_V_CANTIDAD = 2;
    public static final int VENTA_IX_VW_V_TOTAL = 3;

    // script de creacion de tabla
    public static final String VENTA_CREATE_SCRIPT = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
            VENTA_ID + " integer primary key autoincrement, " +
            VENTA_TOTAL + " real, " +
            VENTA_CANTIDAD + " int," +
            VENTA_FECHA + " text, " +
            VENTA_HORA + " text, " +
            VENTA_MEDIO_PAGO + " text, " +
            VENTA_CODIGO + " text, " +
            VENTA_USERID + " int, " +
            VENTA_FC_TYPE + " text, " +
            VENTA_FC_ID + " int, " +
            VENTA_FC_FOLIO + " text, " +
            VENTA_FC_VALIDATION + " text, " +
            VENTA_NETO + " real, " +
            VENTA_IVA + " real, " +
            VENTA_SYNC_STATUS + " text default 'N'," +
            VENTA_TIMBRE + " text," +
            VENTA_ANULADA + " int default 0"+
            ");";

    // creamos la vista de ventas por dia / aplicacion
    public static final String VENTA_VW_DIA_APP_CREATE_SCRIPT = "CREATE VIEW IF NOT EXISTS " + VW_VENTAS_DIARIAS_APP + " AS " +
            "SELECT " + VENTA_FECHA + ", " + VENTA_CODIGO + ", count(*) AS " + VENTA_VW_CANTIDAD + ", sum(" + VENTA_TOTAL + ") AS " + VENTA_VW_TOTAL +
            " FROM ( select _id, total, cantidad, fecha, hora, medio_pago, codigo, userid from " + TABLE_NAME + " UNION select _id, total, cantidad, fecha, hora, medio_pago, codigo, userid from " + ComprobanteTable.TABLE_NAME + " ) " +
            " GROUP BY " + VENTA_FECHA + ", " + VENTA_CODIGO;

    // creamos la vista de ventas por dia / vendedor
    public static final String VENTA_VW_DIA_VDDOR_CREATE_SCRIPT = "CREATE VIEW IF NOT EXISTS " + VW_VENTAS_DIARIAS_VDDOR + " AS " +
            "SELECT " + VENTA_FECHA + ", " + UsuarioHelper.USUARIOS_NOMBRE + " AS " + VENTA_VW_USER + ", count(*) AS " + VENTA_VW_CANTIDAD + ", sum(" + VENTA_TOTAL + ") AS " + VENTA_VW_TOTAL +
            " FROM ( select _id, total, cantidad, fecha, hora, medio_pago, codigo, userid from " + TABLE_NAME + " UNION select _id, total, cantidad, fecha, hora, medio_pago, codigo, userid from " + ComprobanteTable.TABLE_NAME + " ) " +
            " INNER JOIN " + UsuarioHelper.TABLE_NAME + " ON " + UsuarioHelper.TABLE_NAME + "." + UsuarioHelper.USUARIOS_ID + "=" + VENTA_USERID +
            " GROUP BY " + VENTA_FECHA + ", " + UsuarioHelper.USUARIOS_NOMBRE;

    // creamos la vista de ventas por mes / aplicacion
    public static final String VENTA_VW_MES_APP_CREATE_SCRIPT = "CREATE VIEW IF NOT EXISTS " + VW_VENTAS_MES_APP + " AS " +
            "SELECT substr(" + VENTA_FECHA + ",4) as " + VENTA_FECHA + ", " + VENTA_CODIGO + ", count(*) AS " + VENTA_VW_CANTIDAD + ", sum(" + VENTA_TOTAL + ") AS " + VENTA_VW_TOTAL +
            " FROM ( select _id, total, cantidad, fecha, hora, medio_pago, codigo, userid from " + TABLE_NAME + " UNION select _id, total, cantidad, fecha, hora, medio_pago, codigo, userid from " + ComprobanteTable.TABLE_NAME + " ) " +
            " GROUP BY substr(" + VENTA_FECHA + ",4) , " + VENTA_CODIGO;

    // creamos la vista de ventas por mes / vendedor
    public static final String VENTA_VW_MES_VDDOR_CREATE_SCRIPT = "CREATE VIEW IF NOT EXISTS " + VW_VENTAS_MES_VDDOR + " AS " +
            "SELECT substr(" + VENTA_FECHA + ",4) as " + VENTA_FECHA + ", " + UsuarioHelper.USUARIOS_NOMBRE + " AS " + VENTA_VW_USER + ", count(*) AS " + VENTA_VW_CANTIDAD + ", sum(" + VENTA_TOTAL + ") AS " + VENTA_VW_TOTAL +
            " FROM ( select _id, total, cantidad, fecha, hora, medio_pago, codigo, userid from " + TABLE_NAME + " UNION select _id, total, cantidad, fecha, hora, medio_pago, codigo, userid from " + ComprobanteTable.TABLE_NAME + " ) " +
            " INNER JOIN " + UsuarioHelper.TABLE_NAME + " ON " + UsuarioHelper.TABLE_NAME + "." + UsuarioHelper.USUARIOS_ID + "=" + VENTA_USERID +
            " GROUP BY substr(" + VENTA_FECHA + ",4) , " + UsuarioHelper.USUARIOS_NOMBRE;

    //script de alter
    public static final String VENTA_ALTER_SYNC_STATUS_SCRIPT = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + VENTA_SYNC_STATUS + " text default 'N'";
    public static final String VENTA_ALTER_SYNC_STATUS_SCRIPT_2 = "UPDATE " + TABLE_NAME + " SET " + VENTA_SYNC_STATUS + " = 'N'";

    public static final String VENTA_ALTER_FC_1 = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + VENTA_FC_TYPE + " text";
    public static final String VENTA_ALTER_FC_2 = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + VENTA_FC_ID + " int";
    public static final String VENTA_ALTER_FC_3 = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + VENTA_FC_FOLIO + " text";
    public static final String VENTA_ALTER_FC_4 = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + VENTA_FC_VALIDATION + " text";

    public static final String VENTA_ALTER_NETO = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + VENTA_NETO + " real";
    public static final String VENTA_ALTER_IVA = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + VENTA_IVA + " real";

    public static final String VENTA_ALTER_TIMBRE = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + VENTA_TIMBRE + " text";

    public static final String VENTA_ALTER_ANULADA = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + VENTA_ANULADA + " int default 0";

    public static final String[] columnas = new String[]{
            VENTA_ID,
            VENTA_TOTAL,
            VENTA_CANTIDAD,
            VENTA_FECHA,
            VENTA_HORA,
            VENTA_MEDIO_PAGO,
            VENTA_CODIGO,
            VENTA_USERID,
            VENTA_FC_TYPE,
            VENTA_FC_ID,
            VENTA_FC_FOLIO,
            VENTA_FC_VALIDATION,
            VENTA_NETO,
            VENTA_IVA,
            VENTA_SYNC_STATUS,
            VENTA_TIMBRE,
            VENTA_ANULADA
    };

    public static final String[] columnas_vw_ventas_dia_app = new String[]{
            VENTA_FECHA,
            VENTA_CODIGO,
            VENTA_VW_CANTIDAD,
            VENTA_VW_TOTAL
    };

    public static final String[] columnas_vw_ventas_dia_vddor = new String[]{
            VENTA_FECHA,
            VENTA_VW_USER,
            VENTA_VW_CANTIDAD,
            VENTA_VW_TOTAL
    };

}
