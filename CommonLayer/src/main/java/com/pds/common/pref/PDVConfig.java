package com.pds.common.pref;

import android.net.Uri;

/**
 * Created by Hernan on 17/12/2014.
 */
public abstract class PDVConfig {

    public final static String PROVIDER_NAME = "com.pds.ficle.pdv.provider.config";
    public final static String BASE_PATH = "config";
    public final static Uri CONTENT_PROVIDER = Uri.parse("content://" + PROVIDER_NAME + "/" + BASE_PATH);
    public final static String SHARED_PREF_NAME = "com.pds.ficle.pdv";

    public final static String PDV_MEDIO_DEFAULT = "medio_default";
    public final static String PDV_PIE_TICKET = "pie_ticket";
    public final static String PDV_AJUSTE_SENCILLO = "ajuste_sencillo";
    public final static String PDV_TIEMPO_CONFIRMACION = "tiempo_confirmacion";
    public final static String PDV_SOLICITAR_CHECKOUT = "solicitar_checkout";
    public final static String PDV_IMPRIMIR_AUTOMATICO = "impresion_automatica";
    public final static String PDV_MOSTRAR_DESGLOSE_IMPUESTOS = "mostrar_desglose_impuestos";
    public final static String PDV_MOSTRAR_PRODUCTOS_SIN_PRECIO = "mostrar_productos_sin_precio";
    public final static String PDV_EMITIR_VALE = "emision_vales";
    public final static String PDV_COMPROB_HABILIT = "comprobante_habilitado";
    public final static String PDV_ACTUALIZ_PRECIO_LISTA = "actualiza_precio_lista";
    public final static String PDV_LAST_USER_ID_VALE = "last_user_id_vale";
    public final static String PDV_IMPRESION_VALE_ERROR = "count_error_print_vale";

    public final static String[] PROJECTION = {
            PDV_MEDIO_DEFAULT,
            PDV_PIE_TICKET,
            PDV_AJUSTE_SENCILLO,
            PDV_TIEMPO_CONFIRMACION,
            PDV_SOLICITAR_CHECKOUT,
            PDV_IMPRIMIR_AUTOMATICO,
            PDV_MOSTRAR_DESGLOSE_IMPUESTOS,
            PDV_MOSTRAR_PRODUCTOS_SIN_PRECIO,
            PDV_EMITIR_VALE,
            PDV_COMPROB_HABILIT,
            PDV_ACTUALIZ_PRECIO_LISTA,
            PDV_IMPRESION_VALE_ERROR
    };
}
