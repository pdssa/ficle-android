package com.pds.common.pref;

import android.net.Uri;

/**
 * Created by Hernan on 17/12/2014.
 */
public abstract class ValeConfig {

    public final static String PROVIDER_NAME = "com.pds.ficle.vale.provider.config";
    public final static String BASE_PATH = "config";
    public final static Uri CONTENT_PROVIDER = Uri.parse("content://" + PROVIDER_NAME + "/" + BASE_PATH);
    public final static String SHARED_PREF_NAME = "com.pds.ficle.vale";

    public final static String VALE_ULT_CIERRE_Z = "ult_cierre_z";
    public final static String VALE_ULT_TRACE = "ult_trace_id";
    //public final static String VALE_SERVICIO_ACTIVADO = "servicio_activado";
    public final static String VALE_CIERRE_Z_REQUIRED = "is_cierre_z_required";

    public final static String[] PROJECTION = {
            VALE_ULT_CIERRE_Z,
            VALE_ULT_TRACE,
            VALE_CIERRE_Z_REQUIRED
    };
}
