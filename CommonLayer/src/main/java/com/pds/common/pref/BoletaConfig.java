package com.pds.common.pref;

import android.net.Uri;

/**
 * Created by Hernan on 17/12/2014.
 */
public abstract class BoletaConfig {

    public final static String PROVIDER_NAME = "com.pds.boletaelectronica.provider.config";
    public final static String BASE_PATH = "config";
    public final static Uri CONTENT_PROVIDER = Uri.parse("content://" + PROVIDER_NAME + "/" + BASE_PATH);
    public final static String SHARED_PREF_NAME = "com.pds.boletaelectronica";

    public final static String BOLETA_FM_HOST = "fm_host";
    public final static String BOLETA_FM_PORT = "fm_port";
    public final static String BOLETA_PDS_HOST = "pds_host";
    public final static String BOLETA_PDS_PORT = "pds_port";
    public final static String BOLETA_HORA_SYNC_1 = "hora_sync_1";
    public final static String BOLETA_HORA_SYNC_2 = "hora_sync_2";

    public final static String[] PROJECTION = {
            BOLETA_FM_HOST,
            BOLETA_FM_PORT,
            BOLETA_PDS_HOST,
            BOLETA_PDS_PORT,
            BOLETA_HORA_SYNC_1,
            BOLETA_HORA_SYNC_2
    };
}
