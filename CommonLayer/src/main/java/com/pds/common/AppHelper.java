package com.pds.common;

/**
 * Created by Hernan on 10/06/2014.
 */
public class AppHelper {

    //************TABLE APPS***************
    public static final String TABLE_NAME = "apps";

    //columns names
    public static final String APPS_ID = "_id";
    public static final String APPS_CODIGO = "codigo";
    public static final String APPS_NOMBRE = "nombre";
    public static final String APPS_PACKAGE = "package";
    public static final String APPS_ACTUALIZA = "actualiza";

    //columns indexes
    public static final int APPS_IX_ID = 0;
    public static final int APPS_IX_CODIGO = 1;
    public static final int APPS_IX_NOMBRE = 2;
    public static final int APPS_IX_PACKAGE = 3;
    public static final int APPS_IX_ACTUALIZA = 4;

    // script de creacion de tabla
    public static final String APPS_CREATE_SCRIPT = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
            APPS_ID + " integer primary key autoincrement, " +
            APPS_CODIGO + " text, " +
            APPS_NOMBRE + " text, " +
            APPS_PACKAGE + " text, " +
            APPS_ACTUALIZA + " text" +
            ");";

    //script
    private static final String APPS_INSERT_SCRIPT = "INSERT INTO " + TABLE_NAME +
            "(" + APPS_CODIGO + "," + APPS_NOMBRE + "," + APPS_PACKAGE + "," + APPS_ACTUALIZA + ") ";

    public static final String APPS_INIT_ALTA_SCRIPT = APPS_INSERT_SCRIPT +
            "SELECT " + "'ETP','ENTRY POINT','com.pds.ficle.ep','S' " +
            "UNION SELECT " + "'PDV','PDS PDV','com.pds.ficle.pdv','S' " +
            "UNION SELECT " + "'DSH','DASHBOARD','com.pds.ficle.dashboard','S' " +
            "UNION SELECT " + "'CDW','CASHDRAWER','com.pds.ficle.cashdrawer','S' " +
            "UNION SELECT " + "'PRO','GESTION PRODUCTOS','com.pds.ficle.gestprod','S' " +
            "UNION SELECT " + "'CHR','CONTROL HORARIO','com.pds.ficle.ctrlhora','S' ; ";

    public static final String[] columnas = new String[]{
            APPS_ID,
            APPS_CODIGO,
            APPS_NOMBRE,
            APPS_PACKAGE,
            APPS_ACTUALIZA
    };
}
