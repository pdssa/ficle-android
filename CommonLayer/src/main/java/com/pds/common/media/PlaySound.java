package com.pds.common.media;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;

/**
 * Created by Hernan on 28/08/2014.
 */
public class PlaySound {
    private MediaPlayer mMediaPlayer;
    private Context _context;

    public PlaySound(Context context){
        _context = context;
    }

    public void PlaySound(int soundResourceId) throws Exception {
        try {
            mMediaPlayer = MediaPlayer.create(_context, soundResourceId);
            mMediaPlayer.setLooping(false);

            mMediaPlayer.start();

            mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer arg0) {
                    if (mMediaPlayer != null) {
                        mMediaPlayer.release();
                        mMediaPlayer = null;
                    }
                }
            });
        } catch (Exception e) {
            Log.e("beep", "error: " + e.getMessage(), e);
            throw e;
        }
    }

}
