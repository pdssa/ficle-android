package com.pds.common;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Hernan on 14/02/14.
 */
public abstract class Formato {

    //private static final Locale locale = new Locale("es", "CL");
    //private static final Locale localDefault = Locale.US;
    //private static final Locale localPE = new Locale("es", "PE");

    public static final SimpleDateFormat DateFormat = new SimpleDateFormat("dd-MM-yyyy");
    public static final SimpleDateFormat MonthYearFormat = new SimpleDateFormat("MM-yyyy");
    public static final SimpleDateFormat YearMonthFormat = new SimpleDateFormat("yyyy-MM");
    public static final SimpleDateFormat ShortDateFormat = new SimpleDateFormat("dd-MM-yy");
    public static final SimpleDateFormat DayMonthFormat = new SimpleDateFormat("dd-MM");
    public static final SimpleDateFormat DbDateFormat = new SimpleDateFormat("yyyyMMdd");
    public static final SimpleDateFormat DbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final SimpleDateFormat DbDateTimeFormatTimeZone = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
    public static final SimpleDateFormat DbDate_Format = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat TimeFormat = new SimpleDateFormat("HH:mm:ss");
    public static final SimpleDateFormat ShortTimeFormat = new SimpleDateFormat("HH:mm");
    public static final SimpleDateFormat PDVDateTimeFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    public static final SimpleDateFormat FullDateTimeFormat = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
    public static final SimpleDateFormat FullDateTimeFormatNoSeconds = new SimpleDateFormat("dd-MM-yy HH:mm");
    public static final SimpleDateFormat ShortDateFormatNameOfDay = new SimpleDateFormat("EEE dd/MM", new Locale("es", "CL"));
    public static final SimpleDateFormat DateFormatSlash = new SimpleDateFormat("dd/MM/yyyy");
    public static final SimpleDateFormat DateFormatSlashAndTime = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    public static final SimpleDateFormat SIIDateTimeFormat = new SimpleDateFormat("yyMMddHHmmss");
    public static final SimpleDateFormat DateTwoDigFormatSlash = new SimpleDateFormat("dd/MM/yy");


    //+++++++++++++++++++++++DECIMAL++++++++++++++++++++++++++++++++++++


    public static final String CurrencySymbol = "$";
    public static final String CurrencySymbol_PE = "S/.";

    public static enum Decimal_Format {
        AR,
        US,
        CH,
        PE
    }


    public static double RedondeaDecimal(double valor) {
        return RedondeaDecimal(valor, Decimal_Format.CH);
    }

    public static double RedondeaDecimal(double valor, Decimal_Format format) {
        return Formato.ParseaDecimal(Formato.FormateaDecimal(valor, format), format);
    }

    public static String ParseaFormateaDecimal(String valor, Decimal_Format formatoOrigen, Decimal_Format formatoDestino) {
        double doubleParseado = ParseaDecimal(valor, formatoOrigen);

        return FormateaDecimal(doubleParseado, formatoDestino);
    }

    public static String ParseaFormateaDecimal(String valor, Decimal_Format formatoOrigen, Decimal_Format formatoDestino, String moneda) {
        return moneda + " " + ParseaFormateaDecimal(valor, formatoOrigen, formatoDestino);
    }

    public static String FormateaDecimal(String valor, Decimal_Format formato, String moneda) {
        return moneda + " " + FormateaDecimal(ParseaDecimal(valor), formato);
    }

    public static String FormateaDecimal(String valor, Decimal_Format formato) {
        return FormateaDecimal(ParseaDecimal(valor), formato);
    }

    public static String FormateaDecimal(double valor, Decimal_Format formato, String moneda) {
        return moneda + " " + FormateaDecimal(valor, formato);
    }

    public static String FormateaDecimal(double valor, Decimal_Format formato) {
        return PreparaFormato(formato).format(valor);
    }

    public static double ParseaDecimal(String valor) {
        return ParseaDecimal(valor, Decimal_Format.US);
    }

    public static double ParseaDecimal(String valor, Decimal_Format formato) {
        return ParseaDecimal(valor, formato, false);
    }


    public static double ParseaDecimal(String valor, Decimal_Format formato, boolean forceCast) {
        try {

            if(forceCast && formato.ordinal() == Decimal_Format.AR.ordinal()){
                //esta funcionalidad solo aplica para AR
                //los editText con inputType "numberDecimal" solo te permiten utilizar el punto decimal con "."
                //entonces, vamos a convertirlo a "," previo a parsear
                if(!valor.contains(",") && valor.contains(".")){
                    int ultimo_punto = valor.lastIndexOf(".");

                    //reemplazamos el ultimo punto, por una coma
                    if(ultimo_punto > -1){
                        valor = valor.substring(0,ultimo_punto) + "," + valor.substring(ultimo_punto + 1);
                    }
                }
            }

            return PreparaFormato(formato).parse(valor).doubleValue();
        } catch (ParseException e) {
            e.printStackTrace();
            return 0.0;
        }
    }

    private static DecimalFormat PreparaFormato(Decimal_Format formato) {
        Locale localDefault = Locale.US;

        if (formato == Decimal_Format.AR) {

            Locale locale = new Locale("es", "CL");

            DecimalFormat DecimalFormat_AR = (DecimalFormat) NumberFormat.getNumberInstance(locale);

            //AR (1.500,90)
            DecimalFormat_AR.setMinimumFractionDigits(2);
            DecimalFormat_AR.setMaximumFractionDigits(2);
            DecimalFormat_AR.setRoundingMode(RoundingMode.HALF_UP);

            return DecimalFormat_AR;
        } else if (formato == Decimal_Format.CH) {
            DecimalFormat DecimalFormat_CH = (DecimalFormat) NumberFormat.getNumberInstance(localDefault);

            //CH
            DecimalFormatSymbols CH_Symbols = new DecimalFormatSymbols(localDefault);
            CH_Symbols.setGroupingSeparator('.');
            CH_Symbols.setDecimalSeparator(',');//Fix kitkat parse..
            DecimalFormat_CH.setDecimalFormatSymbols(CH_Symbols);
            DecimalFormat_CH.setGroupingSize(3);
            DecimalFormat_CH.setMaximumFractionDigits(0);
            DecimalFormat_CH.setRoundingMode(RoundingMode.HALF_UP);

            return DecimalFormat_CH;
        } else if (formato == Decimal_Format.PE) {

            Locale localPE = new Locale("es", "PE");

            DecimalFormat DecimalFormat_PE = (DecimalFormat) NumberFormat.getNumberInstance(localPE);

            //PE (1,500.90)
            DecimalFormatSymbols PE_Symbols = new DecimalFormatSymbols(localDefault);
            PE_Symbols.setDecimalSeparator('.');
            PE_Symbols.setGroupingSeparator(',');
            DecimalFormat_PE.setDecimalFormatSymbols(PE_Symbols);
            DecimalFormat_PE.setGroupingSize(3);
            DecimalFormat_PE.setMinimumFractionDigits(2);
            DecimalFormat_PE.setMaximumFractionDigits(2);
            DecimalFormat_PE.setRoundingMode(RoundingMode.HALF_UP);
            DecimalFormat_PE.setCurrency(Currency.getInstance(localPE));

            return DecimalFormat_PE;
        } else {
            //Decimal_Format.US

            DecimalFormat DecimalFormat_US = (DecimalFormat) NumberFormat.getNumberInstance(localDefault);

            //US (Default)
            DecimalFormat_US.setGroupingSize(0);

            return DecimalFormat_US;
        }

    }


    public static boolean isNumeric(String s) {
        return s.matches("\\d+");
    }

    public static DecimalFormat getDecimalFormat(String codPais) {
            return PreparaFormato(getDecimal_Format(codPais));
    }

    public static Decimal_Format getDecimal_Format(String codPais) {
        if (codPais.equals("AR"))
            return Decimal_Format.AR;
        else if (codPais.equals("PE"))
            return Decimal_Format.PE;
        else
            return Decimal_Format.CH;
    }

    public static String getCurrencySymbol(String codPais) {
        if (codPais.equals("PE"))
            return Formato.CurrencySymbol_PE;
        else
            return Formato.CurrencySymbol;
    }

    public static String getCurrencySymbol(Decimal_Format formato) {
        if (formato == Decimal_Format.PE)
            return Formato.CurrencySymbol_PE;
        else
            return Formato.CurrencySymbol;
    }

    public static String getDecimalSymbol(Decimal_Format formato) {
        return String.valueOf(PreparaFormato(formato).getDecimalFormatSymbols().getDecimalSeparator());
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


    //++++++++++++++++++++++++++DATES++++++++++++++++++++++++++++++++++++++++++
    public static String FormateaDate(Date date, SimpleDateFormat formato) {
        return formato.format(date);
    }

    public static Date ObtieneDate(String date, SimpleDateFormat formato) throws Exception {
        return formato.parse(date);
    }

    public static Date DbDate(String date) throws Exception {
        return ObtieneDate(date, DbDateTimeFormat);
    }

    public static String DbDate(Date date) {
        return FormateaDate(date, DbDateTimeFormat);
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
}
