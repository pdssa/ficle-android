package com.pds.common;


import java.io.Serializable;

/**
 * Created by Hernan on 25/10/13.
 */
@SuppressWarnings("serial") //with this annotation we are going to hide compiler warning
public class Usuario implements Serializable {
    private int id;
    private String login;
    private String nombre;
    private String apellido;
    private String password;
    private int id_perfil;
    private String perfil;
    private String foto;
    private String celular;
    private String hora_entrada;
    private String hora_salida;
    private String entrada_de_hoy;
    private String salida_de_hoy;

    public String getEntrada_de_hoy() {
        return entrada_de_hoy != null ? !entrada_de_hoy.equals("") ? entrada_de_hoy.substring(0, 5) : "" : "";
    }

    public void setEntrada_de_hoy(String entrada_de_hoy) {
        this.entrada_de_hoy = entrada_de_hoy;
    }

    public String getSalida_de_hoy() {
        return salida_de_hoy != null ? !salida_de_hoy.equals("") ? salida_de_hoy.substring(0, 5) : "" : "";
    }

    public void setSalida_de_hoy(String salida_de_hoy) {
        this.salida_de_hoy = salida_de_hoy;
    }

    public String getHora_salida() {
        return hora_salida != null ? hora_salida : "";
    }

    public void setHora_salida(String hora_salida) {
        this.hora_salida = hora_salida;
    }

    public String getHora_entrada() {
        return hora_entrada != null ? hora_entrada : "";
    }

    public void setHora_entrada(String hora_entrada) {
        this.hora_entrada = hora_entrada;
    }


    public enum Perfiles {
        admin,
        vendedor
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getCelular() {
        return celular;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public void setId_perfil(int id_perfil) {
        this.id_perfil = id_perfil;
    }

    public int getId() {
        return id;
    }

    public int getId_perfil() {
        return id_perfil;
    }

    public String getApellido() {
        return apellido;
    }

    public String getFoto() {
        return foto;
    }

    public String getNombre() {
        return nombre;
    }

    public String getPassword() {
        return password;
    }

    public String getPerfil() {
        return PerfilUser.getPerfil(this.id_perfil);
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getLogin() {
        return login;
    }

    @Override
    public String toString() {
        if (this.apellido.isEmpty())
            return this.nombre + " (" + this.login + ")";
        else
            return this.apellido + ", " + this.nombre + " (" + this.login + ")";
    }


}
