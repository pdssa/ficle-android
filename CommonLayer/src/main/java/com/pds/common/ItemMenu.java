package com.pds.common;

import java.util.ArrayList;
import java.util.List;

public class ItemMenu {
    private String descripcion;
    List<ItemMenu> hijos;
    private String accion;
    private String valor;

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setHijos(List<ItemMenu> hijos) {
        this.hijos = hijos;
    }

    public List<ItemMenu> getHijos() {
        return hijos;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public String getAccion() {
        return accion;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }

    public ItemMenu() {
        this.hijos = new ArrayList<ItemMenu>();
    }

    public ItemMenu(String descripcion) {
        this.hijos = new ArrayList<ItemMenu>();
        this.descripcion = descripcion;
        this.accion = "";
        this.valor = "";
    }

    public ItemMenu(String descripcion, List<ItemMenu> hijos, String accion, String valor) {
        this.hijos = new ArrayList<ItemMenu>();
        this.hijos = hijos;
        this.descripcion = descripcion;
        this.accion = accion;
        this.valor = valor;
    }
}