package com.pds.common.SII.vale.util;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import android.util.Log;

import com.pds.common.Config;
import com.pds.common.Formatos;
import com.pds.common.SII.vale.model.ValeManualTotal;
import com.pds.common.Usuario;
import com.pds.common.UsuarioHelper;
import com.pds.common.model.Venta;
import com.pds.common.dao.CierreDao;
import com.pds.common.dao.VentaDao;
import com.pds.common.model.Cierre;
import com.pds.common.model.VentaDetalle;
import com.pds.common.pref.PDVConfig;
import com.pds.common.pref.ValeConfig;
import com.pds.common.util.ContentProviders;

import java.util.Date;
import java.util.List;
import java.util.zip.CRC32;

/**
 * Created by Hernan on 26/06/2015.
 */
public abstract class ValeUtils {

    public static String GetUltimoCodOperacion(Context context, ContentResolver contentResolver) throws Exception {
            /*
            //sino llega a estar disponible el content provider del vale, tomamos la info de las ventas
            Venta _ult_vale = new VentaDao(contentResolver).first("fc_type = '" + ValeConst.TIPO_DOCUMENTO + "'", null, "_id DESC");

            if (_ult_vale != null)
                cod_op = String.valueOf(_ult_vale.getFc_id());
                */

        //obtenemos el último codigo de operacion utilizado
        String cod_op = "";

        if (ContentProviders.ProviderIsAvailable(context, ValeConfig.CONTENT_PROVIDER.getAuthority())) {

            Cursor cursor = contentResolver.query(ValeConfig.CONTENT_PROVIDER, new String[]{ValeConfig.VALE_ULT_TRACE}, null, null, null);

            if (cursor.moveToFirst()) {
                int i = cursor.getColumnIndex(ValeConfig.VALE_ULT_TRACE);
                cod_op = (i != -1 ? cursor.getString(i) : "");
            }
        } else {
            throw new Exception("No fue posible obtener el ultimo traceID. Verifique que la aplicación Vale v1.02 se encuentre instalada");
        }

        return cod_op;
    }

    public static void SetUltimoCodOperacion(Context context, ContentResolver contentResolver, String ult_trace) {
        //actualizamos el último codigo de operacion obtenido por la plataforma
        if (ContentProviders.ProviderIsAvailable(context, ValeConfig.CONTENT_PROVIDER.getAuthority())) {
            ContentValues cv = new ContentValues();

            cv.put(ValeConfig.VALE_ULT_TRACE, ult_trace);

            contentResolver.insert(ValeConfig.CONTENT_PROVIDER, cv);
        }
    }


    public static ValeManualTotal ObtieneVtasManuales(ContentResolver contentResolver, Date fechaUltCierre) {
        ValeManualTotal manuales = new ValeManualTotal();

        String filter = "codigo in ('PDV','STK') and anulada = 0 and fc_type <> '" + ValeConst.TIPO_DOCUMENTO + "'";
        filter += " and julianday(substr(fecha, 7)||'-'||substr(fecha,4,2)||'-'||substr(fecha,1,2)||' '||hora) > julianday('" + Formatos.FormateaDate(fechaUltCierre, Formatos.DbDateTimeFormat) + "')";

        List<Venta> vtas_manuales = new VentaDao(contentResolver).list(filter, null, null);

        if (!vtas_manuales.isEmpty()) {

            for (Venta vta : vtas_manuales) {

                manuales.q_vales_emitid++;

                vta.addDetalles(contentResolver);

                //sumamos los items afectos y exentos
                for (VentaDetalle det : vta.getDetalles()) {
                    if (det.getIva() != 0)
                        manuales.tot_afe_emitid += det.getTotal();
                    else
                        manuales.tot_exe_emitd += det.getTotal();
                }

            }

        }

        return manuales;
    }

    public static String GetUserID(String terminal_id, ContentResolver contentResolver) {
        Usuario admin = null;

        try {
            admin = UsuarioHelper.ObtenerUsuarioById(contentResolver, 1);
        } catch (Exception ex) {
            Log.e("ValeUtils", ex.getMessage());
        }

        String pass_admin = admin != null ? admin.getPassword() : "";

        return GetUserID(terminal_id, pass_admin);
    }

    public static String GetUserID(String terminal_id, String password) {
        return CalcularID(terminal_id, password);
    }

    private static String CalcularID(String terminal_id, String password) {

        String result = "";

        if (terminal_id.length() < 8) {//ajustamos a 8
            terminal_id = "11111111" + terminal_id;
            terminal_id = terminal_id.substring(terminal_id.length() - 8);
        }

        for (int i = 0; i < 4; i++) {
            int a = Character.getNumericValue(password.charAt(3 - i)) * ((Double) Math.pow(2, i)).intValue(); //

            int b = Character.getNumericValue(terminal_id.charAt(2 * i)) * 10 + Character.getNumericValue(terminal_id.charAt(2 * i + 1)) + a;

            b = b % 100; //para tomar solo los ultimos dos digitos

            String c = "00" + String.valueOf(b);

            c = c.substring(c.length() - 2);

            result += c;
        }

        return result;
    }

    /*public static boolean isValeServiceEnabled(Context context) {
        try {
            if (ContentProviders.ProviderIsAvailable(context, ValeConfig.CONTENT_PROVIDER.getAuthority())) {
                Cursor cursor = context.getContentResolver().query(ValeConfig.CONTENT_PROVIDER, new String[]{ValeConfig.VALE_SERVICIO_ACTIVADO}, null, null, null);
                if (cursor.moveToFirst()) {
                    int i = cursor.getColumnIndex(ValeConfig.VALE_SERVICIO_ACTIVADO);
                    return (i != -1 ? Boolean.parseBoolean(cursor.getString(i)) : false);
                }
            }
        } catch (Exception ex) {
            Log.e("ValeUtils", "ValeUtils.isValeServiceEnabled", ex);
        }

        return false;
    }*/

    /*public static boolean isEmisionValesActivado(ContentResolver contentResolver) {
        try{
            //if (ContentProviders.ProviderIsAvailable(this, PDVConfig.CONTENT_PROVIDER.getAuthority())) {
                Cursor cursor = contentResolver.query(PDVConfig.CONTENT_PROVIDER, new String[]{PDVConfig.PDV_EMITIR_VALE}, null, null, null);
                if (cursor.moveToFirst()) {
                    int i = cursor.getColumnIndex(PDVConfig.PDV_EMITIR_VALE);
                    return (i != -1 ? Boolean.parseBoolean(cursor.getString(i)) : false);
                }
            //}
        }
        catch (Exception ex){
            Log.e("ValeUtils", "ValeUtils.isEmisionValesActivado", ex);
        }

        return false;
    }*/

    public static int MapearMedioPago(String MEDIO_PAGO) {
        if (MEDIO_PAGO.equalsIgnoreCase("EFECTIVO"))
            return 0; //EFECTIVO
        else if (MEDIO_PAGO.equalsIgnoreCase("TARJETA CREDITO"))
            return 2;//PAGO_ELECTRONICO
        else if (MEDIO_PAGO.equalsIgnoreCase("CHEQUE"))
            return 3; //OTRO
        else if (MEDIO_PAGO.equalsIgnoreCase("CTACTE"))
            return 1;//CREDITO_COMERCIO
        else if (MEDIO_PAGO.equalsIgnoreCase("OTRO"))
            return 3; //OTRO
        else
            return 3; //OTRO
    }

    //transforma una fecha en formato yyyyMMddHHmmss a un formato legible
    public static String ExtractFecha(String fechaCorta) {
        String fechaFormat = fechaCorta.substring(6, 8) + "/" + fechaCorta.substring(4, 6) + "/" + fechaCorta.substring(2, 4);

        return fechaFormat;
    }

    //transforma una fecha en formato HHmmss a un formato legible
    public static String ExtractHora(String fechaCorta) {
        String horaFormat = fechaCorta.substring(8, 10) + ":" + fechaCorta.substring(10, 12) + ":" + fechaCorta.substring(12, 14);

        return horaFormat;
    }

    public static String ExtractFechaHora(String fechaCorta) {
        return ExtractFecha(fechaCorta) + " " + ExtractHora(fechaCorta);
    }

    public static String FormatRUT(String rut) {
        int cont = 0;
        String format;
        if (rut.length() == 0) {
            return "";
        } else {
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            format = "-" + rut.substring(rut.length() - 1);
            for (int i = rut.length() - 2; i >= 0; i--) {
                format = rut.substring(i, i + 1) + format;
                cont++;
                if (cont == 3 && i != 0) {
                    format = "." + format;
                    cont = 0;
                }
            }
            return format;
        }
    }

    public static String UnformatRUT(String formatted_rut) {
        if (formatted_rut.length() == 0) {
            return "";
        } else {
            formatted_rut = formatted_rut.replace(".", "");
            formatted_rut = formatted_rut.replace("-", "");
            return formatted_rut.toUpperCase();
        }
    }

    public static boolean ValidateRUT(String rut_dv) {

        try {
            int rut = Integer.parseInt(rut_dv.substring(0, rut_dv.length() - 1));
            char dv = rut_dv.charAt(rut_dv.length() - 1);

            int m = 0, s = 1;
            for (; rut != 0; rut /= 10) {
                s = (s + rut % 10 * (9 - m++ % 6)) % 11;
            }
            return dv == (char) (s != 0 ? s + 47 : 75);
        } catch (Exception ex) {
            Log.e("ValidateRUT", ex.toString());
            return false;
        }

    }

    public static String CalcularCRC(Config config, String merchantNumber) {

        String content = String.format("RUT=%s\nRAZON=%s\nGIRO1=%s\nGIRO2=%s\nGIRO3=%s\nDIRECCION=%s\nRES_SII=%s\nNUM_COMERCIO=%s\nTERM_ID=%s\nNET_ID=%s\n",
                UnformatRUT(config.CLAVEFISCAL),
                config.RAZON_SOCIAL,
                config.SII_GIRO1,
                config.SII_GIRO2,
                config.SII_GIRO3,
                config.DIRECCION,
                config.SII_RESOLUCION_VALE,
                merchantNumber,
                config.SII_TERMINAL_ID,
                config.getNET_ID()
        );

        CRC32 crc32 = new CRC32();
        crc32.update(content.getBytes());
        String res = "00000000" + Long.toHexString(crc32.getValue());

        //en la conversión se omiten los ceros iniciales, entonces hacemos un relleno a izquierda
        return res.substring(res.length() - 8);
    }

    public static String getAppVersion(Context context) {
        //version VALE + version PDV
        String version = "";

        try {
            version = context.getPackageManager().getPackageInfo("com.pds.ficle.vale", 0).versionName + ".";
        } catch (Exception ex) {
            Log.e("getAppVersion", "Error al obtener version de com.pds.ficle.vale", ex);
        }

        try {
            version += context.getPackageManager().getPackageInfo("com.pds.ficle.pdv", 0).versionName;
        } catch (Exception ex) {
            Log.e("getAppVersion", "Error al obtener version de com.pds.ficle.pdv", ex);
        }

        return version;
    }

    public static void updateLastCierreZdate(Context context, Date ultCierreZ) {
        if (ContentProviders.ProviderIsAvailable(context, ValeConfig.CONTENT_PROVIDER.getAuthority())) {
            ContentValues cv_vale = new ContentValues();
            cv_vale.put(ValeConfig.VALE_ULT_CIERRE_Z, Formatos.FormateaDate(ultCierreZ, Formatos.DbDateTimeFormatTimeZone));
            context.getContentResolver().insert(ValeConfig.CONTENT_PROVIDER, cv_vale);
        }
    }

    public static Date getLastCierreZdate(Context context){

        Cursor cursor = null;
        Date date = new Date();

        try {
            if (ContentProviders.ProviderIsAvailable(context, ValeConfig.CONTENT_PROVIDER.getAuthority())) {
                cursor = context.getContentResolver().query(ValeConfig.CONTENT_PROVIDER, new String[]{ValeConfig.VALE_ULT_CIERRE_Z}, null, null, null);
                if (cursor.moveToFirst()) {
                    int i = cursor.getColumnIndex(ValeConfig.VALE_ULT_CIERRE_Z);

                    String ultCierreZDateString = i != -1 ? cursor.getString(i) : "";

                    if(!TextUtils.isEmpty(ultCierreZDateString)){
                        date = Formatos.ObtieneDate(ultCierreZDateString, Formatos.DbDateTimeFormatTimeZone);
                    }
                }
            }
        } catch (Exception ex) {
            Log.e("ValeUtils", "ValeUtils.getLastCierreZdate", ex);
        }

        if(cursor != null)
            cursor.close();

        return date;
    }

    public static boolean isCierreZIntervalExceeded(Context context){

        Date ultCierreZ = getLastCierreZdate(context);
        Date now = new Date();

        //calculamos la diferencia de tiempo
        long difference = 0;
        if (now.after(ultCierreZ))
            difference = now.getTime() - ultCierreZ.getTime();
        else
            difference = ultCierreZ.getTime() - now.getTime();

        //vemos si se supero la cantidad de horas entre cierres
        long x = difference / 1000;//seg
        x /= 60;//min
        x /= 60;//hs
        long hs = x;

        return hs >= ValeConst.HORAS_TOLERANCIA_CIERRE_Z;
    }

    public static boolean isCierreZRequired(Context context){
        Cursor cursor = null;

        try {
            if (ContentProviders.ProviderIsAvailable(context, ValeConfig.CONTENT_PROVIDER.getAuthority())) {
                cursor = context.getContentResolver().query(ValeConfig.CONTENT_PROVIDER, new String[]{ValeConfig.VALE_CIERRE_Z_REQUIRED}, null, null, null);
                if (cursor.moveToFirst()) {
                    int i = cursor.getColumnIndex(ValeConfig.VALE_CIERRE_Z_REQUIRED);

                    return (i != -1 ? cursor.getInt(i) : 0) >= 1;
                }
            }
        } catch (Exception ex) {
            Log.e("ValeUtils", "ValeUtils.isCierreZRequired", ex);
        }

        if(cursor != null)
            cursor.close();

        return false;
    }

    public static void updateIsCierreZRequired(Context context, boolean cierreRequired) {
        if (ContentProviders.ProviderIsAvailable(context, ValeConfig.CONTENT_PROVIDER.getAuthority())) {
            ContentValues cv_vale = new ContentValues();
            cv_vale.put(ValeConfig.VALE_CIERRE_Z_REQUIRED, cierreRequired ? 1 : 0);
            context.getContentResolver().insert(ValeConfig.CONTENT_PROVIDER, cv_vale);
        }
    }
}
