package com.pds.common.SII.vale.services;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Formato;
import com.pds.common.Formatos;
import com.pds.common.R;
import com.pds.common.SII.vale.listener.InformeListener;
import com.pds.common.SII.vale.model.InformeReq;
import com.pds.common.SII.vale.model.InformeResp;
import com.pds.common.SII.vale.parser.InformeRespXmlParser;
import com.pds.common.SII.vale.util.ValeConst;
import com.pds.common.SII.vale.util.ValeError;
import com.pds.common.SII.vale.util.ValeUtils;
import com.pds.common.network.MySSLSocketFactory;
import com.pds.common.pref.ValeConfig;
import com.pds.common.util.ConnectivityUtils;
import com.pds.common.util.ContentProviders;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyStore;
import java.util.Date;

import android_serialport_api.Printer;

/**
 * Created by Hernan on 23/06/2015.
 */
public class CierreZtask extends AsyncTask<InformeReq, String, String> {

    private ProgressDialog dialog;
    private Context context;
    private boolean RESULT;
    private InformeListener listener;
    private Config config;
    private boolean cierreForzado;

    public CierreZtask(Context _context, Config _config, boolean _cierreForzado, InformeListener _listener) {
        context = _context;
        dialog = new ProgressDialog(context);
        listener = _listener;
        config = _config;
        cierreForzado = _cierreForzado;
    }

    private InformeReq getRequestZ() {
        return new InformeReq(config, ValeUtils.GetUserID(config.SII_TERMINAL_ID, context.getContentResolver()), 0, 0d, 0, 0d , cierreForzado, config.SII_MERCHANT_ID);
    }

    private void setUltCierreZ(Date ultCierreZ) {
        ValeUtils.updateLastCierreZdate(context, ultCierreZ);
    }

    public void startDialog() {
        //por un tema de orden de los botones en pantalla mezclamos los ids
        final int OPCION_SI = AlertDialog.BUTTON_NEGATIVE;
        final int OPCION_NO = AlertDialog.BUTTON_NEUTRAL;
        final int OPCION_CANCELAR = AlertDialog.BUTTON_POSITIVE;

        //vamos a presentar un dialogo para que el usuario confirme la operacion, e ingrese vales manuales si lo desea
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        // Get the layout inflater
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate and set the layout for the dialog
        View cierreView = inflater.inflate(R.layout.dialog_cierre_z, null);

        if (cierreForzado) {
            ((TextView) cierreView.findViewById(R.id.dialog_cierre_z_message)).setText(Html.fromHtml(context.getString(R.string.cierre_z_msj_forc_1)));
        } else {
            ((TextView) cierreView.findViewById(R.id.dialog_cierre_z_message)).setText(Html.fromHtml(context.getString(R.string.cierre_z_msj_1)));
        }

        builder.setView(cierreView);

        final AlertDialog dialog = builder.create();

        dialog.setTitle(cierreForzado ? "Cierre Z Necesario" : "Cierre Z");
        dialog.setIcon(android.R.drawable.ic_menu_help);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setOnCancelListener(null);

        dialog.setButton(OPCION_SI, "SI", (DialogInterface.OnClickListener) null);
        dialog.setButton(OPCION_NO, "NO", (DialogInterface.OnClickListener) null);

        if (!cierreForzado) {
            //no mostramos la opcion de cancelar
            dialog.setButton(OPCION_CANCELAR, "CANCELAR", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
        }

        //asignamos los eventos al dialog
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface d) {

                dialog.getButton(OPCION_NO).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();

                        //lanzamos el cierre Z sin ventas manuales
                        InformeReq informeReq = getRequestZ();
                        //EnviarCierreZ(informeReq);
                        execute(informeReq);
                    }
                });

                dialog.getButton(OPCION_SI).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //mostramos la seccion de declaracion de emisiones manuales
                        dialog.findViewById(R.id.dialgo_cierre_z_sec_manual).setVisibility(View.VISIBLE);

                        //ocultamos el NO
                        dialog.getButton(OPCION_NO).setVisibility(View.GONE);

                        //cambiamos el SI
                        Button btnPositive = dialog.getButton(OPCION_SI);
                        btnPositive.setText("ACEPTAR");
                        btnPositive.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                try {
                                    //tenemos que capturar y validar los datos ingresados

                                    int cant_registros_afectos_manuales = 0, cant_registros_no_afectos_manuales = 0;
                                    double total_afecto_vales_manuales = 0d, total_no_afecto_vales_manuales = 0d;

                                    ((EditText) dialog.findViewById(R.id.dialog_cierre_z_cant_afecto)).setError(null);
                                    ((EditText) dialog.findViewById(R.id.dialog_cierre_z_cant_exento)).setError(null);
                                    ((EditText) dialog.findViewById(R.id.dialog_cierre_z_tot_exento)).setError(null);
                                    ((EditText) dialog.findViewById(R.id.dialog_cierre_z_tot_afecto)).setError(null);

                                    String s_cant_afecto = ((EditText) dialog.findViewById(R.id.dialog_cierre_z_cant_afecto)).getText().toString().trim();
                                    String s_cant_no_afecto = ((EditText) dialog.findViewById(R.id.dialog_cierre_z_cant_exento)).getText().toString().trim();
                                    String s_monto_afecto = ((EditText) dialog.findViewById(R.id.dialog_cierre_z_tot_afecto)).getText().toString().trim();
                                    String s_monto_no_afecto = ((EditText) dialog.findViewById(R.id.dialog_cierre_z_tot_exento)).getText().toString().trim();

                                    try {
                                        cant_registros_afectos_manuales = Integer.parseInt(s_cant_afecto);
                                    } catch (NumberFormatException nfe) {
                                        cant_registros_afectos_manuales = 0;
                                    }

                                    try {
                                        cant_registros_no_afectos_manuales = Integer.parseInt(s_cant_no_afecto);
                                    } catch (NumberFormatException nfe) {
                                        cant_registros_no_afectos_manuales = 0;
                                    }

                                    try {
                                        total_afecto_vales_manuales = Double.parseDouble(s_monto_afecto);
                                    } catch (NumberFormatException nfe) {
                                        total_afecto_vales_manuales = 0d;
                                    }

                                    try {
                                        total_no_afecto_vales_manuales = Double.parseDouble(s_monto_no_afecto);
                                    } catch (NumberFormatException nfe) {
                                        total_no_afecto_vales_manuales = 0d;
                                    }

                                    //validacion cruzada, si cargaron cantidad afecta , tienen que cargar monto afecto y viceversa
                                    if (cant_registros_afectos_manuales > 0 && total_afecto_vales_manuales < 1) {
                                        ((EditText) dialog.findViewById(R.id.dialog_cierre_z_tot_afecto)).setError("Ingrese total monto afecto");
                                        return;
                                    }
                                    if (total_afecto_vales_manuales > 0 && cant_registros_afectos_manuales < 1) {
                                        ((EditText) dialog.findViewById(R.id.dialog_cierre_z_cant_afecto)).setError("Ingrese cantidad registros afectos");
                                        return;
                                    }

                                    //validacion cruzada, si cargaron cantidad exenta, tienen que cargar monto exento y viceversa
                                    if (cant_registros_no_afectos_manuales > 0 && total_no_afecto_vales_manuales < 1) {
                                        ((EditText) dialog.findViewById(R.id.dialog_cierre_z_tot_exento)).setError("Ingrese total monto exento");
                                        return;
                                    }
                                    if (total_no_afecto_vales_manuales > 0 && cant_registros_no_afectos_manuales < 1) {
                                        ((EditText) dialog.findViewById(R.id.dialog_cierre_z_cant_exento)).setError("Ingrese cantidad registros no afectos");
                                        return;
                                    }

                                    //vemos si alguno tiene valor cargado para mostrar allí el error
                                    if (cant_registros_afectos_manuales + cant_registros_no_afectos_manuales < 1) {
                                        if (TextUtils.isEmpty(s_cant_afecto) && !TextUtils.isEmpty(s_cant_no_afecto))
                                            ((EditText) dialog.findViewById(R.id.dialog_cierre_z_cant_exento)).setError("Cantidad ingresada errónea");
                                        else
                                            ((EditText) dialog.findViewById(R.id.dialog_cierre_z_cant_afecto)).setError("Cantidad ingresada errónea");
                                        return;
                                    }

                                    //vemos si alguno tiene valor cargado para mostrar allí el error
                                    if (total_afecto_vales_manuales + total_no_afecto_vales_manuales < 1) {
                                        if (TextUtils.isEmpty(s_monto_afecto) && !TextUtils.isEmpty(s_monto_no_afecto))
                                            ((EditText) dialog.findViewById(R.id.dialog_cierre_z_tot_exento)).setError("Monto total ingresado erróneo");
                                        else
                                            ((EditText) dialog.findViewById(R.id.dialog_cierre_z_tot_afecto)).setError("Monto total ingresado erróneo");
                                        return;
                                    }


                                    dialog.dismiss();

                                    //lanzamos el cierre Z con los valores ingresados para ventas manuales
                                    InformeReq informeReq = getRequestZ();
                                    informeReq.setReqZ(cant_registros_afectos_manuales, total_afecto_vales_manuales, cant_registros_no_afectos_manuales, total_no_afecto_vales_manuales);
                                    execute(informeReq);

                                } catch (Exception ex) {
                                    Toast.makeText(context, "Error : " + ex.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }
                        });


                    }
                });
            }
        });

        dialog.show();
    }

    @Override
    protected void onPreExecute() {
        //iniciamos un mensaje para el usuario
        dialog = new ProgressDialog(context);
        dialog.setMessage("Iniciando Cierre Z de la jornada. Aguarde por favor...");
        dialog.setIndeterminate(false);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    @Override
    protected String doInBackground(InformeReq... params) {
        try {

            if (!ConnectivityUtils.isOnline(context)) {
                return ConnectivityUtils.SIN_CONEXION;
            }

            //enviamos los datos para request del informe
            String response = PostDataToServer(params[0]);

            RESULT = true;

            return response;

        } catch (Exception e) {

            RESULT = false;

            return e.getMessage();
        }
    }

    @Override
    protected void onProgressUpdate(String... values) {
        dialog.setMessage(values[0]);
    }

    @Override
    protected void onPostExecute(String result) {
        if (dialog.isShowing())
            dialog.dismiss();

        if (!RESULT) {
            AlertMessage(result);//mostramos mensaje de error
        } else {

            try {

                InformeResp resp = new InformeRespXmlParser().parse(result);

                showResults(resp);

            } catch (Exception ex) {
                AlertMessage(ex.toString());
            }
        }
    }

    @Override
    protected void onCancelled(String s) {
        AlertMessage(s);
    }

    private void AlertMessage(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setTitle("VALE ELECTRONICO: CIERRE Z");
        builder.setMessage(Html.fromHtml("Se ha producido un error al intentar realizar el cierre Z:" + "<br/><b>" + message + "</b>"));
        builder.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface _dialog, int which) {
                _dialog.dismiss();

                listener.onError();
            }
        });

        builder.setCancelable(false);
        builder.show();
    }

    private String PostDataToServer(InformeReq request) throws Exception {

        // 1. create HttpClient
        HttpClient httpclient = new DefaultHttpClient();
        HttpParams params = httpclient.getParams();
        HttpConnectionParams.setConnectionTimeout(params, ValeConst.CONNECTION_TIMEOUT);
        HttpConnectionParams.setSoTimeout(params, ValeConst.SOCKET_TIMEOUT);

        // 2. create HttpsClient
        KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
        trustStore.load(null, null);

        MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
        sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

        ClientConnectionManager ccm = httpclient.getConnectionManager();

        SchemeRegistry sr = ccm.getSchemeRegistry();
        sr.register(new Scheme("https", sf, 443));

        DefaultHttpClient httpsclient = new DefaultHttpClient(ccm, httpclient.getParams());

        // 2. make POST request to the given URL
        HttpPost httpPost = new HttpPost(ValeConst.getURL_WS(context) + ValeConst.METHOD_INFORME_Z);

        // 3. build POST content
        //RUT_EMISOR=string&TERMINAL_ID=string&USER_ID=string
        String content = request.toStringCierreZ();

        // 4. set content to StringEntity
        StringEntity se = new StringEntity(content);

        // 5. set httpPost Entity
        httpPost.setEntity(se);

        // 6. Set some headers to inform server about the type of the content
        httpPost.setHeader("Content-type", "application/x-www-form-urlencoded");

        // 7. Execute POST request to the given URL
        HttpResponse httpResponse = httpsclient.execute(httpPost);

        // 8. receive response as inputStream
        int codeResponse = httpResponse.getStatusLine().getStatusCode();
        InputStream inputStream = null;
        inputStream = httpResponse.getEntity().getContent();

        String response = "";
        if (inputStream == null)
            throw new Exception("Error al obtener respuesta del servidor");
        else {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            response = "";
            while ((line = bufferedReader.readLine()) != null)
                response += line;

            inputStream.close();
        }

        if (codeResponse == HttpStatus.SC_OK) {
            //OK => obtenemos la respuesta
            return response;
        } else {
            //NO OK => devolvemos excepcion para el error obtenido
            throw new Exception(ValeError.getMessageError(codeResponse, response));
        }

    }

    private void showResults(final InformeResp response) {
        final Date cierre_z = new Date();

        setUltCierreZ(cierre_z);//--> grabamos la fecha de ult cierre Z

        ValeUtils.SetUltimoCodOperacion(context, context.getContentResolver(), response.ULT_TRACE_ID);//--> actualizamos el ultimo traceid

        //dado que se pudo realizar OK el cierre z, entonces grabamos que NO es requerido el cierre
        ValeUtils.updateIsCierreZRequired(context, false);

        //mostramos los resultados en pantalla, impresion opcional
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        // Get the layout inflater
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate and set the layout for the dialog
        View cierreResultView = inflater.inflate(R.layout.cierre_z_result, null);

        ((TextView) cierreResultView.findViewById(R.id.cierre_result_msj)).setText("Cierre Z realizado correctamente. A continuación los resultados:");

        try {
            ((TextView) cierreResultView.findViewById(R.id.cierre_result_cantidad_vales)).setText(response.CANT_VALES);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            ((TextView) cierreResultView.findViewById(R.id.cierre_result_total_ventas)).setText(Formato.ParseaFormateaDecimal(response.TOTAL, Formato.Decimal_Format.US, Formato.Decimal_Format.CH, "$"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            ((TextView) cierreResultView.findViewById(R.id.cierre_result_total_ventas_afecto)).setText(Formatos.ParseaFormateaDecimal(response.TOTAL_AFECTO, Formatos.DecimalFormat_US, Formatos.DecimalFormat_CH, "$"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            ((TextView) cierreResultView.findViewById(R.id.cierre_result_total_ventas_exento)).setText(Formatos.ParseaFormateaDecimal(response.TOTAL_NO_AFECTO, Formatos.DecimalFormat_US, Formatos.DecimalFormat_CH, "$"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            ((TextView) cierreResultView.findViewById(R.id.cierre_result_vale_ini)).setText(response.VALE_INI);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            ((TextView) cierreResultView.findViewById(R.id.cierre_result_vale_fin)).setText(response.VALE_FIN);
        } catch (Exception e) {
            e.printStackTrace();
        }


        builder.setView(cierreResultView);

        String nro_lote = "00000" + response.ULT_LOTE;
        nro_lote = nro_lote.substring(nro_lote.length() - 5);

        AlertDialog dialog = builder.create();
        dialog.setTitle("Cierre Z Nro. " + nro_lote);
        dialog.setIcon(android.R.drawable.ic_menu_info_details);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setOnCancelListener(null);

        dialog.setButton(AlertDialog.BUTTON_NEUTRAL, "ACEPTAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

                listener.onConsultaCompletada(response);//retornamos el resultado de la consulta
            }
        });
        dialog.setButton(AlertDialog.BUTTON_POSITIVE, "IMPRIMIR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                imprimirCierreZ(response, cierre_z);

                listener.onConsultaCompletada(response);//retornamos el resultado de la consulta
            }
        });

        dialog.show();
    }

    private void imprimirCierreZ(final InformeResp response, final Date cierre_z) {
        try {

            final Printer _printer = Printer.getInstance(config.PRINTER_MODEL, context);

            _printer.delayActivated = true;
            _printer.printerCallback = new Printer.PrinterCallback() {
                @Override
                public void onPrinterReady() {
                    _printer.lineFeed();
                    _printer.lineFeed();

                    _printer.format(1, 1, Printer.ALINEACION.CENTER);

                    //titulo
                    _printer.printLine(config.COMERCIO);
                    _printer.printLine("R.U.T.: " + ValeUtils.FormatRUT(config.CLAVEFISCAL));

                    //actividades
                    if (!TextUtils.isEmpty(config.SII_GIRO1))
                        _printer.printLine(config.SII_GIRO1);
                    if (!TextUtils.isEmpty(config.SII_GIRO2))
                        _printer.printLine(config.SII_GIRO2);
                    if (!TextUtils.isEmpty(config.SII_GIRO3))
                        _printer.printLine(config.SII_GIRO3);


                    /*
                    if (_printer.get_longitudLinea() - config.DIRECCION.length() < 2) {
                        _printer.format(1, 1, Printer.ALINEACION.LEFT);
                        _printer.printLine(config.DIRECCION);
                    } else
                        _printer.print(config.DIRECCION);
                    */

                    //_printer.format(1, 1, Printer.ALINEACION.LEFT);

                    if (_printer.get_longitudLinea() - config.DIRECCION.length() < 2) {
                        _printer.printOpposite(config.DIRECCION, "");
                    } else
                        _printer.printLine(config.DIRECCION);


                    /*if (!TextUtils.isEmpty(config.LOCALIDAD)) {
                        if (_printer.get_longitudLinea() - config.LOCALIDAD.length() < 2) {
                            _printer.format(1, 1, Printer.ALINEACION.LEFT);
                            _printer.printLine(config.LOCALIDAD);
                        } else
                            _printer.print(config.LOCALIDAD);
                    }*/

                    if (!TextUtils.isEmpty(config.LOCALIDAD)) {
                        if (_printer.get_longitudLinea() - config.LOCALIDAD.length() < 2) {
                            _printer.printOpposite(config.LOCALIDAD, "");
                        } else
                            _printer.printLine(config.LOCALIDAD);
                    }


                    try {
                        Thread.sleep(600);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    _printer.sendSeparadorHorizontal('=');
                    _printer.format(1, 1, Printer.ALINEACION.CENTER);
                    String nro_lote = "00000" + response.ULT_LOTE;
                    nro_lote = nro_lote.substring(nro_lote.length() - 5);
                    _printer.printLine("INFORME Z Nro. " + nro_lote);
                    _printer.sendSeparadorHorizontal('=');
                    _printer.format(1, 1, Printer.ALINEACION.CENTER);
                    _printer.printLine(Formatos.FormateaDate(cierre_z, Formatos.DateFormatSlashAndTimeSec));
                    _printer.printLine("TER: " + config.SII_TERMINAL_ID);
                    _printer.sendSeparadorHorizontal();
                    _printer.format(1, 1, Printer.ALINEACION.CENTER);
                    _printer.printLine("INFORME DE TOTALES FISCALES");
                    _printer.lineFeed();

                    _printer.cancelCurrentFormat();


                    //DATOS
                    _printer.printOpposite("VENTAS    :", Formato.ParseaFormateaDecimal(response.TOTAL, Formato.Decimal_Format.US, Formato.Decimal_Format.CH, "$"));
                    _printer.printOpposite("VALE INIC.:", response.VALE_INI);
                    _printer.printOpposite("VALE FINAL:", response.VALE_FIN);
                    _printer.sendSeparadorHorizontal();

                    //TOTALES POR MEDIO PAGO
                    _printer.format(1, 1, Printer.ALINEACION.CENTER);
                    _printer.printLine("TOTALES POR FORMA DE PAGO");
                    _printer.lineFeed();

                    _printer.cancelCurrentFormat();

                    if (!TextUtils.isEmpty(response.DET_TIPO_PAGO)) {
                        String[] transacciones = response.DET_TIPO_PAGO.split("#");//#<medio>*<total>

                        for (String transaccion : transacciones) {
                            if (!TextUtils.isEmpty(transaccion)) {
                                String[] datos = transaccion.split("\\*");

                                //transaccion
                                String medio_pago = datos[0] + "                ";
                                medio_pago = medio_pago.substring(0, 17);
                                _printer.printOpposite(medio_pago + ": $", Formato.ParseaFormateaDecimal(datos[1], Formato.Decimal_Format.US, Formato.Decimal_Format.CH));
                            }
                        }

                    }

                    _printer.sendSeparadorHorizontal();

                    //TOTALES PARCIALES DEL PERIODO
                    _printer.format(1, 1, Printer.ALINEACION.CENTER);
                    _printer.printLine("TOTAL DEL PERIODO FISCAL");
                    _printer.printLine(response.PERIODO);
                    _printer.lineFeed();

                    _printer.printOpposite("TOTAL ACUM.:", Formato.ParseaFormateaDecimal(response.TOT_ACUM, Formato.Decimal_Format.US, Formato.Decimal_Format.CH, "$"));

                    _printer.sendSeparadorHorizontal('=');

                    _printer.footer();

                    _printer.end();
                }
            };

            //iniciamos la printer
            if (!_printer.initialize()) {
                Toast.makeText(context, "Error al inicializar", Toast.LENGTH_SHORT).show();

                _printer.end();
            }

        } catch (Exception ex) {
            Toast.makeText(context, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }
}
