package com.pds.common.SII.vale.model;

import com.pds.common.Config;
import com.pds.common.SII.vale.util.ValeUtils;

/**
 * Created by Hernan on 15/07/2016.
 */
public class AnulacionReq {

    public String RUT_EMISOR;
    public String NUM_COMERCIO;
    public String TERMINAL_ID;
    public String USER_ID;
    public String NET_ID;
    public String CRC_CONFIG;
    public String NRO_UNICO_VALE;

    public AnulacionReq(Config config, String merchantNumber, String user_id, String nroValeAnular) {
        this.RUT_EMISOR = ValeUtils.UnformatRUT(config.CLAVEFISCAL);
        this.TERMINAL_ID = config.SII_TERMINAL_ID;
        this.NUM_COMERCIO = merchantNumber;
        this.USER_ID = user_id;
        this.NET_ID = config.getNET_ID();
        this.CRC_CONFIG = ValeUtils.CalcularCRC(config, merchantNumber);
        this.NRO_UNICO_VALE = nroValeAnular;
    }

    @Override
    public String toString() {
        return String.format("NUM_COMERCIO=%s&ID_TERMINAL=%s&RUT_COMERCIO=%s&NET_ID=%s&USER_ID=%s&CRC_CONFIG=%s&NRO_UNICO_VALE=%s",
                NUM_COMERCIO,
                TERMINAL_ID,
                RUT_EMISOR,
                NET_ID,
                USER_ID,
                CRC_CONFIG,
                NRO_UNICO_VALE);
    }
}
