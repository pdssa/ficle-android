package com.pds.common.SII.vale.model;

import com.pds.common.Config;

/**
 * Created by Hernan on 17/10/2016.
 */
public class SendMailReportReq extends ReportReq {
    private String EMAIL;
    private String FEC_DESDE;
    private String FEC_HASTA;

    public SendMailReportReq(Config config, String user_id, String email, String fecDesde, String fecHasta, String merchantNumber)  {
        super(merchantNumber, config, user_id);

        EMAIL = email;
        FEC_DESDE = fecDesde;
        FEC_HASTA = fecHasta;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    @Override
    public String toString() {
        return String.format("NUM_COMERCIO=%s&ID_TERMINAL=%s&RUT_COMERCIO=%s&NET_ID=%s&USER_ID=%s&CRC_CONFIG=%s&EMAIL=%s&FEC_DESDE=%s&FEC_HASTA=%s",
                NUM_COMERCIO,
                ID_TERMINAL,
                RUT_COMERCIO,
                NET_ID,
                USER_ID,
                CRC_CONFIG,
                EMAIL,
                FEC_DESDE,
                FEC_HASTA);
    }
}
