package com.pds.common.SII.vale.parser;

import android.util.Xml;

import com.pds.common.SII.vale.model.InformeResp;
import com.pds.common.SII.vale.model.Operacion;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

/**
 * Created by Hernan on 23/06/2015.
 */
public class InformeRespXmlParser {
    // sin namespaces
    private static final String ns = null;

    public InformeResp parse(String in) throws XmlPullParserException, Exception {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(new StringReader(in));
            parser.nextTag();
            return readXML(parser);
        } catch (Exception ex) {
            throw ex;
        } finally {
            //in.close();
        }
    }

    private InformeResp readXML(XmlPullParser parser) throws XmlPullParserException, IOException {

        InformeResp informeResp = new InformeResp();
        String text = "";

        // Iniciamos recorriendo los tags

        int eventType = parser.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            String tagname = parser.getName();

            switch (eventType) {
                case XmlPullParser.START_TAG: {

                }
                break;
                case XmlPullParser.TEXT: {
                    text = parser.getText();
                }
                break;
                case XmlPullParser.END_TAG: {
                    if (tagname.equalsIgnoreCase("TOTAL")) {
                        informeResp.TOTAL = text;
                    } else if (tagname.equalsIgnoreCase("TOTAL_AFECTO")) {
                        informeResp.TOTAL_AFECTO = text;
                    } else if (tagname.equalsIgnoreCase("TOTAL_NO_AFECTO")) {
                        informeResp.TOTAL_NO_AFECTO = text;
                    } else if (tagname.equalsIgnoreCase("VALE_INI")) {
                        informeResp.VALE_INI = text;
                    } else if (tagname.equalsIgnoreCase("VALE_FIN")) {
                        informeResp.VALE_FIN = text;
                    } else if (tagname.equalsIgnoreCase("ULT_LOTE")) {
                        informeResp.ULT_LOTE = text;
                    } else if (tagname.equalsIgnoreCase("DET_TIPO_PAGO")) {
                        informeResp.DET_TIPO_PAGO = text;
                    } else if (tagname.equalsIgnoreCase("ULT_TRACE_ID")) {
                        informeResp.ULT_TRACE_ID = text;
                    } else if (tagname.equalsIgnoreCase("PERIODO")) {
                        informeResp.PERIODO = text;
                    } else if (tagname.equalsIgnoreCase("TOT_ACUM")) {
                        informeResp.TOT_ACUM = text;
                    } else if (tagname.equalsIgnoreCase("CANT_VALES")) {
                        informeResp.CANT_VALES = text;
                    }
                }
                break;
                default:
                    break;

            }

            eventType = parser.next();

        }

        return informeResp;
    }
}
