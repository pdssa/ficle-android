package com.pds.common.SII.vale.listener;

/**
 * Created by Hernan on 15/07/2016.
 */
public interface AnulacionListener {

    void onAnulacionCompletada();

    void onAnulacionError();
}
