package com.pds.common.SII.vale.parser;

import android.util.Xml;

import com.pds.common.SII.vale.model.InformeResp;
import com.pds.common.SII.vale.model.InitTerminalResp;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.StringReader;

/**
 * Created by Hernan on 23/06/2015.
 */
public class InitTerminalRespXmlParser {
    // sin namespaces
    private static final String ns = null;

    public InitTerminalResp parse(String in) throws XmlPullParserException, Exception {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(new StringReader(in));
            parser.nextTag();
            return readXML(parser);
        } catch (Exception ex) {
            throw ex;
        } finally {
            //in.close();
        }
    }

    private InitTerminalResp readXML(XmlPullParser parser) throws XmlPullParserException, IOException {

        InitTerminalResp initTerminalResp = new InitTerminalResp();
        String text = "";

        // Iniciamos recorriendo los tags

        int eventType = parser.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            String tagname = parser.getName();

            switch (eventType) {
                case XmlPullParser.START_TAG: {

                }
                break;
                case XmlPullParser.TEXT: {
                    text = parser.getText();
                }
                break;
                case XmlPullParser.END_TAG: {
                    if (tagname.equalsIgnoreCase("ULT_TRACE_ID")) {
                        initTerminalResp.ULT_TRACE_ID = text;
                    }
                }
                break;
                default:
                    break;

            }

            eventType = parser.next();

        }

        return initTerminalResp;
    }
}
