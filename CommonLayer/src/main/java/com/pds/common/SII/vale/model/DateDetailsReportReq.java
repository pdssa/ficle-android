package com.pds.common.SII.vale.model;

import com.pds.common.Config;

/**
 * Created by Hernan on 17/10/2016.
 */
public class DateDetailsReportReq extends ReportReq {
    private String DIA;

    public DateDetailsReportReq(Config config, String user_id, String dia, String merchantNumber)  {
        super(merchantNumber, config, user_id);

        DIA = dia;
    }

    @Override
    public String toString() {
        return String.format("NUM_COMERCIO=%s&ID_TERMINAL=%s&RUT_COMERCIO=%s&NET_ID=%s&USER_ID=%s&CRC_CONFIG=%s&DIA=%s",
                NUM_COMERCIO,
                ID_TERMINAL,
                RUT_COMERCIO,
                NET_ID,
                USER_ID,
                CRC_CONFIG,
                DIA);
    }
}
