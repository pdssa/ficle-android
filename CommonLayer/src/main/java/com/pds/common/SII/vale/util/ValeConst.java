package com.pds.common.SII.vale.util;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import android.util.Log;

import com.pds.common.Config;
import com.pds.common.pref.ValeConfig;

/**
 * Created by Hernan on 01/07/2015.
 */
public abstract class ValeConst {
    //diferentes status
    public static final String VALE_STATUS_DESACTIVADO = "D";//el servicio está desactivado en la plataforma
    public static final String VALE_STATUS_HABILITADO = "H";//servicio habilitado en plataforma, pero aún no inicializado
    public static final String VALE_STATUS_INICIALIZADO = "A";//inicialización ejecutada, terminal listo para emitir vales

    public static final int CONNECTION_TIMEOUT = 15000;//the timeout until a connection is established
    public static final int SOCKET_TIMEOUT = 15000; //is the timeout for waiting for data

    public static final int HORAS_TOLERANCIA_CIERRE_Z = 24;//cada que cantidad de hs tiene que hacer un cierre

    public static final String TIPO_DOCUMENTO = "47";//47: Vales Electrónicos en Reemplazo de Boletas Afectas

    public static final String MODO_CIERRE_NORMAL = "1";//InformeZ on demand en modelo online
    public static final String MODO_CIERRE_FORZADO = "2";//InformeZ forzado en modelo online

    public static final String METHOD_INIT_TERMINAL = "/InicializarTerminal";
    public static final String METHOD_CAMBIO_USER = "/CambiarUser";
    public static final String METHOD_INFORMAR_VALE = "/InformarVale";
    public static final String METHOD_INFORME_X = "/InformeX";
    public static final String METHOD_INFORME_Z = "/CierreInfZ"; //"/CierreZ"; //"/InformeZ";
    public static final String METHOD_REQUIERE_CIERRE = "/RequiereCierre";
    public static final String METHOD_INFORME_TRANSACCIONES = "/InformeTransacciones";
    public static final String METHOD_OBTENER_ULTIMO_VALE = "/ObtenerUltimoVale";
    public static final String METHOD_ANULAR_VALE = "/AnularTransaccion";
    public static final String METHOD_ACTIVAR_TERMINAL = "/ActivarTerminal";

    public static final String METHOD_RPT_TOTALES_MES = "/TotalesMes";
    public static final String METHOD_RPT_DETALLE_DIA = "/DetalleDiario";
    public static final String METHOD_RPT_ENVIAR_MAIL = "/EnviarReporte";

    public static String getURL_WS(Context context) {
        //return "https://www.valefiscal.cl/WS.asmx";

        Config config = new Config(context);

        return config.SII_SERVIDOR_PLATFORM  + "/WS.asmx";
    }

    public static String getURL_CONSULTAS(Context context) {
        //return "http://190.111.56.80:8888/ConsultasQR/vale";

        Config config = new Config(context);

        return config.SII_SERVIDOR_CONSULTA_QR;

    }

    public static String getURL_Rpt(Context context) {
        //return "https://www.valefiscal.cl/WS.asmx";

        Config config = new Config(context);

        return config.SII_SERVIDOR_PLATFORM  + "/wsrpt.asmx";
    }
}
