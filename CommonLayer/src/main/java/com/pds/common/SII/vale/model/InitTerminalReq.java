package com.pds.common.SII.vale.model;

import com.pds.common.Config;
import com.pds.common.SII.vale.util.ValeUtils;

/**
 * Created by Hernan on 15/06/2016.
 */
public class InitTerminalReq {
    public String RUT_COMERCIO;
    public String NUM_COMERCIO;
    public String TERMINAL_ID;
    public String NET_ID;
    public String IMSI;
    public String USER_ID;
    public String CRC_CONFIG;


    public InitTerminalReq(String merchantNumber, Config config, String user_id) {
        this.RUT_COMERCIO = ValeUtils.UnformatRUT(config.CLAVEFISCAL);
        this.TERMINAL_ID = config.SII_TERMINAL_ID;
        this.NUM_COMERCIO = merchantNumber;
        this.USER_ID = user_id;
        this.NET_ID = config.getNET_ID();
        this.IMSI = config.IMSI;
        this.CRC_CONFIG = ValeUtils.CalcularCRC(config, merchantNumber);
    }

    @Override
    public String toString() {
        return String.format("NUM_COMERCIO=%s&ID_TERMINAL=%s&RUT_COMERCIO=%s&NET_ID=%s&IMSI=%s&USER_ID=%s&CRC_CONFIG=%s",
                NUM_COMERCIO,
                TERMINAL_ID,
                RUT_COMERCIO,
                NET_ID,
                IMSI,
                USER_ID,
                CRC_CONFIG);
    }
}
