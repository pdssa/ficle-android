package com.pds.common.SII.vale.model;

/**
 * Created by Hernan on 23/06/2015.
 */
public class Operacion {
    //<Operacion><NRO_FOLIO>00</NRO_FOLIO><COD_OPERACION>1</COD_OPERACION><NRO_UNICO_VALE>12345678-150806110000001</NRO_UNICO_VALE><HASH_TRX>875503b4667edf40d8c0</HASH_TRX></Operacion>
    public String NRO_FOLIO;
    public String COD_OPERACION;
    public String NRO_UNICO_VALE;
    public String HASH_TRX;

    public Operacion() {

    }
}
