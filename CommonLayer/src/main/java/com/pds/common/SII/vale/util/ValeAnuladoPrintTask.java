package com.pds.common.SII.vale.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import java.util.Hashtable;

import android_serialport_api.LineaTicket;
import android_serialport_api.Printer;

/**
 * Created by Hernan on 13/07/2016.
 */
public class ValeAnuladoPrintTask extends AsyncTask<ValeAnuladoTicketBuilder, Void, Boolean> {

    private ProgressDialog dialog;
    private Context context;
    private String printerModel;
    private ValeAnuladoTicketBuilder valeTicketBuilder;

    public ValeAnuladoPrintTask(Context context, String printerModel) {
        this.context = context;
        this.printerModel = printerModel;
    }

    @Override
    protected void onPreExecute() {
        dialog = new ProgressDialog(context);
        dialog.setMessage("Imprimiendo comprobante de anulación, aguarde por favor...");
        dialog.setIndeterminate(false);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    @Override
    protected Boolean doInBackground(ValeAnuladoTicketBuilder... params) {

        if(params.length < 1)
            return false;

        valeTicketBuilder = params[0];

        printVale();

        return true;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        if (dialog.isShowing())
            dialog.dismiss();
    }

    private void printVale() {
        try {

            final Printer _printer = Printer.getInstance(printerModel, context);

            _printer.delayActivated = true;
            _printer.printerCallback = new Printer.PrinterCallback() {
                @Override
                public void onPrinterReady() {
                    _printer.lineFeed();
                    _printer.lineFeed();

                    if (valeTicketBuilder.is_isDemoMode()) {
                        _printer.format(2, 1, Printer.ALINEACION.CENTER);
                        _printer.printLine("COMPROBANTE ANULACION");
                        _printer.printLine("SIN VALIDEZ FISCAL");
                    } else {
                        _printer.format(2, 1, Printer.ALINEACION.CENTER);
                        _printer.printLine("COMPROBANTE");
                        _printer.printLine("ANULACION VALE");
                    }

                    if (!TextUtils.isEmpty(valeTicketBuilder.get_header())) {
                        _printer.format(1, 2, Printer.ALINEACION.CENTER);
                        _printer.printLine(valeTicketBuilder.get_header());
                    }

                    _printer.format(1, 1, Printer.ALINEACION.CENTER);
                    _printer.printLine(valeTicketBuilder.get_razonSocial());
                    _printer.printLine("R.U.T.:" + ValeUtils.FormatRUT(valeTicketBuilder.get_clavefiscal()));

                    //actividades
                    for (String g : valeTicketBuilder.get_giro()) {
                        _printer.printLine(g);
                    }

                    _printer.printLine(valeTicketBuilder.get_nombreFantasia());

                    if (_printer.get_longitudLinea() - valeTicketBuilder.get_direccion().length() < 2) {
                        _printer.printOpposite(valeTicketBuilder.get_direccion(), "");
                    } else
                        _printer.printLine(valeTicketBuilder.get_direccion());


                    if (!TextUtils.isEmpty(valeTicketBuilder.get_localidad())) {
                        if (_printer.get_longitudLinea() - valeTicketBuilder.get_localidad().length() < 2) {
                            _printer.printOpposite(valeTicketBuilder.get_localidad(), "");
                        } else
                            _printer.printLine(valeTicketBuilder.get_localidad());
                    }

                    _printer.printLine("Terminal ID: " + valeTicketBuilder.get_terminal());

                    //_printer.format(1, 1, Printer.ALINEACION.LEFT);
                    _printer.printOpposite(valeTicketBuilder.get_fecha(), valeTicketBuilder.get_hora());
                    _printer.lineFeed();

                    _printer.format(1, 1, Printer.ALINEACION.CENTER);
                    _printer.printLine("VALE ANULADO: " + valeTicketBuilder.get_nro_operacion());
                    _printer.lineFeed();


                    //total
                    _printer.format(2, 1, Printer.ALINEACION.LEFT);
                    _printer.printOpposite("TOTAL", "$ -" + valeTicketBuilder.get_total());


                    /*if (!TextUtils.isEmpty(valeTicketBuilder.get_header())) {
                        _printer.format(1, 2, Printer.ALINEACION.CENTER);
                        _printer.printLine(valeTicketBuilder.get_header());
                    }*/

                    _printer.format(1, 1, Printer.ALINEACION.CENTER);

                    if (valeTicketBuilder.is_isDemoMode()) {
                        _printer.printOpposite(valeTicketBuilder.get_resolucion(), "");
                    }
                    /*else {
                        _printer.printOpposite("VALE AUTORIZADO SII -", "v" + valeTicketBuilder.get_app_version());
                        _printer.printOpposite(valeTicketBuilder.get_resolucion(), "");
                    }*/

                    _printer.footer();

                    _printer.end();
                }
            };

            //iniciamos la printer
            if (!_printer.initialize()) {

                _printer.end();

                throw new Exception("Error al inicializar");

            }

        } catch (Exception e) {
            Log.e("ValeAnuladoPrintTask", e.getMessage());
        }


    }

}
