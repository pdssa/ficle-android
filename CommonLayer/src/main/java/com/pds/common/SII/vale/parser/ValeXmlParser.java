package com.pds.common.SII.vale.parser;

import android.util.Xml;

import com.pds.common.SII.vale.model.Operacion;
import com.pds.common.SII.vale.model.Vale;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

/**
 * Created by Hernan on 23/06/2015.
 */
public class ValeXmlParser {
    // sin namespaces
    private static final String ns = null;

    public Vale parse(String in) throws XmlPullParserException, Exception {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(new StringReader(in));
            parser.nextTag();
            return readXML(parser);
        }
        catch (Exception ex){
            throw ex;
        }
        finally {
            //in.close();
        }
    }

    private Vale readXML(XmlPullParser parser) throws XmlPullParserException, IOException {

        Vale vale = new Vale();
        String text="";

        // Iniciamos buscando el tag de inicio
        parser.require(XmlPullParser.START_TAG, ns, "VALE");
        String tag = parser.getName();

        if (tag.equalsIgnoreCase("VALE")) {

            int eventType = parser.getEventType();
            while(eventType != XmlPullParser.END_DOCUMENT)
            {
                String tagname = parser.getName();

                switch (eventType) {
                    case XmlPullParser.START_TAG: {
                        if(tagname.equalsIgnoreCase("VALE")) {
                            vale = new Vale();
                        }
                    }break;
                    case XmlPullParser.TEXT: {
                        text = parser.getText();
                    }break;
                    case XmlPullParser.END_TAG: {
                        if(tagname.equalsIgnoreCase("NROVALE")) {
                            vale.NRO_UNICO_VALE = text;
                        }
                        else if(tagname.equalsIgnoreCase("NRO_FOLIO")) {
                            vale.NRO_FOLIO = text;
                        }
                        else if(tagname.equalsIgnoreCase("NROOPERACION")) {
                            vale.COD_OPERACION = text;
                        }
                        else if(tagname.equalsIgnoreCase("FECHAHORA")) {
                            vale.s_FECHA_HORA = text;
                        }
                        else if(tagname.equalsIgnoreCase("MONTOAFECTO")) {
                            vale.TOTAL_AFECTO = Double.parseDouble(text);
                        }
                        else if(tagname.equalsIgnoreCase("MONTOEXENTO")) {
                            vale.TOTAL_EXENTO = Double.parseDouble(text);
                        }
                        else if(tagname.equalsIgnoreCase("HASH")) {
                            vale.HASH_TRX = text;
                        }
                    }break;
                    default:
                        break;

                }

                eventType = parser.next();
            }
        }
        return vale;
    }
}
