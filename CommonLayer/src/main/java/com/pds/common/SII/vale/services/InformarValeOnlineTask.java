package com.pds.common.SII.vale.services;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.AsyncTask;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;

import com.pds.common.Logger;
import com.pds.common.SII.vale.model.CambioUser;
import com.pds.common.SII.vale.model.Operacion;
import com.pds.common.SII.vale.model.Vale;
import com.pds.common.SII.vale.parser.OperacionXmlParser;
import com.pds.common.SII.vale.util.ValeConst;
import com.pds.common.SII.vale.util.ValeError;
import com.pds.common.SII.vale.util.ValeUtils;
import com.pds.common.network.MySSLSocketFactory;
import com.pds.common.pref.PDVConfig;
import com.pds.common.util.ConnectivityUtils;
import com.pds.common.util.EscapeChars;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyStore;

/**
 * Created by Hernan on 23/06/2015.
 */
public class InformarValeOnlineTask extends AsyncTask<Vale, String, String> {

    private ProgressDialog dialog;
    private Context context;

    private static final String MENSAJE_OK = "ok";
    private static final String MENSAJE_CAMBIAR_USER = "___1";

    private Vale vale;

    private InformarValeOnlineListener listener;

    public InformarValeOnlineTask(Context _context, InformarValeOnlineListener _listener) {
        context = _context;
        dialog = new ProgressDialog(context);
        listener = _listener;
    }

    private boolean hasToSendUserID;
    private String lastUserID;
    private boolean hasToSendUserID() {
        Cursor cursor = context.getContentResolver().query(PDVConfig.CONTENT_PROVIDER, new String[]{PDVConfig.PDV_LAST_USER_ID_VALE}, null, null, null);

        if (cursor.moveToFirst()) {
            int i = cursor.getColumnIndex(PDVConfig.PDV_LAST_USER_ID_VALE);
            lastUserID = (i != -1 ? cursor.getString(i) : "").trim();

            return !TextUtils.isEmpty(lastUserID);//si es vacio, no tenemos que enviar
        }

        return false;
    }

    @Override
    protected void onPreExecute() {
        Log.d("vale", "pre");
        //iniciamos un mensaje para el usuario
        dialog = new ProgressDialog(context);
        dialog.setMessage("Emitiendo VALE ELECTRONICO en línea. Aguarde por favor...");
        dialog.setIndeterminate(false);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        //evaluamos si es necesario cambiar la clave del usuario primero
        hasToSendUserID = hasToSendUserID();

    }

    @Override
    protected String doInBackground(Vale... params) {
        try {
            vale = params[0];

            if(vale.TOTAL() == 0){
                throw new Exception("No puede emitir un VALE con total igual a cero");
            }

            //Log.d("vale", "doinbackground");
            if (!ConnectivityUtils.isOnline(context)) {
                return ConnectivityUtils.SIN_CONEXION_UP;
            }

            //Log.d("vale", "doinbackground2");
            if (hasToSendUserID) {
                return MENSAJE_CAMBIAR_USER;
            }
            else {

                Enviar(vale); //enviamos el vale

                return MENSAJE_OK;
            }

        } catch (Exception e) {
            return (TextUtils.isEmpty(e.getMessage()) ? "Error desconocido" : e.getMessage()) ;
        }
    }

    @Override
    protected void onProgressUpdate(String... values) {
        Log.d("vale", "update: " + values[0]);dialog.setMessage(values[0]);
    }

    @Override
    protected void onPostExecute(String success) {
        Log.d("vale", "post ");
        /*if(success.equalsIgnoreCase(MENSAJE_OK))//si está OK, ponemos el mensaje de "imprimiendo..." para que se quede ese mensaje en pantalla
            dialog.setMessage("Imprimiendo VALE ELECTRONICO. Aguarde por favor...");*/

        if (dialog.isShowing())
            dialog.dismiss();

        if (!success.equalsIgnoreCase(MENSAJE_OK)) {
            if(success.equalsIgnoreCase(MENSAJE_CAMBIAR_USER)) {
                //forzadamente tenemos que enviar la clave primero
                CambioUser cambioUser = new CambioUser(vale, lastUserID, vale.USER_ID);

                new CambiarUserIDOnlineTask(this.context, new CambiarUserIDOnlineTask.CambiarUserIDOnlineListener() {
                    @Override
                    public void onClaveModificada() {
                        //reejecutamos el envío....
                        new InformarValeOnlineTask(context, listener).execute(vale);
                    }

                    @Override
                    public void onError() {
                        //cortamos el proceso
                        AlertMessage("No ha sido posible habilitar la clave del usuario para emitir vale");
                    }
                }).execute(cambioUser);
            }
            else
                AlertMessage(success);//mostramos mensaje de error
        }
        else
            onValeGenerado(ValeConst.TIPO_DOCUMENTO, vale.NRO_FOLIO, Long.parseLong(vale.COD_OPERACION),vale.NRO_UNICO_VALE, vale.HASH_TRX);
    }

    @Override
    protected void onCancelled(String s) {
        AlertMessage(s);
    }

    private void AlertMessage(String message) {

        Logger.RegistrarEvento(context, "e", "VALE Error", message);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setTitle("REGISTRAR VALE ELECTRÓNICO");
        builder.setMessage(Html.fromHtml("Se ha producido un error al intentar generar el Vale Electrónico:" + "<br/><b>" + message + "</b>"));

        /*if(message.equalsIgnoreCase(ConnectivityUtils.SIN_CONEXION_UP) || message.equalsIgnoreCase(ConnectivityUtils.SIN_RESPUESTA)){
            //error de comunicacion

        }*/


        builder.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface _dialog, int which) {
                listener.onValeError();

                _dialog.dismiss();
            }
        });


        /*
        builder.setPositiveButton("EMITIR MANUALMENTE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface _dialog, int which) {
                listener.onValeError(true);

                _dialog.dismiss();
            }
        });

        builder.setNegativeButton("ANULAR VENTA", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface _dialog, int which) {
                listener.onValeError(false);

                _dialog.dismiss();
            }
        });*/
        builder.setCancelable(false);
        builder.show();
    }


    private void Enviar(Vale vale) throws Exception {

        try {Log.d("vale", "doinbackground3");
            String result = PostDataToServer(vale);
            Log.d("vale", "doinbackground4");
            result = EscapeChars.fromXML(result);

            publishProgress("Leyendo respuesta del servidor. Aguarde por favor...");

            Operacion op = ParseXML(result);
            Log.d("vale", "doinbackground5");

            vale.NRO_FOLIO = op.NRO_FOLIO;
            vale.COD_OPERACION = op.COD_OPERACION;
            vale.HASH_TRX = op.HASH_TRX;
            vale.NRO_UNICO_VALE = op.NRO_UNICO_VALE;

        } catch (Exception ex) {
            throw ex;
        }

    }


    private String PostDataToServer(Vale vale) throws Exception{

        // 1. create HttpClient
        HttpClient httpclient = new DefaultHttpClient();
        HttpParams params = httpclient.getParams();
        HttpConnectionParams.setConnectionTimeout(params, ValeConst.CONNECTION_TIMEOUT);
        HttpConnectionParams.setSoTimeout(params, ValeConst.SOCKET_TIMEOUT);

        // 2. create HttpsClient
        KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
        trustStore.load(null, null);

        MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
        sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

        ClientConnectionManager ccm = httpclient.getConnectionManager();

        SchemeRegistry sr = ccm.getSchemeRegistry();
        sr.register(new Scheme("https", sf, 443));

        DefaultHttpClient httpsclient = new DefaultHttpClient(ccm, httpclient.getParams());

        // 2. make POST request to the given URL
        HttpPost httpPost = new HttpPost(ValeConst.getURL_WS(context) + ValeConst.METHOD_INFORMAR_VALE);

        // 3. build POST content
        //RUT_EMISOR=string&TERMINAL_ID=string&NET_ID=string&IMSI=string&VERSION=string&USER_ID=string&FECHA_HORA=string&TOTAL_AFECTO=string&TOTAL_EXENTO=string&ULT_COD_OPERACION=string
        String content = vale.toString();

        // 4. set content to StringEntity
        StringEntity se = new StringEntity(content);

        // 5. set httpPost Entity
        httpPost.setEntity(se);

        // 6. Set some headers to inform server about the type of the content
        httpPost.setHeader("Content-type", "application/x-www-form-urlencoded");

        // 7. Execute POST request to the given URL
        HttpResponse httpResponse = null;
        try {
            httpResponse = httpsclient.execute(httpPost);
        }
        catch (org.apache.http.conn.HttpHostConnectException e){
            throw new Exception(ConnectivityUtils.SIN_RESPUESTA);
        }
        catch (org.apache.http.client.ClientProtocolException e){
            throw new Exception(ConnectivityUtils.SIN_RESPUESTA);
        }


        // 8. receive response as inputStream
        int codeResponse = httpResponse.getStatusLine().getStatusCode();
        InputStream inputStream = null;
        inputStream = httpResponse.getEntity().getContent();

        String response = "";
        if (inputStream == null)
            throw new Exception("Error al obtener respuesta del servidor");
        else {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            response = "";
            while ((line = bufferedReader.readLine()) != null)
                response += line;

            inputStream.close();
        }

        if (codeResponse == HttpStatus.SC_OK) {
            //OK => obtenemos la respuesta
            return response;
        } else {
            //NO OK => devolvemos excepcion para el error obtenido
            throw new Exception(ValeError.getMessageError(codeResponse, response));
        }

    }

    private static Operacion ParseXML(String xmlIn) throws Exception {
        OperacionXmlParser parser = new OperacionXmlParser();
        return parser.parse(new ByteArrayInputStream(xmlIn.getBytes("UTF-8")));
    }


    private void onValeGenerado(String tipo_doc, String nro_folio, long cod_operacion, String nro_unico_vale, String hash_trx){

        //guardamos el ultimo trace
        ValeUtils.SetUltimoCodOperacion(context, context.getContentResolver(), String.valueOf(cod_operacion));//--> actualizamos el ultimo traceid

        listener.onValeGenerado(tipo_doc, nro_folio, cod_operacion, nro_unico_vale, hash_trx);
    }

    public interface InformarValeOnlineListener {
        void onValeGenerado(String tipo_doc, String nro_folio, long cod_operacion, String nro_unico_vale, String hash_trx);

        void onValeError();
    }
}
