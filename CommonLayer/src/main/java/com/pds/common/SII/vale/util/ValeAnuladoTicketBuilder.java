package com.pds.common.SII.vale.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;

import com.pds.common.Config;
import com.pds.common.Formato;
import com.pds.common.SII.vale.model.Vale;
import com.pds.common.VentasHelper;
import com.pds.common.dao.VentaDao;
import com.pds.common.model.Venta;
import com.pds.common.model.VentaDetalle;

import java.util.ArrayList;
import java.util.List;

import android_serialport_api.LineaTicket;

/**
 * Created by Hernan on 22/07/2016.
 */
public class ValeAnuladoTicketBuilder {

    private Context context;

    private String _header;

    //datos comercio
    private String _razonSocial;
    private String _nombreFantasia;
    private String _direccion;
    private String _localidad;
    private String _clavefiscal;
    private String _terminal;
    private boolean _isDemoMode;

    //datos del vale
    private String _fecha;
    private String _hora;
    private String _nro_operacion;
    private String _total;
    private List<String> _giro;
    private String _resolucion;

    public void set_total(String _total) {
        this._total = _total;
    }

    public void set_nro_operacion(String _nro_operacion) {
        this._nro_operacion = _nro_operacion;
    }

    public void set_hora(String _hora) {
        this._hora = _hora;
    }

    public void set_fecha(String _fecha) {
        this._fecha = _fecha;
    }

    public void set_header(String _header) {
        this._header = _header;
    }

    public String get_header() {
        return _header;
    }

    public String get_razonSocial() {
        return _razonSocial;
    }

    public String get_nombreFantasia() {
        return _nombreFantasia;
    }

    public String get_direccion() {
        return _direccion;
    }

    public String get_localidad() {
        return _localidad;
    }

    public String get_clavefiscal() {
        return _clavefiscal;
    }

    public String get_terminal() {
        return _terminal;
    }

    public boolean is_isDemoMode() {
        return _isDemoMode;
    }

    public String get_fecha() {
        return _fecha;
    }

    public String get_hora() {
        return _hora;
    }

    public String get_nro_operacion() {
        return _nro_operacion;
    }

    public String get_total() {
        return _total;
    }

    public List<String> get_giro() {
        return _giro;
    }

    public String get_resolucion() {
        return _resolucion;
    }

    public ValeAnuladoTicketBuilder() {
        this._header = "";
        this._giro = new ArrayList<String>();
    }

    public ValeAnuladoTicketBuilder(Context context, Config config) {
        this();

        this.context = context;

        this._razonSocial = config.RAZON_SOCIAL;
        this._nombreFantasia = config.COMERCIO;
        this._direccion = config.DIRECCION;
        this._localidad = config.LOCALIDAD;
        this._clavefiscal = config.CLAVEFISCAL;
        this._terminal = config.SII_TERMINAL_ID;
        this._isDemoMode = config.isValeModalidadDemo();

        if (!TextUtils.isEmpty(config.SII_GIRO1))
            this._giro.add(config.SII_GIRO1);
        if (!TextUtils.isEmpty(config.SII_GIRO2))
            this._giro.add(config.SII_GIRO2);
        if (!TextUtils.isEmpty(config.SII_GIRO3))
            this._giro.add(config.SII_GIRO3);

        this._resolucion = config.SII_RESOLUCION_VALE;
    }

    public void LoadVenta(Venta venta, Formato.Decimal_Format FORMATO_DECIMAL) {

        //ticket.set_header(header);

        set_fecha(Formato.FormateaDate(venta.getFechaHora_Date(), Formato.DateTwoDigFormatSlash));
        set_hora(venta.getHora());

        set_total(Formato.FormateaDecimal(venta.getTotal(), FORMATO_DECIMAL));

        set_nro_operacion(venta.getFc_folio());//imprimimos el numero unico de vale completo
        //ticket.set_nro_operacion(nro_op.substring(nro_op.length() - 5));

    }

    public void Print(String printerModel){
        ValeAnuladoPrintTask imprimirValeAnuladoTask = new ValeAnuladoPrintTask(this.context, printerModel);

        imprimirValeAnuladoTask.execute(this);
    }


}
