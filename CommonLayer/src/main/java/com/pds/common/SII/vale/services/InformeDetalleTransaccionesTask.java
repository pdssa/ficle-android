package com.pds.common.SII.vale.services;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.text.Html;

import com.pds.common.SII.vale.listener.InformeListener;
import com.pds.common.SII.vale.model.InformeReq;
import com.pds.common.SII.vale.util.ValeConst;
import com.pds.common.SII.vale.util.ValeError;
import com.pds.common.network.MySSLSocketFactory;
import com.pds.common.util.ConnectivityUtils;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyStore;

/**
 * Created by Hernan on 23/06/2015.
 */
public class InformeDetalleTransaccionesTask extends AsyncTask<InformeReq, String, String> {

    private ProgressDialog dialog;
    private Context context;

    //private static final String MENSAJE_OK = "ok";

    private boolean RESULT;

    private InformeListener listener;

    public InformeDetalleTransaccionesTask(Context _context, InformeListener _listener) {
        context = _context;
        dialog = new ProgressDialog(context);
        listener = _listener;
    }

    @Override
    protected void onPreExecute() {
        //iniciamos un mensaje para el usuario
        dialog = new ProgressDialog(context);
        dialog.setMessage("Iniciando consulta de transacciones. Aguarde por favor...");
        dialog.setIndeterminate(false);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    @Override
    protected String doInBackground(InformeReq... params) {
        try {

            if (!ConnectivityUtils.isOnline(context)) {
                return ConnectivityUtils.SIN_CONEXION;
            }

            //enviamos los datos para request del informe
            String response = PostDataToServer(params[0]);

            RESULT = true;

            return response;

        } catch (Exception e) {

            RESULT = false;

            return e.getMessage();
        }
    }

    @Override
    protected void onProgressUpdate(String... values) {
        dialog.setMessage(values[0]);
    }

    @Override
    protected void onPostExecute(String result) {
        if (dialog.isShowing())
            dialog.dismiss();

        if(!RESULT){
            AlertMessage(result);//mostramos mensaje de error
        }
        else{
            listener.onConsultaCompletada(result);//retornamos el resultado de la consulta
        }
    }

    @Override
    protected void onCancelled(String s) {
        AlertMessage(s);
    }

    private void AlertMessage(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setTitle("VALE ELECTRONICO: DETALLE DE TRANSACCIONES");
        builder.setMessage(Html.fromHtml("Se ha producido un error al intentar consultar el detalle de transacciones:" + "<br/><b>" + message + "</b>"));
        builder.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface _dialog, int which) {
                _dialog.dismiss();

                listener.onError();
            }
        });

        builder.setCancelable(false);
        builder.show();
    }

    private String PostDataToServer(InformeReq request) throws Exception {

        // 1. create HttpClient
        HttpClient httpclient = new DefaultHttpClient();
        HttpParams params = httpclient.getParams();
        HttpConnectionParams.setConnectionTimeout(params, ValeConst.CONNECTION_TIMEOUT);
        HttpConnectionParams.setSoTimeout(params, ValeConst.SOCKET_TIMEOUT);

        // 2. create HttpsClient
        KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
        trustStore.load(null, null);

        MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
        sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

        ClientConnectionManager ccm = httpclient.getConnectionManager();

        SchemeRegistry sr = ccm.getSchemeRegistry();
        sr.register(new Scheme("https", sf, 443));

        DefaultHttpClient httpsclient = new DefaultHttpClient(ccm, httpclient.getParams());

        // 2. make POST request to the given URL
        HttpPost httpPost = new HttpPost(ValeConst.getURL_WS(context) + ValeConst.METHOD_INFORME_TRANSACCIONES);

        // 3. build POST content
        //RUT_EMISOR=string&TERMINAL_ID=string&USER_ID=string
        String content = request.toString();

        // 4. set content to StringEntity
        StringEntity se = new StringEntity(content);

        // 5. set httpPost Entity
        httpPost.setEntity(se);

        // 6. Set some headers to inform server about the type of the content
        httpPost.setHeader("Content-type", "application/x-www-form-urlencoded");

        // 7. Execute POST request to the given URL
        HttpResponse httpResponse = httpsclient.execute(httpPost);

        // 8. receive response as inputStream
        int codeResponse = httpResponse.getStatusLine().getStatusCode();
        InputStream inputStream = null;
        inputStream = httpResponse.getEntity().getContent();

        String response = "";
        if (inputStream == null)
            throw new Exception("Error al obtener respuesta del servidor");
        else {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            response = "";
            while ((line = bufferedReader.readLine()) != null)
                response += line;

            inputStream.close();
        }

        if (codeResponse == HttpStatus.SC_OK) {
            //OK => obtenemos la respuesta
            return response;
        } else {
            //NO OK => devolvemos excepcion para el error obtenido
            throw new Exception(ValeError.getMessageError(codeResponse, response));
        }

    }


}
