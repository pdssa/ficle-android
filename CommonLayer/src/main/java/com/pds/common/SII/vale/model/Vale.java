package com.pds.common.SII.vale.model;

import android.content.ContentResolver;
import android.content.Context;

import com.pds.common.Config;
import com.pds.common.Formato;
import com.pds.common.SII.vale.util.ValeConst;
import com.pds.common.SII.vale.util.ValeUtils;

import java.util.Date;

/**
 * Created by Hernan on 23/06/2015.
 */
public class Vale {

    public String RUT;
    public String NUM_COMERCIO;
    public String TERMINAL_ID;
    public String NET_ID;
    public String IMSI;
    public String VERSION;
    public String USER_ID;
    public String s_FECHA_HORA;
    public Date FECHA_HORA;
    /*public String s_FECHA_HORA(){
        return Formatos.FormateaDate(this.FECHA_HORA, Formatos.SIIDateTimeFormat);
    }*/
    public Double TOTAL_AFECTO;

    public String s_TOTAL_AFECTO() {
        return Formato.FormateaDecimal(this.TOTAL_AFECTO, Formato.Decimal_Format.US);
    }

    public Double TOTAL_EXENTO;

    public String s_TOTAL_EXENTO() {
        return Formato.FormateaDecimal(this.TOTAL_EXENTO, Formato.Decimal_Format.US);
    }

    public Double TOTAL() {
        return TOTAL_AFECTO + TOTAL_EXENTO;
    }

    private String s_TOTAL() {
        return Formato.FormateaDecimal(TOTAL(), Formato.Decimal_Format.US);
    }

    public int MEDIO_PAGO;

    public String ULT_COD_OPERACION;

    public String NRO_FOLIO;
    public String COD_OPERACION;
    public String HASH_TRX;
    public String NRO_UNICO_VALE;

    private String CRC_CONFIG;

    public String CRC_CONFIG() {
        return CRC_CONFIG;
    }

    public void setDatosGenerales(Config config, String app_v, String user_id, String merchantNumber) {
        this.RUT = ValeUtils.UnformatRUT(config.CLAVEFISCAL);
        this.NUM_COMERCIO = merchantNumber;
        this.TERMINAL_ID = config.SII_TERMINAL_ID;
        this.NET_ID = config.getNET_ID();
        this.IMSI = config.IMSI;
        this.VERSION = app_v;
        this.USER_ID = user_id;
        this.CRC_CONFIG = ValeUtils.CalcularCRC(config, merchantNumber);
    }

    public void setDatosOperacion(Date fecha, Double total_afecto, Double total_exento, String ult_cod_operacion, int forma_pago) {
        this.FECHA_HORA = fecha;
        this.s_FECHA_HORA = Formato.FormateaDate(fecha, Formato.SIIDateTimeFormat);
        this.TOTAL_AFECTO = total_afecto;
        this.TOTAL_EXENTO = total_exento;
        this.ULT_COD_OPERACION = ult_cod_operacion;
        this.MEDIO_PAGO = forma_pago;
    }

    public Vale() {

    }

    public Vale(String rut, String nro_folio, String cod_op, double tot_afecto, double tot_exento, String hash, Date fecha, String num_comercio, String terminal_id) {
        this.RUT = ValeUtils.UnformatRUT(rut);
        this.NRO_FOLIO = nro_folio;
        this.COD_OPERACION = cod_op;
        this.TOTAL_AFECTO = tot_afecto;
        this.TOTAL_EXENTO = tot_exento;
        this.FECHA_HORA = fecha;
        this.s_FECHA_HORA = Formato.FormateaDate(fecha, Formato.SIIDateTimeFormat);
        this.NUM_COMERCIO = num_comercio;
        this.TERMINAL_ID = terminal_id;
        this.HASH_TRX = hash;
    }

    @Override
    public String toString() {
        return String.format("NUM_COMERCIO=%s&ID_TERMINAL=%s&RUT_COMERCIO=%s&&NET_ID=%s&IMSI=%s&VERSION=%s&USER_ID=%s&CRC_CONFIG=%s&FECHA_HORA=%s&TOTAL_AFECTO=%s&TOTAL_EXENTO=%s&FORMA_PAGO=%s&ULT_COD_OPERACION=%s",
                this.NUM_COMERCIO,
                this.TERMINAL_ID,
                this.RUT,
                this.NET_ID,
                this.IMSI,
                this.VERSION,
                this.USER_ID,
                this.CRC_CONFIG(),
                this.s_FECHA_HORA,
                this.s_TOTAL_AFECTO(),
                this.s_TOTAL_EXENTO(),
                this.MEDIO_PAGO,
                this.ULT_COD_OPERACION);
    }

    //vamos a generar el contenido del QR
    public String toQrContentString(Context context) {
        //https://www.valefiscal.cl/ConsultasQR/vale?RUT,NRO_FOLIO,NRO_OPERACION,TOTAL_VALE,FECHA_HORA,NUM_COMERCIO,ID_TERMINAL,HASH_TRX
        return String.format("%s?%s,%s,%s,%s,%s,%s,%s,%s",
                ValeConst.getURL_CONSULTAS(context),
                this.RUT,
                this.NRO_FOLIO,
                this.COD_OPERACION,
                s_TOTAL(),
                this.s_FECHA_HORA,
                this.NUM_COMERCIO,
                this.TERMINAL_ID,
                this.HASH_TRX);
    }
}
