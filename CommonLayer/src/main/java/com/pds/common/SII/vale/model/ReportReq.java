package com.pds.common.SII.vale.model;

import com.pds.common.Config;
import com.pds.common.Formato;
import com.pds.common.SII.vale.util.ValeUtils;

/**
 * Created by Hernan on 06/08/2015.
 */
public abstract class ReportReq {
    public String ID_TERMINAL;
    public String NUM_COMERCIO;
    public String RUT_COMERCIO;
    public String NET_ID;
    public String USER_ID;
    public String CRC_CONFIG;

    public ReportReq(String merchantNumber, Config config, String user_id) {
        this.RUT_COMERCIO = ValeUtils.UnformatRUT(config.CLAVEFISCAL);
        this.ID_TERMINAL = config.SII_TERMINAL_ID;
        this.NUM_COMERCIO = merchantNumber;
        this.USER_ID = user_id;
        this.NET_ID = config.getNET_ID();
        this.CRC_CONFIG = ValeUtils.CalcularCRC(config, merchantNumber);
    }

}
