package com.pds.common.SII.vale.model;

import com.pds.common.Config;

/**
 * Created by Hernan on 17/10/2016.
 */
public class MonthsTotalsReportReq extends ReportReq {
    private Boolean INCLUIR_DIAS;

    public MonthsTotalsReportReq(Config config, String user_id, Boolean incluir_dias, String merchantNumber)  {
        super(merchantNumber, config, user_id);

        INCLUIR_DIAS = incluir_dias;
    }

    @Override
    public String toString() {
        return String.format("NUM_COMERCIO=%s&ID_TERMINAL=%s&RUT_COMERCIO=%s&NET_ID=%s&USER_ID=%s&CRC_CONFIG=%s&INC_DIAS=%s",
                NUM_COMERCIO,
                ID_TERMINAL,
                RUT_COMERCIO,
                NET_ID,
                USER_ID,
                CRC_CONFIG,
                INCLUIR_DIAS ? "S" : "N");
    }
}
