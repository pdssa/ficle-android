package com.pds.common.SII.vale.listener;


import com.pds.common.SII.vale.model.InitTerminalResp;

/**
 * Created by Hernan on 06/08/2015.
 */
public interface InitTerminalListener {
    void onInitTerminalCompletado(InitTerminalResp resp);

    void onError(boolean reintentar);
}
