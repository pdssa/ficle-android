package com.pds.common.SII.vale.model;

/**
 * Created by Hernan on 20/01/2017.
 */
public class ActivarTerminalReq {
    public String TERMINAL_ID;
    public String COD_INSTALACION;
    public String NET_ID;
    public String IMSI;

    public ActivarTerminalReq(String terminalId, String codInstalacion, String netId, String imsi) {
        this.NET_ID = netId;
        this.IMSI = imsi;
        this.TERMINAL_ID = terminalId;
        this.COD_INSTALACION = codInstalacion;
    }

    @Override
    public String toString() {
        return String.format("ID_TERMINAL=%s&NET_ID=%s&COD_INSTALACION=%s&IMSI=%s",
                TERMINAL_ID,
                NET_ID,
                COD_INSTALACION,
                IMSI);
    }
}
