package com.pds.common.SII.vale.listener;

import com.pds.common.SII.vale.model.InformeResp;

/**
 * Created by Hernan on 06/08/2015.
 */
public interface InformeListener {
    public void onConsultaCompletada(String response);

    public void onConsultaCompletada(InformeResp response);

    public void onError();
}
