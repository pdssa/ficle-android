package com.pds.common.SII.vale.model;

import com.pds.common.Config;
import com.pds.common.Formato;
import com.pds.common.SII.vale.util.ValeConst;
import com.pds.common.SII.vale.util.ValeUtils;

/**
 * Created by Hernan on 06/08/2015.
 */
public class InformeReq {
    public String RUT_EMISOR;
    public String NUM_COMERCIO;
    public String TERMINAL_ID;
    public String USER_ID;
    public String NET_ID;
    public String CRC_CONFIG;
    public Integer MANUAL_CANT_AFECTO;
    public Integer MANUAL_CANT_EXENTO;
    public Double MANUAL_TOTAL_AFECTO;
    private String s_MANUAL_TOTAL_AFECTO() {
        return Formato.FormateaDecimal(this.MANUAL_TOTAL_AFECTO, Formato.Decimal_Format.US);
    }

    public Double MANUAL_TOTAL_EXENTO;
    private String s_MANUAL_TOTAL_EXENTO() {
        return Formato.FormateaDecimal(this.MANUAL_TOTAL_EXENTO, Formato.Decimal_Format.US);
    }

    public String MOTIVO_CIERRE;

    public InformeReq(Config config, String user_id, String merchantNumber) {
        this.RUT_EMISOR = ValeUtils.UnformatRUT(config.CLAVEFISCAL);
        this.TERMINAL_ID = config.SII_TERMINAL_ID;
        this.NUM_COMERCIO = merchantNumber;
        this.USER_ID = user_id;
        this.NET_ID = config.getNET_ID();
        this.CRC_CONFIG = ValeUtils.CalcularCRC(config, merchantNumber);
    }

    public InformeReq(Config config, String user_id, Integer cant_afecto_manuales, Double total_afecto_manual, Integer cant_exento_manuales, Double total_exento_manual, boolean cierreForzado, String merchantNumber) {
        this.RUT_EMISOR = ValeUtils.UnformatRUT(config.CLAVEFISCAL);
        this.TERMINAL_ID = config.SII_TERMINAL_ID;
        this.NUM_COMERCIO = merchantNumber;
        this.USER_ID = user_id;
        this.NET_ID = config.getNET_ID();
        this.CRC_CONFIG = ValeUtils.CalcularCRC(config, merchantNumber);
        this.MANUAL_CANT_AFECTO = cant_afecto_manuales;
        this.MANUAL_CANT_EXENTO = cant_exento_manuales;
        this.MANUAL_TOTAL_AFECTO = total_afecto_manual;
        this.MANUAL_TOTAL_EXENTO = total_exento_manual;
        this.MOTIVO_CIERRE = cierreForzado ? ValeConst.MODO_CIERRE_FORZADO : ValeConst.MODO_CIERRE_NORMAL;
    }

    public void setReqZ(Integer cant_afecto_manuales, Double total_afecto_manual, Integer cant_exento_manuales, Double total_exento_manual) {
        this.MANUAL_CANT_AFECTO = cant_afecto_manuales;
        this.MANUAL_CANT_EXENTO = cant_exento_manuales;
        this.MANUAL_TOTAL_AFECTO = total_afecto_manual;
        this.MANUAL_TOTAL_EXENTO = total_exento_manual;
    }

    @Override
    public String toString() {
        return String.format("NUM_COMERCIO=%s&ID_TERMINAL=%s&RUT_COMERCIO=%s&NET_ID=%s&USER_ID=%s&CRC_CONFIG=%s",
                NUM_COMERCIO,
                TERMINAL_ID,
                RUT_EMISOR,
                NET_ID,
                USER_ID,
                CRC_CONFIG);
    }

    public String toStringCierreZ() {
        return String.format("NUM_COMERCIO=%s&ID_TERMINAL=%s&RUT_COMERCIO=%s&NET_ID=%s&USER_ID=%s&CRC_CONFIG=%s&MANUAL_CANT_VALES_AFECTO=%s&MANUAL_TOTAL_AFECTO=%s&MANUAL_CANT_VALES_EXENTO=%s&MANUAL_TOTAL_EXENTO=%s&MOTIVO=%s",
                NUM_COMERCIO,
                TERMINAL_ID,
                RUT_EMISOR,
                NET_ID,
                USER_ID,
                CRC_CONFIG,
                MANUAL_CANT_AFECTO,
                s_MANUAL_TOTAL_AFECTO(),
                MANUAL_CANT_EXENTO,
                s_MANUAL_TOTAL_EXENTO(),
                MOTIVO_CIERRE
        );
    }
}
