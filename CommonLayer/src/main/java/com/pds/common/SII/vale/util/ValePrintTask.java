package com.pds.common.SII.vale.util;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.pds.common.Logger;
import com.pds.common.R;
import com.pds.common.pref.PDVConfig;
import com.pds.common.util.ContentProviders;

import java.util.Hashtable;

import android_serialport_api.LineaTicket;
import android_serialport_api.Printer;

/**
 * Created by Hernan on 13/07/2016.
 */
public class ValePrintTask extends AsyncTask<ValeTicketBuilder, Void, Boolean> {

    private ProgressDialog dialog;
    private Context context;
    private String printerModel;
    private Bitmap _qr;
    private ValeTicketBuilder valeTicketBuilder;
    boolean printerResult = true;

    public ValePrintTask(Context context, String printerModel) {
        this.context = context;
        this.printerModel = printerModel;
    }

    @Override
    protected void onPreExecute() {
        dialog = new ProgressDialog(context, R.style.ProgressBackgroundTransparent);
        dialog.setMessage("Imprimiendo Vale Electrónico, aguarde por favor...");
        dialog.setIndeterminate(false);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    @Override
    protected Boolean doInBackground(ValeTicketBuilder... params) {

        if (params.length < 1)
            return false;

        valeTicketBuilder = params[0];

        printVale();

        return printerResult;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        if (dialog.isShowing())
            dialog.dismiss();

        if (!result) {

            RegisterPrintValeError();

            AlertMessage();
        }
    }


    private void printVale() {
        try {



            final Printer _printer = Printer.getInstance(printerModel, context);

            _printer.delayActivated = true;
            _printer.printerCallback = new Printer.PrinterCallback2() {

                @Override
                public void onPrinterError() {
                    printerResult = false;

                    _printer.end();
                }

                @Override
                public void onPrinterReady() {
                    _printer.lineFeed();
                    _printer.lineFeed();

                    if (valeTicketBuilder.is_isDemoMode()) {
                        _printer.format(2, 1, Printer.ALINEACION.CENTER);
                        _printer.printLine("SIN VALIDEZ FISCAL");
                    } else {
                        _printer.format(2, 1, Printer.ALINEACION.CENTER);
                        _printer.printLine("VALIDO COMO BOLETA");
                    }


                    if (!TextUtils.isEmpty(valeTicketBuilder.get_header())) {
                        _printer.format(1, 2, Printer.ALINEACION.CENTER);
                        _printer.printLine(valeTicketBuilder.get_header());
                    }

                    _printer.format(1, 1, Printer.ALINEACION.CENTER);
                    _printer.printLine(valeTicketBuilder.get_razonSocial());
                    _printer.printLine("R.U.T.:" + ValeUtils.FormatRUT(valeTicketBuilder.get_clavefiscal()));

                    //actividades
                    for (String g : valeTicketBuilder.get_giro()) {
                        _printer.printLine(g);
                    }

                    _printer.printLine(valeTicketBuilder.get_nombreFantasia());

                    if (_printer.get_longitudLinea() - valeTicketBuilder.get_direccion().length() < 2) {
                        _printer.printOpposite(valeTicketBuilder.get_direccion(), "");
                    } else
                        _printer.printLine(valeTicketBuilder.get_direccion());

                    if (!TextUtils.isEmpty(valeTicketBuilder.get_localidad())) {
                        if (_printer.get_longitudLinea() - valeTicketBuilder.get_localidad().length() < 2) {
                            _printer.printOpposite(valeTicketBuilder.get_localidad(), "");
                        } else
                            _printer.printLine(valeTicketBuilder.get_localidad());
                    }

                    _printer.printLine("VALE NUM.: " + valeTicketBuilder.get_nro_operacion());
                    _printer.printLine("Terminal ID: " + valeTicketBuilder.get_terminal());

                    _printer.format(1, 1, Printer.ALINEACION.LEFT);
                    _printer.printOpposite(valeTicketBuilder.get_fecha(), valeTicketBuilder.get_hora());
                    _printer.lineFeed();

                    //montos afectos
                    if (valeTicketBuilder.getItemsAfectos().size() > 0) {

                        _printer.printLine("ITEMS AFECTOS:");

                        for (LineaTicket itemAfecto : valeTicketBuilder.getItemsAfectos()) {
                            //_printer.format(1, 1, ALINEACION.LEFT);

                            if (TextUtils.isEmpty(itemAfecto.get_cantidad())) {
                                //si no tiene cantidad, tratamos especial
                                int k = _printer.get_longitudLinea() - 18; //4 + 3 (" x ") + 10 + 1 (" ")

                                String descripcion = itemAfecto.get_descripcion().replace("\r\n", " ").replace("\n", " ").trim() + _printer.getPadding(k); //hacemos un padding para cortar el nombre del producto en caso de excederse
                                descripcion = descripcion.substring(0, k);

                                String subtotal = "          " + itemAfecto.get_subtotal(); //hacemos un padding de 10
                                subtotal = subtotal.substring(subtotal.length() - 10);

                                _printer.printOpposite("  " + descripcion, subtotal);

                            } else {

                                String cantidad = "    " + itemAfecto.get_cantidad(); //hacemos un padding con 4 espacios (izquierda)
                                cantidad = cantidad.substring(cantidad.length() - 4);

                                int k = _printer.get_longitudLinea() - 18; //4 + 3 (" x ") + 1 (" ") + 10

                                String descripcion = itemAfecto.get_descripcion().replace("\r\n", " ").replace("\n", " ").trim() + _printer.getPadding(k); //hacemos un padding para cortar el nombre del producto en caso de excederse
                                descripcion = descripcion.substring(0, k);

                                String subtotal = "          " + itemAfecto.get_subtotal(); //hacemos un padding de 10
                                subtotal = subtotal.substring(subtotal.length() - 10);

                                _printer.printOpposite(cantidad + " x " + descripcion, subtotal);
                            }
                        }

                    }

                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    //montos exentos
                    if (valeTicketBuilder.getItemsExentos().size() > 0) {

                        _printer.printLine("ITEMS NO AFECTOS:");

                        for (LineaTicket itemExento : valeTicketBuilder.getItemsExentos()) {
                            //_printer.format(1, 1, ALINEACION.LEFT);

                            if (TextUtils.isEmpty(itemExento.get_cantidad())) {
                                //si no tiene cantidad, tratamos especial
                                int k = _printer.get_longitudLinea() - 18; //4 + 3 (" x ") + 10 + 1 (" ")

                                String descripcion = itemExento.get_descripcion().replace("\r\n", " ").replace("\n", " ").trim() + _printer.getPadding(k); //hacemos un padding para cortar el nombre del producto en caso de excederse
                                descripcion = descripcion.substring(0, k);

                                String subtotal = "          " + itemExento.get_subtotal(); //hacemos un padding de 10
                                subtotal = subtotal.substring(subtotal.length() - 10);

                                _printer.printOpposite("  " + descripcion, subtotal);

                            } else {
                                String cantidad = "    " + itemExento.get_cantidad(); //hacemos un padding con 4 espacios (izquierda)
                                cantidad = cantidad.substring(cantidad.length() - 4);

                                int k = _printer.get_longitudLinea() - 18; //4 + 3 (" x ") + 10 + 1 (" ")

                                String descripcion = itemExento.get_descripcion().replace("\r\n", " ").replace("\n", " ").trim() + _printer.getPadding(k); //hacemos un padding para cortar el nombre del producto en caso de excederse
                                descripcion = descripcion.substring(0, k);

                                String subtotal = "          " + itemExento.get_subtotal(); //hacemos un padding de 10
                                subtotal = subtotal.substring(subtotal.length() - 10);

                                _printer.printOpposite(cantidad + " x " + descripcion, subtotal);
                            }
                        }

                    }

                    //total
                    _printer.lineFeed();
                    _printer.format(2, 1, Printer.ALINEACION.LEFT);
                    _printer.printOpposite("TOTAL", "$ " + valeTicketBuilder.get_total());

                    //_printer.cancelCurrentFormat();

                    //_printer.lineFeed();

                    //QR code
                    if (!TextUtils.isEmpty(valeTicketBuilder.get_qr_content())) {
                        if (_printer.isQrPrintSupported()) {
                            _printer.printQR(valeTicketBuilder.get_qr_content());
                        } else {
                            //QR code
                            if (!TextUtils.isEmpty(valeTicketBuilder.get_qr_content())) {

                                Hashtable<EncodeHintType, ErrorCorrectionLevel> hintMap = new Hashtable<EncodeHintType, ErrorCorrectionLevel>();
                                hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
                                try {
                                    _qr = new QRCodeWriter().encodeAsBitmap(valeTicketBuilder.get_qr_content(), BarcodeFormat.QR_CODE, 250, 250, hintMap);
                                } catch (WriterException e) {
                                    Log.e("ValePrintTask", e.getMessage());
                                }

                                if (_qr != null) {

                                    try {
                                        Thread.sleep(500);
                                        _printer.format(1, 1, Printer.ALINEACION.CENTER);
                                        _printer.printBitmap(_qr);
                                        Thread.sleep(300);
                                        //_printer.lineFeed();
                                    } catch (Exception e) {
                                        Log.e("ValePrintTask", e.getMessage());
                                    }

                                }
                            }
                        }
                    }

                    _printer.format(1, 1, Printer.ALINEACION.CENTER);
                    _printer.printLine(separateString(valeTicketBuilder.get_hash(), 4));
                    _printer.lineFeed();
                    if (!TextUtils.isEmpty(valeTicketBuilder.get_header())) {
                        _printer.format(1, 2, Printer.ALINEACION.CENTER);
                        _printer.printLine(valeTicketBuilder.get_header());
                    }

                    _printer.format(1, 1, Printer.ALINEACION.CENTER);

                    if (valeTicketBuilder.is_isDemoMode()) {
                        _printer.printOpposite(valeTicketBuilder.get_resolucion(), "");
                    } else {
                        _printer.printOpposite("VALE AUTORIZADO SII -", "v" + valeTicketBuilder.get_app_version());
                        _printer.printOpposite(valeTicketBuilder.get_resolucion(), "");
                    }


                /*if (_resolucion.length() == _printer.get_longitudLinea())
                    _printer.print(_resolucion);
                else
                    _printer.printLine(_resolucion);*/

                    _printer.footer();

                    //abrimos el cajon
                    if (_printer.isCashDrawerActivated()) {
                        _printer.openCashDrawer();
                    }

                    _printer.end();
                }
            };

            //iniciamos la printer
            if (!_printer.initialize()) {

                _printer.end();

                throw new Exception("Error al inicializar");

            }

        } catch (Exception e) {
            printerResult = false;

            e.printStackTrace();
            Logger.RegistrarEvento(context, "e", "ValePrintTask", (e.getMessage() == null ? "NullException" : e.getMessage()));
        }

    }

    private String separateString(String input, int groupSize) {

        String output = "";

        int i = 0;
        while (i < input.length()) {
            if (i % groupSize == 0)
                output += " ";
            output += input.substring(i, ++i);
        }

        return output.trim();
    }

    private void RegisterPrintValeError() {
        try {

            int countPrintValeError = 0;

            if (!ContentProviders.ProviderIsAvailable(context, PDVConfig.CONTENT_PROVIDER.getAuthority()))
                throw new Exception("Provider PDV_Config no disponible");

            Cursor cursor = context.getContentResolver().query(PDVConfig.CONTENT_PROVIDER, new String[]{PDVConfig.PDV_IMPRESION_VALE_ERROR}, null, null, null);
            int i = cursor.getColumnIndex(PDVConfig.PDV_IMPRESION_VALE_ERROR);

            if (cursor.moveToFirst()) {
                if (i != -1)
                    countPrintValeError = cursor.getInt(i);
            }

            ContentValues cv = new ContentValues();
            cv.put(PDVConfig.PDV_IMPRESION_VALE_ERROR, countPrintValeError + 1);
            context.getContentResolver().insert(PDVConfig.CONTENT_PROVIDER, cv);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void AlertMessage() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setTitle("VALE ELECTRONICO: IMPRESIÓN");
        builder.setMessage(Html.fromHtml("Se ha detectado un posible error de impresión<br/>¿Desea reintentar la impresión?"));
        builder.setPositiveButton("SI", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface _dialog, int which) {
                _dialog.dismiss();

                //mandamos a reimprimir
                new ValePrintTask(context, printerModel).execute(valeTicketBuilder);
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface _dialog, int which) {
                _dialog.dismiss();
            }
        });

        builder.setCancelable(false);
        builder.show();
    }
}
