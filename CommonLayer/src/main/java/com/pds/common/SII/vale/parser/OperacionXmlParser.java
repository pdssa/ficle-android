package com.pds.common.SII.vale.parser;

import android.util.Xml;

import com.pds.common.SII.vale.model.Operacion;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Hernan on 23/06/2015.
 */
public class OperacionXmlParser {
    // sin namespaces
    private static final String ns = null;

    public Operacion parse(InputStream in) throws XmlPullParserException, Exception {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readXML(parser);
        }
        catch (Exception ex){
            throw ex;
        }
        finally {
            in.close();
        }
    }

    private Operacion readXML(XmlPullParser parser) throws XmlPullParserException, IOException {

        Operacion operacion = new Operacion();
        String text="";

        // Iniciamos buscando el tag de inicio
        parser.require(XmlPullParser.START_TAG, ns, "VALE_ONLINE");
        String tag = parser.getName();

        if (tag.equalsIgnoreCase("VALE_ONLINE")) {

            int eventType = parser.getEventType();
            while(eventType != XmlPullParser.END_DOCUMENT)
            {
                String tagname = parser.getName();

                switch (eventType) {
                    case XmlPullParser.START_TAG: {
                        if(tagname.equalsIgnoreCase("VALE_ONLINE")) {
                            operacion = new Operacion();
                        }
                    }break;
                    case XmlPullParser.TEXT: {
                        text = parser.getText();
                    }break;
                    case XmlPullParser.END_TAG: {
                        if(tagname.equalsIgnoreCase("COD_OPERACION")) {
                            operacion.COD_OPERACION = text;
                        }
                        else if(tagname.equalsIgnoreCase("NRO_FOLIO")) {
                            operacion.NRO_FOLIO = text;
                        }
                        else if(tagname.equalsIgnoreCase("NRO_UNICO_VALE")) {
                            operacion.NRO_UNICO_VALE = text;
                        }
                        else if(tagname.equalsIgnoreCase("HASH_TRX")) {
                            operacion.HASH_TRX = text;
                        }
                    }break;
                    default:
                        break;

                }

                eventType = parser.next();
            }
        }
        return operacion;
    }
}
