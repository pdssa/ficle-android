package com.pds.common.SII.vale.services;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.text.Html;

import com.pds.common.Logger;
import com.pds.common.SII.vale.model.CambioUser;
import com.pds.common.SII.vale.util.ValeConst;
import com.pds.common.SII.vale.util.ValeError;
import com.pds.common.network.MySSLSocketFactory;
import com.pds.common.pref.PDVConfig;
import com.pds.common.util.ConnectivityUtils;
import com.pds.common.util.ContentProviders;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyStore;

/**
 * Created by Hernan on 23/06/2015.
 */
public class CambiarUserIDOnlineTask extends AsyncTask<CambioUser, String, String> {

    private ProgressDialog dialog;
    private Context context;

    private static final String MENSAJE_OK = "ok";

    private CambiarUserIDOnlineListener listener;

    private CambioUser cambioUser;

    public CambiarUserIDOnlineTask(Context _context, CambiarUserIDOnlineListener _listener) {
        context = _context;
        dialog = new ProgressDialog(context);
        listener = _listener;
    }

    private void grabarLastUserID(String lastuserid) {

        if (ContentProviders.ProviderIsAvailable(context, PDVConfig.CONTENT_PROVIDER.getAuthority())) {

            ContentValues cv = new ContentValues();

            cv.put(PDVConfig.PDV_LAST_USER_ID_VALE, lastuserid);

            context.getContentResolver().insert(PDVConfig.CONTENT_PROVIDER, cv);
        }
    }

    @Override
    protected void onPreExecute() {
        //iniciamos un mensaje para el usuario
        dialog = new ProgressDialog(context);
        dialog.setMessage("Modificando clave del comercio. Aguarde por favor...");
        dialog.setIndeterminate(false);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    @Override
    protected String doInBackground(CambioUser... params) {
        try {

            if (!ConnectivityUtils.isOnline(context)) {
                return ConnectivityUtils.SIN_CONEXION;
            }

            cambioUser = params[0];

            //enviamos el cambio de clave
            return PostDataToServer(cambioUser);

        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @Override
    protected void onProgressUpdate(String... values) {
        dialog.setMessage(values[0]);
    }

    @Override
    protected void onPostExecute(String success) {
        if (dialog.isShowing())
            dialog.dismiss();

        if (!success.equalsIgnoreCase(MENSAJE_OK)) {

            grabarLastUserID(cambioUser.USER_ID_ACTUAL);//guardamos para q se vuelva a enviar en la siguiente trx

            AlertMessage(success);//mostramos mensaje de error
        }
        else {

            grabarLastUserID("");//limpiamos el user_id para q no se vuelva a enviar

            Logger.RegistrarEvento(context, "i", "VALE-CAMBIO CLAVE", "OK");

            listener.onClaveModificada();

        }
    }

    @Override
    protected void onCancelled(String s) {
        AlertMessage(s);
    }

    private void AlertMessage(String message) {

        Logger.RegistrarEvento(context, "e", "VALE-CAMBIO CLAVE", message);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("CAMBIO CLAVE COMERCIO");
        builder.setMessage(Html.fromHtml("Se ha producido un error al intentar modificar la clave del comercio:" + "<br/>" + message));
        builder.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface _dialog, int which) {
                listener.onError();

                _dialog.dismiss();
            }
        });

        builder.setCancelable(false);
        builder.show();
    }

    private String PostDataToServer(CambioUser user) throws Exception {

        // 1. create HttpClient
        HttpClient httpclient = new DefaultHttpClient();
        HttpParams params = httpclient.getParams();
        HttpConnectionParams.setConnectionTimeout(params, ValeConst.CONNECTION_TIMEOUT);
        HttpConnectionParams.setSoTimeout(params, ValeConst.SOCKET_TIMEOUT);

        // 2. create HttpsClient
        KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
        trustStore.load(null, null);

        MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
        sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

        ClientConnectionManager ccm = httpclient.getConnectionManager();

        SchemeRegistry sr = ccm.getSchemeRegistry();
        sr.register(new Scheme("https", sf, 443));

        DefaultHttpClient httpsclient = new DefaultHttpClient(ccm, httpclient.getParams());

        // 2. make POST request to the given URL
        HttpPost httpPost = new HttpPost(ValeConst.getURL_WS(context) + ValeConst.METHOD_CAMBIO_USER);

        // 3. build POST content
        //NUM_COMERCIO=string&ID_TERMINAL=string&RUT_COMERCIO=string&NET_ID=string&IMSI=string&USER_ID_ACTUAL=string&CRC_CONFIG=string&USER_ID_NUEVO=string
        String content = user.toString();

        // 4. set content to StringEntity
        StringEntity se = new StringEntity(content);

        // 5. set httpPost Entity
        httpPost.setEntity(se);

        // 6. Set some headers to inform server about the type of the content
        httpPost.setHeader("Content-type", "application/x-www-form-urlencoded");

        // 7. Execute POST request to the given URL
        HttpResponse httpResponse = httpsclient.execute(httpPost);

        // 8. receive response as inputStream
        int codeResponse = httpResponse.getStatusLine().getStatusCode();
        InputStream inputStream = null;
        inputStream = httpResponse.getEntity().getContent();

        String response = "";
        if (inputStream == null)
            throw new Exception("Error al obtener respuesta del servidor");
        else {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            response = "";
            while ((line = bufferedReader.readLine()) != null)
                response += line;

            inputStream.close();
        }

        if (codeResponse == HttpStatus.SC_OK) {
            //OK => obtenemos la respuesta
            return MENSAJE_OK;
        } else {
            //NO OK => devolvemos excepcion para el error obtenido
            throw new Exception(ValeError.getMessageError(codeResponse, response));
        }

    }

    public interface CambiarUserIDOnlineListener {
        void onClaveModificada();

        void onError();
    }
}
