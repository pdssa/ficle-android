package com.pds.common.SII.vale.listener;

import com.pds.common.SII.vale.model.InformeResp;

/**
 * Created by Hernan on 06/08/2015.
 */
public interface RequiereCierreZListener {
    void onConsultaCompletada(boolean requiereCierreZ);

    void onError();
}
