package com.pds.common.SII.vale.services;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.Html;
import android.text.TextUtils;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.ConfigHelper;
import com.pds.common.Formatos;
import com.pds.common.Logger;
import com.pds.common.R;
import com.pds.common.SII.vale.listener.InitTerminalListener;
import com.pds.common.SII.vale.model.InitTerminalReq;
import com.pds.common.SII.vale.model.InitTerminalResp;
import com.pds.common.SII.vale.parser.InitTerminalRespXmlParser;
import com.pds.common.SII.vale.util.ValeConst;
import com.pds.common.SII.vale.util.ValeError;
import com.pds.common.SII.vale.util.ValeUtils;
import com.pds.common.network.MySSLSocketFactory;
import com.pds.common.pref.ValeConfig;
import com.pds.common.util.ConnectivityUtils;
import com.pds.common.util.ContentProviders;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyStore;
import java.util.Date;

import android_serialport_api.Printer;

/**
 * Created by Hernan on 15/06/2016.
 */
public class InicializarTerminalTask extends AsyncTask<InitTerminalReq, String, String> {
    private final String mMerchantNumber;
    private ProgressDialog dialog;
    private Context context;
    private boolean isModalidadDemo;

    //private static final String MENSAJE_OK = "ok";

    private boolean RESULT;

    private InitTerminalListener listener;

    public InicializarTerminalTask(Context _context, boolean _isModalidadDemo, InitTerminalListener _listener, String merchantNumber) {
        context = _context;
        dialog = new ProgressDialog(context);
        listener = _listener;
        isModalidadDemo = _isModalidadDemo;
        mMerchantNumber = merchantNumber;
    }

    @Override
    protected void onPreExecute() {
        //iniciamos un mensaje para el usuario
        try {
            dialog = new ProgressDialog(context);
            dialog.setMessage(context.getString(isModalidadDemo ? R.string.vale_activando_demo : R.string.vale_activando));
            dialog.setIndeterminate(false);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected String doInBackground(InitTerminalReq... params) {
        try {

            if (!ConnectivityUtils.isOnline(context)) {
                return ConnectivityUtils.SIN_CONEXION;
            }

            //enviamos los datos para request del informe
            String response = PostDataToServer(params[0]);

            RESULT = true;

            return response;

        } catch (Exception e) {

            RESULT = false;

            return e.getMessage();
        }
    }


    @Override
    protected void onProgressUpdate(String... values) {
        dialog.setMessage(values[0]);
    }

    @Override
    protected void onCancelled(String s) {
        AlertMessage(s);
    }

    @Override
    protected void onPostExecute(String result) {
        try {
            if (dialog.isShowing())
                dialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!RESULT) {
            AlertMessage(result);//mostramos mensaje de error
        } else {

            try {

                InitTerminalResp resp = new InitTerminalRespXmlParser().parse(result);

                //habilitamos la emision de vales en PDV
                /*if (ContentProviders.ProviderIsAvailable(context, PDVConfig.CONTENT_PROVIDER.getAuthority())) {
                    ContentValues cv_pdv = new ContentValues();
                    cv_pdv.put(PDVConfig.PDV_EMITIR_VALE, true);
                    context.getContentResolver().insert(PDVConfig.CONTENT_PROVIDER, cv_pdv);
                }*/

                ContentValues cv = new ContentValues();
                cv.put(ConfigHelper.CONFIG_SII_VALE_STATUS, ValeConst.VALE_STATUS_INICIALIZADO);

                context.getContentResolver().update(
                        Uri.parse("content://com.pds.ficle.ep.config.contentprovider/configs/1"),
                        cv,
                        "",
                        new String[]{});

                Logger.RegistrarEvento(context, "i", "VALE-ACTIVACION", "OK");


                //inicializamos parametros VALE
                if (ContentProviders.ProviderIsAvailable(context, ValeConfig.CONTENT_PROVIDER.getAuthority())) {
                    ContentValues cv_vale = new ContentValues();
                    cv_vale.put(ValeConfig.VALE_ULT_TRACE, resp.ULT_TRACE_ID);
                    //guardamos fecha de ahora, para que empiece a contar para el cierre z automatico
                    cv_vale.put(ValeConfig.VALE_ULT_CIERRE_Z, Formatos.FormateaDate(new Date(), Formatos.DbDateTimeFormatTimeZone));
                    context.getContentResolver().insert(ValeConfig.CONTENT_PROVIDER, cv_vale);
                }


                // finalizó OK => imprimimos el ticket de resultado
                final Config c = new Config(context, true);
                String modelo = c.PRINTER_MODEL;

                try {

                    if (TextUtils.isEmpty(modelo) || modelo.equals("null"))
                        throw new Exception("Impresora no configurada");

                    final Printer printer = Printer.getInstance(modelo, context);

                    printer.printerCallback = new Printer.PrinterCallback() {
                        @Override
                        public void onPrinterReady() {
                            printer.sendSeparadorHorizontal('=');
                            printer.format(1, 1, Printer.ALINEACION.CENTER);
                            printer.printLine("VALE ELECTRONICO");
                            printer.printLine("ACTIVACION DEL SERVICIO");
                            printer.sendSeparadorHorizontal('=');

                            printer.printLine("R.U.T.: " + ValeUtils.FormatRUT(c.CLAVEFISCAL));
                            printer.printLine("RAZON SOCIAL: " + c.COMERCIO);
                            printer.printLine("MODO: " + c.getValeModalidadLabel());
                            printer.lineFeed();

                            printer.printLine("COMERCIO:    " + mMerchantNumber);
                            printer.printLine("TERMINAL:    " + c.SII_TERMINAL_ID);
                            printer.printLine("GIROS   : ");
                            if (!TextUtils.isEmpty(c.SII_GIRO1))
                                printer.printLine("  " + c.SII_GIRO1);
                            if (!TextUtils.isEmpty(c.SII_GIRO2))
                                printer.printLine("  " + c.SII_GIRO2);
                            if (!TextUtils.isEmpty(c.SII_GIRO3))
                                printer.printLine("  " + c.SII_GIRO3);

                            printer.lineFeed();
                            printer.printLine("VERSION:     " + ValeUtils.getAppVersion(context));
                            printer.printLine("IMEI:        " + c.IMEI());
                            printer.printLine("S/N:         " + c.ANDROID_ID);

                            if (!TextUtils.isEmpty(c.SIM_SERIALNUMBER)) {
                                printer.printLine("SERIE SIM: " + c.SIM_SERIALNUMBER);
                                printer.printLine("IMSI:      " + c.IMSI);
                                printer.printLine("SIM:       " + c.SIM_OPERATOR_NAME);
                            }
                            printer.lineFeed();
                            printer.sendSeparadorHorizontal('=');

                            printer.footer();
                            //printer.end();
                        }
                    };


                    if (!printer.initialize()) {
                        Toast.makeText(context, "Error al inicializar impresora", Toast.LENGTH_SHORT).show();
                        printer.end();
                    }

                } catch (Exception ex) {
                    Toast.makeText(context, "Error al imprimir: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
                }

                listener.onInitTerminalCompletado(resp);//retornamos el resultado de la consulta

            } catch (Exception ex) {
                AlertMessage(ex.toString());
            }
        }
    }

    private void AlertMessage(String message) {

        Logger.RegistrarEvento(context, "e", "VALE-ACTIVACION", message);


        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setTitle(context.getString(isModalidadDemo ? R.string.vale_activac_serv_demo : R.string.vale_activac_serv));
        builder.setMessage(Html.fromHtml("Se ha producido un error al intentar activar el servicio:" + "<br/><b>" + message + "</b>"));
        builder.setNeutralButton(context.getString(R.string.vale_inicializac_opt_ahora), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface _dialog, int which) {
                _dialog.dismiss();

                listener.onError(true);
            }
        });
        builder.setPositiveButton(context.getString(R.string.vale_inicializac_opt_luego), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface _dialog, int which) {
                _dialog.dismiss();

                listener.onError(false);
            }
        });

        builder.setCancelable(false);
        builder.show();
    }

    private String PostDataToServer(InitTerminalReq request) throws Exception {

        // 1. create HttpClient
        HttpClient httpclient = new DefaultHttpClient();
        HttpParams params = httpclient.getParams();
        HttpConnectionParams.setConnectionTimeout(params, ValeConst.CONNECTION_TIMEOUT);
        HttpConnectionParams.setSoTimeout(params, ValeConst.SOCKET_TIMEOUT);

        // 2. create HttpsClient
        KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
        trustStore.load(null, null);

        MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
        sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

        ClientConnectionManager ccm = httpclient.getConnectionManager();

        SchemeRegistry sr = ccm.getSchemeRegistry();
        sr.register(new Scheme("https", sf, 443));

        DefaultHttpClient httpsclient = new DefaultHttpClient(ccm, httpclient.getParams());

        // 2. make POST request to the given URL
        HttpPost httpPost = new HttpPost(ValeConst.getURL_WS(context) + ValeConst.METHOD_INIT_TERMINAL);

        // 3. build POST content
        //string NUM_COMERCIO, string ID_TERMINAL, string RUT_COMERCIO, string NET_ID, string IMSI, string USER_ID
        String content = request.toString();

        // 4. set content to StringEntity
        StringEntity se = new StringEntity(content);

        // 5. set httpPost Entity
        httpPost.setEntity(se);

        // 6. Set some headers to inform server about the type of the content
        httpPost.setHeader("Content-type", "application/x-www-form-urlencoded");

        // 7. Execute POST request to the given URL
        HttpResponse httpResponse = httpsclient.execute(httpPost);

        // 8. receive response as inputStream
        int codeResponse = httpResponse.getStatusLine().getStatusCode();
        InputStream inputStream = null;
        inputStream = httpResponse.getEntity().getContent();

        String response = "";
        if (inputStream == null)
            throw new Exception("Error al obtener respuesta del servidor");
        else {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            response = "";
            while ((line = bufferedReader.readLine()) != null)
                response += line;

            inputStream.close();
        }

        if (codeResponse == HttpStatus.SC_OK) {
            //OK => obtenemos la respuesta
            return response;
        } else {
            //NO OK => devolvemos excepcion para el error obtenido
            throw new Exception(ValeError.getMessageError(codeResponse, response));
        }
    }
}
