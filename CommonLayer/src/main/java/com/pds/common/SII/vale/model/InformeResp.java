package com.pds.common.SII.vale.model;

/**
 * Created by Hernan on 23/06/2015.
 */
public class InformeResp {
    //<TOTAL></TOTAL><TOTAL_AFECTO></TOTAL_AFECTO><TOTAL_NO_AFECTO></TOTAL_NO_AFECTO><VALE_INI></VALE_INI><VALE_FIN></VALE_FIN><ULT_LOTE></ULT_LOTE><ULT_TRACE_ID></ULT_TRACE_ID><PERIODO></PERIODO><TOT_ACUM></TOT_ACUM><CANT_VALES></CANT_VALES><DET_TIPO_PAGO>#<0>*<1></DET_TIPO_PAGO>
    public String TOTAL;
    public String TOTAL_AFECTO;
    public String TOTAL_NO_AFECTO;
    public String VALE_INI;
    public String VALE_FIN;
    public String ULT_LOTE;
    public String DET_TIPO_PAGO;
    public String ULT_TRACE_ID;
    public String PERIODO;
    public String TOT_ACUM;
    public String CANT_VALES;

    public InformeResp() {

    }
}
