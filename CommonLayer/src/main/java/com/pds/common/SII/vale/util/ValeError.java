package com.pds.common.SII.vale.util;

/**
 * Created by Hernan on 13/06/2016.
 */
public abstract class ValeError {

    static final int ERROR_GENERAL = 900;
    static final int TERMINAL_DESACTIVADO = 901;
    static final int USUARIO_NO_VALIDO = 903;
    static final int INCONSISTENCIA_DE_DATOS = 906;
    static final int USUARIO_DESCONOCIDO = 909;
    static final int TERMINAL_NO_VALIDO = 918;
    static final int TRANSACCION_INEXISTENTE = 920;
    static final int TRANSACCION_YA_ANULADA = 921;

    public static String getMessageError(int codeResponse, String response) {
        switch (codeResponse) {
            case TERMINAL_NO_VALIDO:
            case TERMINAL_DESACTIVADO: {
                //TERMINAL DESACTIVADO o NO VALIDO
                return "TERMINAL NO VALIDO. CONTACTE A SOPORTE TECNICO";
            }
            case USUARIO_NO_VALIDO:
            case USUARIO_DESCONOCIDO: {
                //PROBLEMAS CON USUARIO / PASSWORD
                return "USUARIO NO VALIDO. CONTACTE A SOPORTE TECNICO";
            }
            case INCONSISTENCIA_DE_DATOS:{
                //CRC INVALIDO
                return "ERROR DE SEGURIDAD. CONTACTE A SOPORTE TECNICO";
            }
            case TRANSACCION_INEXISTENTE:{
                return "TRANSACCION INEXISTENTE";
            }
            case TRANSACCION_YA_ANULADA:{
                return "TRANSACCION YA ANULADA";
            }
            case ERROR_GENERAL:{
                return response;
            }
            default:
                return String.format("Error al obtener respuesta del servidor: %s - %s", String.valueOf(codeResponse), response);
        }
    }

}
