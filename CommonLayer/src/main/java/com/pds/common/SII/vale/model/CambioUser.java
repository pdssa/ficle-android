package com.pds.common.SII.vale.model;

import com.pds.common.Config;
import com.pds.common.SII.vale.util.ValeUtils;

/**
 * Created by Hernan on 01/07/2015.
 */
public class CambioUser {
    public String RUT_COMERCIO;
    public String NUM_COMERCIO;
    public String TERMINAL_ID;
    public String NET_ID;
    public String IMSI;
    public String CRC_CONFIG;
    public String USER_ID_ACTUAL;
    public String USER_ID_NUEVO;

    public CambioUser(Config config, String CLAVE_ACTUAL, String CLAVE_NUEVA, String merchantNumber){
        this.RUT_COMERCIO = ValeUtils.UnformatRUT(config.CLAVEFISCAL);
        this.TERMINAL_ID = config.SII_TERMINAL_ID;
        this.NUM_COMERCIO = merchantNumber;
        this.USER_ID_ACTUAL = CLAVE_ACTUAL;
        this.USER_ID_NUEVO = CLAVE_NUEVA;
        this.NET_ID = config.getNET_ID();
        this.IMSI = config.IMSI;
        this.CRC_CONFIG = ValeUtils.CalcularCRC(config, merchantNumber);
    }

    public CambioUser(Vale vale, String CLAVE_ACTUAL, String CLAVE_NUEVA){
        this.RUT_COMERCIO = ValeUtils.UnformatRUT(vale.RUT);
        this.TERMINAL_ID = vale.TERMINAL_ID;
        this.NUM_COMERCIO = vale.NUM_COMERCIO;
        this.USER_ID_ACTUAL = CLAVE_ACTUAL;
        this.USER_ID_NUEVO = CLAVE_NUEVA;
        this.NET_ID = vale.NET_ID;
        this.IMSI = vale.IMSI;
        this.CRC_CONFIG = vale.CRC_CONFIG();
    }

    @Override
    public String toString() {
        return String.format("NUM_COMERCIO=%s&ID_TERMINAL=%s&RUT_COMERCIO=%s&NET_ID=%s&IMSI=%s&USER_ID_ACTUAL=%s&CRC_CONFIG=%s&USER_ID_NUEVO=%s",
                NUM_COMERCIO,
                TERMINAL_ID,
                RUT_COMERCIO,
                NET_ID,
                IMSI,
                USER_ID_ACTUAL,
                CRC_CONFIG,
                USER_ID_NUEVO);
    }
}
