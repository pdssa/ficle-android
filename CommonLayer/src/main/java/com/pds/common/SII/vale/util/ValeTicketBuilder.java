package com.pds.common.SII.vale.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Log;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.pds.common.Config;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import android_serialport_api.LineaTicket;
import android_serialport_api.Printer;

import com.pds.common.Formato;
import com.pds.common.SII.vale.model.Vale;
import com.pds.common.VentasHelper;
import com.pds.common.dao.VentaDao;
import com.pds.common.model.Venta;
import com.pds.common.model.VentaDetalle;
import com.pds.common.util.ImprimirEtiquetaTask;

/**
 * Created by Hernan on 24/06/2015.
 */
public class ValeTicketBuilder { //extends Ticket_Builder {

    private Context context;

    private String _header;

    //datos comercio
    private String _razonSocial;
    private String _nombreFantasia;
    private String _direccion;
    private String _localidad;
    private String _clavefiscal;
    private String _terminal;
    private String _num_comercio;
    private boolean _isDemoMode;

    //datos del vale
    private String _fecha;
    private String _hora;
    private String _nro_operacion;

    private List<LineaTicket> itemsAfectos;
    private List<LineaTicket> itemsExentos;

    private String _total;
    private String _hash;

    private double totalAfecto;
    private double totalExento;

    private String _qr_content;
    private List<String> _giro;
    private String _resolucion;

    private String _app_version;

    private Bitmap _qr;

    public void set_qr_content(String _qr_content) {
        this._qr_content = _qr_content;
    }

    public void set_hash(String _hash) {
        this._hash = _hash;
    }

    public void set_total(String _total) {
        this._total = _total;
    }

    public void setItemsAfectos(List<LineaTicket> _itemsAfectos) {
        this.itemsAfectos = _itemsAfectos;
    }

    public void setItemsExentos(List<LineaTicket> _itemsExentos) {
        this.itemsExentos = _itemsExentos;
    }

    public void set_nro_operacion(String _nro_operacion) {
        this._nro_operacion = _nro_operacion;
    }

    public void set_hora(String _hora) {
        this._hora = _hora;
    }

    public void set_fecha(String _fecha) {
        this._fecha = _fecha;
    }

    public void set_header(String _header) {
        this._header = _header;
    }

    public void set_app_version(String _app_version) {
        this._app_version = _app_version;
    }


    public String get_header() {
        return _header;
    }

    public String get_razonSocial() {
        return _razonSocial;
    }

    public String get_nombreFantasia() {
        return _nombreFantasia;
    }

    public String get_direccion() {
        return _direccion;
    }

    public String get_localidad() {
        return _localidad;
    }

    public String get_clavefiscal() {
        return _clavefiscal;
    }

    public String get_terminal() {
        return _terminal;
    }

    public String get_num_comercio() {
        return _num_comercio;
    }

    public boolean is_isDemoMode() {
        return _isDemoMode;
    }

    public String get_fecha() {
        return _fecha;
    }

    public String get_hora() {
        return _hora;
    }

    public String get_nro_operacion() {
        return _nro_operacion;
    }

    public List<LineaTicket> getItemsAfectos() {
        return itemsAfectos;
    }

    public List<LineaTicket> getItemsExentos() {
        return itemsExentos;
    }

    public String get_total() {
        return _total;
    }

    public String get_hash() {
        return _hash;
    }

    public String get_qr_content() {
        return _qr_content;
    }

    public List<String> get_giro() {
        return _giro;
    }

    public String get_resolucion() {
        return _resolucion;
    }

    public String get_app_version() {
        return _app_version;
    }

    public Bitmap get_qr() {
        return _qr;
    }

    public ValeTicketBuilder() {
        this._header = "";
        this._giro = new ArrayList<String>();
        this.itemsAfectos = new ArrayList<LineaTicket>();
        this.itemsExentos = new ArrayList<LineaTicket>();
        this.totalAfecto = 0d;
        this.totalExento = 0d;
    }

    public ValeTicketBuilder(Context context, Config config, String app_version, String merchantNumber) {
        this();

        this.context = context;

        this._razonSocial = config.RAZON_SOCIAL;
        this._nombreFantasia = config.COMERCIO;
        this._direccion = config.DIRECCION;
        this._localidad = config.LOCALIDAD;
        this._clavefiscal = config.CLAVEFISCAL;
        this._terminal = config.SII_TERMINAL_ID;
        this._num_comercio = merchantNumber;
        this._isDemoMode = config.isValeModalidadDemo();

        if (!TextUtils.isEmpty(config.SII_GIRO1))
            this._giro.add(config.SII_GIRO1);
        if (!TextUtils.isEmpty(config.SII_GIRO2))
            this._giro.add(config.SII_GIRO2);
        if (!TextUtils.isEmpty(config.SII_GIRO3))
            this._giro.add(config.SII_GIRO3);

        //this._resolucion = "Res.SII Nro." + config.SII_RESOLUCION_VALE;
        this._resolucion = config.SII_RESOLUCION_VALE;

        this._app_version = app_version;
    }

    public void LoadVenta(Venta venta, Formato.Decimal_Format FORMATO_DECIMAL) {

        //ticket.set_header(header);

        set_fecha(Formato.FormateaDate(venta.getFechaHora_Date(), Formato.DateTwoDigFormatSlash));
        set_hora(venta.getHora());

        set_total(Formato.FormateaDecimal(venta.getTotal(), FORMATO_DECIMAL));

        set_nro_operacion(venta.getFc_folio());//imprimimos el numero unico de vale completo
        //ticket.set_nro_operacion(nro_op.substring(nro_op.length() - 5));

        set_hash(venta.getFc_validation());

        loadItems(venta, FORMATO_DECIMAL);

        String qr_content = new Vale(
                this._clavefiscal,
                venta.getFc_Timbre(),
                String.valueOf(venta.getFc_id()),
                this.totalAfecto,
                this.totalExento,
                venta.getFc_validation(),
                venta.getFechaHora_Date(),
                this._num_comercio,
                this._terminal
        ).toQrContentString(context);

        set_qr_content(qr_content);

    }

    private void loadItems(Venta venta, Formato.Decimal_Format FORMATO_DECIMAL) {
        List<LineaTicket> itemsAfectos = new ArrayList<LineaTicket>();
        List<LineaTicket> itemsExentos = new ArrayList<LineaTicket>();

        if (venta.getDetalles() != null) {
            //sumamos los items afectos y exentos
            for (VentaDetalle det : venta.getDetalles()) {
                if (det.esGravado()) {
                    this.totalAfecto += det.getTotal();
                    //montos_afectos.add(Formato.FormateaDecimal(det.getTotal(), FORMATO_DECIMAL));

                    itemsAfectos.add(new LineaTicket(
                            Formato.FormateaDecimal(det.getCantidad(), Formato.Decimal_Format.US),
                            det.getDescripcion(),
                            Formato.FormateaDecimal(det.getPrecio(), FORMATO_DECIMAL),
                            Formato.FormateaDecimal(det.getTotal(), FORMATO_DECIMAL)
                    ));
                } else {
                    this.totalExento += det.getTotal();
                    //montos_exentos.add(Formato.FormateaDecimal(det.getTotal(), FORMATO_DECIMAL));

                    itemsExentos.add(new LineaTicket(
                            Formato.FormateaDecimal(det.getCantidad(), Formato.Decimal_Format.US),
                            det.getDescripcion(),
                            Formato.FormateaDecimal(det.getPrecio(), FORMATO_DECIMAL),
                            Formato.FormateaDecimal(det.getTotal(), FORMATO_DECIMAL)
                    ));
                }
            }
        }

        setItemsAfectos(itemsAfectos);
        setItemsExentos(itemsExentos);
    }

    public void LoadVale(Vale vale) {

        set_header("-- DUPLICADO --");

        set_fecha(ValeUtils.ExtractFecha("20" + vale.s_FECHA_HORA));
        set_hora(ValeUtils.ExtractHora("20" + vale.s_FECHA_HORA));

        set_total(Formato.FormateaDecimal(vale.TOTAL(), Formato.Decimal_Format.CH));

        set_nro_operacion(vale.NRO_UNICO_VALE);//imprimimos el numero unico de vale completo

        set_hash(vale.HASH_TRX);

        //buscamos la venta relacionada con el VALE, a fin de obtener el detalle de la venta
        Venta venta = new VentaDao(context.getContentResolver()).first(
                String.format("%s=%s AND %s='%s'",
                        VentasHelper.VENTA_FC_TYPE,
                        ValeConst.TIPO_DOCUMENTO,
                        VentasHelper.VENTA_FC_FOLIO,
                        vale.NRO_UNICO_VALE), null, "_id DESC");

        if (venta != null) {
            venta.addDetalles(context.getContentResolver());
            if (venta.getDetalles() != null) {
                loadItems(venta, Formato.Decimal_Format.CH);
            }
        }

        //si no se pudieron cargar detalles => generamos un item directamente con totales
        if (this.itemsAfectos.size() + this.itemsExentos.size() == 0) {

            List<LineaTicket> itemsAfectos = new ArrayList<LineaTicket>();
            List<LineaTicket> itemsExentos = new ArrayList<LineaTicket>();

            //no contamos con el detalle para la reimpresion, creamos un item con el total de cada uno
            if (vale.TOTAL_AFECTO > 0) {
                itemsAfectos.add(new LineaTicket(
                        "",
                        "SUBTOTAL",
                        Formato.FormateaDecimal(vale.TOTAL_AFECTO, Formato.Decimal_Format.CH),
                        Formato.FormateaDecimal(vale.TOTAL_AFECTO, Formato.Decimal_Format.CH)
                ));
            }

            if (vale.TOTAL_EXENTO > 0) {
                itemsExentos.add(new LineaTicket(
                        "",
                        "SUBTOTAL",
                        Formato.FormateaDecimal(vale.TOTAL_EXENTO, Formato.Decimal_Format.CH),
                        Formato.FormateaDecimal(vale.TOTAL_EXENTO, Formato.Decimal_Format.CH)
                ));
            }

            setItemsAfectos(itemsAfectos);
            setItemsExentos(itemsExentos);
        }

        String qr_content = vale.toQrContentString(context);

        set_qr_content(qr_content);
    }

    public void Print(String printerModel){

        ValePrintTask imprimirValeTask = new ValePrintTask(this.context, printerModel);

        imprimirValeTask.execute(this);
    }


}
