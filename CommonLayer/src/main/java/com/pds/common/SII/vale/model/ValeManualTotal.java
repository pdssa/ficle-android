package com.pds.common.SII.vale.model;

/**
 * Created by Hernan on 28/08/2015.
 */
public class ValeManualTotal {
    public int q_vales_emitid;
    public double tot_afe_emitid;
    public double tot_exe_emitd;


    public ValeManualTotal() {
        q_vales_emitid = 0;
        tot_afe_emitid = 0d;
        tot_exe_emitd = 0d;
    }
}
