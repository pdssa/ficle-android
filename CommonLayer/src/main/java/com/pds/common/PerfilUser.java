package com.pds.common;

/**
 * Created by Hernan on 31/03/2015.
 */
public abstract class PerfilUser {
    public static String getPerfil(int id_perfil) {
        switch (id_perfil) {
            case 1:
                return "Administrador";
            case 2:
                return "Vendedor";
            case 3:
                return "Invitado";
            default:
                return "";
        }
    }

    public static int getPerfil(String nombre_perfil) {
        if (nombre_perfil.equals("Administrador")) {
            return 1;
        } else if (nombre_perfil.equals("Vendedor")) {
            return 2;
        } else if (nombre_perfil.equals("Invitado")) {
            return 3;
        } else {
            return 3;
        }
    }
}
