package com.pds.common;

import android.app.Application;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.content.Context;
import android.net.Uri;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Hernan on 24/10/13.
 */
public class UsuarioHelper {

    //************TABLE USUARIOS***************
    public static final String TABLE_NAME = "usuarios";
    //columns names
    public static final String USUARIOS_ID = "_id";
    public static final String USUARIOS_LOGIN = "user";
    public static final String USUARIOS_NOMBRE = "nombre";
    public static final String USUARIOS_APELLIDO = "apellido";
    public static final String USUARIOS_PASSWORD = "password";
    public static final String USUARIOS_PERFIL = "perfil";
    public static final String USUARIOS_FOTO = "foto";
    public static final String USUARIOS_CELULAR = "celular";
    public static final String USUARIOS_HORA_ENTRADA = "entrada";
    public static final String USUARIOS_HORA_SALIDA = "salida";
    public static final String USUARIOS_SYNC_STATUS = "sync";

    //columns indexes
    public static final int USUARIOS_IX_ID = 0;
    public static final int USUARIOS_IX_LOGIN = 1;
    public static final int USUARIOS_IX_NOMBRE = 2;
    public static final int USUARIOS_IX_APELLIDO = 3;
    public static final int USUARIOS_IX_PASSWORD = 4;
    public static final int USUARIOS_IX_PERFIL = 5;
    public static final int USUARIOS_IX_FOTO = 6;
    public static final int USUARIOS_IX_CELULAR = 7;
    public static final int USUARIOS_IX_HORA_ENTRADA = 8;
    public static final int USUARIOS_IX_HORA_SALIDA = 9;
    public static final int USUARIOS_IX_SYNC_STATUS = 10;

    private DbHelper db;

    // script de creacion de tabla
    public static final String USUARIOS_CREATE_SCRIPT = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
            USUARIOS_ID + " integer primary key autoincrement, " +
            USUARIOS_LOGIN + " text not null, " +
            USUARIOS_NOMBRE + " text, " +
            USUARIOS_APELLIDO + " text, " +
            USUARIOS_PASSWORD + " text, " +
            USUARIOS_PERFIL + " integer not null, " +
            USUARIOS_FOTO + " text, " +
            USUARIOS_CELULAR + " text, " +
            USUARIOS_HORA_ENTRADA + " text, " +
            USUARIOS_HORA_SALIDA + " text, " +
            USUARIOS_SYNC_STATUS + " text default 'N'" +
            ");";

    public static final String USUARIOS_ALTER_v20_v21 = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + USUARIOS_HORA_ENTRADA + " text " ;
    public static final String USUARIOS_ALTER_v21_v22 = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + USUARIOS_HORA_SALIDA + " text " ;

    //script de alter
    public static final String USUARIOS_ALTER_SYNC_STATUS_SCRIPT = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + USUARIOS_SYNC_STATUS + " text default 'N'" ;
    public static final String USUARIOS_ALTER_SYNC_STATUS_SCRIPT_2 = "UPDATE " + TABLE_NAME + " SET " + USUARIOS_SYNC_STATUS + " = 'N'" ;

    public static final String USUARIO_ADMIN_CREATE_SCRIPT = "INSERT INTO " + TABLE_NAME + "(" +
            USUARIOS_LOGIN + "," + USUARIOS_NOMBRE + "," + USUARIOS_APELLIDO + "," +
            USUARIOS_PASSWORD + "," + USUARIOS_PERFIL + "," + USUARIOS_FOTO + "," +
            USUARIOS_CELULAR + "," + USUARIOS_HORA_ENTRADA + "," + USUARIOS_HORA_SALIDA + ") values (" +
            "'admin','admin','admin','1234', 1, null, null, null, null)";

    //querys standards
    public static final String USUARIOS_FULL_SELECT_SCRIPT = "SELECT " +
            USUARIOS_ID + ", " + USUARIOS_LOGIN + ", " + USUARIOS_NOMBRE + ", " +
            USUARIOS_APELLIDO + ", " + USUARIOS_PASSWORD + ", " + USUARIOS_PERFIL + ", " + USUARIOS_FOTO +
            ", " + USUARIOS_CELULAR + ", " + USUARIOS_HORA_ENTRADA + ", " + USUARIOS_HORA_SALIDA +
            " FROM " + TABLE_NAME +
            " ORDER BY " + USUARIOS_LOGIN;

    public static final String[] columnas = new String[]{
            USUARIOS_ID,
            USUARIOS_LOGIN,
            USUARIOS_NOMBRE,
            USUARIOS_APELLIDO,
            USUARIOS_PASSWORD,
            USUARIOS_PERFIL,
            USUARIOS_FOTO,
            USUARIOS_CELULAR,
            USUARIOS_HORA_ENTRADA,
            USUARIOS_HORA_SALIDA,
            USUARIOS_SYNC_STATUS
    };


    public UsuarioHelper(Context context) {
        db = new DbHelper(context);
    }

    public void create(Usuario user) throws Exception {
        try {

            ContentValues cv = new ContentValues();

            cv.put(this.USUARIOS_LOGIN, user.getLogin());
            cv.put(this.USUARIOS_NOMBRE, user.getNombre());
            cv.put(this.USUARIOS_APELLIDO, user.getApellido());
            cv.put(this.USUARIOS_PASSWORD, user.getPassword());
            cv.put(this.USUARIOS_PERFIL, user.getId_perfil());
            cv.put(this.USUARIOS_FOTO, user.getFoto());
            cv.put(this.USUARIOS_CELULAR, user.getCelular());
            cv.put(this.USUARIOS_HORA_ENTRADA, user.getHora_entrada());
            cv.put(this.USUARIOS_HORA_SALIDA, user.getHora_salida());

            user.setId((int)db.insert(this.TABLE_NAME, cv));

          } catch (Exception ex) {
            throw ex;
        }
    }

    public boolean update(Usuario user) throws Exception {
        try {

            ContentValues cv = new ContentValues();

            cv.put(this.USUARIOS_LOGIN, user.getLogin());
            cv.put(this.USUARIOS_NOMBRE, user.getNombre());
            cv.put(this.USUARIOS_APELLIDO, user.getApellido());
            cv.put(this.USUARIOS_PASSWORD, user.getPassword());
            cv.put(this.USUARIOS_PERFIL, user.getId_perfil());
            cv.put(this.USUARIOS_FOTO, user.getFoto());
            cv.put(this.USUARIOS_CELULAR, user.getCelular());
            cv.put(this.USUARIOS_HORA_ENTRADA, user.getHora_entrada());
            cv.put(this.USUARIOS_HORA_SALIDA, user.getHora_salida());
            cv.put(this.USUARIOS_SYNC_STATUS, "N");

            return db.update(this.TABLE_NAME, cv, user.getId());

        } catch (Exception ex) {
            throw ex;
        }
    }

    public boolean delete(Usuario user)  throws Exception {
        try {

            return db.delete(this.TABLE_NAME, user.getId());

        } catch (Exception ex) {
            throw ex;
        }
    }

    public Cursor getAllCursor() throws Exception {
        try {
            return db.query(this.USUARIOS_FULL_SELECT_SCRIPT);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public List<Usuario> getAllList() throws Exception {
        try {
            List<Usuario> usuarios = new ArrayList<Usuario>() ;

            Cursor cursor = db.query(this.USUARIOS_FULL_SELECT_SCRIPT);

            // recorremos los items
            if (cursor.moveToFirst()) {
                do {
                    usuarios.add(extractFromCursor(cursor));
                } while (cursor.moveToNext());
            }

            cursor.close();

            return usuarios;
        } catch (Exception ex) {
            throw ex;
        }
    }

    private Usuario extractFromCursor(Cursor cursor){
        Usuario user = new Usuario();

        if (cursor != null) {
            //cursor.moveToFirst();

            user.setId(cursor.getInt(this.USUARIOS_IX_ID));
            user.setLogin(cursor.getString(this.USUARIOS_IX_LOGIN));
            user.setNombre(cursor.getString(this.USUARIOS_IX_NOMBRE));
            user.setApellido(cursor.getString(this.USUARIOS_IX_APELLIDO));
            user.setId_perfil(cursor.getInt(this.USUARIOS_IX_PERFIL));
            user.setPassword(cursor.getString(this.USUARIOS_IX_PASSWORD));
            user.setFoto(cursor.getString(this.USUARIOS_IX_FOTO));
            user.setCelular(cursor.getString(this.USUARIOS_IX_CELULAR));
            user.setHora_entrada(cursor.getString(this.USUARIOS_IX_HORA_ENTRADA));
            user.setHora_salida(cursor.getString(this.USUARIOS_IX_HORA_SALIDA));

            //cursor.close();
        }

        return user;

    }

    public Usuario getById(Context context, long id) throws Exception {

        try {

            Cursor cursor = context.getContentResolver().query(
                    Uri.parse("content://com.pds.ficle.ep.usuarios.contentprovider/usuarios"),
                    UsuarioHelper.columnas,
                    UsuarioHelper.USUARIOS_ID +"=?",
                    new String[]{String.valueOf(id)},
                    UsuarioHelper.USUARIOS_LOGIN + " ASC");

            Usuario user = null;

            //Cursor cursor = db.query(query);
            if(cursor != null && cursor.moveToFirst()){
                //cursor.moveToFirst();
                user = extractFromCursor(cursor);
            }

            cursor.close();

            return user;

        } catch (Exception ex) {
            throw ex;
        }
    }

    public static Usuario ObtenerUsuarioById(ContentResolver contentResolver,int idUser) throws Exception {
        try {

            Usuario usuario = null;

            Cursor usuarios = contentResolver.query(
                    Uri.parse("content://com.pds.ficle.ep.usuarios.contentprovider/usuarios/" + String.valueOf(idUser)),
                    UsuarioHelper.columnas,
                    "",
                    new String[]{},
                    "");

            // recorremos los items
            if (usuarios.moveToFirst()) {
                do {

                    usuario = new Usuario();

                    usuario.setId(usuarios.getInt(UsuarioHelper.USUARIOS_IX_ID));
                    usuario.setLogin(usuarios.getString(UsuarioHelper.USUARIOS_IX_LOGIN));
                    usuario.setNombre(usuarios.getString(UsuarioHelper.USUARIOS_IX_NOMBRE));
                    usuario.setApellido(usuarios.getString(UsuarioHelper.USUARIOS_IX_APELLIDO));
                    usuario.setId_perfil(usuarios.getInt(UsuarioHelper.USUARIOS_IX_PERFIL));
                    usuario.setPassword(usuarios.getString(UsuarioHelper.USUARIOS_IX_PASSWORD));
                    usuario.setFoto(usuarios.getString(UsuarioHelper.USUARIOS_IX_FOTO));
                    usuario.setCelular(usuarios.getString(UsuarioHelper.USUARIOS_IX_CELULAR));

                } while (usuarios.moveToNext());
            }

            usuarios.close();

            return usuario;

        } catch (Exception ex) {
            throw ex;
        }
    }

    /*
    public Usuario getById(long id) throws Exception {

        try {

            String query = "SELECT " +
                    USUARIOS_ID + ", " + USUARIOS_LOGIN + ", " + USUARIOS_NOMBRE + ", " +
                    USUARIOS_APELLIDO + ", " + USUARIOS_PASSWORD + ", " + USUARIOS_PERFIL + ", " + USUARIOS_FOTO +
                    ", " + USUARIOS_CELULAR + ", " + USUARIOS_HORA_ENTRADA + ", " + USUARIOS_HORA_SALIDA +
                    " FROM " + TABLE_NAME +
                    " WHERE " + USUARIOS_ID + " = " + id +
                    " ORDER BY " + USUARIOS_LOGIN;

            Usuario user = null;

            Cursor cursor = db.query(query);
            if(cursor != null && cursor.moveToFirst()){
                //cursor.moveToFirst();
                user = extractFromCursor(cursor);
            }

            cursor.close();

            return user;

        } catch (Exception ex) {
            throw ex;
        }
    }*/

    public Usuario getByUserName(String user_name) throws Exception {

        try {
            String query = "SELECT " +
                    USUARIOS_ID + ", " + USUARIOS_LOGIN + ", " + USUARIOS_NOMBRE + ", " +
                    USUARIOS_APELLIDO + ", " + USUARIOS_PASSWORD + ", " + USUARIOS_PERFIL + ", " + USUARIOS_FOTO +
                    ", " + USUARIOS_CELULAR + ", " + USUARIOS_HORA_ENTRADA + ", " + USUARIOS_HORA_SALIDA +
                    " FROM " + TABLE_NAME +
                    " WHERE " + USUARIOS_LOGIN + " = '" + user_name + "' " +
                    " ORDER BY " + USUARIOS_LOGIN;

            Cursor cursor = db.query(query);
            cursor.moveToFirst();

            Usuario user = extractFromCursor(cursor);

            cursor.close();

            return user;

        } catch (Exception ex) {
            throw ex;
        }
    }
}
