package com.pds.common;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pds.common.Usuario;

import java.util.List;

/**
 * Created by Hernan on 03/12/13.
 */
public class UsuarioAdapter extends ArrayAdapter<Usuario> {
    private Context context;
    private List<Usuario> datos;
    private Boolean simple;
    private Boolean horario;

    public UsuarioAdapter(Context context, List<Usuario> datos, int resource, Boolean simple) {
        this(context, datos, resource, simple, false);
    }

    public UsuarioAdapter(Context context, List<Usuario> datos, int resource, Boolean simple, Boolean horario) {
        super(context, resource, datos);

        this.context = context;
        this.datos = datos;
        this.simple = simple;
        this.horario = horario;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // En primer lugar "inflamos" una nueva vista, que será la que se
        // mostrará en la celda del ListView. Para ello primero creamos el
        // inflater, y después inflamos la vista.
        LayoutInflater inflater = LayoutInflater.from(context);
        View item = null;

        Usuario user = datos.get(position);

        // A partir de la vista, recogeremos los controles que contiene para
        // poder manipularlos.
        // Recogemos el ImageView y le asignamos una foto.
        if (simple) {
            item = inflater.inflate(R.layout.list_row_simple, null);

            ImageView imagen = (ImageView) item.findViewById(R.id.list_row_icon);
            imagen.setImageResource(R.drawable.usuarios);

            // Recogemos los TextView para mostrar datos
            ((TextView) item.findViewById(R.id.list_row_title)).setText(user.getLogin());
            ((TextView) item.findViewById(R.id.list_row_subtitle)).setText(user.getApellido() + ", " + user.getNombre());

        } else if (horario) {
            item = inflater.inflate(R.layout.list_row, null);

            ImageView imagen = (ImageView) item.findViewById(R.id.list_row_icon);
            imagen.setImageResource(R.drawable.usuarios);

            // Recogemos los TextView para mostrar datos
            ((TextView) item.findViewById(R.id.list_row_title)).setText(user.getLogin());
            ((TextView) item.findViewById(R.id.list_row_subtitle)).setText(user.getApellido() + ", " + user.getNombre());

            ((TextView) item.findViewById(R.id.list_row_subtitle2)).setText("INGRESO: " + user.getEntrada_de_hoy());
            ((TextView) item.findViewById(R.id.list_row_subtitle3)).setText("EGRESO: " + user.getSalida_de_hoy());

            ((TextView) item.findViewById(R.id.list_row_subtitle4)).setVisibility(View.GONE);
            ((TextView) item.findViewById(R.id.list_row_subtitle5)).setVisibility(View.GONE);

        } else {
            item = inflater.inflate(R.layout.list_row, null);

            ImageView imagen = (ImageView) item.findViewById(R.id.list_row_icon);
            imagen.setImageResource(R.drawable.usuarios);

            // Recogemos los TextView para mostrar datos
            ((TextView) item.findViewById(R.id.list_row_title)).setText(user.getLogin());
            ((TextView) item.findViewById(R.id.list_row_subtitle)).setText(user.getApellido() + ", " + user.getNombre());

            ((TextView) item.findViewById(R.id.list_row_subtitle2)).setText(user.getPerfil());
            ((TextView) item.findViewById(R.id.list_row_subtitle3)).setText(user.getCelular());

            if (!user.getHora_entrada().equals(""))
                ((TextView) item.findViewById(R.id.list_row_subtitle4)).setText("INGRESO: " + user.getHora_entrada());
            if (!user.getHora_salida().equals(""))
                ((TextView) item.findViewById(R.id.list_row_subtitle5)).setText("EGRESO: " + user.getHora_salida());

        }

        // Devolvemos la vista para que se muestre en el ListView.
        return item;

    }
}