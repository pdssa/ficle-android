package com.pds.common.activity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.pds.common.receiver.FinishReceiver;

/**
 * Created by Hernan on 14/01/2015.
 */
public class TimerActivity extends Activity {

    private FinishReceiver finishActivityReceiver;

    protected void registerBaseActivityReceiver() {
        Log.i("TimerActivity", "registerReceiver");

        registerReceiver(finishActivityReceiver, FinishReceiver.INTENT_FILTER);
    }

    protected void unRegisterBaseActivityReceiver() {
        Log.i("TimerActivity", "unregisterReceiver");

        try {
            if(finishActivityReceiver != null)
                unregisterReceiver(finishActivityReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        finishActivityReceiver = new FinishReceiver(this);

        registerBaseActivityReceiver();
    }

    private App getApp() {
        return (App) getApplication();
    }

    public void resetInactivityTimer() {
        getApp().resetInactivityTimer();
    }

    public void setActive() {
        getApp().setActive(true);
    }

    public void stopInactivityTimer() {
        getApp().stopInactivityTimer();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        //reset timer -> 0
        if (!isFinishing())
            resetInactivityTimer();
    }

    @Override
    protected void onResume() {
        super.onResume();

        resetInactivityTimer();
    }

    @Override
    public void onStop() {
        super.onStop();
        /*if (isFinishing())
            stopInactivityTimer();*/
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unRegisterBaseActivityReceiver();
    }

    //metodo para que las actividades que extiendan esta clase puedan definir que hacer al salir
    public void onFinish() {
        //Toast.makeText(getApplicationContext(),"Cierre por inactividad",  Toast.LENGTH_SHORT).show();

        finish();//cerramos la aplicacion
    }
}
