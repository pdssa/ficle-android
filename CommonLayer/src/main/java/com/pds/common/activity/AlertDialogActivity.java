package com.pds.common.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.pds.common.R;
import com.pds.common.receiver.NetworkStateReceiver;

import java.util.Timer;
import java.util.TimerTask;

public class AlertDialogActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        showNoConnectionAlert();
    }

    private void showNoConnectionAlert(){

        SharedPreferences prefs = getSharedPreferences("com.pds.ficle.ep", MODE_PRIVATE);
        Boolean showAlert = !prefs.getBoolean("alert_no_conn_not_show", false);
        Boolean activityVisible = prefs.getBoolean("activity_no_conn_visible", false);

        if(showAlert){

            //programamos el aviso para dentro de media hora

            TimerTask timerTask = new TimerTask(){

                @Override
                public void run() {
                    NetworkStateReceiver.getInstance().checkConnectionState();
                }
            };
            Timer timer = new Timer();
            timer.schedule(timerTask, 10 * 60 * 1000); //10'

            if (!activityVisible) {

                View checkBoxView = View.inflate(AlertDialogActivity.this, R.layout.no_connectivity_dialog, null);
                CheckBox checkBox = (CheckBox) checkBoxView.findViewById(R.id.checkbox);
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        // Save to shared preferences
                        SharedPreferences.Editor editor = getSharedPreferences("com.pds.ficle.ep", MODE_PRIVATE).edit();
                        editor.putBoolean("alert_no_conn_not_show", isChecked);
                        editor.apply();
                    }
                });
                checkBox.setText("No volver a mostrar");

                new AlertDialog.Builder(AlertDialogActivity.this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Sin Conexión !")
                        .setMessage("No se encontró una conexión disponible")
                        .setView(checkBoxView)
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                closeActivity();
                            }
                        })
                        .setCancelable(false)
                        .show();

                // Save to shared preferences
                SharedPreferences.Editor editor = getSharedPreferences("com.pds.ficle.ep", MODE_PRIVATE).edit();
                editor.putBoolean("activity_no_conn_visible", true);
                editor.apply();
            }
            else{
                finish();
            }
        }
        else{
            finish();
        }
    }

    private void closeActivity(){
        SharedPreferences.Editor editor = getSharedPreferences("com.pds.ficle.ep", MODE_PRIVATE).edit();
        editor.putBoolean("activity_no_conn_visible", false);
        editor.apply();

        finish();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

}
