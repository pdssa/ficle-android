package com.pds.common.activity;

/**
 * Created by Hernan on 08/05/2015.
 */
public interface ActividadProgress {
    public void showProgress(final boolean show);

    public void showProgressMessage(CharSequence message) ;
}
