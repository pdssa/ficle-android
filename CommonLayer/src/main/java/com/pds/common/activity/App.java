package com.pds.common.activity;

import android.app.Application;
import android.content.Intent;
import android.os.CountDownTimer;
import android.util.Log;

import com.pds.common.Config;
import com.pds.common.receiver.FinishReceiver;

/**
 * Created by Hernan on 14/01/2015.
 */
public class App extends Application {

    private MyCountDownTimer countDownTimer;
    private int inactivityTime;
    private boolean isActive;

    public void setActive(boolean _isActive){
        isActive = _isActive;
    }
    private boolean isActive(){
        return isActive;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        int time = new Config(this).INACTIVITY_TIME;

        setTimer(time);

        Log.i("TimerActivity", "onCreate:setTimer");

    }

    public void setTimer(int inactivity_time) {
        inactivityTime = inactivity_time * 60 * 1000; //min to milisec

        countDownTimer = new MyCountDownTimer(inactivityTime, 1000);

        setActive(true);

        //Log.i("TimerActivity","Start Timer");
    }


    public void resetInactivityTimer() {

        if(isActive()) {
            Log.i("TimerActivity", "resetTimer");

            countDownTimer.reset();///////Reset timer
        }

    }

    public void stopInactivityTimer() {

        Log.i("TimerActivity", "stopTimer");

        countDownTimer.cancel();///////Reset timer

        setActive(false);
    }

    private void finalizar() {
        Log.i("TimerActivity", "Cierre por inactividad");

        /*int pid = android.os.Process.myPid();
        android.os.Process.killProcess(pid);
        System.exit(0);*/
        closeAllActivities();
    }

    protected void closeAllActivities(){
        sendBroadcast(new Intent(FinishReceiver.FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION));
    }

    class MyCountDownTimer extends CountDownTimer {
        public MyCountDownTimer(long startTime, long interval) {
            super(startTime, interval);
        }

        @Override
        public void onFinish() {
            //ejecutar accion de cierre por inactividad
            finalizar();
        }

        @Override
        public void onTick(long millisUntilFinished) {
        }

        public void reset() {
            this.cancel();
            this.start();
        }
    }
}
