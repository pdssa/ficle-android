package com.pds.common.activity;

import android.app.Activity;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Hernan on 19/12/2014.
 */
public class TimerActivity_ extends Activity {

    private MyCountDownTimer countDownTimer;
    private int inactivityTime;
    private boolean active;

    public void setActive(boolean isActive){
        active = isActive;
    }

    public void setTimer(int inactivity_time){
        inactivityTime = inactivity_time * 60 * 1000; //min to milisec

        countDownTimer = new MyCountDownTimer(inactivityTime, 1000);

        setActive(true);

        //Log.i("TimerActivity","Start Timer");
    }

    public void resetInactivityTimer(){
        if(active) {
            countDownTimer.reset();///////Reset timer

            //Log.i("TimerActivity", "Reset Timer");
        }
    }

    public void stopInactivityTimer(){
        countDownTimer.cancel();///////Reset timer

        setActive(false);

        //Log.i("TimerActivity","Stop Timer");
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        //reset timer -> 0
        resetInactivityTimer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        resetInactivityTimer();
    }

    @Override
    public void onStop() {
        super.onStop();
        if(isFinishing())
            stopInactivityTimer();
    }

    private void finalizar(){
        Log.i("TimerActivity","Cierre por inactividad");

        onFinish();
    }

    //metodo para que las actividades que extiendan esta clase puedan definir que hacer al salir
    public void onFinish(){
        Toast.makeText(getApplicationContext(),"Cierre por inactividad",  Toast.LENGTH_SHORT).show();

        finish();//cerramos la aplicacion
    }


    class MyCountDownTimer extends CountDownTimer {
        public MyCountDownTimer(long startTime, long interval) {
            super(startTime, interval);
        }

        @Override
        public void onFinish() {
            //ejecutar accion de cierre por inactividad
            finalizar();
        }

        @Override
        public void onTick(long millisUntilFinished) {
        }

        public void reset(){
            this.cancel();
            this.start();
        }
    }
}
