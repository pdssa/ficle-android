package com.pds.common.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.text.Html;

/**
 * Created by Hernan on 10/11/2014.
 */
public class Actividad extends TimerActivity {
    @Override
    public void onBackPressed() {

        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.stat_sys_warning)
                .setTitle("Salir")
                .setMessage(Html.fromHtml("Desea abandonar la pantalla actual ?<br/>Si continúa perderá los cambios no guardados"))
                .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {Actividad.super.onBackPressed();}
                })
                .setNegativeButton("NO", null)
                .setCancelable(false)
                .show();
    }


    public void onSave(DialogInterface.OnClickListener action){
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_menu_help)
                .setTitle("Confirmar acción")
                .setMessage("Desea guardar los cambios en curso?")
                .setPositiveButton("SI", action)
                .setNegativeButton("NO", null)
                .setCancelable(false)
                .show();
    }
}
