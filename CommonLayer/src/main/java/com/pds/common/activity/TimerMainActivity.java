package com.pds.common.activity;

/**
 * Created by Hernan on 16/01/2015.
 */
//CLASE PARA IMPLEMENTAR EN LA PRINCIPAL ACTIVITY DE CADA APP, RENUEVA EL TIMER EN EL RESUME (REAPERTURA DE LA APP) Y
//FINALIZA EL TIMER EN EL BACK
public class TimerMainActivity extends TimerActivity {
    @Override
    protected void onResume() {
        super.setActive();

        super.onResume();
    }

    @Override
    public void onBackPressed() {
        //como vamos a finalizar => detenemos el timer y salimos
        super.stopInactivityTimer();
        super.onFinish();
    }
}
