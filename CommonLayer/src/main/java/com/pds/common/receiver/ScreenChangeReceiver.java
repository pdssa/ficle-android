package com.pds.common.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.widget.Toast;

/**
 * Created by Hernan on 18/11/2014.
 */
public class ScreenChangeReceiver extends BroadcastReceiver {
    public static boolean wasScreenOn = true;
    public static IntentFilter filter;
    public OnUserInactivityListener inactivityListener;

    public void setInactivityListener(OnUserInactivityListener inactivityListener) {
        this.inactivityListener = inactivityListener;
    }

    public interface OnUserInactivityListener {
        void onUserInactivityAction();
    }

    public ScreenChangeReceiver(){
        filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
    }

    public static IntentFilter getFilter() {
        return filter;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
            wasScreenOn = false;
            Toast.makeText(context, "SCREEN OFF", Toast.LENGTH_SHORT).show();
            if(inactivityListener != null)
                inactivityListener.onUserInactivityAction();
        } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
            wasScreenOn = true;
            Toast.makeText(context, "SCREEN ON", Toast.LENGTH_SHORT).show();
        }
    }

}
