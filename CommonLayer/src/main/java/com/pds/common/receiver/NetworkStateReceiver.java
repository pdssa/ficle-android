package com.pds.common.receiver;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.pds.common.R;
import com.pds.common.activity.AlertDialogActivity;

/**
 * Created by Hernan on 18/11/2014.
 */
public class NetworkStateReceiver extends BroadcastReceiver {
    String TAG = "NetworkStateReceiver";

    private static Context _context;

    private static NetworkStateReceiver instance = null;
    public NetworkStateReceiver() {
        // Exists only to defeat instantiation.
    }
    public static NetworkStateReceiver getInstance() {
        if(instance == null) {
            instance = new NetworkStateReceiver();
        }
        return instance;
    }
    public static NetworkStateReceiver getInstance(Context context) {
        if(instance == null) {
            instance = new NetworkStateReceiver();
            _context = context;
        }
        return instance;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.d(TAG, "Network connectivity change");

        checkConnectionState(context);
    }
    public void checkConnectionState()
    {
        checkConnectionState(_context);
    }
    public void checkConnectionState(Context context){
        ConnectivityManager conn = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = conn.getActiveNetworkInfo();

        if (!(activeNetworkInfo != null && activeNetworkInfo.isConnected())) {

            //Toast.makeText(context, "Sin conexión!", Toast.LENGTH_SHORT).show();

            Intent i = new Intent(context, AlertDialogActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);

            //si queres saber que tipo de conexion está activa
            //NetworkInfo networkInfo = conn.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            //boolean isWifiConn = networkInfo.isConnected();
            //networkInfo = conn.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            //boolean isMobileConn = networkInfo.isConnected();
        }
    }
}
