package com.pds.common.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.pds.common.activity.TimerActivity;

/**
 * Created by Hernan on 15/01/2015.
 */
public class FinishReceiver extends BroadcastReceiver {

    public static final String FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION =    "com.pds.FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION";

    public static final IntentFilter INTENT_FILTER = createIntentFilter();

    private static IntentFilter createIntentFilter(){
        IntentFilter filter = new IntentFilter();
        filter.addAction(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION);
        return filter;
    }

    private TimerActivity _activity;

    public FinishReceiver(TimerActivity activity){
        _activity = activity;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION)){
            _activity.onFinish();
        }
    }
}