package com.pds.common;

import java.util.List;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pds.common.ItemMenu;

public class ItemMenuAdapter extends ArrayAdapter<ItemMenu> {

    private Context context;
    List<ItemMenu> datos;

    public List<ItemMenu> getDatos() {
        return datos;
    }

    public void setDatos(List<ItemMenu> datos) {
        this.datos = datos;
    }

    public ItemMenuAdapter(Context context, List<ItemMenu> datos) {
        super(context, android.R.layout.simple_list_item_1, datos);

        this.context = context;
        this.datos = datos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // En primer lugar "inflamos" una nueva vista, que será la que se
        // mostrará en la celda del ListView. Para ello primero creamos el
        // inflater, y después inflamos la vista.
        LayoutInflater inflater = LayoutInflater.from(context);
        //View item = inflater.inflate(android.R.layout.simple_list_item_1, null);
        View item = inflater.inflate(R.layout.recarga_menu_item, null);

        ItemMenu row = datos.get(position);

        // A partir de la vista, recogeremos los controles que contiene para
        // poder manipularlos.
        // Recogemos los TextView para mostrar datos
        ((TextView) item.findViewById(android.R.id.text1)).setText(row.getDescripcion());

        if(row.getDescripcion().equals("Volver")){
            ((TextView) item.findViewById(android.R.id.text1)).setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
            ((TextView) item.findViewById(android.R.id.text1)).setPadding(0,0, 10, 0);
        }

        // Devolvemos la vista para que se muestre en el ListView.
        return item;

    }

}


