package com.pds.common;

/**
 * Created by Hernan on 14/02/14.
 */
public class HorarioHelper {

    public static enum TipoMovimientoHorario {
        ENTRADA, //0
        SALIDA //1
    }

    //************TABLE HORARIO***************
    public static final String TABLE_NAME = "horario";
    public static final String VIEW_NAME = "grilla_horario";
    public static final String VIEW_NAME_2 = "vw_grilla_horario";

    //columns names
    public static final String HORARIO_ID = "_id";
    public static final String HORARIO_USER_ID = "idUser";
    public static final String HORARIO_TIPO_MOV = "tipoMov";
    public static final String HORARIO_FECHA = "fecha";
    public static final String HORARIO_HORA = "hora";
    public static final String HORARIO_OBSERVACION = "observacion";
    public static final String HORARIO_VW_USUARIO = UsuarioHelper.USUARIOS_NOMBRE;
    public static final String HORARIO_VW_HORA_IN = "ingreso";
    public static final String HORARIO_VW_HORA_OUT = "salida";
    public static final String HORARIO_VW_HORA_IN_HAB = "ingreso_hab";
    public static final String HORARIO_VW_HORA_OUT_HAB = "salida_hab";

    //columns indexes
    public static final int HORARIO_IX_ID = 0;
    public static final int HORARIO_IX_USER_ID = 1;
    public static final int HORARIO_IX_TIPO_MOV = 2;
    public static final int HORARIO_IX_FECHA = 3;
    public static final int HORARIO_IX_HORA = 4;
    public static final int HORARIO_IX_OBSERVACION = 5;

    public static final int HORARIO_IX_VW_USER_ID = 0;
    public static final int HORARIO_IX_VW_USUARIO = 1;
    public static final int HORARIO_IX_VW_HORA_IN_HAB = 2;
    public static final int HORARIO_IX_VW_HORA_OUT_HAB = 3;
    public static final int HORARIO_IX_VW_FECHA = 4;
    public static final int HORARIO_IX_VW_HORA_IN = 5;
    public static final int HORARIO_IX_VW_HORA_OUT = 6;


    // script de creacion de tabla
    public static final String HORARIO_CREATE_SCRIPT = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
            HORARIO_ID + " integer primary key autoincrement, " +
            HORARIO_USER_ID + " integer not null, " +
            HORARIO_TIPO_MOV + " integer, " +
            HORARIO_FECHA + " text, " +
            HORARIO_HORA + " text, " +
            HORARIO_OBSERVACION + " text " +
            ");";

    //script de creacion de la vista de grilla horaria
    public static final String HORARIO_CREATE_VIEW = "CREATE VIEW IF NOT EXISTS " + VIEW_NAME + " AS " +
            "SELECT DISTINCT h1." + HORARIO_USER_ID + ", u." + HORARIO_VW_USUARIO + ", " +
            "u." + UsuarioHelper.USUARIOS_HORA_ENTRADA + " as " + HORARIO_VW_HORA_IN_HAB + ", u." + UsuarioHelper.USUARIOS_HORA_SALIDA + " as " + HORARIO_VW_HORA_OUT_HAB + ", h1." + HORARIO_FECHA + ", " +
            "ifnull(a." + HORARIO_HORA + ",'') AS " + HORARIO_VW_HORA_IN + ", ifnull(b." + HORARIO_HORA + ",'') AS " + HORARIO_VW_HORA_OUT +
            " FROM " + TABLE_NAME + " h1" +
            " INNER JOIN " + UsuarioHelper.TABLE_NAME + " u ON u." + UsuarioHelper.USUARIOS_ID + " = h1." + HORARIO_USER_ID +
            " LEFT OUTER JOIN (SELECT h2.* FROM " + TABLE_NAME + " h2 WHERE h2." + HORARIO_TIPO_MOV + "=0) as a ON h1." + HORARIO_USER_ID + " = a." + HORARIO_USER_ID + " AND h1." + HORARIO_FECHA + "= a." + HORARIO_FECHA  +
            " LEFT OUTER JOIN (SELECT h2.* FROM " + TABLE_NAME + " h2 WHERE h2." + HORARIO_TIPO_MOV + "=1) as b ON h1." + HORARIO_USER_ID + " = b." + HORARIO_USER_ID + " AND h1." + HORARIO_FECHA + "= b." + HORARIO_FECHA  ;

    /*
    public static final String HORARIO_CREATE_VIEW = "CREATE VIEW IF NOT EXISTS " + VIEW_NAME + " AS " +
            "SELECT DISTINCT h1." + HORARIO_USER_ID + ", u." + HORARIO_VW_USUARIO + ", " +
            "u." + UsuarioHelper.USUARIOS_HORA_ENTRADA + " as " + HORARIO_VW_HORA_IN_HAB + ", u." + UsuarioHelper.USUARIOS_HORA_SALIDA + " as " + HORARIO_VW_HORA_OUT_HAB + ", h1." + HORARIO_FECHA + ", " +
            "(CASE h1." + HORARIO_TIPO_MOV + " WHEN 0 THEN h1." + HORARIO_HORA + " ELSE (SELECT h2." + HORARIO_HORA + " FROM " + TABLE_NAME + " h2 WHERE h1." + HORARIO_USER_ID + " = h2." + HORARIO_USER_ID + " AND h2." + HORARIO_TIPO_MOV + " = 0 AND h2." + HORARIO_FECHA + "= h1." + HORARIO_FECHA + ") END) AS " + HORARIO_VW_HORA_IN + ", " +
            "(CASE h1." + HORARIO_TIPO_MOV + " WHEN 1 THEN h1." + HORARIO_HORA + " ELSE (SELECT h2." + HORARIO_HORA + " FROM " + TABLE_NAME + " h2 WHERE h1." + HORARIO_USER_ID + " = h2." + HORARIO_USER_ID + " AND h2." + HORARIO_TIPO_MOV + " = 1 AND h2." + HORARIO_FECHA + "= h1." + HORARIO_FECHA + ") END) AS " + HORARIO_VW_HORA_OUT + " " +
            "FROM " + TABLE_NAME + " h1 " +
            "INNER JOIN " + UsuarioHelper.TABLE_NAME + " u ON u." + UsuarioHelper.USUARIOS_ID + " = h1." + HORARIO_USER_ID;
    */

    public static final String HORARIO_CREATE_VIEW_2 = "CREATE VIEW IF NOT EXISTS " + VIEW_NAME_2 + " AS " +
            "SELECT * FROM " + VIEW_NAME + " UNION " +
            "SELECT " + UsuarioHelper.USUARIOS_ID + ", " + UsuarioHelper.USUARIOS_NOMBRE + ", " +
                UsuarioHelper.USUARIOS_HORA_ENTRADA + ", " + UsuarioHelper.USUARIOS_HORA_SALIDA + ", " +
            " null, null, null" +
            " FROM " + UsuarioHelper.TABLE_NAME;

    /*
    public static final String HORARIO_CREATE_VIEW_2 = "CREATE VIEW IF NOT EXISTS " + VIEW_NAME_2 + " AS " +
            "SELECT " + "IFNULL(h." + HORARIO_USER_ID + ",u." + UsuarioHelper.USUARIOS_ID + ") AS " + HORARIO_USER_ID + ", " +
            "IFNULL(h." + HORARIO_VW_USUARIO + ",u." + UsuarioHelper.USUARIOS_NOMBRE + ") AS " + HORARIO_VW_USUARIO + ", " +
            "IFNULL(h." + HORARIO_VW_HORA_IN_HAB + ",u." + UsuarioHelper.USUARIOS_HORA_ENTRADA + ") AS " + HORARIO_VW_HORA_IN_HAB + ", " +
            "IFNULL(h." + HORARIO_VW_HORA_OUT_HAB + ",u." + UsuarioHelper.USUARIOS_HORA_SALIDA + ") AS " + HORARIO_VW_HORA_OUT_HAB + ", " +
            "h." + HORARIO_FECHA + ", h." + HORARIO_VW_HORA_IN + ", h." + HORARIO_VW_HORA_OUT + " " +
            "FROM " + UsuarioHelper.TABLE_NAME + " u " +
            "LEFT OUTER JOIN " + VIEW_NAME + " h ON u." + UsuarioHelper.USUARIOS_ID + " = h." + HORARIO_USER_ID;
    */
    public static final String[] columnas = new String[]{
            HORARIO_ID,
            HORARIO_USER_ID,
            HORARIO_TIPO_MOV,
            HORARIO_FECHA,
            HORARIO_HORA,
            HORARIO_OBSERVACION
    };

    public static final String[] columnas_grilla_horario = new String[]{
            HORARIO_USER_ID,
            HORARIO_VW_USUARIO,
            HORARIO_VW_HORA_IN_HAB,
            HORARIO_VW_HORA_OUT_HAB,
            HORARIO_FECHA,
            HORARIO_VW_HORA_IN,
            HORARIO_VW_HORA_OUT
    };
}
