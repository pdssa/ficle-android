package com.pds.gestionpagos.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pds.common.Formatos;
import com.pds.gestionpagos.R;
import com.pds.gestionpagos.model.Invoice;
import com.pds.gestionpagos.model.PaymentProvider;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by Hernan on 03/08/2016.
 */
public class InvoiceAdapter extends ArrayAdapter<Invoice> {

    private Context context;
    private List<Invoice> datos;
    private DecimalFormat FORMATO_DECIMAL;
    private PaymentProvider paymentProvider;

    public InvoiceAdapter(Context context, List<Invoice> datos, DecimalFormat decimalFormat, PaymentProvider paymentProvider) {
        super(context, android.R.layout.simple_list_item_1, datos);

        this.context = context;
        this.datos = datos;
        this.FORMATO_DECIMAL = decimalFormat;
        this.paymentProvider = paymentProvider;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.invoice_item_view, null);

        Invoice invoice = datos.get(position);

        TextView txtNumber = (TextView) view.findViewById(R.id.invoice_item_number);
        TextView txtAmount = (TextView) view.findViewById(R.id.invoice_item_amount);
        TextView txtDate = (TextView) view.findViewById(R.id.invoice_item_date);
        TextView txtExpDate = (TextView) view.findViewById(R.id.invoice_item_exp_date);
        TextView txtPayment = (TextView) view.findViewById(R.id.invoice_item_payment);
        TextView txtProvider = (TextView) view.findViewById(R.id.invoice_item_provider);

        txtNumber.setText(invoice.getInvoiceNumber());
        //TODO: use decimal formats
        txtAmount.setText(String.valueOf(invoice.getInvoiceAmount()));
        txtPayment.setText(String.valueOf(invoice.getPaymentAmount()));

        //txtDate.setText(Formato.FormateaDate(invoice.getInvoiceDate(), Formato.DateFormatSlash));
        //txtExpDate.setText(Formato.FormateaDate(invoice.getExpirationDate(), Formato.DateFormatSlash));

        txtDate.setText(invoice.getInvoiceDate());
        txtExpDate.setText(invoice.getExpirationDate());

        //si no hay un provider activo => mostramos el provider de las invoices
        if(paymentProvider == null) {
            txtProvider.setVisibility(View.VISIBLE);
            txtProvider.setText("PROVIDER");
        }
        else{
            txtProvider.setVisibility(View.GONE);
        }
        return view;

    }

    @Override
    public Invoice getItem(int position) {
        return datos.get(position);
    }
}

