package com.pds.gestionpagos.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.pds.gestionpagos.R;
import com.pds.gestionpagos.model.PaymentProvider;

import java.util.List;

/**
 * Created by Hernan on 18/08/2016.
 */
public class PaymentProviderAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<PaymentProvider> paymentProviderList;
    private PaymentProviderListener paymentProviderListener;

    public PaymentProviderAdapter(Context context, List<PaymentProvider> paymentProviderList, PaymentProviderListener paymentProviderListener){
        this.context = context;
        this.paymentProviderList = paymentProviderList;
        this.paymentProviderListener = paymentProviderListener;
    }

    public interface PaymentProviderListener{
        void onPaymentProviderSelected(PaymentProvider paymentProvider);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(this.context).inflate(R.layout.payment_prov_item_view, parent, false );

        //int width = parent.getMeasuredWidth() / 3;
        //int height = parent.getMeasuredHeight() / 2;
        //view.setMinimumWidth(width);
        //view.setMinimumHeight(height);

        PaymentProviderViewHolder paymentProviderViewHolder = new PaymentProviderViewHolder(view, this.paymentProviderListener);
        //vistaPromo.setOnClickListener(this);
        return paymentProviderViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((PaymentProviderViewHolder)holder).bindPaymentProvider(paymentProviderList.get(position));
    }

    @Override
    public int getItemCount() {
        return paymentProviderList.size();
    }

    private class PaymentProviderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private ImageView imgLogo;
        private PaymentProviderListener listener;
        private PaymentProvider paymentProvider;

        public PaymentProviderViewHolder(View view, PaymentProviderListener paymentProviderListener){
            super(view);

            listener = paymentProviderListener;

            itemView.setOnClickListener(this);

            this.imgLogo = (ImageView) view.findViewById(R.id.paymentprov_img_logo);
        }

        public void bindPaymentProvider(PaymentProvider paymentProvider){
            this.paymentProvider = paymentProvider;

            this.imgLogo.setImageResource(paymentProvider.getIcon());
        }

        @Override
        public void onClick(View v) {
            if(listener != null && paymentProvider != null){
                listener.onPaymentProviderSelected(paymentProvider);
            }
        }
    }
}
