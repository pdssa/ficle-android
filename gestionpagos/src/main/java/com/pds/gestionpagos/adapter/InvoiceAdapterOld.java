package com.pds.gestionpagos.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pds.common.Formato;
import com.pds.gestionpagos.R;
import com.pds.gestionpagos.model.Invoice;
import com.pds.gestionpagos.model.PaymentProvider;

import java.util.List;

/**
 * Created by Hernan on 18/08/2016.
 */
public class InvoiceAdapterOld extends RecyclerView.Adapter {

    private Context context;
    private List<Invoice> invoiceList;
    private InvoiceListener invoiceListener;

    public InvoiceAdapterOld(Context context, List<Invoice> invoiceList, InvoiceListener invoiceListener){
        this.context = context;
        this.invoiceList = invoiceList;
        this.invoiceListener = invoiceListener;
    }

    public interface InvoiceListener{
        void onInvoiceSelected(Invoice invoice);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(this.context).inflate(R.layout.invoice_item_view, parent, false );

        return new InvoiceViewHolder(view, this.invoiceListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((InvoiceViewHolder)holder).bindInvoice(invoiceList.get(position));
    }

    @Override
    public int getItemCount() {
        return invoiceList.size();
    }

    private class InvoiceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private InvoiceListener listener;
        private Invoice invoice;
        private TextView txtNumber, txtAmount, txtDate, txtExpDate, txtPayment, txtProvider;

        public InvoiceViewHolder(View view, InvoiceListener invoiceListener){
            super(view);

            listener = invoiceListener;

            itemView.setOnClickListener(this);

            this.txtNumber = (TextView) view.findViewById(R.id.invoice_item_number);
            this.txtAmount = (TextView) view.findViewById(R.id.invoice_item_amount);
            this.txtDate = (TextView) view.findViewById(R.id.invoice_item_date);
            this.txtExpDate = (TextView) view.findViewById(R.id.invoice_item_exp_date);
            this.txtPayment = (TextView) view.findViewById(R.id.invoice_item_payment);
            this.txtProvider = (TextView) view.findViewById(R.id.invoice_item_provider);
        }

        public void bindInvoice(Invoice invoice){
            this.invoice = invoice;

            txtNumber.setText(invoice.getInvoiceNumber());
            //TODO: use decimal formats
            txtAmount.setText(String.valueOf(invoice.getInvoiceAmount()));
            txtPayment.setText(String.valueOf(invoice.getPaymentAmount()));

            //txtDate.setText(Formato.FormateaDate(invoice.getInvoiceDate(), Formato.DateFormatSlash));
            //txtExpDate.setText(Formato.FormateaDate(invoice.getExpirationDate(), Formato.DateFormatSlash));

            txtDate.setText(invoice.getInvoiceDate());
            txtExpDate.setText(invoice.getExpirationDate());

            //txtProvider.setText(invoice.getProvider().getName());
        }

        @Override
        public void onClick(View v) {
            if(listener != null && invoice != null){
                listener.onInvoiceSelected(invoice);
            }
        }
    }
}
