package com.pds.gestionpagos.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Formato;
import com.pds.gestionpagos.R;
import com.pds.gestionpagos.adapter.InvoiceAdapter;
import com.pds.gestionpagos.adapter.InvoiceAdapterOld;
import com.pds.gestionpagos.dao.InvoiceDAO;
import com.pds.gestionpagos.model.Invoice;
import com.pds.gestionpagos.model.PaymentProvider;

import java.util.Date;
import java.util.List;

import android_serialport_api.Printer;

public class InvoicesListActivity extends AppCompatActivity implements InvoiceAdapterOld.InvoiceListener {

    public static final String KEY_PROVIDER = "__PROVIDER";

    private ListView listViewInvoices;
    private PaymentProvider paymentProvider;
    private Config config;
    private List<Invoice> invoiceList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoices_list);

        listViewInvoices = (ListView) findViewById(R.id.invoice_list_lst);

        if (getIntent().hasExtra(InvoicesListActivity.KEY_PROVIDER)) {
            paymentProvider = getIntent().getParcelableExtra(InvoicesListActivity.KEY_PROVIDER);

            ((ImageView) findViewById(R.id.invoice_list_logo)).setImageResource(paymentProvider.getIcon());
        }

        config = new Config(this);

        loadInvoices();
    }

    private void loadInvoices() {

        if(paymentProvider == null){
            findViewById(R.id.invoice_header_provider).setVisibility(View.VISIBLE);
        }
        else{
            findViewById(R.id.invoice_header_provider).setVisibility(View.GONE);
        }

        invoiceList = new InvoiceDAO().getInvoices(this);

        listViewInvoices.setAdapter(new InvoiceAdapter(this, invoiceList, null, paymentProvider));
        listViewInvoices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onInvoiceSelected((Invoice) parent.getItemAtPosition(position));
            }
        });

        /*listViewInvoices.setAdapter(new InvoiceAdapterOld(this, invoiceList, this));
        listViewInvoices.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        listViewInvoices.setHasFixedSize(true);*/
    }

    @Override
    public void onInvoiceSelected(Invoice invoice) {

    }

    public void formasPago(View view) {
        startActivity(new Intent(this, PaymentsActivity.class));
    }

    public void startTransferPayment(View view){
        startActivity(new Intent(this, TransferPaymentActivity.class));
    }

    public void printCartola(View view){
        imprimirCartola();
    }

    private Printer _printer;

    private void imprimirCartola() {
        try {
            _printer = Printer.getInstance(config.PRINTER_MODEL, this);

            _printer.printerCallback = new Printer.PrinterCallback() {
                @Override
                public void onPrinterReady() {

                    _printer.format(2, 2, Printer.ALINEACION.CENTER);
                    _printer.printLine("CARTOLA");
                    _printer.sendSeparadorHorizontal();

                    if(paymentProvider != null) {
                        _printer.printLine("PROVEEDOR  : " + paymentProvider.getName());
                        _printer.printLine("RUT        : " + paymentProvider.getRut());
                    }

                    //_printer.printLine("DISPONIBLE : " + Formato.FormateaDecimal(disponible, FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA));
                    _printer.printLine("DEUDA : " + "1000 $");

                    _printer.sendSeparadorHorizontal(false);
                    _printer.printOpposite("FC.NUM  IMPORTE  ", " VTO.");

                    for (Invoice invoice : invoiceList) {
                        _printer.printLine(invoice.getInvoiceNumber());
                    }

                    _printer.sendSeparadorHorizontal(true);

                    _printer.format(1, 1, Printer.ALINEACION.CENTER);
                    _printer.printLine(Formato.FormateaDate(new Date(), Formato.DateFormat) + " " + Formato.FormateaDate(new Date(), Formato.TimeFormat));
                    _printer.footer();


                    /*
                    Double deuda = 0d, disponible = cliente.getLimite(); //por default, tiene como limite el disponible y no tiene deuda

                    CuentaCteTotal _total = new CuentaCteTotalesDao(getContentResolver()).find(cliente.getId());

                    if (_total != null) {  //si hay registros del cliente
                        deuda = _total.getSaldo();
                        disponible = _total.getDisponible();
                    }


                    _printer.printLine("LIMITE     : " + Formato.FormateaDecimal(cliente.getLimite(), FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA));
                    _printer.printLine("FEC. DESDE : " + Formatos.FormateaDate(fechaDesde, Formatos.DateFormat));
                    _printer.printLine("FEC. HASTA : " + Formatos.FormateaDate(fechaHasta, Formatos.DateFormat));

                    //_printer.print("01-10 VT#00001253   5000    6000");

                    //List<CuentaCte> _movimientos = new CuentaCteDao(getContentResolver()).list("id_cliente = " + String.valueOf(cliente.getId()) + " and fecha >= '" + Formatos.FormateaDate(fechaDesde, Formatos.DbDate_Format) + " 00:00:00' and fecha <= '" + Formatos.FormateaDate(fechaHasta, Formatos.DbDate_Format) + " 23:59:59'" , null, "_id");
                    List<CuentaCte> _movimientos = new CuentaCteDao(getContentResolver()).list("id_cliente = " + String.valueOf(cliente.getId()), null, "_id");

                    double saldo = 0d; //el saldo del primer movimiento es la deuda actual
                    boolean printSaldoInit = false;
                    for (CuentaCte _mov : _movimientos) {//traemos todos los movimientos, a los anteriores a la fechaDesde los sumamos como SALDO de inicio

                        double monto = _mov.getTipoMov() == CuentaCte.TipoMovimientoCtaCte.COBRO.ordinal() ? _mov.getMonto() * (-1) : _mov.getMonto();

                        if (_mov.getFechaHora().compareTo(fechaDesde) < 0)
                            saldo += monto;//acumulamos el saldo como SALDO inicial
                        else {

                            if (!printSaldoInit) {
                                String _saldo = Formato.FormateaDecimal(saldo, FORMATO_DECIMAL);
                                _printer.printOpposite("SALDO INICIAL", _saldo);

                                printSaldoInit = true;
                            }

                            String concepto = "";
                            if (_mov.getTipoMov() == CuentaCte.TipoMovimientoCtaCte.DEUDA.ordinal() && _mov.getIdVenta() == CuentaCte.ID_PRESTAMO) {
                                // es un PRESTAMO
                                concepto = "PRESTAMO   "; //long 11
                            } else if (_mov.getTipoMov() == CuentaCte.TipoMovimientoCtaCte.DEUDA.ordinal()) {
                                String nroVenta = "00000000" + String.valueOf(_mov.getIdVenta());
                                nroVenta = nroVenta.substring(nroVenta.length() - 8);
                                String tipoVta = TextUtils.isEmpty(_mov.getTipoVenta()) ? "VT" : _mov.getTipoVenta().substring(0, 2).toUpperCase();
                                concepto = tipoVta + "#" + nroVenta; //long 3+8 = 11
                            } else {
                                // es un COBRO
                                concepto = "PAGO       "; //long 11
                            }

                            String _monto = Formato.FormateaDecimal(monto, FORMATO_DECIMAL);
                            _printer.printOpposite(Formatos.FormateaDate(_mov.getFechaHora(), Formatos.DayMonthFormat) + " " + concepto, _monto);

                            saldo += monto;
                        }
                    }

                    int k = (_printer.get_longitudLinea() - 5) / 2; //(SALDO)

                    _printer.print(_printer.getPadding(k, "-") + "SALDO" + _printer.getPadding(k + 1, "-"));

                    _printer.format(2, 1, Printer.ALINEACION.CENTER);
                    if (deuda >= 0)
                        _printer.printLine("DEUDA ACTUAL : " + Formato.FormateaDecimal(deuda, FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA));
                    else
                        _printer.printLine("SALDO A FAVOR : " + Formato.FormateaDecimal(deuda, FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA));

                    */

                }
            };

            //iniciamos la printer
            if (!_printer.initialize()) {
                Toast.makeText(this, "Error al inicializar", Toast.LENGTH_SHORT).show();
                _printer.end();
            }

        } catch (Exception ex) {
            Toast.makeText(this, "Error al imprimir detalle: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

}
