package com.pds.gestionpagos.view;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.util.ConnectivityUtils;
import com.pds.gestionpagos.R;

public class TransferPaymentActivity extends AppCompatActivity {

    private WebView webViewPagos;
    private ProgressBar progressBar;

    //private String paymentUrl = "http://190.111.56.80:39877/PDS/Payzen/SetPayment.aspx?Amount=1999&Currency=152";
    //private String paymentUrl = "http://190.111.56.80:39877/PDS_TEST/Payzen/SetPayment.aspx?Amount=2000&Currency=CLP&TranId=FC12345&Serv=VALE%20ELECTRONICO&ServDesc=Abono%20Servicio%20Vale%20Sept2016&UserEmail=hcarrozzo@gmail.com&UserName=hernan&UserRUT=123123123";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_payment);


        webViewPagos = (WebView)findViewById(R.id.wv_pago);
        progressBar = (ProgressBar) findViewById(R.id.prg_pago_progress);

        if(!ConnectivityUtils.isOnline(this)){
            Toast.makeText(TransferPaymentActivity.this, "Sin conexion!", Toast.LENGTH_SHORT).show();
        }
        else{
            iniciarPago(null);
        }
    }


    public void iniciarPago(View view){

        //Toast.makeText(TransferPaymentActivity.this, "pago!", Toast.LENGTH_SHORT).show();

        /*if (!ConnectivityUtils.isOnline(this))
            return false;*/

        // --- Configure related browser settings ---
        WebSettings webSettings = webViewPagos.getSettings();
        webSettings.setJavaScriptEnabled(true);
        //webSettings.setLoadsImagesAutomatically(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setSupportMultipleWindows(true);

        webViewPagos.setInitialScale(1);
        webViewPagos.clearCache(false);
        webViewPagos.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        webViewPagos.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webViewPagos.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(TransferPaymentActivity.this, "ERROR " + description, Toast.LENGTH_LONG).show();
                //super.onReceivedError(view, errorCode, description, failingUrl);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                //Toast.makeText(MainActivity.this, "ERROR " + description, Toast.LENGTH_LONG).show();

                super.onReceivedError(view, request, error);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                //scrollToBottom
                view.scrollTo(0, view.getContentHeight());
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                // evita que los enlaces se abran fuera nuestra app en el navegador de android

                /*if (url.toLowerCase().contains("survey-thanks") || url.toLowerCase().contains("survey-taken")) {

                    new Thread(new Runnable() {

                        public void run() {
                            try {
                                //esperamos unos seg para cerrar la encuesta
                                Thread.sleep(4000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                }
                            });

                        }
                    }).start();


                }*/
                return false;
            }
        });
        webViewPagos.setWebChromeClient(new PopupWebChromeClient(this, webViewPagos, (FrameLayout)webViewPagos.getParent(), progressBar));

        /*webViewPagos.setWebChromeClient(new WebChromeClient() {

            @Override
            public void onProgressChanged(WebView view, int progress) {
                progressBar.setProgress(0);
                progressBar.setVisibility(View.VISIBLE);
                progressBar.setProgress(progress * 1000);
                progressBar.incrementProgressBy(progress);

                if (progress == 100) {
                    progressBar.setVisibility(View.GONE);
                }
            }

            // Add new webview in same window
            @Override
            public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
                WebView childView = new WebView(getContext());
                childView.getSettings().setJavaScriptEnabled(true);
                childView.setWebChromeClient(this);
                childView.setWebViewClient(new FacebookWebViewClient());
                childView.setLayoutParams(FILL);
                mContent.addView(childView);
                WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
                transport.setWebView(childView);
                resultMsg.sendToTarget();
                return true;


                return super.onCreateWindow(view, isDialog, isUserGesture, resultMsg);
            }

            // remove new added webview whenever onCloseWindow gets called for new webview.
            @Override
            public void onCloseWindow(WebView window) {
                mContent.removeViewAt(mContent.getChildCount() - 1);
            }
        });
        */

        String paymentUrl = generatePaymentUrl();

        webViewPagos.loadUrl(paymentUrl);
    }


    private String generatePaymentUrl(){

        Config config = new Config(this);

        return String.format(
                "http://190.111.56.80:39877/PDS_TEST/Payzen/SetPayment.aspx?Amount=%s&Currency=%s&TranId=%s&Serv=%s&ServDesc=%s&UserEmail=%s&UserName=%s&UserRUT=%s",
                2000,
                "CLP",
                "FC12345",
                "VALE%20ELECTRONICO",
                "Abono%20Servicio%20Vale%20Sept2016",
                "hcarrozzo@gmail.com",
                config.COMERCIO,
                "123123123"//config.CLAVEFISCAL
        );
    }

    @Override
    public void onBackPressed() {
        AlertDialog dialog = new AlertDialog.Builder(this).setIcon(android.R.drawable.ic_menu_help).setTitle("Salir?").setMessage("Esta seguro de salir?").create();

        dialog.setButton(AlertDialog.BUTTON_POSITIVE, "SI", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                TransferPaymentActivity.super.onBackPressed();
            }
        });
        dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });
        dialog.show();
    }
}
