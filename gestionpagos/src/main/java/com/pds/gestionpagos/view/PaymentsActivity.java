package com.pds.gestionpagos.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.pds.gestionpagos.R;

public class PaymentsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payments);
    }

    public void startPayment(View view){
        switch (view.getId()){
            case R.id.payment_btn_bco_estado:{
                Toast.makeText(PaymentsActivity.this, "Banco estado!", Toast.LENGTH_SHORT).show();
            }break;
            case R.id.payment_btn_caja_vecina:{
                Toast.makeText(PaymentsActivity.this, "Caja vecina!", Toast.LENGTH_SHORT).show();
            }break;
            case R.id.payment_btn_transfer:{
                startTransferPayment();
            }break;
        }
    }

    private void startTransferPayment(){
        startActivity(new Intent(this, TransferPaymentActivity.class));
    }
}
