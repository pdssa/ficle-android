package com.pds.gestionpagos.view;

import android.app.Activity;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.pds.gestionpagos.R;

/**
 * Created by Hernan on 31/08/2016.
 */
public class PopupWebChromeClient extends WebChromeClient implements View.OnClickListener {

    protected Activity activity;
    protected WebView parentWebView;
    protected FrameLayout container;
    protected View popupView;
    protected WebView popupWebView;
    protected ProgressBar progressBar;

    public PopupWebChromeClient(
            Activity activity,
            WebView parentWebView,
            FrameLayout container,
            ProgressBar progressBar
    )
    {
        super();
        this.activity = activity;
        this.parentWebView = parentWebView;
        this.container = container;
        this.progressBar = progressBar;
    }

    @Override
    public boolean onCreateWindow(WebView view, boolean dialog, boolean userGesture, Message resultMsg) {

        //this.parentWebView.setVisibility(WebView.GONE);
        this.popupView = LayoutInflater.from(this.activity).inflate(R.layout.web_view_pay_layout, null );
        this.popupView.findViewById(R.id.web_view_pay_btn_cancel).setOnClickListener(this);
        this.popupView.findViewById(R.id.web_view_pay_btn_end).setOnClickListener(this);

        //this.popupView = new LinearLayout(this.activity);
        this.popupView.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT)
        );
        //this.popupView.setBackgroundResource(R.color.fondo_gris_transparente);

        this.popupWebView = new WebView(this.activity);

        // setup popupview and add
        this.popupWebView.getSettings().setJavaScriptEnabled(true);
        this.popupWebView.setWebChromeClient(this);
        //this.popupWebView.setWebViewClient(new ApkfWebViewClient(this.activity, true));
        this.popupWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(activity, "ERROR " + description, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                // evita que los enlaces se abran fuera nuestra app en el navegador de android
                return false;
            }
        });

        /*this.popupWebView.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        ));*/
        //this.popupWebView.setLayoutParams(new LinearLayout.LayoutParams(800, 300, Gravity.CENTER | Gravity.TOP));
        this.popupWebView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT));
        //this.popupWebView.setPadding(40,40,40,40);

        ((LinearLayout)this.popupView.findViewById(R.id.web_view_pay_container)).addView(this.popupWebView);
        this.container.addView(this.popupView);

        // send popup window infos back to main (cross-document messaging)
        WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
        transport.setWebView(popupWebView);
        resultMsg.sendToTarget();

        return true;
    }

    // remove new added webview on close
    @Override
    public void onCloseWindow(WebView window) {
        if(this.popupView != null && this.popupView.getVisibility() == WebView.VISIBLE){
            this.popupView.setVisibility(WebView.GONE);

            Log.d("PopupWebChromeClient", "PopupWindow close");
        }
        else {
            super.onCloseWindow(window);
            this.parentWebView.setVisibility(WebView.GONE);
            this.activity.finish();

            Log.d("PopupWebChromeClient", "Window close");
        }
    }

    @Override
    public void onProgressChanged(WebView view, int progress) {
        progressBar.setProgress(0);
        progressBar.setVisibility(View.VISIBLE);
        progressBar.setProgress(progress * 1000);
        progressBar.incrementProgressBy(progress);

        if (progress == 100) {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        /*if(v.getId() == R.id.web_view_pay_btn_end){

        }*/
        onCloseWindow(this.popupWebView);
    }


    /*public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (mWebView.canGoBack()) {
                        mWebView.goBack();
                    } else {
                        finish();
                    }
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }*/
}