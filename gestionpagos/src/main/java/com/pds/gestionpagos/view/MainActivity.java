package com.pds.gestionpagos.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.pds.gestionpagos.R;
import com.pds.gestionpagos.adapter.PaymentProviderAdapter;
import com.pds.gestionpagos.dao.PaymentProviderDAO;
import com.pds.gestionpagos.model.PaymentProvider;

import java.util.List;

public class MainActivity extends AppCompatActivity implements PaymentProviderAdapter.PaymentProviderListener {

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.main_providers_rec);

        loadProviders();
    }

    private void loadProviders() {
        recyclerView.setAdapter(new PaymentProviderAdapter(this, new PaymentProviderDAO().getPaymentProviders(), this));
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerView.setHasFixedSize(true);
    }

    public void pagosPendientes(View view) {
        startPaymentListActivity(null);
    }

    public void pagosRealizados(View view) {
    }

    public void formasPago(View view) {
        startActivity(new Intent(this, PaymentsActivity.class));
    }

    private void startPaymentListActivity(PaymentProvider paymentProvider) {

        Intent intent = new Intent(this, InvoicesListActivity.class);

        if (paymentProvider != null)
            intent.putExtra(InvoicesListActivity.KEY_PROVIDER, paymentProvider);

        startActivity(intent);
    }

    @Override
    public void onPaymentProviderSelected(PaymentProvider paymentProvider) {
        startPaymentListActivity(paymentProvider);
    }
}
