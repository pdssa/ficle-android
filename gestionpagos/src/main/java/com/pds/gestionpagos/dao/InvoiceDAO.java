package com.pds.gestionpagos.dao;

import android.content.Context;
import android.content.res.AssetManager;

import com.google.gson.Gson;
import com.pds.gestionpagos.model.Invoice;
import com.pds.gestionpagos.model.InvoiceContainer;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

/**
 * Created by Hernan on 18/08/2016.
 */
public class InvoiceDAO {

    public List<Invoice> getInvoices(Context context){
        InvoiceContainer invoiceContainer = (InvoiceContainer)getObjectJSON(context, InvoiceContainer.class, "invoices.json");

        return invoiceContainer.getInvoiceList();
    }

    private Object getObjectJSON(Context context, Class aClass, String fileName){

        Object object = null;
        try{

            AssetManager manager = context.getAssets();
            InputStream inputStream = manager.open(fileName);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            Gson gson = new Gson();
            object = gson.fromJson(bufferedReader, aClass);
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return object;
    }
}
