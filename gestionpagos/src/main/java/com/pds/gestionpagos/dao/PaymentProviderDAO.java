package com.pds.gestionpagos.dao;

import com.pds.gestionpagos.R;
import com.pds.gestionpagos.model.PaymentProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hernan on 18/08/2016.
 */
public class PaymentProviderDAO {

    public List<PaymentProvider> getPaymentProviders(){

        List<PaymentProvider> providerList = new ArrayList<>();
        providerList.add(new PaymentProvider("11111", "ABC", "VALE ELECTRONICO", R.drawable.vale ));
        providerList.add(new PaymentProvider("11111", "ABC", "COCA COLA EMBONOR", R.drawable.coca_logo ));
        providerList.add(new PaymentProvider("11111", "ABC", "JJD", R.drawable.jjd_logo));
        providerList.add(new PaymentProvider("11111", "ABC", "VALE ELECTRONICO", R.drawable.vale ));
        providerList.add(new PaymentProvider("11111", "ABC", "VALE ELECTRONICO", R.drawable.vale ));


        return providerList;
    }

}
