package com.pds.gestionpagos.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Hernan on 18/08/2016.
 */
public class PaymentProvider implements Parcelable {
    private int icon;
    private String name;
    private String code;
    private String rut;

    public PaymentProvider(String rut, String code, String name, int icon) {
        this.rut = rut;
        this.code = code;
        this.name = name;
        this.icon = icon;
    }

    protected PaymentProvider(Parcel in) {
        icon = in.readInt();
        name = in.readString();
        code = in.readString();
        rut = in.readString();
    }

    public static final Creator<PaymentProvider> CREATOR = new Creator<PaymentProvider>() {
        @Override
        public PaymentProvider createFromParcel(Parcel in) {
            return new PaymentProvider(in);
        }

        @Override
        public PaymentProvider[] newArray(int size) {
            return new PaymentProvider[size];
        }
    };

    public int getIcon() {
        return icon;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public String getRut() {
        return rut;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(icon);
        dest.writeString(name);
        dest.writeString(code);
        dest.writeString(rut);
    }
}
