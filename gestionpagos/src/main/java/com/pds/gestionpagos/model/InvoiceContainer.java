package com.pds.gestionpagos.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Hernan on 18/08/2016.
 */
public class InvoiceContainer {
    @SerializedName("invoices")
    private List<Invoice> invoiceList;

    public List<Invoice> getInvoiceList() {
        return invoiceList;
    }

    public InvoiceContainer(){

    }
}
