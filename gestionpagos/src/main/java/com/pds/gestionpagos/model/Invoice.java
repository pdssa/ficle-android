package com.pds.gestionpagos.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Hernan on 18/08/2016.
 */
public class Invoice {
    private PaymentProvider provider;

    @SerializedName("number")
    private String invoiceNumber;

    @SerializedName("amount")
    private double invoiceAmount;

    @SerializedName("date")
    private String invoiceDate;

    @SerializedName("expiration")
    private String expirationDate;

    @SerializedName("payment")
    private double paymentAmount;

    public Invoice(){

    }

    public PaymentProvider getProvider() {
        return provider;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public double getInvoiceAmount() {
        return invoiceAmount;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public double getPaymentAmount() {
        return paymentAmount;
    }
}
