package com.pds.lib.maincommon.ping.db;

/**
 * Created by Hernan on 22/11/2016.
 */
public class PingPerDayTable {

    public static final String TABLE_NAME  = "TBL_PING_PER_DAY";

    // TBL_PING_PER_DAY Columns names
    public static final String KEY_ID = "id";
    public static final String KEY_COUNT_LOG_OFF_TOTAL = "count_log_off_total";
    public static final String KEY_COUNT_LOG_OFF_OK    = "count_log_off_ok";
    public static final String KEY_AVG_ELAPSED_TIME    = "avg_elapsed_time";
    public static final String KEY_FECHA               = "fecha";
    public static final String KEY_COUNT_LOG_IN_TOTAL  = "count_log_int_total";
    public static final String KEY_COUNT_LOG_IN_OK     = "count_log_in_ok";

    public static final String CREATE_PING_PER_DAY_TABLE = "CREATE TABLE " + TABLE_NAME + "("       +
            KEY_ID                              + " INTEGER PRIMARY KEY,"   +
            KEY_COUNT_LOG_OFF_TOTAL             + " INTEGER,"               +
            KEY_COUNT_LOG_OFF_OK                + " INTEGER,"               +
            KEY_AVG_ELAPSED_TIME                + " INTEGER,"               +
            KEY_FECHA                           + " INTEGER,"                  +
            KEY_COUNT_LOG_IN_TOTAL              + " INTEGER,"               +
            KEY_COUNT_LOG_IN_OK                 + " INTEGER" + ")";
}
