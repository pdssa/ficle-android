package com.pds.lib.maincommon.sync.main;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.pds.common.util.ConnectivityUtils;
import com.pds.lib.maincommon.api.ApiParams;
import com.pds.lib.maincommon.api.ApiResponse;
import com.pds.lib.maincommon.sync.model.DeviceInfo;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Hernan on 29/11/2016.
 */
public class SendDeviceInfoTask extends AsyncTask<Void, String, ApiResponse> {
    private Context _context;
    private static String TAG = "SendDeviceInfoTask";
    private String ERROR = "";

    private ApiParams apiParams;

    public SendDeviceInfoTask(Context _context, ApiParams apiParams) {
        this.apiParams = apiParams;
        this._context = _context;
    }

    @Override
    protected ApiResponse doInBackground(Void... params) {
        try {
            Log.v(TAG, "Sending device info...");
            //Logger.RegistrarEvento(_context, "i", "Download: Start", "");

            if (!ConnectivityUtils.isOnline(_context)) {
                throw new Exception("No hay una conexion disponible");
            }

            DeviceInfo deviceInfo = new DeviceInfoHelper(this._context).getDeviceInfo();
            return sendParamsToServer(deviceInfo);

        } catch (IOException e) {
            ERROR = "Error IO: " + e.getMessage();
            return null;
        } catch (Exception e) {
            ERROR = "Error: " + e.getMessage();
            return null;
        }
    }

    @Override
    protected void onProgressUpdate(String... p) {

    }

    @Override
    protected void onPostExecute(ApiResponse apiResponse) {
        if(apiResponse == null){
            Log.e(TAG, ERROR);
        }
        else if(!apiResponse.hasError()){
            Log.v(TAG, "Result OK");
        }
        else{
            Log.e(TAG, ERROR);
        }
    }

    private ApiResponse sendParamsToServer(DeviceInfo obj) {
        try {

            String jsonToPost = new Gson().toJson(obj);

            URL url = new URL(this.apiParams.getApiURL(ApiParams.DEVICE_INFO_CONTROLLER));
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            conn.setConnectTimeout(ApiParams.CONN_TIMEOUT);

            Log.v(TAG, "json:" + jsonToPost);

            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");
            conn.setUseCaches(false);

            DataOutputStream printout = new DataOutputStream(conn.getOutputStream());
            printout.writeBytes(jsonToPost);
            printout.flush();
            printout.close();

            Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

            StringBuilder sb = new StringBuilder();
            for (int c; (c = in.read()) >= 0; )
                sb.append((char) c);

            Log.v(TAG, "response:" + sb.toString());

            return new Gson().fromJson(sb.toString(), ApiResponse.class);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
