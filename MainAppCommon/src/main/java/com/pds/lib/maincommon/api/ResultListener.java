package com.pds.lib.maincommon.api;

public interface ResultListener<T> {
    void onSuccess(T result);

    void onError(String error, boolean retryOperation);
}