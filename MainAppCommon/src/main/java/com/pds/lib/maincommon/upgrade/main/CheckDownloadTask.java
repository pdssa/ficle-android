package com.pds.lib.maincommon.upgrade.main;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Xml;

import com.google.gson.Gson;
import com.pds.common.Logger;
import com.pds.common.util.ConnectivityUtils;
import com.pds.lib.maincommon.api.ApiParams;
import com.pds.lib.maincommon.upgrade.model.AppUpgrade;
import com.pds.lib.maincommon.upgrade.model.AppVersion;
import com.pds.lib.maincommon.upgrade.model.AppVersionResponse;
import com.pds.lib.maincommon.upgrade.utils.XmlAppsParser;

import org.xmlpull.v1.XmlSerializer;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hernan on 7/20/16.
 */
public class CheckDownloadTask extends AsyncTask<Void, String, String> {

    private Context _context;
    private static String TAG = "CheckDownloadTask";

    private ApiParams apiParams;

    public static String downloadFolder = "/download/lep/apps/";

    public CheckDownloadTask(Context _context, ApiParams apiParams) {
        this.apiParams = apiParams;
        this._context = _context;
    }

    private AppVersionResponse GetVersionsFromServer() throws Exception {
        HttpURLConnection conn = null;

        try {
            URL url = new URL(String.format("%s?deviceId=%s&IMEI=%s", this.apiParams.getApiURL(ApiParams.VERSIONS_CONTROLLER), apiParams.API_DEVICE_ID, apiParams.API_IMEI));
            conn = (HttpURLConnection) url.openConnection();

            conn.setConnectTimeout(ApiParams.CONN_TIMEOUT);

            //Log.v(TAG, "json:" + jsonToPost);

            //conn.setDoOutput(true);
            //conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");
            //conn.setUseCaches(false);

            /*DataOutputStream printout = new DataOutputStream(conn.getOutputStream());
            printout.writeBytes(jsonToPost);
            printout.flush();
            printout.close();*/

            Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

            StringBuilder sb = new StringBuilder();
            for (int c; (c = in.read()) >= 0; )
                sb.append((char) c);

            Log.v(TAG, "response:" + sb.toString());

            return new Gson().fromJson(sb.toString(), AppVersionResponse.class);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(conn != null)
                conn.disconnect();
        }

        return null;
    }

    @Override
    protected String doInBackground(Void... params) {
        try {
            Log.v(TAG, "Starting checking apps to download...");
            //Logger.RegistrarEvento(_context, "i", "Download: Start", "");

            if (!ConnectivityUtils.isOnline(_context)) {
                return "No hay una conexion disponible";
            }

            publishProgress("Obteniendo versiones del servidor...");

            AppVersionResponse response = GetVersionsFromServer();

            if(response == null)
                return "Error en respuesta del servidor";
            else if(!response.hasError()){
                List<AppVersion> lstApps = response.getVersiones();

                publishProgress("Revisando si es necesario actualizar...");

                //4-foreach
                int count = 0;

                List<AppUpgrade> lstAppsToInstall = new ArrayList<AppUpgrade>();

                for (AppVersion appVersion : lstApps) {
                    AppUpgrade app = new AppUpgrade(appVersion);

                    app.set_context(_context);

                    if (app.CorrespondeActualizar()) {
                        //si corresponde actualizar

                        //1-Descargamos (solo si no existe)
                        app.Download(downloadFolder, false);

                        lstAppsToInstall.add(app);

                        count++;
                    } else {
                        app.DeleteFileIfExists(downloadFolder);
                    }
                }

                String xmlAppsToInstall = "";
                if (lstAppsToInstall.size() > 0) {
                    try {
                        StringWriter buffer = new StringWriter();
                        writeXML(lstAppsToInstall, buffer);
                        xmlAppsToInstall = buffer.toString();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        xmlAppsToInstall = "";
                    }
                }

                SharedPreferences pref = _context.getSharedPreferences("com.pds.lep", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("apps_to_install", xmlAppsToInstall);
                editor.apply();

                return count > 0 ? "Descargas: " + String.valueOf(count) : "No update require";
            } else {
                return "Error: " + response.getResponseMessage();
            }

        } catch (IOException e) {
            //return "No es posible conectarse. Intente mas tarde.";
            return "Error IO: " + e.getMessage();
        } catch (Exception e) {
            return "Error: " + e.getMessage();
        }
    }

    @Override
    protected void onProgressUpdate(String... p) {
        for (String _p : p) {
            Log.v(TAG, _p);
        }
    }

    @Override
    protected void onPostExecute(String result) {
        Log.v(TAG, "Finished downloading apks...");
        //Logger.RegistrarEvento(_context, "i", "Download: End", result);
    }

    @Override
    protected void onCancelled(String s) {
        Log.e(TAG, s);
    }

    private void writeXML(List<AppUpgrade> appsList, StringWriter outputStream) throws Exception {
        //we create a XmlSerializer in order to write xml data
        XmlSerializer serializer = Xml.newSerializer();

        //we set the PrintWriter as output for the serializer
        serializer.setOutput(outputStream);
        serializer.startDocument(null, null);

        //tag ROOT
        serializer.startTag(null, "versiones");

        for (AppUpgrade appUpgrade : appsList) {

            serializer.startTag(null, "VERSIONsAll");

            serializer.startTag(null, "versionPackage");
            serializer.text(appUpgrade.get_packageName());
            serializer.endTag(null, "versionPackage");

            serializer.startTag(null, "versionVersion");
            serializer.text(appUpgrade.get_packageLastVersion());
            serializer.endTag(null, "versionVersion");

            serializer.startTag(null, "versionApkUrl");
            serializer.text(appUpgrade.get_localPath());
            serializer.endTag(null, "versionApkUrl");

            serializer.endTag(null, "VERSIONsAll");
        }

        serializer.endTag(null, "versiones");

        serializer.endDocument();

        //escribimos el xml dentro del OutputStream
        serializer.flush();

        //finally we close the file stream
        outputStream.flush();

    }
}
