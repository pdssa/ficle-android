package com.pds.lib.maincommon.ping.main;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.pds.lib.maincommon.db.DbHelper;
import com.pds.lib.maincommon.ping.db.PingPerDayTable;
import com.pds.lib.maincommon.ping.db.PingTable;
import com.pds.lib.maincommon.ping.model.Ping;
import com.pds.lib.maincommon.ping.model.PingPerDay;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by Hernan on 22/11/2016.
 */
public class PingController {

    private static final String TAG = "PingController";
    private DbHelper dbHelper;

    public PingController(Context context){
        dbHelper = new DbHelper(context);
    }

    private SQLiteDatabase getWritableDatabase(){
        return  dbHelper.getWritableDatabase();
    }

    private SQLiteDatabase getReadableDatabase(){
        return  dbHelper.getReadableDatabase();
    }

    // Adding new PingPerDay
    public boolean addPingPerDay(PingPerDay ppd)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(PingPerDayTable.KEY_COUNT_LOG_OFF_TOTAL, ppd.getCOUNT_LOG_OFF_TOTAL());
        values.put(PingPerDayTable.KEY_COUNT_LOG_OFF_OK, ppd.getCOUNT_LOG_OFF_OK());
        values.put(PingPerDayTable.KEY_AVG_ELAPSED_TIME, ppd.getAVG_ELAPSED_TIME());
        values.put(PingPerDayTable.KEY_FECHA, ppd.getFECHA());
        values.put(PingPerDayTable.KEY_COUNT_LOG_IN_TOTAL, ppd.getCOUNT_LOG_IN_TOTAL());
        values.put(PingPerDayTable.KEY_COUNT_LOG_IN_OK, ppd.getCOUNT_LOG_IN_OK());

        try
        {
            db.insert(PingPerDayTable.TABLE_NAME, null, values);

        } catch (Exception e)
        {
            Log.v(TAG, e.toString());
            return false;
        }

        db.close();

        return true;

    }

    // Adding new Ping
    public void addPing(Ping ping)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(PingTable.KEY_PING_OK_NOT, ping.getPING_OK_NOT());
        values.put(PingTable.KEY_FECHA, ping.getFECHA());
        values.put(PingTable.KEY_ELAPSED_TIME, ping.getELAPSED_TIME());
        values.put(PingTable.KEY_LOGGED_IN_OR_NOT, ping.getLOGGED_IN_OR_NOT());

        try
        {
            db.insert(PingTable.TABLE_NAME, null, values);

        } catch (Exception e)
        {
            Log.v(TAG, e.toString());
        }

        db.close();
    }

    // Get PingPerDay by id.
    public PingPerDay getPingPerDayById(int id)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(
                "SELECT * FROM " + PingPerDayTable.TABLE_NAME +
                        " WHERE id = " + id + ";", null);

        if (cursor != null)
            cursor.moveToFirst();

        PingPerDay ppd = new PingPerDay(
                cursor.getInt(0),
                cursor.getInt(1),
                cursor.getInt(2),
                cursor.getInt(3),
                cursor.getInt(4),
                cursor.getLong(5),
                cursor.getInt(6));

        return ppd;
    }

    // Get Ping by id.
    public Ping getPingById(int id)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " +  PingTable.TABLE_NAME + " WHERE id = " + id + ";", null);

        if (cursor != null)
            cursor.moveToFirst();

        Ping ping = new Ping(
                cursor.getInt(0),
                cursor.getInt(1),
                cursor.getInt(2),
                cursor.getInt(3),
                cursor.getInt(4));

        return ping;
    }

    // Get All Pings
    public List<Ping> getAllPings()
    {

        List<Ping> pingList = new ArrayList<Ping>();

        String selectQuery = "SELECT * FROM " +  PingTable.TABLE_NAME + ";";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst())
        {
            do
            {
                Ping ping = new Ping();
                ping.setID(cursor.getInt(0));
                ping.setPING_OK_NOT(cursor.getInt(1));
                ping.setFECHA(cursor.getInt(2));
                ping.setELAPSED_TIME(cursor.getInt(3));
                ping.setLOGGED_IN_OR_NOT(cursor.getInt(4));
                pingList.add(ping);

            } while (cursor.moveToNext());
        }

        cursor.close();

        return pingList;
    }

    public List<Date> getPingDates()
    {

        List<Date> pingDatesList = new ArrayList<Date>();

        String selectQuery = "SELECT distinct strftime('%Y-%m-%d', " + PingTable.KEY_FECHA + " / 1000, 'unixepoch', 'localtime') fecha  FROM " + PingTable.TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        int count = cursor.getCount();

        if (cursor.moveToFirst())
        {
            do
            {
                SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-M-dd");
                dateformat.setTimeZone(TimeZone.getDefault());
                Date day = null;

                try
                {

                    day = dateformat.parse(cursor.getString(0));

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Log.v(TAG, "ORIGINAL DATE: " + day.toString());

                SimpleDateFormat dateformatest= new SimpleDateFormat("yyyy-M-dd");
                dateformatest.setTimeZone(TimeZone.getDefault());
                Date daytest = null;

                try
                {

                    daytest = dateformatest.parse(cursor.getString(0));

                } catch (ParseException e) {
                    e.printStackTrace();
                }


                Log.v(TAG, "TEST DATE: " + daytest.toString());


                pingDatesList.add(day);

            } while (cursor.moveToNext());
        }

        cursor.close();

        return pingDatesList;

    }


    // Get All Ping Per Day.
    public List<PingPerDay> getAllPingPerDay()
    {

        List<PingPerDay> pingList = new ArrayList<PingPerDay>();

        String selectQuery = "SELECT * FROM " + PingPerDayTable.TABLE_NAME + ";";

        Log.v(TAG, selectQuery);

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        int count = cursor.getCount();

        if (cursor.moveToFirst())
        {
            do
            {
                PingPerDay ppd = new PingPerDay();
                ppd.setID(cursor.getInt(0));
                ppd.setCOUNT_LOG_OFF_TOTAL(cursor.getInt(1));
                ppd.setCOUNT_LOG_OFF_OK(cursor.getInt(2));
                ppd.setAVG_ELAPSED_TIME(cursor.getFloat(3));
                ppd.setFECHA(cursor.getLong(4));
                ppd.setCOUNT_LOG_IN_TOTAL(cursor.getInt(5));
                ppd.setCOUNT_LOG_IN_OK(cursor.getInt(6));
                pingList.add(ppd);

            } while (cursor.moveToNext());
        }

        cursor.close();

        return pingList;
    }


    // Get ping last insert
    public Ping getPingLastInsert()
    {
        String countQuery = "SELECT * FROM "    +  PingTable.TABLE_NAME +
                " WHERE ID = (SELECT MAX(ID) FROM "     +  PingTable.TABLE_NAME + ");";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        Ping ping = new Ping();

        if (cursor.moveToFirst())
        {
            do
            {
                ping.setID(cursor.getInt(0));
                ping.setPING_OK_NOT(cursor.getInt(1));
                ping.setFECHA(cursor.getLong(2));
                ping.setELAPSED_TIME(cursor.getInt(3));
                ping.setLOGGED_IN_OR_NOT(cursor.getInt(4));

            } while (cursor.moveToNext());
        }

        cursor.close();

        return ping;
    }

    // Get ping Count Logoff Total
    public int getPingCountLogoffTotal(String date)
    {
        String countQuery = "SELECT  COUNT (*) FROM " +  PingTable.TABLE_NAME +
                " WHERE " + PingTable.KEY_LOGGED_IN_OR_NOT + " = 0" +
                " AND strftime('%Y-%m-%d', " + PingTable.KEY_FECHA + " / 1000, 'unixepoch','localtime') = '" + date + "';";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.moveToFirst();
        int count= cursor.getInt(0);
        cursor.close();
        return count;
    }

    // Get ping Count Logoff Okay
    public int getPingCountLogoffOkay(String date)
    {
        String countQuery = "SELECT  COUNT (*) FROM " +  PingTable.TABLE_NAME +
                " WHERE " + PingTable.KEY_LOGGED_IN_OR_NOT + " = 0 AND " + PingTable.KEY_PING_OK_NOT + " = 1" +
                " AND strftime('%Y-%m-%d', " + PingTable.KEY_FECHA + " / 1000, 'unixepoch', 'localtime') = '" + date + "';";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.moveToFirst();
        int count= cursor.getInt(0);
        cursor.close();
        return count;
    }

    // Get ping Count Login Total
    public int getPingCountLoginTotal(String date)
    {
        String countQuery = "SELECT  COUNT (*) FROM " +  PingTable.TABLE_NAME +
                " WHERE " + PingTable.KEY_LOGGED_IN_OR_NOT + " = 1" +
                " AND strftime('%Y-%m-%d', " + PingTable.KEY_FECHA + " / 1000, 'unixepoch', 'localtime') = '" + date + "';";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.moveToFirst();
        int count= cursor.getInt(0);
        cursor.close();
        return count;
    }

    // Get ping Count Login Okay
    public int getPingCountLoginOkay(String date)
    {
        String countQuery = "SELECT  COUNT (*) FROM " +  PingTable.TABLE_NAME +
                " WHERE " + PingTable.KEY_LOGGED_IN_OR_NOT + " = 1 AND " + PingTable.KEY_PING_OK_NOT + " = 1" +
                " AND strftime('%Y-%m-%d', " + PingTable.KEY_FECHA + " / 1000, 'unixepoch', 'localtime', 'localtime') = '" + date + "';";

        Log.v(TAG, countQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.moveToFirst();
        int count= cursor.getInt(0);
        cursor.close();
        return count;
    }

    // Get ping Elapsed average
    public float getPingAverageElapsed(String date)
    {
        String countQuery = "SELECT AVG(" + PingTable.KEY_ELAPSED_TIME + ") FROM " + PingTable.TABLE_NAME + " WHERE " + PingTable.KEY_PING_OK_NOT + " = 1" +
                " AND strftime('%Y-%m-%d', " + PingTable.KEY_FECHA + " / 1000, 'unixepoch', 'localtime') = '" + date + "';";

        Log.v(TAG, countQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.moveToFirst();
        float average = cursor.getFloat(0);
        cursor.close();
        return average;
    }

    // Truncate table Ping.
    public void truncatePing()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL("DELETE FROM "+  PingTable.TABLE_NAME);
    }

    // Truncate table PingPerDay.
    public void truncatePingPerDay()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL("DELETE FROM "+ PingPerDayTable.TABLE_NAME);
    }

    // Get ping Count
    public int getPingCount()
    {
        String countQuery = "SELECT  * FROM " + PingTable.TABLE_NAME + ";";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }


    // Get PingPerDay Count
    public int getPingPerDayCount()
    {
        String countQuery = "SELECT * FROM " + PingPerDayTable.TABLE_NAME + ";";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    // Update a PingPerDay
    public int updatePingPerDay(PingPerDay ppd)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(PingPerDayTable.KEY_COUNT_LOG_OFF_TOTAL, ppd.getCOUNT_LOG_OFF_TOTAL());
        values.put(PingPerDayTable.KEY_COUNT_LOG_OFF_OK, ppd.getCOUNT_LOG_OFF_OK());
        values.put(PingPerDayTable.KEY_AVG_ELAPSED_TIME, ppd.getAVG_ELAPSED_TIME());
        values.put(PingPerDayTable.KEY_FECHA, ppd.getFECHA());
        values.put(PingPerDayTable.KEY_COUNT_LOG_IN_TOTAL, ppd.getCOUNT_LOG_IN_TOTAL());
        values.put(PingPerDayTable.KEY_COUNT_LOG_IN_OK, ppd.getCOUNT_LOG_IN_OK());

        return db.update(PingPerDayTable.TABLE_NAME, values, PingPerDayTable.KEY_ID + " = ?",
                new String[]{String.valueOf(ppd.getID())});
    }

    // Update a Ping
    public int updatePing(Ping ping)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(PingTable.KEY_PING_OK_NOT, ping.getPING_OK_NOT());
        values.put(PingTable.KEY_FECHA, ping.getFECHA());
        values.put(PingTable.KEY_ELAPSED_TIME, ping.getELAPSED_TIME());
        values.put(PingTable.KEY_LOGGED_IN_OR_NOT, ping.getLOGGED_IN_OR_NOT());

        return db.update(PingTable.TABLE_NAME, values, PingTable.KEY_ID + " = ?",
                new String[]{String.valueOf(ping.getID())});
    }

    // Delete ping
    public void deletePing(Ping ping)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(PingTable.TABLE_NAME, PingTable.KEY_ID + " = ?",
                new String[] { String.valueOf(ping.getID()) });
        db.close();
    }
}
