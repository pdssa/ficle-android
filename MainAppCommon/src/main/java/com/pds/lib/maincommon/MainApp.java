package com.pds.lib.maincommon;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.text.Spanned;
import android.util.Log;
import android.widget.Toast;

import com.pds.common.activity.App;
import com.pds.lib.maincommon.api.ApiParams;
import com.pds.lib.maincommon.api.ResultListener;
import com.pds.lib.maincommon.login.main.DeviceLoginTask;
import com.pds.lib.maincommon.login.model.DeviceApp;
import com.pds.lib.maincommon.login.model.DeviceLoginResponse;
import com.pds.lib.maincommon.ping.main.PingService;
import com.pds.lib.maincommon.sync.main.SendDeviceInfoTask;
import com.pds.lib.maincommon.upgrade.main.DownloadService;
import com.pds.lib.maincommon.upgrade.main.InstallUpdatesTask;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hernan on 22/11/2016.
 */
public abstract class MainApp extends Application {
    private static String TAG  = "MainApp";

    private static MainApp sInstance;

    public static MainApp getInstance()
    {
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
    }


    //services
    protected static final int SERVICE_LOGIN = 1;
    protected static final int SERVICE_PING = 2;
    protected static final int SERVICE_DOWNLOAD_APPS = 4;
    protected static final int SERVICE_SEND_DATA = 8;

    private int enabledServices = 0;
    protected void setEnabledAppServices(int enabledAppServices){
        enabledServices = enabledAppServices;
    }

    public boolean isServiceEnabled(int service){
        return (enabledServices & service) != 0;
    }

    private boolean _isDeviceEnabled;
    public boolean isDeviceEnabled(){
        return _isDeviceEnabled;
    }

    private String _lastLoginError;
    public String getLastLoginError() {
        return _lastLoginError;
    }

    private List<DeviceApp> _appsToShow;
    public List<DeviceApp> getAppsToShow() {
        return _appsToShow;
    }

    private int _mainLogoId;
    protected void setMainLogoId(int _mainLogoId) {
        this._mainLogoId = _mainLogoId;
    }

    public int getMainLogoId() {
        return _mainLogoId;
    }

    private ApiParams apiParams;
    public void setApiParams(ApiParams apiParams){
        this.apiParams = apiParams;
    }
    public ApiParams getApiParams(){
        return this.apiParams;
    }

    public void startApp(final ResultListener<DeviceLoginResponse> responseListener) throws Exception{

        if(this.apiParams == null)
            throw new Exception("ApiParams null");

        if(!isServiceEnabled(SERVICE_LOGIN)){
            //la app no necesita login
            _isDeviceEnabled = true;
            _appsToShow = new ArrayList<DeviceApp>();
            //_mainLogoId = defaultMainLogo;
            initiateApp();
            responseListener.onSuccess(null);
            return;
        }

        _isDeviceEnabled = false;
        _lastLoginError = "";
        _appsToShow = new ArrayList<DeviceApp>();
        //_mainLogoId = defaultMainLogo;

        //realizamos login
        DeviceLoginTask deviceLoginTask = new DeviceLoginTask(this, apiParams, new ResultListener<DeviceLoginResponse>() {
            @Override
            public void onSuccess(DeviceLoginResponse result) {
                _isDeviceEnabled = result.isDeviceEnabled();
                _lastLoginError = "";

                if(result.isDeviceEnabled()) {
                    _appsToShow = result.getApps();
                    initiateApp();
                }
                else{
                    Toast.makeText(MainApp.this, "Device is not enabled to operate!", Toast.LENGTH_SHORT).show();
                }

                responseListener.onSuccess(result);
            }

            @Override
            public void onError(String error, boolean retryOperation) {
                _lastLoginError = error;
                responseListener.onError(error, retryOperation);
                Toast.makeText(MainApp.this, "Error during device login:" + error, Toast.LENGTH_SHORT).show();
            }
        });
        deviceLoginTask.execute();
    }

    private void initiateApp(){

        if(isServiceEnabled(SERVICE_PING)) {
            //iniciamos el servicio de PING sino está corriendo
            if (!isServiceRunning(PingService.class)) {
                startService(PingService.newPingServiceIntent(this, apiParams));
            }
        }

        if(isServiceEnabled(SERVICE_DOWNLOAD_APPS)) {
            //iniciamos el servicio de descarga de aplicaciones en background
            if (!isServiceRunning(DownloadService.class)) {
                startService(DownloadService.newDownloadServiceIntent(this, apiParams));
            }
        }

        if(isServiceEnabled(SERVICE_SEND_DATA)) {
            //enviamos información del device
            SendDeviceInfoTask sendDeviceInfoTask = new SendDeviceInfoTask(this, apiParams);
            sendDeviceInfoTask.execute();
        }
    }


    public void activityStarted(Activity activity, MainAppListener listener){

        if(isServiceEnabled(SERVICE_DOWNLOAD_APPS)) {
            //iniciamos la tarea de revisar si hay apks para instalar
            checkNewVersionsReadyToInstall(activity, listener);
        }
        else {
            listener.onAppReady();
        }
    }


    private void checkNewVersionsReadyToInstall(Activity activity, final MainAppListener listener){

        new InstallUpdatesTask(activity).checkAndInstall(new InstallUpdatesTask.InstallUpdatesListener() {
            @Override
            public void onStart() {
                //Toast.makeText(MainActivity.this, "start!", Toast.LENGTH_SHORT).show();
                Log.i(TAG, "checkNewVersionsReadyToInstall -> onStart");
            }

            @Override
            public void onUpdate(Spanned message) {
                Log.i(TAG, "checkNewVersionsReadyToInstall -> onUpdate: " + message);

                //Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFinished() {
                Log.i(TAG, "checkNewVersionsReadyToInstall -> onFinished");

                listener.onAppReady();

                //Toast.makeText(MainActivity.this, "finished!", Toast.LENGTH_SHORT).show();

                //Window.FocusViewShowSoftKeyboard(LoginActivity.this, mPasswordView);
            }
        });
    }

    public interface MainAppListener{
        void onAppReady();
    }

    public boolean isServiceRunning(Class<?> serviceClass)
    {
        Log.v(TAG, "checking if service: " + serviceClass.getName() + " is running...");

        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);

        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE))
        {
            //Log.v(TAG, "ClassName: " + service.service.getClassName());
            if (serviceClass.getName().equalsIgnoreCase(service.service.getClassName()))
            {
                //Log.v(TAG, "TRUE: " + service.service.getClassName());
                Log.v(TAG, "service " + serviceClass.getName() + ": RUNNING");
                return true;
            }
        }
        //Log.v(TAG, "FALSE");
        Log.v(TAG, "service " + serviceClass.getName() + ": NOT RUNNING");
        return false;
    }

    public abstract Intent getExitAppIntent();
    public abstract Intent getTechModeIntent();
    public abstract boolean isSingleAppAutoStartEnabled();

}
