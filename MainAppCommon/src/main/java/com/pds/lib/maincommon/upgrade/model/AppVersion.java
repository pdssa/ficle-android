package com.pds.lib.maincommon.upgrade.model;

/**
 * Created by Hernan on 30/11/2016.
 */
public class AppVersion {

    private String VersionPackage;
    private String VersionVersion;
    private String VersionApkUrl;

    public AppVersion() {
    }

    public String getVersionPackage() {
        return VersionPackage;
    }

    public void setVersionPackage(String versionPackage) {
        this.VersionPackage = versionPackage;
    }

    public String getVersionVersion() {
        return VersionVersion;
    }

    public void setVersionVersion(String versionVersion) {
        this.VersionVersion = versionVersion;
    }

    public String getVersionApkUrl() {
        return VersionApkUrl;
    }

    public void setVersionApkUrl(String versionApkUrl) {
        this.VersionApkUrl = versionApkUrl;
    }
}
