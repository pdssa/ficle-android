package com.pds.lib.maincommon.sync.model;

/**
 * Created by Hernan on 22/11/2016.
 */
public class DeviceInfo {

    public String LOCATION_LATITUDE;
    public String LOCATION_LONGITUDE;

    public String IMEI;
    public String IMSI;
    public String ANDROID_ID;
    public String WIFI_MAC;
    public String LAN_MAC;
    public SimInfo SIM_INFO;

    public String DATETIME;
    public String CONNECT_TYPE;
    public String IP_ADDRESS;
    public float BATTERY_LEVEL;
    public String BATTERY_STATUS;
    public String ANDROID_VERS;
    public String MODEL;


    public DeviceInfo(){
        DATETIME = "";
        CONNECT_TYPE = "";
        IP_ADDRESS= "";
        BATTERY_LEVEL= 0f;
        BATTERY_STATUS= "";
        ANDROID_VERS= "";
        MODEL= "";

        IMEI = "";
        IMSI = "";
        ANDROID_ID = "";
        WIFI_MAC = "";
        LAN_MAC = "";

        LOCATION_LATITUDE = "";
        LOCATION_LONGITUDE = "";

        SIM_INFO = new SimInfo();
    }
}
