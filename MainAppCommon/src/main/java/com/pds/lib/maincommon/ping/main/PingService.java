package com.pds.lib.maincommon.ping.main;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.pds.lib.maincommon.api.ApiParams;
import com.pds.lib.maincommon.ping.model.Ping;
import com.pds.lib.maincommon.ping.model.PingPerDay;
import com.pds.lib.maincommon.ping.utils.PingCommunication;
import com.pds.lib.maincommon.ping.utils.PingRequests;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by jose on 7/1/16.
 */
public class PingService extends Service {

    private static final String TAG = "PingService";

    private boolean isRunning = false;

    private Context context;

    private int tempcount;

    public static final String HOST = "__HOST";
    public static final String PORT = "__PORT";
    public static final String DEVICE_ID = "__DEVICE_ID";
    public static final String IMEI = "__IMEI";

    private String API_URL = "";

    //private PingApplication app = PingApplication.getInstance();

    private PingCommunication conn;

    public static Intent newPingServiceIntent(Context context, ApiParams apiParams) {

        Intent intentservice = new Intent(context, PingService.class);
        intentservice.putExtra(PingService.DEVICE_ID, apiParams.API_DEVICE_ID);
        intentservice.putExtra(PingService.HOST, apiParams.API_HOST);
        intentservice.putExtra(PingService.PORT, apiParams.API_PORT);
        intentservice.putExtra(PingService.IMEI, apiParams.API_IMEI);

        return intentservice;
    }

    @Override
    public void onCreate() {
        Log.i(TAG, "Service onCreate");
        isRunning = true;

        context = this;

        conn = new PingCommunication(context);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, final int startId) {

        Log.i(TAG, "Service onStartCommand");

        String host = intent.getStringExtra(HOST);
        String port = intent.getStringExtra(PORT);
        String deviceId = intent.getStringExtra(DEVICE_ID);
        String imei = intent.getStringExtra(IMEI);

        this.API_URL = new ApiParams(host, port, deviceId, imei).getApiURL(ApiParams.PING_CONTROLLER);

        new Thread(new Runnable() {

            @Override
            public void run() {

                PingController db = new PingController(context);
                PingRequests request = new PingRequests(context, API_URL);
                boolean get = false;
                long ldaterec = 0;
                Date now = null;

                boolean SendPingWithGprs = true;

                while (true) {

                    try {

                        try {

                            Thread.sleep(10000);
                            //Thread.sleep(1000);

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        Calendar c = Calendar.getInstance();

                        SimpleDateFormat date1999format = new SimpleDateFormat("yyyy");
                        String date999 = date1999format.format(c.getTime());

                        if (date999 == "1999") {
                            continue;
                        }

                        // CONNECTION

                        int[] connresponse = conn.getGprsConnected();
                        //int islooged = app.getLogged();//TODO: revisar
                        int islooged = 1;

                        boolean gprs = false;
                        String connectionType = "NOT CONNECTED";

                        int countpingperdays = db.getPingPerDayCount();

                        long millinit = System.currentTimeMillis();

                        if (countpingperdays > 0 && connresponse[0] == conn.CONNECTED) {

                            JSONObject jsonpost = null;

                            int responsecode = 0;

                            try {

                                jsonpost = request.post(db);
                                responsecode = jsonpost.getInt("ResponseCode");

                            } catch (Exception e) {
                                //e.printStackTrace();
                            }

                            if (responsecode == 0) {
                                Log.v(TAG, "STORE POST.");

                                if (jsonpost != null)
                                    Log.v(TAG, jsonpost.toString());

                                db.truncatePingPerDay();
                            }

                        }

                        int countpings = db.getPingCount();

                        Calendar calnow = Calendar.getInstance();
                        calnow.setTimeInMillis(millinit);
                        now = calnow.getTime();

                        if (countpings > 0) {

                            ////////////////////////
                            ////    EVERY 10   /////STORE PING
                            ///////////////////////

                            Ping lp = db.getPingLastInsert();
                            ldaterec = lp.getFECHA();

                            Calendar calda = Calendar.getInstance();
                            calda.setTimeInMillis(ldaterec);
                            Date last = calda.getTime();

                            long difference = now.getTime() - last.getTime();

                            //int days = (int) (difference / (1000*60*60*24));
                            //int hours = (int) ((difference - (1000*60*60*24*days)) / (1000*60*60));
                            //int min = (int) (difference - (1000*60*60*24*days) - (1000*60*60*hours)) / (1000*60);

                            int min = (int) (difference / (1000 * 60));

                            if (min >= 10)  // FRECUENCIA
                            {
                                get = true;
                            }

                        } else {
                            get = true;
                        }

                        if (connresponse[1] == conn.GPRS) {
                            gprs = true;
                            connectionType = "GPRS";

                        } else if (connresponse[1] == conn.WIFI) {
                            gprs = false;
                            connectionType = "LAN";
                        }

                        if (gprs && islooged == 0) {
                            // GPRS NOT LOGGED
                            if (!SendPingWithGprs) {
                                get = false;
                            }
                        }

                        if (get) {
                            Ping newPing = null;

                            if (connresponse[0] == conn.CONNECTED) {

                                try {
                                    JSONObject jsonget = request.get(context, connectionType);

                                    Log.v(TAG, "GET PING.");
                                    Log.v(TAG, jsonget.toString());

                                    int responsecode = jsonget.getInt("ResponseCode");

                                    SendPingWithGprs = jsonget.getBoolean("SendPingWithGprs");

                                    long millafter = System.currentTimeMillis();

                                    long elapsed = millafter - millinit;

                                    Log.v(TAG, "" + millafter);

                                    if (responsecode == 0) {
                                        responsecode = 1;
                                    } else {
                                        responsecode = 0;
                                    }

                                    newPing = new Ping(0, responsecode, millinit, elapsed, islooged);

                                } catch (Exception e) {
                                    newPing = new Ping(0, 0, millinit, 0, islooged);
                                }

                            } else {
                                newPing = new Ping(0, 0, millinit, 0, islooged);
                            }

                            get = false;

                            if (countpings > 0) {
                                /////////////////////
                                ////    DAILY   /////
                                /////////////////////

                                SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-M-dd");
                                String datenow = dateformat.format(c.getTime());

                                Date today = null;
                                Date lastday = null;

                                DateFormat formatter = new SimpleDateFormat("yyyy-M-dd");
                                Calendar calda = Calendar.getInstance();
                                calda.setTimeInMillis(ldaterec);
                                String lstday = formatter.format(calda.getTime());

                                try {

                                    today = dateformat.parse(datenow);
                                    lastday = dateformat.parse(lstday);

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }


                                // if ( tempcount >= 3 )
                                if (today.compareTo(lastday) > 0) {
                                    boolean success = true;

                                    for (Date pingDate : db.getPingDates()) {

                                        DateFormat date = new SimpleDateFormat("yyyy-MM-dd");
                                        String pingd = date.format(pingDate);

                                        int CountLoginOk = db.getPingCountLoginOkay(pingd);
                                        int CountLogoffOk = db.getPingCountLogoffOkay(pingd);
                                        int CountLogoffTotal = db.getPingCountLogoffTotal(pingd);
                                        int CountLoginTotal = db.getPingCountLoginTotal(pingd);
                                        float AverageElapsedTime = db.getPingAverageElapsed(pingd);

                                        PingPerDay pingperday = new PingPerDay();
                                        pingperday.setCOUNT_LOG_IN_OK(CountLoginOk);
                                        pingperday.setCOUNT_LOG_OFF_TOTAL(CountLogoffTotal);
                                        pingperday.setCOUNT_LOG_OFF_OK(CountLogoffOk);
                                        pingperday.setAVG_ELAPSED_TIME(AverageElapsedTime);

                                        Calendar caldate = Calendar.getInstance();
                                        caldate.setTime(pingDate);
                                        long caldatemillis = caldate.getTimeInMillis();

                                        pingperday.setFECHA(caldatemillis); //
                                        pingperday.setCOUNT_LOG_IN_TOTAL(CountLoginTotal);

                                        if (db.addPingPerDay(pingperday) == false) {
                                            success = false;
                                        }

                                    }

                                    if (success) {
                                        db.truncatePing();
                                        tempcount = 0;
                                    }

                                }

                            }

                            if (newPing != null) {
                                db.addPing(newPing);
                                Log.v(TAG, "STORE PING.");
                                tempcount++;
                            }
                        }

                        if (isRunning) {
                            Log.i(TAG, "Count: " + tempcount);
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

        }).start();

        return Service.START_STICKY;
    }


    @Override
    public IBinder onBind(Intent arg0) {
        Log.i(TAG, "Service onBind");
        return null;
    }

    @Override
    public void onDestroy() {

        isRunning = false;

        Log.i(TAG, "Service onDestroy");
    }

}