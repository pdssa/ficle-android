package com.pds.lib.maincommon.ping.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.DetailedState;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.util.Log;

import java.util.List;

/**
 * Created by jose on 7/12/16.
 */
public class PingCommunication
{
	private static String TAG = "PingCommunication";

	private Context context;

	public static final int NO_CONNECTION 	= 1000;
	public static final int CONNECTED 		= 1001;
	public static final int FAILED 			= 1002;
	public static final int SUSPENDED 		= 1003;
	public static final int UNKNOWN 		= 1004;

	public static final int GPRS 			= 1005;
	public static final int WIFI 			= 1006;

    public PingCommunication(Context context)
    {
		this.context = context;
    }

    public int[] getGprsConnected()
    {
    	////// GPRS Signal
				
		int gprs_status = -1;

		boolean gprs = false;

		int[] response = new int[2];

		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo Info = cm.getActiveNetworkInfo();
	    if (Info == null || !Info.isConnectedOrConnecting()) 
	    {
	    	
	        Log.i(TAG, "No connection");
	        
	        //gprs_status = 2;

			response[0] = NO_CONNECTION;

	    } else 
	    {
	        int netType = Info.getType();
	      

	        if (netType == ConnectivityManager.TYPE_WIFI)
			{

				//Log.i(TAG, "Wifi connection");

	            WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
	            List<ScanResult> scanResult = wifiManager.getScanResults();

				//for (int j = 0; j < scanResult.size(); j++) {
	            //    Log.d("scanResult", "Speed of wifi"+scanResult.get(j).level);//The db level of signal
	            //    Log.v("sinsenial","Speed of wifi"+scanResult.get(j).level);
	            //}
	            // Need to get wifi strength

				if (Info.getState()== NetworkInfo.State.CONNECTED)
				{
					response[0] = CONNECTED;

				} else if (Info.getState()== NetworkInfo.State.DISCONNECTED)
				{
					response[0] = NO_CONNECTION;

				}

				response[1] = WIFI;

			} else if (netType == ConnectivityManager.TYPE_MOBILE)
			{
	            
	        	Log.i(TAG, "GPRS/3G connection");
	            
	            final NetworkInfo mobile =
	            		cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
	            
	           DetailedState state = mobile.getDetailedState();
	           
	           String signal = null;
	           
	           switch (state)
	           {
	           		case CONNECTED:
	           			signal = "CONNECTED";
						response[0] = CONNECTED;
	           			//gprs_status = 1;
	           			break;
	           		case DISCONNECTED:
	           			signal = "DISCONNECTED";
						response[0] = NO_CONNECTION;
	           			//gprs_status = 2;
	           			break;
	           		case FAILED:
	           			signal = "FAILED";
						response[0] = FAILED;
	           			break;
	           		case SUSPENDED:
	           			signal = "SUSPENDED";
						response[0] = SUSPENDED;
	           			break;
	           		default:
	           			signal = "UNKNOWN";
						response[0] = UNKNOWN;
	           			break;
	           }
	           Log.v("sin señal", "GPRS: " +  signal);

	            // Need to get differentiate between 3G/GPRS
				response[1] = GPRS;
	        }

	    }
		return response;
    }
}
