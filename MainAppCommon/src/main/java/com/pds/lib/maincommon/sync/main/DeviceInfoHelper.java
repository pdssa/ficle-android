package com.pds.lib.maincommon.sync.main;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import com.pds.common.Formatos;
import com.pds.lib.maincommon.sync.model.DeviceInfo;

import org.apache.http.conn.util.InetAddressUtils;
import org.w3c.dom.Text;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by Hernan on 22/11/2016.
 */
public class DeviceInfoHelper {

    private Context _context;
    public static final String DEFAULT_VALUE = "FFFFFFFFFF";

    public DeviceInfoHelper(Context context) {
        this._context = context;
    }

    public DeviceInfo getDeviceInfo() {

        DeviceInfo deviceInfo = new DeviceInfo();

        deviceInfo.DATETIME = Formatos.FormateaDate(new Date(), Formatos.DbDateTimeFormatTimeZone);
        deviceInfo.CONNECT_TYPE = getConnectivityType();
        deviceInfo.IP_ADDRESS = getIPAddress(true);
        deviceInfo.BATTERY_LEVEL = getBatteryLevel();
        deviceInfo.BATTERY_STATUS = getBatteryStatus();
        deviceInfo.ANDROID_VERS = getAndroidVers();
        deviceInfo.MODEL = getModelName();

        TelephonyManager telephonyManager = ((TelephonyManager) _context.getSystemService(Context.TELEPHONY_SERVICE));

        //deviceInfo.IMEI = telephonyManager.getDeviceId() != null ? telephonyManager.getDeviceId() : "";
        deviceInfo.IMEI = getDeviceIMEI();

        String imsi = telephonyManager.getSubscriberId();
        deviceInfo.IMSI = imsi != null ? imsi : "";

        String simserialnumber = telephonyManager.getSimSerialNumber();
        deviceInfo.SIM_INFO.SIM_SERIALNUMBER = simserialnumber != null ? simserialnumber : "";

        //String androidId = Settings.Secure.getString(_context.getContentResolver(), Settings.Secure.ANDROID_ID);
        //deviceInfo.ANDROID_ID = androidId != null ? androidId : "";
        deviceInfo.ANDROID_ID = getDeviceId();

        Location location = getLocation();
        if (location != null) {
            deviceInfo.LOCATION_LONGITUDE = String.valueOf(location.getLongitude());
            deviceInfo.LOCATION_LATITUDE = String.valueOf(location.getLatitude());
        }

        String simOperatorCode = telephonyManager.getSimOperator(); //*OPERATOR CODE*
        deviceInfo.SIM_INFO.SIM_OPERATOR_CODE = simOperatorCode != null ? simOperatorCode : "";

        String simOperatorName = telephonyManager.getSimOperatorName(); //*OPERATOR NAME*
        deviceInfo.SIM_INFO.SIM_OPERATOR_NAME = simOperatorName != null ? simOperatorName : "";

        String simCountry = telephonyManager.getSimCountryIso(); //*COUNTRY ISO*
        deviceInfo.SIM_INFO.SIM_COUNTRY = simCountry != null ? simCountry : "";

        deviceInfo.SIM_INFO.SIM_STATE = getSIMStatus(telephonyManager);//*STATE*

        String simVoiceMail = telephonyManager.getVoiceMailAlphaTag(); //*VOICE MAIL TEXT ID*
        deviceInfo.SIM_INFO.SIM_VOICE_MAIL = simVoiceMail != null ? simVoiceMail : "";

        String simVoiceMailNro = telephonyManager.getVoiceMailNumber(); //*VOICE MAIL NUMBER*
        deviceInfo.SIM_INFO.SIM_VOICE_MAIL_NRO = simVoiceMailNro != null ? simVoiceMailNro : "";

        deviceInfo.SIM_INFO.SIM_PHONE_TYPE = getSIMPhoneType(telephonyManager);//*PHONE TYPE*

        String simNetworkCountry = telephonyManager.getNetworkCountryIso(); //*NETWORK COUNTRY ISO*
        deviceInfo.SIM_INFO.SIM_NETWORK_COUNTRY = simNetworkCountry != null ? simNetworkCountry : "";

        String simNetworkOperator = telephonyManager.getNetworkOperator(); //*NETWORK OPERATOR CODE*
        deviceInfo.SIM_INFO.SIM_NETWORK_OPERATOR = simNetworkOperator != null ? simNetworkOperator : "";

        String simNetworkOperatorName = telephonyManager.getNetworkOperatorName(); //*NETWORK OPERATOR NAME*
        deviceInfo.SIM_INFO.SIM_NETWORK_OPERATOR_NAME = simNetworkOperatorName != null ? simNetworkOperatorName : "";

        deviceInfo.SIM_INFO.SIM_NETWORK_TYPE = getSIMNetworkType(telephonyManager);

        String simLine1Nro = telephonyManager.getLine1Number(); //*PHONE NUMBER*
        deviceInfo.SIM_INFO.SIM_LINE1_NRO = simLine1Nro != null ? simLine1Nro : "";

        /*WifiInfo wifiInf = ((WifiManager) _context.getSystemService(Context.WIFI_SERVICE)).getConnectionInfo();
        String wifiMac = wifiInf.getMacAddress();
        deviceInfo.WIFI_MAC = wifiMac != null ? wifiMac : "";*/

        String wifiMac = getMACAddress("wlan0");
        deviceInfo.WIFI_MAC = wifiMac != null ? wifiMac : "";

        String lanMac = getMACAddress("eth0");
        deviceInfo.LAN_MAC = lanMac != null ? lanMac : "";

        return deviceInfo;
    }

    public String getDeviceId() {
        String androidId = Settings.Secure.getString(_context.getContentResolver(), Settings.Secure.ANDROID_ID);
        //return androidId != null ? androidId : "";
        return TextUtils.isEmpty(androidId) ? DEFAULT_VALUE : androidId;

    }

    public String getDeviceIMEI() {
        TelephonyManager telephonyManager = ((TelephonyManager) _context.getSystemService(Context.TELEPHONY_SERVICE));

        String deviceIMEI = telephonyManager.getDeviceId();

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(_context);
        String deviceIMEISaved = settings.getString("imei", DEFAULT_VALUE);

        if(!TextUtils.isEmpty(deviceIMEI) && !deviceIMEI.equals(deviceIMEISaved)){
            //si no es el mismo que el guardado, lo actualizamos para futuras referencias...
            SharedPreferences.Editor editor = settings.edit();
            editor.putString("imei", deviceIMEI);
            editor.apply();
        }

        //si pudimos leer retornamos ese, sino el guardado o el default
        if(!TextUtils.isEmpty(deviceIMEI))
            return deviceIMEI;
        else {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(_context);
            return sharedPreferences.getString("imei", DEFAULT_VALUE);
        }

    }
/*
    public String getDeviceIMEI() {
        TelephonyManager telephonyManager = ((TelephonyManager) _context.getSystemService(Context.TELEPHONY_SERVICE));

        //return telephonyManager.getDeviceId() != null ? telephonyManager.getDeviceId() : "";

        String IMEI = "";
        try {
            IMEI = telephonyManager.getDeviceId();
            Log.w("ApiParams", IMEI);
        }
        catch (Exception e){
            e.printStackTrace();
        }

        */
/**intentamos leer el IMEI hasta 3 veces, dado que cuando la app inicia rápidamente en el boot
         * el modem puede no estar incializado aún
         * *//*

        int maxReTries = 3;
        for (int i = 0; TextUtils.isEmpty(IMEI) && i < maxReTries; i++) {

            //vamos a reintentar tomar el IMEI
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            try {
                telephonyManager = ((TelephonyManager) _context.getSystemService(Context.TELEPHONY_SERVICE));
                IMEI = telephonyManager.getDeviceId();
                Log.w("ApiParams", IMEI);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }

        if(TextUtils.isEmpty(IMEI)){
            Log.w("ApiParams", "IMEI is null");
            return DEFAULT_VALUE;
        }

        return IMEI;
    }
*/

    /*public boolean isDeviceIMEIAvailable(){

    }*/

    private String getSIMNetworkType(TelephonyManager telephonyManager) {
        int simNetworkType = telephonyManager.getNetworkType(); //*NETWORK TYPE*
        switch (simNetworkType) {
            case TelephonyManager.NETWORK_TYPE_1xRTT:
                return "1XRTT";
            case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                return "UNKNOWN";
            case TelephonyManager.NETWORK_TYPE_GPRS:
                return "GPRS";
            case TelephonyManager.NETWORK_TYPE_EDGE:
                return "EDGE";
            case TelephonyManager.NETWORK_TYPE_UMTS:
                return "UMTS";
            case TelephonyManager.NETWORK_TYPE_CDMA:
                return "CMA";
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
                return "EVDO_0";
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
                return "EVDO_A";
            case TelephonyManager.NETWORK_TYPE_HSDPA:
                return "HSDPA";
            case TelephonyManager.NETWORK_TYPE_HSUPA:
                return "HSUPA";
            case TelephonyManager.NETWORK_TYPE_HSPA:
                return "HSPA";
            case TelephonyManager.NETWORK_TYPE_IDEN:
                return "IDEN";
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
                return "EVDO_B";
            case TelephonyManager.NETWORK_TYPE_LTE:
                return "LTE";
            case TelephonyManager.NETWORK_TYPE_EHRPD:
                return "EHRPD";
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                return "HSPAP";
            default:
                return "";
        }
    }

    private String getSIMStatus(TelephonyManager telephonyManager) {
        int simState = telephonyManager.getSimState(); //*STATE*
        switch (simState) {
            case TelephonyManager.SIM_STATE_READY:
                return "READY";
            case TelephonyManager.SIM_STATE_ABSENT:
                return "ABSENT";
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                return "NETWORK_LOCKED";
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                return "PIN_REQUIRED";
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                return "PUK_REQUIRED";
            case TelephonyManager.SIM_STATE_UNKNOWN:
                return "UNKNOWN";
            default:
                return "";
        }
    }

    private String getSIMPhoneType(TelephonyManager telephonyManager) {
        int simSystemService = telephonyManager.getPhoneType(); //*PHONE TYPE*
        switch (simSystemService) {
            case TelephonyManager.PHONE_TYPE_CDMA:
                return "CDMA";
            case TelephonyManager.PHONE_TYPE_GSM:
                return "GSM";
            case TelephonyManager.PHONE_TYPE_SIP:
                return "SIP";
            case TelephonyManager.PHONE_TYPE_NONE:
                return "NONE";
            default:
                return "";
        }
    }

    private float getBatteryLevel() {
        Intent batteryIntent = this._context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

        // What level does it have?
        int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        // Error checking that probably isn't needed but I added just in case.
        if (level == -1 || scale == -1) {
            return 50.0f;
        }

        return ((float) level / (float) scale) * 100.0f;
    }

    private String getBatteryStatus() {
        Intent batteryIntent = this._context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

        // Are we charging / charged?
        int status = batteryIntent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        boolean isCharging = (status == BatteryManager.BATTERY_STATUS_CHARGING ||
                status == BatteryManager.BATTERY_STATUS_FULL);

        // How are we charging?
        int chargePlug = batteryIntent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        boolean usbCharge = (chargePlug == BatteryManager.BATTERY_PLUGGED_USB);
        boolean acCharge = (chargePlug == BatteryManager.BATTERY_PLUGGED_AC);

        String _status = "";

        if (isCharging)
            _status = "C";//charging /connected
        else
            _status = "X";//not charging / disconnected

        if (usbCharge)
            _status += "U";//USB
        else if (acCharge)
            _status += "A";//AC
        else
            _status += "N";//none

        return _status;
    }

    private String getAndroidVers() {
        try {
            return Build.VERSION.RELEASE;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

    private String getModelName() {
        try {
            String manufacturer = Build.MANUFACTURER;
            String model = Build.MODEL;

            if (model.startsWith(manufacturer)) {
                return capitalize(model);
            } else
                return capitalize(manufacturer) + " " + capitalize(model);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return "";
    }

    /**
     * Returns MAC address of the given interface name.
     *
     * @param interfaceName eth0, wlan0 or NULL=use first interface
     * @return mac address or empty string
     */
    private static String getMACAddress(String interfaceName) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                if (interfaceName != null) {
                    if (!intf.getName().equalsIgnoreCase(interfaceName)) continue;
                }
                byte[] mac = intf.getHardwareAddress();
                if (mac == null) return "";
                StringBuilder buf = new StringBuilder();
                for (int idx = 0; idx < mac.length; idx++)
                    buf.append(String.format("%02X:", mac[idx]));
                if (buf.length() > 0) buf.deleteCharAt(buf.length() - 1);

                String result = buf.toString();

                return (result != null ? result : "");
            }
        } catch (Exception ex) {
        } // for now eat exceptions
        return "";
        /*try {
            // this is so Linux hack
            return loadFileAsString("/sys/class/net/" +interfaceName + "/address").toUpperCase().trim();
        } catch (IOException ex) {
            return null;
        }*/
    }

    /**
     * Get IP address from first non-localhost interface
     *
     * @param useIPv4 true=return ipv4, false=return ipv6
     * @return address or empty string
     */
    private static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress().toUpperCase();
                        boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 port suffix
                                return delim < 0 ? sAddr : sAddr.substring(0, delim);
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
        } // for now eat exceptions
        return "";
    }

    /**
     * *
     * GPS Process
     * <p/>
     * First of all call listener of Location
     * then checking for GPS_PROVIDER
     * if not available then check for NETWORK_PROVIDER
     * and if its also not available then pass 0.00,0.00 to longitude and latitude
     * <p/>
     * ***
     */
    private Location getLocation() {

        /** PROCESS for Get Longitude and Latitude **/
        LocationManager locationManager = (LocationManager) _context.getSystemService(Context.LOCATION_SERVICE);

        List<String> providers = locationManager.getProviders(true);

        /* Loop over the array backwards, and if you get an accurate location, then break out the loop*/
        Location l = null;

        for (int i = providers.size() - 1; i >= 0; i--) {
            l = locationManager.getLastKnownLocation(providers.get(i));
            if (l != null) break;
        }
        return l;
        /*
        // getting GPS status
        boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        // check if GPS enabled
        if (isGPSEnabled) {

            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (location != null) {
                return location;
            } else {
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                if (location != null) {
                    return location;
                } else {
                    return null;
                }
            }
        }
        else{
            return null;
        }*/
    }

    private String getConnectivityType() {
        ConnectivityManager cm = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();

        if (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI) {
            return "LAN";
        } else if (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_MOBILE) {
            return "GPRS";
        } else
            return "";
    }

    private String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase.append(c);
        }
        return phrase.toString();
    }
}
