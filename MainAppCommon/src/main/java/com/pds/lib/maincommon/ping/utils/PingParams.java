package com.pds.lib.maincommon.ping.utils;

import android.content.Context;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;


import com.pds.lib.maincommon.ping.main.PingController;
import com.pds.lib.maincommon.ping.model.PingPerDay;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Created by jose on 7/6/16.
 */
public class PingParams
{

    private Context context;

    String imei;
    String serialNumber;

    private static String TAG = "PingParams";

    public PingParams(Context context)
    {
        this.context = context;

        TelephonyManager telephonyManager = ((TelephonyManager) context.getSystemService(context.TELEPHONY_SERVICE));
        imei = telephonyManager.getDeviceId();
        serialNumber = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);

    }

    public JSONObject getParamsForPost(PingController db)
    {

        List<PingPerDay> ppdList = db.getAllPingPerDay();

        JSONObject days = new JSONObject();
        JSONArray statistics = new JSONArray();

        try
        {

            days.put("Imei", imei);
            days.put("SerialNumber", serialNumber);

            for (int i=0; i<ppdList.size(); i++)
            {
                PingPerDay ppd = ppdList.get(i);

                JSONObject stat = new JSONObject();
                stat.put("CountLogoffTotal", ppd.getCOUNT_LOG_OFF_TOTAL()); //CountLogoffTotal);
                stat.put("CountLogoffOk", ppd.getCOUNT_LOG_OFF_OK()); //CountLogoffOk);
                stat.put("AverageElapsedTime", ppd.getAVG_ELAPSED_TIME()); //AverageElapsedTime);

                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'00:00:00'Z'");
                Calendar calda = Calendar.getInstance();
                long millis = ppd.getFECHA();
                calda.setTimeInMillis(millis);
                String sampledate = formatter.format(calda.getTime());

                stat.put("SampleDate", sampledate); //sampledate);
                stat.put("CountLoginTotal", ppd.getCOUNT_LOG_IN_TOTAL()); //CountLoginTotal);
                stat.put("CountLoginOk", ppd.getCOUNT_LOG_IN_OK()); //CountLoginOk);
                statistics.put(stat);
            }

            days.put("PingStatistics", statistics);


        } catch (JSONException e)
        {
            e.printStackTrace();
        }

        Log.v(TAG, days.toString());

        return days;
    }   

    public String addParamsToGet(String url, String connectionType)
    {
        if(!url.endsWith("?"))
            url += "?";      

        String charset = "UTF-8";
        String s = null;

        try {

            s =         "imei="                 + URLEncoder.encode(imei,           charset);
            s +=        "&serialNumber="        + URLEncoder.encode(serialNumber,   charset);
            s +=        "&connectionType="      + URLEncoder.encode(connectionType, charset);
            s +=        "&messageToBeEchoed="   + URLEncoder.encode("ABCDEF",       charset);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        url += s;

        return url;
    }    

}
