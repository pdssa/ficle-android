package com.pds.lib.maincommon.sync.model;

/**
 * Created by Hernan on 29/11/2016.
 */
public class SimInfo {

    public String SIM_SERIALNUMBER;
    public String SIM_OPERATOR_CODE;
    public String SIM_OPERATOR_NAME;
    public String SIM_COUNTRY;
    public String SIM_STATE;
    public String SIM_VOICE_MAIL;
    public String SIM_VOICE_MAIL_NRO;
    public String SIM_PHONE_TYPE;
    public String SIM_NETWORK_COUNTRY;
    public String SIM_NETWORK_OPERATOR;
    public String SIM_NETWORK_OPERATOR_NAME;
    public String SIM_NETWORK_TYPE;
    public String SIM_LINE1_NRO;

    public SimInfo(){
        SIM_SERIALNUMBER = "";
        SIM_OPERATOR_CODE = "";
        SIM_OPERATOR_NAME = "";
        SIM_COUNTRY = "";
        SIM_STATE = "";
        SIM_VOICE_MAIL = "";
        SIM_VOICE_MAIL_NRO = "";
        SIM_PHONE_TYPE = "";
        SIM_NETWORK_COUNTRY = "";
        SIM_NETWORK_OPERATOR = "";
        SIM_NETWORK_OPERATOR_NAME = "";
        SIM_NETWORK_TYPE = "";
        SIM_LINE1_NRO = "";
    }
}
