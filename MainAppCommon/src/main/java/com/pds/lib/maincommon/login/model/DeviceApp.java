package com.pds.lib.maincommon.login.model;

import com.pds.lib.maincommon.R;

/**
 * Created by Hernan on 29/11/2016.
 */
public class DeviceApp {
    private String appPackage;
    private String appName;
    private String appLogo;

    public DeviceApp(){

    }

    public String getAppPackage() {
        return appPackage;
    }

    public void setAppPackage(String appPackage) {
        this.appPackage = appPackage;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppLogo() {
        return appLogo;
    }

    public void setAppLogo(String appLogo) {
        this.appLogo = appLogo;
    }

    public int getAppLogoId(){
        return mapAppLogoId();
    }

    private int mapAppLogoId(){
        if(appLogo.equals("app_btn_pya_logo"))
            return R.drawable.app_pya;
        else
            return R.drawable.default_app_logo;
    }

}
