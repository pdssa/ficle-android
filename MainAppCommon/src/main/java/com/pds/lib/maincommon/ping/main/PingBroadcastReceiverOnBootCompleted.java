package com.pds.lib.maincommon.ping.main;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Hernan on 22/11/2016.
 */
public class PingBroadcastReceiverOnBootCompleted extends BroadcastReceiver {

    private static final String TAG = "PingBroadRecOnBootCompl";

    @Override
    public void onReceive(Context context, Intent intent)
    {
        Log.v(TAG, "PIN STARTED ON BOOT ACTION: " + Intent.ACTION_BOOT_COMPLETED);

        if (intent.getAction().equalsIgnoreCase(Intent.ACTION_BOOT_COMPLETED))
        {
            Intent intentservice = new Intent(context, PingService.class);
            context.startService(intentservice);
        }
    }
}
