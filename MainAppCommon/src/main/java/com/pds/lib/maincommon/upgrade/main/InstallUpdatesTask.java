package com.pds.lib.maincommon.upgrade.main;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;

import com.pds.lib.maincommon.upgrade.model.AppUpgrade;
import com.pds.lib.maincommon.upgrade.utils.XmlAppsParser;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hernan on 29/11/2016.
 */
public class InstallUpdatesTask extends AsyncTask<Void, String, Boolean> {

    private List<AppUpgrade> appsToInstall = new ArrayList<AppUpgrade>();
    private InstallUpdatesListener installUpdatesListener;
    private Context context;

    public InstallUpdatesTask(Context context) {
        this.context = context;
    }

    public void checkAndInstall(InstallUpdatesListener installUpdatesListener) {

        this.installUpdatesListener = installUpdatesListener;

        try {
            SharedPreferences pref = context.getSharedPreferences("com.pds.lep", Context.MODE_PRIVATE);
            String appsToInstallXML = pref.getString("apps_to_install", "");

            if(!TextUtils.isEmpty(appsToInstallXML)){
                List<AppUpgrade> lstApps = ParseXML(appsToInstallXML);

                for(AppUpgrade app : lstApps){
                    app.set_context(context);
                    app.set_localPath(app.get_url());//tomamos el path que el servicio descarga dejó en url
                    if(app.CorrespondeActualizar()){
                        Log.i("InstallUpdatesTask", "app to update: " + app.get_packageName());

                        appsToInstall.add(app);
                    }
                }
            }


        } catch (Exception ex) {
            Log.e("InstallUpdatesTask", "checkAndInstall", ex);
        }


        if (!appsToInstall.isEmpty()) {
            execute();
        } else {
            Log.i("InstallUpdatesTask", "No apk to install");
            installUpdatesListener.onFinished();
        }
    }

    private ProgressDialog progressDialog;

    @Override
    protected void onPreExecute() {
        this.installUpdatesListener.onStart();

        Spanned message = Html.fromHtml("<center>Instalando actualizaciones<br/>Por favor no apague el equipo</center>");

        this.installUpdatesListener.onUpdate(message);

        //initialize the progress dialog and show it
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();

    }

    @Override
    protected Boolean doInBackground(Void... params) {

        try {

            //buscamos si está el EntryPoint, para instalarlo al final
            AppUpgrade mainApp = null;
            for (AppUpgrade appToInstall : appsToInstall) {

                if(appToInstall.isMainApp()){
                    mainApp = appToInstall;
                }
                else{
                    publishProgress("<center>Actualizando " + appToInstall.get_packageName() + "<br/>Por favor no apague el equipo</center>");

                    //Logger.RegistrarEvento(this.context, "i", "Install: ", appToInstall.get_fileName());

                    //instalamos
                    appToInstall.Install();

                    Log.i("InstallUpdatesTask", "apk installed: " + appToInstall.get_fileName());
                }

            }

            //instalamos el EP
            if (mainApp != null) {
                publishProgress("<center>Actualizando " + mainApp.get_packageName() + "<br/>Por favor no apague el equipo</center>");

                //Logger.RegistrarEvento(this.context, "i", "Install: ", entryPointApp.get_fileName());

                //instalamos
                mainApp.Install();

                Log.i("InstallUpdatesTask", "apk installed: " + mainApp.get_fileName());
            }

        } catch (Exception ex) {
            Log.e("InstallUpdatesTask", "checkAndInstall", ex);
        }

        return null;
    }


    @Override
    protected void onCancelled() {
        progressDialog.dismiss();

        this.installUpdatesListener.onFinished();
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        progressDialog.dismiss();

        this.installUpdatesListener.onFinished();
    }

    @Override
    protected void onProgressUpdate(String... values) {
        Spanned message = Html.fromHtml(values[0]);

        progressDialog.setMessage(message);

        this.installUpdatesListener.onUpdate(message);
    }

    public interface InstallUpdatesListener {
        void onStart();
        void onUpdate(Spanned message);
        void onFinished();
    }

    public static List<AppUpgrade> ParseXML(String xmlIn) throws Exception {
        XmlAppsParser parser = new XmlAppsParser();
        return parser.parse(new ByteArrayInputStream(xmlIn.getBytes("UTF-8")));
    }
}