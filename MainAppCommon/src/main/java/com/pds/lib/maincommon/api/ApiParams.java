package com.pds.lib.maincommon.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.pds.lib.maincommon.sync.main.DeviceInfoHelper;

/**
 * Created by Hernan on 29/11/2016.
 */
public class ApiParams {

    public static final String DEVICE_LOGIN_CONTROLLER = "DeviceLogin";
    public static final String DEVICE_INFO_CONTROLLER = "DeviceInfo";
    public static final String PING_CONTROLLER = "Ping";
    public static final String VERSIONS_CONTROLLER = "DeviceAppVersions";

    public static final int CONN_TIMEOUT = 30 * 1000;

    public String API_HOST;
    public String API_PORT;
    public String API_DEVICE_ID;
    public String API_IMEI;

    public ApiParams(Context context){

        //Obtenemos acceso a las preferencias
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        this.API_HOST = settings.getString("ip_servidor", ""); //"190.111.56.80";
        this.API_PORT = settings.getString("port_servidor", ""); //"6010";

        //Obtenemos
        DeviceInfoHelper deviceInfoHelper = new DeviceInfoHelper(context);

        //deviceInfoHelper.isDeviceIMEIAvailable()

        this.API_DEVICE_ID = deviceInfoHelper.getDeviceId();
        this.API_IMEI = deviceInfoHelper.getDeviceIMEI();

        /*this.API_HOST = "190.111.56.80";
        this.API_PORT = "6010";
        this.API_DEVICE_ID = "";
        this.API_IMEI = "";*/
    }

    public ApiParams(String API_HOST, String API_PORT, String API_DEVICE_ID, String API_IMEI) {
        this.API_HOST = API_HOST;
        this.API_PORT = API_PORT;
        this.API_DEVICE_ID = API_DEVICE_ID;
        this.API_IMEI = API_IMEI;
    }

    public String getApiURL(String controller){
        return String.format("http://%s:%s/api/%s",
                this.API_HOST,
                this.API_PORT,
                controller);
    }
}
