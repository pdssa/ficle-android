package com.pds.lib.maincommon.upgrade.main;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import com.pds.lib.maincommon.api.ApiParams;

/**
 * Created by jose on 7/1/16.
 */
public class DownloadService extends IntentService {

    private static final String TAG = "DownloadService";
    public static final String HOST = "__HOST";
    public static final String PORT = "__PORT";
    public static final String DEVICE_ID = "__DEVICE_ID";
    public static final String IMEI = "__IMEI";
    private Context context;

    public static Intent newDownloadServiceIntent(Context context, ApiParams apiParams) {

        Intent intentservice = new Intent(context, DownloadService.class);
        intentservice.putExtra(DownloadService.DEVICE_ID, apiParams.API_DEVICE_ID);
        intentservice.putExtra(DownloadService.HOST, apiParams.API_HOST);
        intentservice.putExtra(DownloadService.PORT, apiParams.API_PORT);
        intentservice.putExtra(DownloadService.IMEI, apiParams.API_IMEI);

        return intentservice;
    }

    public DownloadService() {
        super(DownloadService.class.getName());
        this.context = this;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String host = intent.getStringExtra(HOST);
        String port = intent.getStringExtra(PORT);
        String deviceId = intent.getStringExtra(DEVICE_ID);
        String imei = intent.getStringExtra(IMEI);

        ApiParams apiParams = new ApiParams(host, port, deviceId, imei);

        new CheckDownloadTask(context, apiParams).execute();
    }

}