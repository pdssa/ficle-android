package com.pds.lib.maincommon.ping.utils;

import android.content.Context;
import android.util.Log;

import com.pds.lib.maincommon.ping.main.PingController;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by jose on 7/6/16.
 */
public class PingRequests
{

    private static final String TAG = "PingRequests";

    private Context context;
    private PingParams params;
    private String apiUrl;


    public PingRequests(Context context, String apiUrl) {
        this.context = context;
        this.apiUrl = apiUrl;
        params = new PingParams(context);
    }

    public JSONObject get(Context context, String connectionType) throws Exception
    {   

        URL url;
        HttpURLConnection urlConnection = null;
        BufferedReader in = null;
        String jsonstr = null;          

        try
        {
            String urlparams = params.addParamsToGet(this.apiUrl, connectionType);
            url = new URL(urlparams);

            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.setConnectTimeout(30000);

            InputStream instr = urlConnection.getInputStream();

            InputStreamReader isw = new InputStreamReader(instr);

            in = new BufferedReader(isw);

            jsonstr = in.readLine();

        } catch (Exception e)
        {
            e.printStackTrace();
        }

        JSONTokener tokener = new JSONTokener(jsonstr);

        try
        {
            return new JSONObject(tokener);           

        } catch (JSONException e)
        {
            e.printStackTrace();
        } 

        return null;

    }


    public JSONObject post(PingController db) throws IOException {

        URL url = new URL( this.apiUrl );
        HttpURLConnection conn  = (HttpURLConnection) url.openConnection();

        conn.setConnectTimeout(30000);

        JSONObject json = params.getParamsForPost(db);

        Log.v(TAG, "json:" + json.toString());

        conn.setDoOutput( true );
        conn.setInstanceFollowRedirects( false );
        conn.setRequestMethod( "POST" );
        conn.setRequestProperty( "Content-Type", "application/json" );
        conn.setRequestProperty( "charset", "utf-8" );
        conn.setUseCaches( false );

        DataOutputStream printout;

        printout = new DataOutputStream(conn.getOutputStream ());
        printout.writeBytes(json.toString());
        printout.flush ();
        printout.close ();

        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

        StringBuilder sb = new StringBuilder();
        for (int c; (c = in.read()) >= 0;)
            sb.append((char)c);

         JSONTokener tokener = new JSONTokener(sb.toString());

        Log.v(TAG, "tokener: "+tokener);

        try
        {
            return new JSONObject(tokener);           

        } catch (JSONException e)
        {
            e.printStackTrace();
        } 

        
        return null;

    }

   

}

