package com.pds.lib.maincommon.api;

/**
 * Created by Hernan on 05/05/2016.
 */
public class ApiResponse {

    private int ResponseCode;
    private String ResponseMessage;

    public ApiResponse() {
    }

    public int getResponseCode() {
        return ResponseCode;
    }

    public void setResponseCode(int responseCode) {
        ResponseCode = responseCode;
    }

    public String getResponseMessage() {
        return ResponseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        ResponseMessage = responseMessage;
    }

    public boolean hasError(){
        return ResponseCode != 0;
    }
}
