package com.pds.lib.maincommon.ping.db;

/**
 * Created by Hernan on 22/11/2016.
 */
public class PingTable {

    public static final String TABLE_NAME          = "TBL_PING";

    // TBL_PING Columns names
    public static final String KEY_ID = "id";
    public static final String KEY_FECHA               = "fecha";
    public static final String KEY_PING_OK_NOT         = "ping_ok_not";
    public static final String KEY_ELAPSED_TIME        = "elapsed_time";
    public static final String KEY_LOGGED_IN_OR_NOT    = "logged_in";

    public static final String CREATE_PING_TABLE = "CREATE TABLE " + TABLE_NAME + "("   +
            KEY_ID                              + " INTEGER PRIMARY KEY,"       +
            KEY_PING_OK_NOT                     + " INTEGER,"                   +
            KEY_FECHA                           + " INTEGER,"                      +
            KEY_ELAPSED_TIME                    + " INTEGER,"                   +
            KEY_LOGGED_IN_OR_NOT                + " INTEGER" + ")";
}
