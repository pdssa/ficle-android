package com.pds.lib.maincommon.login.main;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.pds.common.util.ConnectivityUtils;
import com.pds.lib.maincommon.api.ApiParams;
import com.pds.lib.maincommon.api.ApiResponse;
import com.pds.lib.maincommon.api.ResultListener;
import com.pds.lib.maincommon.login.model.DeviceLoginResponse;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Hernan on 29/11/2016.
 */
public class DeviceLoginTask extends AsyncTask<Void, String, DeviceLoginResponse> {

    private Context _context;
    private static String TAG = "DeviceLoginTask";
    private String ERROR = "";

    private ApiParams apiParams;
    private ResultListener<DeviceLoginResponse> resultListener;

    public DeviceLoginTask(Context _context, ApiParams apiParams, ResultListener<DeviceLoginResponse> resultListener) {
        this.apiParams = apiParams;
        this._context = _context;
        this.resultListener = resultListener;
    }

    @Override
    protected DeviceLoginResponse doInBackground(Void... params) {
        try {
            Log.v(TAG, "Doing device login...");
            //Logger.RegistrarEvento(_context, "i", "Download: Start", "");

            if (!ConnectivityUtils.isOnline(_context)) {
                throw new Exception("No hay una conexion disponible");
            }

            DeviceLoginResponse response =  sendDeviceLogin();
            if(response == null)
                ERROR = "Exception found!";
            return response;

        } catch (IOException e) {
            ERROR = "Error IO: " + e.getMessage();
            return null;
        } catch (Exception e) {
            ERROR = "Error: " + e.getMessage();
            return null;
        }
    }

    @Override
    protected void onPostExecute(DeviceLoginResponse apiResponse) {
        if(apiResponse == null){
            Log.e(TAG, ERROR);
            this.resultListener.onError(ERROR, true);
        }
        else if(!apiResponse.hasError()){
            Log.v(TAG, "Result OK");
            this.resultListener.onSuccess(apiResponse);
        }
        else{
            this.resultListener.onError(apiResponse.getResponseMessage(), false);
        }
    }

    private DeviceLoginResponse sendDeviceLogin() {
        HttpURLConnection conn = null;
        try {

            //String jsonToPost = new Gson().toJson(obj);

            URL url = new URL(String.format("%s?deviceId=%s&IMEI=%s", this.apiParams.getApiURL(ApiParams.DEVICE_LOGIN_CONTROLLER), apiParams.API_DEVICE_ID, apiParams.API_IMEI));
            conn = (HttpURLConnection) url.openConnection();

            conn.setConnectTimeout(ApiParams.CONN_TIMEOUT);

            //Log.v(TAG, "json:" + jsonToPost);

            //conn.setDoOutput(true);
            //conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");
            //conn.setUseCaches(false);

            /*DataOutputStream printout = new DataOutputStream(conn.getOutputStream());
            printout.writeBytes(jsonToPost);
            printout.flush();
            printout.close();*/

            Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

            StringBuilder sb = new StringBuilder();
            for (int c; (c = in.read()) >= 0; )
                sb.append((char) c);

            Log.v(TAG, "response:" + sb.toString());

            return new Gson().fromJson(sb.toString(), DeviceLoginResponse.class);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(conn != null)
                conn.disconnect();
        }

        return null;
    }
}
