package com.pds.lib.maincommon.ping.model;

/**
 * Created by jose on 7/1/16.
 */
public class Ping
{
	private int ID;
	private int PING_OK_NOT;
	private long FECHA;
	private long ELAPSED_TIME;
	private int LOGGED_IN_OR_NOT;

	public Ping()
	{

	}

	public Ping(int ID, int PING_OK_NOT, long FECHA, long ELAPSED_TIME, int LOGGED_IN_OR_NOT)
	{
		this.ID = ID;
		this.PING_OK_NOT = PING_OK_NOT;
		this.FECHA = FECHA;
		this.ELAPSED_TIME = ELAPSED_TIME;
		this.LOGGED_IN_OR_NOT = LOGGED_IN_OR_NOT;
	}

	public int getID() {
		return ID;
	}

	public int getPING_OK_NOT() {
		return PING_OK_NOT;
	}

	public long getFECHA() {
		return FECHA;
	}

	public long getELAPSED_TIME() {
		return ELAPSED_TIME;
	}

	public int getLOGGED_IN_OR_NOT() {
		return LOGGED_IN_OR_NOT;
	}

	public void setID(int ID) {
		this.ID = ID;
	}

	public void setPING_OK_NOT(int PING_OK_NOT) {
		this.PING_OK_NOT = PING_OK_NOT;
	}

	public void setFECHA(long FECHA) {
		this.FECHA = FECHA;
	}

	public void setELAPSED_TIME(long ELAPSED_TIME) {
		this.ELAPSED_TIME = ELAPSED_TIME;
	}

	public void setLOGGED_IN_OR_NOT(int LOGGED_IN_OR_NOT) {
		this.LOGGED_IN_OR_NOT = LOGGED_IN_OR_NOT;
	}
}

