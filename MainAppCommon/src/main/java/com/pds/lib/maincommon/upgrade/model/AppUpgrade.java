package com.pds.lib.maincommon.upgrade.model;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Hernan on 10/06/2014.
 */
public class AppUpgrade {

    private String _packageName;
    private String _packageInstalledVersion;
    private String _packageLastVersion;
    private String _url;
    private String _fileName;
    private String _localPath;
    private Context _context;

    public AppUpgrade() {

    }

    public AppUpgrade(AppVersion appVersion){
        set_packageName(appVersion.getVersionPackage());
        set_packageLastVersion(appVersion.getVersionVersion());
        set_url(appVersion.getVersionApkUrl());
    }

    public AppUpgrade(String _packageName, String _fileName, String _localPath, Context _context) {
        this._packageName = _packageName;
        this._fileName = _fileName;
        this._localPath = _localPath;
        this._context = _context;
    }

    public boolean isMainApp() {
        return
                get_packageName().equalsIgnoreCase("com.pds.lep") ||
                        _fileName.contains("EpLight");
    }

    public void set_url(String _url) {
        this._url = _url;
        //obtenemos el file name como el ultimo componente del url
        String[] separadores = _url.split("/");
        this._fileName = separadores[separadores.length - 1];
    }

    public void set_fileName(String _fileName) {
        this._fileName = _fileName;
    }

    public void set_context(Context _context) {
        this._context = _context;
    }

    public void set_packageName(String _packageName) {
        this._packageName = _packageName;
    }

    public void set_packageLastVersion(String _packageLastVersion) {
        this._packageLastVersion = _packageLastVersion;
    }

    public String get_packageName() {
        return _packageName;
    }

    public String get_packageInstalledVersion() {
        return _packageInstalledVersion;
    }

    public String get_packageLastVersion() {
        return _packageLastVersion;
    }

    public String get_url() {
        return _url;
    }

    public String get_fileName() {
        return _fileName;
    }

    public String get_localPath() {
        return _localPath;
    }

    public void set_localPath(String _localPath) {
        this._localPath = _localPath;
    }

    private void GetInstalledVersion() throws Exception {
        try {
            /* Get current Version Name */
            this._packageInstalledVersion = _context.getPackageManager().getPackageInfo(_packageName, 0).versionName;
        } catch (NameNotFoundException e) {
            this._packageInstalledVersion = "0"; //esto lo hacemos para poder instalar packages por primera vez
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean CorrespondeActualizar() {
        try {
            this.GetInstalledVersion();
        } catch (Exception ex) {
            return false;
        }
        //retorna true si son diferentes (entonces corresponde actualizar), si son iguales false
        //convertimos la version en int quitando el punto, ejemplo: 1.23 = > 123
        //y luego comparamos como int para solo actualizar por mayor
        //return !_packageInstalledVersion.equals(_packageLastVersion);

        //pueden pasar dos situaciones, que sean packages nuestros (sabemos que manejamos en versionName = "1.27" por ejemplo
        //o pueden ser third party packages, normalmente usan como descripcion dicho campo
        if (_packageName.startsWith("com.pds.")) {
            //OWN PACKAGE
            int installedVersion = Integer.parseInt(this._packageInstalledVersion.replace(".", ""));
            int lastVersion = Integer.parseInt(_packageLastVersion.replace(".", ""));

            return lastVersion > installedVersion;
        } else {
            //THIRD PARTY PACKAGE
            //solo comparamos si coinciden en el versionName => actualizamos si no coinciden
            return !this._packageInstalledVersion.equals(_packageLastVersion);
        }
    }

    public void Download(String folder, boolean forcedDownload) throws Exception {
        try {

            if (!isSdPresent()) {
                throw new Exception("Tarjeta SD no disponible");
            }

            URL url = new URL(this._url);
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(false);
            c.connect();


            String path = Environment.getExternalStorageDirectory() + folder;
            File downloadFolder = new File(path); // PATH = /mnt/sdcard/download/pds
            if (!downloadFolder.exists()) {//creamos el dir si no existe
                downloadFolder.mkdirs();
            }
            File apkFile = new File(downloadFolder, this._fileName);

            boolean hasToDownload = forcedDownload;

            if(!forcedDownload && !apkFile.exists()) {
                hasToDownload = true;
            }

            if (hasToDownload) {//no descargamos si ya existe

                Log.v("CheckDownloadTask", "descargando " + this._url);

                String tempfile = this._fileName + ".temp";
                File outputFile = new File(downloadFolder, tempfile);
                FileOutputStream fos = new FileOutputStream(outputFile);

                InputStream is = c.getInputStream(); // Get from Server and Catch In Input Stream Object.

                byte[] buffer = new byte[1024];
                int len1 = 0;
                while ((len1 = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);// Write In FileOutputStream.
                }
                fos.close();
                is.close();//en este punto el .apk ya deberia estar descargado en el storage

                //rename to apk
                if (outputFile.exists()) {
                    outputFile.renameTo(apkFile);
                    this._localPath = path + this._fileName;
                }
            }
            else{
                Log.v("CheckDownloadTask", "app ya descargada " + this._url);

                this._localPath = path + this._fileName;
            }

        } catch (Exception e) {
            throw e;
        }
    }

    public void DeleteFileIfExists(String folder) throws Exception {
        try {

            if (!isSdPresent()) {
                throw new Exception("Tarjeta SD no disponible");
            }

            String path = Environment.getExternalStorageDirectory() + folder;
            File downloadFolder = new File(path); // PATH = /mnt/sdcard/download/pds
            if (downloadFolder.exists()) {//entramos si existe
                File apkFile = new File(downloadFolder, this._fileName);
                if (apkFile.exists()) {//borramos si ya existe
                    apkFile.delete();
                }
            }

        } catch (Exception e) {
            throw e;
        }
    }

    public void Install() throws Exception {
        try {

            if(!new File(this._localPath).exists()){
                return;//si el file no existe, salimos
            }

            if (this.isMainApp()) {
                InstallByIntent();//instalamos de esta forma, para evitar conflictos con la APP iniciada
            } else {
                //primero intentamos instalar de modo SILENT
                try {

                    int result = InstallSilent();

                    //Log.d("INSTALL", String.valueOf(result));

                    if (result != 0) //instalacion erronea
                        throw new Exception("Failure SilentInstall. Exit code: " + String.valueOf(result));

                } catch (Exception e) {
                    e.printStackTrace();
                    //sino podemos instalamos vía INTENT
                    InstallByIntent();
                }
            }
        } catch (Exception e) {
            throw e;
        }

    }

    private int InstallSilent() throws Exception {
        try {
            String filename = this._localPath;
            String command = "pm install -r " + filename;
            Process proc = Runtime.getRuntime().exec(new String[]{"su", "-c", command});
            return proc.waitFor();

        } catch (Exception e) {
            throw e;
        }

    }

    private void InstallByIntent() throws Exception {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(new File(this._localPath)), "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this._context.startActivity(intent);
        } catch (Exception e) {
            throw e;
        }

    }

    private static boolean isSdPresent() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }

    /*public boolean needToBeRegistered() {
        return isEntryPoint();
    }

    public boolean registerBeforeInstall() {
        try {
            SharedPreferences pref = _context.getSharedPreferences("com.pds.ficle.ep", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();

            editor.putString("lastEpInstalled", this._fileName);

            editor.commit();

            return true;
        } catch (Exception ex) {

            return false;
        }
    }

    public boolean wasInstalledBefore() {
        SharedPreferences pref = _context.getSharedPreferences("com.pds.ficle.ep", Context.MODE_PRIVATE);

        String lastEpInstalled = pref.getString("lastEpInstalled", "");

        return lastEpInstalled.equalsIgnoreCase(this._fileName);
    }*/
}
