package com.pds.lib.maincommon.ping.model;

/**
 * Created by jose on 7/1/16.
 */
public class PingPerDay
{
	private int ID;
	private int COUNT_LOG_OFF_TOTAL;
	private int COUNT_LOG_OFF_OK;
	private float AVG_ELAPSED_TIME;
	private long FECHA;
	private int COUNT_LOG_IN_TOTAL;
	private int COUNT_LOG_IN_OK;

	public PingPerDay(int ID, int COUNT_LOG_IN_OK, int COUNT_LOG_OFF_TOTAL, int COUNT_LOG_OFF_OK, float AVG_ELAPSED_TIME, long FECHA, int COUNT_LOG_IN_TOTAL) {
		this.ID = ID;
		this.COUNT_LOG_IN_OK = COUNT_LOG_IN_OK;
		this.COUNT_LOG_OFF_TOTAL = COUNT_LOG_OFF_TOTAL;
		this.COUNT_LOG_OFF_OK = COUNT_LOG_OFF_OK;
		this.FECHA = FECHA;
		this.AVG_ELAPSED_TIME = AVG_ELAPSED_TIME;
		this.COUNT_LOG_IN_TOTAL = COUNT_LOG_IN_TOTAL;
	}

	public PingPerDay() {
	}

	public int getID() {
		return ID;
	}

	public int getCOUNT_LOG_OFF_TOTAL() {
		return COUNT_LOG_OFF_TOTAL;
	}

	public int getCOUNT_LOG_OFF_OK() {
		return COUNT_LOG_OFF_OK;
	}

	public float getAVG_ELAPSED_TIME() {
		return AVG_ELAPSED_TIME;
	}

	public long getFECHA() {
		return FECHA;
	}

	public int getCOUNT_LOG_IN_TOTAL() {
		return COUNT_LOG_IN_TOTAL;
	}

	public int getCOUNT_LOG_IN_OK() {
		return COUNT_LOG_IN_OK;
	}

	public void setID(int ID) {
		this.ID = ID;
	}

	public void setCOUNT_LOG_OFF_TOTAL(int COUNT_LOG_OFF_TOTAL) {
		this.COUNT_LOG_OFF_TOTAL = COUNT_LOG_OFF_TOTAL;
	}

	public void setCOUNT_LOG_OFF_OK(int COUNT_LOG_OFF_OK) {
		this.COUNT_LOG_OFF_OK = COUNT_LOG_OFF_OK;
	}

	public void setAVG_ELAPSED_TIME(float AVG_ELAPSED_TIME) {
		this.AVG_ELAPSED_TIME = AVG_ELAPSED_TIME;
	}

	public void setFECHA(long FECHA) {
		this.FECHA = FECHA;
	}

	public void setCOUNT_LOG_IN_TOTAL(int COUNT_LOG_IN_TOTAL) {
		this.COUNT_LOG_IN_TOTAL = COUNT_LOG_IN_TOTAL;
	}

	public void setCOUNT_LOG_IN_OK(int COUNT_LOG_IN_OK) {
		this.COUNT_LOG_IN_OK = COUNT_LOG_IN_OK;
	}
}

