package com.pds.lib.maincommon.upgrade.model;

import com.pds.lib.maincommon.api.ApiResponse;

import java.util.List;

/**
 * Created by Hernan on 30/11/2016.
 */
public class AppVersionResponse extends ApiResponse{
    private List<AppVersion> versiones;

    public AppVersionResponse() {
    }

    public List<AppVersion> getVersiones() {
        return versiones;
    }

    public void setVersiones(List<AppVersion> versiones) {
        this.versiones = versiones;
    }
}
