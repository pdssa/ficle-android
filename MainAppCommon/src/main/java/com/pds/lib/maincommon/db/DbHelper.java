package com.pds.lib.maincommon.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.pds.lib.maincommon.ping.db.PingPerDayTable;
import com.pds.lib.maincommon.ping.db.PingTable;

/**
 * Created by Hernan on 22/11/2016.
 */
public class DbHelper extends SQLiteOpenHelper {
    private static final String TAG = "DbHelper";

    private static final String DATABASE_NAME = "lep_data.db";

    //TODO: siempre agregar rutina en onUpgrade para realizar los updates
    //TODO: siempre agregar TABLAS nuevas en onCreate
    private static final int DATABASE_VERSION = 1;

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //-------------TABLAS Y VALORES POR DEFECTO-------------
        db.execSQL(PingTable.CREATE_PING_TABLE);
        db.execSQL(PingPerDayTable.CREATE_PING_PER_DAY_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG, String.format("Upgrade DB from %d to %d", oldVersion, newVersion));
    }
}
