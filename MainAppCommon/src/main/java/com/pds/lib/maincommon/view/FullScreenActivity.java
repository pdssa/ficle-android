package com.pds.lib.maincommon.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.lib.maincommon.MainApp;
import com.pds.lib.maincommon.api.ApiParams;
import com.pds.lib.maincommon.R;
import com.pds.lib.maincommon.util.PassGenerator;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;

/**
 * Created by Hernan on 04/12/2016.
 */
public class FullScreenActivity extends Activity {

    private int fullScreenView = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LOW_PROFILE | View.SYSTEM_UI_FLAG_FULLSCREEN
            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE); // to hide android status bar

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        getWindow().addFlags(0x80000000); // for capture home key
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON); //dont close window

        /*getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);

        getWindow().getDecorView().setSystemUiVisibility(fullScreenView);

        getWindow().addFlags(0x80000000); // for capture home key
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON); //dont close window*/

        /*android.view.WindowManager.LayoutParams layoutparams = getWindow().getAttributes();
        layoutparams.flags = 0x80000000 | layoutparams.flags;
        getWindow().setAttributes(layoutparams);*/

        /*final View decorView = getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {

            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                    int fullScreenView = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
                    fullScreenView |= View.SYSTEM_UI_FLAG_LOW_PROFILE | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;


                    getWindow().getDecorView().setSystemUiVisibility(fullScreenView);
                }
            }
        });*/

        //********* WINDOW FLAGS ****************
        /*
        getWindow().getDecorView().setSystemUiVisibility(8);

        requestWindowFeature(Window.FEATURE_NO_TITLE); // to hide android status bar

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        this.getWindow().addFlags(0x80000000); // for take home key

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON); //dont close window
        */
        //********* *********** ****************
    }

    @Override
    protected void onResume() {
        super.onResume();


        /*getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {

            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                    uiHandler.sendEmptyMessage(REMOVE_NAVIGATION_BAR);
                    uiHandler.sendEmptyMessageDelayed(REMOVE_NAVIGATION_BAR, 3000);
                }
            }
        });*/
    }

    /*private boolean powerButtonWasPressed = false;
    private boolean powerDialogOption = false;*/
    private int lostFocusOptions = 0;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_HOME: {// Android home key
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    //TODO: custom action...
                }
                return true;
            }
            /*case KeyEvent.KEYCODE_POWER: {
                Log.d("onKeyDown", "POWER");
                //event.startTracking(); // Needed to track long presses
                powerButtonWasPressed = true;
                return true;
            }*/
            default:
                break;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_POWER: {
                Log.d("onKeyUp", "POWER");
                //powerButtonWasPressed = true;
                lostFocusOptions = 2;
                return true;
            }
            default:
                break;
        }
        return super.onKeyUp(keyCode, event);
    }

    /*@Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_POWER) {
            Log.d("onKeyLongPress", "POWER");
            return true;
        }
        return super.onKeyLongPress(keyCode, event);
    }*/

    private static final String RECENTS_ACTIVITY = "com.android.systemui.recent.RecentsActivity";
    private static final String SYSTEM_UI_PACKAGE_NAME = "com.android.systemui";

    private void closeRecents() {
        sendBroadcast(new Intent("com.android.systemui.recent.action.CLOSE"));
        Intent closeRecents = new Intent("com.android.systemui.recent.action.TOGGLE_RECENTS");
        closeRecents.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        ComponentName recents = new ComponentName(SYSTEM_UI_PACKAGE_NAME, RECENTS_ACTIVITY);
        closeRecents.setComponent(recents);
        startActivity(closeRecents);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (getTopActivity().getClassName().equalsIgnoreCase(RECENTS_ACTIVITY)) {
            closeRecents();
        } else {
            validateAgainWithDelay(1000);
        }
    }

    private ComponentName getTopActivity() {
        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo = activityManager.getRunningTasks(1);
        ActivityManager.RunningTaskInfo runningTaskInfo = taskInfo.get(0);
        ComponentName topActivity = runningTaskInfo.topActivity;
        return topActivity;
    }

    private void validateAgainWithDelay(long delay) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (getTopActivity().getClassName().equalsIgnoreCase(RECENTS_ACTIVITY)) {
                    closeRecents();
                }
            }
        }, delay);
    }

    private static long first_back_pressed_time = 0;
    private static int back_pressed_times = 0;
    private final static int EXIT_TIMES = 5;
    private final static long EXIT_TIME = 2000; //2 segundos
    private final static String EXIT_PWD = "1111";


    @Override
    public void onBackPressed() {
        //RUTINA PARA SALIR DEL MODO KIOSCO
        long now = System.currentTimeMillis();

        if (now - first_back_pressed_time > EXIT_TIME) {
            first_back_pressed_time = now; //reiniciamos la secuencia
            back_pressed_times = 1;
        } else {
            back_pressed_times++; //continuamos la secuencia
            if (back_pressed_times == EXIT_TIMES)
                SolicitarClaveSalida(); //finalizamos la secuencia
        }
    }

    private void SolicitarClaveSalida() {

        final String API_IMEI = MainApp.getInstance().getApiParams().API_IMEI;
        final String API_DEVICE_ID = MainApp.getInstance().getApiParams().API_DEVICE_ID;

        //vamos a solicitar la clave de tecnico para ingresar a la edicion de la configuracion
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Add action buttons
        builder
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        String passIngresada = ((EditText) ((AlertDialog) dialog).findViewById(R.id.dialog_clave_config_txt_pass)).getText().toString();

                        checkClaveSalida(passIngresada, API_IMEI, API_DEVICE_ID);
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

        final AlertDialog dialog = builder.create();

        // Get the layout inflater
        LayoutInflater inflater = getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View vw = inflater.inflate(R.layout.dialog_clave_config, null);

        ((TextView) vw.findViewById(R.id.dialog_clave_config_txt_id)).setText(API_DEVICE_ID);
        ((TextView) vw.findViewById(R.id.dialog_clave_config_txt_imei)).setText(API_IMEI);
        ((EditText) vw.findViewById(R.id.dialog_clave_config_txt_pass)).setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    checkClaveSalida(v.getText().toString(), API_IMEI, API_DEVICE_ID);
                    dialog.dismiss();
                    return true;
                }
                return false;
            }
        });
        ((EditText) vw.findViewById(R.id.dialog_clave_config_txt_pass)).setHint(R.string.hint_ing_clave_salida);

        dialog.setView(vw);
        dialog.setTitle("Ingrese Clave para salir");
        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
            }
        });

        dialog.show();

        //---------------------------------------------------------------
    }

    private void checkClaveSalida(String passIngresada, String IMEI, String ID) {
        try {

            if (passIngresada.equals(EXIT_PWD)) {
                //salimos
                finalizar();
            } else if (CheckConfigPassword(passIngresada, IMEI, ID)) {
                //vemos si es la clave tecnica
                //mostramos un cuadro de opciones de acceso tecnico


                Intent intent = MainApp.getInstance().getTechModeIntent();

                if (intent != null) {
                    Toast.makeText(this, "Acceso técnico !", Toast.LENGTH_SHORT).show();
                    startActivity(MainApp.getInstance().getTechModeIntent());
                } else
                    Log.w("FullScreenActivity", "TechModeIntent is null");

            } else {
                Toast.makeText(this, "Clave ingresada erronea", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private boolean CheckConfigPassword(String passIngresada, String IMEI, String ID) {
        try {
            return passIngresada.equals("123456");

            /*String claveTecnico = PassGenerator.GenerarClaveTecnico(new Date(), IMEI, ID);
            return passIngresada.equals(claveTecnico);*/
        } catch (Exception e) {
            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }

        return false;
    }

    private void finalizar() {
        try {

            Intent exitIntent = MainApp.getInstance().getExitAppIntent();

            finish();

            if (exitIntent != null)
                startActivity(exitIntent);
            else
                Log.w("FullScreenActivity", "ExitIntent is null");

        } catch (Exception ex) {
            Toast.makeText(this, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    /*
    protected Intent getExitAppIntent(){
        //implement in activity's exit
        return null;
    }*/

    private final Handler uiHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == REMOVE_STATUS_BAR) {
                Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
                sendBroadcast(closeDialog);
                collapseStatusBar();
                return true;
            }
            else if(msg.what == REMOVE_FOCUS){
                lostFocusOptions = 0;
                return true;
            }
            /*else if(msg.what == REMOVE_NAVIGATION_BAR){
                getWindow().getDecorView().setSystemUiVisibility(fullScreenView);
                return true;
            }*/
            return false;
        }
    });

    private int REMOVE_FOCUS = 9770;
    //private int REMOVE_NAVIGATION_BAR = 9880;
    private int REMOVE_STATUS_BAR = 9990;


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        //Log.d("onWindowFocusChanged", String.valueOf(hasFocus) + " " + String.valueOf(powerButtonWasPressed) + " " + String.valueOf(powerDialogOption));
        Log.d("onWindowFocusChanged", String.valueOf(hasFocus) + " " + String.valueOf(lostFocusOptions));
        super.onWindowFocusChanged(hasFocus);
        if (!hasFocus){

            if(lostFocusOptions > 0) {
                lostFocusOptions --;
                return;
            }

            /*if(powerButtonWasPressed) {
                powerButtonWasPressed = false;
                powerDialogOption = true;
                return;
            }

            if(powerDialogOption){
                powerDialogOption = false;
                return;
            }*/

            uiHandler.sendEmptyMessage(REMOVE_STATUS_BAR);
            uiHandler.sendEmptyMessageDelayed(REMOVE_STATUS_BAR, 100);
            uiHandler.sendEmptyMessageDelayed(REMOVE_STATUS_BAR, 500);
            uiHandler.sendEmptyMessageDelayed(REMOVE_STATUS_BAR, 1000);
            uiHandler.sendEmptyMessageDelayed(REMOVE_STATUS_BAR, 2000);
        }
        else{
            uiHandler.sendEmptyMessageDelayed(REMOVE_FOCUS, 500);
        }
        /*else{
            powerButtonWasPressed = false;
            powerDialogOption = false;
        }*/
    }

    private void collapseStatusBar() {
        try {
            Object service = getSystemService("statusbar");
            Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
            Method collapse = statusbarManager.getMethod(hasCollapseMethod(statusbarManager) ? "collapse" : "collapsePanels");
            collapse.setAccessible(true);
            collapse.invoke(service);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean hasCollapseMethod(Class<?> statusbarManager) {
        for (Method method : statusbarManager.getMethods()) {
            if (method.getName().equals("collapse")) {
                return true;
            }
        }
        return false;

    }



    /*
    @SuppressLint("NewApi")
    @Override
    public void onWindowFocusChanged(boolean hasFocus)
    {
        super.onWindowFocusChanged(hasFocus);
        if(hasFocus)
        {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            );
        }
    }*/

}
