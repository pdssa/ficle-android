package com.pds.lib.maincommon.login.model;

import com.pds.lib.maincommon.api.ApiResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hernan on 29/11/2016.
 */
public class DeviceLoginResponse extends ApiResponse {
    private boolean deviceEnabled;
    private boolean kioskMode;
    private int mainLogoId;
    private List<DeviceApp> apps;

    public DeviceLoginResponse(){
        this.apps = new ArrayList<DeviceApp>();
    }

    public boolean isDeviceEnabled() {
        return deviceEnabled;
    }

    public void setDeviceStatus(boolean deviceStatus) {
        this.deviceEnabled = deviceStatus;
    }

    public boolean isKioskMode() {
        return kioskMode;
    }

    public void setKioskMode(boolean kioskMode) {
        this.kioskMode = kioskMode;
    }

    public int getMainLogoId() {
        return mainLogoId;
    }

    public void setMainLogoId(int mainLogoId) {
        this.mainLogoId = mainLogoId;
    }

    public List<DeviceApp> getApps() {
        return apps;
    }

    public void setApps(List<DeviceApp> apps) {
        this.apps = apps;
    }
}
