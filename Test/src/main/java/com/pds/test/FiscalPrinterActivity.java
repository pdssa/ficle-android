package com.pds.test;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import IFDrivers.EpsonTicket;

/**
 * Created by Hernan on 26/06/2014.
 */
public class FiscalPrinterActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.printer);
    }

    public void onCierreZ(View v)
    {
        int nError = 0;

        EpsonTicket IPrinter = new EpsonTicket();

        nError = IPrinter.IF_OPEN("/dev/ttyS0", 9600);

        if ( 0 != nError)
        {
            return;
        }

        nError= IPrinter.IF_WRITE("@CIERREZ|P|");

        nError= IPrinter.CIERREZ();

        IPrinter.IF_CLOSE();

        return;
    }

    public void onTicket(View v)
    {
        int nError = 0;

        EpsonTicket IPrinter = new EpsonTicket();

        nError = IPrinter.IF_OPEN("/dev/ttyS0", 9600);

        if ( 0 != nError)
        {
            return;
        }

        nError= IPrinter.IF_WRITE("@TIQUEABRE|C|");

        nError= IPrinter.TIQUEITEM("ABRAZADERAS-ACER.IN", 1.00, 3.57, 0.21, "M", 1, 0.0, 0.0);

        nError= IPrinter.IF_WRITE("@TIQUEITEM|ABRAZADERAS  ACER.IN|    1.000|      3.57|0.2100|M|1|0|0|");
        nError= IPrinter.IF_WRITE("@TIQUEITEM|BARDAHL ADIT. INYECT|    1.000|      4.84|0.2100|M|1|0|0|");
        nError= IPrinter.IF_WRITE("@TIQUEITEM|CABALLA AL NATURAL C|    1.000|      3.03|0.2100|M|1|0|0|");
        nError= IPrinter.IF_WRITE("@TIQUEITEM|GALL.FLIARES C/SALVA|    1.000|      2.66|0.2100|M|1|0|0|");
        nError= IPrinter.IF_WRITE("@TIQUEITEM|REFRIG. FLUIDO LAVAC|    1.000|      9.68|0.2100|M|1|0|0|");
        nError= IPrinter.IF_WRITE("@TIQUEITEM|NAIPES CASINO x 40  |    1.000|      3.39|0.2100|M|1|0|0|");
        nError= IPrinter.IF_WRITE("@TIQUEITEM|PALITO BOMBON       |    1.000|      1.82|0.2100|M|1|0|0|");
        nError= IPrinter.IF_WRITE("@TIQUEITEM|ELAION DIESEL 15W40 |    1.000|     10.29|0.2100|M|1|0|0|");
        nError= IPrinter.IF_WRITE("@TIQUEITEM|TABLETAS FUYI       |    1.000|      1.21|0.2100|M|1|0|0|");
        nError= IPrinter.IF_WRITE("@TIQUEITEM|D1  15W40 x 5 LT    |    1.000|     27.83|0.2100|M|1|0|0|");

        nError= IPrinter.IF_WRITE("@TIQUESUBTOTAL|P|Subtotal");

        nError= IPrinter.IF_WRITE("@TIQUECIERRA|T|");

        IPrinter.IF_CLOSE();

        return;
    }

    public void onFactura(View v)
    {
        int nError = 0;

        EpsonTicket IPrinter = new EpsonTicket();

        nError = IPrinter.IF_OPEN("/dev/ttyS0", 9600);

        if ( 0 != nError)
        {
            return;
        }

        nError = IPrinter.IF_WRITE("@PONEENCABEZADO|5|EJEMPLO FACTURA A");

        nError = IPrinter.IF_WRITE("@FACTABRE|T|C|A|1|P|10|I|I|SUDAMERICANA LIBROS SRL||CUIT|30692137449|N|MEXICO 564|Capital Federal||Remito 01-2345||C");

        nError = IPrinter.IF_WRITE("@FACTITEM|Mouse Genius XScrol|1.0|4.08|0.1050|M|1|0||||0|0");
        nError = IPrinter.IF_WRITE("@FACTITEM|Patchcord Cat.5E Gr|5.0|4.10|0.2100|M|1|0||||0|0");
        nError = IPrinter.IF_WRITE("@FACTITEM|Microfono NG-H300 N|1.0|4.12|0.2100|M|1|0||||0|0");
        nError = IPrinter.IF_WRITE("@FACTITEM|Mouse Genius Netscr|1.0|4.12|0.1050|M|1|0||||0|0");
        nError = IPrinter.IF_WRITE("@FACTITEM|Ventilador Cyber Co|2.0|4.12|0.2100|M|1|0||||0|0");
        nError = IPrinter.IF_WRITE("@FACTITEM|Lector 3.5 MultiCar|2.0|4.22|0.2100|M|1|0||||0|0");
        nError = IPrinter.IF_WRITE("@FACTITEM|Teclado Noganet Esp|2.0|4.30|0.1050|M|1|0||||0|0");
        nError = IPrinter.IF_WRITE("@FACTITEM|Antena SMA Kozumi W|2.0|4.33|0.2100|M|1|0||||0|0");
        nError = IPrinter.IF_WRITE("@FACTITEM|Teclado Ecovision W|1.0|4.39|0.1050|M|1|0||||0|0");
        nError = IPrinter.IF_WRITE("@FACTITEM|Limpiador para Pant|1.0|4.44|0.2100|M|1|0||||0|0");
        nError = IPrinter.IF_WRITE("@FACTITEM|Auricular Genius Mo|1.0|4.46|0.2100|M|1|0||||0|0");

        nError = IPrinter.IF_WRITE("@FACTPAGO|DESCUENTO 10%|9.46|D");
        nError = IPrinter.IF_WRITE("@FACTPAGO|PAGO|100.00|T");

        nError = IPrinter.IF_WRITE("@FACTCIERRA|T|A|FINAL");

        IPrinter.IF_CLOSE();

        return;
    }

}
