package com.pds.test;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.content.pm.IPackageInstallObserver;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.RemoteException;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import junit.framework.Test;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;

import android_serialport_api.CardReader;
import android_serialport_api.CardReader.OnReadSerialPortDataListener;
import android_serialport_api.CardReader.SerialPortData;
import android_serialport_api.Printer;
import android_serialport_api.Printer.ALINEACION;

/**
 * Created by Hernan on 19/06/2014.
 */
public class TestActivity extends Activity {

    private Printer _printer;
    private CardReader _cardReader;
    TextView _output;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {

            super.onCreate(savedInstanceState);
            setContentView(R.layout.test_layout);

            _output = (TextView) findViewById(R.id.textView);
            _output.setText("");


            if (!isSdPresent()) {
                throw new Exception("Tarjeta SD no disponible");
            }

            new ExecuteRequestTask().execute();


            /*
            try {
                //************* Get Registered Gmail Account *************
                //Account[] accounts = AccountManager.get(this).getAccountsByType("com.gmail");
                Account[] accounts = AccountManager.get(this).getAccounts();
                for (Account account : accounts) {
                    _output.append(account.type + "-" + account.name);
                }
            } catch (Exception ex) {
                Toast.makeText(this, "accounts:" + ex.getMessage(), Toast.LENGTH_LONG).show();
            }
            */

            //Toast.makeText(TestActivity.this, "start", Toast.LENGTH_SHORT).show();

            /*
            _cardReader = CardReader.getInstance();

            Toast.makeText(TestActivity.this, "getInstance", Toast.LENGTH_SHORT).show();

            _cardReader.initialize();

            Toast.makeText(TestActivity.this, "initialize", Toast.LENGTH_SHORT).show();


            _cardReader.setOnReadDataListener(new OnReadSerialPortDataListener() {
                @Override
                public void onReadSerialPortData(SerialPortData serialPortData) {
                    final String str = new String(serialPortData.getDataByte());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            _output.setText(str);
                        }
                    });
                }
            });

            findViewById(R.id.btn_read).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {

                        _output.setText("");

                        _cardReader.read();

                        Toast.makeText(TestActivity.this, "read", Toast.LENGTH_SHORT).show();

                    } catch (Exception ex) {
                        Toast.makeText(TestActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            });
            */


            findViewById(R.id.btn_print).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {

                        _printer = Printer.getInstance("");

                        if (!_printer.initialize()) {
                            Toast.makeText(TestActivity.this, "error al inicializar", Toast.LENGTH_SHORT).show();

                            _printer.end();
                        } else {

                            Toast.makeText(TestActivity.this, "initialize", Toast.LENGTH_SHORT).show();

                            _printer.format(2, 1, ALINEACION.LEFT);
                            _printer.printLine("CENTRADA DOBLE ALTO");
                            //_printer.lineFeed();

                            _printer.format(1, 2, ALINEACION.LEFT);
                            _printer.printLine("CENTRADA DOBLE ANCHO");
                            //_printer.lineFeed();

                            _printer.cancelCurrentFormat();
                            _printer.printLine("TEXTO NORMAL");

                            _printer.format(2, 2, ALINEACION.LEFT);
                            _printer.printLine("TITULO");
                            //_printer.lineFeed();

                            _printer.format(2, 2, ALINEACION.CENTER);
                            _printer.printLine("TITULO");

                            _printer.sendSeparadorHorizontal();

                            _printer.footer();

                            //_printer.end();

                            Toast.makeText(TestActivity.this, "print", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception ex) {
                        Toast.makeText(TestActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            });

            findViewById(R.id.btn_test).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {

                        _printer = Printer.getInstance("");

                        if (!_printer.initialize()) {
                            Toast.makeText(TestActivity.this, "error al inicializar", Toast.LENGTH_SHORT).show();

                            _printer.end();
                        }

                        Toast.makeText(TestActivity.this, "init", Toast.LENGTH_SHORT).show();


                        //_printer.test();

                        //Toast.makeText(TestActivity.this, "test", Toast.LENGTH_SHORT).show();

                    } catch (Exception ex) {
                        Toast.makeText(TestActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            });

            findViewById(R.id.btn_read).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {

                        _printer.end();

                        Toast.makeText(TestActivity.this, "end", Toast.LENGTH_SHORT).show();

                    } catch (Exception ex) {
                        Toast.makeText(TestActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            });


        } catch (Exception ex) {
            Toast.makeText(TestActivity.this, "onCreate():" + ex.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    private static boolean isSdPresent() {
        return Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
    }

    @Override
    protected void onDestroy() {

        //Toast.makeText(TestActivity.this, "destroy", Toast.LENGTH_SHORT).show();

        //_printer.end();

        //_cardReader.end();

        super.onDestroy();
    }


    private class ExecuteRequestTask extends AsyncTask<String, String, String> {

        //constructor
        public ExecuteRequestTask() {
        }

        // onPreExecute used to setup the AsyncTask.
        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... messages) {

            // params comes from the execute() call: params[0] is the message.
            try {

                String path = Environment.getExternalStorageDirectory() + "/test";
                String fullPath = path + "/test3.apk";

                File file = new File(path); // PATH = /mnt/sdcard/download/
                if (!file.exists()) {//creamos el dir si no existe
                    boolean created = file.mkdirs();
                    publishProgress("created? " + created);
                }

                publishProgress("path? " + file.getAbsoluteFile());
                publishProgress(" exists? " + file.exists());
                publishProgress(" read? " + file.canRead());
                publishProgress(" write? " + file.canWrite());


                File outputFile = new File(file, "test3.apk");
                /**if (file.exists()) {//lo borramos para sobreescribirlo
                    file.delete();
                }*/
                //boolean created = outputFile.createNewFile();

                //publishProgress("createdF? " + created);
                publishProgress("path? " + outputFile.getAbsoluteFile());
                publishProgress(" exists? " + outputFile.exists());
                publishProgress(" read? " + outputFile.canRead());
                publishProgress(" write? " + outputFile.canWrite());

                URL url = new URL("http://190.111.56.80:37800/apk/EntryPoint_167.apk");
                HttpURLConnection c = (HttpURLConnection) url.openConnection();
                c.setRequestMethod("GET");
                c.setDoOutput(false);
                c.connect();

                FileOutputStream fos = new FileOutputStream(outputFile);

                publishProgress(" type? " + c.getContentType());

                InputStream is = c.getInputStream(); // Get from Server and Catch In Input Stream Object.

                publishProgress(" length? " + c.getContentLength());

                byte[] buffer = new byte[1024];
                int len1 = 0;
                while ((len1 = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);// Write In FileOutputStream.
                }
                fos.close();
                is.close();//en este punto el .apk ya deberia estar descargado en el storage

                publishProgress("path? " + outputFile.getAbsoluteFile());
                publishProgress(" exists? " + outputFile.exists());
                publishProgress(" read? " + outputFile.canRead());
                publishProgress(" write? " + outputFile.canWrite());

                //ya tenemos el apk, instalemoslo

                /*PackageManager pm = TestActivity.this.getPackageManager();

                Method accion = pm.getClass().getMethod("installPackage", new Class[]{Uri.class, IPackageInstallObserver.class, Integer.TYPE, String.class});
                accion.invoke(pm, new Object[] {Uri.fromFile(new File(fullPath)), new PackageInstallObserver(), 2, null});

                return "";*/

                String filename = outputFile.getAbsolutePath();
                String command = "pm install -r " + filename;
                Process proc = Runtime.getRuntime().exec(new String[] { "su", "-c", command });
                proc.waitFor();

                return "ok";

            } catch (IOException e) {
                return e.getMessage();
            } catch (Exception e) {
                return "Error: " + e.getMessage();
            }
        }

        @Override
        protected void onProgressUpdate(String... s) {
            _output.append(s[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            try {
                if (result.equals(""))
                    Toast.makeText(TestActivity.this, "Fin", Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(TestActivity.this, "Fin: " + result, Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                Toast.makeText(TestActivity.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }


        @Override
        protected void onCancelled() {

        }
    }

    class PackageInstallObserver extends IPackageInstallObserver.Stub {
        public void packageInstalled(String packageName, int returnCode) throws RemoteException {
            Toast.makeText(TestActivity.this, "fin", Toast.LENGTH_SHORT).show();
        }

    }

    public interface OnInstalledPackaged {

        public void packageInstalled(String packageName, int returnCode);

    }
}
