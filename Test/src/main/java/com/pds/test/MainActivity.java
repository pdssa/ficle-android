package com.pds.test;

import android.content.res.Configuration;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {

    private OnHardKeyboardEnabledChangeListener mHardKeyboardEnabledChangeListener;
    private boolean mHardKeyboardAvailable = false;

    public interface OnHardKeyboardEnabledChangeListener {
        public void onHardKeyboardEnabledChange(boolean enabled);
    }

    public void setHardKeyboardEnabledChangeListener(
            OnHardKeyboardEnabledChangeListener listener) {
        mHardKeyboardEnabledChangeListener = listener;
    }

    private void updateHardKeyboardEnabled() {
        if (mHardKeyboardAvailable) {
            if (mHardKeyboardEnabledChangeListener != null)
                mHardKeyboardEnabledChangeListener.onHardKeyboardEnabledChange(false);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Toast.makeText(this, "CREATE!", Toast.LENGTH_SHORT).show();

        Configuration currentConfig = getResources().getConfiguration();

        //VIRTUAL KEYBOARD ON
        if (currentConfig.keyboard == Configuration.KEYBOARD_NOKEYS &&
                currentConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_YES)
            Toast.makeText(this, "EXTERNAL KEYBOARD OFF", Toast.LENGTH_SHORT).show();

        //HARDWARE KEYBOARD ON
        if (currentConfig.keyboard == Configuration.KEYBOARD_QWERTY &&
                currentConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO) {
            Toast.makeText(this, "EXTERNAL KEYBOARD ON", Toast.LENGTH_SHORT).show();
            mHardKeyboardAvailable = true;
        }

        try {
            //currentConfig.keyboard = Configuration.KEYBOARD_NOKEYS;
            //currentConfig.hardKeyboardHidden = Configuration.HARDKEYBOARDHIDDEN_YES;

            //getResources().updateConfiguration(currentConfig, null);
            updateHardKeyboardEnabled();
        } catch (Exception ex) {
            Toast.makeText(this, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    /*
    @Override
    protected void onResume() {
        super.onResume();

        Toast.makeText(this, "RESUME!", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart() {
        super.onStart();

        Toast.makeText(this, "START!", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();

        Toast.makeText(this, "PAUSE!", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStop() {
        super.onStop();

        Toast.makeText(this, "STOP!", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Toast.makeText(this, "DESTROY!", Toast.LENGTH_SHORT).show();
    }
    */
    /*
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if(newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO)
            Toast.makeText(this, "EXTERNAL KEYBOARD ON", Toast.LENGTH_SHORT).show();
        if(newConfig.hardKeyboardHidden != Configuration.HARDKEYBOARDHIDDEN_NO)
            Toast.makeText(this, "EXTERNAL KEYBOARD OFF", Toast.LENGTH_SHORT).show();

        Toast.makeText(this, newConfig.toString(), Toast.LENGTH_SHORT).show();
    }
    */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
