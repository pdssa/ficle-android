package com.pds.remote.server;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Messenger;
import android.os.RemoteException;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.display.Message;
import com.pds.common.display.server.ServerDisplayService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TestActivity extends Activity {

    private static final String DEBUG_TAG = TestActivity.class.getName();

    private Button btnSendText;
    private EditText txtSendText;

    private Button btnInput;
    private EditText txtInput;
    private TextView txtInputResponse;

    private Button btnTicketTest;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);//evitamos el sleep durante la ejecucion de esta aplicacion

        txtSendText = (EditText) findViewById(R.id.txtSend);
        btnSendText = (Button) findViewById(R.id.btnSend);
        btnSendText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String text = txtSendText.getText().toString();

                notifyUser("Enviando instruccion MOSTRAR_TEXTO - Parametro:" + text);

                SendTEXT(text);
            }
        });


        /*txtInput = (EditText) findViewById(R.id.txtInput);
        txtInputResponse = (TextView) findViewById(R.id.txtInputResponse);
        btnInput = (Button) findViewById(R.id.btnInput);
        btnInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String text = txtInput.getText().toString();

                notifyUser("Enviando instruccion SOLICITAR_INPUT - Parametro:" + text);

            }
        });*/

        btnTicketTest = (Button) findViewById(R.id.btnTicketTest);
        btnTicketTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notifyUser("Enviando ticket DEMO - Duración 1 minuto");

                ShowTestTICKET();
            }
        });

        findViewById(R.id.btnBindService).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getService();
            }
        });

        //notifyUser("Iniciando servidor...");

        btnTicketTest.setEnabled(false);
        btnSendText.setEnabled(false);

    }

    private boolean mBound = false;
    private Messenger mService;

    @Override
    protected void onStart() {
        super.onStart();

        /** Defines callbacks for service binding, passed to bindService() */
        mConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName className, IBinder service) {
                // This is called when the connection with the service has been
                // established, giving us the object we can use to
                // interact with the service.
                // Cast the IBinder and get LocalService instance
                //ServerDisplayService.ServiceBinder binder = (ServerDisplayService.ServiceBinder) service;
                //mService = binder.getService();


                // This is called when the connection with the service has been
                // established, giving us the object we can use to
                // interact with the service.  We are communicating with the
                // service using a Messenger, so here we get a client-side
                // representation of that from the raw IBinder object.
                mService = new Messenger(service);
                mBound = true;


                btnTicketTest.setEnabled(true);
                btnSendText.setEnabled(true);
            }

            @Override
            public void onServiceDisconnected(ComponentName className) {
                // This is called when the connection with the service has been
                // unexpectedly disconnected -- that is, its process crashed.
                mService = null;
                mBound = false;

                btnTicketTest.setEnabled(false);
                btnSendText.setEnabled(false);
            }
        };


    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }

    /**
     * Class for interacting with the main interface of the service.
     */
    private ServiceConnection mConnection;

    private void getService(){
        // Bind to Service
        if(!mBound) {
            Intent i = new Intent();
            i.setClassName("com.pds.remote.server", "com.pds.common.display.server.ServerDisplayService");
            mBound = bindService(i, mConnection, Context.BIND_AUTO_CREATE);
        }

        // Bind to LocalService
        //Intent i = new Intent(this, ServerDisplayService.class);
        //bindService(i, mConnection, Context.BIND_AUTO_CREATE);
    }

    public void notifyUser(final String msg) {
        TextView t = (TextView) findViewById(R.id.txt);
        t.setText(t.getText() + "\n=== " + msg);
    }


    private void SendTEXT(String text){
        /*if(mService.isOnLine()){
            mService.showText(text);
        }
        else{
            notifyUser("Sin display conectado");
        }*/
        // Create and send a message to the service, using a supported 'what' value
        android.os.Message _msg = android.os.Message.obtain(null, ServerDisplayService.MSG_SHOW_TEXT);

        Bundle data = new Bundle();
        data.putString("text", text);

        _msg.setData(data);

        try {
            mService.send(_msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void ShowTestTICKET() {

        //if(mService.isOnLine()) {

            Message msg = new Message();
            msg.type = Message.MessageType.SHOW_SCREEN_TYPE;
            msg.timeout = 1;


            Message.ShowScreenRequest request = msg.new ShowScreenRequest();

            request.screen = "TICKET";
            request.data = new ArrayList<Message.ShowScreenViewContent>();

            Message.ShowScreenViewContent item = msg.new ShowScreenViewContent();
            item.idView = "txt_total_monto";
            item.classTypeView = "android.widget.TextView";
            item.viewContentType = "java.lang.CharSequence";
            item.viewContent = "100";

            request.data.add(item);

            item = msg.new ShowScreenViewContent();
            item.idView = "txt_total_cant";
            item.classTypeView = "android.widget.TextView";
            item.viewContentType = "java.lang.CharSequence";
            item.viewContent = "7";

            request.data.add(item);

            item = msg.new ShowScreenViewContent();
            item.idView = "txt_pedido";
            item.classTypeView = "android.widget.TextView";
            item.viewContentType = "java.lang.CharSequence";
            item.viewContent = txtSendText.getText().toString();

            request.data.add(item);

            //agregamos una lista, para ello, primero creamos la lista de ITEMS
            List<Message.ShowScreenListContent> viewContentList = new ArrayList<Message.ShowScreenListContent>();

            //cada item, es una lista de views
            Message.ShowScreenListContent listItem = msg.new ShowScreenListContent();

            listItem.item = new ArrayList<Message.ShowScreenViewContent>();

            item = msg.new ShowScreenViewContent();
            item.idView = "item_txt_producto";
            item.classTypeView = "android.widget.TextView";
            item.viewContentType = "java.lang.CharSequence";
            item.viewContent = "AAAA";

            listItem.item.add(item);

            item = msg.new ShowScreenViewContent();
            item.idView = "item_txt_cantidad";
            item.classTypeView = "android.widget.TextView";
            item.viewContentType = "java.lang.CharSequence";
            item.viewContent = "2";

            listItem.item.add(item);

            item = msg.new ShowScreenViewContent();
            item.idView = "item_txt_precio";
            item.classTypeView = "android.widget.TextView";
            item.viewContentType = "java.lang.CharSequence";
            item.viewContent = "50";

            listItem.item.add(item);

            item = msg.new ShowScreenViewContent();
            item.idView = "item_txt_total";
            item.classTypeView = "android.widget.TextView";
            item.viewContentType = "java.lang.CharSequence";
            item.viewContent = "100";

            listItem.item.add(item);

            viewContentList.add(listItem);
            //fin item 1


            //cada item, es una lista de views
            listItem = msg.new ShowScreenListContent();

            listItem.item = new ArrayList<Message.ShowScreenViewContent>();

            item = msg.new ShowScreenViewContent();
            item.idView = "item_txt_producto";
            item.classTypeView = "android.widget.TextView";
            item.viewContentType = "java.lang.CharSequence";
            item.viewContent = "BBBB";

            listItem.item.add(item);

            item = msg.new ShowScreenViewContent();
            item.idView = "item_txt_cantidad";
            item.classTypeView = "android.widget.TextView";
            item.viewContentType = "java.lang.CharSequence";
            item.viewContent = "1";

            listItem.item.add(item);

            item = msg.new ShowScreenViewContent();
            item.idView = "item_txt_precio";
            item.classTypeView = "android.widget.TextView";
            item.viewContentType = "java.lang.CharSequence";
            item.viewContent = "25";

            listItem.item.add(item);

            item = msg.new ShowScreenViewContent();
            item.idView = "item_txt_total";
            item.classTypeView = "android.widget.TextView";
            item.viewContentType = "java.lang.CharSequence";
            item.viewContent = "25";

            listItem.item.add(item);

            viewContentList.add(listItem);
            //fin item 2

            item = msg.new ShowScreenViewContent();
            item.idView = "lst_pedido";
            item.classTypeView = "android.widget.ListView";
            item.viewContentType = "pedido_item";
            item.setListContent(viewContentList);

            request.data.add(item);
            //

            msg.content = request.toString();

            //mService.showScreen(msg);

        // Create and send a message to the service, using a supported 'what' value
        android.os.Message _msg = android.os.Message.obtain(null, ServerDisplayService.MSG_SHOW_SCREEN);

        Bundle data = new Bundle();
        data.putParcelable("d", msg);

        _msg.setData(data);

        try {
            mService.send(_msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        /*}
        else{
            notifyUser("Sin display conectado");
        }*/

    }


}
