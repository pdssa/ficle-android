package com.pds.ficle.ctrlhora;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Dialog;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.os.Build;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Caja;
import com.pds.common.Caja.TipoMovimientoCaja;
import com.pds.common.CajaHelper;
import com.pds.common.Config;
import com.pds.common.Formatos;
import com.pds.common.HorarioHelper;
import com.pds.common.HorarioHelper.TipoMovimientoHorario;
import com.pds.common.Logger;
import com.pds.common.Usuario;
import com.pds.common.UsuarioAdapter;
import com.pds.common.UsuarioHelper;
import com.pds.common.activity.TimerActivity;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ControlHorarioMain extends TimerActivity {

    List<Usuario> list_all_users;
    //private Spinner mUserView;
    private ListView mUsersView;
    //private Button btnEntrada;
    //private Button btnSalida;
    private ImageButton btnReporte;
    private EditText mPasswordView;
    private Usuario _user;
    Usuario mUser;

    BroadcastReceiver _broadcastReceiver;

    @Override
    public void onStart() {
        super.onStart();
        _broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().compareTo(Intent.ACTION_TIME_TICK) == 0) {
                    Date now = new Date();
                    ((TextView) findViewById(R.id.control_horario_txt_fecha)).setText(Formatos.FormateaDate(now, Formatos.ShortDateFormat));
                    ((TextView) findViewById(R.id.control_horario_txt_hora)).setText(Formatos.FormateaDate(now, Formatos.ShortTimeFormat));
                }
            }
        };

        registerReceiver(_broadcastReceiver, new IntentFilter(Intent.ACTION_TIME_TICK));
    }

    @Override
    public void onStop() {
        super.onStop();
        if (_broadcastReceiver != null)
            unregisterReceiver(_broadcastReceiver);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_control_horario_main);

            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);//evitamos el sleep durante la ejecucion de esta aplicacion

            ((TextView) findViewById(R.id.txtVersion)).setText("Version:" + getPackageManager().getPackageInfo(this.getPackageName().toString(), 0).versionName);

            //logueo Usuario *****************************************************
            //obtenemos los parametros pasamos a la actividad
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                //tomamos el id de usuario
                //_id_user_login = extras.getInt("_id_user_login");
                this._user = (Usuario) extras.getSerializable("current_user");
            }

            //no tenemos user logueado...volvemos a EntryPoint
            if (_user == null) {
                Toast.makeText(this, R.string.unknown_user, Toast.LENGTH_LONG).show();
                this.finish();
            } else {
                //********* USER LOGUEADO OK *********
                AsignarViews();

                AsignarEventos();

                Config config = new Config(ControlHorarioMain.this);

                //setTimer(config.INACTIVITY_TIME);
            }
        } catch (Exception ex) {
            Toast.makeText(this, "Error al iniciar: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        ValidaSeguridadPerfil();

        LeerUsuarios();

        ((TextView) findViewById(R.id.control_horario_txt_fecha)).setText(Formatos.FormateaDate(new Date(), Formatos.ShortDateFormat));
        ((TextView) findViewById(R.id.control_horario_txt_hora)).setText(Formatos.FormateaDate(new Date(), Formatos.ShortTimeFormat));
    }

    private void AsignarViews() {
        this.mUsersView = (ListView) findViewById(R.id.control_horario_lst_users);
        this.mUsersView.setEmptyView(findViewById(android.R.id.empty));
        //this.mUserView = (Spinner) findViewById(R.id.spn_users);
        //this.btnEntrada = (Button) findViewById(R.id.control_horario_btn_entrada);
        //this.btnSalida = (Button) findViewById(R.id.control_horario_btn_salida);
        //this.mPasswordView = (EditText) findViewById(R.id.password);
        this.btnReporte = (ImageButton) findViewById(R.id.control_horario_btn_reporte);


    }

    private void AsignarEventos() {

        this.mUsersView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mUser = list_all_users.get(position);
                VisualizaPopupRegistro(list_all_users.get(position).getNombre());
            }
        });

        /*
        this.mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });*/
        /*
        this.btnEntrada.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                RegistrarMovimientoHorario(TipoMovimientoHorario.ENTRADA, "");
                mPasswordView.setText("");

            }
        });

        this.btnSalida.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                RegistrarMovimientoHorario(TipoMovimientoHorario.SALIDA, "");
                mPasswordView.setText("");
            }
        });*/

        this.btnReporte.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    //Toast.makeText(ControlHorarioMain.this, "Funcionalidad no implementada", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(ControlHorarioMain.this, ActivityRptHorarios.class);
                    //i.putExtra("_id", ((Usuario) mUserView.getSelectedItem()).getId());
                    startActivity(i);

                } catch (Exception ex) {
                    Toast.makeText(ControlHorarioMain.this, "Error al abrir reporte: " + ex.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });

        /*
        ((Button) findViewById(R.id.control_horario_btn_info_test)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        */

        findViewById(R.id.txtVersion).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowPopupMenu(view);
            }
        });
    }

    public boolean ValidarClave() {


        String mPassword;

        // Reset errors.
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.

        mPassword = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password.
        if (TextUtils.isEmpty(mPassword)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        } else if (mPassword.length() != 4) {
            mPasswordView.setError(getString(R.string.error_short_password));
            focusView = mPasswordView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.

            boolean result = mUser.getPassword().equals(mPassword);

            if (!result) {

                mPasswordView.setError(getString(R.string.error_incorrect_password));
                mPasswordView.requestFocus();
            }

            return result;
        }

        return false;
    }

    private void LeerUsuarios() {
        try {

            Cursor usuarios = getContentResolver().query(
                    Uri.parse("content://com.pds.ficle.ep.usuarios.contentprovider/usuarios"),
                    UsuarioHelper.columnas,
                    "",
                    new String[]{},
                    UsuarioHelper.USUARIOS_LOGIN + " ASC");

            list_all_users = new ArrayList<Usuario>();

            Usuario loggedUser = null;

            // recorremos los items
            if (usuarios.moveToFirst()) {
                do {

                    Usuario userRow = new Usuario();

                    //no vamos a mostrar el usuario admin
                    if (usuarios.getInt(UsuarioHelper.USUARIOS_IX_ID) != 1) {
                        userRow.setId(usuarios.getInt(UsuarioHelper.USUARIOS_IX_ID));
                        userRow.setLogin(usuarios.getString(UsuarioHelper.USUARIOS_IX_LOGIN));
                        userRow.setNombre(usuarios.getString(UsuarioHelper.USUARIOS_IX_NOMBRE));
                        userRow.setApellido(usuarios.getString(UsuarioHelper.USUARIOS_IX_APELLIDO));
                        userRow.setId_perfil(usuarios.getInt(UsuarioHelper.USUARIOS_IX_PERFIL));
                        userRow.setPassword(usuarios.getString(UsuarioHelper.USUARIOS_IX_PASSWORD));
                        userRow.setFoto(usuarios.getString(UsuarioHelper.USUARIOS_IX_FOTO));
                        userRow.setCelular(usuarios.getString(UsuarioHelper.USUARIOS_IX_CELULAR));
                        userRow.setHora_entrada(usuarios.getString(UsuarioHelper.USUARIOS_IX_HORA_ENTRADA));
                        userRow.setHora_salida(usuarios.getString(UsuarioHelper.USUARIOS_IX_HORA_SALIDA));

                        //leemos los horario de ingreso/egreso de hoy
                        LeerHorariosHoy(userRow);

                        list_all_users.add(userRow);

                        if (userRow.getId() == _user.getId())
                            loggedUser = userRow;
                    }

                } while (usuarios.moveToNext());
            }


            mUsersView.setAdapter(new UsuarioAdapter(this, list_all_users, android.R.layout.simple_spinner_dropdown_item, false, true));

            usuarios.close();

            //mUserView.setSelection(((UsuarioAdapter) mUserView.getAdapter()).getPosition(loggedUser));

            //this._user.getId()

        } catch (Exception ex) {
            Toast.makeText(this, "Error al obtener usuarios: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    private void LeerHorariosHoy(Usuario userRow) {
        try {

            Cursor result = getContentResolver().query(
                    Uri.parse("content://com.pds.ficle.ep.horario.contentprovider/horario"),
                    HorarioHelper.columnas_grilla_horario,
                    HorarioHelper.HORARIO_USER_ID + "= " + userRow.getId() + " and (" + HorarioHelper.HORARIO_FECHA + " is null or substr(" + HorarioHelper.HORARIO_FECHA + ",7)||substr(" + HorarioHelper.HORARIO_FECHA + ",4,2)||substr(" + HorarioHelper.HORARIO_FECHA + ",1,2) = '" + Formatos.FormateaDate(new Date(), Formatos.DbDateFormat) + "')",
                    new String[]{},
                    null);

            // recorremos los items
            if (result.moveToFirst()) {
                do {

                    //cargamos las fechas
                    userRow.setEntrada_de_hoy(result.getString(HorarioHelper.HORARIO_IX_VW_HORA_IN));
                    userRow.setSalida_de_hoy(result.getString(HorarioHelper.HORARIO_IX_VW_HORA_OUT));

                } while (result.moveToNext());
            }

            result.close();

        } catch (Exception ex) {
            Toast.makeText(ControlHorarioMain.this, "Error en LeerHorarios: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    private boolean ExisteRegistroHoy(Usuario userRow, TipoMovimientoHorario tipoMov) {

        boolean existe = false;

        try {

            if(tipoMov == TipoMovimientoHorario.ENTRADA)
                existe = !TextUtils.isEmpty(userRow.getEntrada_de_hoy());
            if(tipoMov==TipoMovimientoHorario.SALIDA)
                existe = !TextUtils.isEmpty(userRow.getSalida_de_hoy());

            /*
            Cursor result = getContentResolver().query(
                    Uri.parse("content://com.pds.ficle.ep.horario.contentprovider/horario"),
                    HorarioHelper.columnas_grilla_horario,
                    HorarioHelper.HORARIO_USER_ID + "= " + userRow.getId() + " and (substr(" + HorarioHelper.HORARIO_FECHA + ",7)||substr(" + HorarioHelper.HORARIO_FECHA + ",4,2)||substr(" + HorarioHelper.HORARIO_FECHA + ",1,2) = '" + Formatos.FormateaDate(new Date(), Formatos.DbDateFormat) + "')",
                    new String[]{},
                    null);

            // validamos si existe registro
            // recorremos los items
            if (result.moveToFirst()) {
                do {

                    if()

                    //cargamos las fechas
                    userRow.setEntrada_de_hoy(result.getString(HorarioHelper.HORARIO_IX_VW_HORA_IN));
                    userRow.setSalida_de_hoy(result.getString(HorarioHelper.HORARIO_IX_VW_HORA_OUT));

                } while (result.moveToNext());
            }
            existe = result.moveToFirst();

            result.close();
            */

        } catch (Exception ex) {
            Toast.makeText(ControlHorarioMain.this, "Error en LeerHorarios: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }

        return existe;

    }

    private void ValidaSeguridadPerfil() {
        if (_user.getId_perfil() == 1) {
            this.btnReporte.setVisibility(View.VISIBLE);
        } else {
            this.btnReporte.setVisibility(View.INVISIBLE);
        }
    }

    public boolean RegistrarMovimientoHorario(
            TipoMovimientoHorario tipoMov,
            String observacion) {

        if (ValidarClave()) {
            try {

                //CONTROL PARA EVITAR DOBLE REGISTRO DE MISMO TIPO MISMO DIA
                if (ExisteRegistroHoy(mUser, tipoMov)) {
                    Toast.makeText(this, "Error: ya posee un registro de " + tipoMov.name() + " para el dia de hoy" , Toast.LENGTH_LONG).show();
                } else {

                    //mUser = (Usuario) mUserView.getSelectedItem();

                    ContentValues cv = new ContentValues();

                    cv.put(HorarioHelper.HORARIO_TIPO_MOV, tipoMov.ordinal());
                    cv.put(HorarioHelper.HORARIO_USER_ID, mUser.getId());
                    cv.put(HorarioHelper.HORARIO_FECHA, new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
                    cv.put(HorarioHelper.HORARIO_HORA, new SimpleDateFormat("HH:mm:ss").format(new Date()));
                    cv.put(HorarioHelper.HORARIO_OBSERVACION, observacion);


                    Uri newProdUri = this.getContentResolver().insert(Uri.parse("content://com.pds.ficle.ep.horario.contentprovider/horario"), cv);

                    Logger.RegistrarEvento(
                            ControlHorarioMain.this,
                            "i",
                            tipoMov.name(),
                            "User: " + mUser.getNombre()
                    );

                    Toast.makeText(this, tipoMov.name() + " de " + mUser.getNombre() + " registrada OK", Toast.LENGTH_SHORT).show();

                    return true;
                }
            } catch (Exception ex) {
                Toast.makeText(this, "Error al registrar: " + ex.getMessage(), Toast.LENGTH_LONG).show();
            }
        }

        return false;

    }

    public boolean RegistrarMovimientoHorarioDemo(
            TipoMovimientoHorario tipoMov,
            String observacion,
            Usuario usuario,
            Date fecha,
            String hora) {

        try {

            ContentValues cv = new ContentValues();

            cv.put(HorarioHelper.HORARIO_TIPO_MOV, tipoMov.ordinal());
            cv.put(HorarioHelper.HORARIO_USER_ID, usuario.getId());
            cv.put(HorarioHelper.HORARIO_FECHA, new SimpleDateFormat("dd-MM-yyyy").format(fecha));
            cv.put(HorarioHelper.HORARIO_HORA, hora);
            cv.put(HorarioHelper.HORARIO_OBSERVACION, observacion);

            Uri newProdUri = this.getContentResolver().insert(Uri.parse("content://com.pds.ficle.ep.horario.contentprovider/horario"), cv);

            return true;

        } catch (Exception ex) {
            Toast.makeText(this, "Error al registrar: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }


        return false;

    }

    public void VisualizaPopupRegistro(String userSeleccionado) {
        // custom dialog
        final Dialog dialog = new Dialog(ControlHorarioMain.this);
        dialog.setContentView(R.layout.popup_registra_horario);
        dialog.setTitle("Registrar Evento");
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        //seteamos el nombre de usuario seleccionado
        TextView usuario = (TextView) dialog.findViewById(R.id.user);
        usuario.setText("Usuario: " + userSeleccionado);

        //limpiamos la clave
        this.mPasswordView = (EditText) dialog.findViewById(R.id.password);
        this.mPasswordView.setText("");

        // si se presiona ENTRADA, registramos una ENTRADA
        Button btnEntrada = (Button) dialog.findViewById(R.id.control_horario_btn_entrada);
        btnEntrada.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (RegistrarMovimientoHorario(TipoMovimientoHorario.ENTRADA, "")) {
                    dialog.dismiss();

                    LeerUsuarios();//actualizamos la lista de usuarios
                }
            }
        });

        // si se presiona SALIDA, registramos una SALIDA
        Button btnSalida = (Button) dialog.findViewById(R.id.control_horario_btn_salida);
        btnSalida.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (RegistrarMovimientoHorario(TipoMovimientoHorario.SALIDA, "")) {
                    dialog.dismiss();

                    LeerUsuarios();//actualizamos la lista de usuarios
                }

            }
        });

        dialog.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.control_horario_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_info_demo) {

            GenerarInfoTest();

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void ShowPopupMenu(View v){
        PopupMenu popup = new PopupMenu(this, v);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return onOptionsItemSelected(item);
            }
        });
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.control_horario_main, popup.getMenu());

        popup.show();
    }

    public void GenerarInfoTest() {
        try {

            // Get calendar set to current date and time
            Calendar c = Calendar.getInstance();

            //para cada usuario completamos informacion de los ultimos 15 dias
            for (int i = 0; i < 15; i++) {

                for (Usuario usuario : list_all_users) {

                    String horaEntrada = "";
                    String horaSalida = "";

                    if (i % 2 == 0) {
                        horaEntrada = usuario.getHora_entrada().substring(0, 3) + "10" + ":00";
                        horaSalida = usuario.getHora_salida().substring(0, 3) + "00" + ":00";
                    } else {
                        horaEntrada = usuario.getHora_entrada().substring(0, 3) + "20" + ":00";
                        horaSalida = usuario.getHora_salida().substring(0, 3) + "59" + ":00";
                    }

                    RegistrarMovimientoHorarioDemo(TipoMovimientoHorario.ENTRADA, "", usuario, c.getTime(), horaEntrada);
                    RegistrarMovimientoHorarioDemo(TipoMovimientoHorario.SALIDA, "", usuario, c.getTime(), horaSalida);
                }

                //retrocedemos un dia
                c.add(Calendar.DATE, -1);
            }

            Logger.RegistrarEvento(
                    ControlHorarioMain.this,
                    "i",
                    "Control Horario",
                    "Informacion de test generada correctamente"
            );

            Toast.makeText(ControlHorarioMain.this, "Informacion de test generada correctamente", Toast.LENGTH_SHORT).show();

            LeerUsuarios();//actualizamos la lista de usuarios

        } catch (Exception ex) {
            Toast.makeText(ControlHorarioMain.this, "Error al generar informacion de test: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

}
