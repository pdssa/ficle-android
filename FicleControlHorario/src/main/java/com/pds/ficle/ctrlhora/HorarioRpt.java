package com.pds.ficle.ctrlhora;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hernan on 04/03/14.
 */
public class HorarioRpt {
    private String username;
    private int userid;
    private String horarioEntradaHabitual;
    private String horarioSalidaHabitual;
    private List<HorarioRptItem> horarios;

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getHorarioEntradaHabitual() {
        if(horarioEntradaHabitual == null)
            return "";
        else
            return horarioEntradaHabitual;
    }

    public void setHorarioEntradaHabitual(String horarioEntradaHabitual) {
        this.horarioEntradaHabitual = horarioEntradaHabitual;
    }

    public String getHorarioSalidaHabitual() {
        if(horarioSalidaHabitual == null)
            return "";
        else
            return horarioSalidaHabitual;
    }

    public void setHorarioSalidaHabitual(String horarioSalidaHabitual) {
        this.horarioSalidaHabitual = horarioSalidaHabitual;
    }

    public List<HorarioRptItem> getHorarios() {
        if(horarios == null)
            horarios = new ArrayList<HorarioRptItem>();
        return horarios;
    }

    public void setHorarios(List<HorarioRptItem> horarios) {
        this.horarios = horarios;
    }
}

