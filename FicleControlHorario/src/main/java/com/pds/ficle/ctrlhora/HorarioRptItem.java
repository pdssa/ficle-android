package com.pds.ficle.ctrlhora;

/**
 * Created by Hernan on 04/03/14.
 */
public class HorarioRptItem {
    private String dia;
    private String horarioEntradaDia;
    private String horarioSalidaDia;

    public String getDia() {
        if(dia == null)
            return "";
        else
            return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getHorarioEntradaDia() {
        if (horarioEntradaDia == null)
            return "";
        else {
            if (horarioEntradaDia.length() == 8)
                return horarioEntradaDia.substring(0, 5);
            else
                return horarioEntradaDia;
        }
    }

    public void setHorarioEntradaDia(String horarioEntradaDia) {
        this.horarioEntradaDia = horarioEntradaDia;
    }

    public String getHorarioSalidaDia() {
        if (horarioSalidaDia == null)
            return "";
        else {
            if (horarioSalidaDia.length() == 8)
                return horarioSalidaDia.substring(0, 5);
            else
                return horarioSalidaDia;
        }
    }

    public void setHorarioSalidaDia(String horarioSalidaDia) {
        this.horarioSalidaDia = horarioSalidaDia;
    }

    public HorarioRptItem(String dia, String horarioEntradaDia, String horarioSalidaDia) {
        this.dia = dia;
        this.horarioEntradaDia = horarioEntradaDia;
        this.horarioSalidaDia = horarioSalidaDia;
    }
}