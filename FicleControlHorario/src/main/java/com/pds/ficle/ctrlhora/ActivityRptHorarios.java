package com.pds.ficle.ctrlhora;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.Toast;

import com.pds.common.Formatos;
import com.pds.common.HorarioHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ActivityRptHorarios extends Activity {

    private WebView vw_rpt;
    private List<HorarioRpt> dataReporte;
    private List<Date> fechasSemana;
    private List<Date> fechasMes;
    private int semanaIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rpt_horarios);

        //obtenemos los parametros pasamos a la actividad
        /*Bundle extras = getIntent().getExtras();

        int idUsuario = -1;

        if (extras != null) {
            idUsuario = extras.getInt("_id");
        }
        */
        this.vw_rpt = (WebView) findViewById(R.id.rpt_horarios_vw);

        try {
            ((ImageButton) findViewById(R.id.rpt_horarios_btn_sem_ant)).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    semanaIndex--;
                    EjecutarReporte(); //obtenemos los nuevos días y actualizamos la informacion
                }
            });

            ((ImageButton) findViewById(R.id.rpt_horarios_btn_sem_sig)).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    semanaIndex++;
                    EjecutarReporte(); //obtenemos los nuevos días y actualizamos la informacion
                }
            });

            semanaIndex = 0;//arrancamos en la semana en curso

            EjecutarReporte();
        } catch (Exception ex) {
            Toast.makeText(ActivityRptHorarios.this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void EjecutarReporte() {

        ObtenerDiasSemana();

        ObtenerDiasMes();

        ObtenerDataReporte(fechasSemana.get(0), fechasSemana.get(6));

        GenerarReporteHTML();

    }

   /* private void ReporteTest() {
        StringBuilder htmlBuilder = new StringBuilder();

        htmlBuilder.append("<html><head><link href='styles.css' rel='stylesheet'></head><body>");
        htmlBuilder.append("<table>");

        for (Date date : fechasSemana) {
            htmlBuilder.append("<tr><td>");

            htmlBuilder.append(Formatos.FormateaDate(date, Formatos.ShortDateFormatNameOfDay));

            htmlBuilder.append("</td></tr>");
        }
        htmlBuilder.append("</table></body></html>");

        this.vw_rpt.loadDataWithBaseURL("file:///android_asset/", htmlBuilder.toString(), "text/html", "utf-8", null);
    }*/

    private void ObtenerDiasSemana() {
        try {
            // Get calendar set to current date and time
            Calendar c = Calendar.getInstance();

            //si es domingo, retrocedemos un día para que no tome la semana siguiente
            if (c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
                c.add(Calendar.DATE, -1);

            // Set the calendar to Monday of the current week
            c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

            // Ubicamos la semana del scroll (+/- 7 dias)
            c.add(Calendar.DATE, 7 * semanaIndex);


            // Print dates of the current week starting on Sunday
            //DateFormat df = new SimpleDateFormat("EEE dd/MM/yyyy");
            fechasSemana = new ArrayList<Date>();

            for (int i = 0; i < 7; i++) {
                fechasSemana.add(c.getTime());
                //System.out.println(df.format(c.getTime()));
                //avanzamos un dia
                c.add(Calendar.DATE, 1);
            }
        } catch (Exception ex) {
            Toast.makeText(ActivityRptHorarios.this, "Error en ObtenerDiasSemana: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void ObtenerDiasMes() {
        try {
            // Get calendar set to current date and time
            Calendar c = Calendar.getInstance();

            //*******idem a inicio de ObtenerDiaSemana*******
            //si es domingo, retrocedemos un día para que no tome la semana siguiente
            if (c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
                c.add(Calendar.DATE, -1);

            // Set the calendar to Monday of the current week
            c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

            // Ubicamos la semana del scroll (+/- 7 dias)
            c.add(Calendar.DATE, 7 * semanaIndex);

            // Recorremos dias de la semana
            for (int i = 0; i < 7; i++) {
                //avanzamos un dia
                c.add(Calendar.DATE, 1);
            }

            //************************************************

            // Ubicamos el mes (-30 dias)
            c.add(Calendar.DATE, -30);

            fechasMes = new ArrayList<Date>();

            for (int i = 0; i < 30; i++) {
                fechasMes.add(c.getTime());

                //avanzamos un dia
                c.add(Calendar.DATE, 1);
            }
        } catch (Exception ex) {
            Toast.makeText(ActivityRptHorarios.this, "Error en ObtenerDiasMes: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private double DiferenciaDeHorasEnMinutos(String horaDesde, String horaHasta){
        try{

            //validamos que alguno no venga en blanco o en null
            if(horaDesde == null || horaHasta == null)
                return -1;

            if(horaDesde.equals("") || horaHasta.equals(""))
                return -1;

            //calculamos la cantidad de hs que corresponde trabajar (en minutos): hasta - desde
            Date horaEntrada = Formatos.ObtieneDate(horaDesde, Formatos.ShortTimeFormat);
            Date horaSalida = Formatos.ObtieneDate(horaHasta, Formatos.ShortTimeFormat);

            double tiempoCorresponde = (horaSalida.getTime() - horaEntrada.getTime()) / 60000; //lo pasamos de milisegundos a minutos

            return tiempoCorresponde;

        } catch (Exception ex) {
            Toast.makeText(ActivityRptHorarios.this, "Error en DiferenciaDeHorasEnMinutos: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }

        return -1;
    }

    private String CalculaIndicador(double tiempoTrabajado, int diasTrabajados, String horaEntradaHabitual, String horaSalidaHabitual){
        try{

            if(diasTrabajados == 0)
                return "0";

            //calculamos la cantidad de hs que corresponde trabajar (en minutos): salidaHabitual - entradaHabitual
            double tiempoCorresponde = DiferenciaDeHorasEnMinutos(horaEntradaHabitual, horaSalidaHabitual);


            if(tiempoCorresponde != -1) { //calculo OK
                double result = tiempoTrabajado / (diasTrabajados * tiempoCorresponde); //calculamos el indicador

                String result_str = String.valueOf(result * 100); //para pasarlo a porcentaje

                return result_str.substring(0,result_str.indexOf("."));
            }
            else{
                return "X"; //falta algun valor para el calculo
            }

        } catch (Exception ex) {
            Toast.makeText(ActivityRptHorarios.this, "Error en CalculaIndicador: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
        return "";
    }

    private String CalculaIndicadorMes(Date desde, Date hasta, int idUser) {

        int cantidadDiasTrabajoMes = 0;
        double tiempoTrabajadoMes = 0;
        String horaEntradaHabitual = "";
        String horaSalidaHabitual = "";

        try {

            Cursor result = getContentResolver().query(
                    Uri.parse("content://com.pds.ficle.ep.horario.contentprovider/horario"),
                    HorarioHelper.columnas_grilla_horario,
                    "substr(" + HorarioHelper.HORARIO_FECHA +
                            ",7)||substr(" + HorarioHelper.HORARIO_FECHA + ",4,2)||substr(" + HorarioHelper.HORARIO_FECHA + ",1,2) between '" +
                            Formatos.FormateaDate(desde, Formatos.DbDateFormat) + "' and '" + Formatos.FormateaDate(hasta, Formatos.DbDateFormat) + "' and " +
                            HorarioHelper.HORARIO_USER_ID + "=" + String.valueOf(idUser),
                    new String[]{},
                    ""
            );

            HorarioRpt horarioUser = new HorarioRpt();

            // recorremos los items
            if (result.moveToFirst()) {
                do {

                    //horarioUser.setUserid(result.getInt(HorarioHelper.HORARIO_IX_USER_ID));
                    //horarioUser.setUsername(result.getString(HorarioHelper.HORARIO_IX_VW_USUARIO));
                    horarioUser.setHorarioEntradaHabitual(result.getString(HorarioHelper.HORARIO_IX_VW_HORA_IN_HAB));
                    horarioUser.setHorarioSalidaHabitual(result.getString(HorarioHelper.HORARIO_IX_VW_HORA_OUT_HAB));

                    horarioUser.getHorarios().add(new HorarioRptItem(
                            result.getString(HorarioHelper.HORARIO_IX_VW_FECHA),
                            result.getString(HorarioHelper.HORARIO_IX_VW_HORA_IN),
                            result.getString(HorarioHelper.HORARIO_IX_VW_HORA_OUT)
                    ));

                } while (result.moveToNext());

            }


            //** tabla horarios **
             horaEntradaHabitual = horarioUser.getHorarioEntradaHabitual();
             horaSalidaHabitual = horarioUser.getHorarioSalidaHabitual();


            //recorremos la informacion por cada dia del mes
            for (Date date : fechasMes) {
                String dia = Formatos.FormateaDate(date, Formatos.DateFormat);

                //buscamos la info del dia correspondiente de la semana
                for (HorarioRptItem horarioDia : horarioUser.getHorarios()) {
                    if (horarioDia.getDia().equals(dia)) {

                        cantidadDiasTrabajoMes++;

                        //contabilizamos como dia trabajado solo si tiene la entrada y salida registrada
                        if(!horarioDia.getHorarioEntradaDia().equals("") && !horarioDia.getHorarioSalidaDia().equals("")){
                            cantidadDiasTrabajoMes++;
                            tiempoTrabajadoMes += DiferenciaDeHorasEnMinutos(horarioDia.getHorarioEntradaDia(), horarioDia.getHorarioSalidaDia());
                        }

                        break; //que siga con el proximo dia
                    }
                }

                result.close();
            }
        } catch (Exception ex) {
            Toast.makeText(ActivityRptHorarios.this, "Error en CalculaIndicadorMes: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }

        //return String.valueOf(cantidadDiasTrabajoMes == 0 ? 0 : (cantidadDiasCumplioHorario * 100) / cantidadDiasTrabajoMes) ;
        return CalculaIndicador(tiempoTrabajadoMes, cantidadDiasTrabajoMes,horaEntradaHabitual, horaSalidaHabitual );
    }

    private void ObtenerDataReporte(Date desde, Date hasta) {
        try {

            Cursor result = getContentResolver().query(
                    Uri.parse("content://com.pds.ficle.ep.horario.contentprovider/horario"),
                    HorarioHelper.columnas_grilla_horario,
                    HorarioHelper.HORARIO_USER_ID + "<> 1 and (" + HorarioHelper.HORARIO_FECHA + " is null or substr(" + HorarioHelper.HORARIO_FECHA + ",7)||substr(" + HorarioHelper.HORARIO_FECHA + ",4,2)||substr(" + HorarioHelper.HORARIO_FECHA + ",1,2) between '" + Formatos.FormateaDate(desde, Formatos.DbDateFormat) + "' and '" + Formatos.FormateaDate(hasta, Formatos.DbDateFormat) + "')",
                    new String[]{},
                    null);

            //iniciamos la lista de resultados
            dataReporte = new ArrayList<HorarioRpt>();

            String current_user = ""; //variable usada para el corte de control

            HorarioRpt horariosSemanaUser = new HorarioRpt();

            // recorremos los items
            if (result.moveToFirst()) {
                do {

                    if (!result.getString(HorarioHelper.HORARIO_IX_VW_USUARIO).equals(current_user)) {

                        if (!current_user.equals("")) //sino es el primero, agregamos a la lista el anterior
                            dataReporte.add(horariosSemanaUser);

                        current_user = result.getString(HorarioHelper.HORARIO_IX_VW_USUARIO);

                        horariosSemanaUser = new HorarioRpt();
                        horariosSemanaUser.setUserid(result.getInt(HorarioHelper.HORARIO_IX_VW_USER_ID));
                        horariosSemanaUser.setUsername(current_user);
                        horariosSemanaUser.setHorarioEntradaHabitual(result.getString(HorarioHelper.HORARIO_IX_VW_HORA_IN_HAB));
                        horariosSemanaUser.setHorarioSalidaHabitual(result.getString(HorarioHelper.HORARIO_IX_VW_HORA_OUT_HAB));
                    }

                    horariosSemanaUser.getHorarios().add(new HorarioRptItem(
                            result.getString(HorarioHelper.HORARIO_IX_VW_FECHA),
                            result.getString(HorarioHelper.HORARIO_IX_VW_HORA_IN),
                            result.getString(HorarioHelper.HORARIO_IX_VW_HORA_OUT)
                    ));

                } while (result.moveToNext());

                //agregamos el ultimo
                dataReporte.add(horariosSemanaUser);
            }


            result.close();

        } catch (Exception ex) {
            Toast.makeText(ActivityRptHorarios.this, "Error en ObtenerDataReporte: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    private void GenerarReporteHTML() {

        try {
            //*** armamos la fila con los dias ***
            String htmlRowDias = "<tr>";
            htmlRowDias += "<td style='border:none;'>&nbsp;</td>";
            for (Date date : fechasSemana) {
                htmlRowDias += "<td style='border:none;font-weight:normal;font-size:18px;padding:3px;' align='center'>";

                htmlRowDias += Formatos.FormateaDate(date, Formatos.ShortDateFormatNameOfDay);

                htmlRowDias += "</td>";
            }
            htmlRowDias += "<td style='border:none;'>&nbsp;</td>";
            htmlRowDias += "</tr>";

            //*** codigo general ***
            StringBuilder htmlBuilder = new StringBuilder();

            htmlBuilder.append("<html><head><link href='styles.css' rel='stylesheet'></head><body>");

            //** tabla del reporte **
            htmlBuilder.append("<table width='100%' CELLSPACING='0' CELLPADDING='0'>");

            for (HorarioRpt horarioUser : dataReporte) {

                //** fila por usuario **
                htmlBuilder.append("<tr>");

                //** imagen y nombre **
                htmlBuilder.append("<td style='border:none;font-weight:normal;' align='center' rowspan='2'>");
                htmlBuilder.append("<img src='usuarios.png'/><br/>" + horarioUser.getUsername());
                htmlBuilder.append("</td>");

                //** tabla horarios **
                String horaEntradaHabitual = horarioUser.getHorarioEntradaHabitual();
                String horaSalidaHabitual = horarioUser.getHorarioSalidaHabitual();

                //FILA ENTRADA y SALIDA
                String htmlRowIn = "";
                String htmlRowOut = "";

                boolean hayInfoDelDia = false;

                int cantidadDiasTrabajoSem = 0;
                double tiempoTrabajadoSem = 0;

                //recorremos la informacion por cada dia de la semana
                for (Date date : fechasSemana) {
                    String dia = Formatos.FormateaDate(date, Formatos.DateFormat);

                    //***variables locales para tratar cada dia***
                    hayInfoDelDia = false; //este flag nos permite saber si el usuario tenia info del dia buscado
                    boolean sumaIndiceTrabajoDia = false;
                    double tiempoTrabajado = 0;
                    String _htmlIn = "";
                    String _htmlOut = "";

                    //buscamos la info del dia correspondiente de la semana
                    for (HorarioRptItem horarioDia : horarioUser.getHorarios()) {
                        if (horarioDia.getDia().equals(dia)) {

                            hayInfoDelDia = true;

                            //vemos el horario de entrada:
                            if (!horarioDia.getHorarioEntradaDia().equals(horaEntradaHabitual))//comparamos sino coinciden ponen color rojo
                                _htmlIn = "<td style='color:red;' align='center'>" + horarioDia.getHorarioEntradaDia() + "</td>";
                            else
                                _htmlIn  = "<td align='center'>" + horarioDia.getHorarioEntradaDia() + "</td>";

                            //vemos el horario de salida:
                            if (!horarioDia.getHorarioSalidaDia().equals(horaSalidaHabitual))//comparamos sino coinciden ponen color rojo
                                _htmlOut = "<td style='color:red;' align='center'>" + horarioDia.getHorarioSalidaDia() + "</td>";
                            else
                                _htmlOut = "<td align='center'>" + horarioDia.getHorarioSalidaDia() + "</td>";

                            //contabilizamos como dia trabajado solo si tiene la entrada y salida registrada
                            if(!horarioDia.getHorarioEntradaDia().equals("") && !horarioDia.getHorarioSalidaDia().equals("")){
                                sumaIndiceTrabajoDia = true;
                                tiempoTrabajado = DiferenciaDeHorasEnMinutos(horarioDia.getHorarioEntradaDia(), horarioDia.getHorarioSalidaDia());
                            }

                            //break; //que siga con el proximo dia
                        }
                    }

                    if(sumaIndiceTrabajoDia){
                        cantidadDiasTrabajoSem++;
                        tiempoTrabajadoSem += tiempoTrabajado;
                    }

                    if (hayInfoDelDia) {
                        htmlRowIn += _htmlIn;
                        htmlRowOut += _htmlOut;
                    }
                    else{//si no se encontró info del dia, tenemos que dejar los recuadros en blanco
                        htmlRowIn += "<td align='center'>&nbsp;</td>";
                        htmlRowOut += "<td align='center'>&nbsp;</td>";
                    }

                }

                //int cantidadDiasTrabajoSem = horarioUser.getHorarios().size() > 1 ? horarioUser.getHorarios().size()-1 : 1 ;


                //ultima celda
                //htmlRowIn += "<td align='center'>Mensual<br/>" + CalculaIndicadorMes(fechasMes.get(0), fechasMes.get(29), horarioUser.getUserid())+ "%</td>";
                //htmlRowOut += "<td align='center'>Semanal<br/>" + String.valueOf(cantidadDiasTrabajoSem == 0 ? 0 : (cantidadDiasCumplioHorario * 100) / cantidadDiasTrabajoSem) + "%</td>";
                htmlRowIn += "<td align='center'>Mensual<br/>" + CalculaIndicadorMes(fechasMes.get(0), fechasMes.get(29), horarioUser.getUserid())+ "%</td>";
                htmlRowOut += "<td align='center'>Semanal<br/>" + CalculaIndicador(tiempoTrabajadoSem,cantidadDiasTrabajoSem, horaEntradaHabitual, horaSalidaHabitual) + "%</td>";

                //Toast.makeText(ActivityRptHorarios.this, String.valueOf(tiempoTrabajadoSem) + " " + String.valueOf(cantidadDiasTrabajoSem) + " " + horaEntradaHabitual + " " + horaSalidaHabitual + " " + CalculaIndicador(tiempoTrabajadoSem,cantidadDiasTrabajoSem, horaEntradaHabitual, horaSalidaHabitual), Toast.LENGTH_LONG ).show();

                htmlBuilder.append(htmlRowIn + "</tr>");
                htmlBuilder.append("<tr>" + htmlRowOut + "</tr>");
                htmlBuilder.append(htmlRowDias);

            }


            htmlBuilder.append("</table>");

            htmlBuilder.append("</body></html>");

            //this.vw_rpt.loadData(htmlBuilder.toString(), "text/html", "utf-8");
            this.vw_rpt.loadDataWithBaseURL("file:///android_asset/", htmlBuilder.toString(), "text/html", "utf-8", null);
        } catch (Exception ex) {
            Toast.makeText(ActivityRptHorarios.this, "Error on GenerarReporteHTML: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

   /* private void GenerarReporteHTML_() {

        //fila con los dias
        String htmlRowDias = "<tr>";
        htmlRowDias += "<td style='border:none;'>&nbsp;</td>";
        for (HorarioRptItem horarioDia : dataReporte.get(0).getHorarios()) {
            htmlRowDias += "<td style='border:none;font-weight:normal;font-size:18px;padding:3px;' align='center'>";

            htmlRowDias += horarioDia.getDia();

            htmlRowDias += "</td>";
        }
        htmlRowDias += "<td style='border:none;'>&nbsp;</td>";
        htmlRowDias += "</tr>";

        StringBuilder htmlBuilder = new StringBuilder();

        htmlBuilder.append("<html><head><link href='styles.css' rel='stylesheet'></head><body>");

        //tabla del reporte
        htmlBuilder.append("<table width='100%' CELLSPACING='0' CELLPADDING='0'>");

        for (HorarioRpt horarioUser : dataReporte) {

            //fila por usuario
            htmlBuilder.append("<tr>");

            //imagen y nombre
            htmlBuilder.append("<td style='border:none;font-weight:normal;' align='center' rowspan='2'>");
            htmlBuilder.append("<img src='usuarios.png'/><br/>" + horarioUser.getUsername());
            htmlBuilder.append("</td>");

            //tabla horarios
            String horaEntradaHabitual = horarioUser.getHorarioEntradaHabitual();
            String horaSalidaHabitual = horarioUser.getHorarioSalidaHabitual();

            //FILA ENTRADA y SALIDA
            String htmlRowIn = "";
            String htmlRowOut = "";


            for (HorarioRptItem horarioDia : horarioUser.getHorarios()) {
                if (!horarioDia.getHorarioEntradaDia().equals(horaEntradaHabitual))//comparamos sino coinciden ponen color rojo
                    htmlRowIn += "<td style='color:red;' align='center'>" + horarioDia.getHorarioEntradaDia() + "</td>";
                else
                    htmlRowIn += "<td align='center'>" + horarioDia.getHorarioEntradaDia() + "</td>";

                if (!horarioDia.getHorarioSalidaDia().equals(horaSalidaHabitual))//comparamos sino coinciden ponen color rojo
                    htmlRowOut += "<td style='color:red;' align='center'>" + horarioDia.getHorarioSalidaDia() + "</td>";
                else
                    htmlRowOut += "<td align='center'>" + horarioDia.getHorarioSalidaDia() + "</td>";
            }

            //ultima celda
            htmlRowIn += "<td align='center'>Mensual<br/>78%</td>";
            htmlRowOut += "<td align='center'>Semanal<br/>112%</td>";

            htmlBuilder.append(htmlRowIn + "</tr>");
            htmlBuilder.append("<tr>" + htmlRowOut + "</tr>");
            htmlBuilder.append(htmlRowDias);
            //htmlBuilder.append("<tr><td style='border:none;' colspan='" + String.valueOf(horarioUser.getHorarios().size() + 2) + "'>&nbsp;</td></tr>");

            //htmlBuilder.append("</table></td>");
            //htmlBuilder.append("</tr>");
        }


        htmlBuilder.append("</table>");

        htmlBuilder.append("</body></html>");

        //this.vw_rpt.loadData(htmlBuilder.toString(), "text/html", "utf-8");
        this.vw_rpt.loadDataWithBaseURL("file:///android_asset/", htmlBuilder.toString(), "text/html", "utf-8", null);
    }

    private void GenerarReporteHTML__() {
        StringBuilder htmlBuilder = new StringBuilder();

        htmlBuilder.append("<html><body>");

        //tabla del reporte
        htmlBuilder.append("<table CELLSPACING='0' CELLPADDING='0'>");

        for (HorarioRpt horarioUser : dataReporte) {

            //buscamos la menor y maxima hora
            int horaMin = 24;
            int horaMax = 0;

            for (HorarioRptItem horarioDia : horarioUser.getHorarios()) {
                if (Integer.parseInt(horarioDia.getHorarioEntradaDia().substring(0, 2)) < horaMin)
                    horaMin = Integer.parseInt(horarioDia.getHorarioEntradaDia().substring(0, 2));

                if (Integer.parseInt(horarioDia.getHorarioSalidaDia().substring(0, 2)) > horaMax)
                    horaMax = Integer.parseInt(horarioDia.getHorarioSalidaDia().substring(0, 2));
            }

            //fila por usuario
            htmlBuilder.append("<tr>");
            //imagen y nombre
            htmlBuilder.append("<td align='center' rowspan='" + String.valueOf(horaMax - horaMin + 2) + "'>");
            htmlBuilder.append("<img src='usuarios.png'/><br/>" + horarioUser.getUsername());
            htmlBuilder.append("</td>");

            //tabla horarios

            boolean flagEntrada, flagSalida;

            //fila por hora
            for (int hora = horaMax; hora >= horaMin; hora--) {

                flagEntrada = (hora == Integer.parseInt(horarioUser.getHorarioEntradaHabitual().substring(0, 2)));
                flagSalida = (hora == Integer.parseInt(horarioUser.getHorarioSalidaHabitual().substring(0, 2)));

                htmlBuilder.append("<tr>");


                for (HorarioRptItem horarioDia : horarioUser.getHorarios()) {

                    boolean agregaRegla = (flagEntrada || flagSalida);

                    String regla = "<div style='height: 2px; background-color: blue; text-align: center'> </div>";
                    String marca = "<img src='circle.png' alt='X' style='position: relative; top: -0.5em;' />";
                    String marcaRegla = "<div style='height: 2px; background-color: blue; text-align: center'> <img src='circle.png' alt='X' style='position: relative; top: -0.5em;' /> </div>";

                    if (agregaRegla && hora == Integer.parseInt(horarioDia.getHorarioEntradaDia().substring(0, 2)))
                        htmlBuilder.append("<td align='center'>" + marcaRegla + "</td>");
                    else if (hora == Integer.parseInt(horarioDia.getHorarioEntradaDia().substring(0, 2)))
                        htmlBuilder.append("<td align='center'>" + marca + "</td>");
                    else if (agregaRegla && hora == Integer.parseInt(horarioDia.getHorarioSalidaDia().substring(0, 2)))
                        htmlBuilder.append("<td align='center'>" + marcaRegla + "</td>");
                    else if (hora == Integer.parseInt(horarioDia.getHorarioSalidaDia().substring(0, 2)))
                        htmlBuilder.append("<td align='center'>" + marca + "</td>");
                    else if (agregaRegla)
                        htmlBuilder.append("<td align='center'>" + regla + "</td>");
                    else
                        htmlBuilder.append("<td>&nbsp;</td>");

                }

                //ultima celda
                if (flagEntrada)
                    htmlBuilder.append("<td align='center'>" + horarioUser.getHorarioEntradaHabitual() + "</td>");
                else if (flagSalida)
                    htmlBuilder.append("<td align='center'>" + horarioUser.getHorarioSalidaHabitual() + "</td>");
                else
                    htmlBuilder.append("<td>&nbsp;</td>");

                htmlBuilder.append("</tr>");
            }


            // htmlBuilder.append("</table></td>");

            //htmlBuilder.append("</tr>");
        }

        //fila con los dias
        htmlBuilder.append("<tr>");
        htmlBuilder.append("<td>&nbsp;</td>");

        for (HorarioRptItem horarioDia : dataReporte.get(0).getHorarios()) {
            htmlBuilder.append("<td align='center'>");

            htmlBuilder.append(horarioDia.getDia());

            htmlBuilder.append("</td>");
        }

        htmlBuilder.append("<td>&nbsp;</td>");
        htmlBuilder.append("</tr>");

        htmlBuilder.append("</table>");

        htmlBuilder.append("</body></html>");

        //this.vw_rpt.loadData(htmlBuilder.toString(), "text/html", "utf-8");
        this.vw_rpt.loadDataWithBaseURL("file:///android_asset/", htmlBuilder.toString(), "text/html", "utf-8", null);
    }*/



/*
    private void ObtenerDataReporte() {

        dataReporte = new ArrayList<HorarioRpt>();

        HorarioRpt horarioRpt = new HorarioRpt();
        horarioRpt.setUsername("Reno");
        horarioRpt.setHorarioEntradaHabitual("09:00");
        horarioRpt.setHorarioSalidaHabitual("18:00");
        horarioRpt.getHorarios().add(new HorarioRptItem("01/01", "09:00", "19:00"));
        horarioRpt.getHorarios().add(new HorarioRptItem("02/01", "08:30", "18:10"));
        horarioRpt.getHorarios().add(new HorarioRptItem("05/01", "09:00", "20:00"));
        horarioRpt.getHorarios().add(new HorarioRptItem("06/01", "09:00", "15:00"));
        horarioRpt.getHorarios().add(new HorarioRptItem("09/01", "09:00", "15:00"));
        horarioRpt.getHorarios().add(new HorarioRptItem("10/01", "09:00", "18:00"));
        horarioRpt.getHorarios().add(new HorarioRptItem("12/01", "10:00", "16:00"));

        dataReporte.add(horarioRpt);

        horarioRpt = new HorarioRpt();
        horarioRpt.setUsername("Hernan");
        horarioRpt.setHorarioEntradaHabitual("09:00");
        horarioRpt.setHorarioSalidaHabitual("18:00");
        horarioRpt.getHorarios().add(new HorarioRptItem("01/01", "09:00", "19:00"));
        horarioRpt.getHorarios().add(new HorarioRptItem("02/01", "08:30", "18:10"));
        horarioRpt.getHorarios().add(new HorarioRptItem("05/01", "09:00", "20:00"));
        horarioRpt.getHorarios().add(new HorarioRptItem("06/01", "09:00", "15:00"));
        horarioRpt.getHorarios().add(new HorarioRptItem("09/01", "09:00", "16:00"));
        horarioRpt.getHorarios().add(new HorarioRptItem("10/01", "09:00", "18:00"));
        horarioRpt.getHorarios().add(new HorarioRptItem("12/01", "11:00", "16:00"));

        dataReporte.add(horarioRpt);

        horarioRpt = new HorarioRpt();
        horarioRpt.setUsername("Pia");
        horarioRpt.setHorarioEntradaHabitual("09:00");
        horarioRpt.setHorarioSalidaHabitual("18:00");
        horarioRpt.getHorarios().add(new HorarioRptItem("01/01", "09:00", "19:00"));
        horarioRpt.getHorarios().add(new HorarioRptItem("02/01", "08:30", "18:10"));
        horarioRpt.getHorarios().add(new HorarioRptItem("05/01", "09:00", "20:00"));
        horarioRpt.getHorarios().add(new HorarioRptItem("06/01", "09:00", "15:00"));
        horarioRpt.getHorarios().add(new HorarioRptItem("09/01", "09:00", "16:00"));
        horarioRpt.getHorarios().add(new HorarioRptItem("10/01", "09:00", "18:00"));
        horarioRpt.getHorarios().add(new HorarioRptItem("12/01", "11:00", "16:00"));

        dataReporte.add(horarioRpt);
    }
    */
}
