package com.pds.bip;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Caja;
import com.pds.common.Caja.TipoMovimientoCaja;
import com.pds.common.Config;
import com.pds.common.Formatos;
import com.pds.common.ItemMenu;
import com.pds.common.ItemMenuAdapter;
import com.pds.common.Logger;
import com.pds.common.Usuario;
import com.pds.common.Venta;
import com.pds.common.VentaDetalleHelper;
import com.pds.common.VentasHelper;
import com.pds.common.integration.android.ScanIntegrator;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android_serialport_api.Printer;
import android_serialport_api.Printer.ALINEACION;
import android_serialport_api.SerialPortActivity;


public class MainActivity extends Activity {

    Printer _printer;

    //controles del panel
    ImageView imgIcono_panel;
    TextView txtWelcome_panel;
    TextView txtComentario_panel;
    TextView txtFuncion_panel;
    TextView txtValor_panel;
    TextView txtPanel;
    TextView txtUser_panel;
    TextView txtHora_panel;
    TextView txtFecha_panel;

    //control de fechas
    BroadcastReceiver _broadcastReceiver;
    private ImageButton btnCalc;
    private ImageView dummy_view;
    private View bipView;
    private View bipStatusView;
    private TextView bipStatusMessageView;
    private Button btnImprimir;
    private Button btnReintentar;
    private Button btnVolver;

    //teclado
    Button btnCancelar;
    Button btnPrecio;
    Button btnEnter;
    ImageButton btnBackspace;
    Button btn0;
    Button btn1;
    Button btn2;
    Button btn3;
    Button btn4;
    Button btn5;
    Button btn6;
    Button btn7;
    Button btn8;
    Button btn9;
    Button btnPunto;

    Button btnLectura;

    //controles del popup de medio de pago
    Button btnConfirmar_mp;
    Button btnCancelar_mp;
    TextView txtTotal_mp;
    RadioGroup rbtMedios_mp;

    private Usuario _user;
    private int bipStep;
    private List<String> lineasLayout;

    private String CARGA_TARJETA;
    private String CARGA_MONTO;
    private String CARGA_MEDIOPAGO;
    private Date CARGA_FECHAHORA;
    private String inputIngresado = "";
    private boolean scanIniciado = false;

    private Config config;

    @Override
    public void onStart() {
        super.onStart();
        _broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().compareTo(Intent.ACTION_TIME_TICK) == 0) {
                    Date now = new Date();
                    ((TextView) findViewById(R.id.panel_txt_fecha)).setText(Formatos.FormateaDate(now, Formatos.DateFormat));
                    ((TextView) findViewById(R.id.panel_txt_hora)).setText(Formatos.FormateaDate(now, Formatos.ShortTimeFormat));
                }
            }
        };

        registerReceiver(_broadcastReceiver, new IntentFilter(Intent.ACTION_TIME_TICK));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {

            ((TextView) findViewById(R.id.txtVersion)).setText("Version:" + getPackageManager().getPackageInfo(this.getPackageName().toString(), 0).versionName);

            //logueo Usuario *****************************************************
            //obtenemos los parametros pasamos a la actividad
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                //tomamos el usuario
                this._user = (Usuario) extras.getSerializable("current_user");
            }

            //no tenemos user logueado...volvemos a EntryPoint
            if (_user == null) {
                Toast.makeText(this, R.string.unknown_user, Toast.LENGTH_LONG).show();
                this.finish();
            } else {

                //********* USER LOGUEADO OK *********

                //asignamos los view
                Init_AsignarViews();

                //asignamos los eventos
                Init_AsignarEventos();

                this.txtUser_panel.append(this._user.getNombre());

                this.txtWelcome_panel.setVisibility(View.INVISIBLE);
                this.imgIcono_panel.setImageResource(R.drawable.tarj_bip_min);
                this.imgIcono_panel.setVisibility(View.VISIBLE);

                this.btnCancelar.setText("CANCELAR");
                //this.btnCancelar.setBackgroundColor(getResources().getColor(R.color.boton_rojo));
                this.btnCancelar.setBackgroundResource(R.drawable.botones_main_izq_red);

                this.btnEnter.setBackgroundResource(R.drawable.botones_main_izq);

                this.btnPrecio.setEnabled(false);
                this.btnPrecio.setBackgroundColor(getResources().getColor(R.color.boton_negro));
                this.btnPrecio.setText("");

                this.bipStep = 0;
                Proceso();

            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume() {
        try {
            super.onResume();

            config = new Config(this);

            this.txtFecha_panel.setText(Formatos.FormateaDate(new Date(), Formatos.DateFormat));
            this.txtHora_panel.setText(Formatos.FormateaDate(new Date(), Formatos.ShortTimeFormat));

        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    /*
        @Override
        protected void onDataReceived(final byte[] buffer, final int size) {
            runOnUiThread(new Runnable() {
                public void run() {
                    //leemos la data recibida
                    String tString = new String(buffer, 0, size);

                    String mensajeError = mSerialPort.EsError(tString);

                    //vemos si coincide con un mensaje de error conocido
                    if (mensajeError != null)
                        Toast.makeText(MainActivity.this, mensajeError, Toast.LENGTH_LONG).show();
                    else
                        Toast.makeText(MainActivity.this, "Recibido:" + "\n" + tString, Toast.LENGTH_LONG).show();

                }
            });
        }
    */
    @Override
    public void onStop() {
        super.onStop();

        if (_broadcastReceiver != null)
            unregisterReceiver(_broadcastReceiver);
    }

    @Override
    protected void onDestroy() {
        if (_printer != null)
            _printer.end();

        super.onDestroy();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        try {

            super.onActivityResult(requestCode, resultCode, intent);

            if (scanIniciado) {
                if (intent != null) {

                    //obtenemos el resultado del scan
                    String scanContent = ScanIntegrator.parseActivityResult(requestCode, resultCode, intent);
                    //String scanContent = "555351313";
                    //revisamos si es un valor valido
                    if (scanContent != null) {
                        //lo asignamos
                        this.txtValor_panel.setText(scanContent);
                        this.txtValor_panel.setVisibility(View.VISIBLE);

                        scanIniciado = false;

                        btnEnter.performClick();

                    } else {
                        //datos invalidos o scan cancelado
                        Toast.makeText(MainActivity.this,
                                "No se recibieron datos del scan!", Toast.LENGTH_SHORT).show();

                    }

                } else {
                    //datos invalidos o scan cancelado
                    Toast.makeText(MainActivity.this,
                            "No se recibieron datos del scan!", Toast.LENGTH_SHORT).show();
                }

                scanIniciado = false;
            }
        } catch (Exception ex) {
            Toast.makeText(MainActivity.this,
                    "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {

        if (event.isPrintingKey() || keyCode == KeyEvent.KEYCODE_ENTER) {

            if (keyCode == KeyEvent.KEYCODE_ENTER) {

                txtValor_panel.setText(inputIngresado);

                inputIngresado = "";

                btnEnter.performClick();

            } else {
                inputIngresado += String.valueOf(event.getDisplayLabel());
            }

            return true;

        } else {
            return super.onKeyUp(keyCode, event);
        }
    }

    private void Init_AsignarViews() {

        this.btnCalc = (ImageButton) findViewById(R.id.bip_btnCalc);

        this.dummy_view = (ImageView) findViewById(R.id.bip_dummy);

        this.bipView = findViewById(R.id.bip_main_layout);
        this.bipStatusView = findViewById(R.id.bip_status);
        this.bipStatusMessageView = (TextView) findViewById(R.id.bip_status_message);

        this.txtPanel = (TextView) findViewById(R.id.panel_txt_valor);

        this.btnImprimir = (Button) findViewById(R.id.bip_btn_imprimir);
        this.btnReintentar = (Button) findViewById(R.id.bip_btn_reintentar);
        this.btnVolver = (Button) findViewById(R.id.bip_btn_volver);

        this.btnLectura = (Button) findViewById(R.id.bip_btnScan);

        //controles del pad
        this.btnCancelar = (Button) findViewById(R.id.pad_btnCantidad);
        this.btnPrecio = (Button) findViewById(R.id.pad_btnPrecio);
        this.btnEnter = (Button) findViewById(R.id.pad_btnEnter);
        this.btnBackspace = (ImageButton) findViewById(R.id.pad_btnClear);
        this.btn0 = (Button) findViewById(R.id.pad_btnNro0);
        this.btn1 = (Button) findViewById(R.id.pad_btnNro1);
        this.btn2 = (Button) findViewById(R.id.pad_btnNro2);
        this.btn3 = (Button) findViewById(R.id.pad_btnNro3);
        this.btn4 = (Button) findViewById(R.id.pad_btnNro4);
        this.btn5 = (Button) findViewById(R.id.pad_btnNro5);
        this.btn6 = (Button) findViewById(R.id.pad_btnNro6);
        this.btn7 = (Button) findViewById(R.id.pad_btnNro7);
        this.btn8 = (Button) findViewById(R.id.pad_btnNro8);
        this.btn9 = (Button) findViewById(R.id.pad_btnNro9);
        this.btnPunto = (Button) findViewById(R.id.pad_btnPunto);

        //controles del panel
        this.imgIcono_panel = (ImageView) findViewById(R.id.panel_img_icono);
        this.txtWelcome_panel = (TextView) findViewById(R.id.panel_txt_welcome);
        this.txtUser_panel = (TextView) findViewById(R.id.panel_txt_user);
        this.txtComentario_panel = (TextView) findViewById(R.id.panel_txt_comentario);
        this.txtFuncion_panel = (TextView) findViewById(R.id.panel_txt_funcion);
        this.txtValor_panel = (TextView) findViewById(R.id.panel_txt_valor);
        this.txtFecha_panel = (TextView) findViewById(R.id.panel_txt_fecha);
        this.txtHora_panel = (TextView) findViewById(R.id.panel_txt_hora);

        //controles del popup medio pago
        this.btnConfirmar_mp = (Button) findViewById(R.id.medio_pago_btn_aceptar);
        this.btnCancelar_mp = (Button) findViewById(R.id.medio_pago_btn_cancelar);
        this.txtTotal_mp = (TextView) findViewById(R.id.medio_pago_txt_total);
        this.rbtMedios_mp = (RadioGroup) findViewById(R.id.medio_pago_medios);
    }

    private void Init_AsignarEventos() {

        this.btnBackspace.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                String texto = txtPanel.getText().toString();

                if (!texto.isEmpty()) {
                    txtPanel.setText(texto.substring(0, texto.length() - 1));
                }
            }
        });

        this.btnBackspace.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                txtPanel.setText("");

                return false;
            }
        });

        //APERTURA DE CALCULADORA
        btnCalc.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_MAIN);
                i.setComponent(new ComponentName("com.android.calculator2", "com.android.calculator2.Calculator"));


                try {
                    MainActivity.this.startActivity(i);
                } catch (ActivityNotFoundException noSuchActivity) {
                    // si el calculator intent no está registrado, probamos con un nombre alternativo
                    Intent i2 = new Intent();
                    i2.setAction(Intent.ACTION_MAIN);
                    i2.setComponent(new ComponentName("com.sec.android.app.popupcalculator", "com.sec.android.app.popupcalculator.Calculator"));
                    try {
                        MainActivity.this.startActivity(i2);
                    } catch (ActivityNotFoundException noSuchActivity2) {
                        // calculator intent not found
                        Toast.makeText(MainActivity.this, "Acceso a calculadora no encontrado", Toast.LENGTH_LONG).show();
                    }
                }

            }
        });

        this.btnEnter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bipStep > 0)
                    Proceso();
                else
                    btnCancelar.performClick();
            }
        });

        this.btnLectura.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bipStep == 2) {
                    //btnEnter.performClick();
                    try {
                        //instanciamos la clase de integracion con el scan
                        ScanIntegrator scanIntegrator = new ScanIntegrator(MainActivity.this);
                        //iniciamos scan
                        scanIniciado = true;
                        scanIntegrator.initiateScan();

                    } catch (Exception e) {
                        Toast.makeText(MainActivity.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else
                    Toast.makeText(MainActivity.this, "Funcion no valida en este momento", Toast.LENGTH_SHORT).show();
            }
        });

        this.btnImprimir.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    ImprimeTicketCarga();
                } catch (Exception ex) {
                    Toast.makeText(MainActivity.this, "Error al imprimir: " + ex.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });

        this.btnReintentar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                btnEnter.performClick();
            }
        });

        this.btn0.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("0");
            }
        });

        this.btn1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("1");
            }
        });

        this.btn2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("2");
            }
        });

        this.btn3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("3");
            }
        });

        this.btn4.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("4");
            }
        });

        this.btn5.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("5");
            }
        });

        this.btn6.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("6");
            }
        });

        this.btn7.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("7");
            }
        });

        this.btn8.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("8");
            }
        });

        this.btn9.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("9");
            }
        });

        this.btnPunto.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel(".");
            }
        });


        this.btnCancelar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Cancelar();
            }
        });

        this.btnVolver.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Cancelar();
            }
        });

        this.dummy_view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                dummy_view.setBackgroundColor(Color.TRANSPARENT);
                dummy_view.setVisibility(View.INVISIBLE);
                (findViewById(R.id.bip_medio_pago)).setVisibility(View.INVISIBLE);
            }
        });

        (findViewById(R.id.bip_medio_pago)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //dejamos el evento click seteado en la vista para que no se vaya al click del dummyview
            }
        });

        this.btnConfirmar_mp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                dummy_view.setBackgroundColor(Color.TRANSPARENT);
                dummy_view.setVisibility(View.INVISIBLE);
                findViewById(R.id.bip_medio_pago).setVisibility(View.INVISIBLE);

                if (bipStep > 0) {
                    CARGA_MEDIOPAGO = ((RadioButton) rbtMedios_mp.findViewById(rbtMedios_mp.getCheckedRadioButtonId())).getText().toString();
                    //registramos el cobro
                    bipStep = 5;
                    Proceso();
                }
            }
        });

        this.btnCancelar_mp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                dummy_view.setBackgroundColor(Color.TRANSPARENT);
                dummy_view.setVisibility(View.INVISIBLE);
                findViewById(R.id.bip_medio_pago).setVisibility(View.INVISIBLE);
            }
        });
    }

    private void ImprimeTicketCarga() throws Exception {
        try {

            if (_printer == null)
                _printer = Printer.getInstance(config.PRINTER_MODEL);

            if (!_printer.initialize()) {

                Toast.makeText(this, "Error al inicializar", Toast.LENGTH_SHORT).show();

                _printer.end();
            } else {
                _printer.lineFeed();

                _printer.format(2, 2, ALINEACION.CENTER);
                _printer.printLine("TARJETA BIP");
                _printer.cancelCurrentFormat();
                _printer.sendSeparadorHorizontal();

                _printer.printLine("Tarjeta #    : " + CARGA_TARJETA);
                _printer.printLine("Monto        : $ " + Formatos.FormateaDecimal(CARGA_MONTO, Formatos.DecimalFormat));
                _printer.printLine("Fecha y Hora : " + Formatos.FormateaDate(CARGA_FECHAHORA, Formatos.FullDateTimeFormatNoSeconds));

                _printer.sendSeparadorHorizontal();

                _printer.printLine("Recuerde que debera presentar   ");
                _printer.printLine("la tarjeta frente a una antena  ");
                _printer.printLine("del METRO                       ");

                _printer.sendSeparadorHorizontal();

                _printer.footer();

            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    private void RegistrarVenta() {

        try {

            //obtenemos el medio de pago seleccionado

            Venta venta = new Venta(
                    0,
                    Double.valueOf(CARGA_MONTO),
                    1,
                    Formatos.FormateaDate(CARGA_FECHAHORA, Formatos.DateFormat),
                    Formatos.FormateaDate(CARGA_FECHAHORA, Formatos.TimeFormat),
                    "BIP",
                    this._user.getId(),
                    CARGA_MEDIOPAGO
            );

            //VENTA
            ContentValues cvVenta = new ContentValues();

            cvVenta.put(VentasHelper.VENTA_TOTAL, venta.getTotal());
            cvVenta.put(VentasHelper.VENTA_FECHA, venta.getFecha());
            cvVenta.put(VentasHelper.VENTA_HORA, venta.getHora());
            cvVenta.put(VentasHelper.VENTA_MEDIO_PAGO, venta.getMedio_pago());
            cvVenta.put(VentasHelper.VENTA_CODIGO, venta.getCodigo());
            cvVenta.put(VentasHelper.VENTA_CANTIDAD, venta.getCantidad());
            cvVenta.put(VentasHelper.VENTA_USERID, venta.getUserid());

            Uri newVentaUri = getContentResolver().insert(Uri.parse("content://com.pds.ficle.ep.ventas.contentprovider/ventas"), cvVenta);

            venta.setId(Integer.valueOf(newVentaUri.getPathSegments().get(1)));

            //DETALLES
            ContentValues cvDetalle = new ContentValues();

            cvDetalle.put(VentaDetalleHelper.VENTA_DET_VENTA_ID, venta.getId());
            cvDetalle.put(VentaDetalleHelper.VENTA_DET_PRODUCTO_ID, 1);
            cvDetalle.put(VentaDetalleHelper.VENTA_DET_CANTIDAD, venta.getCantidad());
            cvDetalle.put(VentaDetalleHelper.VENTA_DET_PRECIO, venta.getTotal());
            cvDetalle.put(VentaDetalleHelper.VENTA_DET_TOTAL, venta.getTotal());

            Uri newVentaDetUri = getContentResolver().insert(Uri.parse("content://com.pds.ficle.ep.venta_detalles.contentprovider/venta_detalles"), cvDetalle);

            //enviamos a caja todos los cobros, independiente del medio de pago
            Caja.RegistrarMovimientoCaja(MainActivity.this, TipoMovimientoCaja.DEPOSITO, Formatos.FormateaDecimal(venta.getTotal(), Formatos.DecimalFormat_US), venta.getMedio_pago(), "", venta.getId());

            Logger.RegistrarEvento(this, "i", "CARGA BIP #" + venta.getNroVenta(), "Total: " + Formatos.FormateaDecimal(venta.getTotal(), Formatos.DecimalFormat, "$"), "Medio Pago: " + venta.getMedio_pago());

        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), "Se ha producido un error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void Proceso() {

        String header = "TARJETA BIP";

        switch (bipStep) {
            case 0: {

                this.txtComentario_panel.setText("TARJETA BIP");
                this.txtComentario_panel.setVisibility(View.VISIBLE);

                this.lineasLayout = null;

                EscribeLinea("Ingrese Monto:", header);

                this.btnVolver.setVisibility(View.VISIBLE);

                bipStep = 1;

            }
            break;
            case 1: {

                String montoIngresado = txtValor_panel.getText().toString().trim();

                //validamos el monto ingresado
                if (montoIngresado == null || montoIngresado.equals("")) {
                    Toast.makeText(MainActivity.this, "Debe ingresar un monto a cargar para continuar", Toast.LENGTH_LONG).show();
                } else if (montoIngresado.contains(".")) {
                    Toast.makeText(MainActivity.this, "El monto a ingresar debe ser entero", Toast.LENGTH_LONG).show();
                } else if (montoIngresado.length() > 5) {
                    Toast.makeText(MainActivity.this, "El monto a ingresar no puede superar $ 99999", Toast.LENGTH_LONG).show();
                } else {

                    CARGA_MONTO = montoIngresado;
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    EscribeLinea("MONTO:", header);
                    EscribeLinea("\t" + Formatos.FormateaDecimal(montoIngresado, Formatos.DecimalFormat, "$"), header);
                    EscribeLinea("PRESENTE TARJETA Y", header);
                    EscribeLinea("\t" + " PRESIONE LECTURA...", header);

                    txtValor_panel.setText("");

                    bipStep = 2;
                }
            }
            break;
            case 2: {

                String numeroIngresado = txtValor_panel.getText().toString();
                //String numeroIngresado = "452663";

                //validamos el numero ingresado
                if (numeroIngresado.equals("")) {
                    Toast.makeText(MainActivity.this, "Debe ingresar un numero de tarjeta para continuar", Toast.LENGTH_LONG).show();
                } else if (numeroIngresado.contains(".")) {
                    Toast.makeText(MainActivity.this, "El numero solo debe tener digitos numericos", Toast.LENGTH_LONG).show();
                } else {

                    CARGA_TARJETA = numeroIngresado;

                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);

                    EscribeLinea("TARJETA:");
                    EscribeLinea("\t" + "#" + CARGA_TARJETA);
                    txtValor_panel.setText("");

                    EscribeLinea("VERIFIQUE DATOS Y VALIDE...", header);

                    this.btnEnter.setText("VALIDAR");
                    this.btnEnter.setTextSize(30);

                    bipStep = 3;
                }
            }
            break;
            case 3: {

                this.btnEnter.setText("OK");
                this.btnEnter.setTextSize(40);

                EnviarCarga();
            }
            break;
            case 4: {
                dummy_view.setBackgroundColor(getResources().getColor(R.color.blur));
                dummy_view.setVisibility(View.VISIBLE);
                findViewById(R.id.bip_medio_pago).setVisibility(View.VISIBLE);
                txtTotal_mp.setText(Formatos.FormateaDecimal(CARGA_MONTO, Formatos.DecimalFormat, "$"));

                //seteamos por default en EFECTIVO
                rbtMedios_mp.check(R.id.medio_pago_rbt_efvo);
            }
            break;
            case 5: {

                RegistrarVenta();
                //GeneraTicketCarga();

                //this.btnVolver.setVisibility(View.VISIBLE);
                this.btnImprimir.setVisibility(View.VISIBLE);

                bipStep = 6;
            }
            break;
            default:
                btnCancelar.performClick();
                break;
        }

    }

    private void Cancelar() {

        if (bipStep > 0) {
            switch (bipStep) {
                case 1: {
                    //volvemos al inicio
                    bipStep = 0;
                    btnCancelar.performClick();
                }
                break;
                case 2: {

                    //volvemos al inicio
                    bipStep = 0;
                    btnCancelar.performClick();

                }
                break;
                case 3: {
                    this.btnEnter.setText("OK");
                    this.btnEnter.setTextSize(40);

                    bipStep = 1;

                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);

                    this.txtValor_panel.setText(CARGA_MONTO);

                    btnEnter.performClick();
                }
                break;
                case 4: {
                    //volver al VERIFIQUE DATOS y VALIDE
                    bipStep = 2;

                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);

                    this.txtValor_panel.setText(CARGA_TARJETA);

                    btnEnter.performClick();
                }
                break;
                case 6: {
                    //volvemos al inicio
                    bipStep = 0;
                    btnCancelar.performClick();
                }
                break;
            }
        } else {

            //ocultamos el layout de lineas
            //(findViewById(R.id.bip_lineas)).setVisibility(View.GONE);
            this.btnVolver.setVisibility(View.GONE);
            this.btnReintentar.setVisibility(View.GONE);
            this.btnImprimir.setVisibility(View.GONE);
            this.txtComentario_panel.setText("");
            this.txtComentario_panel.setVisibility(View.INVISIBLE);

            this.txtPanel.setText("");

            this.btnEnter.setText("OK");
            this.btnEnter.setTextSize(40);

            bipStep = 0;
            Proceso();
        }

    }

    public void EnviarCargaResponse() {

        this.bipStep = 4;
        String header = "TARJETA BIP";

        this.lineasLayout.remove(this.lineasLayout.size() - 1);

        CARGA_FECHAHORA = new Date();

        EscribeLinea("@C@@B@" + "TRANSACCION APROBADA", header);

        EscribeLinea("@C@La tarjeta debe ser presentada", header);
        EscribeLinea("@C@ ante una antena del METRO", header);

        //this.btnEnter.setText("CONTINUAR");
        //this.btnEnter.setTextSize(30);

        this.btnEnter.performClick();
    }

    private void AgregarTextoPanel(String texto) {
        this.txtPanel.setText(this.txtPanel.getText().toString() + texto);
        this.txtPanel.setVisibility(View.VISIBLE);
    }

    private void EscribeLinea(String linea) {
        EscribeLinea(linea, "");
    }

    private void EscribeLinea(String linea, String titulo) {

        if (this.lineasLayout == null)
            this.lineasLayout = new ArrayList<String>();

        this.lineasLayout.add(linea);

        EscribeLineas(this.lineasLayout, titulo);
    }

    private void EscribeLineas(List<String> lineas) {
        EscribeLineas(lineas, "");
    }

    private void EscribeLineas(List<String> lineas, String titulo) {

        //ocultamos el menu y la lista de resultados
        (findViewById(R.id.bip_lstMenu)).setVisibility(View.GONE);
        //mostramos el layout de lineas
        (findViewById(R.id.bip_lineas)).setVisibility(View.VISIBLE);

        if (!titulo.equals("")) {
            TextView txtTitulo = (TextView) findViewById(R.id.bip_txt_titulo);
            txtTitulo.setVisibility(View.VISIBLE);
            txtTitulo.setText(titulo);
        } else {
            TextView txtTitulo = (TextView) findViewById(R.id.bip_txt_titulo);
            txtTitulo.setVisibility(View.GONE);
            txtTitulo.setText("");
        }

        //blanqueamos
        ((TextView) findViewById(R.id.bip_txt1)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.bip_txt2)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.bip_txt3)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.bip_txt4)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.bip_txt5)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.bip_txt6)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.bip_txt7)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.bip_txt8)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.bip_txt9)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.bip_txt10)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.bip_txt11)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.bip_txt12)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.bip_txt13)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.bip_txt14)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.bip_txt15)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.bip_txt16)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.bip_txt17)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.bip_txt18)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.bip_txt19)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.bip_txt20)).setVisibility(View.GONE);

        for (int i = 0; i < lineas.size(); i++) {

            TextView txtLinea = null;

            switch (i + 1) {
                case 1:
                    txtLinea = (TextView) findViewById(R.id.bip_txt1);
                    break;
                case 2:
                    txtLinea = (TextView) findViewById(R.id.bip_txt2);
                    break;
                case 3:
                    txtLinea = (TextView) findViewById(R.id.bip_txt3);
                    break;
                case 4:
                    txtLinea = (TextView) findViewById(R.id.bip_txt4);
                    break;
                case 5:
                    txtLinea = (TextView) findViewById(R.id.bip_txt5);
                    break;
                case 6:
                    txtLinea = (TextView) findViewById(R.id.bip_txt6);
                    break;
                case 7:
                    txtLinea = (TextView) findViewById(R.id.bip_txt7);
                    break;
                case 8:
                    txtLinea = (TextView) findViewById(R.id.bip_txt8);
                    break;
                case 9:
                    txtLinea = (TextView) findViewById(R.id.bip_txt9);
                    break;
                case 10:
                    txtLinea = (TextView) findViewById(R.id.bip_txt10);
                    break;
                case 11:
                    txtLinea = (TextView) findViewById(R.id.bip_txt11);
                    break;
                case 12:
                    txtLinea = (TextView) findViewById(R.id.bip_txt12);
                    break;
                case 13:
                    txtLinea = (TextView) findViewById(R.id.bip_txt13);
                    break;
                case 14:
                    txtLinea = (TextView) findViewById(R.id.bip_txt14);
                    break;
                case 15:
                    txtLinea = (TextView) findViewById(R.id.bip_txt15);
                    break;
                case 16:
                    txtLinea = (TextView) findViewById(R.id.bip_txt16);
                    break;
                case 17:
                    txtLinea = (TextView) findViewById(R.id.bip_txt17);
                    break;
                case 18:
                    txtLinea = (TextView) findViewById(R.id.bip_txt18);
                    break;
                case 19:
                    txtLinea = (TextView) findViewById(R.id.bip_txt19);
                    break;
                case 20:
                    txtLinea = (TextView) findViewById(R.id.bip_txt20);
                    break;

            }

            String texto = lineas.get(i);
            if (texto.startsWith("@C@")) {
                texto = texto.replace("@C@", "");
                txtLinea.setGravity(Gravity.CENTER);
            } else {
                txtLinea.setGravity(Gravity.LEFT);
            }
            if (texto.startsWith("@B@")) {
                texto = texto.replace("@B@", "");
                txtLinea.setTypeface(null, Typeface.BOLD);
            } else {
                txtLinea.setTypeface(null, Typeface.NORMAL);
            }

            txtLinea.setText(texto);
            txtLinea.setVisibility(View.VISIBLE);
        }

        //envia las lineas al fondo para visualizar siempre lo ultimo
        ((ScrollView) findViewById(R.id.scrollView)).post(new Runnable() {
            public void run() {
                ((ScrollView) findViewById(R.id.scrollView)).fullScroll(View.FOCUS_DOWN);
            }
        });
    }

    public void EnviarCarga() {
        try {

            if (!isOnline()) {
                Toast.makeText(this, "Error: No hay una conexion disponible. Intente mas tarde.", Toast.LENGTH_LONG).show();
            } else {
                new ExecuteRequestTask("EnviarCargaResponse", "").execute();
            }

        } catch (Exception ex) {
            Toast.makeText(this, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    //tiene conectividad?
    public boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        return (networkInfo != null && networkInfo.isConnected());

        //si queres saber que tipo de conexion está activa
        //NetworkInfo networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        //boolean isWifiConn = networkInfo.isConnected();
        //networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        //boolean isMobileConn = networkInfo.isConnected();
    }


    private class ExecuteRequestTask extends AsyncTask<String, Void, String> {

        private String nextAction = "";
        private String paramsNextAction = "";

        //constructor
        public ExecuteRequestTask(String NextAction, String ParamsNextAction) {
            this.nextAction = NextAction;
            this.paramsNextAction = ParamsNextAction;
        }

        // onPreExecute used to setup the AsyncTask.
        @Override
        protected void onPreExecute() {
            showProgress(true);
            bipStatusMessageView.setText("Consultando servidor...");
        }

        @Override
        protected String doInBackground(String... messages) {

            // params comes from the execute() call: params[0] is the message.
            try {
                // Simulate network access.
                Thread.sleep(2000);

                publishProgress();

                return "";

                //} catch (IOException e) {
                //    return "No es posible conectarse. Intente mas tarde.";
            } catch (Exception e) {
                return "Error: " + e.getMessage();
            }
        }

        @Override
        protected void onProgressUpdate(Void... v) {
            bipStatusMessageView.setText("Leyendo respuesta...");
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            try {
                showProgress(false);

                if (result.equals("") && !this.nextAction.equals("")) {

                    if (!this.paramsNextAction.equals("")) {
                        //si tiene param
                        Method accion = MainActivity.this.getClass().getMethod(this.nextAction, this.paramsNextAction.getClass());

                        accion.invoke(MainActivity.this, this.paramsNextAction);
                    } else {
                        //sino tiene param
                        Method accion = MainActivity.this.getClass().getMethod(this.nextAction, null);

                        accion.invoke(MainActivity.this, null);
                    }


                } else {
                    Toast.makeText(MainActivity.this, result.toString(), Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {
                Toast.makeText(MainActivity.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }


        @Override
        protected void onCancelled() {
            showProgress(false);
        }
    }

    /**
     * Shows the progress UI and hides the main layout.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            bipStatusView.setVisibility(View.VISIBLE);
            bipStatusView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(show ? 1 : 0)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            bipStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
                        }
                    });

            bipView.setVisibility(View.VISIBLE);
            bipView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(show ? 0 : 1)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            bipView.setVisibility(show ? View.GONE : View.VISIBLE);
                        }
                    });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            bipStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
            bipView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
