package com.pds.servipag;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Caja;
import com.pds.common.Caja.TipoMovimientoCaja;
import com.pds.common.Config;
import com.pds.common.Formatos;
import com.pds.common.ItemMenu;
import com.pds.common.ItemMenuAdapter;
import com.pds.common.Logger;
import com.pds.common.Usuario;
import com.pds.common.Venta;
import com.pds.common.Venta.TipoMedioPago;
import com.pds.common.VentaDetalleHelper;
import com.pds.common.VentasHelper;
import com.pds.common.integration.android.ScanIntegrator;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android_serialport_api.Printer;
import android_serialport_api.Printer.ALINEACION;
import android_serialport_api.SerialPortX;
import android_serialport_api.SerialPortActivity;

public class ServipagActivityMain extends Activity {

    private Printer _printer;
    private Config config;

    // declaro varibles de controles
    private ListView lstMenu;
    private List<ItemMenu> menuServipagList;
    private ItemMenuAdapter menuAdapter;
    private Usuario _user;
    private View _servipagStatusView;
    private TextView _servipagStatusMessageView;
    private View _servipagView;
    ImageView dummy_view;
    private List<String> lineasLayout;
    private int depositoStep;
    private int pagoServicioStep;
    private ListView servipag_lstResultados;
    private ImageButton btnCalc;
    private Button btnVolver;

    //controles del panel
    ImageView imgIcono_panel;
    TextView txtWelcome_panel;
    TextView txtComentario_panel;
    TextView txtFuncion_panel;
    TextView txtValor_panel;
    TextView txtPanel;
    TextView txtUser_panel;
    TextView txtHora_panel;
    TextView txtFecha_panel;

    //teclado
    Button btnScan;
    Button btnCancelar;
    Button btnPrecio;
    Button btnEnter;
    ImageButton btnBackspace;
    Button btn0;
    Button btn1;
    Button btn2;
    Button btn3;
    Button btn4;
    Button btn5;
    Button btn6;
    Button btn7;
    Button btn8;
    Button btn9;
    Button btnPunto;

    //controles del popup de medio de pago
    Button btnConfirmar_mp;
    Button btnCancelar_mp;
    TextView txtTotal_mp;
    RadioGroup rbtMedios_mp;


    //control de fechas
    BroadcastReceiver _broadcastReceiver;

    //variables del deposito
    private String DEPOSITO_MONTO;
    private String DEPOSITO_BANCO;
    private String DEPOSITO_RUT;
    private String DEPOSITO_CUENTA;

    //variables del pago de servicio
    private String PAGO_SERV_COD_SERV;
    private String PAGO_SERV_MEDIO_PAGO;
    private String PAGO_SERV_RUT;
    private String PAGO_SERV_MONTO;
    private String PAGO_SERV_DEUDA_MONTO;
    private String PAGO_SERV_DEUDA_PAGOMIN;
    private String PAGO_SERV_DEUDA_PAGOMAX;

    @Override
    public void onStart() {
        super.onStart();
        _broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().compareTo(Intent.ACTION_TIME_TICK) == 0) {
                    Date now = new Date();
                    ((TextView) findViewById(R.id.panel_txt_fecha)).setText(Formatos.FormateaDate(now, Formatos.DateFormat));
                    ((TextView) findViewById(R.id.panel_txt_hora)).setText(Formatos.FormateaDate(now, Formatos.ShortTimeFormat));
                }
            }
        };

        registerReceiver(_broadcastReceiver, new IntentFilter(Intent.ACTION_TIME_TICK));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.servipag_activity_main);
        try {
            ((TextView) findViewById(R.id.txtVersion)).setText("Version:" + getPackageManager().getPackageInfo(this.getPackageName().toString(), 0).versionName);


            //logueo Usuario *****************************************************
            //obtenemos los parametros pasamos a la actividad
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                //tomamos el id de usuario
                //_id_user_login = extras.getInt("_id_user_login");
                this._user = (Usuario) extras.getSerializable("current_user");
            }

            //no tenemos user logueado...volvemos a EntryPoint
            if (_user == null) {
                Toast.makeText(this, R.string.unknown_user, Toast.LENGTH_LONG).show();
                this.finish();
            } else {
                //********* USER LOGUEADO OK *********

                //asignamos los view
                Init_AsignarViews();

                //asignamos los eventos
                Init_AsignarEventos();

                //cargamos el menu
                GeneraMenu();

                //this.txtUser_panel.append("admin");
                this.txtUser_panel.append(this._user.getNombre());

                //this.txtWelcome_panel.setText("SERVIPAG EXPRESS");
                this.txtWelcome_panel.setVisibility(View.INVISIBLE);
                this.imgIcono_panel.setImageResource(R.drawable.servipag_min);
                this.imgIcono_panel.setVisibility(View.VISIBLE);

                this.btnCancelar.setText("CANCELAR");
                this.btnCancelar.setBackgroundResource(R.drawable.botones_main_izq_red);

                this.btnEnter.setBackgroundResource(R.drawable.botones_main_izq);

                this.btnPrecio.setEnabled(false);
                this.btnPrecio.setBackgroundColor(getResources().getColor(R.color.boton_negro));
                this.btnPrecio.setText("");
            }

        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        GeneraMenu();

        this.config = new Config(this);

        this.txtFecha_panel.setText(Formatos.FormateaDate(new Date(), Formatos.DateFormat));
        this.txtHora_panel.setText(Formatos.FormateaDate(new Date(), Formatos.ShortTimeFormat));

        //blanqueamos los steps
        //depositoStep=0;
        //pagoServicioStep=0;
    }

    /*
    @Override
    protected void onDataReceived(final byte[] buffer, final int size) {
        runOnUiThread(new Runnable() {
            public void run() {
                //leemos la data recibida
                String tString = new String(buffer, 0, size);

                String mensajeError = mSerialPort.EsError(tString);

                //vemos si coincide con un mensaje de error conocido
                if (mensajeError != null)
                    Toast.makeText(ServipagActivityMain.this, mensajeError, Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(ServipagActivityMain.this, "Recibido:" + "\n" + tString, Toast.LENGTH_LONG).show();

            }
        });
    }
    */

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        try {

            super.onActivityResult(requestCode, resultCode, intent);

            if (intent != null) {

                //obtenemos el resultado del scan
                String scanContent = ScanIntegrator.parseActivityResult(requestCode, resultCode, intent);

                //revisamos si es un valor valido
                if (scanContent != null) {
                    //lo asignamos
                    this.txtValor_panel.setText(scanContent);
                    this.txtValor_panel.setVisibility(View.VISIBLE);

                    btnEnter.performClick();

                } else {

                    //datos invalidos o scan cancelado
                    Toast.makeText(getApplicationContext(),
                            "No se recibieron datos del scan!", Toast.LENGTH_SHORT).show();

                    this.txtFuncion_panel.setVisibility(View.INVISIBLE);

                }

            }


        } catch (Exception ex) {
            Toast.makeText(ServipagActivityMain.this,
                    "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (_broadcastReceiver != null)
            unregisterReceiver(_broadcastReceiver);
    }

    @Override
    protected void onDestroy() {
        if (_printer != null)
            _printer.end();

        super.onDestroy();
    }

    private void Init_AsignarViews() {

        this.btnCalc = (ImageButton) findViewById(R.id.servipag_btnCalc);

        this.dummy_view = (ImageView) findViewById(R.id.servipag_dummy);

        this._servipagView = findViewById(R.id.servipag_main_layout);
        this._servipagStatusView = findViewById(R.id.servipag_status);
        this._servipagStatusMessageView = (TextView) findViewById(R.id.servipag_status_message);

        this.txtPanel = (TextView) findViewById(R.id.panel_txt_valor);

        this.lstMenu = (ListView) findViewById(R.id.servipag_lstMenu);

        this.servipag_lstResultados = (ListView) findViewById(R.id.servipag_lstResultados);
        //this.recarga_btnImprimirResultados = (Button) findViewById(R.id.recarga_btn_imprimir);
        this.btnVolver = (Button) findViewById(R.id.servipag_btn_volver);

        //controles del pad
        this.btnScan = (Button) findViewById(R.id.servipag_btnScan);
        this.btnCancelar = (Button) findViewById(R.id.pad_btnCantidad);
        this.btnPrecio = (Button) findViewById(R.id.pad_btnPrecio);
        this.btnEnter = (Button) findViewById(R.id.pad_btnEnter);
        this.btnBackspace = (ImageButton) findViewById(R.id.pad_btnClear);
        this.btn0 = (Button) findViewById(R.id.pad_btnNro0);
        this.btn1 = (Button) findViewById(R.id.pad_btnNro1);
        this.btn2 = (Button) findViewById(R.id.pad_btnNro2);
        this.btn3 = (Button) findViewById(R.id.pad_btnNro3);
        this.btn4 = (Button) findViewById(R.id.pad_btnNro4);
        this.btn5 = (Button) findViewById(R.id.pad_btnNro5);
        this.btn6 = (Button) findViewById(R.id.pad_btnNro6);
        this.btn7 = (Button) findViewById(R.id.pad_btnNro7);
        this.btn8 = (Button) findViewById(R.id.pad_btnNro8);
        this.btn9 = (Button) findViewById(R.id.pad_btnNro9);
        this.btnPunto = (Button) findViewById(R.id.pad_btnPunto);

        //controles del panel
        this.imgIcono_panel = (ImageView) findViewById(R.id.panel_img_icono);
        this.txtWelcome_panel = (TextView) findViewById(R.id.panel_txt_welcome);
        this.txtUser_panel = (TextView) findViewById(R.id.panel_txt_user);
        this.txtComentario_panel = (TextView) findViewById(R.id.panel_txt_comentario);
        this.txtFuncion_panel = (TextView) findViewById(R.id.panel_txt_funcion);
        this.txtValor_panel = (TextView) findViewById(R.id.panel_txt_valor);
        this.txtFecha_panel = (TextView) findViewById(R.id.panel_txt_fecha);
        this.txtHora_panel = (TextView) findViewById(R.id.panel_txt_hora);

        //controles del popup medio pago
        this.btnConfirmar_mp = (Button) findViewById(R.id.medio_pago_btn_aceptar);
        this.btnCancelar_mp = (Button) findViewById(R.id.medio_pago_btn_cancelar);
        this.txtTotal_mp = (TextView) findViewById(R.id.medio_pago_txt_total);
        this.rbtMedios_mp = (RadioGroup) findViewById(R.id.medio_pago_medios);
    }

    private void Init_AsignarEventos() {

        btnCalc.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_MAIN);
                i.setComponent(new ComponentName("com.android.calculator2", "com.android.calculator2.Calculator"));


                try {
                    ServipagActivityMain.this.startActivity(i);
                } catch (ActivityNotFoundException noSuchActivity) {
                    // si el calculator intent no está registrado, probamos con un nombre alternativo
                    Intent i2 = new Intent();
                    i2.setAction(Intent.ACTION_MAIN);
                    i2.setComponent(new ComponentName("com.sec.android.app.popupcalculator", "com.sec.android.app.popupcalculator.Calculator"));
                    try {
                        ServipagActivityMain.this.startActivity(i2);
                    } catch (ActivityNotFoundException noSuchActivity2) {
                        // calculator intent not found
                        Toast.makeText(ServipagActivityMain.this, "Acceso a calculadora no encontrado", Toast.LENGTH_LONG).show();
                    }
                }

            }
        });

        this.lstMenu.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ItemMenu item = menuAdapter.getDatos().get(position);

                if (item.getHijos() != null && item.getHijos().size() > 0) {
                    menuAdapter = new ItemMenuAdapter(ServipagActivityMain.this, item.getHijos());

                    lstMenu.setAdapter(menuAdapter);
                } else {
                    try {

                        if (item.getAccion().equals("")) {
                            Toast.makeText(ServipagActivityMain.this, "Funcionalidad no implementada", Toast.LENGTH_SHORT).show();
                        } else {

                            String valor = item.getValor();
                            if (!valor.equals("")) {
                                Method accion = ServipagActivityMain.this.getClass().getMethod(item.getAccion(), valor.getClass());
                                accion.invoke(ServipagActivityMain.this, valor);
                            } else {
                                Method accion = ServipagActivityMain.this.getClass().getMethod(item.getAccion(), null);
                                accion.invoke(ServipagActivityMain.this, null);
                            }
                        }
                    } catch (Exception ex) {
                        Toast.makeText(ServipagActivityMain.this, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

            }
        });


        this.btnBackspace.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                String texto = txtPanel.getText().toString();

                if (!texto.isEmpty()) {
                    txtPanel.setText(texto.substring(0, texto.length() - 1));
                }
            }
        });

        this.btnBackspace.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                txtPanel.setText("");

                return false;
            }
        });


        this.btnEnter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if (depositoStep > 0) {//si estamos en el proceso de deposito
                    ProcesoDeposito();
                } else if (pagoServicioStep > 0) {
                    ProcesoPagoServicio();
                } else {
                    btnCancelar.performClick();
                }

            }
        });

        /*


        this.recarga_btnImprimirResultados.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ImpresionResultados();
            }
        });

        */
        this.btn0.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("0");
            }
        });

        this.btn1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("1");
            }
        });

        this.btn2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("2");
            }
        });

        this.btn3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("3");
            }
        });

        this.btn4.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("4");
            }
        });

        this.btn5.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("5");
            }
        });

        this.btn6.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("6");
            }
        });

        this.btn7.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("7");
            }
        });

        this.btn8.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("8");
            }
        });

        this.btn9.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("9");
            }
        });

        this.btnPunto.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel(".");
            }
        });


        this.btnCancelar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Cancelar();
            }
        });


        this.btnVolver.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Cancelar();
            }
        });


        this.dummy_view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                dummy_view.setBackgroundColor(Color.TRANSPARENT);
                dummy_view.setVisibility(View.INVISIBLE);
                (findViewById(R.id.servipag_medio_pago)).setVisibility(View.INVISIBLE);
            }
        });

        (findViewById(R.id.servipag_medio_pago)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //dejamos el evento click seteado en la vista para que no se vaya al click del dummyview
            }
        });

        this.btnConfirmar_mp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                dummy_view.setBackgroundColor(Color.TRANSPARENT);
                dummy_view.setVisibility(View.INVISIBLE);
                (findViewById(R.id.servipag_medio_pago)).setVisibility(View.INVISIBLE);

                //tomamos el medio de pago seleccionado
                PAGO_SERV_MEDIO_PAGO = ((RadioButton) rbtMedios_mp.findViewById(rbtMedios_mp.getCheckedRadioButtonId())).getText().toString();

                pagoServicioStep = 3;

                btnEnter.performClick();
            }
        });

        this.btnCancelar_mp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.servipag_medio_pago).setVisibility(View.INVISIBLE);
                dummy_view.setBackgroundColor(Color.TRANSPARENT);
                dummy_view.setVisibility(View.INVISIBLE);
            }
        });

        this.btnScan.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if (pagoServicioStep != 1) {
                    Toast.makeText(ServipagActivityMain.this, "Funcion no valida en este momento", Toast.LENGTH_SHORT).show();
                } else {

                    try {
                        //instanciamos la clase de integracion con el scan
                        //ScanIntegrator2 scanIntegrator2 = new ScanIntegrator2(MainActivity.this);
                        //iniciamos scan
                        //scanIntegrator2.initiateScan();

                        //instanciamos la clase de integracion con el scan
                        ScanIntegrator scanIntegrator = new ScanIntegrator(ServipagActivityMain.this);
                        //iniciamos scan
                        scanIntegrator.initiateScan();

                    } catch (Exception e) {
                        Toast.makeText(ServipagActivityMain.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    private void ProcesoPagoServicio() {
        switch (pagoServicioStep) {
            case 1: {

                String codigoIngresado = txtValor_panel.getText().toString().trim();

                //validamos el codigo ingresado
                if (codigoIngresado == null || codigoIngresado.equals("")) {
                    Toast.makeText(ServipagActivityMain.this, "Debe ingresar un monto para continuar", Toast.LENGTH_LONG).show();
                } else {

                    PAGO_SERV_COD_SERV = codigoIngresado;

                    EscribeLinea("\t" + PAGO_SERV_COD_SERV, "PAGO SERVICIOS");

                    txtValor_panel.setText("");
                    pagoServicioStep = 2;

                    EscribeLinea("Seleccione Medio Pago:", "PAGO SERVICIOS");

                    btnEnter.performClick();

                }
            }
            break;
            case 2: {

                //activamos popup medio pago
                dummy_view.setBackgroundColor(getResources().getColor(R.color.blur));
                dummy_view.setVisibility(View.VISIBLE);
                findViewById(R.id.servipag_medio_pago).setVisibility(View.VISIBLE);
                txtTotal_mp.setVisibility(View.GONE); //ocultamos
                findViewById(R.id.medio_pago_lbl_total).setVisibility(View.GONE);

                //seteamos por default en EFECTIVO
                rbtMedios_mp.check(R.id.medio_pago_rbt_efvo);
                findViewById(R.id.medio_pago_rbt_chq).setVisibility(View.GONE);//ocultamos los otros medios de pago
                findViewById(R.id.medio_pago_rbt_tc).setVisibility(View.GONE);
                findViewById(R.id.medio_pago_rbt_otro).setVisibility(View.GONE);

                //----**----
            }
            break;
            case 3: {


                EscribeLinea("\t" + PAGO_SERV_MEDIO_PAGO, "PAGO SERVICIOS");

                pagoServicioStep = 4;

                EscribeLinea("Ingrese RUT:", "PAGO SERVICIOS");

            }
            break;
            case 4: {

                String RutIngresado = txtValor_panel.getText().toString().trim();

                //validamos el dato ingresado
                if (RutIngresado == null || RutIngresado.equals("")) {
                    Toast.makeText(ServipagActivityMain.this, "Debe ingresar un RUT para continuar", Toast.LENGTH_LONG).show();
                } else if (RutIngresado.length() != 9) {
                    Toast.makeText(ServipagActivityMain.this, "Debe ingresar los nueve digitos del RUT para continuar", Toast.LENGTH_LONG).show();
                } else {

                    EscribeLinea("\t" + RutIngresado, "PAGO SERVICIOS");

                    PAGO_SERV_RUT = RutIngresado;

                    txtValor_panel.setText("");
                    pagoServicioStep = 5;

                    EnviarConsultaDeuda();

                }
            }
            break;
            case 5: {

                EscribeLinea("Deuda\t\t\tMinimo\t\t\tMaximo", "PAGO SERVICIOS");
                EscribeLinea("\t" + PAGO_SERV_DEUDA_MONTO + "\t\t\t\t\t" + PAGO_SERV_DEUDA_PAGOMIN + "\t\t\t\t\t" + PAGO_SERV_DEUDA_PAGOMAX, "PAGO SERVICIOS");

                pagoServicioStep = 6;

                EscribeLinea("Ingrese Monto:", "PAGO SERVICIOS");
            }
            break;
            case 6: {

                String montoIngresado = txtValor_panel.getText().toString().trim();

                //validamos el monto ingresado
                if (montoIngresado == null || montoIngresado.equals("")) {
                    Toast.makeText(ServipagActivityMain.this, "Debe ingresar un monto para continuar", Toast.LENGTH_LONG).show();
                } else if (montoIngresado.contains(".")) {
                    Toast.makeText(ServipagActivityMain.this, "El monto a ingresar debe ser entero", Toast.LENGTH_LONG).show();
                } //else if (montoIngresado.length() > 5) {
                //  Toast.makeText(ServipagActivityMain.this, "El monto a ingresar no puede superar $ 99999", Toast.LENGTH_LONG).show();
                //}
                else {
                    PAGO_SERV_MONTO = montoIngresado;

                    //EscribeLinea("\t" + "$ " + new DecimalFormat("0.00").format(Double.valueOf(montoIngresado)), "PAGO SERVICIOS");
                    EscribeLinea("\t" + Formatos.FormateaDecimal(montoIngresado, Formatos.DecimalFormat, "$"), "PAGO SERVICIOS");

                    txtValor_panel.setText("");
                    pagoServicioStep = 7;

                    EscribeLinea("Presione OK para confirmar...", "PAGO SERVICIOS");
                }
            }
            break;
            case 7: {
                EnviarPagoServicio();
            }
            break;
            default:
                btnCancelar.performClick();
                break;
        }
    }


    private void ProcesoDeposito() {
        switch (depositoStep) {
            case 1: {

                String montoIngresado = txtValor_panel.getText().toString().trim();

                //validamos el monto ingresado
                if (montoIngresado == null || montoIngresado.equals("")) {
                    Toast.makeText(ServipagActivityMain.this, "Debe ingresar un monto para continuar", Toast.LENGTH_LONG).show();
                } else if (montoIngresado.contains(".")) {
                    Toast.makeText(ServipagActivityMain.this, "El monto a ingresar debe ser entero", Toast.LENGTH_LONG).show();
                } //else if (montoIngresado.length() > 5) {
                //  Toast.makeText(ServipagActivityMain.this, "El monto a ingresar no puede superar $ 99999", Toast.LENGTH_LONG).show();
                //}
                else {
                    DEPOSITO_MONTO = montoIngresado;

                    //EscribeLinea("\t" + "$ " + new DecimalFormat("0.00").format(Double.valueOf(montoIngresado)), "DEPOSITO");
                    EscribeLinea("\t" + Formatos.FormateaDecimal(montoIngresado, Formatos.DecimalFormat, "$"), "DEPOSITO");

                    txtValor_panel.setText("");
                    depositoStep = 2;

                    EscribeLinea("Seleccione Banco:", "DEPOSITO");

                    List<ResultadoMenu> bancos = new ArrayList<ResultadoMenu>();
                    bancos.add(new ResultadoMenu("BANCO DE CHILE", null, R.drawable.logo_banco_de_chile));
                    bancos.add(new ResultadoMenu("BCI", null, R.drawable.logo_bci));

                    this.servipag_lstResultados.setAdapter(new ResultadosMenuAdapter(ServipagActivityMain.this, bancos));
                    this.servipag_lstResultados.setVisibility(View.VISIBLE);
                    this.servipag_lstResultados.setOnItemClickListener(new OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            ResultadoMenu item = ((ResultadosMenuAdapter) servipag_lstResultados.getAdapter()).datos.get(position);

                            DEPOSITO_BANCO = item.get_descripcion();

                            EscribeLinea("\t" + DEPOSITO_BANCO, "DEPOSITO");

                            depositoStep = 2;

                            ProcesoDeposito();

                            servipag_lstResultados.setVisibility(View.GONE);
                            //(findViewById(R.id.servipag_tr_resultados)).setVisibility(View.GONE);
                        }
                    });
                    //(findViewById(R.id.servipag_tr_resultados)).setVisibility(View.VISIBLE);
                }
            }
            break;
            case 2: {

                depositoStep = 3;
                EscribeLinea("Ingrese RUT del Titular:", "DEPOSITO");
            }
            break;
            case 3: {

                String RutIngresado = txtValor_panel.getText().toString().trim();

                //validamos el dato ingresado
                if (RutIngresado == null || RutIngresado.equals("")) {
                    Toast.makeText(ServipagActivityMain.this, "Debe ingresar un RUT para continuar", Toast.LENGTH_LONG).show();
                } else if (RutIngresado.length() != 9) {
                    Toast.makeText(ServipagActivityMain.this, "Debe ingresar los nueve digitos del RUT para continuar", Toast.LENGTH_LONG).show();
                } else {

                    EscribeLinea("\t" + RutIngresado, "DEPOSITO");

                    DEPOSITO_RUT = RutIngresado;

                    txtValor_panel.setText("");
                    depositoStep = 4;

                    EscribeLinea("Ingrese Numero de Cuenta Destino:", "DEPOSITO");
                }
            }
            break;
            case 4: {

                String NroCuentaIngresado = txtValor_panel.getText().toString().trim();

                //validamos el dato ingresado
                if (NroCuentaIngresado == null || NroCuentaIngresado.equals("")) {
                    Toast.makeText(ServipagActivityMain.this, "Debe ingresar un Nro de Cuenta para continuar", Toast.LENGTH_LONG).show();
                } else {

                    EscribeLinea("\t" + NroCuentaIngresado, "DEPOSITO");

                    DEPOSITO_CUENTA = NroCuentaIngresado;

                    txtValor_panel.setText("");
                    depositoStep = 5;

                    EscribeLinea("@C@@B@SI USTED NO POSEE SALDO SUFICIENTE", "DEPOSITO");
                    EscribeLinea("@C@@B@EL DEPOSITO NO SE REALIZARA", "DEPOSITO");
                    EscribeLinea("Presione OK para confirmar...", "DEPOSITO");
                }

            }
            break;
            case 5: {

                EnviarDeposito();

            }

            break;
            default:
                btnCancelar.performClick();
                break;
        }
    }

    private void GeneraMenu() {

        menuServipagList = new ArrayList<ItemMenu>();

        //***************** Menu Principal *****************

        ItemMenu itemPagoServicios = new ItemMenu("Pago de Servicios", null, "IniciarPagoServicios", "");
        ItemMenu itemBancarizacion = new ItemMenu("Bancarizacion");
        ItemMenu itemResumenOp = new ItemMenu("Resumen Operador");
        ItemMenu itemReimprimirTicket = new ItemMenu("Reimprimir Ticket", null, "ReimprimirTicket", "");

        //**************Bancarizacion************************************

        itemBancarizacion.getHijos().add(new ItemMenu("Deposito", null, "IniciarDeposito", ""));
        itemBancarizacion.getHijos().add(new ItemMenu("Giro", null, "", ""));
        itemBancarizacion.getHijos().add(new ItemMenu("Consulta de Saldo", null, "", ""));
        itemBancarizacion.getHijos().add(new ItemMenu("Volver", menuServipagList, "", ""));

        //***************************************************************

        menuServipagList.add(itemPagoServicios);
        menuServipagList.add(itemBancarizacion);
        menuServipagList.add(itemResumenOp);
        menuServipagList.add(itemReimprimirTicket);

        menuAdapter = new ItemMenuAdapter(this, menuServipagList);
        lstMenu.setAdapter(menuAdapter);
    }

    public void IniciarPagoServicios() {
        //ocultamos la lista
        this.lstMenu.setVisibility(View.GONE);

        this.txtComentario_panel.setText("PAGO SERVICIOS");
        this.txtComentario_panel.setVisibility(View.VISIBLE);

        this.pagoServicioStep = 1;
        this.lineasLayout = null;

        EscribeLinea("Ingrese Codigo Servicio:", "PAGO SERVICIOS");

    }


    public void IniciarDeposito() {
        //ocultamos la lista
        this.lstMenu.setVisibility(View.GONE);

        this.txtComentario_panel.setText("DEPOSITO");
        this.txtComentario_panel.setVisibility(View.VISIBLE);

        this.depositoStep = 1;
        this.lineasLayout = null;

        EscribeLinea("Ingrese Monto:", "DEPOSITO");

    }

    public void EnviarConsultaDeuda() {
        try {

            if (!isOnline()) {
                Toast.makeText(ServipagActivityMain.this, "Error: No hay una conexion disponible. Intente mas tarde.", Toast.LENGTH_LONG).show();
            } else {

                new ExecuteRequestTask("ConsultaDeudaResponse", "").execute();
            }

        } catch (Exception ex) {
            Toast.makeText(ServipagActivityMain.this, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void EnviarPagoServicio() {
        try {

            if (!isOnline()) {
                Toast.makeText(ServipagActivityMain.this, "Error: No hay una conexion disponible. Intente mas tarde.", Toast.LENGTH_LONG).show();
            } else {

                new ExecuteRequestTask("PagoServicioResponse", "").execute();
            }

        } catch (Exception ex) {
            Toast.makeText(ServipagActivityMain.this, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void EnviarDeposito() {
        try {

            if (!isOnline()) {
                Toast.makeText(ServipagActivityMain.this, "Error: No hay una conexion disponible. Intente mas tarde.", Toast.LENGTH_LONG).show();
            } else {

                new ExecuteRequestTask("DepositoResponse", "").execute();
            }

        } catch (Exception ex) {
            Toast.makeText(ServipagActivityMain.this, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void ConsultaDeudaResponse() {

        PAGO_SERV_DEUDA_MONTO = "6848";
        PAGO_SERV_DEUDA_PAGOMIN = "685";
        PAGO_SERV_DEUDA_PAGOMAX = "6848";

        btnEnter.performClick();

    }

    public void PagoServicioResponse() {

        this.pagoServicioStep = 0;

        List<String> lineas = new ArrayList<String>();
        lineas.add("");
        lineas.add("@C@" + "TRANSACCION");
        lineas.add("@C@" + "APROBADA");

        EscribeLineas(lineas, "PAGO SERVICIOS");

        RegistrarVenta("PAG", "PAGO SERV.", PAGO_SERV_MONTO);

        GeneraTicketPagoServicio();

    }

    public void DepositoResponse() {

        this.depositoStep = 0;

        List<String> lineas = new ArrayList<String>();
        lineas.add("");
        lineas.add("@C@" + "TRANSACCION");
        lineas.add("@C@" + "APROBADA");

        EscribeLineas(lineas, "DEPOSITO");

        RegistrarVenta("DEP", "DEPOSITO", DEPOSITO_MONTO);

        GeneraTicketDeposito();

    }

    private void RegistrarVenta(String codTipoOperacion, String tipoOperacion, String monto) {

        try {

            //Date now = Util_FechaHora(mensajeRecarga.getFechaHora());
            Date now = new Date();
            //obtenemos el medio de pago seleccionado
            //String medio_pago = ((RadioButton) this.rbtMedios_mp.findViewById(this.rbtMedios_mp.getCheckedRadioButtonId())).getText().toString();


            Venta venta = new Venta(
                    0,
                    Double.valueOf(monto),
                    1,
                    Formatos.FormateaDate(now, Formatos.DateFormat),
                    Formatos.FormateaDate(now, Formatos.TimeFormat),
                    codTipoOperacion,
                    this._user.getId(),
                    TipoMedioPago.EFECTIVO.name()
            );

            //VENTA
            ContentValues cvVenta = new ContentValues();

            cvVenta.put(VentasHelper.VENTA_TOTAL, venta.getTotal());
            cvVenta.put(VentasHelper.VENTA_FECHA, venta.getFecha());
            cvVenta.put(VentasHelper.VENTA_HORA, venta.getHora());
            cvVenta.put(VentasHelper.VENTA_MEDIO_PAGO, venta.getMedio_pago());
            cvVenta.put(VentasHelper.VENTA_CODIGO, venta.getCodigo());
            cvVenta.put(VentasHelper.VENTA_CANTIDAD, venta.getCantidad());
            cvVenta.put(VentasHelper.VENTA_USERID, venta.getUserid());

            Uri newVentaUri = getContentResolver().insert(Uri.parse("content://com.pds.ficle.ep.ventas.contentprovider/ventas"), cvVenta);

            venta.setId(Integer.valueOf(newVentaUri.getPathSegments().get(1)));

            //DETALLES
            ContentValues cvDetalle = new ContentValues();

            cvDetalle.put(VentaDetalleHelper.VENTA_DET_VENTA_ID, venta.getId());
            cvDetalle.put(VentaDetalleHelper.VENTA_DET_PRODUCTO_ID, 1);
            cvDetalle.put(VentaDetalleHelper.VENTA_DET_CANTIDAD, venta.getCantidad());
            cvDetalle.put(VentaDetalleHelper.VENTA_DET_PRECIO, venta.getTotal());
            cvDetalle.put(VentaDetalleHelper.VENTA_DET_TOTAL, venta.getTotal());

            Uri newVentaDetUri = getContentResolver().insert(Uri.parse("content://com.pds.ficle.ep.venta_detalles.contentprovider/venta_detalles"), cvDetalle);

            if (codTipoOperacion.equals("PAG")) {//si es operacion de pago de serv registramos el movimiento en la caja

                Caja.RegistrarMovimientoCaja(ServipagActivityMain.this, TipoMovimientoCaja.DEPOSITO, Formatos.FormateaDecimal(venta.getTotal(), Formatos.DecimalFormat_US), venta.getMedio_pago(), "", venta.getId());
            }

            Logger.RegistrarEvento(this, "i", tipoOperacion + " #" + venta.getNroVenta(), "Total: " + Formatos.FormateaDecimal(venta.getTotal(), Formatos.DecimalFormat, "$"));

        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), "Se ha producido un error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void GeneraTicketPagoServicio() {
        try {
            if (_printer == null)
                _printer = Printer.getInstance(config.PRINTER_MODEL);

            if (!_printer.initialize()) {
                Toast.makeText(ServipagActivityMain.this, "Error al inicializar", Toast.LENGTH_SHORT).show();
                _printer.end();
            } else {
                _printer.lineFeed();

                //COMPROBANTE DEPOSITO
                _printer.format(2, 2, ALINEACION.CENTER);
                _printer.printLine("COMPROBANTE");

                _printer.format(2, 2, ALINEACION.CENTER);
                _printer.printLine("PAGO SERVICIO");
                _printer.cancelCurrentFormat();
                _printer.sendSeparadorHorizontal();

                //fecha y hora
                _printer.printLine("Fecha y Hora  : " + Formatos.FormateaDate(new Date(), Formatos.FullDateTimeFormatNoSeconds));
                _printer.lineFeed();

                //codigoServicio
                _printer.printLine("Cod. Servicio : " + PAGO_SERV_COD_SERV);
                _printer.lineFeed();

                //rutTitular
                _printer.printLine("RUT           : " + PAGO_SERV_RUT);
                _printer.lineFeed();

                //montoPago
                _printer.printLine("Monto Pago    : $ " + PAGO_SERV_MONTO);
                _printer.lineFeed();

                //medioPago
                _printer.printLine("Medio Pago    : " + PAGO_SERV_MEDIO_PAGO);
                _printer.lineFeed();

                _printer.format(2, 1, ALINEACION.CENTER);
                _printer.printLine("TRANSACCION");
                _printer.format(2, 1, ALINEACION.CENTER);
                _printer.printLine("APROBADA");
                _printer.cancelCurrentFormat();
                _printer.lineFeed();

                _printer.footer();
            }
        } catch (Exception ex) {
            Toast.makeText(ServipagActivityMain.this, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void GeneraTicketDeposito() {
        try {
            if (_printer == null)
                _printer = Printer.getInstance(config.PRINTER_MODEL);

            if (!_printer.initialize()) {
                Toast.makeText(ServipagActivityMain.this, "Error al inicializar", Toast.LENGTH_SHORT).show();
                _printer.end();
            } else {

                _printer.lineFeed();

                //COMPROBANTE DEPOSITO
                _printer.format(2, 2, ALINEACION.CENTER);
                _printer.printLine("COMPROBANTE");
                _printer.cancelCurrentFormat();

                _printer.format(2, 2, ALINEACION.CENTER);
                _printer.printLine("DEPOSITO");
                _printer.cancelCurrentFormat();
                _printer.sendSeparadorHorizontal();

                //Banco
                _printer.format(2, 1, ALINEACION.CENTER);
                _printer.printLine(DEPOSITO_BANCO);
                _printer.cancelCurrentFormat();
                _printer.lineFeed();

                //fecha y hora
                _printer.printLine("Fecha y Hora   : " + Formatos.FormateaDate(new Date(), Formatos.FullDateTimeFormatNoSeconds));
                _printer.lineFeed();

                //rutTitular
                _printer.printLine("RUT Titular    : " + DEPOSITO_RUT);
                _printer.lineFeed();

                //montoDeposito
                _printer.printLine("Monto Deposito : $ " + DEPOSITO_MONTO);
                _printer.lineFeed();

                //cuentaDestino
                _printer.printLine("Numero Cuenta  : " + DEPOSITO_CUENTA);
                _printer.lineFeed();
                _printer.cancelCurrentFormat();

                _printer.format(2, 1, ALINEACION.CENTER);
                _printer.printLine("TRANSACCION");
                _printer.cancelCurrentFormat();

                _printer.format(2, 1, ALINEACION.CENTER);
                _printer.printLine("APROBADA");
                _printer.cancelCurrentFormat();
                _printer.lineFeed();

                _printer.footer();
            }
        } catch (Exception ex) {
            Toast.makeText(ServipagActivityMain.this, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void ReimprimirTicket() {
        //mSerialPort.rePrint();
    }

    private void EscribeLinea(String linea) {
        EscribeLinea(linea, "");
    }

    private void EscribeLinea(String linea, String titulo) {

        if (this.lineasLayout == null)
            this.lineasLayout = new ArrayList<String>();

        this.lineasLayout.add(linea);

        EscribeLineas(this.lineasLayout, titulo);
    }

    private void EscribeLineas(List<String> lineas) {
        EscribeLineas(lineas, "");
    }

    private void EscribeLineas(List<String> lineas, String titulo) {

        //ocultamos el menu y la lista de resultados
        (findViewById(R.id.servipag_lstMenu)).setVisibility(View.GONE);
        //(findViewById(R.id.servipag_tr_resultados)).setVisibility(View.GONE);
        (findViewById(R.id.servipag_lstResultados)).setVisibility(View.GONE);
        //mostramos el layout de lineas
        (findViewById(R.id.servipag_lineas)).setVisibility(View.VISIBLE);
        this.btnVolver.setVisibility(View.VISIBLE);

        if (!titulo.equals("")) {

            TextView txtTitulo = (TextView) findViewById(R.id.servipag_txt_titulo);
            txtTitulo.setVisibility(View.VISIBLE);
            txtTitulo.setText(titulo);
        } else {
            TextView txtTitulo = (TextView) findViewById(R.id.servipag_txt_titulo);
            txtTitulo.setVisibility(View.GONE);
            txtTitulo.setText("");
        }

        //blanqueamos
        ((TextView) findViewById(R.id.servipag_txt1)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.servipag_txt2)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.servipag_txt3)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.servipag_txt4)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.servipag_txt5)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.servipag_txt6)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.servipag_txt7)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.servipag_txt8)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.servipag_txt9)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.servipag_txt10)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.servipag_txt11)).setVisibility(View.GONE);

        for (int i = 0; i < lineas.size(); i++) {

            TextView txtLinea = null;

            switch (i + 1) {
                case 1:
                    txtLinea = (TextView) findViewById(R.id.servipag_txt1);
                    break;
                case 2:
                    txtLinea = (TextView) findViewById(R.id.servipag_txt2);
                    break;
                case 3:
                    txtLinea = (TextView) findViewById(R.id.servipag_txt3);
                    break;
                case 4:
                    txtLinea = (TextView) findViewById(R.id.servipag_txt4);
                    break;
                case 5:
                    txtLinea = (TextView) findViewById(R.id.servipag_txt5);
                    break;
                case 6:
                    txtLinea = (TextView) findViewById(R.id.servipag_txt6);
                    break;
                case 7:
                    txtLinea = (TextView) findViewById(R.id.servipag_txt7);
                    break;
                case 8:
                    txtLinea = (TextView) findViewById(R.id.servipag_txt8);
                    break;
                case 9:
                    txtLinea = (TextView) findViewById(R.id.servipag_txt9);
                    break;
                case 10:
                    txtLinea = (TextView) findViewById(R.id.servipag_txt10);
                    break;
                case 11:
                    txtLinea = (TextView) findViewById(R.id.servipag_txt11);
                    break;

            }

            String texto = lineas.get(i);
            if (texto.startsWith("@C@")) {
                texto = texto.replace("@C@", "");
                txtLinea.setGravity(Gravity.CENTER);
            } else {
                txtLinea.setGravity(Gravity.LEFT);
            }

            if (texto.startsWith("@B@")) {
                texto = texto.replace("@B@", "");
                txtLinea.setTypeface(null, Typeface.BOLD);
            } else {
                txtLinea.setTypeface(null, Typeface.NORMAL);
            }


            txtLinea.setText(texto);
            txtLinea.setVisibility(View.VISIBLE);
        }

    }

    private void AgregarTextoPanel(String texto) {
        this.txtPanel.setText(this.txtPanel.getText().toString() + texto);
        this.txtPanel.setVisibility(View.VISIBLE);
    }

    private void Cancelar() {
        //mostramos el menu
        GeneraMenu();

        this.lstMenu.setVisibility(View.VISIBLE);

        //ocultamos el layout de lineas, la lista de resultados y los montos fijos
        //(findViewById(R.id.servipag_tr_resultados)).setVisibility(View.GONE);
        (findViewById(R.id.servipag_lstResultados)).setVisibility(View.GONE);
        (findViewById(R.id.servipag_lineas)).setVisibility(View.GONE);
        this.btnVolver.setVisibility(View.GONE);

        this.txtComentario_panel.setText("");
        this.txtComentario_panel.setVisibility(View.INVISIBLE);

        this.txtPanel.setText("");

        //blanqueamos los steps
        depositoStep = 0;
        pagoServicioStep = 0;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.servipag_activity_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //tiene conectividad?
    public boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        return (networkInfo != null && networkInfo.isConnected());

        //si queres saber que tipo de conexion está activa
        //NetworkInfo networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        //boolean isWifiConn = networkInfo.isConnected();
        //networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        //boolean isMobileConn = networkInfo.isConnected();
    }

    private class ExecuteRequestTask extends AsyncTask<String, Void, String> {

        private String nextAction = "";
        private String paramsNextAction = "";

        //constructor
        public ExecuteRequestTask(String NextAction, String ParamsNextAction) {
            this.nextAction = NextAction;
            this.paramsNextAction = ParamsNextAction;
        }

        // onPreExecute used to setup the AsyncTask.
        @Override
        protected void onPreExecute() {
            showProgress(true);
            _servipagStatusMessageView.setText("Consultando servidor...");
        }

        @Override
        protected String doInBackground(String... messages) {

            // params comes from the execute() call: params[0] is the message.
            try {
                // Simulate network access.
                Thread.sleep(2000);

                publishProgress();

                return "";

                //} catch (IOException e) {
                //    return "No es posible conectarse. Intente mas tarde.";
            } catch (Exception e) {
                return "Error: " + e.getMessage();
            }
        }

        @Override
        protected void onProgressUpdate(Void... v) {
            _servipagStatusMessageView.setText("Leyendo respuesta...");
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            try {
                showProgress(false);

                if (result.equals("") && !this.nextAction.equals("")) {

                    if (!this.paramsNextAction.equals("")) {
                        //si tiene param
                        Method accion = ServipagActivityMain.this.getClass().getMethod(this.nextAction, this.paramsNextAction.getClass());

                        accion.invoke(ServipagActivityMain.this, this.paramsNextAction);
                    } else {
                        //sino tiene param
                        Method accion = ServipagActivityMain.this.getClass().getMethod(this.nextAction, null);

                        accion.invoke(ServipagActivityMain.this, null);
                    }


                } else {
                    Toast.makeText(ServipagActivityMain.this, result.toString(), Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {
                Toast.makeText(ServipagActivityMain.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }


        @Override
        protected void onCancelled() {
            showProgress(false);
        }
    }


    /**
     * Shows the progress UI and hides the main layout.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            _servipagStatusView.setVisibility(View.VISIBLE);
            _servipagStatusView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(show ? 1 : 0)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            _servipagStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
                        }
                    });

            _servipagView.setVisibility(View.VISIBLE);
            _servipagView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(show ? 0 : 1)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            _servipagView.setVisibility(show ? View.GONE : View.VISIBLE);
                        }
                    });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            _servipagStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
            _servipagView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    class ResultadoMenu {

        private String _descripcion;
        private Object _item;
        private int _idImagen;

        public String get_descripcion() {
            return _descripcion;
        }

        public void set_descripcion(String _descripcion) {
            this._descripcion = _descripcion;
        }

        public Object get_item() {
            return _item;
        }

        public void set_item(Object _item) {
            this._item = _item;
        }

        public int get_idImagen() {
            return _idImagen;
        }

        public void set_idImagen(int _idImagen) {
            this._idImagen = _idImagen;
        }

        public ResultadoMenu(String descripcion, Object item, int idImagen) {
            this._descripcion = descripcion;
            this._item = item;
            this._idImagen = idImagen;
        }
    }


    class ResultadosMenuAdapter extends ArrayAdapter<ResultadoMenu> {

        private Context context;
        List<ResultadoMenu> datos;

        public ResultadosMenuAdapter(Context context, List<ResultadoMenu> datos) {
            super(context, android.R.layout.simple_list_item_1, datos);

            this.context = context;
            this.datos = datos;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // En primer lugar "inflamos" una nueva vista, que será la que se
            // mostrará en la celda del ListView. Para ello primero creamos el
            // inflater, y después inflamos la vista.
            LayoutInflater inflater = LayoutInflater.from(context);

            View item = inflater.inflate(R.layout.servipag_menu_item, null);

            ResultadoMenu row = datos.get(position);

            // A partir de la vista, recogeremos los controles que contiene para
            // poder manipularlos.
            // Recogemos los TextView para mostrar datos
            ((TextView) item.findViewById(android.R.id.text1)).setText(row.get_descripcion());
            ((ImageView) item.findViewById(R.id.image1)).setImageResource(row.get_idImagen());

            // Devolvemos la vista para que se muestre en el ListView.
            return item;

        }

    }
}
