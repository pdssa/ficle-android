package com.pds.ficle.vale;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Formatos;
import com.pds.common.SII.vale.model.InformeResp;
import com.pds.common.SII.vale.model.InitTerminalReq;
import com.pds.common.SII.vale.model.InitTerminalResp;
import com.pds.common.SII.vale.model.Vale;
import com.pds.common.SII.vale.services.CierreZtask;
import com.pds.common.SII.vale.services.GetUltValeTask;
import com.pds.common.SII.vale.services.InformeDetalleTransaccionesTask;
import com.pds.common.SII.vale.model.InformeReq;
import com.pds.common.SII.vale.listener.InformeListener;
import com.pds.common.SII.vale.services.InformeXtask;
import com.pds.common.SII.vale.services.InicializarTerminalTask;
import com.pds.common.SII.vale.listener.InitTerminalListener;
import com.pds.common.SII.vale.util.ValeTicketBuilder;
import com.pds.common.SII.vale.util.ValeUtils;
import com.pds.common.Usuario;
import com.pds.common.activity.TimerMainActivity;
import com.pds.common.dialog.Dialog;
import com.pds.common.pref.ValeConfig;

import java.util.Date;

import android_serialport_api.Printer;


public class MainActivity extends TimerMainActivity {

    private View btnReimprimirUltimoVale;
    private View btnCierreX;
    private View btnCierreZ;
    private View btnDetalleTrxs;
    private View btnBack;
    private Usuario _user;
    private Config config;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            ((TextView) findViewById(R.id.txtVersion)).setText("Version:" + getPackageManager().getPackageInfo(this.getPackageName().toString(), 0).versionName);

            //logueo Usuario *****************************************************
            //obtenemos los parametros pasamos a la actividad
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                //tomamos el id de usuario
                //_id_user_login = extras.getInt("_id_user_login");
                this._user = (Usuario) extras.getSerializable("current_user");
            }

            //no tenemos user logueado...volvemos a EntryPoint
            if (_user == null) {
                Toast.makeText(this, R.string.unknown_user, Toast.LENGTH_LONG).show();
                this.finish();
            } else {

                Init_Views();

                Init_Metodos();

                config = new Config(this);

                if(!config.isValeHabilitado()){
                    //servicio de vales no activado
                    ocultarOpciones();

                    ServicioNoHabilitadoAlertMessage();
                }
                else if(!config.isValeInicializado()){
                    //servicio activado pero no inicializado
                    ocultarOpciones();

                    TerminalNoInicializadoAlertMessage(config.isValeModalidadDemo());
                }
                else{
                    mostrarOpciones();
                }

                /*if(!ValeUtils.isValeServiceEnabled(MainActivity.this)){
                    //servicio de vales no activado
                    ocultarOpciones();

                    ServicioNoHabilitadoAlertMessage();
                }
                else if(!ValeUtils.isEmisionValesActivado(getContentResolver())){
                    //servicio activado pero no inicializado
                    ocultarOpciones();

                    TerminalNoInicializadoAlertMessage();
                }
                else{
                    mostrarOpciones();
                }*/

            }

        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void Init_Views() {
        btnReimprimirUltimoVale = findViewById(R.id.vale_reimprimir_vale);
        btnCierreX = findViewById(R.id.vale_cierre_x);
        btnCierreZ = findViewById(R.id.vale_cierre_z);
        btnDetalleTrxs = findViewById(R.id.vale_detalle_transacciones);
        btnBack = findViewById(R.id.vale_main_btn_back);
    }

    private void ocultarOpciones(){
        btnReimprimirUltimoVale.setVisibility(View.GONE);
        btnCierreX.setVisibility(View.GONE);
        btnCierreZ.setVisibility(View.GONE);
        btnDetalleTrxs.setVisibility(View.GONE);

        findViewById(R.id.vale_modo_simulacion).setVisibility(View.GONE);

    }

    private void mostrarOpciones(){
        btnReimprimirUltimoVale.setVisibility(View.VISIBLE);
        btnCierreX.setVisibility(View.VISIBLE);
        btnCierreZ.setVisibility(View.VISIBLE);
        btnDetalleTrxs.setVisibility(View.VISIBLE);

        if(config.isValeModalidadDemo())
            findViewById(R.id.vale_modo_simulacion).setVisibility(View.VISIBLE);
        else
            findViewById(R.id.vale_modo_simulacion).setVisibility(View.GONE);

    }

    private InformeReq getRequest() {
        return new InformeReq(config, ValeUtils.GetUserID(config.SII_TERMINAL_ID, getContentResolver()), config.SII_MERCHANT_ID);
    }

    private InformeReq getRequestZ() {
        //return new InformeReq(config.CLAVEFISCAL, config.ID_COMERCIO, ValeUtils.GetUserID(config.ID_COMERCIO, getContentResolver()), 0, 0d, 0d);
        return new InformeReq(config, ValeUtils.GetUserID(config.SII_TERMINAL_ID, getContentResolver()), 0, 0d, 0, 0d, false, config.SII_MERCHANT_ID);
    }

    private void ImprimirDetalleTransacciones() {

        InformeReq informeReq = getRequest();

        InformeDetalleTransaccionesTask inf = new InformeDetalleTransaccionesTask(MainActivity.this, new InformeListener() {
            @Override
            public void onConsultaCompletada(final String response) {
                try {

                    //DETALLES
                    if (!TextUtils.isEmpty(response)) {
                        final Printer _printer = Printer.getInstance(config.PRINTER_MODEL, MainActivity.this);

                        _printer.delayActivated = true;
                        _printer.printerCallback = new Printer.PrinterCallback() {
                            @Override
                            public void onPrinterReady() {
                                _printer.lineFeed();
                                _printer.lineFeed();


                                _printer.format(1, 1, Printer.ALINEACION.CENTER);


                                //titulo
                                _printer.printLine("DETALLE DE TRANSACCIONES");
                                _printer.sendSeparadorHorizontal(false);

                                //_printer.format(1, 1, Printer.ALINEACION.CENTER);
                                _printer.printLine(Formatos.FormateaDate(new Date(), Formatos.DateFormatSlashAndTimeSec));
                                _printer.printLine("TER: " + config.SII_TERMINAL_ID);

                                _printer.sendSeparadorHorizontal('=');


                                //DETALLES
                                String[] transacciones = response.split("#");//#<fecha>*<nroVale>*<total>*<medio>

                                for (int i = 0; i < transacciones.length; i++) {

                                    /*if(i % 3 == 0 ) {
                                        //pausamos cada 3
                                        try {
                                            Thread.sleep(1500);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        //Log.d("pdv", String.valueOf(i));
                                    }*/

                                    String transaccion = transacciones[i];

                                    if (!TextUtils.isEmpty(transaccion)) {
                                        String[] datos = transaccion.split("\\*");

                                        //transaccion
                                        _printer.printOpposite("VALE:" + datos[1], ValeUtils.ExtractHora(datos[0]));
                                        _printer.printOpposite(datos[3], Formatos.ParseaFormateaDecimal(datos[2], Formatos.DecimalFormat_US, Formatos.DecimalFormat_CH, "$"));

                                        _printer.sendSeparadorHorizontal(false);
                                    }

                                }

                                _printer.footer();

                                _printer.end();
                            }

                            /*@Override
                            public void onPrinterOutOfPaper() {
                                Toast.makeText(MainActivity.this, "Sin PAPEL", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onPrinterError(String error) {
                                Toast.makeText(MainActivity.this, "Error: " + error, Toast.LENGTH_SHORT).show();
                            }*/
                        };

                        //iniciamos la printer
                        if (!_printer.initialize()) {
                            Toast.makeText(MainActivity.this, "Error al inicializar", Toast.LENGTH_SHORT).show();

                            _printer.end();
                        }
                    } else {
                        AlertMessage("DETALLE DE TRANSACCIONES", "<b>SIN TRANSACCIONES</b>");
                    }

                } catch (Exception ex) {
                    Toast.makeText(MainActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onConsultaCompletada(InformeResp response) {

            }

            @Override
            public void onError() {
            }
        });
        inf.execute(informeReq);
    }

    private void GenerarCierreX() {
        InformeReq informeReq = getRequest();

        InformeXtask inf = new InformeXtask(MainActivity.this, new InformeListener() {
            @Override
            public void onConsultaCompletada(final String response) {

            }

            @Override
            public void onConsultaCompletada(final InformeResp response) {

                //mostramos los resultados en pantalla, impresion opcional
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                // Get the layout inflater
                LayoutInflater inflater = MainActivity.this.getLayoutInflater();

                // Inflate and set the layout for the dialog
                View cierreResultView = inflater.inflate(R.layout.cierre_x_result, null);

                ((TextView) cierreResultView.findViewById(R.id.cierre_result_msj)).setText("Informe X ejecutado correctamente. A continuación los resultados:");
                ((TextView) cierreResultView.findViewById(R.id.cierre_result_total_ventas)).setText(Formatos.ParseaFormateaDecimal(response.TOTAL, Formatos.DecimalFormat_US, Formatos.DecimalFormat_CH, "$"));
                ((TextView) cierreResultView.findViewById(R.id.cierre_result_total_ventas_afecto)).setText(Formatos.ParseaFormateaDecimal(response.TOTAL_AFECTO, Formatos.DecimalFormat_US, Formatos.DecimalFormat_CH, "$"));
                ((TextView) cierreResultView.findViewById(R.id.cierre_result_total_ventas_exento)).setText(Formatos.ParseaFormateaDecimal(response.TOTAL_NO_AFECTO, Formatos.DecimalFormat_US, Formatos.DecimalFormat_CH, "$"));
                ((TextView) cierreResultView.findViewById(R.id.cierre_result_vale_ini)).setText(response.VALE_INI);
                ((TextView) cierreResultView.findViewById(R.id.cierre_result_vale_fin)).setText(response.VALE_FIN);

                builder.setView(cierreResultView);

                AlertDialog dialog = builder.create();
                dialog.setTitle("Informe X");
                dialog.setIcon(android.R.drawable.ic_menu_info_details);
                dialog.setCanceledOnTouchOutside(false);
                dialog.setCancelable(false);
                dialog.setOnCancelListener(null);

                dialog.setButton(AlertDialog.BUTTON_NEUTRAL, "ACEPTAR", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                dialog.setButton(AlertDialog.BUTTON_POSITIVE, "IMPRIMIR", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        imprimirInformeX(response);
                    }
                });

                dialog.show();

            }

            @Override
            public void onError() {
            }
        });
        inf.execute(informeReq);

    }

    private void GenerarCierreZ(){
        CierreZtask inf = new CierreZtask(MainActivity.this, config, false, new InformeListener() {
            @Override
            public void onConsultaCompletada(String response) {

            }

            @Override
            public void onConsultaCompletada(InformeResp response) {

            }

            @Override
            public void onError() {

            }
        });
        inf.startDialog();
    }
    private void _GenerarCierreZ() {

        //por un tema de orden de los botones en pantalla mezclamos los ids
        final int OPCION_SI = AlertDialog.BUTTON_NEGATIVE;
        final int OPCION_NO = AlertDialog.BUTTON_NEUTRAL;
        final int OPCION_CANCELAR = AlertDialog.BUTTON_POSITIVE;

        //vamos a presentar un dialogo para que el usuario confirme la operacion, e ingrese vales manuales si lo desea
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        // Get the layout inflater
        LayoutInflater inflater = MainActivity.this.getLayoutInflater();

        // Inflate and set the layout for the dialog
        View cierreView = inflater.inflate(R.layout.dialog_cierre_z, null);

        ((TextView) cierreView.findViewById(R.id.dialog_cierre_z_message)).setText(Html.fromHtml(getString(R.string.cierre_z_msj_1)));

        builder.setView(cierreView);

        final AlertDialog dialog = builder.create();

        dialog.setTitle("Cierre Z");
        dialog.setIcon(android.R.drawable.ic_menu_help);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setOnCancelListener(null);

        dialog.setButton(OPCION_SI, "SI", (DialogInterface.OnClickListener) null);
        dialog.setButton(OPCION_NO, "NO", (DialogInterface.OnClickListener) null);
        dialog.setButton(OPCION_CANCELAR, "CANCELAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        //asignamos los eventos al dialog
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface d) {

                dialog.getButton(OPCION_NO).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();

                        //lanzamos el cierre Z sin ventas manuales
                        InformeReq informeReq = getRequestZ();
                        EnviarCierreZ(informeReq);
                    }
                });

                dialog.getButton(OPCION_SI).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //mostramos la seccion de declaracion de emisiones manuales
                        dialog.findViewById(R.id.dialgo_cierre_z_sec_manual).setVisibility(View.VISIBLE);

                        //ocultamos el NO
                        dialog.getButton(OPCION_NO).setVisibility(View.GONE);

                        //cambiamos el SI
                        Button btnPositive = dialog.getButton(OPCION_SI);
                        btnPositive.setText("ACEPTAR");
                        btnPositive.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                try {
                                    //tenemos que capturar y validar los datos ingresados

                                    int cant_registros_afectos_manuales = 0, cant_registros_no_afectos_manuales = 0;
                                    double total_afecto_vales_manuales = 0d, total_no_afecto_vales_manuales = 0d;

                                    ((EditText) dialog.findViewById(R.id.dialog_cierre_z_cant_afecto)).setError(null);
                                    ((EditText) dialog.findViewById(R.id.dialog_cierre_z_cant_exento)).setError(null);
                                    ((EditText) dialog.findViewById(R.id.dialog_cierre_z_tot_exento)).setError(null);
                                    ((EditText) dialog.findViewById(R.id.dialog_cierre_z_tot_afecto)).setError(null);

                                    String s_cant_afecto = ((EditText) dialog.findViewById(R.id.dialog_cierre_z_cant_afecto)).getText().toString().trim();
                                    String s_cant_no_afecto = ((EditText) dialog.findViewById(R.id.dialog_cierre_z_cant_exento)).getText().toString().trim();
                                    String s_monto_afecto = ((EditText) dialog.findViewById(R.id.dialog_cierre_z_tot_afecto)).getText().toString().trim();
                                    String s_monto_no_afecto = ((EditText) dialog.findViewById(R.id.dialog_cierre_z_tot_exento)).getText().toString().trim();

                                    try {
                                        cant_registros_afectos_manuales = Integer.parseInt(s_cant_afecto);
                                    } catch (NumberFormatException nfe) {
                                        cant_registros_afectos_manuales = 0;
                                    }

                                    try {
                                        cant_registros_no_afectos_manuales = Integer.parseInt(s_cant_no_afecto);
                                    } catch (NumberFormatException nfe) {
                                        cant_registros_no_afectos_manuales = 0;
                                    }

                                    try {
                                        total_afecto_vales_manuales = Double.parseDouble(s_monto_afecto);
                                    } catch (NumberFormatException nfe) {
                                        total_afecto_vales_manuales = 0d;
                                    }

                                    try {
                                        total_no_afecto_vales_manuales = Double.parseDouble(s_monto_no_afecto);
                                    } catch (NumberFormatException nfe) {
                                        total_no_afecto_vales_manuales = 0d;
                                    }

                                    //validacion cruzada, si cargaron cantidad afecta , tienen que cargar monto afecto y viceversa
                                    if(cant_registros_afectos_manuales > 0 && total_afecto_vales_manuales < 1){
                                        ((EditText) dialog.findViewById(R.id.dialog_cierre_z_tot_afecto)).setError("Ingrese total monto afecto");
                                        return;
                                    }
                                    if(total_afecto_vales_manuales > 0 && cant_registros_afectos_manuales < 1){
                                        ((EditText) dialog.findViewById(R.id.dialog_cierre_z_cant_afecto)).setError("Ingrese cantidad registros afectos");
                                        return;
                                    }

                                    //validacion cruzada, si cargaron cantidad exenta, tienen que cargar monto exento y viceversa
                                    if(cant_registros_no_afectos_manuales > 0 && total_no_afecto_vales_manuales < 1){
                                        ((EditText) dialog.findViewById(R.id.dialog_cierre_z_tot_exento)).setError("Ingrese total monto exento");
                                        return;
                                    }
                                    if(total_no_afecto_vales_manuales > 0 && cant_registros_no_afectos_manuales < 1){
                                        ((EditText) dialog.findViewById(R.id.dialog_cierre_z_cant_exento)).setError("Ingrese cantidad registros no afectos");
                                        return;
                                    }

                                    //vemos si alguno tiene valor cargado para mostrar allí el error
                                    if (cant_registros_afectos_manuales + cant_registros_no_afectos_manuales < 1) {
                                        if (TextUtils.isEmpty(s_cant_afecto) && !TextUtils.isEmpty(s_cant_no_afecto))
                                            ((EditText) dialog.findViewById(R.id.dialog_cierre_z_cant_exento)).setError("Cantidad ingresada errónea");
                                        else
                                            ((EditText) dialog.findViewById(R.id.dialog_cierre_z_cant_afecto)).setError("Cantidad ingresada errónea");
                                        return;
                                    }

                                    //vemos si alguno tiene valor cargado para mostrar allí el error
                                    if (total_afecto_vales_manuales + total_no_afecto_vales_manuales < 1) {
                                        if (TextUtils.isEmpty(s_monto_afecto) && !TextUtils.isEmpty(s_monto_no_afecto))
                                            ((EditText) dialog.findViewById(R.id.dialog_cierre_z_tot_exento)).setError("Monto total ingresado erróneo");
                                        else
                                            ((EditText) dialog.findViewById(R.id.dialog_cierre_z_tot_afecto)).setError("Monto total ingresado erróneo");
                                        return;
                                    }


                                    dialog.dismiss();

                                    //lanzamos el cierre Z con los valores ingresados para ventas manuales
                                    InformeReq informeReq = getRequestZ();
                                    informeReq.setReqZ(cant_registros_afectos_manuales, total_afecto_vales_manuales, cant_registros_no_afectos_manuales, total_no_afecto_vales_manuales);
                                    EnviarCierreZ(informeReq);

                                } catch (Exception ex) {
                                    Toast.makeText(MainActivity.this, "Error : " + ex.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }
                        });


                    }
                });
            }
        });

        dialog.show();

    }

    private void EnviarCierreZ(InformeReq cierreZ){


        CierreZtask inf = new CierreZtask(MainActivity.this,config, false, new InformeListener() {
            @Override
            public void onConsultaCompletada(final String response) {

            }

            @Override
            public void onConsultaCompletada(final InformeResp response) {

                final Date cierre_z = new Date();

                setUltCierreZ(cierre_z);//--> grabamos la fecha de ult cierre Z

                ValeUtils.SetUltimoCodOperacion(MainActivity.this, getContentResolver(), response.ULT_TRACE_ID);//--> actualizamos el ultimo traceid

                //mostramos los resultados en pantalla, impresion opcional
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                // Get the layout inflater
                LayoutInflater inflater = MainActivity.this.getLayoutInflater();

                // Inflate and set the layout for the dialog
                View cierreResultView = inflater.inflate(R.layout.cierre_z_result, null);

                ((TextView) cierreResultView.findViewById(R.id.cierre_result_msj)).setText("Cierre Z realizado correctamente. A continuación los resultados:");
                ((TextView) cierreResultView.findViewById(R.id.cierre_result_total_ventas)).setText(Formatos.ParseaFormateaDecimal(response.TOTAL, Formatos.DecimalFormat_US, Formatos.DecimalFormat_CH, "$"));
                ((TextView) cierreResultView.findViewById(R.id.cierre_result_vale_ini)).setText(response.VALE_INI);
                ((TextView) cierreResultView.findViewById(R.id.cierre_result_vale_fin)).setText(response.VALE_FIN);
                ((TextView) cierreResultView.findViewById(R.id.cierre_result_periodo)).setText(response.PERIODO);
                ((TextView) cierreResultView.findViewById(R.id.cierre_result_periodo_ventas)).setText(Formatos.ParseaFormateaDecimal(response.TOT_ACUM, Formatos.DecimalFormat_US, Formatos.DecimalFormat_CH, "$"));

                builder.setView(cierreResultView);

                String nro_lote = "00000" + response.ULT_LOTE;
                nro_lote = nro_lote.substring(nro_lote.length() - 5);

                AlertDialog dialog = builder.create();
                dialog.setTitle("Cierre Z Nro. " + nro_lote);
                dialog.setIcon(android.R.drawable.ic_menu_info_details);
                dialog.setCanceledOnTouchOutside(false);
                dialog.setCancelable(false);
                dialog.setOnCancelListener(null);

                dialog.setButton(AlertDialog.BUTTON_NEUTRAL, "ACEPTAR", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                dialog.setButton(AlertDialog.BUTTON_POSITIVE, "IMPRIMIR", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        imprimirCierreZ(response, cierre_z);
                    }
                });

                dialog.show();

            }

            @Override
            public void onError() {
            }
        });
        //inf.execute(cierreZ);
        inf.startDialog();
    }

    private void imprimirCierreZ(final InformeResp response, final Date cierre_z){
        try {

            final Printer _printer = Printer.getInstance(config.PRINTER_MODEL, MainActivity.this);

            _printer.delayActivated = true;
            _printer.printerCallback = new Printer.PrinterCallback() {
                @Override
                public void onPrinterReady() {
                    _printer.lineFeed();
                    _printer.lineFeed();

                    _printer.format(1, 1, Printer.ALINEACION.CENTER);

                    //titulo
                    _printer.printLine(config.COMERCIO);
                    _printer.printLine("R.U.T.: " + ValeUtils.FormatRUT(config.CLAVEFISCAL));

                    //actividades
                    if (!TextUtils.isEmpty(config.SII_GIRO1))
                        _printer.printLine(config.SII_GIRO1);
                    if (!TextUtils.isEmpty(config.SII_GIRO2))
                        _printer.printLine(config.SII_GIRO2);
                    if (!TextUtils.isEmpty(config.SII_GIRO3))
                        _printer.printLine(config.SII_GIRO3);


                    if (_printer.get_longitudLinea() - config.DIRECCION.length() < 2) {
                        _printer.format(1, 1, Printer.ALINEACION.LEFT);
                        _printer.printLine(config.DIRECCION);
                    } else
                        _printer.print(config.DIRECCION);

                    if(!TextUtils.isEmpty(config.LOCALIDAD)) {
                        if (_printer.get_longitudLinea() - config.LOCALIDAD.length() < 2) {
                            _printer.format(1, 1, Printer.ALINEACION.LEFT);
                            _printer.printLine(config.LOCALIDAD);
                        } else
                            _printer.print(config.LOCALIDAD);
                    }

                    _printer.sendSeparadorHorizontal('=');
                    _printer.format(1, 1, Printer.ALINEACION.CENTER);
                    String nro_lote = "00000" + response.ULT_LOTE;
                    nro_lote = nro_lote.substring(nro_lote.length() - 5);
                    _printer.printLine("INFORME Z Nro. " + nro_lote);
                    _printer.sendSeparadorHorizontal('=');
                    _printer.format(1, 1, Printer.ALINEACION.CENTER);
                    _printer.printLine(Formatos.FormateaDate(cierre_z, Formatos.DateFormatSlashAndTimeSec));
                    _printer.printLine("TER: " + config.SII_TERMINAL_ID);
                    _printer.sendSeparadorHorizontal();
                    _printer.format(1, 1, Printer.ALINEACION.CENTER);
                    _printer.printLine("INFORME DE TOTALES FISCALES");
                    _printer.lineFeed();

                    _printer.cancelCurrentFormat();


                    //DATOS
                    _printer.printOpposite("VENTAS    :", Formatos.ParseaFormateaDecimal(response.TOTAL, Formatos.DecimalFormat_US, Formatos.DecimalFormat_CH, "$"));
                    _printer.printOpposite("VALE INIC.:", response.VALE_INI);
                    _printer.printOpposite("VALE FINAL:", response.VALE_FIN);
                    _printer.sendSeparadorHorizontal();

                    //TOTALES POR MEDIO PAGO
                    _printer.format(1, 1, Printer.ALINEACION.CENTER);
                    _printer.printLine("TOTALES POR FORMA DE PAGO");
                    _printer.lineFeed();

                    _printer.cancelCurrentFormat();

                    if (!TextUtils.isEmpty(response.DET_TIPO_PAGO)) {
                        String[] transacciones = response.DET_TIPO_PAGO.split("#");//#<medio>*<total>

                        for (String transaccion : transacciones) {
                            if (!TextUtils.isEmpty(transaccion)) {
                                String[] datos = transaccion.split("\\*");

                                //transaccion
                                String medio_pago = datos[0] + "                ";
                                medio_pago = medio_pago.substring(0, 17);
                                _printer.printOpposite(medio_pago + ": $", Formatos.ParseaFormateaDecimal(datos[1], Formatos.DecimalFormat_US, Formatos.DecimalFormat_CH));
                            }
                        }

                    }

                    _printer.sendSeparadorHorizontal();

                    //TOTALES PARCIALES DEL PERIODO
                    _printer.format(1, 1, Printer.ALINEACION.CENTER);
                    _printer.printLine("TOTAL DEL PERIODO FISCAL");
                    _printer.printLine(response.PERIODO);
                    _printer.lineFeed();

                    _printer.printOpposite("TOTAL ACUM.:", Formatos.ParseaFormateaDecimal(response.TOT_ACUM, Formatos.DecimalFormat_US, Formatos.DecimalFormat_CH, "$"));

                    _printer.sendSeparadorHorizontal('=');

                    _printer.footer();

                    _printer.end();
                }
            };

            //iniciamos la printer
            if (!_printer.initialize()) {
                Toast.makeText(MainActivity.this, "Error al inicializar", Toast.LENGTH_SHORT).show();

                _printer.end();
            }

        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void imprimirInformeX(final InformeResp response){

        try {

            final Printer _printer = Printer.getInstance(config.PRINTER_MODEL, MainActivity.this);

            _printer.delayActivated = true;
            _printer.printerCallback = new Printer.PrinterCallback() {
                @Override
                public void onPrinterReady() {
                    _printer.lineFeed();
                    _printer.lineFeed();

                    _printer.format(1, 1, Printer.ALINEACION.CENTER);

                    //titulo
                    _printer.printLine(config.COMERCIO);
                    _printer.printLine("R.U.T.: " + ValeUtils.FormatRUT(config.CLAVEFISCAL));
                    _printer.sendSeparadorHorizontal('=');
                    _printer.format(1, 1, Printer.ALINEACION.CENTER);
                    _printer.printLine("INFORME X");
                    _printer.sendSeparadorHorizontal('=');
                    _printer.format(1, 1, Printer.ALINEACION.CENTER);
                    _printer.printLine(Formatos.FormateaDate(new Date(), Formatos.DateFormatSlashAndTimeSec));
                    _printer.printLine("TER: " + config.SII_TERMINAL_ID);
                    _printer.sendSeparadorHorizontal();
                    _printer.format(1, 1, Printer.ALINEACION.CENTER);
                    _printer.printLine("INFORME DE TOTALES FISCALES");
                    _printer.lineFeed();

                    _printer.cancelCurrentFormat();


                    //DATOS
                    _printer.printOpposite("VALE INIC.:", response.VALE_INI);
                    _printer.printOpposite("VALE FINAL:", response.VALE_FIN);
                    String nro_lote = "00000" + response.ULT_LOTE;
                    nro_lote = nro_lote.substring(nro_lote.length() - 5);
                    _printer.printOpposite("Nro Informes Z:", nro_lote);

                    _printer.printOpposite("VENTAS TOTALES:", Formatos.ParseaFormateaDecimal(response.TOTAL, Formatos.DecimalFormat_US, Formatos.DecimalFormat_CH, "$"));
                    _printer.printOpposite("   AFECTAS:", Formatos.ParseaFormateaDecimal(response.TOTAL_AFECTO, Formatos.DecimalFormat_US, Formatos.DecimalFormat_CH, "$"));
                    _printer.printOpposite("   NO AFECTAS:", Formatos.ParseaFormateaDecimal(response.TOTAL_NO_AFECTO, Formatos.DecimalFormat_US, Formatos.DecimalFormat_CH, "$"));

                    _printer.sendSeparadorHorizontal();


                    //TOTALES POR MEDIO PAGO
                    _printer.format(1, 1, Printer.ALINEACION.CENTER);
                    _printer.printLine("TOTALES POR FORMA DE PAGO");
                    _printer.lineFeed();

                    _printer.cancelCurrentFormat();

                    if (!TextUtils.isEmpty(response.DET_TIPO_PAGO)) {
                        String[] transacciones = response.DET_TIPO_PAGO.split("#");//#<medio>*<total>

                        for (String transaccion : transacciones) {
                            if (!TextUtils.isEmpty(transaccion)) {
                                String[] datos = transaccion.split("\\*");

                                //transaccion
                                String medio_pago = datos[0] + "                ";
                                medio_pago = medio_pago.substring(0, 17);
                                _printer.printOpposite(medio_pago + ": $", Formatos.ParseaFormateaDecimal(datos[1], Formatos.DecimalFormat_US, Formatos.DecimalFormat_CH));
                            }
                        }

                    }

                    _printer.sendSeparadorHorizontal('=');

                    _printer.footer();

                    _printer.end();
                }
            };

            //iniciamos la printer
            if (!_printer.initialize()) {
                Toast.makeText(MainActivity.this, "Error al inicializar", Toast.LENGTH_SHORT).show();

                _printer.end();
            }

        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void ReimprimirUltimoVale() {

        InformeReq informeReq = getRequest();

        GetUltValeTask getUltValeTask = new GetUltValeTask(MainActivity.this, new GetUltValeTask.GetValeListener() {
            @Override
            public void onValeRecuperado(Vale vale) {
                try {

                    ValeTicketBuilder ticket = new ValeTicketBuilder(MainActivity.this, config, ValeUtils.getAppVersion(MainActivity.this), config.SII_MERCHANT_ID);

                    vale.setDatosGenerales(config, "", "", config.SII_MERCHANT_ID);

                    ticket.LoadVale(vale);

                    //Printer _printer = Printer.getInstance(config.PRINTER_MODEL, MainActivity.this);

                    ticket.Print(config.PRINTER_MODEL);

                } catch (Exception ex) {
                    Toast.makeText(MainActivity.this, "Error al reimprimir vale: " + ex.toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError() {

            }
        });

        getUltValeTask.execute(informeReq);
    }

    private void Init_Metodos() {

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnDetalleTrxs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImprimirDetalleTransacciones();
            }
        });

        btnCierreX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GenerarCierreX();
            }
        });

        btnCierreZ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GenerarCierreZ();
            }
        });

        btnReimprimirUltimoVale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ReimprimirUltimoVale();
            }
        });
    }

    private void AlertMessage(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setTitle(title);
        builder.setMessage(Html.fromHtml(message));
        builder.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface _dialog, int which) {
                _dialog.dismiss();
            }
        });

        builder.setCancelable(false);
        builder.show();
    }

    private void ServicioNoHabilitadoAlertMessage() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setTitle(getString(R.string.vale_electronico));
        builder.setMessage(Html.fromHtml(getString(R.string.vale_no_habilitado)));
        builder.setPositiveButton(getString(R.string.button_accept), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface _dialog, int which) {
                _dialog.dismiss();

                MainActivity.this.finish();
            }
        });

        builder.setCancelable(false);
        builder.show();
    }

    private void TerminalNoInicializadoAlertMessage(final boolean esModalidadDemo) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setTitle(getString(esModalidadDemo ? R.string.vale_activac_serv_demo : R.string.vale_activac_serv));
        builder.setMessage(Html.fromHtml(getString(R.string.vale_no_activado)));
        builder.setPositiveButton(getString(R.string.vale_activac_opt_luego), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface _dialog, int which) {
                _dialog.dismiss();

                Dialog.Alert(MainActivity.this, getString(esModalidadDemo ? R.string.vale_activac_serv_atencion_demo : R.string.vale_activac_serv_atencion), getString(R.string.vale_activac_serv_recuerde), android.R.drawable.ic_dialog_alert, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        MainActivity.this.finish();
                    }
                });
            }
        });
        builder.setNeutralButton(getString(R.string.vale_activac_opt_ahora), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface _dialog, int which) {
                _dialog.dismiss();

                InicializarTerminal(esModalidadDemo);
            }
        });


        builder.setCancelable(false);
        builder.show();
    }

    private void InicializarTerminal(final boolean esModalidadDemo){
        InitTerminalReq req = new InitTerminalReq(config.SII_MERCHANT_ID, config, ValeUtils.GetUserID(config.SII_TERMINAL_ID, getContentResolver()));

        InicializarTerminalTask inicializarTerminalTask = new InicializarTerminalTask(this, config.isValeModalidadDemo(), new InitTerminalListener() {
            @Override
            public void onInitTerminalCompletado(InitTerminalResp resp) {
                Dialog.Alert(MainActivity.this, getString(esModalidadDemo ? R.string.vale_activac_serv_demo : R.string.vale_activac_serv), getString(R.string.vale_activac_serv_bienvenida), android.R.drawable.ic_dialog_info, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mostrarOpciones();
                    }
                });
            }

            @Override
            public void onError(boolean reintentarAhora) {

                if (reintentarAhora) {
                    InicializarTerminal(esModalidadDemo);//repetimos el step
                } else {
                    //reintentar luego
                    Dialog.Alert(MainActivity.this, getString(esModalidadDemo ? R.string.vale_activac_serv_atencion_demo : R.string.vale_activac_serv_atencion), getString(R.string.vale_activac_serv_recuerde), android.R.drawable.ic_dialog_alert, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            MainActivity.this.finish();
                        }
                    });
                }

            }
        }, config.SII_MERCHANT_ID);

        inicializarTerminalTask.execute(req);
    }

    /*public Date getUltCierreZ() {

        Date date_default = new Date();
        //date_default.setTime(1420070400000L); //---> 01/01/2015 00:00
        date_default.setTime(10800000L); //---> 01/01/1970 00:00 GMT-3

        try {
            SharedPreferences pref = getSharedPreferences(ValeConfig.SHARED_PREF_NAME, 0);
            String last_cierre_z_time_string = pref.getString(ValeConfig.VALE_ULT_CIERRE_Z, Formatos.FormateaDate(date_default, Formatos.DbDateTimeFormatTimeZone));

            //si es vacio, va a romper
            return Formatos.ObtieneDate(last_cierre_z_time_string, Formatos.DbDateTimeFormatTimeZone);
        } catch (Exception e) {
            return date_default;
        }
    }*/

    public void setUltCierreZ(Date ultCierreZ) {

        SharedPreferences.Editor editor = getSharedPreferences(ValeConfig.SHARED_PREF_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(ValeConfig.VALE_ULT_CIERRE_Z, Formatos.FormateaDate(ultCierreZ, Formatos.DbDateTimeFormatTimeZone));
        editor.apply();

    }
}
