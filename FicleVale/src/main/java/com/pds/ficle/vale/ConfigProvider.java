package com.pds.ficle.vale;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;

import com.pds.common.pref.ValeConfig;

import java.util.Map;
import java.util.Set;

/**
 * Created by Hernan on 16/12/2014.
 */
public class ConfigProvider extends ContentProvider {

    private static final int ALL = 1;
    private static final int ID = 2;

    static final UriMatcher sUriMatcher;

    static {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(ValeConfig.PROVIDER_NAME, ValeConfig.BASE_PATH, ALL);
        sUriMatcher.addURI(ValeConfig.PROVIDER_NAME, ValeConfig.BASE_PATH + "/#", ID);
    }


    private SharedPreferences getSharedPreferences() {
        return getContext().getSharedPreferences(ValeConfig.SHARED_PREF_NAME, 0);
    }

    @Override
    public boolean onCreate() {
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        Set<? extends Map.Entry<String, ?>> values = getSharedPreferences().getAll().entrySet();

        String[] _columns = new String[values.size()];
        String[] _values = new String[values.size()];

        int i = 0;
        for (Map.Entry<String, ?> entry : values) {

            _columns[i] = entry.getKey();

            Object val = entry.getValue();
            _values[i++] = val != null ? String.valueOf(val) : "";
        }

        MatrixCursor matrixCursor = new MatrixCursor(_columns);
        matrixCursor.addRow(_values);

        return matrixCursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues cv) {
        SharedPreferences.Editor edit = getSharedPreferences().edit();

        //Set<String> keys = cv.keySet();

        for (String key : cv.keySet()) {
            edit.putString(key, cv.getAsString(key));
        }

        edit.commit();

        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
