package com.pds.remote.common;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Log;
import android.widget.Toast;

import com.pds.remote.client.ClientDisplayService;


/**
 * Created by Hernan on 12/04/2014.
 */
public class BootUpReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        //esta clase recibe el aviso que Android termino de iniciar y realiza una serie de acciones
        //1.START MODO PUBLICIDAD
        //StartSureVideo(context);
        //2.START SERVICE
        StartDisplayService(context);
    }

    private void StartDisplayService(Context context) {
        try {
            Intent i = new Intent(context, ClientDisplayService.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startService(i);
        }catch (Exception ex) {
            Log.e("Display",ex.toString());
        }
    }

    private void StartSureVideo(Context context) {
        try {
            Intent i = new Intent();
            i.setAction(Intent.ACTION_MAIN);
            i.setComponent(new ComponentName("com.gears42.surevideo", "com.gears42.surevideo/.SureVideoScreen"));
            context.startActivity(i);
        } catch (Exception ex) {
            Toast.makeText(context, "Error al iniciar SureVideo: " + ex.toString(), Toast.LENGTH_LONG).show();
        }
    }
}
