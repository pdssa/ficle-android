package com.pds.remote.common;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.pds.remote.client.TicketActivity;

/**
 * Created by Hernan on 14/04/2015.
 */
public class CloseScreenReceiver extends BroadcastReceiver {

    private Activity mScreen;

    public static String intentCloseAction = "com.pds.action.screen.close";
    public static String intentTicketAction = "com.pds.action.ticket.show";

    public static IntentFilter intentFilter = new IntentFilter(intentCloseAction);

    public CloseScreenReceiver(){

    }
    public CloseScreenReceiver(Activity screen){
        this.mScreen = screen;

        //filter actions
        //this.mIntentFilter = new IntentFilter();
        //this.mIntentFilter.addAction(intentCloseAction);
        //this.mIntentFilter.addAction(intentTicketAction);
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        if(intent.getAction().equalsIgnoreCase(intentCloseAction)) {
            this.mScreen.finish();
        }
        /*else if(intent.getAction().equalsIgnoreCase(intentTicketAction)) {
            if(this.mScreen != null) {
                ((TicketActivity)mScreen).updateContent(intent);
            }
            else{
                Intent i = new Intent(context, TicketActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                i.putExtras(intent.getExtras());

                context.startActivity(i);
            }
        }*/
    }
}
