package com.pds.remote.client;

import android.content.Context;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceRequest;
import android.util.Log;
import android.widget.Toast;

import com.pds.remote.common.WiFiDirectBroadcastReceiver;
import com.pds.remote.common.WiFiP2pService;

import java.util.Map;


/**
 * Created by Hernan on 11/07/2014.
 */
public class ClientServiceHandler {
    private final String DEBUG_TAG = ClientServiceHandler.class.getName();

    // TXT RECORD properties
    public static final String TXTRECORD_PROP_AVAILABLE = "available";
    public static final String SERVICE_INSTANCE = "_PDSSERVER";
    public static final String SERVICE_REG_TYPE = "_presence._tcp";

    public static final String DEVICE_SERVICE_PREFIX = "BPOS";

    private WifiP2pManager mManager;
    private WifiP2pManager.Channel mChannel;
    private Context mContext;
    private WifiP2pDnsSdServiceRequest mServiceRequest;

    public ClientServiceHandler(Context context, WifiP2pManager manager, WifiP2pManager.Channel channel) {
        mContext = context;
        mManager = manager;
        mChannel = channel;

        try {
            //WifiManager wifi = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
            //WifiInfo wifiInfo = wifi.getConnectionInfo();
            //int intaddr = wifiInfo.getIpAddress();

        } catch (Exception e) {
            Log.d(DEBUG_TAG, "Error in ServerServiceHandler creation: " + e);
        }
    }

    public void appendStatus(String status) {
        Log.d(DEBUG_TAG, status);
        Toast.makeText(mContext, status, Toast.LENGTH_SHORT).show();
    }

    /**
     * Initiates a service discovery
     */
    public void discoverService() {

        /*
         * Register listeners for DNS-SD services. These are callbacks invoked
         * by the system when a service is actually discovered.
         */

        this.mManager.setDnsSdResponseListeners(this.mChannel,
                new WifiP2pManager.DnsSdServiceResponseListener() {

                    @Override
                    public void onDnsSdServiceAvailable(String instanceName, String registrationType, WifiP2pDevice srcDevice) {
                        //appendStatus("Inst: " + instanceName + " Dev:" +  srcDevice.deviceName);
                        // A service has been discovered. Is this our app?
                        if (instanceName.equalsIgnoreCase(SERVICE_INSTANCE) &&
                                srcDevice.deviceName.startsWith(DEVICE_SERVICE_PREFIX)) {

                            appendStatus("onBonjourServiceAvailable " + instanceName);

                            WiFiP2pService service = new WiFiP2pService();
                            service.device = srcDevice;
                            service.instanceName = instanceName;
                            service.serviceRegistrationType = registrationType;

                            connectP2pService(service);

                        }
                    }
                },
                new WifiP2pManager.DnsSdTxtRecordListener() {

                    /* Callback includes:
                     * fullDomain: full domain name: e.g "printer._ipp._tcp.local."
                     * record: TXT record dta as a map of key/value pairs.
                     * device: The device running the advertised service.
                     */
                    @Override
                    public void onDnsSdTxtRecordAvailable(String fullDomainName, Map<String, String> record, WifiP2pDevice device) {
                        //appendStatus("TEST 2" );
                        Log.d(DEBUG_TAG, device.deviceName + " is " + record.get(TXTRECORD_PROP_AVAILABLE));
                    }
                });

        // After attaching listeners, create a service request and initiate discovery.

        removeRequest();//removing previous request, if exists, before

        this.mServiceRequest = WifiP2pDnsSdServiceRequest.newInstance();
        this.mManager.addServiceRequest(this.mChannel, this.mServiceRequest,addServiceRequestActionListener);
        this.mManager.discoverServices(this.mChannel, discoverServiceActionListener);
    }

    /**
     * Connect to an available service
     */
    public void connectP2pService(WiFiP2pService service) {

        WifiP2pConfig config = new WifiP2pConfig();

        config.deviceAddress = service.device.deviceAddress;
        config.wps.setup = WpsInfo.PBC;

        removeRequest();

        this.mManager.connect(this.mChannel, config, connectServiceRequestActionListener);
    }

    /**
     * Stop service requesting
     */
    private void removeRequest(){
        if (this.mServiceRequest != null)
            this.mManager.removeServiceRequest(this.mChannel, this.mServiceRequest, removeServiceRequestActionListener);
    }

    /**
     * Stop service discovery
     */
    public void stopDiscovery(){
        removeRequest();
    }

    private WifiP2pManager.ActionListener discoverServiceActionListener = new WifiP2pManager.ActionListener() {
        @Override
        public void onSuccess() {appendStatus("Service discovery initiated");}

        @Override
        public void onFailure(int errorCode) {appendStatus("Service discovery failed. Error:" + WiFiDirectBroadcastReceiver.getErrorResultDescription(errorCode));}
    };

    private WifiP2pManager.ActionListener addServiceRequestActionListener = new WifiP2pManager.ActionListener() {
        @Override
        public void onSuccess() {appendStatus("Added service discovery request");}

        @Override
        public void onFailure(int errorCode) {appendStatus("Failed adding service discovery request. Error:" + WiFiDirectBroadcastReceiver.getErrorResultDescription(errorCode));}
    };

    private WifiP2pManager.ActionListener connectServiceRequestActionListener = new WifiP2pManager.ActionListener() {
        @Override
        public void onSuccess() {appendStatus("Connecting to service");}

        @Override
        public void onFailure(int errorCode) {appendStatus("Failed connecting to service. Error:" + WiFiDirectBroadcastReceiver.getErrorResultDescription(errorCode));}
    };

    private WifiP2pManager.ActionListener removeServiceRequestActionListener = new WifiP2pManager.ActionListener() {
        @Override
        public void onSuccess() {}

        @Override
        public void onFailure(int error) {}
    };
}
