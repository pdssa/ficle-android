package com.pds.remote.client;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hernan on 31/03/2015.
 */
public class ViewLoader {

    private final String VIEW_ID_PROP = "idView";
    private final String VIEW_TYPE_PROP = "classTypeView";
    private final String VIEW_CONTENT_PROP = "viewContent";
    private final String CONTENT_TYPE_PROP = "viewContentType";

    private Activity _context;

    public ViewLoader(Activity context) {
        _context = context;
    }

    private View getViewById(String viewId) {
        return getViewById(viewId, null);
    }

    private View getViewById(String viewId, View parent) {
        int id = _context.getResources().getIdentifier(viewId, "id", _context.getPackageName());
        return parent != null ? parent.findViewById(id) : _context.findViewById(id);
    }

    private int getLayoutId(String layoutId) {
        return _context.getResources().getIdentifier(layoutId, "layout", _context.getPackageName());
    }

    public void LoadContent(JSONArray jsonArray) throws Exception {
        LoadContent(jsonArray, null);
    }

    public void LoadContent(JSONArray jsonArray, View parent) throws Exception {
        for (int i = 0; i < jsonArray.length(); i++) {
            //cargamos cada objeto
            LoadContent(jsonArray.getJSONObject(i), parent);
        }
    }

    public void LoadContent(JSONObject object) throws Exception {
        LoadContent(object, null);
    }

    public void LoadContent(JSONObject object, View parent) throws Exception {
        try {
            String viewId = object.getString(VIEW_ID_PROP); //id de la vista
            String viewType = object.getString(VIEW_TYPE_PROP);//class de la vista
            Object value = object.get(VIEW_CONTENT_PROP); //content
            String valueType = object.getString(CONTENT_TYPE_PROP); //class del content

            //obtenemos la vista por ID
            View view = getViewById(viewId, parent);

            Class<?> _class = Class.forName(viewType);

            //seteamos el value via reflection
            String methodName = "";

            if (view instanceof EditText || view instanceof TextView) {
                methodName = "setText";
            } else if (view instanceof ListView) {

                //en content tenemos la lista nombre del layout del item a cargar
                JSONArray array = object.getJSONArray(VIEW_CONTENT_PROP);

                List<JSONObject> list = new ArrayList<JSONObject>();
                for (int i = 0; i < array.length(); i++) {
                    list.add(array.getJSONObject(i));//cada objeto de la lista es un ITEM
                }

                //en valueType tenemos el nombre del layout del item
                ((ListView) view).setAdapter(new ListAdapter(_context, list, getLayoutId(valueType)));

                if(list.size() > 0) //si hubieron items => mostramos la lista
                    view.setVisibility(View.VISIBLE);

                return;

            } else //tipo no compatible aún
                return;

            if (!TextUtils.isEmpty(methodName)) {
                Method _method = _class.getMethod(methodName, Class.forName(valueType));
                _method.invoke(view, value);
            }

        } catch (Exception ex) {
            throw ex;
        }
    }


    //VIA REFLECTION
                /*Class _class = this.getClass();
                Field _field = _class.getDeclaredField(idView);
                _field.setAccessible(true);

                setValue(_class, )

                //
                Class<?> __class = Class.forName(typeView);
                //Field __field = __class.getDeclaredField("Text");
                Method _method = __class.getMethod("setText", Class.forName("java.lang.CharSequence"));

                _method.invoke(_field,value);

                //__field.set(_field, value);
                */


    class ListAdapter extends BaseAdapter {
        private int _layoutId;
        private LayoutInflater _inflater;
        private Context _context;
        private List<JSONObject> _datos; //cada item de la lista es un objeto "item" con un array con distintos objetos (view)
        private ViewLoader _loader;

        public ListAdapter(Context context, List<JSONObject> datos, int layout_id) {
            super();

            this._inflater = LayoutInflater.from(context);
            this._context = context;
            this._datos = datos;
            this._layoutId = layout_id;
            this._loader = new ViewLoader((Activity) context);
        }

        @Override
        public View getView(int position, View item, ViewGroup viewGroup) {
            try {
                //reciclamos la vista
                if (item == null) {
                    item = this._inflater.inflate(_layoutId, null);
                }

                //el objeto tiene un solo atributo, llamado "item" que tiene un array de objetos view
                JSONArray itemViews = _datos.get(position).getJSONArray("item");

                //cargamos el contenido del item
                _loader.LoadContent(itemViews, item);

            } catch (Exception e) {
                e.printStackTrace();
            }

            // Devolvemos la vista para que se muestre en el ListView.
            return item;
        }

        @Override
        public int getCount() {
            return this._datos.size();
        }

        @Override
        public Object getItem(int i) {
            return this._datos.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }


    }

}
