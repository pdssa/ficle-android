package com.pds.remote.client;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceRequest;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.remote.common.Connection;
import com.pds.remote.common.Message;
import com.pds.remote.common.MessageHandler;
import com.pds.remote.common.WiFiDirectBroadcastReceiver;
import com.pds.remote.old.Communication;
import com.pds.remote.sserver.ServerServiceHandler;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

//, Handler.Callback, ChatManager.MessageTarget
public class _MainActivity extends Activity
        implements WifiP2pManager.ConnectionInfoListener, Connection.ConnectionListener, WiFiDirectBroadcastReceiver.WifiDirectListener {

    //NsdManager mNsdManager;
    //NsdManager.ResolveListener mResolveListener;
    //NsdManager.DiscoveryListener mDiscoveryListener;
    //NsdManager.RegistrationListener mRegistrationListener;
    //NsdServiceInfo mService;
    //private Handler handler = new Handler(this);

    /*public Handler getHandler() {
        return handler;
    }*/

    private static final String DEBUG_TAG = _MainActivity.class.getName();
    private static final String TAG = "Wifi-P2P";


    private WifiP2pManager mManager;
    private WifiP2pManager.Channel mChannel;
    private WiFiDirectBroadcastReceiver mReceiver;
    private IntentFilter mIntentFilter;
    private WifiP2pDnsSdServiceRequest mServiceRequest;
    private List<android.net.wifi.p2p.WifiP2pDevice> mPeersList = new ArrayList<android.net.wifi.p2p.WifiP2pDevice>();

    private WifiP2pDevice mServerPeer;


    // TXT RECORD properties
    //public static final String TXTRECORD_PROP_AVAILABLE = "available";
    //public static final String SERVICE_INSTANCE = "_wifidemotest";
    //public static final String SERVICE_REG_TYPE = "_presence._tcp";


    public static final int MESSAGE_READ = 0x400 + 1;
    public static final int MY_HANDLE = 0x400 + 2;
    public static final int SERVER_PORT = 4545;

    private boolean modoServer;

    private Connection mConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);//evitamos el sleep durante la ejecucion de esta aplicacion

        //new WiFiP2pManager
        mManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        //init
        mChannel = mManager.initialize(this, getMainLooper(), null);

        //new WiFiBroadcastReceiver
        mReceiver = new WiFiDirectBroadcastReceiver(mManager, mChannel, this, null, this);

        //filters action interested in
        mIntentFilter = mReceiver.intentFilter();

        //START DISCOVERING PEERS IN RANGE
        /*mManager.discoverPeers(mChannel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                // discovered ok , but does not provide any information about the actual peers
                // that it discovered the system broadcasts => WIFI_P2P_PEERS_CHANGED_ACTION
                Log.i("Wifi-P2P", "Discover Peer success! ");
                Toast.makeText(MainActivity.this, "Discover Peer success! ", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int reasonCode) {
                //connection error
                Log.i("Wifi-P2P", "Discover Peer failure! :" + String.valueOf(reasonCode));
                Toast.makeText(MainActivity.this, "Discover Peer failure! :" + String.valueOf(reasonCode), Toast.LENGTH_SHORT).show();
            }
        });*/


        modoServer = (1 == 0);
        //modoServer = false;

    }


    /* register the broadcast receiver with the intent values to be matched */
    @Override
    protected void onResume() {
        registerReceiver(mReceiver, mIntentFilter);
        super.onResume();
    }

    /* unregister the broadcast receiver */
    @Override
    protected void onPause() {
        unregisterReceiver(mReceiver);
        super.onPause();
    }

    @Override
    protected void onStop() {

        //stopping connection
        if (this.mConnection != null) {
            this.mConnection.closeConnection();
            this.mConnection = null;
        }

        //stop server service
        if (mServerServiceHandler != null) {
            mServerServiceHandler.stopServer();
            mServerServiceHandler = null;
        }

        //stop client service discovery
        if (mClientServiceHandler != null) {
            mClientServiceHandler.stopDiscovery();
            mClientServiceHandler = null;
        }

        super.onStop();
    }


    @Override
    protected void onDestroy() {
        onStop();

        super.onDestroy();
    }


    /*private ChatManager mChatManager;

    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case MESSAGE_READ:
                byte[] readBuf = (byte[]) msg.obj;
                // construct a string from the valid bytes in the buffer
                String readMessage = new String(readBuf, 0, msg.arg1);
                Log.d(TAG, readMessage);
                appendStatus("Buddy: " + readMessage);
                if(modoServer)
                    mChatManager.write("RE: " + readMessage);
                break;

            case MY_HANDLE:
                Object obj = msg.obj;
                //(chatFragment).setChatManager((ChatManager) obj);
                mChatManager = (ChatManager) obj;
                ((ChatManager) obj).write("HOLA");
        }
        return true;
    }*/

    private ServerServiceHandler mServerServiceHandler;
    private ClientServiceHandler mClientServiceHandler;

    private WifiP2pDevice mThisDevice;

    @Override
    public void onWiFiAvailable(WifiP2pDevice thisDevice) {
        mThisDevice = thisDevice;

        if (modoServer) {
            //Start Service
            mServerServiceHandler = new ServerServiceHandler(this, mManager, mChannel);
            mServerServiceHandler.startServer();
        } else {

            //iniciamos el modo publicidad
            modoPublicidad(true);

            //Discover Service
            mClientServiceHandler = new ClientServiceHandler(this, mManager, mChannel);
            mClientServiceHandler.discoverService();
        }
    }

    /**
     * ESTE EVENTO PROVIENE DE UNA CONEXION ESTABLECIDA
     *
     * @param wifiP2pInfo
     */
    @Override
    public void onConnectionInfoAvailable(WifiP2pInfo wifiP2pInfo) {
        // InetAddress from WifiP2pInfo struct.
        InetAddress groupOwnerAddress = wifiP2pInfo.groupOwnerAddress;

        //Thread handler = null;

        // After the group negotiation, we can determine the group owner.
        if (wifiP2pInfo.groupFormed && wifiP2pInfo.isGroupOwner) {
            // Do whatever tasks are specific to the group owner.
            // One common case is creating a server thread and accepting
            // incoming connections.
            appendStatus("Connected as group owner");
            try {
                mConnection = new Connection(SERVER_PORT, this, mServerHandler);
                mConnection.createServer();
                //handler = new GroupOwnerSocketHandler(((ChatManager.MessageTarget) this).getHandler());
                //handler.start();
            } catch (Exception e) {
                appendStatus("Failed to create a server thread - " + e.getMessage());
                return;
            }
        } else if (wifiP2pInfo.groupFormed) {
            // The other device acts as the client. In this case,
            // you'll want to create a client thread that connects to the group
            // owner.
            appendStatus("Connected as peer");

            //handler = new ClientSocketHandler(((ChatManager.MessageTarget) this).getHandler(), groupOwnerAddress);
            //handler.start();
            mConnection = new Connection(SERVER_PORT, this, mClientHandler);
            mConnection.connectToServer(groupOwnerAddress, SERVER_PORT);

        }
    }


    public void appendStatus(String status) {
        Log.d(TAG, status);
        Toast.makeText(this, status, Toast.LENGTH_SHORT).show();
    }


    public void modoPublicidad(boolean activar) {
        if (activar) {
            findViewById(R.id.layout_general).setVisibility(View.VISIBLE);
            findViewById(R.id.layout_input).setVisibility(View.GONE);
            ((TextView) findViewById(R.id.txt)).setText("< PUBLICIDAD >");
            ((TextView) findViewById(R.id.txt_display)).setText("");
        } else
            ((TextView) findViewById(R.id.txt)).setText("");
    }

    public void solicitarInput(String label, String hint) {
        findViewById(R.id.layout_general).setVisibility(View.GONE);
        findViewById(R.id.layout_input).setVisibility(View.VISIBLE);
        ((TextView) findViewById(R.id.txtInputLabel)).setText(label);
        ((EditText) findViewById(R.id.txtInput)).setHint(hint);
        ((Button) findViewById(R.id.btnInputAceptar)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String input = ((EditText) findViewById(R.id.txtInput)).getText().toString();

                /*getConnectionWrapper().send(
                        new HashMap<String, String>() {{
                            put(Communication.MESSAGE_TYPE, Communication.InputResponse.TYPE);
                            put(Communication.InputResponse.VALUE, input);
                        }}
                );*/

                modoPublicidad(true);
            }
        });
    }

    /*public void startServer() {

        WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        int intaddr = wifi.getConnectionInfo().getIpAddress();

        if (wifi.getWifiState() == WifiManager.WIFI_STATE_DISABLED || intaddr == 0) {
            startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
        } else {
            getConnectionWrapper().stopNetworkDiscovery();
            getConnectionWrapper().startServer();
            getConnectionWrapper().setHandler(mServerHandler);
        }

    }*/

    private Handler mServerHandler = new MessageHandler(_MainActivity.this) {
        @Override
        public void onMessage(String type, Message message) {
            try {
                if (type.equals(Message.MessageType.CONNECT_TYPE)) {//COMMUNICATION REQUEST

                    Message.ConnectionRequest deviceFrom = message.getConnectionRequestContent();

                    Toast.makeText(getApplicationContext(), "Device: " + deviceFrom.device, Toast.LENGTH_SHORT).show();

                    Message msg = new Message();
                    msg.type = Message.MessageType.SHOW_SPLASH_TYPE;
                    msg.content = message.new ShowSplashRequest(String.valueOf(Math.min((int) (Math.random() * 1000), 20))).toString();

                    mConnection.sendMessage(msg.toString());

                } else if (type.equals(Message.MessageType.SHOW_SPLASH_TYPE)) {
                    //Toast.makeText(getApplicationContext(), "Type: " + type + " - Message: " + message.toString(), Toast.LENGTH_LONG).show();
                    try {

                        Thread.sleep(1000);

                        Message.ShowSplashRequest content = message.getShowTextRequestContent();

                        Message msg = new Message();

                        if (Integer.parseInt(content.text) == 0) {
                            msg.type = Message.MessageType.SHOW_SCREEN_TYPE;
                            Message.ShowScreenRequest request = message.new ShowScreenRequest();

                            request.screen = "TICKET";
                            request.data = new ArrayList<Message.ShowScreenViewContent>();

                            Message.ShowScreenViewContent item = msg.new ShowScreenViewContent();
                            item.idView = "txt_total_monto";
                            item.classTypeView = "android.widget.TextView";
                            item.viewContentType = "java.lang.CharSequence";
                            item.viewContent = "999";

                            request.data.add(item);

                            item = msg.new ShowScreenViewContent();
                            item.idView = "txt_total_cant";
                            item.classTypeView = "android.widget.TextView";
                            item.viewContentType = "java.lang.CharSequence";
                            item.viewContent = "7";

                            request.data.add(item);

                            //agregamos una lista, para ello, primero creamos la lista de ITEMS
                            List<Message.ShowScreenListContent> viewContentList = new ArrayList<Message.ShowScreenListContent>();

                            //cada item, es una lista de views
                            Message.ShowScreenListContent listItem = msg.new ShowScreenListContent();

                            listItem.item = new ArrayList<Message.ShowScreenViewContent>();

                            item = msg.new ShowScreenViewContent();
                            item.idView = "item_txt_producto";
                            item.classTypeView = "android.widget.TextView";
                            item.viewContentType = "java.lang.CharSequence";
                            item.viewContent = "AAAA";

                            listItem.item.add(item);

                            item = msg.new ShowScreenViewContent();
                            item.idView = "item_txt_cantidad";
                            item.classTypeView = "android.widget.TextView";
                            item.viewContentType = "java.lang.CharSequence";
                            item.viewContent = "2";

                            listItem.item.add(item);

                            item = msg.new ShowScreenViewContent();
                            item.idView = "item_txt_precio";
                            item.classTypeView = "android.widget.TextView";
                            item.viewContentType = "java.lang.CharSequence";
                            item.viewContent = "50";

                            listItem.item.add(item);

                            item = msg.new ShowScreenViewContent();
                            item.idView = "item_txt_total";
                            item.classTypeView = "android.widget.TextView";
                            item.viewContentType = "java.lang.CharSequence";
                            item.viewContent = "100";

                            listItem.item.add(item);

                            viewContentList.add(listItem);
                            //fin item 1


                            //cada item, es una lista de views
                            listItem = msg.new ShowScreenListContent();

                            listItem.item = new ArrayList<Message.ShowScreenViewContent>();

                            item = msg.new ShowScreenViewContent();
                            item.idView = "item_txt_producto";
                            item.classTypeView = "android.widget.TextView";
                            item.viewContentType = "java.lang.CharSequence";
                            item.viewContent = "BBBB";

                            listItem.item.add(item);

                            item = msg.new ShowScreenViewContent();
                            item.idView = "item_txt_cantidad";
                            item.classTypeView = "android.widget.TextView";
                            item.viewContentType = "java.lang.CharSequence";
                            item.viewContent = "1";

                            listItem.item.add(item);

                            item = msg.new ShowScreenViewContent();
                            item.idView = "item_txt_precio";
                            item.classTypeView = "android.widget.TextView";
                            item.viewContentType = "java.lang.CharSequence";
                            item.viewContent = "25";

                            listItem.item.add(item);

                            item = msg.new ShowScreenViewContent();
                            item.idView = "item_txt_total";
                            item.classTypeView = "android.widget.TextView";
                            item.viewContentType = "java.lang.CharSequence";
                            item.viewContent = "25";

                            listItem.item.add(item);

                            viewContentList.add(listItem);
                            //fin item 2

                            item = msg.new ShowScreenViewContent();
                            item.idView = "lst_pedido";
                            item.classTypeView = "android.widget.ListView";
                            item.viewContentType = "pedido_item";
                            item.setListContent(viewContentList);

                            request.data.add(item);
                            //


                            msg.content = request.toString();

                        } else {
                            msg.type = Message.MessageType.SHOW_SPLASH_TYPE;
                            msg.content = message.new ShowSplashRequest(String.valueOf(Integer.parseInt(content.text) - 1)).toString();
                        }

                        mConnection.sendMessage(msg.toString());

                    } catch (Exception e) {
                        Log.e(DEBUG_TAG, e.getMessage());
                    }
                }
            } catch (Exception e) {
                Log.d(DEBUG_TAG, "JSON parsing exception: " + e);
            }
        }
    };

    private Handler mClientHandler = new MessageHandler(_MainActivity.this) {
        @Override
        public void onMessage(String type, Message message) {
            try {
                if (type.equals(Message.MessageType.CONNECT_TYPE)) {
                    Toast.makeText(getApplicationContext(), "Connection succesfully performed!", Toast.LENGTH_SHORT).show();
                } else if (type.equals(Message.MessageType.SHOW_SPLASH_TYPE)) {
                    //Toast.makeText(getApplicationContext(), "Type: " + type + " - Message: " + message.toString(), Toast.LENGTH_LONG).show();
                    try {

                        Message.ShowSplashRequest content = message.getShowTextRequestContent();

                        modoPublicidad(false);

                        notifyUser(content.text);
                        ((TextView) findViewById(R.id.txt_display)).setText(content.text);

                        if (!content.text.equalsIgnoreCase("FIN")) {
                            Message msg = new Message();
                            msg.type = Message.MessageType.SHOW_SPLASH_TYPE;
                            msg.content = message.new ShowSplashRequest(String.valueOf(Integer.parseInt(content.text))).toString();

                            mConnection.sendMessage(msg.toString());
                        }

                    } catch (Exception e) {
                        Log.e(DEBUG_TAG, e.getMessage());
                    }
                } else if (type.equals(Message.MessageType.SHOW_SCREEN_TYPE)) {
                    try{
                        Message.ShowScreenRequest request = message.getShowScreenRequestContent();

                        if(request.screen.equalsIgnoreCase("Ticket"))
                        {
                            Intent i = new Intent(_MainActivity.this, TicketActivity.class);

                            i.putExtra("timeout", message.timeout);
                            i.putExtra("data", request.dataToString());

                            startActivity(i);
                        }
                    }
                    catch (Exception e){
                        Log.e(DEBUG_TAG, e.toString());
                    }
                } else if (type.equals(Communication.InputRequest.TYPE)) {
                    //String label = message.getString(Communication.InputRequest.LABEL);
                    //String hint = message.getString(Communication.InputRequest.HINT);
                    //solicitarInput(label, hint);
                }
            } catch (Exception e) {
                Log.d(DEBUG_TAG, "JSON parsing exception: " + e);
            }
        }
    };

    @Override
    public void onConnection() {
        if (!modoServer) {
            Message message = new Message();
            message.type = Message.MessageType.CONNECT_TYPE;
            message.content = message.new ConnectionRequest(mThisDevice.deviceName).toString();

            mConnection.sendMessage(message.toString());
        }
    }

    /*private void connect() {
        getConnectionWrapper().findServers(new NetworkDiscovery.OnFoundListener() {
            @Override
            public void onFound(javax.jmdns.ServiceInfo info) {
                Log.d(DEBUG_TAG, "Server Found");
                if (info != null && info.getInet4Addresses().length > 0) {
                    getConnectionWrapper().stopNetworkDiscovery();
                    getConnectionWrapper().connectToServer(
                            info.getInet4Addresses()[0],
                            info.getPort(),
                            mConnectionListener
                    );
                    getConnectionWrapper().setHandler(mClientHandler);
                }
            }
        });
    }

    private Connection.ConnectionListener mConnectionListener = new Connection.ConnectionListener() {
        @Override
        public void onConnection() {
            getConnectionWrapper().send(
                    new HashMap<String, String>() {{
                        put(Communication.MESSAGE_TYPE, Communication.Connect.TYPE);
                        put(Communication.Connect.DEVICE, Build.MODEL);
                    }}
            );
        }
    };*/






    /*
    private ConnectionWrapper getConnectionWrapper() {
        return ((SampleApplication) getApplication()).getConnectionWrapper();
    }*/

    /*
    @Override
    protected void onDestroy() {

        super.onDestroy();
        if (jmdns != null) {

            jmdns.unregisterAllServices();
            try {
                jmdns.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                Log.e("SERVER", "Error: " + e.getMessage());
                e.printStackTrace();
            }
            jmdns = null;
        }

    }


    public void registerService(int port) {
        //NsdServiceInfo serviceInfo  = new NsdServiceInfo();
        //serviceInfo.setPort(port);
        //serviceInfo.setServiceName(mServiceName);
        //serviceInfo.setServiceType(SERVICE_TYPE);

        //mNsdManager.registerService(serviceInfo, NsdManager.PROTOCOL_DNS_SD, mRegistrationListener);
        //serviceInfo = ServiceInfo.create("_test._tcp.local.", "AndroidTest", 0, "test from android");
    }*/

    private void notifyUser(final String msg) {
        TextView t = (TextView) findViewById(R.id.txt);
        t.setText(msg + "\n=== " + t.getText());
    }







    /*
    private class DataTask extends AsyncTask<Void, String, String> {
        private Context _context;

        public DataTask(Context _context) {
            this._context = _context;
        }

        // onPreExecute used to setup the AsyncTask.
        @Override
        protected void onPreExecute() {
            //Toast.makeText(_context, "Consultando nuevas versiones..", Toast.LENGTH_SHORT).show();
            //notifyUser("Iniciando servicio...");
            ((TextView)findViewById(R.id.txt)).append("Iniciando servicio..." +"\n=== " );
        }

        @Override
        protected String doInBackground(Void... params) {

            // params comes from the execute() call: params[0] is the message.
            try {

                wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
                lock = wifiManager.createMulticastLock("mylockthereturn");
                lock.setReferenceCounted(true);
                lock.acquire();
                try {
                    jmdns = JmDNS.create();
                    jmdns.addServiceListener(SERVICE_TYPE, listener = new ServiceListener() {

                        @Override
                        public void serviceResolved(ServiceEvent ev) {
                            Log.d("SERVER", "Service resolved: " + ev.getInfo().getQualifiedName() + " port:" + ev.getInfo().getPort());
                            //notifyUser("Service resolved: " + ev.getInfo().getQualifiedName() + " port:" + ev.getInfo().getPort());
                            ((TextView)findViewById(R.id.txt)).append("Service resolved: " + ev.getInfo().getQualifiedName() + " port:" + ev.getInfo().getPort() +"\n=== " );
                            //publishProgress("Service resolved: " + ev.getInfo().getQualifiedName() + " port:" + ev.getInfo().getPort());
                        }

                        @Override
                        public void serviceRemoved(ServiceEvent ev) {
                            Log.d("SERVER", "Service removed: " + ev.getName());
                            //notifyUser("Service removed: " + ev.getName());
                            ((TextView)findViewById(R.id.txt)).append("Service removed: " + ev.getName() +"\n=== " );
                            //publishProgress("Service removed: " + ev.getName());
                        }

                        @Override
                        public void serviceAdded(ServiceEvent event) {
                            // Required to force serviceResolved to be called again (after the first search)
                            Log.d("SERVER", "Service added: type " +event.getType() + " name " + event.getName());
                            ((TextView)findViewById(R.id.txt)).append("Service added: type " +event.getType() + " name " + event.getName() +"\n=== " );
                            jmdns.requestServiceInfo(event.getType(), event.getName(), 1);
                        }
                    });
                    serviceInfo = ServiceInfo.create(SERVICE_TYPE, mServiceName, 6999, "plain test service from android");
                    jmdns.registerService(serviceInfo);

                    Log.d("SERVER", "Service started Successful ");
                    //publishProgress("Service started Successful " );
                    ((TextView)findViewById(R.id.txt)).append("Service started Successful " +"\n=== " );

                } catch (IOException e) {
                    Log.e("SERVER", "Error: " + e.getMessage());
                    e.printStackTrace();
                    return e.getMessage();
                } catch (Exception e) {
                    Log.e("SERVER", "Error: " + e.getMessage());
                    e.printStackTrace();
                    return e.getMessage();
                }

                return "OK";
            }
            catch (Exception e) {
                Log.e("SERVER", "Error: " + e.getMessage());
                return "Error: " + e.getMessage();
            }
        }

        @Override
        protected void onProgressUpdate(String... p) {
            //Toast.makeText(_context, p[0], Toast.LENGTH_LONG).show();
            notifyUser( p[0]);
        }


        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            Toast.makeText(_context, result, Toast.LENGTH_SHORT).show();
            //notifyUser( result);
            ((TextView)findViewById(R.id.txt)).append(result +"\n=== " );
        }

        @Override
        protected void onCancelled(String s) {
            Toast.makeText(_context, s, Toast.LENGTH_LONG).show();
        }
    }

    */
}
