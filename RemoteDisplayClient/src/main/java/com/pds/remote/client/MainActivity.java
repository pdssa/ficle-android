package com.pds.remote.client;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceRequest;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.remote.common.Connection;
import com.pds.remote.common.Message;
import com.pds.remote.common.MessageHandler;
import com.pds.remote.common.WiFiDirectBroadcastReceiver;
import com.pds.remote.old.Communication;
import com.pds.remote.sserver.ServerServiceHandler;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //2.START SERVICE
        StartDisplayService(this);

        finish();
    }

    private void StartDisplayService(Context context) {
        try {
            Intent i = new Intent(context, ClientDisplayService.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startService(i);
        }catch (Exception ex) {
            Log.e("Display",ex.toString());
        }
    }

}
