package com.pds.remote.client;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.remote.common.CloseScreenReceiver;
import com.pds.remote.common.FinishActivityTimerTask;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Timer;


public class TicketActivity extends Activity {

    private ListView lstPedido;
    private TextView lblNuevoPedido;
    private TextView txtNroPedido;
    private TextView lblPedidoRegistrado;
    private TextView txtCantTotal;
    private TextView txtMontoTotal;
    private TextView lblImpuestos;
    private TextView txtTotalImpuestos;
    private TextView txtSubtotalImpuestos;

    private long timeout = 0;//time to finish the activity

    private CloseScreenReceiver mCloseReceiver;

    public void updateContent(Intent intent){
        //leer el json del intent
        String input = "[\n" +
                "\t\t{\"idView\":\"txt_total_monto\",\"classTypeView\":\"android.widget.TextView\",\"viewContentType\": \"java.lang.CharSequence\",\"viewContent\": \"200\"},\n" +
                "\t\t{\"idView\":\"txt_total_cant\",\"classTypeView\":\"android.widget.TextView\",\"viewContentType\": \"java.lang.CharSequence\",\"viewContent\": \"1\"},\n" +
                "\t\t{\"idView\":\"lst_pedido\",\"classTypeView\":\"android.widget.ListView\",\"viewContentType\": \"pedido_item\",\"viewContent\": \n" +
                "\t\t\t[\n" +
                "\t\t\t\t[{\"idView\":\"item_txt_producto\",\"classTypeView\":\"android.widget.TextView\",\"viewContent\":\"GENERICO\",\"viewContentType\":\"java.lang.CharSequence\"},\n" +
                "\t\t\t\t{\"idView\":\"item_txt_cantidad\",\"classTypeView\":\"android.widget.TextView\",\"viewContent\":\"1\",\"viewContentType\":\"java.lang.CharSequence\"},\n" +
                "\t\t\t\t{\"idView\":\"item_txt_precio\",\"classTypeView\":\"android.widget.TextView\",\"viewContent\":\"200\",\"viewContentType\":\"java.lang.CharSequence\"},\n" +
                "\t\t\t\t{\"idView\":\"item_txt_total\",\"classTypeView\":\"android.widget.TextView\",\"viewContent\":\"200\",\"viewContentType\":\"java.lang.CharSequence\"}\n" +
                "\t\t\t\t],\n" +
                "\t\t\t\t[{\"idView\":\"item_txt_producto\",\"classTypeView\":\"android.widget.TextView\",\"viewContent\":\"desodorante\",\"viewContentType\":\"java.lang.CharSequence\"},\n" +
                "\t\t\t\t{\"idView\":\"item_txt_cantidad\",\"classTypeView\":\"android.widget.TextView\",\"viewContent\":\"5\",\"viewContentType\":\"java.lang.CharSequence\"},\n" +
                "\t\t\t\t{\"idView\":\"item_txt_precio\",\"classTypeView\":\"android.widget.TextView\",\"viewContent\":\"28\",\"viewContentType\":\"java.lang.CharSequence\"},\n" +
                "\t\t\t\t{\"idView\":\"item_txt_total\",\"classTypeView\":\"android.widget.TextView\",\"viewContent\":\"140\",\"viewContentType\":\"java.lang.CharSequence\"}\n" +
                "\t\t\t\t]\n" +
                "\t\t\t]\n" +
                "\t\t}\n" +
                "\t\t\n" +
                "\t]";

        if (intent.getExtras().containsKey("data")) {
            input = getIntent().getExtras().getString("data");
        }

        try {
            LoadContent(parseJson(input));

            Set_Timer();
        } catch (Exception ex) {
            Toast.makeText(TicketActivity.this, ex.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    /*@Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        updateContent(intent);
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);//evitamos el sleep durante la ejecucion de esta aplicacion

        mCloseReceiver = new CloseScreenReceiver(this);

        //registramos el receiver para escuchar por si es necesario el cierre
        registerReceiver(mCloseReceiver, CloseScreenReceiver.intentFilter);

        Init_AsignarViews();

        timeout = 1 * 30 * 1000; //medio minuto

        updateContent(getIntent());

    }

    private void Init_AsignarViews() {

        //controles del pedido
        this.lstPedido = (ListView) findViewById(R.id.lst_pedido);
        this.lblNuevoPedido = (TextView) findViewById(R.id.lbl_nuevo_pedido);
        this.txtNroPedido = (TextView) findViewById(R.id.txt_pedido);
        this.lblPedidoRegistrado = (TextView) findViewById(R.id.lbl_pedido_registrado);

        this.txtCantTotal = (TextView) findViewById(R.id.txt_total_cant);
        this.txtMontoTotal = (TextView) findViewById(R.id.txt_total_monto);

        this.lblImpuestos = (TextView) findViewById(R.id.lbl_impuesto);
        this.txtSubtotalImpuestos = (TextView) findViewById(R.id.txt_subtotal);
        this.txtTotalImpuestos = (TextView) findViewById(R.id.txt_impuesto);

    }

    private JSONArray parseJson(String input) throws JSONException {
        return new JSONArray(input);
    }

    private void LoadContent(JSONArray content) {
        try {
            //cargamos el contenido
            new ViewLoader(this).LoadContent(content);

            //reglas de visibilidad
            if (this.lstPedido.getVisibility() == View.VISIBLE) {
                this.lblNuevoPedido.setVisibility(View.GONE);
            }

        } catch (Exception ex) {
            Toast.makeText(TicketActivity.this, ex.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    private Timer timer;//timer para control de la tarea de confirmacion automatica
    private FinishActivityTimerTask finishTask;
    private void Set_Timer() {
        //si corresponde seteamos el timer
        if (timeout > 0) {
            if (timer != null) {
                timer.cancel();
            }

            timer = new Timer();
            finishTask = new FinishActivityTimerTask(this);
            timer.schedule(finishTask, timeout);
        }
    }

    @Override
    protected void onDestroy() {

        if (timer != null) {
            timer.cancel();
        }

        if(mCloseReceiver != null){
            unregisterReceiver(mCloseReceiver);
        }

        super.onDestroy();
    }
}
