package com.pds.remote.client;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceRequest;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.remote.common.CloseScreenReceiver;
import com.pds.remote.common.Connection;
import com.pds.remote.common.Message;
import com.pds.remote.common.MessageHandler;
import com.pds.remote.common.WiFiDirectBroadcastReceiver;
import com.pds.remote.common.WiFiP2pService;
import com.pds.remote.old.Communication;
import com.pds.remote.sserver.ServerServiceHandler;

import java.lang.reflect.Method;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

public class ClientDisplayService extends Service
        implements WifiP2pManager.ConnectionInfoListener, Connection.ConnectionListener, WiFiDirectBroadcastReceiver.WifiDirectListener {

    private static final String DEBUG_TAG = ClientDisplayService.class.getName();
    private static final String TAG = "Wifi-P2P";


    private WifiP2pManager mManager;
    private WifiP2pManager.Channel mChannel;
    private WiFiDirectBroadcastReceiver mReceiver;
    private IntentFilter mIntentFilter;
    private WifiP2pDnsSdServiceRequest mServiceRequest;
    private List<WifiP2pDevice> mPeersList = new ArrayList<WifiP2pDevice>();
    private Connection mConnection;

    private WifiP2pDevice mServerPeer;

    public static final int SERVER_PORT = 4545;


    public ClientDisplayService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appendStatus("starting service...");

        //iniciamos el modo publicidad
        modoPublicidad(true);

        //new WiFiP2pManager
        mManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        //init
        mChannel = mManager.initialize(this, getMainLooper(), null);

        //new WiFiBroadcastReceiver
        mReceiver = new WiFiDirectBroadcastReceiver(mManager, mChannel, this, null, this);

        //filters action interested in
        mIntentFilter = mReceiver.intentFilter();

        /* register the broadcast receiver with the intent values to be matched */
        registerReceiver(mReceiver, mIntentFilter);

        appendStatus("Service Created");
    }

    /* unregister the broadcast receiver and close connection */
    @Override
    public void onDestroy() {

        unregisterReceiver(mReceiver);

        //stopping connection
        if (this.mConnection != null) {
            this.mConnection.closeConnection();
            this.mConnection = null;
        }

        //stop client service discovery
        if (mClientServiceHandler != null) {
            mClientServiceHandler.stopDiscovery();
            mClientServiceHandler = null;
        }

        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(DEBUG_TAG, "onStartCommand");
        return android.app.Service.START_STICKY;
    }



    private ClientServiceHandler mClientServiceHandler;

    private WifiP2pDevice mThisDevice;

    @Override
    public void onWiFiAvailable(WifiP2pDevice thisDevice) {
        mThisDevice = thisDevice;
        //Discover Service
        mClientServiceHandler = new ClientServiceHandler(this, mManager, mChannel);
        mClientServiceHandler.discoverService();
    }


    /**
     * ESTE EVENTO PROVIENE DE UNA CONEXION ESTABLECIDA
     *
     * @param wifiP2pInfo
     */
    @Override
    public void onConnectionInfoAvailable(WifiP2pInfo wifiP2pInfo) {
        // InetAddress from WifiP2pInfo struct.
        InetAddress groupOwnerAddress = wifiP2pInfo.groupOwnerAddress;

        //Thread handler = null;

        // After the group negotiation, we can determine the group owner.
        if (wifiP2pInfo.groupFormed && wifiP2pInfo.isGroupOwner) {
            // Do whatever tasks are specific to the group owner.
            // One common case is creating a server thread and accepting
            // incoming connections.
            appendStatus("Connected as group owner");
            /*try {
                mConnection = new Connection(SERVER_PORT, this, mServerHandler);
                mConnection.createServer();
                //handler = new GroupOwnerSocketHandler(((ChatManager.MessageTarget) this).getHandler());
                //handler.start();
            } catch (Exception e) {
                appendStatus("Failed to create a server thread - " + e.getMessage());
                return;
            }*/
        } else if (wifiP2pInfo.groupFormed) {
            // The other device acts as the client. In this case,
            // you'll want to create a client thread that connects to the group
            // owner.
            appendStatus("Connected as peer");

            //handler = new ClientSocketHandler(((ChatManager.MessageTarget) this).getHandler(), groupOwnerAddress);
            //handler.start();
            mConnection = new Connection(SERVER_PORT, this, mClientHandler);
            mConnection.connectToServer(groupOwnerAddress, SERVER_PORT);

        }
    }


    public void appendStatus(String status) {
        Log.d(TAG, status);
        Toast.makeText(this, status, Toast.LENGTH_SHORT).show();
    }


    public void modoPublicidad(boolean activar) {
        if (activar) {
            StartSureVideo(this);
        } else {
            //((TextView) findViewById(R.id.txt)).setText("");
        }
    }

    private void StartSureVideo(Context context) {
        try {
            Intent i = new Intent();
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.setAction(Intent.ACTION_MAIN);
            i.setPackage("com.gears42.surevideo");
            //i.setComponent(new ComponentName("com.gears42.surevideo", ".SureVideoScreen"));

            context.startActivity(i);
        } catch (Exception ex) {
            Toast.makeText(context, "Error al iniciar SureVideo: " + ex.toString(), Toast.LENGTH_LONG).show();
        }
    }


    private Handler mClientHandler = new MessageHandler(this) {
        @Override
        public void onMessage(String type, Message message) {
            try {
                if (type.equals(Message.MessageType.CONNECT_TYPE)) {
                    Toast.makeText(ClientDisplayService.this, "Connection succesfully performed!", Toast.LENGTH_SHORT).show();
                } else if (type.equals(Message.MessageType.CLOSE_SCREEN)) {
                    stopScreen();
                } else if (type.equals(Message.MessageType.SHOW_SPLASH_TYPE)) {
                    //Toast.makeText(getApplicationContext(), "Type: " + type + " - Message: " + message.toString(), Toast.LENGTH_LONG).show();
                    try {

                        Message.ShowSplashRequest content = message.getShowTextRequestContent();

                        modoPublicidad(false);

                        notifyUser(content.text);
                        //((TextView) findViewById(R.id.txt_display)).setText(content.text);

                        /*if (!content.text.equalsIgnoreCase("FIN")) {
                            Message msg = new Message();
                            msg.type = Message.MessageType.SHOW_SPLASH_TYPE;
                            msg.content = message.new ShowSplashRequest(String.valueOf(Integer.parseInt(content.text))).toString();

                            mConnection.sendMessage(msg.toString());
                        }*/

                    } catch (Exception e) {
                        Log.e(DEBUG_TAG, e.getMessage());
                    }
                } else if (type.equals(Message.MessageType.SHOW_SCREEN_TYPE)) {
                    try {
                        Message.ShowScreenRequest request = message.getShowScreenRequestContent();

                        startScreen(request.screen, request.dataToString());

                    } catch (Exception e) {
                        Log.e(DEBUG_TAG, e.toString());
                    }
                } else if (type.equals(Communication.InputRequest.TYPE)) {
                    //String label = message.getString(Communication.InputRequest.LABEL);
                    //String hint = message.getString(Communication.InputRequest.HINT);
                    //solicitarInput(label, hint);
                }
            } catch (Exception ex) {
                Log.d(DEBUG_TAG, "JSON parsing exception: " + ex);
            }
        }
    };

    private void startScreen(String screenId, String data){
        //primero mandamos a cerrar las pantallas abiertas
        //sendBroadcast(new Intent(CloseScreenReceiver.intentCloseAction));

        /*try {
            Thread.sleep(1000); //aguardamos un segundo
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        //llamamos a la pantalla
        if (screenId.equalsIgnoreCase("Ticket")) {
            Intent i = new Intent(ClientDisplayService.this, TicketActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            i.putExtra("data", data);

            startActivity(i);
            /*Intent i = new Intent(CloseScreenReceiver.intentTicketAction);
            i.putExtra("data", data);

            sendBroadcast(i);*/
        }
    }

    private void stopScreen(){
        //mandamos a cerrar las pantallas abiertas
        sendBroadcast(new Intent(CloseScreenReceiver.intentCloseAction));
    }

    @Override
    public void onConnection() {
        Message message = new Message();
        message.type = Message.MessageType.CONNECT_TYPE;
        message.content = message.new ConnectionRequest(getHostName(mThisDevice)).toString();

        mConnection.sendMessage(message.toString());
    }

    public static String getHostName(WifiP2pDevice device) {
        try {
            if(device != null)
                return device.deviceName;
            else {

                Method getString = Build.class.getDeclaredMethod("getString", String.class);
                getString.setAccessible(true);
                return getString.invoke(null, "net.hostname").toString();
            }
        } catch (Exception ex) {
            return "12345678";
        }
    }

    private void notifyUser(final String msg) {
        Toast.makeText(ClientDisplayService.this, msg, Toast.LENGTH_SHORT).show();
    }


    @Override
    public IBinder onBind(Intent intent) {
        // We don't provide binding, so return null
        return null;
    }
}
