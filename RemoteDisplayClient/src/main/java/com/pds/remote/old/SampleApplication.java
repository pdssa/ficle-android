package com.pds.remote.old;

import android.app.Application;
import com.pds.remote.old.ConnectionWrapper;

/**
 * Created by Hernan on 11/07/2014.
 */
public class SampleApplication extends Application {
    private ConnectionWrapper mConnectionWrapper;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public void createConnectionWrapper(ConnectionWrapper.OnCreatedListener listener) {
        mConnectionWrapper = new ConnectionWrapper(getApplicationContext(), listener);
    }

    public ConnectionWrapper getConnectionWrapper() {
        return mConnectionWrapper;
    }
}