package com.pds.remote.old;

/**
 * Created by Hernan on 11/07/2014.
 */
public class Communication {
    public static final String MESSAGE = "message";
    public static final String MESSAGE_TYPE = "type";

    public static final class Connect {
        public static final String TYPE = Connect.class.getName();
        public static final String DEVICE = "device";
    }

    public static final class ConnectSuccess {
        public static final String TYPE = ConnectSuccess.class.getName();
    }

    public static final class ShowText{
        public static final String TYPE = ShowText.class.getName();
        public static final String TEXT = "text";
    }

    public static final class InputRequest{
        public static final String TYPE = InputRequest.class.getName();
        public static final String LABEL = "label";
        public static final String HINT = "hint";
    }

    public static final class InputResponse{
        public static final String TYPE = InputResponse.class.getName();
        public static final String VALUE = "value";
    }
}
