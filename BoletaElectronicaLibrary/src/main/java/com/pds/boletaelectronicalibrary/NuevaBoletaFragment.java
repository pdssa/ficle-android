package com.pds.boletaelectronicalibrary;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.exavore.PDF417.PDF417;
import com.exavore.PDF417.PDF417_eccLevel;
import com.exavore.PDF417.PDF417_size;
import com.pds.common.Config;
import com.pds.common.Formatos;
//import com.pds.common.R;
import com.pds.common.dao.RangoFolioDao;
import com.pds.common.dao.TipoDocDao;
import com.pds.common.db.RangosFoliosTable;
import com.pds.common.db.TipoDocTable;
import com.pds.common.model.RangoFolio;
import com.pds.common.model.TipoDoc;
import com.pds.common.model.Venta;
import com.pds.common.model.VentaAbstract;
import com.pds.common.model.VentaDetalleAbstract;

import java.util.List;

import android_serialport_api.Printer;
import cl.facturamovil.sdk.bo.RestService;
import cl.facturamovil.sdk.boo.OfflineServices;
import cl.facturamovil.sdk.dto.Company;
import cl.facturamovil.sdk.dto.Detail;
import cl.facturamovil.sdk.dto.Document;
import cl.facturamovil.sdk.dto.DocumentType;
import cl.facturamovil.sdk.dto.Invoice;
import cl.facturamovil.sdk.dto.Payment;
import cl.facturamovil.sdk.dto.Product;
import cl.facturamovil.sdk.dto.Response;
import cl.facturamovil.sdk.dto.Ticket;
import cl.facturamovil.sdk.dto.Unit;
import cl.facturamovil.sdk.dto.User;

/**
 * Created by Hernan on 18/11/2014.
 */
public class NuevaBoletaFragment extends Fragment {

    /*
    private String xmlCaf = "<?xml version=\"1.0\"?>\n" +
            "<AUTORIZACION>\n" +
            "<CAF version=\"1.0\">\n" +
            "<DA>\n" +
            "<RE>79662080-3</RE>\n" +
            "<RS>AGRICOLA DON POLLO LIMITADA</RS>\n" +
            "<TD>33</TD>\n" +
            "<RNG><D>5801</D><H>6800</H></RNG>\n" +
            "<FA>2014-08-26</FA>\n" +
            "<RSAPK><M>pX6wuY3k0D0dpLFL012NnFL/jQQSAbB4VWFACsvuP2LJZ4C240p7TeihEBlcascdsPwYkwV72kwkZYlTEipU8Q==</M><E>Aw==</E></RSAPK>\n" +
            "<IDK>100</IDK>\n" +
            "</DA>\n" +
            "<FRMA algoritmo=\"SHA1withRSA\">CaHiBY802GQ11hFpjNGVq02Jo+Zq/TPGDxcZq2Bp1d11r2jLIbVzJc2BzyjP/+AkgZjxw7/AxL1wsK2ZlyMxPg==</FRMA>\n" +
            "</CAF>\n" +
            "<RSASK>-----BEGIN RSA PRIVATE KEY-----\n" +
            "MIIBOQIBAAJBAKV+sLmN5NA9HaSxS9NdjZxS/40EEgGweFVhQArL7j9iyWeAtuNK\n" +
            "e03ooRAZXGrHHbD8GJMFe9pMJGWJUxIqVPECAQMCQG5UddEJQzV+E8Mg3TeTs72M\n" +
            "ql4CtqvK+uOWKrHdSX+WHitwEp47MdfVesnsT/Qz6G4Mz6efY6yBRFV1ixsaxUMC\n" +
            "IQDYFV9l6RTWN7Yxr+6DCwvsICalVgXl8n04rjVF7yqgrwIhAMQQ+TUM3NpScjcx\n" +
            "SGFxbVTrwjvBkIBlDQU3I7x6V4xfAiEAkA4/mUYN5CUkIR/0V1yynWrEbjlZQ/b+\n" +
            "Jcl42Uocax8CIQCCtft4sz3m4aF6INrroPON8oF9K7Wq7giuJMJ9puUIPwIgSBAc\n" +
            "oqWGlPc4IC+gA31dfzq3uniU5Pm89iDblQLPQ68=\n" +
            "-----END RSA PRIVATE KEY-----\n" +
            "</RSASK>\n" +
            "\n" +
            "<RSAPUBK>-----BEGIN PUBLIC KEY-----\n" +
            "MFowDQYJKoZIhvcNAQEBBQADSQAwRgJBAKV+sLmN5NA9HaSxS9NdjZxS/40EEgGw\n" +
            "eFVhQArL7j9iyWeAtuNKe03ooRAZXGrHHbD8GJMFe9pMJGWJUxIqVPECAQM=\n" +
            "-----END PUBLIC KEY-----\n" +
            "</RSAPUBK>\n" +
            "</AUTORIZACION>\n";
    */
    private static final String ARG_PARAM1 = "venta";
    private static final String ARG_PARAM2 = "tipoDoc";
    private static final String ARG_PARAM3 = "fragment_id";
    private static final String ARG_PARAM4 = "function";
    private static final String ARG_PARAM5 = "auto_print";
    private static final String ARG_PARAM6 = "extended_bitmap";


    private NuevaBoletaListener nuevaBoletaListener;
    private Venta venta;
    private TipoDoc tipoDoc;
    private int fragment_id;
    private FUNCION fn;
    private boolean auto_print;
    private boolean extended_bitmap;

    private Context context;

    public enum FUNCION {
        EMISION,
        IMPRESION
    }

    public void setNuevaBoletaListener(NuevaBoletaListener nuevaBoletaListener) {
        this.nuevaBoletaListener = nuevaBoletaListener;
    }

    public interface NuevaBoletaListener {
        void onBoletaGenerada(long id, String doc_type, String folio, String v, String stamp);

        void onError(String mensaje);

        void onWarning(String mensaje);
    }

    public static NuevaBoletaFragment newInstance(Venta _venta, TipoDoc _tipoDoc, int _fragment_id, FUNCION _fn, boolean _auto_print, String _printerModel) {
        NuevaBoletaFragment fragment = new NuevaBoletaFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, _venta);
        args.putParcelable(ARG_PARAM2, _tipoDoc);
        args.putInt(ARG_PARAM3, _fragment_id);
        args.putInt(ARG_PARAM4, _fn.ordinal());
        args.putBoolean(ARG_PARAM5, _auto_print);
        args.putBoolean(ARG_PARAM6, _printerModel.equals("POWA"));

        fragment.setArguments(args);
        return fragment;
    }

    public NuevaBoletaFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            venta = getArguments().getParcelable(ARG_PARAM1);
            tipoDoc = getArguments().getParcelable(ARG_PARAM2);
            fragment_id = getArguments().getInt(ARG_PARAM3);
            fn = FUNCION.values()[getArguments().getInt(ARG_PARAM4)];
            auto_print = getArguments().getBoolean(ARG_PARAM5);
            extended_bitmap = getArguments().getBoolean(ARG_PARAM6);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_productos, container, false);
    }

    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);

        context = getActivity();

        if (fn == FUNCION.EMISION) {
            GenerateDocument();
        } else if (fn == FUNCION.IMPRESION) {
            PrintDocument();
        }
    }

    private void PrintDocument() {
        try {

            if (tipoDoc == null)
                tipoDoc = new TipoDocDao(context.getContentResolver()).first(TipoDocTable.COLUMN_CODIGO + "='" + venta.getFc_type() +"'", null, null);

            if(venta.getDetalles() == null)
                venta.addDetalles(context.getContentResolver()); //agregamos los items detalles

            ImprimirDocument(venta, tipoDoc, venta.getFc_folio(), GetTypeDocumentCAF(venta.getFc_type(), venta.getFc_folio()).getCaf());

        } catch (Exception ex) {
            SendErrorMessage(ex.toString());
        }
    }

    private void ImprimirDocument(Venta _venta, TipoDoc _tipoDoc, String _folio, String _xmlCaf) throws Exception {
        Config config = new Config(context, false);

        final BoletaTicketBuilder ticketBuilder = new BoletaTicketBuilder(config);
        ticketBuilder.set_Venta(venta);
        ticketBuilder.set_tipoDocumento(_tipoDoc.getNombre());
        ticketBuilder.set_timbre(StampToPDF417(VentaToStamp(_venta, _tipoDoc, _folio, _xmlCaf), extended_bitmap));

        final Printer _printer = Printer.getInstance(config.PRINTER_MODEL, context);
        _printer.delayActivated = true;
        _printer.printerCallback = new Printer.PrinterCallback() {
            @Override
            public void onPrinterReady() {
                ticketBuilder.Print(_printer);
            }
        };

        //iniciamos la printer
        if (!_printer.initialize()) {
            SendErrorMessage("Error al inicializar");

            _printer.end();
        }
    }

    private void GenerateDocument() {
        final ProgressDialog ringProgressDialog = ProgressDialog.show(context, "GENERANDO " + tipoDoc.getNombre() + "...", "Aguarde por favor ...", true);
        ringProgressDialog.setCancelable(false);

        //operacion no bloqueante...
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    //if (venta.getFc_id() == -1)
                    //SincronizarBoleta(venta);
                    //else
                    boolean result = GenerarDocument(tipoDoc, venta);

                    ringProgressDialog.dismiss();

                    if (result)//lo cerramos solo si estuvo OK, dado que sino, entonces se va a cerrar en el WARNING
                        CerrarFragment();

                } catch (Exception e) {

                    ringProgressDialog.dismiss();

                    SendErrorMessage(e.getMessage());

                }
            }
        }).start();
    }


    @Override
    public void onDetach() {
        super.onDetach();
        nuevaBoletaListener = null;
    }

    private boolean GenerarDocument(TipoDoc tipoDoc, Venta _venta) throws Exception {
        return VentaToDocument_OFFLINE(tipoDoc, _venta);
    }

    /*private void SincronizarBoleta(Venta _venta) throws Exception {
        VentaToDocument_ONLINE(TipoDocumento.BOLETA_AFECTA, _venta);
    }*/

    private boolean VentaToDocument_OFFLINE(final TipoDoc _tipoDoc, Venta _venta) throws Exception {
        //try {

        //1-Obtenemos el rango CAF para el tipo de DOC a generar
        RangoFolio caf = GetTypeDocumentCAF(_tipoDoc);

        if (caf != null) {

            final long nextFolio = caf.getProximo();//2 - tomamos el siguiente folio
            final String nextFolioString = String.valueOf(nextFolio);

            //3 - controlamos si el folio a generar es valido para el CAF
            OfflineServices offlineServices = OfflineServices.getInstance();
            if (offlineServices.isValidFolio(caf.getCaf(), nextFolio)) {

                //4 - obtenemos el timbre del documento
                final String stamp = VentaToStamp(_venta, _tipoDoc, nextFolioString, caf.getCaf());
                _venta.setFc_Timbre(stamp);
                _venta.setFc_folio(nextFolioString);

                //5 - actualizamos el ultimo emitido
                caf.setUltimo(nextFolio);
                new RangoFolioDao(context.getContentResolver()).saveOrUpdate(caf);

                //6 - retornamos los datos del documento a la aplicacion solicitante
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (nuevaBoletaListener != null)
                            nuevaBoletaListener.onBoletaGenerada(-1, _tipoDoc.getCodigo(), nextFolioString, null, stamp);
                    }
                });


                if(auto_print) {
                    //7 - emitimos el ticket
                    ImprimirDocument(_venta, _tipoDoc, nextFolioString, caf.getCaf());
                }

                //8 - informamos en caso de ser necesario
                boolean result = true;
                if (caf.estaCompleto()) {
                    result = false;
                    SendWarningMessage(String.format("Ha consumido el último folio para el rango %d - %d", caf.getDesde(), caf.getHasta()));
                } else if (caf.estaPorCompletar()) {
                    result = false;
                    SendWarningMessage(String.format("Está proximo a consumir el último folio para el rango %d - %d.<br/>Cantidad restantes %d", caf.getDesde(), caf.getHasta(), caf.getRestantes()));
                }

                return result;

            } else {
                throw new Exception("Folio fuera de rango");
            }

        } else {
            throw new Exception("No se encontraron rangos de folios disponibles");
        }

        /*} catch (Exception ex) {
            final String message = ex.toString();

            Log.e("NuevaBoleta", message);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (nuevaBoletaListener != null)
                        nuevaBoletaListener.onError(message);
                }
            });
        }*/
    }

    private RangoFolio GetTypeDocumentCAF(TipoDoc tipoDoc) {

        RangoFolioDao dao = new RangoFolioDao(context.getContentResolver());

        //obtenemos los CAF que tengan folios disponibles para el tipo de documento
        return dao.first("tipo_doc='" + tipoDoc.getCodigo() + "' and ultimo < hasta", null, "_id ASC");

    }

    private RangoFolio GetTypeDocumentCAF(String tipoDocCode, String folio) {

        RangoFolioDao dao = new RangoFolioDao(context.getContentResolver());

        //obtenemos el CAF que contenga el folio para el tipo de documento
        return dao.first(String.format("tipo_doc='" + tipoDocCode + "' and %s between desde and hasta", folio), null, null);
    }

    private void SendWarningMessage(final String message) {

        //ringProgressDialog.dismiss();

        final AlertDialog.Builder alert = new AlertDialog.Builder(context)
                .setTitle("ATENCIÓN")
                .setMessage(Html.fromHtml(message))
                .setIcon(android.R.drawable.ic_dialog_info)
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                        if (nuevaBoletaListener != null)
                            nuevaBoletaListener.onWarning(message);

                        CerrarFragment();
                    }
                })
                .setCancelable(false);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                alert.show();
            }
        });
    }

    private void SendErrorMessage(final String message){

        final AlertDialog.Builder alert = new AlertDialog.Builder(context)
                .setTitle("ERROR AL GENERAR " + (tipoDoc!= null ? tipoDoc.getNombre() : ""))
                .setMessage(message)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                        if (nuevaBoletaListener != null)
                            nuevaBoletaListener.onError(message);

                        CerrarFragment();
                    }
                })
                .setCancelable(false);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                alert.show();
            }
        });
    }

    /*private void VentaToDocument_ONLINE(TipoDocumento _tipoDoc, Venta _venta) {
        try {

            boolean syncBoletaOffline = _venta.getFc_id() == -1;

            //si hay que sincronizar una boleta generada offline, usamos el tipo de doc de la venta
            final TipoDocumento tipoDoc = syncBoletaOffline ? GetDocType(_venta.getFc_type()) : _tipoDoc;

            Document document = VentaToDocument(tipoDoc, _venta);

            RestService restService = RestService.getInstance();
            restService.setUseProduction(false);

            User user = null;
            try {
                user = restService.authenticate("demo", "demodemo");
            } catch (Exception ex) {
                throw new Exception("Error al autenticar. " + ex.getMessage());
            }

            Company company = user.getCompanies().get(0);
            document.setCompany(company);

            Response response = null;

            if (tipoDoc == TipoDocumento.BOLETA_AFECTA || tipoDoc == TipoDocumento.BOLETA_EXENTA)
                response = restService.sendTicket(user, company, document);
            else if (tipoDoc == TipoDocumento.FACTURA_AFECTA || tipoDoc == TipoDocumento.FACTURA_EXENTA)
                response = restService.sendTicket(user, company, document);
            else
                throw new Exception("TipoDoc desconocido");

            final Response _response = response;

            if (response.getSuccess()) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (nuevaBoletaListener != null)
                            nuevaBoletaListener.onBoletaGenerada(_response.getId(), GetDocType(tipoDoc), _response.getAssignedFolio(), _response.getValidation(), "");
                    }
                });

            } else {
                throw new Exception("Error al generar documento: " + _response.getDetails());
            }


        } catch (Exception ex) {
            final String message = ex.getMessage();

            Log.e("NuevaBoleta", message);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (nuevaBoletaListener != null)
                        nuevaBoletaListener.onError(message);
                }
            });
        }
    }*/

    /*enum TipoDocumento {
        BOLETA_AFECTA,
        BOLETA_EXENTA,
        FACTURA_AFECTA,
        FACTURA_EXENTA
    }*/

    /*private String VentaToStamp(Venta _venta, Context _context) {

        venta = _venta;
        context = _context;

        return VentaToStamp(venta, xmlCaf);
    }*/

    private String VentaToStamp(Venta _venta, TipoDoc _tipoDoc, String _folio, String xmlCaf) {
        //si la venta ya fue timbrada, entonces retornamos ese timbre, NO lo volvemos a generar
        if (TextUtils.isEmpty(_venta.getFc_Timbre())) {

            Document document = VentaToDocument(_venta, _tipoDoc);

            OfflineServices offlineServices = OfflineServices.getInstance();
            return offlineServices.getStamp(document, xmlCaf, _folio);
        } else {
            return _venta.getFc_Timbre();
        }
    }

    public Bitmap StampToPDF417(String _stamp, boolean extendedBitmap) {
        //TIMBRE ELECTRONICO -> PDF417
        PDF417 barcode = new PDF417(_stamp);
        barcode.setEccLevel(new PDF417_eccLevel(5));
        barcode.setTruncated(false);

        if(extendedBitmap){
            //bitmap: width 566 height 232
            barcode.setAspectRatio(8);
            barcode.setSize(new PDF417_size(25, 0));
        }
        else{
            //bitmap: width 379 height 220
            barcode.setAspectRatio(5);
            barcode.setSize(new PDF417_size(40, 0));
        }


        return barcode.encode();
    }

    /*private TipoDocumento GetDocType(String doc_type) {
        if (doc_type.equals("33"))
            return TipoDocumento.BOLETA_AFECTA;
        else if (doc_type.equals("39"))
            return TipoDocumento.FACTURA_AFECTA;
        else
            return TipoDocumento.BOLETA_AFECTA;
    }*/

    /*private String GetDocType(TipoDocumento doc_type) {
        if (doc_type == TipoDocumento.BOLETA_AFECTA || doc_type == TipoDocumento.BOLETA_EXENTA)
            return "33";
        else
            return "39";
    }*/

    private Document VentaToDocument(VentaAbstract _venta, TipoDoc _tipoDoc) {
        Document document = null;

        try {
        /*Client*/
            /*
            Client client = new Client();
            client.setCode("66666666-6"); //cliente vacío
            */

            Config _config = new Config(context);

            /*Company*/
            Company company = new Company();
            company.setCode(_config.CLAVEFISCAL);
            company.setAddress(_config.DIRECCION);
            company.setName(_config.COMERCIO);

        /*Document type*/
            DocumentType documentType = new DocumentType();
            documentType.setCode(_tipoDoc.getCodigo());

        /*Payment*/
            Payment payment = new Payment();
            payment.setPosition(1);
            payment.setAmount(_venta.getTotal());
            payment.setDate(Formatos.FormateaDate(Formatos.ObtieneDate(_venta.getFecha(), Formatos.DateFormat), Formatos.DbDate_Format));//yyyy-MM-dd
            payment.setDescription(_venta.getMedio_pago());

        /*Documento*/
            if (_tipoDoc.getCodigo().equals("33"))
                document = new Ticket();/*Boleta*/
            else if (_tipoDoc.getCodigo().equals("39"))
                document = new Invoice();/*Factura*/
            else
                document = new Document();

            //Document document = new Document();
            document.setDate(_venta.getFecha());
            document.setCompany(company);
            //document.setClient(client);
            document.setDocumentType(documentType);
            document.newDetails();
            document.newPayments();
            document.addPayment(payment);
            document.setTotal(String.valueOf(_venta.getTotal()));

        /*Unit*/
            Unit unit = new Unit();
            unit.setCode("Unid");

            int i = 0;
            for (VentaDetalleAbstract det : _venta.getDetallesAbstract()) {
                i++;

                /*Product*/
                Product product = new Product();
                product.setCode(det.getCode());
                product.setName(det.getDescripcion());
                product.setUnit(unit);
                product.setPrice(det.getPrecio());

                /*Detail*/
                Detail detail = new Detail();
                detail.setPosition(i);
                detail.setProduct(product);
                detail.setQuantity(((Double) det.getCantidad()).intValue());
                detail.setDescription(det.getDescripcion());

                document.addDetail(detail);
            }


/*
            OfflineServices offlineServices = OfflineServices.getInstance();
            String stamp = offlineServices.getStamp(document, xmlCaf, "5802");

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (nuevaBoletaListener != null)
                        nuevaBoletaListener.onBoletaGenerada(109, "33", "10293", "12132146");
                }
            });
*/
        } catch (Exception ex) {
            Log.e("NuevaBoleta", ex.toString());
        }

        return document;
    }

    private void runOnUiThread(Runnable run) {
        getActivity().runOnUiThread(run);
    }

    private void CerrarFragment() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                FragmentManager fm = getActivity().getFragmentManager();
                Fragment fragment = fm.findFragmentById(fragment_id);
                if (fragment != null) {
                    //cerramos el fragment
                    fm.beginTransaction()
                            .remove(fragment)
                            .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                            .commit();
                }
            }
        });

    }

}
