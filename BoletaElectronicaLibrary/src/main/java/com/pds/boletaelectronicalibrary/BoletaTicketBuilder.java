package com.pds.boletaelectronicalibrary;

import android.graphics.Bitmap;

import com.pds.common.Config;
import com.pds.common.Formatos;
import com.pds.common.model.Venta;
import com.pds.common.model.VentaDetalleAbstract;

import java.util.ArrayList;
import java.util.List;

import android_serialport_api.LineaTicket;
import android_serialport_api.Printer;
import android_serialport_api.Ticket_Builder;

/**
 * Created by Hernan on 17/02/2015.
 */
public class BoletaTicketBuilder extends Ticket_Builder {

    private String _tipoDocumento;
    private String _comercio;
    private String _direccion;
    private String _clavefiscal;
    private String _total;
    private String _fecha;
    private String _numero;
    private Bitmap _timbre;
    private List<LineaTicket> _lineas;
    private List<String> _giro;
    private String _resolucion;

    public void add_linea(LineaTicket _linea) {
        this._lineas.add(_linea);
    }

    public String get_comercio() {
        return _comercio;
    }

    public void set_comercio(String _comercio) {
        this._comercio = _comercio;
    }

    public String get_direccion() {
        return _direccion;
    }

    public void set_direccion(String _direccion) {
        this._direccion = _direccion;
    }

    public String get_clavefiscal() {
        return _clavefiscal;
    }

    public void set_clavefiscal(String _clavefiscal) {
        this._clavefiscal = _clavefiscal;
    }

    public String get_total() {
        return _total;
    }

    public void set_total(String _total) {
        this._total = _total;
    }

    public String get_fecha() {
        return _fecha;
    }

    public void set_fecha(String _fecha) {
        this._fecha = _fecha;
    }

    public String get_numero() {
        return _numero;
    }

    public void set_numero(String _numero) {
        this._numero = _numero;
    }

    public Bitmap get_timbre() {
        return _timbre;
    }

    public void set_timbre(Bitmap _timbre) {
        this._timbre = _timbre;
    }

    public String get_tipoDocumento() {
        return _tipoDocumento;
    }

    public void set_tipoDocumento(String _tipoDocumento) {
        this._tipoDocumento = _tipoDocumento;
    }

    public void set_Venta(Venta venta){
        this._numero = venta.getFc_folio();//venta.getNroVenta();
        this._fecha = venta.getFecha();
        //this._hora = venta.getHora();
        this._total = Formatos.FormateaDecimal(venta.getTotal(), Formatos.DecimalFormat_CH);
        //this._tipoDocumento = venta.getFc_type();

        set_Detalles(venta.getDetallesAbstract());
    }
    public void set_Detalles(List<VentaDetalleAbstract> detalles){
        for (VentaDetalleAbstract detalle : detalles) {
            add_linea(new LineaTicket(
                    Formatos.FormateaDecimal(detalle.getCantidad(), Formatos.DecimalFormat_US),
                    detalle.getDescripcion(),
                    Formatos.FormateaDecimal(detalle.getPrecio(), Formatos.DecimalFormat_CH),
                    Formatos.FormateaDecimal(detalle.getTotal(), Formatos.DecimalFormat_CH)
            ));
        }
    }

    public BoletaTicketBuilder(Config config) {
        this();

        this._comercio = config.COMERCIO;
        this._direccion = config.DIRECCION;
        this._clavefiscal = config.CLAVEFISCAL;
    }

    public BoletaTicketBuilder() {
        this._comercio = "";
        this._direccion = "";
        this._clavefiscal = "";
        this._fecha = "";
        this._numero = "";
        this._lineas = new ArrayList<LineaTicket>();
        this._total = "";
        this._resolucion = "Res. 0 de 2013";
        this._giro = new ArrayList<String>();
        this._giro.add("VENTA AL POR MENOR DE PRODUCTOS");
        this._giro.add(" DE PANADERIA Y PASTELERIA");
        this._giro.add("VENTA AL POR MENOR DE PRODUCTOS");
        this._giro.add(" DE CONFITERIAS,CIGARRILLOS,");
        this._giro.add(" Y OTROS");
    }

    @Override
    public void Print(Printer _printer) {

        _printer.lineFeed();
        _printer.lineFeed();

        //comercio
        _printer.format(2, 1, Printer.ALINEACION.CENTER);
        _printer.printLine(this._comercio);
        _printer.format(1, 1, Printer.ALINEACION.CENTER);
        _printer.lineFeed();
        _printer.printLine(this._clavefiscal);
        _printer.lineFeed();

        _printer.format(1, 1, Printer.ALINEACION.LEFT);
        //_printer.printLine("Boleta Electronica N°: " + this._numero);
        _printer.printLine(this._tipoDocumento + " N°: " + this._numero);
        _printer.lineFeed();
        _printer.printLine("C.Matriz: " + this._direccion);
        _printer.lineFeed();
        //actividades
        for(String g : this._giro){
            _printer.printLine(g);
        }
        _printer.lineFeed();
        _printer.printLine("Emision: " + this._fecha );
        _printer.lineFeed();
        _printer.sendSeparadorHorizontal();

        //items
        _printer.printOpposite("Articulo","Valor");
        _printer.printLine("Cant./Precio Unit.");
        _printer.sendSeparadorHorizontal();

        _printer.format(1, 1, Printer.ALINEACION.LEFT);

        for (LineaTicket linea : this._lineas) {

            /*String descripcion = linea.get_descripcion() + "                      "; //hacemos un padding de 22 (derecha)
            descripcion = descripcion.substring(0, 22);

            String subtotal = "       $ " + linea.get_subtotal() ; //hacemos un padding de 9
            subtotal = subtotal.substring(subtotal.length() - 9);

            String linea1 = descripcion + " " + subtotal;
            String linea2 = linea.get_cantidad() + " x $ " + linea.get_precio() ;

            _printer.print(linea1);
            _printer.printLine(linea2);*/

            int k = _printer.get_longitudLinea() - 10; //9 + 1 (" ")

            String descripcion = linea.get_descripcion() + _printer.getPadding(k); //hacemos un padding para cortar el nombre del producto en caso de excederse
            descripcion = descripcion.substring(0, k);

            String subtotal = "       $ " + linea.get_subtotal() ; //hacemos un padding de 9
            subtotal = subtotal.substring(subtotal.length() - 9);

            String linea2 = linea.get_cantidad() + " x $ " + linea.get_precio() ;

            _printer.printOpposite(descripcion, subtotal);
            _printer.printLine(linea2);
        }

        _printer.sendSeparadorHorizontal();

        //total
        _printer.format(1, 2, Printer.ALINEACION.LEFT);
        _printer.printLine("TOTAL $ " + this._total);

        _printer.cancelCurrentFormat();

        if(this._timbre != null) {
            _printer.lineFeed();
            _printer.lineFeed();

            _printer.printBitmap(this._timbre);

            _printer.lineFeed();
            _printer.lineFeed();

            _printer.format(1, 1, Printer.ALINEACION.CENTER);
            _printer.printLine("Timbre Electronico SII");
            _printer.printLine(this._resolucion);
            _printer.printLine("Verifique documento:");
            _printer.printLine("www.facturamovil.cl");
        }

        _printer.footer();

    }
}
