package cl.facturamovil.sdk.boo;

import android.util.Log;

import cl.facturamovil.sdk.dto.Detail;
import cl.facturamovil.sdk.dto.Document;
import cl.facturamovil.sdk.dto.Ticket;
import cl.facturamovil.sdk.util.Base64Coder;
import cl.facturamovil.sdk.util.GenericUtil;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Date;


import org.xml.sax.InputSource;


public class OfflineServices {

    private static OfflineServices instance;

    static {
        instance = new OfflineServices();
    }

    private OfflineServices() {
        java.security.Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    public static OfflineServices getInstance() {
        return instance;
    }

    /*public String getDeveloperName(String anyString) {
        return "Developer name: Enrique Falcón Villafuerte " + anyString;
    }*/

    public String getStamp(Document document, String xmlCaf, String folio) {
        try {
            Date stampDate = new Date();
            StringWriter writer = new StringWriter();

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            // root element TED
            org.w3c.dom.Document doc = builder.newDocument();
            Element rootElement = doc.createElement("TED");
            doc.appendChild(rootElement);
            Attr attrTed = doc.createAttribute("version");
            attrTed.setValue("1.0");
            rootElement.setAttributeNode(attrTed);
            // DD
            Element dd = doc.createElement("DD");
            rootElement.appendChild(dd);
            // company code
            Element re = doc.createElement("RE");
            re.appendChild(doc.createTextNode(document.getCompany().getCode()));
            dd.appendChild(re);
            // document type
            Element td = doc.createElement("TD");
            td.appendChild(doc.createTextNode(document.getDocumentType().getCode()));
            dd.appendChild(td);
            // assigned folio
            Element f = doc.createElement("F");
            f.appendChild(doc.createTextNode(folio));
            dd.appendChild(f);
            // document date
            Element fe = doc.createElement("FE");
            fe.appendChild(doc.createTextNode(document.getDate()));
            dd.appendChild(fe);
            // client
            Element rr = doc.createElement("RR");
            Element rsr = doc.createElement("RSR");
            if (document instanceof Ticket) {
                // client code
                rr.appendChild(doc.createTextNode("66666666-6"));
                dd.appendChild(rr);
                //client name
                rsr.appendChild(doc.createTextNode("Cliente"));
                dd.appendChild(rsr);
            } else {
                // client code
                rr.appendChild(doc.createTextNode(document.getClient().getCode()));
                dd.appendChild(rr);
                //client name
                String clientName = GenericUtil.cleanTedCharacters(document.getClient().getName());
                if (clientName.length() >= 40)
                    clientName = clientName.substring(0, 40);
                rsr.appendChild(doc.createTextNode(clientName));
                dd.appendChild(rsr);
            }
            // amount
            Element mnt = doc.createElement("MNT");
            mnt.appendChild(doc.createTextNode(document.getTotal()));
            dd.appendChild(mnt);
            // product or service
            Detail detail = document.getDetails().get(0);
            Element it1 = doc.createElement("IT1");
            if (detail.getProduct() != null) {
                //product
                String productName = GenericUtil.formattedForXml(detail.getProduct().getName(), 40);
                it1.appendChild(doc.createTextNode(productName));
                dd.appendChild(it1);
            } else {
                //service
                String serviceName = GenericUtil.formattedForXml(detail.getService().getName(), 40);
                it1.appendChild(doc.createTextNode(serviceName));
                dd.appendChild(it1);
            }
            // caf
            Element caf = doc.createElement("CAF");
            dd.appendChild(caf);
            Attr attrCaf = doc.createAttribute("version");
            attrCaf.setValue("1.0");
            caf.setAttributeNode(attrCaf);

            org.w3c.dom.Document xml = convertStringToDocument(xmlCaf);// documentBuilder.parse(file);
            Element element = xml.getDocumentElement();
            // DA
            Element da = doc.createElement("DA");
            caf.appendChild(da);
            // RE
            Element _re = doc.createElement("RE");
            //RS
            Element _rs = doc.createElement("RS");
            //TD
            Element _td = doc.createElement("TD");

            String companyCode = (((Element) (element.getElementsByTagName("DA")).item(0)).getElementsByTagName("RE")).item(0).getChildNodes().item(0).getNodeValue();
            String companyName = (((Element) (element.getElementsByTagName("DA")).item(0)).getElementsByTagName("RS")).item(0).getChildNodes().item(0).getNodeValue();
            String documentType = (((Element) (element.getElementsByTagName("DA")).item(0)).getElementsByTagName("TD")).item(0).getChildNodes().item(0).getNodeValue();

            _re.appendChild(doc.createTextNode(companyCode));
            _rs.appendChild(doc.createTextNode(companyName));
            _td.appendChild(doc.createTextNode(documentType));

            da.appendChild(_re);
            da.appendChild(_rs);
            da.appendChild(_td);

            //RNG
            Element rng = doc.createElement("RNG");
            da.appendChild(rng);
            //D
            Element _d = doc.createElement("D");
            String d = (((Element) (element.getElementsByTagName("RNG")).item(0)).getElementsByTagName("D")).item(0).getChildNodes().item(0).getNodeValue();
            _d.appendChild(doc.createTextNode(d));
            rng.appendChild(_d);
            //H
            Element _h = doc.createElement("H");
            String h = (((Element) (element.getElementsByTagName("RNG")).item(0)).getElementsByTagName("H")).item(0).getChildNodes().item(0).getNodeValue();
            _h.appendChild(doc.createTextNode(h));
            rng.appendChild(_h);
            if (Integer.parseInt(folio.trim()) < Integer.parseInt(d) || Integer.parseInt(folio.trim()) > Integer.parseInt(h)) {
                System.out.println("Folio entered is out of range");
                throw new RuntimeException(null, new Exception());
            }
            //FA
            Element _fa = doc.createElement("FA");
            String fa = (((Element) (element.getElementsByTagName("DA")).item(0)).getElementsByTagName("FA")).item(0).getChildNodes().item(0).getNodeValue();
            _fa.appendChild(doc.createTextNode(fa));
            da.appendChild(_fa);
            //RSAPK
            Element rsapk = doc.createElement("RSAPK");
            da.appendChild(rsapk);
            //M
            Element _m = doc.createElement("M");
            String m = (((Element) (element.getElementsByTagName("DA")).item(0)).getElementsByTagName("M")).item(0).getChildNodes().item(0).getNodeValue();
            _m.appendChild(doc.createTextNode(m));
            rsapk.appendChild(_m);
            //E
            Element _e = doc.createElement("E");
            String e = (((Element) (element.getElementsByTagName("DA")).item(0)).getElementsByTagName("E")).item(0).getChildNodes().item(0).getNodeValue();
            _e.appendChild(doc.createTextNode(e));
            rsapk.appendChild(_e);
            //IDK
            Element _idk = doc.createElement("IDK");
            String idk = (((Element) (element.getElementsByTagName("DA")).item(0)).getElementsByTagName("IDK")).item(0).getChildNodes().item(0).getNodeValue();
            _idk.appendChild(doc.createTextNode(idk));
            da.appendChild(_idk);
            //FRMA
            Element _frma = doc.createElement("FRMA");
            String frma = (((Element) (element.getElementsByTagName("CAF")).item(0)).getElementsByTagName("FRMA")).item(0).getChildNodes().item(0).getNodeValue();
            Attr attrFrma = doc.createAttribute("algoritmo");
            attrFrma.setValue("SHA1withRSA");
            _frma.setAttributeNode(attrFrma);
            _frma.appendChild(doc.createTextNode(frma));
            caf.appendChild(_frma);

            String privateKey = (((Element) (xml.getElementsByTagName("*")).item(0)).getElementsByTagName("RSASK")).item(0).getChildNodes().item(0).getNodeValue();
            privateKey = privateKey.replace("-----BEGIN RSA PRIVATE KEY-----", "");
            privateKey = privateKey.replace("-----END RSA PRIVATE KEY-----", "");
            privateKey = privateKey.replace(" ", "");

            //TSTED
            Element tsted = doc.createElement("TSTED");
            tsted.appendChild(doc.createTextNode(String.format("%tY-%<tm-%<tdT%<tH:%<tM:%<tS", stampDate)));
            dd.appendChild(tsted);
            //FRMT
            Element _frmt = doc.createElement("FRMT");
            Attr attrFrmt = doc.createAttribute("algoritmo");
            attrFrmt.setValue("SHA1withRSA");
            _frmt.setAttributeNode(attrFrmt);
            _frmt.appendChild(doc.createTextNode("@STAMPSIGN"));
            rootElement.appendChild(_frmt);

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(xml.toString());
            result.setWriter(writer);
            transformer.transform(source, result);
            String stamp = writer.toString();

            stamp = GenericUtil.transformToIso88591(stamp, false, false);
//            String stampSign = signStamp(stamp, privateKey);
            stamp = stamp.replace("@STAMPSIGN", signStamp(stamp, privateKey));
            stamp = stamp + "<TmstFirma>" + String.format("%tY-%<tm-%<tdT%<tH:%<tM:%<tS", new Date()) + "</TmstFirma>";
//            System.out.println("stamp -> " + stamp);
            return stamp;
        } catch (Exception ex) {
            System.out.println("Failed to generate signature. " + ex.getMessage());
            return ex.getMessage();
        }
    }

    public boolean isValidFolio(String xmlCaf, long folio){
        org.w3c.dom.Document xml = convertStringToDocument(xmlCaf);// documentBuilder.parse(file);
        Element element = xml.getDocumentElement();
        //D
        String d = (((Element) (element.getElementsByTagName("RNG")).item(0)).getElementsByTagName("D")).item(0).getChildNodes().item(0).getNodeValue();
        //H
        String h = (((Element) (element.getElementsByTagName("RNG")).item(0)).getElementsByTagName("H")).item(0).getChildNodes().item(0).getNodeValue();
        if (folio < Integer.parseInt(d) ||folio > Integer.parseInt(h)) {
            Log.e("NuevaBoleta","Folio entered is out of range");
            //throw new RuntimeException(null, new Exception());
            return false;
        }
        return true;
    }

    private static org.w3c.dom.Document convertStringToDocument(String xmlStr) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = factory.newDocumentBuilder();
            org.w3c.dom.Document doc = builder.parse(new InputSource(new StringReader(xmlStr)));
            return doc;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String signStamp(String stamp, String privateKeyStr) {
        try {
            int lastIndex = stamp.lastIndexOf("<FRMT");
            String dd = stamp.substring(0, lastIndex).replace("<TED version=\"1.0\">", "");

            // Generate KeyPair.
            KeyFactory keyFactory = KeyFactory.getInstance("RSA", "BC");
            PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(Base64Coder.decode(privateKeyStr));
            PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);

            return GenericUtil.signSHA1WithRSA(dd, privateKey);
        } catch (Exception ex) {
            System.out.println("signStamp error -> " + ex.getLocalizedMessage());
        }

        return null;
    }

}
