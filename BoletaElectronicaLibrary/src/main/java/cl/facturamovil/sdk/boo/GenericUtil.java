package cl.facturamovil.sdk.boo;

//import org.apache.commons.codec.binary.Base64;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.security.PrivateKey;
import java.security.Signature;

public class GenericUtil {

    public static String cleanTedCharacters(String input) {
        String output = input.
                replaceAll("  ", " ");
        return output;
    }

    public static String formattedForXml(String input, int maxLength) {
        String output = input;
        output = cleanTedCharacters(output);
        output = (output.length() <= maxLength) ? output : output.substring(0, maxLength).trim();
        return output;
    }

    public static String transformToIso88591(String xml, boolean includeHeader, boolean indent) {
        try {
            // indent only for reception document
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            dbFactory.setNamespaceAware(true);
            InputSource inputSource = new InputSource();
            inputSource.setEncoding("ISO-8859-1");
            inputSource.setCharacterStream(new StringReader(xml));
            Document doc = dbFactory.newDocumentBuilder().parse(inputSource);

            OutputStream outputStream = new ByteArrayOutputStream();
            Transformer trans = TransformerFactory.newInstance().newTransformer();
            trans.setOutputProperty(OutputKeys.METHOD, "xml");
            trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, (includeHeader) ? "no" : "yes");
            trans.setOutputProperty("{http://xml.apache.org/xslt}indent-value", "2");
            trans.setOutputProperty(OutputKeys.INDENT, (indent) ? "yes" : "no");
            trans.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
            trans.transform(new DOMSource(doc.getDocumentElement()), new StreamResult(outputStream));

            return outputStream.toString();
        } catch (Exception ex) {
            return "";
        }
    }

    public static String signSHA1WithRSA(String inputText, PrivateKey privateKey) {
        try {
            // Compute signature
            Signature instance = Signature.getInstance("SHA1withRSA");
            instance.initSign(privateKey);
            instance.update(inputText.getBytes("ISO-8859-1"));
            byte[] signature = instance.sign();

//            String output = new String(Base64.encodeBase64(signature), "ISO-8859-1");
            String output = new String(cl.facturamovil.sdk.util.Base64Coder.encodeToByte(signature, false), "ISO-8859-1");

            return output;
        } catch (Exception ex) {
            System.out.println("signSHA1WithRSA error -> " + ex.getMessage());
            return "";
        }
    }

}
