package com.pds.recarga.mensajes;

/**
 * Created by Hernan on 04/05/2016.
 */
public abstract class MessageType {
    public final static String REQUEST = "0200";
    public final static String RESPONSE = "0210";
}
