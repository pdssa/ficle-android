package com.pds.recarga.mensajes;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hernan on 19/12/13.
 */
public class MensajeProductoRecarga {
    private String Codigo;
    private String Descripcion;
    private String Prefijo;
    private String SaldoProducto;
    private String LimiteCreditoProducto;
    private String CantMontosFijos;
    private String MinDigitos;
    private String MaxDigitos;
    private String DescIngreso;
    private String IndRequiereNroClave;
    private String TipoDeCarga;
    private String AdmSubProd;
    private String ProdSup;
    private String PieTicket;
    private List<MensajeMontoFijoRecarga> MontosFijos;
    private String CantidadOperaciones;
    private String MontoOperaciones;
    private String MontoGanancia;

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String codigo) {
        Codigo = codigo;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getPrefijo() {
        return Prefijo;
    }

    public void setPrefijo(String prefijo) {
        Prefijo = prefijo;
    }

    public String getSaldoProducto() {
        return SaldoProducto;
    }

    public void setSaldoProducto(String saldoProducto) {
        SaldoProducto = saldoProducto;
    }

    public String getLimiteCreditoProducto() {
        return LimiteCreditoProducto;
    }

    public void setLimiteCreditoProducto(String limiteCreditoProducto) {
        LimiteCreditoProducto = limiteCreditoProducto;
    }

    public String getCantMontosFijos() {
        return CantMontosFijos;
    }

    public void setCantMontosFijos(String cantMontosFijos) {
        CantMontosFijos = cantMontosFijos;
    }

    public String getMinDigitos() {
        return MinDigitos;
    }

    public void setMinDigitos(String minDigitos) {
        MinDigitos = minDigitos;
    }

    public String getMaxDigitos() {
        return MaxDigitos;
    }

    public void setMaxDigitos(String maxDigitos) {
        MaxDigitos = maxDigitos;
    }

    public String getDescIngreso() {
        return DescIngreso;
    }

    public void setDescIngreso(String descIngreso) {
        DescIngreso = descIngreso;
    }

    public String getIndRequiereNroClave() {
        return IndRequiereNroClave;
    }

    public void setIndRequiereNroClave(String indRequiereNroClave) {
        IndRequiereNroClave = indRequiereNroClave;
    }

    public String getTipoDeCarga() {
        return TipoDeCarga;
    }

    public void setTipoDeCarga(String tipoDeCarga) {
        TipoDeCarga = tipoDeCarga;
    }

    public String getAdmSubProd() {
        return AdmSubProd;
    }

    public void setAdmSubProd(String admSubProd) {
        AdmSubProd = admSubProd;
    }

    public String getProdSup() {
        return ProdSup;
    }

    public void setProdSup(String prodSup) {
        ProdSup = prodSup;
    }

    public String getPieTicket() {
        return PieTicket;
    }

    public void setPieTicket(String pieTicket) {
        PieTicket = pieTicket;
    }

    public List<MensajeMontoFijoRecarga> getMontosFijos() {
        if(this.MontosFijos == null) //lo inicializamos
            MontosFijos = new ArrayList<MensajeMontoFijoRecarga>();

        return MontosFijos;
    }

    public void setMontosFijos(List<MensajeMontoFijoRecarga> montosFijos) {
        MontosFijos = montosFijos;
    }

    public String getCantidadOperaciones() {
        return CantidadOperaciones != null ? CantidadOperaciones : "0";
    }

    public void setCantidadOperaciones(String cantidadOperaciones) {
        CantidadOperaciones = cantidadOperaciones;
    }

    public String getMontoOperaciones() {
        return MontoOperaciones != null ? MontoOperaciones : "0";
    }

    public void setMontoOperaciones(String montoOperaciones) {
        MontoOperaciones = montoOperaciones;
    }

    public String getMontoGanancia() {
        return MontoGanancia != null ? MontoGanancia : "0";
    }

    public void setMontoGanancia(String montoGanancia) {
        MontoGanancia = montoGanancia;
    }

    //si MinDigitos y MaxDigitos vienen con valor 0
    //no se debe solicitar el ingreso del "Nro de telefono"
    public boolean hayQueSolicitarNroTelefono(){
        return !this.MinDigitos.equals("0") || !this.MaxDigitos.equals("0");
    }
}
