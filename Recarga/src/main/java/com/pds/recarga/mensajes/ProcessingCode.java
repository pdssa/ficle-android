package com.pds.recarga.mensajes;

/**
 * Created by Hernan on 04/05/2016.
 */
public abstract class ProcessingCode {
    public final static String RECARGA_NORMAL = "201105";
    public final static String CONSULTA_TRANSACCION_RECARGA_NORMAL = "301105";
    public final static String CONSULTA_COMERCIAL = "301005";
    public final static String CONSULTA_VENTAS = "300905";
    public final static String CONSULTA_ULTIMAS_RECARGAS = "303305";
    public final static String VENTA_ABONO_COMERCIO = "205505";
    public final static String VISITA_VENDEDOR = "205605";
    public final static String CONSULTA_ULTIMOS_ABONOS = "305505";
    public final static String CONSULTA_MENSAJES_LEIDOS = "300105";
    public final static String ABONOS_PREMIO_COMERCIO = "205805";
    public final static String AVISO_DEPOSITO_CLIENTE = "206505";
    public final static String AVISO_DEPOSITO_VENDEDOR = "206605";
    public final static String CONSULTA_DATOS_REFERENCIALES = "305105";
    public final static String RETIRO_TERMINAL = "206105";
    public final static String MENSAJES_TERMINAL = "206205";
}
