package com.pds.recarga.mensajes;

/**
 * Created by Hernan on 04/05/2016.
 */
public class BancoBNCRecarga {
    private String Codigo;
    private String Desc;
    private String DepCli;
    private String DepVen;
    private String EmiCheque;

    public BancoBNCRecarga() {
    }

    public String getCodigo() {

        return Codigo;
    }

    public void setCodigo(String codigo) {
        Codigo = codigo;
    }

    public String getDesc() {
        return Desc;
    }

    public void setDesc(String descr) {
        Desc = descr;
    }

    public String getDepCli() {
        return DepCli;
    }

    public void setDepCli(String depCli) {
        DepCli = depCli;
    }

    public String getDepVen() {
        return DepVen;
    }

    public void setDepVen(String depVen) {
        DepVen = depVen;
    }

    public String getEmiCheque() {
        return EmiCheque;
    }

    public void setEmiCheque(String emiCheque) {
        EmiCheque = emiCheque;
    }
}
