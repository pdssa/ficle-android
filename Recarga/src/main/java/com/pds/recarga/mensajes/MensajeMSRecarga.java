package com.pds.recarga.mensajes;

/**
 * Created by Hernan on 09/01/14.
 */
public class MensajeMSRecarga {
    private String MsgID;
    private String Fecha;
    private String De;
    private String Texto;

    public String getMsgID() {
        return MsgID;
    }

    public void setMsgID(String msgID) {
        MsgID = msgID;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String fecha) {
        Fecha = fecha;
    }

    public String getDe() {
        return De;
    }

    public void setDe(String de) {
        De = de;
    }

    public String getTexto() {
        return Texto;
    }

    public void setTexto(String texto) {
        Texto = texto;
    }

    public MensajeMSRecarga(){

    }
}
