package com.pds.recarga.mensajes;

import com.pds.recarga.utils.XmlRecargaMaker;

import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hernan on 19/12/13.
 */
public class MensajeRecarga {

    private String NroAuditoriaTerminal;
    private String FechaHoraTerminal;
    private String ModoEntradaTerminal;
    private String CondicionTerminal;
    private String NroTransaccion;
    private String CodigoRespuesta;
    private String FechaHora;
    private String IdentTerminal;
    private String IMEITerminal;
    private String Vapp;
    private String IMSITerminal;
    private String NroSerieTerminal;
    private String NroSerieSIM;
    private String NroTickets;
    private String RazonSocialCliente;
    private String RucCliente;
    private String IDExtCliente;
    private String DomicilioSucursal;
    private String RegionSucursal;
    private String ComunaSucursal;
    private String MensajeLinea1;
    private String MensajeLinea2;
    private String CantProductos;
    private String Root_TipoMensaje;
    private String Root_CodigoProceso;
    private String Root_NumeroComercio;
    private List<MensajeProductoRecarga> Productos;
    private String NroTarjeta;
    private String MontoTransaccion;
    private String DatosAdicionales;
    private String Moneda;
    private String TipoProductoConsumido;
    private String SaldoTarjeta;
    private String SaldoComercio;
    private String CodAutorizacion;
    private String IndMsgNl;
    private String CantMS;
    private String CantTX;
    private List<MensajeMSRecarga> Mensajes;
    private List<MensajeTXRecarga> TXs;
    private String CorteInicio;
    private String CorteFin;
    private String TotalOperaciones;
    private String TotalMonto;
    private String TotalGanancia;
    private String TotalOperacionesMes;
    private String TotalMontoMes;
    private String TotalGananciaMes;
    private String FechaDeposito;
    private String LugarDeDeposito;
    private String MontoDepositado;
    private String NroComprobante;
    private String TipoDeposito;
    private String OrigenCheque;
    private String NroCheque;
    private String NroVendedor;
    private String PINVendedor;
    private String MontoAbono;
    private String Abono;
    private List<BancoBNCRecarga> Bancos;
    private List<MotivoRetiroMOTRETRecarga> MotivosRetiro;
    private List<MsgsTermMSGPOSRecarga> MensajesTerminal;
    private List<PropDepDAVISORecarga> PropositoDeposito;
    private String CodigoMensaje;


    public String getMontoAbono() {
        return MontoAbono;
    }

    public void setMontoAbono(String montoAbono) {
        MontoAbono = montoAbono;
    }

    public String getAbono() {
        return Abono;
    }

    public void setAbono(String abono) {
        Abono = abono;
    }

    public String getNroTarjeta() {
        return NroTarjeta;
    }

    public void setNroTarjeta(String nroTarjeta) {
        NroTarjeta = nroTarjeta;
    }

    public String getMontoTransaccion() {
        return MontoTransaccion;
    }

    public void setMontoTransaccion(String montoTransaccion) {
        MontoTransaccion = montoTransaccion;
    }

    public String getDatosAdicionales() {
        return DatosAdicionales;
    }

    public void setDatosAdicionales(String datosAdicionales) {
        DatosAdicionales = datosAdicionales;
    }

    public String getMoneda() {
        return Moneda;
    }

    public void setMoneda(String moneda) {
        Moneda = moneda;
    }

    public String getTipoProductoConsumido() {
        return TipoProductoConsumido;
    }

    public void setTipoProductoConsumido(String tipoProductoConsumido) {
        TipoProductoConsumido = tipoProductoConsumido;
    }

    public String getSaldoTarjeta() {
        return SaldoTarjeta;
    }

    public void setSaldoTarjeta(String saldoTarjeta) {
        SaldoTarjeta = saldoTarjeta;
    }

    public String getSaldoComercio() {
        return SaldoComercio;
    }

    public void setSaldoComercio(String saldoComercio) {
        SaldoComercio = saldoComercio;
    }

    public String getCodAutorizacion() {
        return CodAutorizacion;
    }

    public void setCodAutorizacion(String codAutorizacion) {
        CodAutorizacion = codAutorizacion;
    }

    public String getIndMsgNl() {
        return IndMsgNl;
    }

    public void setIndMsgNl(String indMsgNl) {
        IndMsgNl = indMsgNl;
    }

    public String getNroAuditoriaTerminal() {
        return NroAuditoriaTerminal;
    }

    public void setNroAuditoriaTerminal(String nroAuditoriaTerminal) {
        NroAuditoriaTerminal = nroAuditoriaTerminal;
    }

    public String getFechaHoraTerminal() {
        return FechaHoraTerminal;
    }

    public void setFechaHoraTerminal(String fechaHoraTerminal) {
        FechaHoraTerminal = fechaHoraTerminal;
    }

    public String getModoEntradaTerminal() {
        return ModoEntradaTerminal;
    }

    public void setModoEntradaTerminal(String modoEntradaTerminal) {
        ModoEntradaTerminal = modoEntradaTerminal;
    }

    public String getCondicionTerminal() {
        return CondicionTerminal;
    }

    public void setCondicionTerminal(String condicionTerminal) {
        CondicionTerminal = condicionTerminal;
    }

    public String getNroTransaccion() {
        return NroTransaccion;
    }

    public void setNroTransaccion(String nroTransaccion) {
        NroTransaccion = nroTransaccion;
    }

    public String getCodigoRespuesta() {
        return CodigoRespuesta;
    }

    public void setCodigoRespuesta(String codigoRespuesta) {
        CodigoRespuesta = codigoRespuesta;
    }

    public String getFechaHora() {
        return FechaHora;
    }

    public void setFechaHora(String fechaHora) {
        FechaHora = fechaHora;
    }

    public String getIdentTerminal() {
        return IdentTerminal;
    }

    public void setIdentTerminal(String identTerminal) {
        IdentTerminal = identTerminal;
    }

    public String getIMEITerminal() {
        return IMEITerminal;
    }

    public void setIMEITerminal(String IMEITerminal) {
        this.IMEITerminal = IMEITerminal;
    }

    public String getVapp() {
        return Vapp;
    }

    public void setVapp(String vapp) {
        Vapp = vapp;
    }

    public String getIMSITerminal() {
        return IMSITerminal;
    }

    public void setIMSITerminal(String IMSITerminal) {
        this.IMSITerminal = IMSITerminal;
    }

    public String getNroSerieTerminal() {
        return NroSerieTerminal;
    }

    public void setNroSerieTerminal(String nroSerieTerminal) {
        NroSerieTerminal = nroSerieTerminal;
    }

    public String getNroSerieSIM() {
        return NroSerieSIM;
    }

    public void setNroSerieSIM(String nroSerieSIM) {
        NroSerieSIM = nroSerieSIM;
    }

    public String getNroTickets() {
        return NroTickets;
    }

    public void setNroTickets(String nroTickets) {
        NroTickets = nroTickets;
    }

    public String getRazonSocialCliente() {
        return RazonSocialCliente;
    }

    public void setRazonSocialCliente(String razonSocialCliente) {
        RazonSocialCliente = razonSocialCliente;
    }

    public String getRucCliente() {
        return RucCliente;
    }

    public void setRucCliente(String rucCliente) {
        RucCliente = rucCliente;
    }

    public String getIDExtCliente() {
        return IDExtCliente;
    }

    public void setIDExtCliente(String IDExtCliente) {
        this.IDExtCliente = IDExtCliente;
    }

    public String getDomicilioSucursal() {
        return DomicilioSucursal;
    }

    public void setDomicilioSucursal(String domicilioSucursal) {
        DomicilioSucursal = domicilioSucursal;
    }

    public String getRegionSucursal() {
        return RegionSucursal;
    }

    public void setRegionSucursal(String regionSucursal) {
        RegionSucursal = regionSucursal;
    }

    public String getComunaSucursal() {
        return ComunaSucursal;
    }

    public void setComunaSucursal(String comunaSucursal) {
        ComunaSucursal = comunaSucursal;
    }

    public String getMensajeLinea1() {
        return MensajeLinea1 != null ? MensajeLinea1 : "";
    }

    public void setMensajeLinea1(String mensajeLinea1) {
        MensajeLinea1 = mensajeLinea1;
    }

    public String getMensajeLinea2() { return MensajeLinea2 != null ? MensajeLinea2 : ""; }

    public void setMensajeLinea2(String mensajeLinea2) {
        MensajeLinea2 = mensajeLinea2;
    }

    public String getCantProductos() {
        return CantProductos;
    }

    public void setCantProductos(String cantProductos) {
        CantProductos = cantProductos;
    }

    public String getRoot_TipoMensaje() {
        return Root_TipoMensaje;
    }

    public void setRoot_TipoMensaje(String root_TipoMensaje) {
        Root_TipoMensaje = root_TipoMensaje;
    }

    public String getRoot_CodigoProceso() {
        return Root_CodigoProceso;
    }

    public void setRoot_CodigoProceso(String root_CodigoProceso) {
        Root_CodigoProceso = root_CodigoProceso;
    }

    public String getRoot_NumeroComercio() {
        return Root_NumeroComercio;
    }

    public void setRoot_NumeroComercio(String root_NumeroComercio) {
        Root_NumeroComercio = root_NumeroComercio;
    }

    public List<MensajeProductoRecarga> getProductos() {
        if (this.Productos == null)
            this.Productos = new ArrayList<MensajeProductoRecarga>();

        return Productos;
    }

    public void setProductos(List<MensajeProductoRecarga> productos) {
        Productos = productos;
    }

    public void setTXs(List<MensajeTXRecarga> TXs) {
        this.TXs = TXs;
    }

    public List<MensajeTXRecarga> getTXs() {
        if (this.TXs == null)
            this.TXs = new ArrayList<MensajeTXRecarga>();

        return TXs;
    }

    public String getCantMS() {
        return CantMS;
    }

    public void setCantMS(String cantMS) {
        CantMS = cantMS;
    }

    public void setMensajes(List<MensajeMSRecarga> mensajes) {
        Mensajes = mensajes;
    }

    public List<MensajeMSRecarga> getMensajes() {
        if (this.Mensajes == null)
            this.Mensajes = new ArrayList<MensajeMSRecarga>();

        return Mensajes;
    }

    public List<BancoBNCRecarga> getBancos() {
        if (this.Bancos == null)
            this.Bancos = new ArrayList<BancoBNCRecarga>();

        return Bancos;
    }

    public void setBancos(List<BancoBNCRecarga> bancos) {
        Bancos = bancos;
    }

    public List<MotivoRetiroMOTRETRecarga> getMotivosRetiro() {
        if (this.MotivosRetiro == null)
            this.MotivosRetiro = new ArrayList<MotivoRetiroMOTRETRecarga>();

        return MotivosRetiro;
    }

    public void setMotivosRetiro(List<MotivoRetiroMOTRETRecarga> motivosRetiro) {
        MotivosRetiro = motivosRetiro;
    }

    public List<MsgsTermMSGPOSRecarga> getMensajesTerminal() {
        if (this.MensajesTerminal == null)
            this.MensajesTerminal = new ArrayList<MsgsTermMSGPOSRecarga>();

        return MensajesTerminal;
    }

    public void setMensajesTerminal(List<MsgsTermMSGPOSRecarga> mensajesTerminal) {
        MensajesTerminal = mensajesTerminal;
    }

    public List<PropDepDAVISORecarga> getPropositoDeposito() {
        if (this.PropositoDeposito == null)
            this.PropositoDeposito = new ArrayList<PropDepDAVISORecarga>();

        return PropositoDeposito;
    }

    public void setPropositoDeposito(List<PropDepDAVISORecarga> propositoDeposito) {
        PropositoDeposito = propositoDeposito;
    }

    public void setCantTX(String cantTX) {
        CantTX = cantTX;
    }

    public String getCantTX() {
        return CantTX;
    }

    public String getFormaPago() {
        return this.DatosAdicionales.substring(0, 2);
    }


    public String getBonosAcumulados() {
        return this.DatosAdicionales.substring(0, 5).replaceFirst("^0+(?!$)", "");
    }

    public String getDescuentosAcumulados() {
        return this.DatosAdicionales.substring(5, 10).replaceFirst("^0+(?!$)", "");
    }

    public String getValidezRecarga() {
        return this.DatosAdicionales.substring(10, 12).replaceFirst("^0+(?!$)", "");
    }

    public String getMensajePromocional() {
        return this.DatosAdicionales.substring(12);
    }

    public void setCorteFin(String corteFin) {
        CorteFin = corteFin;
    }

    public String getCorteFin() {
        return CorteFin;
    }

    public void setCorteInicio(String corteInicio) {
        CorteInicio = corteInicio;
    }

    public String getCorteInicio() {
        return CorteInicio;
    }

    public String getTotalOperaciones() {
        return TotalOperaciones;
    }

    public void setTotalOperaciones(String totalOperaciones) {
        TotalOperaciones = totalOperaciones;
    }

    public String getTotalMonto() {
        return TotalMonto;
    }

    public void setTotalMonto(String totalMonto) {
        TotalMonto = totalMonto;
    }

    public String getTotalGanancia() {
        return TotalGanancia;
    }

    public void setTotalGanancia(String totalGanancia) {
        TotalGanancia = totalGanancia;
    }

    public String getTotalOperacionesMes() {
        return TotalOperacionesMes;
    }

    public void setTotalOperacionesMes(String totalOperacionesMes) {
        TotalOperacionesMes = totalOperacionesMes;
    }

    public String getTotalMontoMes() {
        return TotalMontoMes;
    }

    public void setTotalMontoMes(String totalMontoMes) {
        TotalMontoMes = totalMontoMes;
    }

    public String getTotalGananciaMes() {
        return TotalGananciaMes;
    }

    public void setTotalGananciaMes(String totalGananciaMes) {
        TotalGananciaMes = totalGananciaMes;
    }

    public String getFechaDeposito() {
        return FechaDeposito;
    }

    public void setFechaDeposito(String fechaDeposito) {
        FechaDeposito = fechaDeposito;
    }

    public String getLugarDeDeposito() {
        return LugarDeDeposito;
    }

    public void setLugarDeDeposito(String lugarDeDeposito) {
        LugarDeDeposito = lugarDeDeposito;
    }

    public String getMontoDepositado() {
        return MontoDepositado;
    }

    public void setMontoDepositado(String montoDepositado) {
        MontoDepositado = montoDepositado;
    }

    public String getNroComprobante() {
        return NroComprobante;
    }

    public void setNroComprobante(String nroComprobante) {
        NroComprobante = nroComprobante;
    }

    public String getTipoDeposito() {
        return TipoDeposito;
    }

    public void setTipoDeposito(String tipoDeposito) {
        TipoDeposito = tipoDeposito;
    }

    public String getOrigenCheque() {
        return OrigenCheque;
    }

    public void setOrigenCheque(String origenCheque) {
        OrigenCheque = origenCheque;
    }

    public String getNroCheque() {
        return NroCheque;
    }

    public void setNroCheque(String nroCheque) {
        NroCheque = nroCheque;
    }

    public String getNroVendedor() {
        return NroVendedor;
    }

    public void setNroVendedor(String nroVendedor) {
        NroVendedor = nroVendedor;
    }

    public String getPINVendedor() {
        return PINVendedor;
    }

    public void setPINVendedor(String PINVendedor) {
        this.PINVendedor = PINVendedor;
    }

    public String getCodigoMensaje() {
        return CodigoMensaje;
    }

    public void setCodigoMensaje(String codigoMensaje) {
        CodigoMensaje = codigoMensaje;
    }

    public MensajeRecarga() {

    }

    public MensajeRecarga(MensajeRecarga otroMensaje) {
        this.NroAuditoriaTerminal = otroMensaje.getNroAuditoriaTerminal();
        this.FechaHoraTerminal = otroMensaje.getFechaHoraTerminal();
        this.ModoEntradaTerminal = otroMensaje.getModoEntradaTerminal();
        this.CondicionTerminal = otroMensaje.getCondicionTerminal();
        this.NroTransaccion = otroMensaje.getNroTransaccion();
        this.CodigoRespuesta = otroMensaje.getCodigoRespuesta();
        this.FechaHora = otroMensaje.getFechaHora();
        this.IdentTerminal = otroMensaje.getIdentTerminal();
        this.IMEITerminal = otroMensaje.getIMEITerminal();
        this.Vapp = otroMensaje.getVapp();
        this.IMSITerminal = otroMensaje.getIMSITerminal();
        this.NroSerieTerminal = otroMensaje.getNroSerieTerminal();
        this.NroSerieSIM = otroMensaje.getNroSerieSIM();
        this.NroTickets = otroMensaje.getNroTickets();
        this.RazonSocialCliente = otroMensaje.getRazonSocialCliente();
        this.RucCliente = otroMensaje.getRucCliente();
        this.IDExtCliente = otroMensaje.getIDExtCliente();
        this.DomicilioSucursal = otroMensaje.getDomicilioSucursal();
        this.RegionSucursal = otroMensaje.getRegionSucursal();
        this.ComunaSucursal = otroMensaje.getComunaSucursal();
        this.MensajeLinea1 = otroMensaje.getMensajeLinea1();
        this.MensajeLinea2 = otroMensaje.getMensajeLinea2();
        this.CantProductos = otroMensaje.getCantProductos();
        this.Root_TipoMensaje = otroMensaje.getRoot_TipoMensaje();
        this.Root_CodigoProceso = otroMensaje.getRoot_CodigoProceso();
        this.Root_NumeroComercio = otroMensaje.getRoot_NumeroComercio();
        this.Productos = otroMensaje.getProductos();
        this.NroTarjeta = otroMensaje.getNroTarjeta();
        this.MontoTransaccion = otroMensaje.getMontoTransaccion();
        this.DatosAdicionales = otroMensaje.getDatosAdicionales();
        this.Moneda = otroMensaje.getMoneda();
        this.TipoProductoConsumido = otroMensaje.getTipoProductoConsumido();
        this.SaldoTarjeta = otroMensaje.getSaldoTarjeta();
        this.SaldoComercio = otroMensaje.getSaldoComercio();
        this.CodAutorizacion = otroMensaje.getCodAutorizacion();
        this.IndMsgNl = otroMensaje.getIndMsgNl();
        this.CantMS = otroMensaje.getCantMS();
        this.CantTX = otroMensaje.getCantTX();
        this.Mensajes = otroMensaje.getMensajes();
        this.TXs = otroMensaje.getTXs();
        this.CorteInicio = otroMensaje.getCorteInicio();
        this.CorteFin = otroMensaje.getCorteFin();
        this.TotalGanancia = otroMensaje.getTotalGanancia();
        this.TotalGananciaMes = otroMensaje.getTotalGananciaMes();
        this.TotalMonto = otroMensaje.getTotalMonto();
        this.TotalMontoMes = otroMensaje.getTotalMontoMes();
        this.TotalOperaciones = otroMensaje.getTotalOperaciones();
        this.TotalOperacionesMes = otroMensaje.getTotalOperacionesMes();
        this.FechaDeposito = otroMensaje.getFechaDeposito();
        this.LugarDeDeposito = otroMensaje.getLugarDeDeposito();
        this.MontoDepositado = otroMensaje.getMontoDepositado();
        this.NroComprobante = otroMensaje.getNroComprobante();
        this.TipoDeposito = otroMensaje.getTipoDeposito();
        this.OrigenCheque = otroMensaje.getOrigenCheque();
        this.NroCheque = otroMensaje.getNroCheque();
        this.NroVendedor = otroMensaje.getNroVendedor();
        this.PINVendedor = otroMensaje.getPINVendedor();
        this.MontoAbono = otroMensaje.getMontoAbono();
        this.Abono = otroMensaje.getAbono();
    }

    public MensajeRecarga(InputStream xmlRespuesta) {
        //fromXML(xmlRespuesta, this);
    }

    public void toXML(StringWriter outputStream) {

        XmlRecargaMaker.writeXML(this, outputStream);
    }


}
