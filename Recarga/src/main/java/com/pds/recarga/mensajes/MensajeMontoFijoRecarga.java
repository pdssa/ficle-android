package com.pds.recarga.mensajes;

/**
 * Created by Hernan on 19/12/13.
 */
public class MensajeMontoFijoRecarga {
    private String MontoFijo;
    private String Display;

    public String getMontoFijo() {
        return MontoFijo;
    }

    public void setMontoFijo(String montoFijo) {
        MontoFijo = montoFijo;
    }

    public String getDisplay() {
        return Display;
    }

    public void setDisplay(String display) {
        Display = display;
    }
}
