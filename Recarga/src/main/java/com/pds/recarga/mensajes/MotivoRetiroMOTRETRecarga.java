package com.pds.recarga.mensajes;

/**
 * Created by Hernan on 04/05/2016.
 */
public class MotivoRetiroMOTRETRecarga {
    private String Codigo;
    private String Desc;

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String codigo) {
        Codigo = codigo;
    }

    public String getDesc() {
        return Desc;
    }

    public void setDesc(String desc) {
        Desc = desc;
    }

    public MotivoRetiroMOTRETRecarga() {
    }
}
