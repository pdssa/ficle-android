package com.pds.recarga.mensajes;

/**
 * Created by Hernan on 09/01/14.
 */
public class MensajeTXRecarga {
    private String MsgLn;
    private String MsgAux;

    public String getMsgLn() {
        return MsgLn;
    }

    public void setMsgLn(String msgLn) {
        MsgLn = msgLn;
    }

    public String getMsgAux() {
        return MsgAux;
    }

    public void setMsgAux(String msgAux) {
        MsgAux = msgAux;
    }

    public MensajeTXRecarga(){

    }
}
