package com.pds.recarga.utils;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by Hernan on 12/08/2014.
 */
public final class PassGenerator {

    private static final String TAG_TECNICO = "ClaveTecnico";
    private static final String TAG_RESET = "ClaveReset";
    private static final String TAG_TECNICO_DEFAULT = "ClaveTecnicoDefault";

    public static final SimpleDateFormat ShortDateFormat = new SimpleDateFormat("ddMMyyyy");

    public static String GenerarClaveTecnico(Date fecha, String nroComercio, String idTerminal) throws Exception {
        String _fecha = ShortDateFormat.format(fecha);

        char[] _nroComercioPad = new char[nroComercio.length() > 8 ? 0 : 8 - nroComercio.length()];
        Arrays.fill(_nroComercioPad, '0');
        String _nroComercio = String.copyValueOf(_nroComercioPad) + nroComercio;
        _nroComercio = _nroComercio.substring(0, 8);

        char[] _idTerminalPad = new char[idTerminal.length() > 8 ? 0 : 8 - idTerminal.length()];
        Arrays.fill(_idTerminalPad, '0');
        String _idTerminal = String.copyValueOf(_idTerminalPad) + idTerminal;
        _idTerminal = _idTerminal.substring(0, 8);

        return GenerarClave(_fecha, _nroComercio, _idTerminal, TAG_TECNICO);
    }

    private static String GenerarClave(String fecha, String nroComercio, String idTerminal, String modificador) throws Exception {
        try {
            //armamos el string a codificar--------------------------------------------------
            String codigoAProcesar = idTerminal + nroComercio + fecha + modificador;

            // Obtener el SHA1 --------------------------------------------------------------
            byte[] buffer = codigoAProcesar.getBytes("US-ASCII");
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
            messageDigest.reset();
            messageDigest.update(buffer);
            byte[] claveSHA1 = messageDigest.digest();//<<--el resultado obtenido acá son bytes con signo

            // pasamos a lista para recortar y reversarla
            List<Byte> claveSHA1List = new ArrayList<Byte>(ToObjectList(claveSHA1));
            claveSHA1List = claveSHA1List.subList(0, 4);
            claveSHA1List = ReverseList(claveSHA1List);

            // Armamos la clave de 10 digitos --
            String claveTempString = ToUIntString(ToPrimitiveArray(claveSHA1List));

            // Devuelvo los ultimos 6 digitos --
            return claveTempString.substring(4, 10);

        } catch (Exception ex) {
            throw ex;
        }
    }

    private static byte[] ToPrimitiveArray(List<Byte> input) {
        byte[] output = new byte[input.size()];

        for (int i = 0; i < input.size(); i++) {
            output[i] = input.get(i);
        }

        return output;
    }

    private static List<Byte> ToObjectList(byte[] input) {
        Byte[] output = new Byte[input.length];

        for (int i = 0; i < input.length; i++) {
            output[i] = new Byte(input[i]);
        }

        return Arrays.asList(output);
    }

    private static ArrayList<Byte> ReverseList(List<Byte> input) {
        ArrayList<Byte> ouput = new ArrayList<Byte>();

        int lastIndex = input.size() - 1;

        //recorremos la lista en sentido inverso
        for (int i = lastIndex; i >= 0; i--) {
            ouput.add(lastIndex - i, input.get(i));
        }

        return ouput;
    }

    private static String ToUIntString(byte[] input) {

        // Acá tomamos los 4 bytes y pasarlos a un int
        int val1 = 0;
        for (int i = 0; i < input.length; i++) {
            val1 = val1 | ((input[i] & 0xff) << i * 8);
        }

        // Ahora lo paso a string anteponemos ceros para completar 10 digitos--
        String output = ("0000000000" + Long.toString(val1 & 0xFFFFFFFFL)); //<<---convertimos el valor signed y lo guardamos en un long
        return output.substring(output.length() - 10);
    }

}
