package com.pds.recarga.utils;

import android.util.Log;
import android.util.Xml;

import com.pds.recarga.mensajes.BancoBNCRecarga;
import com.pds.recarga.mensajes.MensajeMSRecarga;
import com.pds.recarga.mensajes.MensajeMontoFijoRecarga;
import com.pds.recarga.mensajes.MensajeProductoRecarga;
import com.pds.recarga.mensajes.MensajeRecarga;
import com.pds.recarga.mensajes.MensajeTXRecarga;
import com.pds.recarga.mensajes.MotivoRetiroMOTRETRecarga;
import com.pds.recarga.mensajes.MsgsTermMSGPOSRecarga;
import com.pds.recarga.mensajes.ProcessingCode;
import com.pds.recarga.mensajes.PropDepDAVISORecarga;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

/**
 * Created by Hernan on 19/12/13.
 */
public class XmlRecargaParser {
    // sin namespaces
    private static final String ns = null;

    public static final String ENCODING = "UTF-8";


    public MensajeRecarga parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, ENCODING);
            parser.nextTag();
            return readXML(parser);
        } finally {
            in.close();
        }
    }

    public static String extractXML(InputStream in, String xml_path, boolean close_stream) throws IOException{
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            Document doc = dbf.newDocumentBuilder().parse(in);

            XPath xPath = XPathFactory.newInstance().newXPath();
            //"A/B[id = '1']"
            Node result = (Node) xPath.evaluate(xml_path, doc, XPathConstants.NODE);

            return nodeToString(result);
        }
        catch (Exception ex){
            Log.e("ExtractXML", "Error", ex);
        }
        finally {
            if(close_stream) in.close();
        }

        return "";
    }

    private static String nodeToString(Node node) throws Exception
    {
        StringWriter buf = new StringWriter();
        Transformer xform = TransformerFactory.newInstance().newTransformer();
        xform.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        xform.transform(new DOMSource(node), new StreamResult(buf));
        return(buf.toString());
    }

    private MensajeRecarga readXML(XmlPullParser parser) throws XmlPullParserException, IOException {

        MensajeRecarga mensajeRecarga = new MensajeRecarga();

        // Iniciamos buscando el tag de inicio
        parser.require(XmlPullParser.START_TAG, ns, "FID_TM_RES");
        String tag = parser.getName();

        if (tag.equals("FID_TM_RES")) {

            mensajeRecarga.setRoot_TipoMensaje(parser.getAttributeValue(null, "TipoMensaje"));
            mensajeRecarga.setRoot_CodigoProceso(parser.getAttributeValue(null, "CodigoProceso"));
            mensajeRecarga.setRoot_NumeroComercio(parser.getAttributeValue(null, "NumeroComercio"));

            //continuamos la lectura
            parser.nextTag();

            readTagTM(parser, mensajeRecarga);

        }

        return mensajeRecarga;
    }


    private void readTagTM(XmlPullParser parser, MensajeRecarga mensajeRecarga) throws XmlPullParserException, IOException {
        String tagTM = "TM" + mensajeRecarga.getRoot_TipoMensaje() + mensajeRecarga.getRoot_CodigoProceso();

        parser.require(XmlPullParser.START_TAG, ns, tagTM);

        String name = parser.getName();
        if (name.equals(tagTM)) {
            mensajeRecarga.setNroSerieSIM(readValue(parser, "NroSerieSIM"));
            mensajeRecarga.setNroSerieTerminal(readValue(parser, "NroSerieTerminal"));
            mensajeRecarga.setIMSITerminal(readValue(parser, "IMSITerminal"));
            mensajeRecarga.setVapp(readValue(parser, "Vapp"));
            mensajeRecarga.setIMEITerminal(readValue(parser, "IMEITerminal"));
            mensajeRecarga.setNroAuditoriaTerminal(readValue(parser, "NroAuditoriaTerminal"));

            mensajeRecarga.setFechaHoraTerminal(readValue(parser, "FechaHoraTerminal"));
            mensajeRecarga.setModoEntradaTerminal(readValue(parser, "ModoEntradaTerminal"));
            mensajeRecarga.setCondicionTerminal(readValue(parser, "CondicionTerminal"));

            mensajeRecarga.setNroTransaccion(readValue(parser, "NroTransaccion"));
            mensajeRecarga.setCodigoRespuesta(readValue(parser, "CodigoRespuesta"));
            mensajeRecarga.setFechaHora(readValue(parser, "FechaHora"));
            mensajeRecarga.setIdentTerminal(readValue(parser, "IdentTerminal"));
            mensajeRecarga.setRazonSocialCliente(readValue(parser, "RazonSocialCliente"));
            mensajeRecarga.setRucCliente(readValue(parser, "RucCliente"));
            mensajeRecarga.setIDExtCliente(readValue(parser, "IDExtCliente"));
            mensajeRecarga.setRegionSucursal(readValue(parser, "RegionSucursal"));
            mensajeRecarga.setComunaSucursal(readValue(parser, "ComunaSucursal"));
            mensajeRecarga.setDomicilioSucursal(readValue(parser, "DomicilioSucursal"));
            mensajeRecarga.setCantProductos(readValue(parser, "CantProductos"));
            mensajeRecarga.setMensajeLinea1(readValue(parser, "MensajeLinea1"));
            mensajeRecarga.setMensajeLinea2(readValue(parser, "MensajeLinea2"));
            mensajeRecarga.setNroTickets(readValue(parser, "NroTickets"));
            mensajeRecarga.setNroTarjeta(readValue(parser, "NroTarjeta"));
            mensajeRecarga.setMontoTransaccion(readValue(parser, "MontoTransaccion"));
            mensajeRecarga.setMoneda(readValue(parser, "Moneda"));
            mensajeRecarga.setSaldoComercio(readValue(parser, "SaldoComercio"));
            mensajeRecarga.setSaldoTarjeta(readValue(parser, "SaldoTarjeta"));
            mensajeRecarga.setCodAutorizacion(readValue(parser, "CodAutorizacion"));
            mensajeRecarga.setTipoProductoConsumido(readValue(parser, "TipoProductoConsumido"));
            mensajeRecarga.setDatosAdicionales(readValue(parser, "DatosAdicionales"));
            mensajeRecarga.setIndMsgNl(readValue(parser, "IndMsgNl"));
            mensajeRecarga.setCantMS(readValue(parser, "CantMS"));
            mensajeRecarga.setCantTX(readValue(parser, "CantTX"));
            mensajeRecarga.setCorteFin(readValue(parser, "CorteFin"));
            mensajeRecarga.setCorteInicio(readValue(parser, "CorteInicio"));
            mensajeRecarga.setTotalGanancia(readValue(parser, "TotalGanancia"));
            mensajeRecarga.setTotalGananciaMes(readValue(parser, "TotalGananciaMes"));
            mensajeRecarga.setTotalMonto(readValue(parser, "TotalMonto"));
            mensajeRecarga.setTotalMontoMes(readValue(parser, "TotalMontoMes"));
            mensajeRecarga.setTotalOperaciones(readValue(parser, "TotalOperaciones"));
            mensajeRecarga.setTotalOperacionesMes(readValue(parser, "TotalOperacionesMes"));
            mensajeRecarga.setFechaDeposito(readValue(parser, "FechaDeposito"));
            mensajeRecarga.setLugarDeDeposito(readValue(parser, "LugarDeDeposito"));
            mensajeRecarga.setMontoDepositado(readValue(parser, "MontoDepositado"));
            mensajeRecarga.setNroComprobante(readValue(parser, "NroComprobante"));
            mensajeRecarga.setTipoDeposito(readValue(parser, "TipoDeposito"));
            mensajeRecarga.setOrigenCheque(readValue(parser, "OrigenCheque"));
            mensajeRecarga.setNroCheque(readValue(parser, "NroCheque"));
            mensajeRecarga.setNroVendedor(readValue(parser, "NroVendedor"));
            mensajeRecarga.setPINVendedor(readValue(parser, "PINVendedor"));
            mensajeRecarga.setMontoAbono(readValue(parser, "MontoAbono"));
            mensajeRecarga.setAbono(readValue(parser, "Abono"));

            //continuamos la lectura
            parser.nextTag(); //<PR> o <MS> o <TX> o <BANCOS> o <MOTRET> o <MSGPOS> o <DAVISO> o </TM>

            //si es un mensaje de consulta comercial buscamos los productos asociados
            if (mensajeRecarga.getRoot_CodigoProceso().equals(ProcessingCode.CONSULTA_COMERCIAL) ||
                    mensajeRecarga.getRoot_CodigoProceso().equals(ProcessingCode.CONSULTA_TRANSACCION_RECARGA_NORMAL) ||
                    mensajeRecarga.getRoot_CodigoProceso().equals(ProcessingCode.CONSULTA_VENTAS))
                readTagsPR(parser, mensajeRecarga);

            //si es un mensaje de consulta de mensajes buscamos los mensajes asociados
            if (mensajeRecarga.getRoot_CodigoProceso().equals(ProcessingCode.CONSULTA_MENSAJES_LEIDOS))
                readTagsMS(parser, mensajeRecarga);

            //si es un mensaje de consulta de mensajes buscamos las ultimas recargas
            if (mensajeRecarga.getRoot_CodigoProceso().equals(ProcessingCode.CONSULTA_ULTIMAS_RECARGAS) ||
                    mensajeRecarga.getRoot_CodigoProceso().equals(ProcessingCode.CONSULTA_ULTIMOS_ABONOS))
                readTagsTX(parser, mensajeRecarga);

            //si es un mensaje de consulta de datos referenciales, buscamos los bancos, motivos, mensajes y tipos de avisos
            if (mensajeRecarga.getRoot_CodigoProceso().equals(ProcessingCode.CONSULTA_DATOS_REFERENCIALES)) {
                readTagsBANCOS(parser, mensajeRecarga);

                readTagsMOTRET(parser, mensajeRecarga);

                readTagsMSGPOS(parser, mensajeRecarga);

                readTagsDAVISO(parser, mensajeRecarga);
            }

        }

    }

    private void readTagsPR(XmlPullParser parser, MensajeRecarga mensajeRecarga) throws XmlPullParserException, IOException {
        do {

            try {

                parser.require(XmlPullParser.START_TAG, ns, "PR");

                readTagPR(parser, mensajeRecarga);

                //parser.nextTag(); //</PR>

            } catch (Exception ex) {

            }
        } while (parser.next() != XmlPullParser.END_TAG); //<PR> o </TM>
    }

    private void readTagPR(XmlPullParser parser, MensajeRecarga mensajeRecarga) throws XmlPullParserException, IOException {


        String name = parser.getName();
        if (name.equals("PR")) {
            MensajeProductoRecarga productoRecarga = new MensajeProductoRecarga();

            productoRecarga.setCodigo(readValue(parser, "Codigo"));
            productoRecarga.setDescripcion(readValue(parser, "Descripcion"));
            productoRecarga.setSaldoProducto(readValue(parser, "SaldoProducto"));
            productoRecarga.setLimiteCreditoProducto(readValue(parser, "LimiteCreditoProducto"));
            productoRecarga.setPrefijo(readValue(parser, "Prefijo"));
            productoRecarga.setMinDigitos(readValue(parser, "MinDigitos"));
            productoRecarga.setMaxDigitos(readValue(parser, "MaxDigitos"));
            productoRecarga.setDescIngreso(readValue(parser, "DescIngreso"));
            productoRecarga.setTipoDeCarga(readValue(parser, "TipoDeCarga"));
            productoRecarga.setIndRequiereNroClave(readValue(parser, "IndRequiereNroClave"));
            productoRecarga.setAdmSubProd(readValue(parser, "AdmSubPro"));
            productoRecarga.setProdSup(readValue(parser, "ProdSup"));
            productoRecarga.setCantMontosFijos(readValue(parser, "CantMontosFijos"));
            productoRecarga.setCantidadOperaciones(readValue(parser, "CantidadOperaciones"));
            productoRecarga.setMontoOperaciones(readValue(parser, "MontoOperaciones"));
            productoRecarga.setMontoGanancia(readValue(parser, "MontoGanancia"));

            if (readValue(parser, "PieTicket") != null)
                productoRecarga.setPieTicket(readValue(parser, "PieTicket"));
            else
                productoRecarga.setPieTicket("");


            //continuamos la lectura
            parser.nextTag();//</PR> o <MF>

            try {
                readTagMF(parser, productoRecarga);
            } catch (Exception ex) {

            }

            mensajeRecarga.getProductos().add(productoRecarga);
        }


    }

    private void readTagMF(XmlPullParser parser, MensajeProductoRecarga productoRecarga) throws XmlPullParserException, IOException {

        do {
            parser.require(XmlPullParser.START_TAG, ns, "MF");

            String name = parser.getName();
            if (name.equals("MF")) {
                MensajeMontoFijoRecarga montoFijoRecarga = new MensajeMontoFijoRecarga();

                montoFijoRecarga.setMontoFijo(readValue(parser, "MontoFijo"));
                montoFijoRecarga.setDisplay(readValue(parser, "Display"));

                productoRecarga.getMontosFijos().add(montoFijoRecarga);
            }

            parser.next();//</MF>

        }
        while (parser.next() != XmlPullParser.END_TAG);//puede ser <MF> o </PR>, mientras sea un tag de inicio sigue

    }

    private void readTagsMS(XmlPullParser parser, MensajeRecarga mensajeRecarga) throws XmlPullParserException, IOException {

        do {
            try {

                parser.require(XmlPullParser.START_TAG, ns, "MS");

                readTagMS(parser, mensajeRecarga);

                parser.nextTag(); //</MS>

            } catch (Exception ex) {

            }
        } while (parser.next() != XmlPullParser.END_TAG); //<MS> o </TM>


    }

    private void readTagMS(XmlPullParser parser, MensajeRecarga mensajeRecarga) throws XmlPullParserException, IOException {

        String name = parser.getName();
        if (name.equals("MS")) {
            MensajeMSRecarga mensajeMSRecarga = new MensajeMSRecarga();

            mensajeMSRecarga.setMsgID(readValue(parser, "MsgID"));
            mensajeMSRecarga.setFecha(readValue(parser, "Fecha"));
            mensajeMSRecarga.setDe(readValue(parser, "De"));
            mensajeMSRecarga.setTexto(readValue(parser, "Texto"));

            mensajeRecarga.getMensajes().add(mensajeMSRecarga);
        }

    }

    private void readTagsTX(XmlPullParser parser, MensajeRecarga mensajeRecarga) throws XmlPullParserException, IOException {

        do {

            try {

                parser.require(XmlPullParser.START_TAG, ns, "TX");

                readTagTX(parser, mensajeRecarga);

                parser.nextTag(); //</TX>

            } catch (Exception ex) {

            }
        } while (parser.next() != XmlPullParser.END_TAG); //<TX> o </TM>

    }

    private void readTagTX(XmlPullParser parser, MensajeRecarga mensajeRecarga) throws XmlPullParserException, IOException {

        String name = parser.getName();
        if (name.equals("TX")) {
            MensajeTXRecarga mensajeTXRecarga = new MensajeTXRecarga();

            mensajeTXRecarga.setMsgLn(readValue(parser, "MsgLn"));
            mensajeTXRecarga.setMsgAux(readValue(parser, "MsgAux"));

            mensajeRecarga.getTXs().add(mensajeTXRecarga);
        }
    }

    private void readTagsBANCOS(XmlPullParser parser, MensajeRecarga mensajeRecarga) throws XmlPullParserException, IOException {
        try {

            parser.require(XmlPullParser.START_TAG, ns, "BANCOS");

            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.getEventType() != XmlPullParser.START_TAG) {
                    continue;
                }

                String name = parser.getName();
                if (name.equals("BNC")) {
                    readTagBNC(parser, mensajeRecarga);
                }
            }

            parser.next();

        } catch (Exception ex) {

        }
    }

    private void readTagBNC(XmlPullParser parser, MensajeRecarga mensajeRecarga) throws XmlPullParserException, IOException {

        parser.require(XmlPullParser.START_TAG, ns, "BNC");

        BancoBNCRecarga bancoBNCRecarga = new BancoBNCRecarga();

        bancoBNCRecarga.setCodigo(readValue(parser, "Codigo"));
        bancoBNCRecarga.setDesc(readValue(parser, "Desc"));
        bancoBNCRecarga.setDepCli(readValue(parser, "DepCli"));
        bancoBNCRecarga.setDepVen(readValue(parser, "DepVen"));
        bancoBNCRecarga.setEmiCheque(readValue(parser, "EmiCheque"));

        mensajeRecarga.getBancos().add(bancoBNCRecarga);

        parser.nextTag();

        parser.require(XmlPullParser.END_TAG, ns, "BNC");
    }

    private void readTagsMOTRET(XmlPullParser parser, MensajeRecarga mensajeRecarga) throws XmlPullParserException, IOException {
        try {

            parser.require(XmlPullParser.START_TAG, ns, "MOTRET");

            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.getEventType() != XmlPullParser.START_TAG) {
                    continue;
                }

                String name = parser.getName();
                if (name.equals("MOT")) {
                    readTagMOT(parser, mensajeRecarga);
                }
            }

            parser.next();

        } catch (Exception ex) {

        }
    }

    private void readTagMOT(XmlPullParser parser, MensajeRecarga mensajeRecarga) throws XmlPullParserException, IOException {

        parser.require(XmlPullParser.START_TAG, ns, "MOT");

        MotivoRetiroMOTRETRecarga motivoRetiroMOTRETRecarga = new MotivoRetiroMOTRETRecarga();

        motivoRetiroMOTRETRecarga.setCodigo(readValue(parser, "Codigo"));
        motivoRetiroMOTRETRecarga.setDesc(readValue(parser, "Desc"));

        mensajeRecarga.getMotivosRetiro().add(motivoRetiroMOTRETRecarga);

        parser.nextTag();

        parser.require(XmlPullParser.END_TAG, ns, "MOT");
    }

    private void readTagsMSGPOS(XmlPullParser parser, MensajeRecarga mensajeRecarga) throws XmlPullParserException, IOException {

        try {

            parser.require(XmlPullParser.START_TAG, ns, "MSGPOS");

            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.getEventType() != XmlPullParser.START_TAG) {
                    continue;
                }

                String name = parser.getName();
                if (name.equals("MSG")) {
                    readTagMSG(parser, mensajeRecarga);
                }
            }

            parser.next();

        } catch (Exception ex) {

        }


    }

    private void readTagMSG(XmlPullParser parser, MensajeRecarga mensajeRecarga) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "MSG");

        MsgsTermMSGPOSRecarga msgsTermMSGPOSRecarga = new MsgsTermMSGPOSRecarga();

        msgsTermMSGPOSRecarga.setCodigo(readValue(parser, "Codigo"));
        msgsTermMSGPOSRecarga.setDesc(readValue(parser, "Desc"));

        mensajeRecarga.getMensajesTerminal().add(msgsTermMSGPOSRecarga);

        parser.nextTag();

        parser.require(XmlPullParser.END_TAG, ns, "MSG");

    }

    private void readTagsDAVISO(XmlPullParser parser, MensajeRecarga mensajeRecarga) throws XmlPullParserException, IOException {

        try {

            parser.require(XmlPullParser.START_TAG, ns, "DAVISO");

            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.getEventType() != XmlPullParser.START_TAG) {
                    continue;
                }

                String name = parser.getName();
                if (name.equals("DAV")) {
                    readTagDAV(parser, mensajeRecarga);
                }
            }

            parser.next();

        } catch (Exception ex) {

        }
    }

    private void readTagDAV(XmlPullParser parser, MensajeRecarga mensajeRecarga) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "DAV");

        PropDepDAVISORecarga propDepDAVISORecarga = new PropDepDAVISORecarga();

        propDepDAVISORecarga.setCodigo(readValue(parser, "Codigo"));
        propDepDAVISORecarga.setDesc(readValue(parser, "Desc"));

        mensajeRecarga.getPropositoDeposito().add(propDepDAVISORecarga);

        parser.nextTag();

        parser.require(XmlPullParser.END_TAG, ns, "DAV");
    }

    // Processes link tags in the feed.
    private String readValue(XmlPullParser parser, String attributeName) throws IOException, XmlPullParserException {

        String result = "";
        result = parser.getAttributeValue(ns, attributeName);
        return result;
    }

    private void saltar(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }

    public static List<MsgsTermMSGPOSRecarga> parseMensajesTerminal(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, ENCODING);
            parser.nextTag();

            MensajeRecarga msj = new MensajeRecarga();

            new XmlRecargaParser().readTagsMSGPOS(parser,msj);

            return msj.getMensajesTerminal();

        } finally {
            in.close();
        }
    }

    public static List<BancoBNCRecarga> parseBancos(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, ENCODING);
            parser.nextTag();

            MensajeRecarga msj = new MensajeRecarga();

            new XmlRecargaParser().readTagsBANCOS(parser, msj);

            return msj.getBancos();

        } finally {
            in.close();
        }
    }

    public static List<PropDepDAVISORecarga> parsePropositosDeposito(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, ENCODING);
            parser.nextTag();

            MensajeRecarga msj = new MensajeRecarga();

            new XmlRecargaParser().readTagsDAVISO(parser, msj);

            return msj.getPropositoDeposito();

        } finally {
            in.close();
        }
    }
}
