package com.pds.recarga.utils;

import com.pds.common.Formato;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Hernan on 12/05/2016.
 */
public abstract class Utils {
    public static boolean esMismoDia(Date fechaA, Date fechaB) {

        //pasamos las fechas al mismo formato de string para comparar
        String strFechaA = Formato.FormateaDate(fechaA, Formato.ShortDateFormat);
        String strFechaB = Formato.FormateaDate(fechaB, Formato.ShortDateFormat);

        return strFechaA.equals(strFechaB);
    }


    public static Date Util_Fecha(String fechaFormat) {
        String result = "";
        try {
            Date fecha = new SimpleDateFormat("yyyy-MM-dd").parse(fechaFormat);
            return fecha;
        } catch (Exception ex) {
            result = "";
        }
        return null;
    }

    public static Date Util_FechaHora(String fechaHoraTformat) {
        String result = "";
        try {
            Date fechaHora = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(fechaHoraTformat);
            return fechaHora;
        } catch (Exception ex) {
            result = "";
        }
        return null;
    }

    public static String Util_FechaHoraTerminal() {
        return Util_FechaHoraTerminal(new Date());
    }

    public static String Util_FechaHoraTerminal(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(date);
    }

    public static String Util_FechaTerminal() {

        return new SimpleDateFormat("yyyy-MM-dd").format(new Date());

    }

    public static String Util_FechaTerminal(String day, String month) {
        return (new SimpleDateFormat("yyyy").format(new Date())) + "-" + month + "-" + day;
    }
}
