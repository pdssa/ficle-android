package com.pds.recarga.utils;

import android.util.Xml;

import com.pds.recarga.mensajes.MensajeRecarga;
import com.pds.recarga.mensajes.ProcessingCode;

import org.xmlpull.v1.XmlSerializer;

import java.io.StringWriter;

/**
 * Created by Hernan on 19/12/13.
 */
public final class XmlRecargaMaker {

    public static void writeXML(MensajeRecarga mensajeRecarga, StringWriter outputStream) {
        try {
            //File newxmlfile = new File(Environment.getExternalStorageDirectory() + File.separator + "new.xml");
            //newxmlfile.createNewFile();
            //we have to bind the new file with a FileOutputStream
            //FileOutputStream fileos = null;
            //fileos = new FileOutputStream(newxmlfile);

            //String request = "GET / HTTP/1.1\r\n";
            //outputStream.write(request);

            //we create a XmlSerializer in order to write xml data
            XmlSerializer serializer = Xml.newSerializer();

            //we set the PrintWriter as output for the serializer
            serializer.setOutput(outputStream);
            //Write <?xml declaration with encoding (if encoding not null) and standalone flag (if standalone not null)
            //serializer.startDocument("UTF-8", true);
            serializer.startDocument(null, null);
            //seteamos identacion
            //serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);

            //tag ROOT
            serializer.startTag(null, "FID_TM_ROOT");

            //seteamos los atributos de ROOT
            serializer.attribute(null, "TipoMensaje", mensajeRecarga.getRoot_TipoMensaje());
            serializer.attribute(null, "CodigoProceso", mensajeRecarga.getRoot_CodigoProceso());
            serializer.attribute(null, "NumeroComercio", mensajeRecarga.getRoot_NumeroComercio());

            //escribimos el tag del proceso correspondiente
            //MENSAJE DE CONSULTA COMERCIAL
            if (mensajeRecarga.getRoot_CodigoProceso().equals(ProcessingCode.CONSULTA_COMERCIAL)) {
                writeXML_301005(mensajeRecarga, serializer);
            }

            //MENSAJE DE RECARGA/CONSULTA NORMAL
            if (    mensajeRecarga.getRoot_CodigoProceso().equals(ProcessingCode.RECARGA_NORMAL) ||
                    mensajeRecarga.getRoot_CodigoProceso().equals(ProcessingCode.CONSULTA_TRANSACCION_RECARGA_NORMAL)) {
                writeXML_201105(mensajeRecarga, serializer);
            }

            //MENSAJE DE CONSULTA MENSAJES
            if (mensajeRecarga.getRoot_CodigoProceso().equals(ProcessingCode.CONSULTA_MENSAJES_LEIDOS)) {
                writeXML_300105(mensajeRecarga, serializer);
            }

            //MENSAJE DE CONSULTA ULTIMA RECARGA
            if (mensajeRecarga.getRoot_CodigoProceso().equals(ProcessingCode.CONSULTA_ULTIMAS_RECARGAS)) {
                writeXML_303305(mensajeRecarga, serializer);
            }

            //MENSAJE DE CONSULTA ULTIMO ABONO
            if (mensajeRecarga.getRoot_CodigoProceso().equals(ProcessingCode.CONSULTA_ULTIMOS_ABONOS)) {
                writeXML_305505(mensajeRecarga, serializer);
            }

            //MENSAJE DE CONSULTA VENTAS
            if (mensajeRecarga.getRoot_CodigoProceso().equals(ProcessingCode.CONSULTA_VENTAS)) {
                writeXML_300905(mensajeRecarga, serializer);
            }

            //MENSAJE DE AVISO DE DEPOSITO CLIENTE
            if (mensajeRecarga.getRoot_CodigoProceso().equals(ProcessingCode.AVISO_DEPOSITO_CLIENTE)) {
                writeXML_206505(mensajeRecarga, serializer);
            }

            //MENSAJE DE AVISO DE DEPOSITO VENDEDOR
            if (mensajeRecarga.getRoot_CodigoProceso().equals(ProcessingCode.AVISO_DEPOSITO_VENDEDOR)) {
                writeXML_206605(mensajeRecarga, serializer);
            }

            //MENSAJE DE REGISTRO DE VISITA DE VENDEDOR
            if (mensajeRecarga.getRoot_CodigoProceso().equals(ProcessingCode.VISITA_VENDEDOR)) {
                writeXML_205605(mensajeRecarga, serializer);
            }

            //MENSAJE DE VENTA DE ABONO A COMERCIOS
            if (mensajeRecarga.getRoot_CodigoProceso().equals(ProcessingCode.VENTA_ABONO_COMERCIO)) {
                writeXML_205505(mensajeRecarga, serializer);
            }

            //MENSAJE DE CONSULTA DE DATOS REFERENCIALES
            if (mensajeRecarga.getRoot_CodigoProceso().equals(ProcessingCode.CONSULTA_DATOS_REFERENCIALES)) {
                writeXML_305105(mensajeRecarga, serializer);
            }

            //MENSAJE DE ENVIO DE MENSAJES DESDE EL TERMINAL
            if (mensajeRecarga.getRoot_CodigoProceso().equals(ProcessingCode.MENSAJES_TERMINAL)) {
                writeXML_206205(mensajeRecarga, serializer);
            }


            serializer.endTag(null, "FID_TM_ROOT");

            serializer.endDocument();

            //escribimos el xml dentro del OutputStream
            serializer.flush();

            //finally we close the file stream
            outputStream.flush();
            //outputStream.close();

            //return fileos;

        } catch (Exception ex) {
            String error = ex.getMessage().toString();
        }
    }

    public static void writeXML_301005(MensajeRecarga mensajeRecarga, XmlSerializer serializer) throws Exception {

        try {
            //generamos el tagTM
            String tagTM = "TM" + mensajeRecarga.getRoot_TipoMensaje() + mensajeRecarga.getRoot_CodigoProceso();

            serializer.startTag(null, tagTM);

            //campos mandatorios:
            serializer.attribute(null, "NroAuditoriaTerminal", mensajeRecarga.getNroAuditoriaTerminal());
            serializer.attribute(null, "FechaHoraTerminal", mensajeRecarga.getFechaHoraTerminal());
            serializer.attribute(null, "ModoEntradaTerminal", mensajeRecarga.getModoEntradaTerminal());
            serializer.attribute(null, "CondicionTerminal", mensajeRecarga.getCondicionTerminal());
            serializer.attribute(null, "IdentTerminal", mensajeRecarga.getIdentTerminal());
            serializer.attribute(null, "NroTickets", mensajeRecarga.getNroTickets());

            //campos opcionales:
            serializer.attribute(null, "IMEITerminal", mensajeRecarga.getIMEITerminal());
            serializer.attribute(null, "Vapp", mensajeRecarga.getVapp());

            if(!mensajeRecarga.getIMSITerminal().equals(""))//solo lo enviamos si esta presente
                serializer.attribute(null, "IMSITerminal", mensajeRecarga.getIMSITerminal());

            if(!mensajeRecarga.getNroSerieTerminal().equals(""))//solo lo enviamos si esta presente
                serializer.attribute(null, "NroSerieTerminal", mensajeRecarga.getNroSerieTerminal());

            if(!mensajeRecarga.getNroSerieSIM().equals(""))//solo lo enviamos si esta presente
                serializer.attribute(null, "NroSerieSIM", mensajeRecarga.getNroSerieSIM());

            serializer.endTag(null, tagTM);

        } catch (Exception ex) {
            throw ex;
        }
    }

    public static void writeXML_201105(MensajeRecarga mensajeRecarga, XmlSerializer serializer) throws Exception {
        try {
            //generamos el tagTM
            String tagTM = "TM" + mensajeRecarga.getRoot_TipoMensaje() + mensajeRecarga.getRoot_CodigoProceso();

            serializer.startTag(null, tagTM);

            //campos mandatorios:
            serializer.attribute(null, "NroTarjeta", mensajeRecarga.getNroTarjeta());
            serializer.attribute(null, "MontoTransaccion", mensajeRecarga.getMontoTransaccion());
            serializer.attribute(null, "NroAuditoriaTerminal", mensajeRecarga.getNroAuditoriaTerminal());
            serializer.attribute(null, "FechaHoraTerminal", mensajeRecarga.getFechaHoraTerminal());
            serializer.attribute(null, "ModoEntradaTerminal", mensajeRecarga.getModoEntradaTerminal());
            serializer.attribute(null, "CondicionTerminal", mensajeRecarga.getCondicionTerminal());
            serializer.attribute(null, "IdentTerminal", mensajeRecarga.getIdentTerminal());
            serializer.attribute(null, "DatosAdicionales", mensajeRecarga.getDatosAdicionales());
            serializer.attribute(null, "Moneda", mensajeRecarga.getMoneda());
            serializer.attribute(null, "NroTickets", mensajeRecarga.getNroTickets());
            serializer.attribute(null, "TipoProductoConsumido", mensajeRecarga.getTipoProductoConsumido());

            //campos opcionales:
            serializer.attribute(null, "IMEITerminal", mensajeRecarga.getIMEITerminal());
            serializer.attribute(null, "Vapp", mensajeRecarga.getVapp());

            serializer.endTag(null, tagTM);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public static void writeXML_300105(MensajeRecarga mensajeRecarga, XmlSerializer serializer) throws Exception {
        try {
            //generamos el tagTM
            String tagTM = "TM" + mensajeRecarga.getRoot_TipoMensaje() + mensajeRecarga.getRoot_CodigoProceso();

            serializer.startTag(null, tagTM);

            //campos mandatorios:
            serializer.attribute(null, "NroAuditoriaTerminal", mensajeRecarga.getNroAuditoriaTerminal());
            serializer.attribute(null, "FechaHoraTerminal", mensajeRecarga.getFechaHoraTerminal());
            serializer.attribute(null, "ModoEntradaTerminal", mensajeRecarga.getModoEntradaTerminal());
            serializer.attribute(null, "CondicionTerminal", mensajeRecarga.getCondicionTerminal());
            serializer.attribute(null, "IdentTerminal", mensajeRecarga.getIdentTerminal());
            serializer.attribute(null, "NroTickets", mensajeRecarga.getNroTickets());

            //campos opcionales:
            serializer.attribute(null, "IMEITerminal", mensajeRecarga.getIMEITerminal());
            serializer.attribute(null, "Vapp", mensajeRecarga.getVapp());

            serializer.endTag(null, tagTM);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public static void writeXML_303305(MensajeRecarga mensajeRecarga, XmlSerializer serializer) throws Exception {
        try {
            //generamos el tagTM
            String tagTM = "TM" + mensajeRecarga.getRoot_TipoMensaje() + mensajeRecarga.getRoot_CodigoProceso();

            serializer.startTag(null, tagTM);

            //campos mandatorios:
            serializer.attribute(null, "NroAuditoriaTerminal", mensajeRecarga.getNroAuditoriaTerminal());
            serializer.attribute(null, "FechaHoraTerminal", mensajeRecarga.getFechaHoraTerminal());
            serializer.attribute(null, "ModoEntradaTerminal", mensajeRecarga.getModoEntradaTerminal());
            serializer.attribute(null, "CondicionTerminal", mensajeRecarga.getCondicionTerminal());
            serializer.attribute(null, "IdentTerminal", mensajeRecarga.getIdentTerminal());
            serializer.attribute(null, "NroTickets", mensajeRecarga.getNroTickets());

            //campos opcionales:
            serializer.attribute(null, "IMEITerminal", mensajeRecarga.getIMEITerminal());
            serializer.attribute(null, "Vapp", mensajeRecarga.getVapp());

            serializer.endTag(null, tagTM);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public static void writeXML_305505(MensajeRecarga mensajeRecarga, XmlSerializer serializer) throws Exception {
        try {
            //generamos el tagTM
            String tagTM = "TM" + mensajeRecarga.getRoot_TipoMensaje() + mensajeRecarga.getRoot_CodigoProceso();

            serializer.startTag(null, tagTM);

            //campos mandatorios:
            serializer.attribute(null, "NroAuditoriaTerminal", mensajeRecarga.getNroAuditoriaTerminal());
            serializer.attribute(null, "FechaHoraTerminal", mensajeRecarga.getFechaHoraTerminal());
            serializer.attribute(null, "ModoEntradaTerminal", mensajeRecarga.getModoEntradaTerminal());
            serializer.attribute(null, "CondicionTerminal", mensajeRecarga.getCondicionTerminal());
            serializer.attribute(null, "IdentTerminal", mensajeRecarga.getIdentTerminal());
            serializer.attribute(null, "NroTickets", mensajeRecarga.getNroTickets());

            //campos opcionales:
            serializer.attribute(null, "IMEITerminal", mensajeRecarga.getIMEITerminal());
            serializer.attribute(null, "Vapp", mensajeRecarga.getVapp());

            serializer.endTag(null, tagTM);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public static void writeXML_300905(MensajeRecarga mensajeRecarga, XmlSerializer serializer) throws Exception {
        try {
            //generamos el tagTM
            String tagTM = "TM" + mensajeRecarga.getRoot_TipoMensaje() + mensajeRecarga.getRoot_CodigoProceso();

            serializer.startTag(null, tagTM);

            //campos mandatorios:
            serializer.attribute(null, "NroAuditoriaTerminal", mensajeRecarga.getNroAuditoriaTerminal());
            serializer.attribute(null, "FechaHoraTerminal", mensajeRecarga.getFechaHoraTerminal());
            serializer.attribute(null, "ModoEntradaTerminal", mensajeRecarga.getModoEntradaTerminal());
            serializer.attribute(null, "CondicionTerminal", mensajeRecarga.getCondicionTerminal());
            serializer.attribute(null, "IdentTerminal", mensajeRecarga.getIdentTerminal());
            serializer.attribute(null, "NroTickets", mensajeRecarga.getNroTickets());

            //campos opcionales:
            serializer.attribute(null, "IMEITerminal", mensajeRecarga.getIMEITerminal());
            serializer.attribute(null, "Vapp", mensajeRecarga.getVapp());

            if (mensajeRecarga.getCorteInicio() != null)
                serializer.attribute(null, "CorteInicio", mensajeRecarga.getCorteInicio());
            if (mensajeRecarga.getCorteFin() != null)
                serializer.attribute(null, "CorteFin", mensajeRecarga.getCorteFin());

            serializer.endTag(null, tagTM);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public static void writeXML_206505(MensajeRecarga mensajeRecarga, XmlSerializer serializer) throws Exception {
        try {
            //generamos el tagTM
            String tagTM = "TM" + mensajeRecarga.getRoot_TipoMensaje() + mensajeRecarga.getRoot_CodigoProceso();

            serializer.startTag(null, tagTM);

            //campos mandatorios:
            serializer.attribute(null, "NroAuditoriaTerminal", mensajeRecarga.getNroAuditoriaTerminal());
            serializer.attribute(null, "FechaHoraTerminal", mensajeRecarga.getFechaHoraTerminal());
            serializer.attribute(null, "ModoEntradaTerminal", mensajeRecarga.getModoEntradaTerminal());
            serializer.attribute(null, "CondicionTerminal", mensajeRecarga.getCondicionTerminal());
            serializer.attribute(null, "IdentTerminal", mensajeRecarga.getIdentTerminal());
            serializer.attribute(null, "NroTickets", mensajeRecarga.getNroTickets());
            serializer.attribute(null, "TipoProductoConsumido", mensajeRecarga.getTipoProductoConsumido());
            serializer.attribute(null, "FechaDeposito", mensajeRecarga.getFechaDeposito());
            serializer.attribute(null, "LugarDeDeposito", mensajeRecarga.getLugarDeDeposito());
            serializer.attribute(null, "MontoDepositado", mensajeRecarga.getMontoDepositado());
            serializer.attribute(null, "NroComprobante", mensajeRecarga.getNroComprobante());
            serializer.attribute(null, "TipoDeposito", mensajeRecarga.getTipoDeposito());

            //campos opcionales:
            serializer.attribute(null, "IMEITerminal", mensajeRecarga.getIMEITerminal());
            serializer.attribute(null, "Vapp", mensajeRecarga.getVapp());

            if (mensajeRecarga.getOrigenCheque() != null)
                serializer.attribute(null, "OrigenCheque", mensajeRecarga.getOrigenCheque());

            if (mensajeRecarga.getNroCheque() != null)
                serializer.attribute(null, "NroCheque", mensajeRecarga.getNroCheque());

            serializer.endTag(null, tagTM);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public static void writeXML_206605(MensajeRecarga mensajeRecarga, XmlSerializer serializer) throws Exception {
        try {
            //generamos el tagTM
            String tagTM = "TM" + mensajeRecarga.getRoot_TipoMensaje() + mensajeRecarga.getRoot_CodigoProceso();

            serializer.startTag(null, tagTM);

            //campos mandatorios:
            serializer.attribute(null, "NroVendedor", mensajeRecarga.getNroVendedor());
            serializer.attribute(null, "PINVendedor", mensajeRecarga.getPINVendedor());
            serializer.attribute(null, "NroAuditoriaTerminal", mensajeRecarga.getNroAuditoriaTerminal());
            serializer.attribute(null, "FechaHoraTerminal", mensajeRecarga.getFechaHoraTerminal());
            serializer.attribute(null, "ModoEntradaTerminal", mensajeRecarga.getModoEntradaTerminal());
            serializer.attribute(null, "CondicionTerminal", mensajeRecarga.getCondicionTerminal());
            serializer.attribute(null, "IdentTerminal", mensajeRecarga.getIdentTerminal());
            serializer.attribute(null, "NroTickets", mensajeRecarga.getNroTickets());
            serializer.attribute(null, "FechaDeposito", mensajeRecarga.getFechaDeposito());
            serializer.attribute(null, "LugarDeDeposito", mensajeRecarga.getLugarDeDeposito());
            serializer.attribute(null, "MontoDepositado", mensajeRecarga.getMontoDepositado());
            serializer.attribute(null, "NroComprobante", mensajeRecarga.getNroComprobante());
            serializer.attribute(null, "TipoDeposito", mensajeRecarga.getTipoDeposito());

            //campos opcionales:
            serializer.attribute(null, "IMEITerminal", mensajeRecarga.getIMEITerminal());
            serializer.attribute(null, "Vapp", mensajeRecarga.getVapp());

            if (mensajeRecarga.getOrigenCheque() != null)
                serializer.attribute(null, "OrigenCheque", mensajeRecarga.getOrigenCheque());

            if (mensajeRecarga.getNroCheque() != null)
                serializer.attribute(null, "NroCheque", mensajeRecarga.getNroCheque());

            serializer.endTag(null, tagTM);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public static void writeXML_205605(MensajeRecarga mensajeRecarga, XmlSerializer serializer) throws Exception {
        try {
            //generamos el tagTM
            String tagTM = "TM" + mensajeRecarga.getRoot_TipoMensaje() + mensajeRecarga.getRoot_CodigoProceso();

            serializer.startTag(null, tagTM);

            //campos mandatorios:
            serializer.attribute(null, "NroVendedor", mensajeRecarga.getNroVendedor());
            serializer.attribute(null, "PINVendedor", mensajeRecarga.getPINVendedor());

            serializer.attribute(null, "NroAuditoriaTerminal", mensajeRecarga.getNroAuditoriaTerminal());
            serializer.attribute(null, "FechaHoraTerminal", mensajeRecarga.getFechaHoraTerminal());
            serializer.attribute(null, "ModoEntradaTerminal", mensajeRecarga.getModoEntradaTerminal());
            serializer.attribute(null, "CondicionTerminal", mensajeRecarga.getCondicionTerminal());
            serializer.attribute(null, "IdentTerminal", mensajeRecarga.getIdentTerminal());
            serializer.attribute(null, "NroTickets", mensajeRecarga.getNroTickets());

            //campos opcionales:
            serializer.attribute(null, "IMEITerminal", mensajeRecarga.getIMEITerminal());
            serializer.attribute(null, "Vapp", mensajeRecarga.getVapp());

            serializer.endTag(null, tagTM);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public static void writeXML_205505(MensajeRecarga mensajeRecarga, XmlSerializer serializer) throws Exception {
        try {
            //generamos el tagTM
            String tagTM = "TM" + mensajeRecarga.getRoot_TipoMensaje() + mensajeRecarga.getRoot_CodigoProceso();

            serializer.startTag(null, tagTM);

            //campos mandatorios:
            serializer.attribute(null, "NroVendedor", mensajeRecarga.getNroVendedor());
            serializer.attribute(null, "PINVendedor", mensajeRecarga.getPINVendedor());
            serializer.attribute(null, "MontoAbono", mensajeRecarga.getMontoAbono());

            serializer.attribute(null, "NroAuditoriaTerminal", mensajeRecarga.getNroAuditoriaTerminal());
            serializer.attribute(null, "FechaHoraTerminal", mensajeRecarga.getFechaHoraTerminal());
            serializer.attribute(null, "ModoEntradaTerminal", mensajeRecarga.getModoEntradaTerminal());
            serializer.attribute(null, "CondicionTerminal", mensajeRecarga.getCondicionTerminal());
            serializer.attribute(null, "IdentTerminal", mensajeRecarga.getIdentTerminal());
            serializer.attribute(null, "NroTickets", mensajeRecarga.getNroTickets());
            serializer.attribute(null, "Moneda", mensajeRecarga.getMoneda());
            serializer.attribute(null, "DatosAdicionales", mensajeRecarga.getDatosAdicionales());
            serializer.attribute(null, "TipoProductoConsumido", mensajeRecarga.getTipoProductoConsumido());

            //campos opcionales:
            serializer.attribute(null, "IMEITerminal", mensajeRecarga.getIMEITerminal());
            serializer.attribute(null, "Vapp", mensajeRecarga.getVapp());

            serializer.endTag(null, tagTM);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public static void writeXML_305105(MensajeRecarga mensajeRecarga, XmlSerializer serializer) throws Exception {
        try {
            //generamos el tagTM
            String tagTM = "TM" + mensajeRecarga.getRoot_TipoMensaje() + mensajeRecarga.getRoot_CodigoProceso();

            serializer.startTag(null, tagTM);

            //campos mandatorios:
            serializer.attribute(null, "NroAuditoriaTerminal", mensajeRecarga.getNroAuditoriaTerminal());
            serializer.attribute(null, "FechaHoraTerminal", mensajeRecarga.getFechaHoraTerminal());
            serializer.attribute(null, "ModoEntradaTerminal", mensajeRecarga.getModoEntradaTerminal());
            serializer.attribute(null, "CondicionTerminal", mensajeRecarga.getCondicionTerminal());
            serializer.attribute(null, "IdentTerminal", mensajeRecarga.getIdentTerminal());

            //campos opcionales:
            serializer.attribute(null, "IMEITerminal", mensajeRecarga.getIMEITerminal());
            serializer.attribute(null, "Vapp", mensajeRecarga.getVapp());

            serializer.endTag(null, tagTM);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public static void writeXML_206205(MensajeRecarga mensajeRecarga, XmlSerializer serializer) throws Exception {
        try {
            //generamos el tagTM
            String tagTM = "TM" + mensajeRecarga.getRoot_TipoMensaje() + mensajeRecarga.getRoot_CodigoProceso();

            serializer.startTag(null, tagTM);

            //campos mandatorios:
            serializer.attribute(null, "NroAuditoriaTerminal", mensajeRecarga.getNroAuditoriaTerminal());
            serializer.attribute(null, "FechaHoraTerminal", mensajeRecarga.getFechaHoraTerminal());
            serializer.attribute(null, "ModoEntradaTerminal", mensajeRecarga.getModoEntradaTerminal());
            serializer.attribute(null, "CondicionTerminal", mensajeRecarga.getCondicionTerminal());
            serializer.attribute(null, "IdentTerminal", mensajeRecarga.getIdentTerminal());
            serializer.attribute(null, "NroTickets", mensajeRecarga.getNroTickets());
            serializer.attribute(null, "CodigoMensaje", mensajeRecarga.getCodigoMensaje());

            //campos opcionales:
            serializer.attribute(null, "IMEITerminal", mensajeRecarga.getIMEITerminal());
            serializer.attribute(null, "Vapp", mensajeRecarga.getVapp());

            serializer.endTag(null, tagTM);
        } catch (Exception ex) {
            throw ex;
        }
    }
}
