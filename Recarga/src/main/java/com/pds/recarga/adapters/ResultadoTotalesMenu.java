package com.pds.recarga.adapters;

/**
 * Created by Hernan on 03/05/2016.
 */
public class ResultadoTotalesMenu {
    private String _producto;
    private String _codProducto;
    private String _recargas;
    private String _total;
    private String _ganancia;
    private String _saldo;
    private String _limite;

    public String get_producto() {
        return _producto;
    }

    public void set_producto(String _producto) {
        this._producto = _producto;
    }

    public String get_recargas() {
        return _recargas;
    }

    public void set_recargas(String _recargas) {
        this._recargas = _recargas;
    }

    public String get_total() {
        return _total;
    }

    public void set_total(String _total) {
        this._total = _total;
    }

    public String get_ganancia() {
        return _ganancia;
    }

    public void set_ganancia(String _ganancia) {
        this._ganancia = _ganancia;
    }

    public String get_saldo() {
        return _saldo;
    }

    public void set_saldo(String _saldo) {
        this._saldo = _saldo;
    }

    public String get_limite() {
        return _limite;
    }

    public void set_limite(String _limite) {
        this._limite = _limite;
    }

    public String get_codProducto() {
        return _codProducto;
    }

    public void set_codProducto(String _codProducto) {
        this._codProducto = _codProducto;
    }

    public ResultadoTotalesMenu(String _producto, String _recargas, String _total, String _ganancia, String _saldo, String _limite, String _codProducto) {
        this._producto = _producto;
        this._recargas = _recargas;
        this._total = _total;
        this._ganancia = _ganancia;
        this._saldo = _saldo;
        this._limite = _limite;
        this._codProducto = _codProducto;
    }
}
