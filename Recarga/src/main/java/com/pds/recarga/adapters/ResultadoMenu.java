package com.pds.recarga.adapters;

/**
 * Created by Hernan on 03/05/2016.
 */
public class ResultadoMenu {
    private String _descripcion;
    private String _subdescripcion;
    private Object _item;
    private int _idImagen;

    public String get_descripcion() {
        return _descripcion;
    }

    public void set_descripcion(String _descripcion) {
        this._descripcion = _descripcion;
    }

    public Object get_item() {
        return _item;
    }

    public void set_item(Object _item) {
        this._item = _item;
    }

    public String get_subdescripcion() {
        return _subdescripcion;
    }

    public void set_subdescripcion(String _subdescripcion) {
        this._subdescripcion = _subdescripcion;
    }

    public int get_idImagen() {
        return _idImagen;
    }

    public void set_idImagen(int _idImagen) {
        this._idImagen = _idImagen;
    }

    public ResultadoMenu(String descripcion, String subdescripcion, Object item) {
        this._descripcion = descripcion;
        this._subdescripcion = subdescripcion;
        this._item = item;
    }

    public ResultadoMenu(String descripcion, String subdescripcion, Object item, int idImagen) {
        this._descripcion = descripcion;
        this._subdescripcion = subdescripcion;
        this._item = item;
        this._idImagen = idImagen;
    }
}
