package com.pds.recarga.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pds.recarga.R;

import java.util.List;

/**
 * Created by Hernan on 03/05/2016.
 */
public class ResultadosCierreTurnoMenuAdapter extends ArrayAdapter<ResultadoTotalesMenu> {

    private Context context;
    List<ResultadoTotalesMenu> datos;

    public ResultadosCierreTurnoMenuAdapter(Context context, List<ResultadoTotalesMenu> datos) {
        super(context, android.R.layout.simple_list_item_1, datos);

        this.context = context;
        this.datos = datos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // En primer lugar "inflamos" una nueva vista, que será la que se
        // mostrará en la celda del ListView. Para ello primero creamos el
        // inflater, y después inflamos la vista.
        LayoutInflater inflater = LayoutInflater.from(context);
        //View item = inflater.inflate(android.R.layout.simple_list_item_1, null);
        View item = inflater.inflate(R.layout.recarga_totales_lineas_item, null);

        ResultadoTotalesMenu row = datos.get(position);

        // A partir de la vista, recogeremos los controles que contiene para
        // poder manipularlos.
        // Recogemos los TextView para mostrar datos
        ((TextView) item.findViewById(R.id.text1)).setText(row.get_producto());
        ((TextView) item.findViewById(R.id.text2)).setText("Cod. Operadora : " + row.get_codProducto());
        ((TextView) item.findViewById(R.id.text3)).setText("Cant. Recargas : " + row.get_recargas());
        ((TextView) item.findViewById(R.id.text4)).setText("Total Recargas : " + row.get_total());
        ((TextView) item.findViewById(R.id.text5)).setVisibility(View.GONE);

        // Devolvemos la vista para que se muestre en el ListView.
        return item;

    }

}