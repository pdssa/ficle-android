package com.pds.recarga.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pds.recarga.R;
import com.pds.recarga.mensajes.BancoBNCRecarga;

import java.util.List;

/**
 * Created by Hernan on 03/05/2016.
 */
public class ResultadosImgMenuAdapter extends ArrayAdapter<ResultadoMenu> {

    private Context context;
    public List<ResultadoMenu> datos;

    public ResultadosImgMenuAdapter(Context context, List<ResultadoMenu> datos) {
        super(context, android.R.layout.simple_list_item_1, datos);

        this.context = context;
        this.datos = datos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // En primer lugar "inflamos" una nueva vista, que será la que se
        // mostrará en la celda del ListView. Para ello primero creamos el
        // inflater, y después inflamos la vista.
        LayoutInflater inflater = LayoutInflater.from(context);

        View item = inflater.inflate(R.layout.servipag_menu_item, null);

        ResultadoMenu row = datos.get(position);

        // A partir de la vista, recogeremos los controles que contiene para
        // poder manipularlos.
        // Recogemos los TextView para mostrar datos
        ((TextView) item.findViewById(android.R.id.text1)).setText(row.get_descripcion());

        if (row.get_idImagen() != -1)
            ((ImageView) item.findViewById(R.id.image1)).setImageResource(row.get_idImagen());
        else
            ((TextView) item.findViewById(android.R.id.text1)).setVisibility(View.VISIBLE);

        // Devolvemos la vista para que se muestre en el ListView.
        return item;

    }

}