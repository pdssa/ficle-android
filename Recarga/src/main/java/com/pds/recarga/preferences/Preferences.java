package com.pds.recarga.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Hernan on 12/05/2016.
 */
public abstract class Preferences {

    public static final String KEY_LAST_TICKET = "NroTicket";
    public static final String KEY_NEXT_NRO_AUDIT = "NroAuditoria";
    public static final String KEY_ID_CLI_EXT = "IDExtCliente";
    public static final String KEY_CONS_COMERC = "cons_comerc_xml";
    public static final String KEY_CFG_COD_COM = "cod_comercio";
    public static final String KEY_CFG_ID_TERM = "id_terminal";
    public static final String KEY_CFG_IP_SERV = "ip_servidor";
    public static final String KEY_CFG_PORT_SERV = "port_servidor";
    public static final String KEY_LIST_MENSAJES = "mensajes_xml";
    public static final String KEY_LIST_BANCOS = "bancos_xml";
    public static final String KEY_LIST_DEP_AVISO = "depaviso_xml";
    public static final String KEY_PEDIR_PAGO = "pedir_pago";

    public static String getString(Context context, String key) {
        return getString(context, key, "");
    }
    public static String getString(Context context, String key, String defValue) {
        //Obtenemos acceso a las preferencias
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        //devolvemos el valor de la clave buscada
        return settings.getString(key, defValue);
    }

    public static Integer getInteger(Context context, String key) {
        return getInteger(context, key, 0);
    }
    public static Integer getInteger(Context context, String key, Integer defValue) {
        //Obtenemos acceso a las preferencias
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        //devolvemos el valor de la clave buscada
        return settings.getInt(key, defValue);
    }

    public static void setString(Context context, String key, String value) {
        //Obtenemos acceso a las preferencias
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        //actualizamos el valor de la propiedad
        editor.putString(key, value);
        editor.commit();
    }

    public static void setInteger(Context context, String key, Integer value) {
        //Obtenemos acceso a las preferencias
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        //actualizamos el valor de la propiedad
        editor.putInt(key, value);
        editor.commit();
    }

    public static void savePreferences(Context context, Integer lastTicket, Integer proxNroAudit, String IDExtCliente) {
        //Obtenemos acceso a las preferencias
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();

        //guardamos los valores de tickets y auditoria
        editor.putInt(KEY_LAST_TICKET, lastTicket);
        editor.putInt(KEY_NEXT_NRO_AUDIT, proxNroAudit);
        editor.putString(KEY_ID_CLI_EXT, IDExtCliente);
        editor.commit();
    }
}
