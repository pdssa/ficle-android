package com.pds.recarga;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.LayoutParams;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Caja;
import com.pds.common.Caja.TipoMovimientoCaja;
import com.pds.common.Config;
import com.pds.common.Formato;
import com.pds.common.Formatos;
import com.pds.common.ItemMenu;
import com.pds.common.ItemMenuAdapter;
import com.pds.common.Logger;
import com.pds.common.Usuario;
import com.pds.common.Venta;
import com.pds.common.VentaDetalleHelper;
import com.pds.common.VentasHelper;
import com.pds.common.activity.TimerActivity;
import com.pds.common.util.ConnectivityUtils;
import com.pds.recarga.adapters.ListadoMensajesTermAdapter;
import com.pds.recarga.adapters.ResultadoMenu;
import com.pds.recarga.adapters.ResultadoTotalesMenu;
import com.pds.recarga.adapters.ResultadosCierreTurnoMenuAdapter;
import com.pds.recarga.adapters.ResultadosImgMenuAdapter;
import com.pds.recarga.adapters.ResultadosMenuAdapter;
import com.pds.recarga.adapters.ResultadosTotalesMenuAdapter;
import com.pds.recarga.conectividad.WebServiceRequest;
import com.pds.recarga.mensajes.BancoBNCRecarga;
import com.pds.recarga.mensajes.MensajeMSRecarga;
import com.pds.recarga.mensajes.MensajeMontoFijoRecarga;
import com.pds.recarga.mensajes.MensajeProductoRecarga;
import com.pds.recarga.mensajes.MensajeRecarga;
import com.pds.recarga.mensajes.MensajeTXRecarga;
import com.pds.recarga.mensajes.MessageType;
import com.pds.recarga.mensajes.MotivoRetiroMOTRETRecarga;
import com.pds.recarga.mensajes.MsgsTermMSGPOSRecarga;
import com.pds.recarga.mensajes.ProcessingCode;
import com.pds.recarga.mensajes.PropDepDAVISORecarga;
import com.pds.recarga.preferences.Preferences;
import com.pds.recarga.utils.PassGenerator;
import com.pds.recarga.utils.Utils;
import com.pds.recarga.utils.XmlRecargaMaker;
import com.pds.recarga.utils.XmlRecargaParser;

import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android_serialport_api.Printer;
import android_serialport_api.Printer.ALINEACION;
import android_serialport_api.TicketRecargaBuilder;


public class MainActivity extends TimerActivity {

    private Printer _printer;

    // declaro varibles de controles *******************
    private ListView lstMenu;
    private List<ItemMenu> menuRecargaList;
    private ItemMenuAdapter menuAdapter;

    private View _recargaStatusView;
    private TextView _recargaStatusMessageView;
    private View _recargaView;
    private MensajeRecarga lastmensajeRecarga;
    private MensajeRecarga lastRecarga;

    private List<MensajeProductoRecarga> listadoProductos;
    private List<BancoBNCRecarga> listadoBancos;
    private List<MotivoRetiroMOTRETRecarga> listadoMotivosRetiro;
    private List<MsgsTermMSGPOSRecarga> listadoMensajesTerminal;
    private List<PropDepDAVISORecarga> listadoPropositoDeposito;

    private Usuario _user;
    ImageView dummy_view;
    ImageButton btnCalc;

    //controles del panel
    ImageView imgIcono_panel;
    TextView txtWelcome_panel;
    TextView txtComentario_panel;
    TextView txtFuncion_panel;
    TextView txtValor_panel;
    TextView txtPanel;
    TextView txtUser_panel;
    TextView txtHora_panel;
    TextView txtFecha_panel;

    //teclado
    Button btnCancelar;
    Button btnPrecio;
    Button btnEnter;
    ImageButton btnBackspace;
    Button btn0;
    Button btn1;
    Button btn2;
    Button btn3;
    Button btn4;
    Button btn5;
    Button btn6;
    Button btn7;
    Button btn8;
    Button btn9;
    Button btnPunto;

    //controles del popup de medio de pago
    Button btnConfirmar_mp;
    Button btnCancelar_mp;
    TextView txtTotal_mp;
    RadioGroup rbtMedios_mp;

    //control de fechas
    BroadcastReceiver _broadcastReceiver;

    private List<String> lineasLayout;
    private int recargaStep;
    private int avisoDepoStep;
    private int visitaVendStep;
    private int ventaAbonoStep;
    private int cierreTurnoStep;
    private ListView recarga_lstResultados;
    private Button recarga_btnImprimirResultados;
    private Button recarga_btnReintentarRecarga;
    private Button recarga_btnVolver;

    //variables para transacciones
    private String RECARGA_CLAVEVENDEDOR = "5284"; //este valor va en duro
    //private String RECARGA_NROCOMERCIO = "74644";
    //private String RECARGA_IDENTTERMINAL = "999912354";
    private String RECARGA_CONDTERMINAL = "00";
    private String RECARGA_VAPP;
    private String RECARGA_MODOENTRADA = "052";
    private String RECARGA_MONEDA = "152";
    //private String RECARGA_IMEITERMINAL = "60026082";
    //private String RECARGA_IMSITERMINAL = "722341225753153";
    //private String RECARGA_SERIALTERMINAL = "521-542-535";
    //private String RECARGA_SERIALSIM = "89543421212257531538";
    private String RECARGA_NROTICKET;
    private int lastTicket = 0;
    private int proxNroAudit;
    private String IDExtCliente;

    private String RECARGA_NROTEL;
    private MensajeProductoRecarga RECARGA_PRODUCTO_OBJ;
    private String RECARGA_PRODUCTO_DESC;
    private String RECARGA_MONTO;
    private String RECARGA_MEDIOPAGO;

    //variables del aviso de deposito
    private String DEPOSITO_CLI_VEND;
    private String DEPOSITO_BANCO;
    private String DEPOSITO_TIPO;
    private String DEPOSITO_NUMERO;
    private String DEPOSITO_MONTO;
    private String DEPOSITO_CHEQUE;
    private String DEPOSITO_BANCO_CHEQUE;
    private String DEPOSITO_FECHA;
    private String DEPOSITO_FECHA_DISPLAY;
    private String DEPOSITO_NROVENDEDOR;
    private String DEPOSITO_NROPIN;

    //variables del registro de visita del vendedor
    private String REGISTRO_NROVENDEDOR;
    private String REGISTRO_NROPIN;

    //variables del cierre del turno
    private String CIERRE_FECHA_INICIO;
    private String CIERRE_FECHA_CIERRE;

    //variables de la venta de abono a comercio
    private String VENTAABONO_MONTO;
    private String VENTAABONO_TIPOPAGO;
    private String VENTAABONO_NROFACTURA;
    private String VENTAABONO_NROVENDEDOR;
    private String VENTAABONO_NROPIN;
    private String VENTAABONO_SALDOANT;

    boolean flagDatosComercio = false; //flag para activar si se ingreso a submenu de datos comercio y regresar a dicha pantalla
    boolean flagObtieneProductos = false; //flag que se activa cuando se esta obteniendo info de productos habilitados

    //configuracion
    private Config config;

    @Override
    public void onStart() {
        super.onStart();
        _broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().compareTo(Intent.ACTION_TIME_TICK) == 0) {
                    Date now = new Date();
                    ((TextView) findViewById(R.id.panel_txt_fecha)).setText(Formato.FormateaDate(now, Formato.DateFormat));
                    ((TextView) findViewById(R.id.panel_txt_hora)).setText(Formato.FormateaDate(now, Formato.ShortTimeFormat));
                }
            }
        };

        registerReceiver(_broadcastReceiver, new IntentFilter(Intent.ACTION_TIME_TICK));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {

            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);//evitamos el sleep durante la ejecucion de esta aplicacion

            ((TextView) findViewById(R.id.txtVersion)).setText("Version:" + getPackageManager().getPackageInfo(this.getPackageName().toString(), 0).versionName);

            RECARGA_VAPP = getPackageManager().getPackageInfo(this.getPackageName().toString(), 0).versionName + "AN";

            //logueo Usuario *****************************************************
            //obtenemos los parametros pasamos a la actividad
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                //tomamos el id de usuario
                //_id_user_login = extras.getInt("_id_user_login");
                this._user = (Usuario) extras.getSerializable("current_user");
            }

            //no tenemos user logueado...volvemos a EntryPoint
            if (_user == null) {
                //Toast.makeText(this, "ACCESO NO VALIDO: Ingrese a través de Entry Point", Toast.LENGTH_LONG).show();
                //this.finish();
                this._user = new Usuario();
                this._user.setNombre("");
            } //else {

            //********* USER LOGUEADO OK *********

            //asignamos los view
            Init_AsignarViews();

            //asignamos los eventos
            Init_AsignarEventos();

            this.txtUser_panel.append(this._user.getNombre());

            //this.txtWelcome_panel.setText("RECARGA");
            this.txtWelcome_panel.setVisibility(View.INVISIBLE);
            this.imgIcono_panel.setImageResource(R.drawable.recarga_min);
            this.imgIcono_panel.setVisibility(View.VISIBLE);

            this.btnCancelar.setText("CANCELAR");
            //this.btnCancelar.setBackgroundColor(getResources().getColor(R.color.boton_rojo));
            this.btnCancelar.setBackgroundResource(R.drawable.botones_main_izq_red);

            this.btnEnter.setBackgroundResource(R.drawable.botones_main_izq);

            this.btnPrecio.setEnabled(false);
            this.btnPrecio.setBackgroundColor(getResources().getColor(R.color.boton_negro));
            this.btnPrecio.setText("");

            //this.recarga_lstResultados.addFooterView(findViewById(R.id.recarga_footer));
            this.config = new Config(this);

            //setTimer(config.INACTIVITY_TIME);
            //}
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    /*
    @Override
    protected void onDataReceived(final byte[] buffer, final int size) {
        runOnUiThread(new Runnable() {
            public void run() {
                //leemos la data recibida
                String tString = new String(buffer, 0, size);

                String mensajeError = mSerialPort.EsError(tString);

                //vemos si coincide con un mensaje de error conocido
                if (mensajeError != null)
                    Toast.makeText(MainActivity.this, mensajeError, Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(MainActivity.this, "Recibido:" + "\n" + tString, Toast.LENGTH_LONG).show();

            }
        });
    }
    */

    @Override
    protected void onResume() {
        try {
            super.onResume();

            if (!CheckValoresConfigurados()) {

                //ADVERTENCIA POR FALTA DE CONFIGURACION
                AlertDialog c = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Configuracion inicial")
                        .setMessage("Antes de iniciar, por favor edite los valores de configuracion")
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Configuracion("INIT");
                            }

                        })
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .create();
                c.setCancelable(false);
                c.setCanceledOnTouchOutside(false);
                c.show();
            }

            //vamos a recuperar las diferentes entidades de configuracion
            ObtenerEntidades();

            //cargamos el menu
            GeneraMenu();

            //Obtenemos los valores de configuracion general
            this.config = new Config(this);

            this.txtFecha_panel.setText(Formato.FormateaDate(new Date(), Formato.DateFormat));
            this.txtHora_panel.setText(Formato.FormateaDate(new Date(), Formato.ShortTimeFormat));

        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        if (_broadcastReceiver != null)
            unregisterReceiver(_broadcastReceiver);

        GuardarPreferencias();
    }


    @Override
    protected void onDestroy() {
        if (_printer != null)
            _printer.end();
        super.onDestroy();
    }

    private boolean CheckValoresConfigurados() {

        //controlamos el codigo de comercio
        if (TextUtils.isEmpty(Preferences.getString(MainActivity.this, Preferences.KEY_CFG_COD_COM).trim()))
            return false;

        //controlamos el id de terminal
        if (TextUtils.isEmpty(Preferences.getString(MainActivity.this, Preferences.KEY_CFG_ID_TERM).trim()))
            return false;

        //controlamos el ip del servidor
        if (TextUtils.isEmpty(Preferences.getString(MainActivity.this, Preferences.KEY_CFG_IP_SERV).trim()))
            return false;

        //controlamos el port del servidor
        if (TextUtils.isEmpty(Preferences.getString(MainActivity.this, Preferences.KEY_CFG_PORT_SERV).trim()))
            return false;

        return true; //config ok
    }

    private void GuardarPreferencias(){
        Preferences.savePreferences(MainActivity.this, lastTicket, proxNroAudit, IDExtCliente);
    }

    private void ObtenerEntidades() {
        //vamos a recuperar los productos habilitados para el comercio
        ObtenerProductosHabilitados();

        //vamos a recuperar los bancos
        ObtenerListadoBancos();

        //vamos a recuperar los mensajes desde el terminal
        ObtenerListadoMensajesTerminal();

        //vamos a recuperar los datos de aviso
        ObtenerListadoPropsDeposito();

        //Obtenemos los valores de tickets y auditoria
        lastTicket = Preferences.getInteger(MainActivity.this, Preferences.KEY_LAST_TICKET, 1); //TODO: va de 1 a 9999
        proxNroAudit = Preferences.getInteger(MainActivity.this, Preferences.KEY_NEXT_NRO_AUDIT, 0); //TODO: va de 0 a 99999
        IDExtCliente = Preferences.getString(MainActivity.this, Preferences.KEY_ID_CLI_EXT);

        //obtenemos el ultimo mensaje de recarga enviado para poder hacer la consulta de ultima recarga
        lastRecarga = LeerLastMensajeRecarga();
    }

    private void ObtenerProductosHabilitados() {
        try {

            //obtenemos el ultimo mensaje de consulta recarga
            String response = Preferences.getString(MainActivity.this, Preferences.KEY_CONS_COMERC);

            listadoProductos = null; //por default lo dejamos en null para ser recuperados proximamente

            //si tengo un mensaje guardado
            if (!response.equals("")) {

                //obtenemos el mensajeRecarga de la respuesta
                LeerRespuesta(response);

                //vemos si no es necesario actualizar la informacion
                SimpleDateFormat formatOrigen = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                if (Utils.esMismoDia(formatOrigen.parse(lastmensajeRecarga.getFechaHora()), new Date())) {

                    //copiamos los productos
                    if (Integer.parseInt(lastmensajeRecarga.getCantProductos()) > 0) {
                        //guardamos una copia de todos los productos obtenidos en la consulta comercial
                        listadoProductos = new ArrayList<MensajeProductoRecarga>(lastmensajeRecarga.getProductos());
                        //Collections.copy(listadoProductos, lastmensajeRecarga.getProductos());
                    }

                    //guardamos el IdExterno del cliente para el aviso de deposito
                    IDExtCliente = lastmensajeRecarga.getIDExtCliente();
                    GuardarPreferencias();
                }

            }

        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }


    private void ObtenerListadoMensajesTerminal() {
        try {

            //obtenemos el XML de mensajes del terminal
            String response = Preferences.getString(MainActivity.this, Preferences.KEY_LIST_MENSAJES);

            if(!TextUtils.isEmpty(response))
                listadoMensajesTerminal = XmlRecargaParser.parseMensajesTerminal(new ByteArrayInputStream(response.getBytes(XmlRecargaParser.ENCODING)));

        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void ObtenerListadoBancos() {
        try {

            //obtenemos el XML de bancos
            String response = Preferences.getString(MainActivity.this, Preferences.KEY_LIST_BANCOS);

            if(!TextUtils.isEmpty(response))
                listadoBancos = XmlRecargaParser.parseBancos(new ByteArrayInputStream(response.getBytes(XmlRecargaParser.ENCODING)));

        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void ObtenerListadoPropsDeposito() {
        try {

            //obtenemos el XML de bancos
            String response = Preferences.getString(MainActivity.this, Preferences.KEY_LIST_DEP_AVISO);

            if(!TextUtils.isEmpty(response))
                listadoPropositoDeposito = XmlRecargaParser.parsePropositosDeposito(new ByteArrayInputStream(response.getBytes(XmlRecargaParser.ENCODING)));

        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }


    public void GuardarLastMensajeRecarga(MensajeRecarga mensajeRecarga) {

        //ROOT
        Preferences.setString(MainActivity.this, "LastRecarga_TipoMensaje", mensajeRecarga.getRoot_TipoMensaje());
        Preferences.setString(MainActivity.this, "LastRecarga_CodigoProceso", mensajeRecarga.getRoot_CodigoProceso());
        Preferences.setString(MainActivity.this, "LastRecarga_NumeroComercio", mensajeRecarga.getRoot_NumeroComercio());

        //TM
        Preferences.setString(MainActivity.this, "LastRecarga_NroTarjeta", mensajeRecarga.getNroTarjeta());
        Preferences.setString(MainActivity.this, "LastRecarga_MontoTransaccion", mensajeRecarga.getMontoTransaccion());
        Preferences.setString(MainActivity.this, "LastRecarga_NroAuditoriaTerminal", mensajeRecarga.getNroAuditoriaTerminal());
        Preferences.setString(MainActivity.this, "LastRecarga_FechaHoraTerminal", mensajeRecarga.getFechaHoraTerminal());
        Preferences.setString(MainActivity.this, "LastRecarga_ModoEntradaTerminal", mensajeRecarga.getModoEntradaTerminal());
        Preferences.setString(MainActivity.this, "LastRecarga_CondicionTerminal", mensajeRecarga.getCondicionTerminal());
        Preferences.setString(MainActivity.this, "LastRecarga_IdentTerminal", mensajeRecarga.getIdentTerminal());
        Preferences.setString(MainActivity.this, "LastRecarga_DatosAdicionales", mensajeRecarga.getDatosAdicionales());
        Preferences.setString(MainActivity.this, "LastRecarga_Moneda", mensajeRecarga.getMoneda());
        Preferences.setString(MainActivity.this, "LastRecarga_NroTickets", mensajeRecarga.getNroTickets());
        Preferences.setString(MainActivity.this, "LastRecarga_TipoProductoConsumido", mensajeRecarga.getTipoProductoConsumido());
        Preferences.setString(MainActivity.this, "LastRecarga_IMEITerminal", mensajeRecarga.getIMEITerminal());
        Preferences.setString(MainActivity.this, "LastRecarga_Vapp", mensajeRecarga.getVapp());
    }

    public MensajeRecarga LeerLastMensajeRecarga() {

        MensajeRecarga mensajeRecarga = new MensajeRecarga();

        //ROOT
        mensajeRecarga.setRoot_TipoMensaje(Preferences.getString(MainActivity.this, "LastRecarga_TipoMensaje"));

        if (mensajeRecarga.getRoot_TipoMensaje().equals("")) //sino encuentro el primer valor, ya retorno null
            return null;

        mensajeRecarga.setRoot_TipoMensaje(Preferences.getString(MainActivity.this, "LastRecarga_TipoMensaje"));
        mensajeRecarga.setRoot_NumeroComercio(Preferences.getString(MainActivity.this, "LastRecarga_NumeroComercio"));

        //TM
        mensajeRecarga.setNroTarjeta(Preferences.getString(MainActivity.this,"LastRecarga_NroTarjeta"));
        mensajeRecarga.setMontoTransaccion(Preferences.getString(MainActivity.this,"LastRecarga_MontoTransaccion", ""));
        mensajeRecarga.setNroAuditoriaTerminal(Preferences.getString(MainActivity.this,"LastRecarga_NroAuditoriaTerminal", ""));
        mensajeRecarga.setFechaHoraTerminal(Preferences.getString(MainActivity.this,"LastRecarga_FechaHoraTerminal", ""));
        mensajeRecarga.setModoEntradaTerminal(Preferences.getString(MainActivity.this,"LastRecarga_ModoEntradaTerminal", ""));
        mensajeRecarga.setCondicionTerminal(Preferences.getString(MainActivity.this,"LastRecarga_CondicionTerminal", ""));
        mensajeRecarga.setIdentTerminal(Preferences.getString(MainActivity.this,"LastRecarga_IdentTerminal", ""));
        mensajeRecarga.setDatosAdicionales(Preferences.getString(MainActivity.this,"LastRecarga_DatosAdicionales", ""));
        mensajeRecarga.setMoneda(Preferences.getString(MainActivity.this,"LastRecarga_Moneda", ""));
        mensajeRecarga.setNroTickets(Preferences.getString(MainActivity.this,"LastRecarga_NroTickets", ""));
        mensajeRecarga.setTipoProductoConsumido(Preferences.getString(MainActivity.this,"LastRecarga_TipoProductoConsumido", ""));
        mensajeRecarga.setIMEITerminal(Preferences.getString(MainActivity.this,"LastRecarga_IMEITerminal", ""));
        mensajeRecarga.setVapp(Preferences.getString(MainActivity.this,"LastRecarga_Vapp", ""));

        return mensajeRecarga;
    }

    private void Init_AsignarViews() {

        this.btnCalc = (ImageButton) findViewById(R.id.recarga_btnCalc);

        this.dummy_view = (ImageView) findViewById(R.id.recarga_dummy);

        this._recargaView = findViewById(R.id.recarga_main_layout);
        this._recargaStatusView = findViewById(R.id.recarga_status);
        this._recargaStatusMessageView = (TextView) findViewById(R.id.recarga_status_message);

        this.txtPanel = (TextView) findViewById(R.id.panel_txt_valor);

        this.lstMenu = (ListView) findViewById(R.id.recarga_lstMenu);

        this.recarga_lstResultados = (ListView) findViewById(R.id.recarga_lstResultados);
        this.recarga_btnImprimirResultados = (Button) findViewById(R.id.recarga_btn_imprimir);
        this.recarga_btnReintentarRecarga = (Button) findViewById(R.id.recarga_btn_reintentar);
        this.recarga_btnVolver = (Button) findViewById(R.id.recarga_btn_volver);

        //controles del pad
        this.btnCancelar = (Button) findViewById(R.id.pad_btnCantidad);
        this.btnPrecio = (Button) findViewById(R.id.pad_btnPrecio);
        this.btnEnter = (Button) findViewById(R.id.pad_btnEnter);
        this.btnBackspace = (ImageButton) findViewById(R.id.pad_btnClear);
        this.btn0 = (Button) findViewById(R.id.pad_btnNro0);
        this.btn1 = (Button) findViewById(R.id.pad_btnNro1);
        this.btn2 = (Button) findViewById(R.id.pad_btnNro2);
        this.btn3 = (Button) findViewById(R.id.pad_btnNro3);
        this.btn4 = (Button) findViewById(R.id.pad_btnNro4);
        this.btn5 = (Button) findViewById(R.id.pad_btnNro5);
        this.btn6 = (Button) findViewById(R.id.pad_btnNro6);
        this.btn7 = (Button) findViewById(R.id.pad_btnNro7);
        this.btn8 = (Button) findViewById(R.id.pad_btnNro8);
        this.btn9 = (Button) findViewById(R.id.pad_btnNro9);
        this.btnPunto = (Button) findViewById(R.id.pad_btnPunto);

        //controles del panel
        this.imgIcono_panel = (ImageView) findViewById(R.id.panel_img_icono);
        this.txtWelcome_panel = (TextView) findViewById(R.id.panel_txt_welcome);
        this.txtUser_panel = (TextView) findViewById(R.id.panel_txt_user);
        this.txtComentario_panel = (TextView) findViewById(R.id.panel_txt_comentario);
        this.txtFuncion_panel = (TextView) findViewById(R.id.panel_txt_funcion);
        this.txtValor_panel = (TextView) findViewById(R.id.panel_txt_valor);
        this.txtFecha_panel = (TextView) findViewById(R.id.panel_txt_fecha);
        this.txtHora_panel = (TextView) findViewById(R.id.panel_txt_hora);

        //controles del popup medio pago
        this.btnConfirmar_mp = (Button) findViewById(R.id.medio_pago_btn_aceptar);
        this.btnCancelar_mp = (Button) findViewById(R.id.medio_pago_btn_cancelar);
        this.txtTotal_mp = (TextView) findViewById(R.id.medio_pago_txt_total);
        this.rbtMedios_mp = (RadioGroup) findViewById(R.id.medio_pago_medios);
    }

    private void Init_AsignarEventos() {

        this.lstMenu.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ItemMenu item = menuAdapter.getDatos().get(position);

                if (item.getHijos() != null && item.getHijos().size() > 0) {
                    /*
                    if(!item.getDescripcion().equals("Volver")){

                        itemVolver.setHijos(menuAdapter.getDatos());

                        if(!item.getHijos().contains(itemVolver))
                            item.getHijos().add(itemVolver);

                        menuAdapter = new ItemMenuAdapter(MainActivity.this, item.getHijos());
                        lstMenu.setAdapter(menuAdapter);
                    }
                    else{

                        menuAdapter = new ItemMenuAdapter(MainActivity.this, item.getHijos());
                        lstMenu.setAdapter(menuAdapter);
                    }
                    */


                    menuAdapter = new ItemMenuAdapter(MainActivity.this, item.getHijos());
                    lstMenu.setAdapter(menuAdapter);

                } else {
                    try {

                        if (item.getAccion().equals("")) {
                            Toast.makeText(MainActivity.this, "Funcionalidad no implementada", Toast.LENGTH_SHORT).show();
                        } else {

                            String valor = item.getValor();
                            if (!valor.equals("")) {
                                Method accion = MainActivity.this.getClass().getMethod(item.getAccion(), valor.getClass());
                                accion.invoke(MainActivity.this, valor);
                            } else {
                                Method accion = MainActivity.this.getClass().getMethod(item.getAccion(), null);
                                accion.invoke(MainActivity.this, null);
                            }
                        }
                    } catch (Exception ex) {
                        Toast.makeText(MainActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

            }
        });


        this.btnBackspace.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                String texto = txtPanel.getText().toString();

                if (!texto.isEmpty()) {
                    txtPanel.setText(texto.substring(0, texto.length() - 1));
                }
            }
        });

        this.btnBackspace.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                txtPanel.setText("");

                return false;
            }
        });

        //APERTURA DE CALCULADORA
        btnCalc = (ImageButton) findViewById(R.id.recarga_btnCalc);
        btnCalc.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_MAIN);
                i.setComponent(new ComponentName("com.android.calculator2", "com.android.calculator2.Calculator"));


                try {
                    MainActivity.this.startActivity(i);
                } catch (ActivityNotFoundException noSuchActivity) {
                    // si el calculator intent no está registrado, probamos con un nombre alternativo
                    Intent i2 = new Intent();
                    i2.setAction(Intent.ACTION_MAIN);
                    i2.setComponent(new ComponentName("com.sec.android.app.popupcalculator", "com.sec.android.app.popupcalculator.Calculator"));
                    try {
                        MainActivity.this.startActivity(i2);
                    } catch (ActivityNotFoundException noSuchActivity2) {
                        // calculator intent not found
                        Toast.makeText(MainActivity.this, "Acceso a calculadora no encontrado", Toast.LENGTH_LONG).show();
                    }
                }

            }
        });

        this.btnEnter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if (recargaStep > 0)
                    ProcesoRecarga();
                else if (avisoDepoStep > 0)
                    ProcesoAvisoDeposito();
                else if (visitaVendStep > 0)
                    ProcesoVisitaVendedor();
                else if (ventaAbonoStep > 0)
                    ProcesoVentaAbono();
                else if (cierreTurnoStep > 0)
                    ProcesoCierreTurno();
                else
                    btnCancelar.performClick();

            }
        });

        RestorePrintEventClick();

        this.recarga_btnReintentarRecarga.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                btnEnter.performClick();
            }
        });

        this.btn0.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("0");
            }
        });

        this.btn1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("1");
            }
        });

        this.btn2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("2");
            }
        });

        this.btn3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("3");
            }
        });

        this.btn4.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("4");
            }
        });

        this.btn5.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("5");
            }
        });

        this.btn6.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("6");
            }
        });

        this.btn7.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("7");
            }
        });

        this.btn8.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("8");
            }
        });

        this.btn9.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("9");
            }
        });

        this.btnPunto.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel(".");
            }
        });


        this.btnCancelar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Cancelar();
            }
        });

        this.recarga_btnVolver.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Cancelar();
            }
        });

        this.dummy_view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                dummy_view.setBackgroundColor(Color.TRANSPARENT);
                dummy_view.setVisibility(View.INVISIBLE);
                (findViewById(R.id.recarga_medio_pago)).setVisibility(View.INVISIBLE);
                //(findViewById(R.id.recarga_date_picker)).setVisibility(View.INVISIBLE);
            }
        });

        (findViewById(R.id.recarga_medio_pago)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //dejamos el evento click seteado en la vista para que no se vaya al click del dummyview
            }
        });

        this.btnConfirmar_mp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                dummy_view.setBackgroundColor(Color.TRANSPARENT);
                dummy_view.setVisibility(View.INVISIBLE);
                (findViewById(R.id.recarga_medio_pago)).setVisibility(View.INVISIBLE);

                if (recargaStep > 0) {
                    RECARGA_MEDIOPAGO = ((RadioButton) rbtMedios_mp.findViewById(rbtMedios_mp.getCheckedRadioButtonId())).getText().toString();
                    //generamos y enviamos la transaccion de recarga
                    Recarga();
                } else if (ventaAbonoStep > 0) {
                    VENTAABONO_TIPOPAGO = ((RadioButton) rbtMedios_mp.findViewById(rbtMedios_mp.getCheckedRadioButtonId())).getText().toString();
                    ProcesoVentaAbono();
                }

            }
        });

        this.btnCancelar_mp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.recarga_medio_pago).setVisibility(View.INVISIBLE);
                dummy_view.setBackgroundColor(Color.TRANSPARENT);
                dummy_view.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void TicketFormatearProx() {
        String strLote, strTicket;

        strTicket = "0000" + String.valueOf(lastTicket <= 9999 ? lastTicket : lastTicket % 9999);
        strLote = "000" + String.valueOf((lastTicket / 10000) + 1);

        //ttttlll : #ticket#lote
        RECARGA_NROTICKET = strTicket.substring(strTicket.length() - 4) + strLote.substring(strLote.length() - 3);
    }


    private void VisualizarMontosFijos() {
        //recarga_montos_fijos

        RadioGroup rgp = (RadioGroup) findViewById(R.id.recarga_montos_fijos);

        List<String> montosFijos = new ArrayList<String>();

        for (MensajeMontoFijoRecarga montoFijo : RECARGA_PRODUCTO_OBJ.getMontosFijos()) {
            String _montoFijo = "      " + montoFijo.getMontoFijo();
            montosFijos.add(_montoFijo.substring(_montoFijo.length() - 6)); //asi los formateamos y quedan todos al mismo nivel
        }

        /*
        if (RECARGA_IDPRODUCTO.equals("12")) { //CLARO
            montosFijos.add(" 2000");
            montosFijos.add(" 2500");
            montosFijos.add(" 3600");
            montosFijos.add(" 5000");
            montosFijos.add(" 7000");
            montosFijos.add("10000");
            montosFijos.add("15000");
            montosFijos.add("25000");
            montosFijos.add("70000");
            montosFijos.add("80000");
        } else if (RECARGA_IDPRODUCTO.equals("23")) { //CLARO-TV
            montosFijos.add(" 1500");
            montosFijos.add(" 6000");
            montosFijos.add("10000");
            montosFijos.add("15000");
            montosFijos.add("35000");
        }
        */

        if (montosFijos.size() > 0) {

            //armamos una opcion por cada montoFijo

            for (int i = 0; i < montosFijos.size(); i++) {
                String montoFijo = montosFijos.get(i);

                RadioButton radioButton = new RadioButton(MainActivity.this);
                radioButton.setText(montoFijo);
                radioButton.setId(i);
                //radioButton.setPadding(20, 0, 0, 0);


                rgp.addView(radioButton, LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
            }

            //findViewById(R.id.recarga_tr_mf).setVisibility(View.VISIBLE);
            findViewById(R.id.recarga_mf_view).setVisibility(View.VISIBLE);

            rgp.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {

                    String montoSeleccionado = ((RadioButton) group.findViewById(checkedId)).getText().toString().trim();

                    RECARGA_MONTO = montoSeleccionado;

                    //EscribeLinea("\t" + "$ " + new DecimalFormat("0.00").format(Double.valueOf(montoSeleccionado)), "RECARGA");
                    EscribeLinea("\t" + Formatos.FormateaDecimal(montoSeleccionado, Formatos.DecimalFormat, "$"), "RECARGA");

                    recargaStep = 4;

                    EscribeLinea("Presione OK para confirmar...", "RECARGA");

                    //findViewById(R.id.recarga_tr_mf).setVisibility(View.GONE);
                    findViewById(R.id.recarga_mf_view).setVisibility(View.GONE);
                }
            });
        }
    }

    private MensajeProductoRecarga ObtieneProductoByDesc(String descripcionProducto) {
        for (MensajeProductoRecarga producto : listadoProductos) {
            if (producto.getDescripcion().equals(descripcionProducto))
                return producto;
        }

        return null;
    }

    private MensajeProductoRecarga ObtieneProductoByCode(String codigoProducto) {
        for (MensajeProductoRecarga producto : listadoProductos) {
            if (producto.getCodigo().equals(codigoProducto))
                return producto;
        }

        return null;
    }

    private void AgregarTextoPanel(String texto) {
        this.txtPanel.setText(this.txtPanel.getText().toString() + texto);
        this.txtPanel.setVisibility(View.VISIBLE);
    }

    private void EscribeLinea(String linea) {
        EscribeLinea(linea, "");
    }

    private void EscribeLinea(String linea, String titulo) {

        if (this.lineasLayout == null)
            this.lineasLayout = new ArrayList<String>();

        this.lineasLayout.add(linea);

        EscribeLineas(this.lineasLayout, titulo);
    }

    private void EscribeLineas(List<String> lineas) {
        EscribeLineas(lineas, "");
    }

    private void EscribeLineas(List<String> lineas, String titulo) {

        //ocultamos el menu y la lista de resultados
        (findViewById(R.id.recarga_lstMenu)).setVisibility(View.GONE);
        //(findViewById(R.id.recarga_tr_resultados)).setVisibility(View.GONE);
        (findViewById(R.id.recarga_lstResultados)).setVisibility(View.GONE);
        //mostramos el layout de lineas
        (findViewById(R.id.recarga_lineas)).setVisibility(View.VISIBLE);

        if (!titulo.equals("")) {

            TextView txtTitulo = (TextView) findViewById(R.id.recarga_txt_titulo);
            txtTitulo.setVisibility(View.VISIBLE);
            txtTitulo.setText(titulo);
        } else {
            TextView txtTitulo = (TextView) findViewById(R.id.recarga_txt_titulo);
            txtTitulo.setVisibility(View.GONE);
            txtTitulo.setText("");
        }

        //blanqueamos
        ((TextView) findViewById(R.id.recarga_txt1)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.recarga_txt2)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.recarga_txt3)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.recarga_txt4)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.recarga_txt5)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.recarga_txt6)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.recarga_txt7)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.recarga_txt8)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.recarga_txt9)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.recarga_txt10)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.recarga_txt11)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.recarga_txt12)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.recarga_txt13)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.recarga_txt14)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.recarga_txt15)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.recarga_txt16)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.recarga_txt17)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.recarga_txt18)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.recarga_txt19)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.recarga_txt20)).setVisibility(View.GONE);

        for (int i = 0; i < lineas.size(); i++) {

            TextView txtLinea = null;

            switch (i + 1) {
                case 1:
                    txtLinea = (TextView) findViewById(R.id.recarga_txt1);
                    break;
                case 2:
                    txtLinea = (TextView) findViewById(R.id.recarga_txt2);
                    break;
                case 3:
                    txtLinea = (TextView) findViewById(R.id.recarga_txt3);
                    break;
                case 4:
                    txtLinea = (TextView) findViewById(R.id.recarga_txt4);
                    break;
                case 5:
                    txtLinea = (TextView) findViewById(R.id.recarga_txt5);
                    break;
                case 6:
                    txtLinea = (TextView) findViewById(R.id.recarga_txt6);
                    break;
                case 7:
                    txtLinea = (TextView) findViewById(R.id.recarga_txt7);
                    break;
                case 8:
                    txtLinea = (TextView) findViewById(R.id.recarga_txt8);
                    break;
                case 9:
                    txtLinea = (TextView) findViewById(R.id.recarga_txt9);
                    break;
                case 10:
                    txtLinea = (TextView) findViewById(R.id.recarga_txt10);
                    break;
                case 11:
                    txtLinea = (TextView) findViewById(R.id.recarga_txt11);
                    break;
                case 12:
                    txtLinea = (TextView) findViewById(R.id.recarga_txt12);
                    break;
                case 13:
                    txtLinea = (TextView) findViewById(R.id.recarga_txt13);
                    break;
                case 14:
                    txtLinea = (TextView) findViewById(R.id.recarga_txt14);
                    break;
                case 15:
                    txtLinea = (TextView) findViewById(R.id.recarga_txt15);
                    break;
                case 16:
                    txtLinea = (TextView) findViewById(R.id.recarga_txt16);
                    break;
                case 17:
                    txtLinea = (TextView) findViewById(R.id.recarga_txt17);
                    break;
                case 18:
                    txtLinea = (TextView) findViewById(R.id.recarga_txt18);
                    break;
                case 19:
                    txtLinea = (TextView) findViewById(R.id.recarga_txt19);
                    break;
                case 20:
                    txtLinea = (TextView) findViewById(R.id.recarga_txt20);
                    break;

            }

            String texto = lineas.get(i);
            if (texto.startsWith("@C@")) {
                texto = texto.replace("@C@", "");
                txtLinea.setGravity(Gravity.CENTER);
            } else {
                txtLinea.setGravity(Gravity.LEFT);
            }
            if (texto.startsWith("@B@")) {
                texto = texto.replace("@B@", "");
                txtLinea.setTypeface(null, Typeface.BOLD);
            } else {
                txtLinea.setTypeface(null, Typeface.NORMAL);
            }

            txtLinea.setText(texto);
            txtLinea.setVisibility(View.VISIBLE);
        }

        //envia las lineas al fondo para visualizar siempre lo ultimo
        ((ScrollView) findViewById(R.id.scrollView)).post(new Runnable() {
            public void run() {
                ((ScrollView) findViewById(R.id.scrollView)).fullScroll(View.FOCUS_DOWN);
            }
        });
    }

    private void GeneraMenu() {

        menuRecargaList = new ArrayList<ItemMenu>();

        //***************** Menu Principal *****************
        ItemMenu itemRecarga;
        ItemMenu itemUltRecarga;
        if (listadoProductos != null) {
            itemRecarga = new ItemMenu("RECARGA");
            itemUltRecarga = new ItemMenu("Ultima Recarga", null, "ConsultaUltimaRecarga", "");
        } else {
            //como no tenemos informacion de productos, hacemos una consulta comercial al llamar a Recarga
            // y luego se reinicia el menu
            itemRecarga = new ItemMenu("RECARGA", null, "ConsultaComercial", "PRODUCTOS");
            itemUltRecarga = new ItemMenu("Ultima Recarga", null, "ConsultaComercial", "PRODUCTOS");
        }

        ItemMenu itemVerMensajes = new ItemMenu("Ver Mensajes", null, "ConsultaMensajes", "");
        ItemMenu itemUltOp = new ItemMenu("Ultimas Operaciones", null, "ConsultaUltimasOperaciones", "");
        ItemMenu itemDatoComer = new ItemMenu("Datos Comercio");
        ItemMenu itemAvisoDep = new ItemMenu("Aviso de Deposito", null, "ProcesoAvisoDepositoInit", "CLI");
        ItemMenu itemMenuVendedor = new ItemMenu("Menu Vendedor");
        ItemMenu menuTec = new ItemMenu("Menu Tecnico");
        ItemMenu itemConf = new ItemMenu("Configuracion");
        ItemMenu itemEnvioMensajes = new ItemMenu("Envio de Mensajes", null, "EnvioMensajesTermInit", "");

        //****************Operadoras**********************************

        /* CREACION DE MENU DINAMICO (EN BASE A LA CONSULTA COMERCIAL)
        */
        if (listadoProductos != null) {//si tenemos info de productos llenamos el menu de Recarga
            for (MensajeProductoRecarga producto : listadoProductos) {
                itemRecarga.getHijos().add(new ItemMenu(producto.getDescripcion(), null, "ProcesoRecargaInit", producto.getDescripcion()));
            }
            itemRecarga.getHijos().add(new ItemMenu("Volver", menuRecargaList, "", ""));
        }

        //**************DatosComercio************************************

        itemDatoComer.getHijos().add(new ItemMenu("Consulta Datos", null, "ConsultaComercial", "DATOS"));
        itemDatoComer.getHijos().add(new ItemMenu("Consulta Saldo", null, "ConsultaComercial", "SALDO"));
        itemDatoComer.getHijos().add(new ItemMenu("Consulta Totales", null, "ConsultaVentas", "TOTALES"));
        itemDatoComer.getHijos().add(new ItemMenu("Consulta Ganancias", null, "ConsultaVentas", "GANANCIAS"));
        itemDatoComer.getHijos().add(new ItemMenu("Consulta Ultimos Abonos", null, "ConsultaUltimosAbonos", ""));
        itemDatoComer.getHijos().add(new ItemMenu("Cierre de Turno", null, "ProcesoCierreTurnoInit", ""));
        itemDatoComer.getHijos().add(new ItemMenu("Volver", menuRecargaList, "", ""));

        //*****************Menu Tecnico********************************* ************

        menuTec.getHijos().add(new ItemMenu("Editar Configuracion", null, "Configuracion", CONFIG_FX_EDIT));
        menuTec.getHijos().add(new ItemMenu("Imprimir Configuracion", null, "Configuracion", CONFIG_FX_PRINT));
        //menuTec.getHijos().add(new ItemMenu("Telecarga"));
        menuTec.getHijos().add(new ItemMenu("Volver", itemConf.getHijos(), "", ""));


        //**************Configuracion************************************ ************

        //itemConf.getHijos().add(new ItemMenu("Cambiar Clave", null, "", ""));
        itemConf.getHijos().add(menuTec);
        itemConf.getHijos().add(new ItemMenu("Pedir Tipo Pago", null, "Configuracion", CONFIG_FX_PAGOS));
        itemConf.getHijos().add(new ItemMenu("Volver", menuRecargaList, "", ""));

        //****************************************************** ************


        //*****************Menu Vendedor********************************* ************

        itemMenuVendedor.getHijos().add(new ItemMenu("Venta de Abono", null, "SolicitarClaveVendedor", "ProcesoVentaAbonoInit"));
        itemMenuVendedor.getHijos().add(new ItemMenu("Visita Vendedor", null, "SolicitarClaveVendedor", "ProcesoVisitaVendedorInit"));
        itemMenuVendedor.getHijos().add(new ItemMenu("Aviso de Deposito", null, "SolicitarClaveVendedor", "ProcesoAvisoDepositoVendInit"));
        //itemMenuVendedor.getHijos().add(new ItemMenu("Retiro de Terminal", null, "SolicitarClaveVendedor", "ProcesoRetiroTerminalInit"));
        itemMenuVendedor.getHijos().add(new ItemMenu("Volver", menuRecargaList, "", ""));

        //****************************************************** ************


        //*****************Menu Envío Mensajes********************************* ************


        //****************************************************** ************


        menuRecargaList.add(itemRecarga);
        menuRecargaList.add(itemUltRecarga);
        menuRecargaList.add(itemVerMensajes);
        menuRecargaList.add(itemUltOp);
        menuRecargaList.add(itemDatoComer);
        menuRecargaList.add(itemAvisoDep);
        menuRecargaList.add(itemMenuVendedor);
        menuRecargaList.add(itemEnvioMensajes);
        menuRecargaList.add(itemConf);
        //menuRecargaList.add(menuVolver);

        menuAdapter = new ItemMenuAdapter(this, menuRecargaList);
        lstMenu.setAdapter(menuAdapter);
    }


    private static final String CONFIG_FX_INIT = "INIT";
    private static final String CONFIG_FX_EDIT = "EDIT";
    private static final String CONFIG_FX_PAGOS = "PAGOS";
    private static final String CONFIG_FX_PRINT = "PRINT";

    public void Configuracion(String funcion) {
        try {
            if (funcion.equals(CONFIG_FX_INIT)) {
                Intent i = new Intent(MainActivity.this, ConfiguracionActivity.class);
                startActivity(i);
            } else if (funcion.equals(CONFIG_FX_PAGOS)) {
                Intent i = new Intent(MainActivity.this, ConfiguracionActivity.class);
                i.putExtra(ConfiguracionActivity.EXTRA_PEDIR_PAGO, true);
                startActivity(i);
            } else if (funcion.equals(CONFIG_FX_PRINT)) {
                ImprimeConfiguracion();
            } else if (funcion.equals(CONFIG_FX_EDIT)) {

                //vamos a solicitar la clave de tecnico para ingresar a la edicion de la configuracion

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                // Get the layout inflater
                LayoutInflater inflater = MainActivity.this.getLayoutInflater();

                // Inflate and set the layout for the dialog
                // Pass null as the parent view because its going in the dialog layout
                builder.setView(inflater.inflate(R.layout.dialog_clave_tecnico, null))
                        // Add action buttons
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                try {

                                    String passIngresada = ((EditText) ((AlertDialog) dialog).findViewById(R.id.dialog_clave_tecnico_password)).getText().toString();
                                    String claveTecnico = PassGenerator.GenerarClaveTecnico(new Date(), Preferences.getString(MainActivity.this, Preferences.KEY_CFG_COD_COM), Preferences.getString(MainActivity.this, Preferences.KEY_CFG_ID_TERM));

                                    if (passIngresada.equals(claveTecnico)) {
                                        Intent i = new Intent(MainActivity.this, ConfiguracionActivity.class);
                                        startActivity(i);
                                    } else {
                                        Toast.makeText(MainActivity.this, "Clave ingresada erronea", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (Exception e) {
                                    Toast.makeText(MainActivity.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }
                        })
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });

                AlertDialog dialog = builder.create();

                dialog.setTitle("Ingrese Clave de Técnico");
                dialog.setCanceledOnTouchOutside(true);
                dialog.setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                    }
                });

                dialog.show();

            }

        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void GeneraTicketRecarga(MensajeRecarga respuestaRecarga, MensajeProductoRecarga productoRecarga, String modo) throws Exception {
        try {

            if (_printer == null)
                _printer = Printer.getInstance(config.PRINTER_MODEL);

            if (!_printer.initialize()) {
                Toast.makeText(MainActivity.this, "Error al inicializar", Toast.LENGTH_SHORT).show();
                _printer.end();
            } else {

                TicketRecargaBuilder ticket = new TicketRecargaBuilder();

                ticket.set_header(modo);
                ticket.set_titulo1("COMPROBANTE");
                ticket.set_titulo2("RECARGA");
                ticket.set_producto(productoRecarga.getDescripcion());
                ticket.set_nroTelefono(respuestaRecarga.getNroTarjeta());
                ticket.set_montoRecarga(respuestaRecarga.getMontoTransaccion());

                ticket.set_codAutoriz(respuestaRecarga.getCodAutorizacion());
                ticket.set_razonSocialComercio(this.config.COMERCIO);
                ticket.set_nroComercio(respuestaRecarga.getRoot_NumeroComercio());
                ticket.set_idTransaccion(respuestaRecarga.getNroTransaccion());
                ticket.set_idControl(respuestaRecarga.getNroAuditoriaTerminal());
                ticket.set_idTerminal(respuestaRecarga.getIdentTerminal());

                //formateamos la fecha recibida
                SimpleDateFormat formatOrigen = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                SimpleDateFormat formatDestino = new SimpleDateFormat("dd-MM-yy HH:mm");

                ticket.set_fechaHora(formatDestino.format(formatOrigen.parse(respuestaRecarga.getFechaHora())));
                ticket.set_nuevoSaldo(respuestaRecarga.getSaldoTarjeta());
                ticket.set_validezRecarga(respuestaRecarga.getValidezRecarga());
                ticket.set_bonosAcumulados(respuestaRecarga.getBonosAcumulados());
                ticket.set_descAcumulados(respuestaRecarga.getDescuentosAcumulados());
                ticket.set_mensajeLinea1(respuestaRecarga.getMensajePromocional());
                ticket.set_mensajeLinea2(productoRecarga.getPieTicket());

                ticket.PrintTicket(_printer);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    private void showDatePickerDialog() {
        /*dummy_view.setBackgroundColor(getResources().getColor(R.color.blur));
        dummy_view.setVisibility(View.VISIBLE);
        findViewById(R.id.recarga_date_picker).setVisibility(View.VISIBLE);*/
        //((DatePicker)findViewById(R.id.datePicker)).s
        /** Get the current date */
        final Calendar cal = Calendar.getInstance();
        int pYear = cal.get(Calendar.YEAR);
        int pMonth = cal.get(Calendar.MONTH);
        int pDay = cal.get(Calendar.DAY_OF_MONTH);

        final String[] meses = {"Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago",
                "Sep", "Oct", "Nov", "Dic"};

        DatePickerDialog a = new DatePickerDialog(MainActivity.this, new OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                String año = String.valueOf(year);
                String mes = "00" + String.valueOf(monthOfYear + 1);
                mes = mes.substring(mes.length() - 2);
                String dia = "00" + String.valueOf(dayOfMonth);
                dia = dia.substring(dia.length() - 2);

                String fechaIngresada = dia + "/" + Arrays.asList(meses).get(monthOfYear) + "/" + año;

                String fechaIngresadaRecarga = año + "-" + mes + "-" + dia;

                DEPOSITO_FECHA = fechaIngresadaRecarga;
                DEPOSITO_FECHA_DISPLAY = fechaIngresada;

                EscribeLinea("\t" + DEPOSITO_FECHA_DISPLAY, "AVISO DEPOSITO");
                EscribeLinea("Ingrese hora del deposito (HH:MM):", "AVISO DEPOSITO");

                //btnEnter.performClick();
            }
        }, pYear, pMonth, pDay);

        a.getDatePicker().setSpinnersShown(false);
        a.getDatePicker().setCalendarViewShown(true);

        a.setTitle("Ingrese Fecha del Deposito");

        a.setButton(DatePickerDialog.BUTTON_POSITIVE, "Seleccionar", a);
        a.setButton(DatePickerDialog.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Cancelar();
            }
        });
        a.setCanceledOnTouchOutside(true);
        a.setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                Cancelar();
            }
        });
        a.show();
    }



    /*
    private void showDateTimeDialog() {
        // Create the dialog
        final Dialog mDateTimeDialog = new Dialog(this);
        // Inflate the root layout
        final RelativeLayout mDateTimeDialogView = (RelativeLayout) getLayoutInflater().inflate(R.layout.date_picker_dialog, null);
        // Grab widget instance
        final DateTimePicker mDateTimePicker = (DateTimePicker) mDateTimeDialogView.findViewById(R.id.DateTimePicker);
        //mDateTimePicker.setDateChangedListener(this);

        // Update demo edittext when the "OK" button is clicked
        ((Button) mDateTimeDialogView.findViewById(R.id.SetDateTime)).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                mDateTimePicker.clearFocus();
                // TODO Auto-generated method stub

                mDateTimeDialog.dismiss();

                DEPOSITO_FECHA = mDateTimePicker.toString_Recarga();

                EscribeLinea("\t" + mDateTimePicker.toString(), "AVISO DEPOSITO");

                btnEnter.performClick();
            }
        });

        // Cancel the dialog when the "Cancel" button is clicked
        ((Button) mDateTimeDialogView.findViewById(R.id.CancelDialog)).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mDateTimeDialog.cancel();
            }
        });

        // Reset Date and Time pickers when the "Reset" button is clicked
        ((Button) mDateTimeDialogView.findViewById(R.id.ResetDateTime)).setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                mDateTimePicker.reset();
            }
        });

        // Setup TimePicker
        // No title on the dialog window
        //mDateTimeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDateTimeDialog.setTitle("Ingrese Fecha y Hora del Deposito");
        // Set the dialog content view
        mDateTimeDialog.setContentView(mDateTimeDialogView);
        // Display the dialog
        mDateTimeDialog.show();
    }
    */

    private void Cancelar() {

        if (this.recargaStep > 0) {
            //region STEPS RECARGA
            switch (this.recargaStep) {
                case 1: {

                    this.recargaStep = 0;

                    //mostramos la lista con las operadoras nuevamente
                    this.lstMenu.setVisibility(View.VISIBLE);

                    //limpiamos la operadora seleccionada
                    this.txtComentario_panel.setText("");
                    this.txtComentario_panel.setVisibility(View.VISIBLE);

                    //ocultamos el layout de lineas
                    (findViewById(R.id.recarga_lineas)).setVisibility(View.GONE);

                    this.txtPanel.setText("");

                }
                break;
                case 2: {
                    //volvemos al paso de ingrese nro
                    //(findViewById(R.id.recarga_tr_mf)).setVisibility(View.GONE);
                    (findViewById(R.id.recarga_mf_view)).setVisibility(View.GONE);
                    //eliminamos los radio button agregados
                    RadioGroup rgp = (RadioGroup) findViewById(R.id.recarga_montos_fijos);
                    rgp.removeAllViews();

                    ProcesoRecargaInit(RECARGA_PRODUCTO_DESC);

                    this.txtPanel.setText("");

                }
                break;
                case 3: {
                    //volvemos al paso de ingrese nro
                    //(findViewById(R.id.recarga_tr_mf)).setVisibility(View.GONE);
                    (findViewById(R.id.recarga_mf_view)).setVisibility(View.GONE);
                    //eliminamos los radio button agregados
                    RadioGroup rgp = (RadioGroup) findViewById(R.id.recarga_montos_fijos);
                    rgp.removeAllViews();

                    ProcesoRecargaInit(RECARGA_PRODUCTO_DESC);

                    this.txtPanel.setText("");

                }
                break;
                case 4: {

                    //volvemos al paso de ingrese/seleccione monto

                    //volvemos al paso de ingrese nro
                    //(findViewById(R.id.recarga_tr_mf)).setVisibility(View.GONE);
                    (findViewById(R.id.recarga_mf_view)).setVisibility(View.GONE);
                    //eliminamos los radio button agregados
                    RadioGroup rgp = (RadioGroup) findViewById(R.id.recarga_montos_fijos);
                    rgp.removeAllViews();

                    ProcesoRecargaInit(RECARGA_PRODUCTO_DESC);

                    //simulamos que ingresamos el nro tel nuevamente
                    this.recargaStep = 1;
                    this.txtPanel.setText(RECARGA_NROTEL);

                    this.btnEnter.performClick();

                }
                break;
                case 5: {
                    this.recargaStep = 0;

                    btnCancelar.performClick();
                }
                break;
                case 6: {
                    this.recargaStep = 0;

                    btnCancelar.performClick();
                }
                break;
            }
            //endregion
        } else if (this.avisoDepoStep > 0) {
            //region STEPS AVISO DEPOSITO
            switch (this.avisoDepoStep) {
                case 100: {
                    this.avisoDepoStep = 0;

                    btnCancelar.performClick();
                }
                break;
                case 1: {

                    if (DEPOSITO_CLI_VEND.equals("VEN")) {
                        //si es VENDEDOR iniciamos nuevamente
                        ProcesoAvisoDepositoInit(DEPOSITO_CLI_VEND);
                    } else {
                        //si es CLIENTE cerramos
                        this.avisoDepoStep = 0;

                        btnCancelar.performClick();
                    }
                }
                break;
                case 2: {

                    if (DEPOSITO_CLI_VEND.equals("VEN")) {
                        //si es VENDEDOR iniciamos nuevamente
                        this.avisoDepoStep = 1;

                        //limpiamos las ultimas 3 lineas
                        this.lineasLayout.remove(this.lineasLayout.size() - 1);
                        this.lineasLayout.remove(this.lineasLayout.size() - 1);
                        this.lineasLayout.remove(this.lineasLayout.size() - 1);

                        ProcesoAvisoDeposito();
                    } else {
                        //si es CLIENTE iniciamos nuevamente
                        ProcesoAvisoDepositoInit(DEPOSITO_CLI_VEND);
                    }

                }
                break;
                case 3: {

                    this.avisoDepoStep = 2;

                    ProcesoAvisoDeposito();

                }
                break;
                case 31: {

                    this.avisoDepoStep = 2;

                    txtValor_panel.setText("");

                    //limpiamos las ultimas 3 lineas
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);

                    ProcesoAvisoDeposito();

                }
                break;
                case 32: {

                    this.avisoDepoStep = 3;

                    txtValor_panel.setText("");

                    //limpiamos las ultimas 3 lineas
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);

                    ProcesoAvisoDeposito();

                }
                break;
                case 41: {

                    this.avisoDepoStep = 2;

                    txtValor_panel.setText("");

                    //limpiamos las ultimas 3 lineas
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);

                    ProcesoAvisoDeposito();

                }
                break;
                case 42: {

                    this.avisoDepoStep = 3;

                    txtValor_panel.setText("");

                    //limpiamos las ultimas 3 lineas
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);

                    ProcesoAvisoDeposito();

                }
                break;
                case 6: {

                    avisoDepoStep = 3;

                    txtValor_panel.setText("");

                    //limpiamos las ultimas 3 lineas
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);

                    ProcesoAvisoDeposito();

                }
                break;
                case 7: {

                    /*if (DEPOSITO_TIPO.equals("EFE") || DEPOSITO_TIPO.equals("CHE"))
                        avisoDepoStep = 32;

                    if (DEPOSITO_TIPO.equals("TRA"))
                        avisoDepoStep = 42;

                    this.txtValor_panel.setText(DEPOSITO_NUMERO);*/

                    if (DEPOSITO_FECHA == null) { //sino se ingresó FECHA

                        avisoDepoStep = 5;

                        //limpiamos las ultimas 3 lineas
                        this.lineasLayout.remove(this.lineasLayout.size() - 1);
                        this.lineasLayout.remove(this.lineasLayout.size() - 1);
                        this.lineasLayout.remove(this.lineasLayout.size() - 1);

                    } else { //si ya se ingresó FECHA
                        this.avisoDepoStep = 6;

                        this.txtValor_panel.setText(DEPOSITO_MONTO);

                        DEPOSITO_FECHA = null;

                        //limpiamos las ultimas 4 lineas
                        this.lineasLayout.remove(this.lineasLayout.size() - 1);
                        this.lineasLayout.remove(this.lineasLayout.size() - 1);
                        this.lineasLayout.remove(this.lineasLayout.size() - 1);
                        this.lineasLayout.remove(this.lineasLayout.size() - 1);


                    }

                    ProcesoAvisoDeposito();

                }
                break;
                case 8: {

                    this.avisoDepoStep = 6;

                    this.txtValor_panel.setText(DEPOSITO_MONTO);

                    DEPOSITO_FECHA = null;

                    //limpiamos las ultimas 4 lineas
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);

                    ProcesoAvisoDeposito();

                }
                break;
                case 81: {

                    this.avisoDepoStep = 6;

                    this.txtValor_panel.setText(DEPOSITO_MONTO);

                    DEPOSITO_FECHA = null;

                    //limpiamos las ultimas 4 lineas
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);

                    ProcesoAvisoDeposito();

                }
                break;
                case 10: {
                    this.avisoDepoStep = 81;

                    this.txtValor_panel.setText("");

                    //limpiamos las ultimas 3 lineas
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);

                    ProcesoAvisoDeposito();

                }
                break;
                case 11: {

                    this.avisoDepoStep = 9;

                    this.txtValor_panel.setText("");

                    //limpiamos las ultimas 3 lineas
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);

                    ProcesoAvisoDeposito();

                }
                break;
                case 14: {

                    if (DEPOSITO_TIPO.equals("CHE")) {

                        this.avisoDepoStep = 9;

                        this.txtValor_panel.setText("");

                        //limpiamos las ultimas 3 lineas
                        this.lineasLayout.remove(this.lineasLayout.size() - 1);
                        this.lineasLayout.remove(this.lineasLayout.size() - 1);
                        this.lineasLayout.remove(this.lineasLayout.size() - 1);

                        ProcesoAvisoDeposito();

                    } else {
                        this.avisoDepoStep = 6;

                        this.txtValor_panel.setText(DEPOSITO_MONTO);

                        DEPOSITO_FECHA = null;

                        //limpiamos las ultimas 4 lineas
                        this.lineasLayout.remove(this.lineasLayout.size() - 1);
                        this.lineasLayout.remove(this.lineasLayout.size() - 1);
                        this.lineasLayout.remove(this.lineasLayout.size() - 1);
                        this.lineasLayout.remove(this.lineasLayout.size() - 1);

                        ProcesoAvisoDeposito();
                    }

                }
                break;
                /*
                case 14: {

                    this.avisoDepoStep = 12;

                    this.txtValor_panel.setText("");

                    //limpiamos las ultimas 3 lineas
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);

                    ProcesoAvisoDeposito();

                }
                break;
                */
                case 15: {

                    /*
                    this.avisoDepoStep = 13;

                    this.txtValor_panel.setText(DEPOSITO_NROVENDEDOR);

                    //limpiamos las ultimas 4 lineas
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);

                    ProcesoAvisoDeposito();
                    */

                    this.avisoDepoStep = 12;

                    this.txtValor_panel.setText("");

                    //limpiamos las ultimas 3 lineas
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);

                    ProcesoAvisoDeposito();

                }
                break;
            }
            //endregion
        } else if (visitaVendStep > 0) {
            //region VISITA VENDEDOR
            switch (visitaVendStep) {
                case 1: {

                    visitaVendStep = 0;

                    //mostramos el menu nuevamente
                    this.lstMenu.setVisibility(View.VISIBLE);

                    //ocultamos el layout de lineas
                    (findViewById(R.id.recarga_lineas)).setVisibility(View.GONE);

                    this.txtPanel.setText("");

                }
                break;
                case 2: {

                    ProcesoVisitaVendedorInit();

                    this.txtPanel.setText("");

                }
                break;
                case 3: {

                    ProcesoVisitaVendedorInit();

                    this.txtPanel.setText(REGISTRO_NROVENDEDOR);

                    this.btnEnter.performClick();
                }
            }
            //endregion
        } else if (ventaAbonoStep > 0) {
            //region VENTA ABONO
            switch (ventaAbonoStep) {
                case 1: {
                    ventaAbonoStep = 0;

                    //mostramos el menu nuevamente
                    this.lstMenu.setVisibility(View.VISIBLE);

                    //ocultamos el layout de lineas
                    (findViewById(R.id.recarga_lineas)).setVisibility(View.GONE);

                    this.txtPanel.setText("");
                }
                break;
                case 2: {

                    this.txtPanel.setText("");

                    //limpiamos las ultimas 3 lineas
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);

                    ProcesoVentaAbonoInit();

                }
                break;
                case 3: {
                    ventaAbonoStep = 1;

                    txtValor_panel.setText(VENTAABONO_MONTO);

                    //limpiamos las ultimas 4 lineas
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);

                    ProcesoVentaAbono();

                }
                break;
                case 4: {
                    ventaAbonoStep = 3;

                    txtValor_panel.setText("");

                    //limpiamos las ultimas 3 lineas
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);

                    EscribeLinea("Ingrese el nro. de factura:", "VENTA ABONO");


                }
                break;
                case 5: {
                    ventaAbonoStep = 4;

                    txtValor_panel.setText("");

                    //limpiamos las ultimas 2 lineas
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);

                    EscribeLinea("Ingrese el nro. de vendedor:", "VENTA ABONO");
                }
                break;
                case 7: {
                    ventaAbonoStep = 4;

                    txtValor_panel.setText(VENTAABONO_NROVENDEDOR);

                    //limpiamos las ultimas 7 lineas
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);

                    ProcesoVentaAbono();

                }
                break;
            }
            //endregion
        } else if (cierreTurnoStep > 0) {
            //region CIERRE TURNO
            switch (cierreTurnoStep) {
                case 1: {
                    cierreTurnoStep = 0;

                    //mostramos el menu nuevamente
                    this.lstMenu.setVisibility(View.VISIBLE);

                    //ocultamos el layout de lineas
                    (findViewById(R.id.recarga_lineas)).setVisibility(View.GONE);

                    this.txtPanel.setText("");

                }
                break;
                case 2: {
                    txtValor_panel.setText("");

                    //limpiamos las ultimas 4 lineas
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);

                    ProcesoCierreTurnoInit();

                }
                break;
                case 3: {

                    cierreTurnoStep = 2;
                    txtValor_panel.setText("");

                    //limpiamos las ultimas 4 lineas
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);

                    //ProcesoCierreTurno();

                }
                break;
            }
            //endregion
        } else if (flagDatosComercio) {

            flagDatosComercio = false;

            //mostramos la lista con el menu nuevamente
            this.lstMenu.setVisibility(View.VISIBLE);

            //ocultamos controles del layout
            (findViewById(R.id.recarga_lineas)).setVisibility(View.GONE);
            (findViewById(R.id.recarga_lstResultados)).setVisibility(View.GONE);
            this.recarga_btnVolver.setVisibility(View.GONE);
            this.recarga_btnReintentarRecarga.setVisibility(View.GONE);
            this.recarga_btnImprimirResultados.setVisibility(View.GONE);
            RestorePrintEventClick();

            this.txtPanel.setText("");

            this.txtComentario_panel.setText("");
            this.txtComentario_panel.setVisibility(View.INVISIBLE);

        } else {

            //mostramos el menu
            GeneraMenu();

            this.lstMenu.setVisibility(View.VISIBLE);

            //ocultamos el layout de lineas, la lista de resultados y los montos fijos
            //(findViewById(R.id.recarga_tr_resultados)).setVisibility(View.GONE);
            (findViewById(R.id.recarga_lstResultados)).setVisibility(View.GONE);
            (findViewById(R.id.recarga_lineas)).setVisibility(View.GONE);
            this.recarga_btnVolver.setVisibility(View.GONE);
            this.recarga_btnReintentarRecarga.setVisibility(View.GONE);
            this.recarga_btnImprimirResultados.setVisibility(View.GONE);
            RestorePrintEventClick();

            //(findViewById(R.id.recarga_tr_mf)).setVisibility(View.GONE);
            (findViewById(R.id.recarga_mf_view)).setVisibility(View.GONE);


            //eliminamos los radio button agregados
            RadioGroup rgp = (RadioGroup) findViewById(R.id.recarga_montos_fijos);
            rgp.removeAllViews();
        /*
        for (int i = 0; i < rgp.getChildCount(); i++) {
            rgp.removeViewAt(i);
        }
        */

            this.txtComentario_panel.setText("");
            this.txtComentario_panel.setVisibility(View.INVISIBLE);

            this.txtPanel.setText("");
        }
    }

    public void RestorePrintEventClick() {
        this.recarga_btnImprimirResultados.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ImpresionResultados();
            }
        });
    }

    private void RegistrarVenta(MensajeRecarga mensajeRecarga) {

        try {

            Date now = Utils.Util_FechaHora(mensajeRecarga.getFechaHora());

            //obtenemos el medio de pago seleccionado
            //String medio_pago = ((RadioButton) this.rbtMedios_mp.findViewById(this.rbtMedios_mp.getCheckedRadioButtonId())).getText().toString();


            Venta venta = new Venta(
                    0,
                    Double.valueOf(mensajeRecarga.getMontoTransaccion()),
                    1,
                    Formatos.FormateaDate(now, Formatos.DateFormat),
                    Formatos.FormateaDate(now, Formatos.TimeFormat),
                    "RCG",
                    this._user.getId(),
                    RECARGA_MEDIOPAGO
            );

            //VENTA
            ContentValues cvVenta = new ContentValues();

            cvVenta.put(VentasHelper.VENTA_TOTAL, venta.getTotal());
            cvVenta.put(VentasHelper.VENTA_FECHA, venta.getFecha());
            cvVenta.put(VentasHelper.VENTA_HORA, venta.getHora());
            cvVenta.put(VentasHelper.VENTA_MEDIO_PAGO, venta.getMedio_pago());
            cvVenta.put(VentasHelper.VENTA_CODIGO, venta.getCodigo());
            cvVenta.put(VentasHelper.VENTA_CANTIDAD, venta.getCantidad());
            cvVenta.put(VentasHelper.VENTA_USERID, venta.getUserid());

            Uri newVentaUri = getContentResolver().insert(Uri.parse("content://com.pds.ficle.ep.ventas.contentprovider/ventas"), cvVenta);

            venta.setId(Integer.valueOf(newVentaUri.getPathSegments().get(1)));

            //DETALLES
            ContentValues cvDetalle = new ContentValues();

            cvDetalle.put(VentaDetalleHelper.VENTA_DET_VENTA_ID, venta.getId());
            cvDetalle.put(VentaDetalleHelper.VENTA_DET_PRODUCTO_ID, 1);
            cvDetalle.put(VentaDetalleHelper.VENTA_DET_CANTIDAD, venta.getCantidad());
            cvDetalle.put(VentaDetalleHelper.VENTA_DET_PRECIO, venta.getTotal());
            cvDetalle.put(VentaDetalleHelper.VENTA_DET_TOTAL, venta.getTotal());

            Uri newVentaDetUri = getContentResolver().insert(Uri.parse("content://com.pds.ficle.ep.venta_detalles.contentprovider/venta_detalles"), cvDetalle);

            //enviamos a caja todos los cobros, independiente del medio de pago
            Caja.RegistrarMovimientoCaja(MainActivity.this, TipoMovimientoCaja.DEPOSITO, Formatos.FormateaDecimal(venta.getTotal(), Formatos.DecimalFormat_US), venta.getMedio_pago(), "", venta.getId());

            Logger.RegistrarEvento(this, "i", "RECARGA #" + venta.getNroVenta(), "Total: " + Formatos.FormateaDecimal(venta.getTotal(), Formatos.DecimalFormat, "$"), "Medio Pago: " + venta.getMedio_pago());

        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), "Se ha producido un error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //region SECCION WIZARDS PROCESOS

    public void ProcesoRecargaInit(String descripcion) {

        //ocultamos la lista
        this.lstMenu.setVisibility(View.GONE);

        this.txtComentario_panel.setText(descripcion);
        this.txtComentario_panel.setVisibility(View.VISIBLE);

        //guardamos el producto seleccionado
        RECARGA_PRODUCTO_DESC = descripcion;
        RECARGA_PRODUCTO_OBJ = ObtieneProductoByDesc(descripcion);
        //RECARGA_IDPRODUCTO = ObtieneIdProducto(descripcion.toUpperCase());

        this.lineasLayout = null;

        EscribeLinea(descripcion.toUpperCase(), "RECARGA");

        if(RECARGA_PRODUCTO_OBJ.hayQueSolicitarNroTelefono()){
            this.recargaStep = 1;
            EscribeLinea("Ingrese " + RECARGA_PRODUCTO_OBJ.getDescIngreso() + ":", "RECARGA");
        }
        else{
            recargaStep = 12;//avanzamos directo a solicitar monto
            ProcesoRecarga();
        }

        this.recarga_btnVolver.setVisibility(View.VISIBLE);

    }

    public void ProcesoRecarga() {
        switch (recargaStep) {
            case 1: {

                String numeroIngresado = txtValor_panel.getText().toString();


                if (numeroIngresado.contains(".")) {
                    Toast.makeText(MainActivity.this, "El numero solo debe tener digitos numericos", Toast.LENGTH_LONG).show();
                } else if (numeroIngresado.length() < Integer.parseInt(RECARGA_PRODUCTO_OBJ.getMinDigitos()) ||
                        numeroIngresado.length() > Integer.parseInt(RECARGA_PRODUCTO_OBJ.getMaxDigitos())
                        ) {

                    String mensaje = "";

                    //adaptamos el mensaje segun si es un intervalo o es fija la cantidad de digitos
                    if (RECARGA_PRODUCTO_OBJ.getMinDigitos().equals(RECARGA_PRODUCTO_OBJ.getMaxDigitos())) {
                        mensaje = "El numero debe tener " + RECARGA_PRODUCTO_OBJ.getMinDigitos() + " digitos";
                    } else {
                        mensaje = "El numero debe tener entre " + RECARGA_PRODUCTO_OBJ.getMinDigitos() + " y " + RECARGA_PRODUCTO_OBJ.getMaxDigitos() + " digitos";
                    }


                    Toast.makeText(MainActivity.this, mensaje, Toast.LENGTH_LONG).show();
                    //if (numeroIngresado.length() != 10) {
                    //    Toast.makeText(MainActivity.this, "Ingrese numero de 10 digitos", Toast.LENGTH_LONG).show();
                } else {

                    RECARGA_NROTEL = numeroIngresado;

                    EscribeLinea("\t" + RECARGA_NROTEL);
                    txtValor_panel.setText("");


                    recargaStep = 12;//avanzamos rapido
                    ProcesoRecarga();
                }

            }
            break;
            case 12:{
                if (Integer.parseInt(RECARGA_PRODUCTO_OBJ.getCantMontosFijos()) > 0) {//estos productos tienen montos fijos
                    recargaStep = 3;

                    EscribeLinea("Seleccione el monto a recargar:", "RECARGA");

                    VisualizarMontosFijos();
                } else {

                    recargaStep = 2;
                    EscribeLinea("Ingrese el monto a recargar:", "RECARGA");
                }

                    /*
                    if (RECARGA_PRODUCTO_OBJ.getCodigo().equals("12") || RECARGA_IDPRODUCTO.equals("23")) { //estos productos tienen montos fijos
                        recargaStep = 3;

                        EscribeLinea("Seleccione el monto a recargar:", "RECARGA");

                        VisualizarMontosFijos();

                    } else {

                        recargaStep = 2;
                        EscribeLinea("Ingrese el monto a recargar:", "RECARGA");
                    }
                    */

            }break;
            case 2: {

                String montoIngresado = txtValor_panel.getText().toString().trim();

                //validamos el monto ingresado
                if (montoIngresado == null) {
                    Toast.makeText(MainActivity.this, "Debe ingresar un monto a recargar para continuar", Toast.LENGTH_LONG).show();
                } else if (montoIngresado.contains(".")) {
                    Toast.makeText(MainActivity.this, "El monto a ingresar debe ser entero", Toast.LENGTH_LONG).show();
                } else if (montoIngresado.length() > 5) {
                    Toast.makeText(MainActivity.this, "El monto a ingresar no puede superar $ 99999", Toast.LENGTH_LONG).show();
                } else {

                    RECARGA_MONTO = montoIngresado;

                    //EscribeLinea("\t" + "$ " + new DecimalFormat("0.00").format(Double.valueOf(montoIngresado)), "RECARGA");
                    EscribeLinea("\t" + Formatos.FormateaDecimal(montoIngresado, Formatos.DecimalFormat, "$"), "RECARGA");

                    txtValor_panel.setText("");

                    recargaStep = 4;
                    EscribeLinea("Presione OK para confirmar...", "RECARGA");

                }

            }
            break;
            case 3: {


            }
            break;
            case 4: {

                //CONTROLAMOS SI SE ESTA REALIZANDO LA MISMA RECARGA QUE LA ANTERIOR:
                if (RepiteRecargaAnterior()) {

                    //control de recarga duplicada
                    ControlRecargaDuplicada();

                } else {

                    //si pedimos el medio de pago mostramos el popup de medios:
                    if (PedirMedioPago()) {
                        //cerramos la recarga
                        dummy_view.setBackgroundColor(getResources().getColor(R.color.blur));
                        dummy_view.setVisibility(View.VISIBLE);
                        findViewById(R.id.recarga_medio_pago).setVisibility(View.VISIBLE);
                        //txtTotal_mp.setText("$ " + new DecimalFormat("0.00").format(Double.valueOf(RECARGA_MONTO))); //seteamos el total para el panel de medio de pago
                        txtTotal_mp.setText(Formatos.FormateaDecimal(RECARGA_MONTO, Formatos.DecimalFormat, "$"));

                        //seteamos por default en EFECTIVO
                        rbtMedios_mp.check(R.id.medio_pago_rbt_efvo);

                        recargaStep = 4;
                    } else {
                        //sino avanzamos directo al envio de la recarga con el medio de pago por default
                        Recarga();
                    }
                }

            }
            break;
            case 6: {

                //REINTENTO REENVIO RECARGA RECHAZADA
                AlertDialog c = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Reenvio de Recarga")
                        .setMessage("Esta por enviar una nueva Recarga al servidor. Esta Ud. seguro?")
                        .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Recarga();
                            }

                        })
                        .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                btnCancelar.performClick();
                            }
                        })
                        .create();
                c.setCancelable(false);
                c.setCanceledOnTouchOutside(false);
                c.show();
            }
            break;
            default:
                btnCancelar.performClick();
                break;
        }

    }

    public boolean RepiteRecargaAnterior() {

        if (lastRecarga != null) {
            //CONTROLAMOS SI SE ESTA REALIZANDO LA MISMA RECARGA QUE LA ANTERIOR:
            if (lastRecarga.getTipoProductoConsumido().equals(RECARGA_PRODUCTO_OBJ.getCodigo()) &&
                    lastRecarga.getMontoTransaccion().equals(RECARGA_MONTO)
                    ) {

                if (!RECARGA_PRODUCTO_OBJ.getPrefijo().equals("")) {
                    //si tiene prefijo se lo sacamos al numero de la recarga anterior
                    String numeroSinPrefijo = lastRecarga.getNroTarjeta().replaceFirst(RECARGA_PRODUCTO_OBJ.getPrefijo(), "");

                    //comparamos contra el numero sin prefijo
                    return numeroSinPrefijo.equals(RECARGA_NROTEL);
                } else {
                    //como no tiene prefijo, comparamos directamente
                    return lastRecarga.getNroTarjeta().equals(RECARGA_NROTEL);
                }

            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    public void ControlRecargaDuplicada() {

        AlertDialog b = new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Control de Recargas")
                .setMessage("Los datos ingresados son iguales a los de la Recarga anterior. Desea consultar la recarga anterior?")
                .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //RECARGA DUPLICADA
                        recargaStep = 5;
                        ConsultaUltimaRecarga();
                    }

                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //creamos la consulta para enviar recarga
                        AlertDialog a = new AlertDialog.Builder(MainActivity.this)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setTitle("Control de Recargas")
                                .setMessage("Desea realizar una recarga nueva con los datos ingresados?")
                                .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Recarga();
                                    }

                                })
                                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        recargaStep = 5;
                                        btnCancelar.performClick();
                                    }
                                })
                                .create();

                        a.setCancelable(false);
                        a.setCanceledOnTouchOutside(false);

                        a.show();


                    }
                })
                .create();

        b.setCancelable(false);
        b.setCanceledOnTouchOutside(false);
        b.show();

    }

    public boolean PedirMedioPago() {
        String pedirPagoPref = Preferences.getString(MainActivity.this, Preferences.KEY_PEDIR_PAGO, "X");

        if (!pedirPagoPref.equals("X")) {

            RECARGA_MEDIOPAGO = pedirPagoPref;

            return false;
        } else
            return true;
    }


    public void ProcesoVentaAbonoInit() {

        //ocultamos la lista
        this.lstMenu.setVisibility(View.GONE);

        this.txtComentario_panel.setText("VENTA ABONO");
        this.txtComentario_panel.setVisibility(View.VISIBLE);

        this.ventaAbonoStep = 1;
        this.lineasLayout = null;

        this.recarga_btnVolver.setVisibility(View.VISIBLE);

        EscribeLinea("Ingrese el monto a recargar:", "VENTA ABONO");
    }

    public void ProcesoVentaAbono() {
        switch (ventaAbonoStep) {
            case 1: {
                String montoIngresado = txtValor_panel.getText().toString().trim();

                //validamos el monto ingresado
                if (montoIngresado == null || montoIngresado.equals("")) {
                    Toast.makeText(MainActivity.this, "Debe ingresar un monto para continuar", Toast.LENGTH_LONG).show();
                } else if (montoIngresado.contains(".")) {
                    Toast.makeText(MainActivity.this, "El monto a ingresar debe ser entero", Toast.LENGTH_LONG).show();
                } else if (montoIngresado.length() > 5) {
                    Toast.makeText(MainActivity.this, "El monto a ingresar no puede superar 5 caracteres", Toast.LENGTH_LONG).show();
                } else if (!Formatos.isNumeric(montoIngresado)) {
                    Toast.makeText(MainActivity.this, "El monto a ingresar debe ser numerico", Toast.LENGTH_LONG).show();
                } else {

                    VENTAABONO_MONTO = montoIngresado;

                    EscribeLinea("\t" + Formatos.FormateaDecimal(VENTAABONO_MONTO, Formatos.DecimalFormat, "$"), "VENTA ABONO");

                    txtValor_panel.setText("");

                    EscribeLinea("Seleccione tipo de pago:", "VENTA ABONO");

                    //cerramos la recarga
                    dummy_view.setBackgroundColor(getResources().getColor(R.color.blur));
                    dummy_view.setVisibility(View.VISIBLE);
                    findViewById(R.id.recarga_medio_pago).setVisibility(View.VISIBLE);

                    txtTotal_mp.setText(Formatos.FormateaDecimal(VENTAABONO_MONTO, Formatos.DecimalFormat, "$"));

                    //seteamos por default en EFECTIVO
                    rbtMedios_mp.check(R.id.medio_pago_rbt_efvo);

                    ventaAbonoStep = 2;
                }
            }
            break;
            case 2: {
                EscribeLinea("\t" + VENTAABONO_TIPOPAGO, "VENTA ABONO");

                EscribeLinea("Ingrese el nro. de factura:", "VENTA ABONO");

                ventaAbonoStep = 3;

            }
            break;
            case 3: {
                String numeroIngresado = txtValor_panel.getText().toString().trim();

                //validamos el numero ingresado
                if (numeroIngresado == null || numeroIngresado.equals("")) {
                    Toast.makeText(MainActivity.this, "Debe ingresar un numero de FC para continuar", Toast.LENGTH_LONG).show();
                } else if (numeroIngresado.length() > 10) {
                    Toast.makeText(MainActivity.this, "El numero ingresado no puede superar los 10 digitos", Toast.LENGTH_LONG).show();
                } else if (!Formatos.isNumeric(numeroIngresado)) {
                    Toast.makeText(MainActivity.this, "Solo debe ingresar numeros, sin guiones ni letras", Toast.LENGTH_LONG).show();
                } else {

                    VENTAABONO_NROFACTURA = numeroIngresado;

                    EscribeLinea("\t" + VENTAABONO_NROFACTURA, "VENTA ABONO");

                    EscribeLinea("Ingrese el nro. de vendedor:", "VENTA ABONO");

                    txtValor_panel.setText("");

                    ventaAbonoStep = 4;
                }
            }
            break;
            case 4: {
                String numeroIngresado = txtValor_panel.getText().toString().trim();

                //validamos el numero ingresado
                if (numeroIngresado == null || numeroIngresado.equals("")) {
                    Toast.makeText(MainActivity.this, "Debe ingresar un numero de vendedor para continuar", Toast.LENGTH_LONG).show();
                } else if (numeroIngresado.length() > 4) {
                    Toast.makeText(MainActivity.this, "El numero no puede superar 4 caracteres", Toast.LENGTH_LONG).show();
                } else if (!Formatos.isNumeric(numeroIngresado)) {
                    Toast.makeText(MainActivity.this, "El valor ingresado debe ser numerico", Toast.LENGTH_LONG).show();
                } else {

                    VENTAABONO_NROVENDEDOR = numeroIngresado;

                    EscribeLinea("\t" + VENTAABONO_NROVENDEDOR, "VENTA ABONO");

                    txtValor_panel.setText("");

                    EscribeLinea("Ingrese su PIN:", "VENTA ABONO");

                    ventaAbonoStep = 5;
                }
            }
            break;

            case 5: {
                String pinIngresado = txtValor_panel.getText().toString().trim();

                //validamos el pin ingresado
                if (pinIngresado == null || pinIngresado.equals("")) {
                    Toast.makeText(MainActivity.this, "Debe ingresar su numero de PIN para continuar", Toast.LENGTH_LONG).show();
                } else if (pinIngresado.length() > 4) {
                    Toast.makeText(MainActivity.this, "El numero de PIN no puede superar 4 caracteres", Toast.LENGTH_LONG).show();
                } else if (!Formatos.isNumeric(pinIngresado)) {
                    Toast.makeText(MainActivity.this, "El valor ingresado debe ser numerico", Toast.LENGTH_LONG).show();
                } else {

                    VENTAABONO_NROPIN = pinIngresado;

                    EscribeLinea("\t" + pinIngresado.replaceAll(".", "*"), "VENTA ABONO");

                    txtValor_panel.setText("");

                    EnviaMensajeConsultaComercial("ProcesoVentaAbono", "");

                    ventaAbonoStep = 6;
                }
            }
            break;
            case 6: {

                if (lastmensajeRecarga.getProductos().size() > 0)
                    VENTAABONO_SALDOANT = Formatos.FormateaDecimal(lastmensajeRecarga.getProductos().get(0).getSaldoProducto(), Formatos.DecimalFormat, "$");
                else
                    VENTAABONO_SALDOANT = "$ 0";

                EscribeLinea("@C@@B@Confirmación del cliente", "VENTA ABONO");

                EscribeLinea("Saldo actual: " + VENTAABONO_SALDOANT, "VENTA ABONO");

                EscribeLinea("Abono adquirido: " + Formatos.FormateaDecimal(VENTAABONO_MONTO, Formatos.DecimalFormat, "$"), "VENTA ABONO");

                EscribeLinea("Ingrese clave para confirmar...", "VENTA ABONO");

                ventaAbonoStep = 7;

            }
            break;
            case 7: {

                String claveIngresada = txtValor_panel.getText().toString().trim();

                //validamos el pin ingresado
                if (claveIngresada == null || claveIngresada.equals("")) {
                    Toast.makeText(MainActivity.this, "Debe ingresar su clave para continuar", Toast.LENGTH_LONG).show();
                } else {

                    if (claveIngresada.equals(this._user.getPassword())) {
                        txtValor_panel.setText("");

                        EnviaMensajeVentaAbonoComercio("VentaAbonoResponse");

                    } else {
                        Toast.makeText(MainActivity.this, "Clave de usuario incorrecta", Toast.LENGTH_LONG).show();
                    }
                }
            }
            break;
            default:
                btnCancelar.performClick();
                break;
        }
    }

    public void ProcesoVisitaVendedorInit() {
        //ocultamos la lista
        this.lstMenu.setVisibility(View.GONE);

        this.txtComentario_panel.setText("VISITA VENDEDOR");
        this.txtComentario_panel.setVisibility(View.VISIBLE);

        this.visitaVendStep = 1;
        this.lineasLayout = null;

        this.recarga_btnVolver.setVisibility(View.VISIBLE);

        EscribeLinea("Ingrese el nro. de vendedor:", "VISITA VENDEDOR");
    }

    public void ProcesoVisitaVendedor() {
        switch (visitaVendStep) {
            case 1: {
                String numeroIngresado = txtValor_panel.getText().toString().trim();

                //validamos el numero ingresado
                if (numeroIngresado == null || numeroIngresado.equals("")) {
                    Toast.makeText(MainActivity.this, "Debe ingresar un numero de vendedor para continuar", Toast.LENGTH_LONG).show();
                } else if (numeroIngresado.length() > 4) {
                    Toast.makeText(MainActivity.this, "El numero no puede superar 4 caracteres", Toast.LENGTH_LONG).show();
                } else if (!Formatos.isNumeric(numeroIngresado)) {
                    Toast.makeText(MainActivity.this, "El valor ingresado debe ser numerico", Toast.LENGTH_LONG).show();
                } else {

                    REGISTRO_NROVENDEDOR = numeroIngresado;

                    EscribeLinea("\t" + REGISTRO_NROVENDEDOR, "VISITA VENDEDOR");

                    txtValor_panel.setText("");

                    EscribeLinea("Ingrese su PIN:", "VISITA VENDEDOR");

                    visitaVendStep = 2;
                }
            }
            break;

            case 2: {
                String pinIngresado = txtValor_panel.getText().toString().trim();

                //validamos el pin ingresado
                if (pinIngresado == null || pinIngresado.equals("")) {
                    Toast.makeText(MainActivity.this, "Debe ingresar su numero de PIN para continuar", Toast.LENGTH_LONG).show();
                } else if (pinIngresado.length() > 4) {
                    Toast.makeText(MainActivity.this, "El numero de PIN no puede superar 4 caracteres", Toast.LENGTH_LONG).show();
                } else if (!Formatos.isNumeric(pinIngresado)) {
                    Toast.makeText(MainActivity.this, "El valor ingresado debe ser numerico", Toast.LENGTH_LONG).show();
                } else {

                    REGISTRO_NROPIN = pinIngresado;

                    //EscribeLinea("\t" + REGISTRO_NROPIN, "VISITA VENDEDOR");
                    EscribeLinea("\t" + pinIngresado.replaceAll(".", "*"), "VISITA VENDEDOR");

                    txtValor_panel.setText("");

                    EscribeLinea("Presione OK para confirmar...", "VISITA VENDEDOR");

                    visitaVendStep = 3;
                }
            }
            break;
            case 3: {
                EnviaMensajeRegistroVisitaVendedor("RegistroVisitaVendedorResponse");
            }
            break;
            default:
                btnCancelar.performClick();
                break;
        }
    }

    public void ProcesoCierreTurnoInit() {
        //limpiamos variables
        CIERRE_FECHA_INICIO = null;
        CIERRE_FECHA_CIERRE = null;

        //ocultamos la lista
        this.lstMenu.setVisibility(View.GONE);

        this.txtComentario_panel.setText("CIERRE DE TURNO");
        this.txtComentario_panel.setVisibility(View.VISIBLE);

        this.cierreTurnoStep = 1;
        this.lineasLayout = null;

        this.recarga_btnVolver.setVisibility(View.VISIBLE);

        EscribeLinea("Ingrese Fecha Inicio del Turno:", "CIERRE DE TURNO");
    }

    public void ProcesoCierreTurno() {
        switch (cierreTurnoStep) {
            case 1: {

                String fechaIngresada = txtValor_panel.getText().toString().trim();

                //validamos la fecha ingresada
                if (fechaIngresada == null || fechaIngresada.equals("")) {
                    Toast.makeText(MainActivity.this, "Debe ingresar la fecha de inicio para continuar", Toast.LENGTH_LONG).show();
                } else if (fechaIngresada.length() != 4 || !Formatos.isNumeric(fechaIngresada)) {
                    Toast.makeText(MainActivity.this, "Debe ingresar dia (2 digitos) y mes (2 digitos)", Toast.LENGTH_LONG).show();
                } else if (Integer.parseInt(fechaIngresada.substring(0, 2)) > 31 || Integer.parseInt(fechaIngresada.substring(0, 2)) < 1) {
                    Toast.makeText(MainActivity.this, "Día ingresado no válido", Toast.LENGTH_LONG).show();
                } else if (Integer.parseInt(fechaIngresada.substring(2, 4)) > 12 || Integer.parseInt(fechaIngresada.substring(2, 4)) < 1) {
                    Toast.makeText(MainActivity.this, "Mes ingresado no válido", Toast.LENGTH_LONG).show();
                } else {

                    String s_FechaInicio = Utils.Util_FechaTerminal(fechaIngresada.substring(0, 2), fechaIngresada.substring(2, 4));

                    Date fechaInicio = Utils.Util_Fecha(s_FechaInicio);

                    if (fechaInicio.compareTo(new Date()) > 0) {
                        Toast.makeText(MainActivity.this, "La Fecha de inicio no puede superar la Fecha actual", Toast.LENGTH_LONG).show();
                    } else {

                        CIERRE_FECHA_INICIO = s_FechaInicio;

                        EscribeLinea("\t" + fechaIngresada.substring(0, 2) + "/" + fechaIngresada.substring(2, 4), "CIERRE DE TURNO");

                        txtValor_panel.setText("");

                        EscribeLinea("Ingrese Hora Inicio del Turno:", "CIERRE DE TURNO");

                        cierreTurnoStep = 2;
                    }
                }

            }
            break;
            case 2: {

                String horaIngresada = txtValor_panel.getText().toString().trim();

                //validamos la hora ingresada
                if (horaIngresada == null || horaIngresada.equals("")) {
                    Toast.makeText(MainActivity.this, "Debe ingresar un horario para continuar", Toast.LENGTH_LONG).show();
                } else if (horaIngresada.length() != 4 || !Formatos.isNumeric(horaIngresada)) {
                    Toast.makeText(MainActivity.this, "Debe ingresar hora (2 digitos) y minutos (2 digitos)", Toast.LENGTH_LONG).show();
                } else if (Integer.parseInt(horaIngresada.substring(0, 2)) > 24) {
                    Toast.makeText(MainActivity.this, "Hora ingresada no valida", Toast.LENGTH_LONG).show();
                } else if (Integer.parseInt(horaIngresada.substring(2, 4)) > 59) {
                    Toast.makeText(MainActivity.this, "Minutos ingresados no validos", Toast.LENGTH_LONG).show();
                } else {

                    String s_fechaHoraInicio = CIERRE_FECHA_INICIO + "T" + horaIngresada.substring(0, 2) + ":" + horaIngresada.substring(2, 4) + ":00";

                    Date fechaHoraInicio = Utils.Util_FechaHora(s_fechaHoraInicio);
                    Date fechaActual = new Date();

                    //no se puede superar en 120 hs
                    Calendar cal = Calendar.getInstance();
                    cal.add(Calendar.HOUR, 120);

                    //validamos que la fecha/hora de inicio no supere la actual
                    if (fechaHoraInicio.compareTo(fechaActual) > 0) {
                        Toast.makeText(MainActivity.this, "La Fecha y Hora de inicio no puede superar la Fecha y Hora actual", Toast.LENGTH_LONG).show();
                    } else if (fechaHoraInicio.compareTo(cal.getTime()) > 0) {
                        Toast.makeText(MainActivity.this, "La Fecha y Hora de inicio no puede superar las 120 horas", Toast.LENGTH_LONG).show();
                    } else {

                        CIERRE_FECHA_INICIO = s_fechaHoraInicio;

                        EscribeLinea("\t" + horaIngresada.substring(0, 2) + ":" + horaIngresada.substring(2, 4), "CIERRE DE TURNO");

                        txtValor_panel.setText("");

                        CIERRE_FECHA_CIERRE = Utils.Util_FechaHoraTerminal(fechaActual);

                        EscribeLinea("Fecha y Hora de Fin del Turno:", "CIERRE DE TURNO");

                        EscribeLinea("\t" + Formatos.FormateaDate(fechaActual, Formatos.DayMonthFormat) + " " + Formatos.FormateaDate(fechaActual, Formatos.ShortTimeFormat), "CIERRE DE TURNO");

                        EscribeLinea("Presione OK para confirmar...", "CIERRE DE TURNO");

                        cierreTurnoStep = 3;
                    }
                }

            }
            break;
            case 3: {
                ConsultaVentas("CIERRE_TURNO");
            }
            break;
            default:
                btnCancelar.performClick();
                break;
            /*case 3: {

                String horaIngresada = txtValor_panel.getText().toString().trim();

                //validamos la hora ingresada
                if (horaIngresada == null || horaIngresada.equals("")) {
                    Toast.makeText(MainActivity.this, "Debe ingresar un horario para continuar", Toast.LENGTH_LONG).show();
                } else if (horaIngresada.length() != 4 || !Formatos.isNumeric(horaIngresada)) {
                    Toast.makeText(MainActivity.this, "Debe ingresar hora (2 digitos) y minutos (2 digitos)", Toast.LENGTH_LONG).show();
                } else if (Integer.parseInt(horaIngresada.substring(0, 2)) > 24) {
                    Toast.makeText(MainActivity.this, "Hora ingresada no valida", Toast.LENGTH_LONG).show();
                } else if (Integer.parseInt(horaIngresada.substring(2, 4)) > 59) {
                    Toast.makeText(MainActivity.this, "Minutos ingresados no validos", Toast.LENGTH_LONG).show();
                } else if (Integer.parseInt(CIERRE_FECHA_INICIO.substring(0, 2)) > Integer.parseInt(horaIngresada.substring(0, 2))) {
                    Toast.makeText(MainActivity.this, "Hora cierre ingresada debe ser superior a la hora de inicio", Toast.LENGTH_LONG).show();
                } else if (Integer.parseInt(CIERRE_FECHA_INICIO.substring(0, 2)) == Integer.parseInt(horaIngresada.substring(0, 2)) &&
                        Integer.parseInt(CIERRE_FECHA_INICIO.substring(2, 4)) >= Integer.parseInt(horaIngresada.substring(2, 4))) {
                    Toast.makeText(MainActivity.this, "Hora cierre ingresada debe ser superior a la hora de inicio", Toast.LENGTH_LONG).show();
                } else {

                    CIERRE_FECHA_CIERRE = horaIngresada;

                    EscribeLinea("\t" + horaIngresada.substring(0, 2) + ":" + horaIngresada.substring(2, 4), "CIERRE DE TURNO");

                    txtValor_panel.setText("");

                    EscribeLinea("Presione OK para confirmar...", "CIERRE DE TURNO");

                    cierreTurnoStep = 3;
                }

            }
            break;*/

        }
    }

    public void ProcesoAvisoDepositoVendInit() {
        ProcesoAvisoDepositoInit("VEN");
    }

    public void ProcesoAvisoDepositoInit(String tipo) {

        //limpiamos todas las variables
        DEPOSITO_CLI_VEND = null;
        DEPOSITO_BANCO = null;
        DEPOSITO_TIPO = null;
        DEPOSITO_NUMERO = null;
        DEPOSITO_MONTO = null;
        DEPOSITO_CHEQUE = null;
        DEPOSITO_BANCO_CHEQUE = null;
        DEPOSITO_FECHA = null;
        DEPOSITO_FECHA_DISPLAY = null;
        DEPOSITO_NROVENDEDOR = null;
        DEPOSITO_NROPIN = null;

        //asignamos el tipo: si es AVISO CLIENTE o AVISO DEPOSITO
        DEPOSITO_CLI_VEND = tipo;

        //ocultamos la lista
        this.lstMenu.setVisibility(View.GONE);

        this.txtComentario_panel.setText("AVISO DEPOSITO");
        this.txtComentario_panel.setVisibility(View.VISIBLE);

        this.avisoDepoStep = 1;
        this.lineasLayout = null;

        this.recarga_btnVolver.setVisibility(View.VISIBLE);

        if (DEPOSITO_CLI_VEND.equals("VEN")) {
            this.avisoDepoStep = 100;

            EscribeLinea("Ingrese codigo de vendedor:", "AVISO DEPOSITO");

        } else {
            this.avisoDepoStep = 0;

            ProcesoAvisoDeposito();
        }

    }

    public void ProcesoAvisoDeposito() {
        switch (avisoDepoStep) {
            case 100: {

                if (DEPOSITO_CLI_VEND.equals("VEN")) {
                    String numeroIngresado = txtValor_panel.getText().toString().trim();

                    //validamos el numero ingresado
                    if (numeroIngresado == null || numeroIngresado.equals("")) {
                        Toast.makeText(MainActivity.this, "Debe ingresar un codigo de vendedor para continuar", Toast.LENGTH_LONG).show();
                    } else if (numeroIngresado.length() > 4) {
                        Toast.makeText(MainActivity.this, "El codigo no puede superar 4 caracteres", Toast.LENGTH_LONG).show();
                    } else if (!Formatos.isNumeric(numeroIngresado)) {
                        Toast.makeText(MainActivity.this, "El valor ingresado debe ser numerico", Toast.LENGTH_LONG).show();
                    } else {

                        DEPOSITO_NROVENDEDOR = numeroIngresado;

                        EscribeLinea("\t" + DEPOSITO_NROVENDEDOR, "AVISO DEPOSITO");

                        txtValor_panel.setText("");

                        this.avisoDepoStep = 1;
                    }

                }
            }
            //break; //esta omitido a proposito
            case 0: {
                EscribeLinea("Seleccione Propósito de Deposito:", "AVISO DEPOSITO");

                List<ResultadoMenu> propositosDeposito = new ArrayList<ResultadoMenu>();

                if(listadoPropositoDeposito != null){
                    for(PropDepDAVISORecarga prop : listadoPropositoDeposito){
                        propositosDeposito.add(new ResultadoMenu(prop.getDesc(), prop.getCodigo(), null, -1));
                    }
                }

                this.recarga_lstResultados.setAdapter(new ResultadosImgMenuAdapter(MainActivity.this, propositosDeposito));
                this.recarga_lstResultados.setVisibility(View.VISIBLE);
                this.recarga_lstResultados.setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        ResultadoMenu item = ((ResultadosImgMenuAdapter) recarga_lstResultados.getAdapter()).datos.get(position);

                        //DEPOSITO_BANCO = item.get_subdescripcion();

                        EscribeLinea("\t" + item.get_descripcion(), "AVISO DEPOSITO");

                        avisoDepoStep = 1;

                        recarga_lstResultados.setVisibility(View.GONE);

                        ProcesoAvisoDeposito();
                    }
                });

            }
            break;
            case 1: {
                EscribeLinea("Seleccione Lugar de Deposito:", "AVISO DEPOSITO");

                List<ResultadoMenu> lugaresDeposito = new ArrayList<ResultadoMenu>();
                //lugaresDeposito.add(new ResultadoMenu("BCI", "BBCI", null, R.drawable.logo_bci));
                //lugaresDeposito.add(new ResultadoMenu("BANCO DE CHILE", "BCHI", null, R.drawable.logo_banco_de_chile));
                //lugaresDeposito.add(new ResultadoMenu("BANCO ESTADO", "BCES", null, R.drawable.logo_banco_estado));
                //lugaresDeposito.add(new ResultadoMenu("BANCO SANTANDER", "BCSA", null, R.drawable.logo_santander));
                //lugaresDeposito.add(new ResultadoMenu("BANCO CAJA CHILE", "BCJA", null, R.drawable.logo_cajachile));

                if(listadoBancos != null){
                    for(BancoBNCRecarga banco : listadoBancos){
                        if(banco.getDepCli().equalsIgnoreCase("S"))
                            lugaresDeposito.add(new ResultadoMenu(banco.getDesc(), banco.getCodigo(), null, -1));
                    }
                }


                this.recarga_lstResultados.setAdapter(new ResultadosImgMenuAdapter(MainActivity.this, lugaresDeposito));
                this.recarga_lstResultados.setVisibility(View.VISIBLE);
                this.recarga_lstResultados.setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        ResultadoMenu item = ((ResultadosImgMenuAdapter) recarga_lstResultados.getAdapter()).datos.get(position);

                        DEPOSITO_BANCO = item.get_subdescripcion();

                        EscribeLinea("\t" + item.get_descripcion(), "AVISO DEPOSITO");

                        avisoDepoStep = 2;

                        recarga_lstResultados.setVisibility(View.GONE);

                        ProcesoAvisoDeposito();
                    }
                });

            }
            break;
            case 2: {

                EscribeLinea("Seleccione Tipo de Deposito:", "AVISO DEPOSITO");

                List<ResultadoMenu> tipoDeposito = new ArrayList<ResultadoMenu>();
                tipoDeposito.add(new ResultadoMenu("DEPOSITO EFECTIVO", "EFE", null, -1));
                tipoDeposito.add(new ResultadoMenu("DEPOSITO CON CHEQUE", "CHE", null, -1));
                tipoDeposito.add(new ResultadoMenu("TRANSFERENCIA", "TRA", null, -1));

                this.recarga_lstResultados.setAdapter(new ResultadosImgMenuAdapter(MainActivity.this, tipoDeposito));
                this.recarga_lstResultados.setVisibility(View.VISIBLE);
                this.recarga_lstResultados.setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        ResultadoMenu item = ((ResultadosImgMenuAdapter) recarga_lstResultados.getAdapter()).datos.get(position);

                        DEPOSITO_TIPO = item.get_subdescripcion();

                        EscribeLinea("\t" + item.get_descripcion(), "AVISO DEPOSITO");

                        avisoDepoStep = 3;

                        recarga_lstResultados.setVisibility(View.GONE);

                        ProcesoAvisoDeposito();
                    }
                });
            }
            break;
            case 3: {

                if (DEPOSITO_TIPO.equals("EFE") || DEPOSITO_TIPO.equals("CHE")) {
                    EscribeLinea("Ingrese el Nro. de Deposito:", "AVISO DEPOSITO");

                    avisoDepoStep = 31;
                }

                if (DEPOSITO_TIPO.equals("TRA")) {
                    EscribeLinea("Ingrese el Nro. de Operacion:", "AVISO DEPOSITO");

                    avisoDepoStep = 41;
                }

            }
            break;
            case 31: {

                String numeroIngresado = txtValor_panel.getText().toString().trim();

                //validamos el codigo ingresado
                if (numeroIngresado == null || numeroIngresado.equals("")) {
                    Toast.makeText(MainActivity.this, "Debe ingresar un numero de deposito para continuar", Toast.LENGTH_LONG).show();
                } else if (numeroIngresado.length() > 12) {
                    Toast.makeText(MainActivity.this, "El numero ingresado no puede superar 12 caracteres", Toast.LENGTH_LONG).show();
                } else if (!Formatos.isNumeric(numeroIngresado)) {
                    Toast.makeText(MainActivity.this, "Solo debe ingresar numeros, sin guiones ni letras", Toast.LENGTH_LONG).show();
                } else {
                    DEPOSITO_NUMERO = numeroIngresado;

                    EscribeLinea("\t" + DEPOSITO_NUMERO, "AVISO DEPOSITO");

                    txtValor_panel.setText("");

                    EscribeLinea("Confirme el Nro. de Deposito:", "AVISO DEPOSITO");

                    avisoDepoStep = 32;
                }
            }
            break;
            case 32: {

                String numeroIngresado = txtValor_panel.getText().toString().trim();

                //validamos el codigo ingresado
                if (numeroIngresado == null || numeroIngresado.equals("")) {
                    Toast.makeText(MainActivity.this, "Debe ingresar un numero de deposito para continuar", Toast.LENGTH_LONG).show();
                } else if (numeroIngresado.length() > 12) {
                    Toast.makeText(MainActivity.this, "El numero ingresado no puede superar 12 caracteres", Toast.LENGTH_LONG).show();
                } else if (!Formatos.isNumeric(numeroIngresado)) {
                    Toast.makeText(MainActivity.this, "Solo debe ingresar numeros, sin guiones ni letras", Toast.LENGTH_LONG).show();
                } else {
                    if (numeroIngresado.equals(DEPOSITO_NUMERO)) {

                        avisoDepoStep = 5;

                        this.lineasLayout.remove(this.lineasLayout.size() - 1);

                        txtValor_panel.setText("");

                        btnEnter.performClick();

                    } else {
                        Toast.makeText(MainActivity.this, "El numero de deposito no coincide", Toast.LENGTH_LONG).show();
                    }
                }

            }
            break;
            case 41: {

                String numeroIngresado = txtValor_panel.getText().toString().trim();

                //validamos el codigo ingresado
                if (numeroIngresado == null || numeroIngresado.equals("")) {
                    Toast.makeText(MainActivity.this, "Debe ingresar un numero de operacion para continuar", Toast.LENGTH_LONG).show();
                } else if (numeroIngresado.length() > 12) {
                    Toast.makeText(MainActivity.this, "El numero ingresado no puede superar 12 caracteres", Toast.LENGTH_LONG).show();
                } else if (!Formatos.isNumeric(numeroIngresado)) {
                    Toast.makeText(MainActivity.this, "Solo debe ingresar numeros, sin guiones ni letras", Toast.LENGTH_LONG).show();
                } else {
                    DEPOSITO_NUMERO = numeroIngresado;

                    EscribeLinea("\t" + DEPOSITO_NUMERO, "AVISO DEPOSITO");

                    txtValor_panel.setText("");

                    EscribeLinea("Confirme el Nro. de Operacion:", "AVISO DEPOSITO");

                    avisoDepoStep = 42;
                }
            }
            break;
            case 42: {

                String numeroIngresado = txtValor_panel.getText().toString().trim();

                //validamos el codigo ingresado
                if (numeroIngresado == null || numeroIngresado.equals("")) {
                    Toast.makeText(MainActivity.this, "Debe ingresar un numero de operacion para continuar", Toast.LENGTH_LONG).show();
                } else if (numeroIngresado.length() > 12) {
                    Toast.makeText(MainActivity.this, "El numero ingresado no puede superar 12 caracteres", Toast.LENGTH_LONG).show();
                } else if (!Formatos.isNumeric(numeroIngresado)) {
                    Toast.makeText(MainActivity.this, "Solo debe ingresar numeros, sin guiones ni letras", Toast.LENGTH_LONG).show();
                } else {
                    if (numeroIngresado.equals(DEPOSITO_NUMERO)) {

                        avisoDepoStep = 5;

                        this.lineasLayout.remove(this.lineasLayout.size() - 1);

                        txtValor_panel.setText("");

                        btnEnter.performClick();

                    } else {
                        Toast.makeText(MainActivity.this, "El numero de operacion no coincide", Toast.LENGTH_LONG).show();
                    }
                }

            }
            break;
            case 5: {
                EscribeLinea("Ingrese el Monto:", "AVISO DEPOSITO");

                avisoDepoStep = 6;

            }
            break;
            case 6: {

                String montoIngresado = txtValor_panel.getText().toString().trim();

                //validamos el monto ingresado
                if (montoIngresado == null || montoIngresado.equals("")) {
                    Toast.makeText(MainActivity.this, "Debe ingresar un monto para continuar", Toast.LENGTH_LONG).show();
                } else if (montoIngresado.contains(".")) {
                    Toast.makeText(MainActivity.this, "El monto a ingresar debe ser entero", Toast.LENGTH_LONG).show();
                } else if (montoIngresado.length() > 8) {
                    Toast.makeText(MainActivity.this, "El monto a ingresar no puede superar 8 caracteres", Toast.LENGTH_LONG).show();
                } else if (!Formatos.isNumeric(montoIngresado)) {
                    Toast.makeText(MainActivity.this, "El monto a ingresar debe ser numerico", Toast.LENGTH_LONG).show();
                } else {

                    String montoValida = "    " + montoIngresado;

                    if (DEPOSITO_CLI_VEND.equals("VEN")) {

                        /* 28-04-13 se quita la validacion solicitado por Veronica

                        //si es VENDEDOR tengo que validar para EFE y TRA
                        //que los ultimos 3 digitos del monto coincidan
                        //con el cod de vendedor

                        montoValida = montoValida.substring(montoValida.length() - 3);

                        if (DEPOSITO_TIPO.equals("CHE") || DEPOSITO_NROVENDEDOR.endsWith(montoValida)) {
                        */
                        DEPOSITO_MONTO = montoIngresado;

                        EscribeLinea("\t" + Formatos.FormateaDecimal(DEPOSITO_MONTO, Formatos.DecimalFormat, "$"), "AVISO DEPOSITO");

                        txtValor_panel.setText("");

                        EscribeLinea("Ingrese dia, hora y minutos del deposito:", "AVISO DEPOSITO");

                        //showDateTimeDialog();
                        //showDialog(DATE_DIALOG_ID);
                        showDatePickerDialog();

                        avisoDepoStep = 7;

                        /*
                        } else {
                            Toast.makeText(MainActivity.this, "Los ultimos 3 digitos del monto que ingresó difiere de los últimos 3 números de su Cod.Vendedor", Toast.LENGTH_LONG).show();
                        }
                        */

                    } else {
                        //si es CLIENTE tengo que validar
                        //que los ultimos 4 digitos del monto coincidan
                        //con el cod cliente externo
                        /* 11-08-14 se quita la validacion solicitado por Veronica
                        montoValida = montoValida.substring(montoValida.length() - 4);


                        if (this.IDExtCliente.endsWith(montoValida)) {

                            DEPOSITO_MONTO = montoIngresado;

                            EscribeLinea("\t" + Formatos.FormateaDecimal(DEPOSITO_MONTO, Formatos.DecimalFormat, "$"), "AVISO DEPOSITO");

                            txtValor_panel.setText("");

                            EscribeLinea("Ingrese dia, hora y minutos del deposito:", "AVISO DEPOSITO");

                            //showDateTimeDialog();
                            //showDialog(DATE_DIALOG_ID);
                            showDatePickerDialog();

                            avisoDepoStep = 7;


                        } else {
                            Toast.makeText(MainActivity.this, "Los ultimos 4 digitos del monto que ingresó difiere de los últimos 4 números de su Cod.Cliente", Toast.LENGTH_LONG).show();
                        }
                        */

                        DEPOSITO_MONTO = montoIngresado;

                        EscribeLinea("\t" + Formatos.FormateaDecimal(DEPOSITO_MONTO, Formatos.DecimalFormat, "$"), "AVISO DEPOSITO");

                        txtValor_panel.setText("");

                        EscribeLinea("Ingrese dia, hora y minutos del deposito:", "AVISO DEPOSITO");

                        showDatePickerDialog();

                        avisoDepoStep = 7;
                    }


                }

            }
            break;
            case 7: {

                String horaIngresada = txtValor_panel.getText().toString().trim();

                //validamos la hora ingresada
                if (horaIngresada == null || horaIngresada.equals("")) {
                    Toast.makeText(MainActivity.this, "Debe ingresar un horario para continuar", Toast.LENGTH_LONG).show();
                } else if (horaIngresada.length() != 4 || !Formatos.isNumeric(horaIngresada)) {
                    Toast.makeText(MainActivity.this, "Debe ingresar hora (2 digitos) y minutos (2 digitos)", Toast.LENGTH_LONG).show();
                } else if (Integer.parseInt(horaIngresada.substring(0, 2)) > 24) {
                    Toast.makeText(MainActivity.this, "Hora ingresada no valida", Toast.LENGTH_LONG).show();
                } else if (Integer.parseInt(horaIngresada.substring(2, 4)) > 59) {
                    Toast.makeText(MainActivity.this, "Minutos ingresados no validos", Toast.LENGTH_LONG).show();
                } else {

                    //DEPOSITO_FECHA  = "2014-03-19T14:20:00";
                    DEPOSITO_FECHA = DEPOSITO_FECHA + "T" + horaIngresada.substring(0, 2) + ":" + horaIngresada.substring(2, 4) + ":00";
                    DEPOSITO_FECHA_DISPLAY = DEPOSITO_FECHA_DISPLAY + "  " + horaIngresada.substring(0, 2) + ":" + horaIngresada.substring(2, 4);

                    //removemos 2 lineas
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);
                    this.lineasLayout.remove(this.lineasLayout.size() - 1);

                    EscribeLinea("\t" + DEPOSITO_FECHA_DISPLAY, "AVISO DEPOSITO");

                    txtValor_panel.setText("");


                    if (DEPOSITO_TIPO.equals("CHE")) {

                        avisoDepoStep = 81;

                        btnEnter.performClick();

                    } else {

                        if (DEPOSITO_CLI_VEND.equals("CLI")) {
                            avisoDepoStep = 8;

                            EscribeLinea("Presione OK para confirmar...", "AVISO DEPOSITO");

                        } else {
                            avisoDepoStep = 12;

                            btnEnter.performClick();
                        }
                    }
                }
            }
            break;
            case 8: {
                EnviaMensajeAvisoDepositoCliente("AvisoDepositoResponse");
            }
            break;
            case 81: {
                EscribeLinea("Seleccione el Banco del cheque:", "AVISO DEPOSITO");

                List<ResultadoMenu> bancoCheque = new ArrayList<ResultadoMenu>();
                bancoCheque.add(new ResultadoMenu("BANCO BCI", "BBCI", null, -1));
                bancoCheque.add(new ResultadoMenu("BANCO BICE", "BICE", null, -1));
                bancoCheque.add(new ResultadoMenu("BANCO CONSORCIO", "CONS", null, -1));
                bancoCheque.add(new ResultadoMenu("BANCO DE CHILE", "BCHI", null, -1));
                bancoCheque.add(new ResultadoMenu("BANCO EDWARDS", "BEDW", null, -1));
                bancoCheque.add(new ResultadoMenu("BANCO FALABELLA", "FALA", null, -1));
                bancoCheque.add(new ResultadoMenu("BANCO HSBC", "HSBC", null, -1));
                bancoCheque.add(new ResultadoMenu("BANCO INTERNACIONAL", "BINT", null, -1));
                bancoCheque.add(new ResultadoMenu("BANCO ITAU", "ITAU", null, -1));
                bancoCheque.add(new ResultadoMenu("BANCO NOVA", "NOVA", null, -1));
                bancoCheque.add(new ResultadoMenu("BANCO PARIS", "PARI", null, -1));
                bancoCheque.add(new ResultadoMenu("BANCO PENTA", "PENT", null, -1));
                bancoCheque.add(new ResultadoMenu("BANCO RIPLEY", "RPLE", null, -1));
                bancoCheque.add(new ResultadoMenu("BANCO SANTANDER", "BCSA", null, -1));
                bancoCheque.add(new ResultadoMenu("BANCO SECURITY", "BSEC", null, -1));
                bancoCheque.add(new ResultadoMenu("BANCO ESTADO", "BCES", null, -1));
                bancoCheque.add(new ResultadoMenu("BBVA", "BBVA", null, -1));
                bancoCheque.add(new ResultadoMenu("CITIBANK", "CITI", null, -1));
                bancoCheque.add(new ResultadoMenu("CORPBANCA", "CORP", null, -1));
                bancoCheque.add(new ResultadoMenu("CREDICHILE", "CRED", null, -1));
                bancoCheque.add(new ResultadoMenu("DEUTSCHE BANK", "DEUT", null, -1));
                bancoCheque.add(new ResultadoMenu("RABOBANKY", "RABO", null, -1));
                bancoCheque.add(new ResultadoMenu("SCOTIABANK", "SCOT", null, -1));

                this.recarga_lstResultados.setAdapter(new ResultadosImgMenuAdapter(MainActivity.this, bancoCheque));
                this.recarga_lstResultados.setVisibility(View.VISIBLE);
                this.recarga_lstResultados.setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        ResultadoMenu item = ((ResultadosImgMenuAdapter) recarga_lstResultados.getAdapter()).datos.get(position);

                        DEPOSITO_BANCO_CHEQUE = item.get_subdescripcion();

                        EscribeLinea("\t" + item.get_descripcion(), "AVISO DEPOSITO");

                        avisoDepoStep = 9;

                        recarga_lstResultados.setVisibility(View.GONE);

                        ProcesoAvisoDeposito();
                    }
                });
            }
            break;
            case 9: {
                EscribeLinea("Ingrese el Nro. del cheque:", "AVISO DEPOSITO");

                avisoDepoStep = 10;

            }
            break;
            case 10: {

                String chequeIngresado = txtValor_panel.getText().toString().trim();

                //validamos el codigo ingresado
                if (chequeIngresado == null || chequeIngresado.equals("")) {
                    Toast.makeText(MainActivity.this, "Debe ingresar un numero de cheque para continuar", Toast.LENGTH_LONG).show();
                } else if (chequeIngresado.length() > 12) {
                    Toast.makeText(MainActivity.this, "El numero de cheque no puede ser mayor que 12 digitos", Toast.LENGTH_LONG).show();
                } else if (!Formatos.isNumeric(chequeIngresado)) {
                    Toast.makeText(MainActivity.this, "El numero de cheque debe ser numerico", Toast.LENGTH_LONG).show();
                } else {

                    DEPOSITO_CHEQUE = chequeIngresado;

                    EscribeLinea("\t" + DEPOSITO_CHEQUE, "AVISO DEPOSITO");

                    txtValor_panel.setText("");

                    if (DEPOSITO_CLI_VEND.equals("CLI")) {

                        avisoDepoStep = 11;

                        new AlertDialog.Builder(this)
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .setTitle("AVISO")
                                .setMessage("Su Abono se ejecutará dentro de las próximas 48 hs, una vez que los fondos sean liberados en la cuenta corriente de JJD Comunicaciones")
                                .setPositiveButton("OK", null)
                                .show();

                        EscribeLinea("Presione OK para confirmar...", "AVISO DEPOSITO");

                    } else {

                        avisoDepoStep = 12;
                        btnEnter.performClick();
                    }
                }
            }
            break;
            case 11: {
                EnviaMensajeAvisoDepositoCliente("AvisoDepositoResponse");
            }
            break;
            case 12: {

                //EscribeLinea("Ingrese el nro. de vendedor:", "AVISO DEPOSITO");
                EscribeLinea("Ingrese su PIN:", "AVISO DEPOSITO");

                //avisoDepoStep = 13;
                avisoDepoStep = 14;

            }
            break;
            /*case 13: {
                String numeroIngresado = txtValor_panel.getText().toString().trim();

                //validamos el numero ingresado
                if (numeroIngresado == null || numeroIngresado.equals("")) {
                    Toast.makeText(MainActivity.this, "Debe ingresar un numero de vendedor para continuar", Toast.LENGTH_LONG).show();
                } else if (numeroIngresado.length() > 4) {
                    Toast.makeText(MainActivity.this, "El numero no puede superar 4 caracteres", Toast.LENGTH_LONG).show();
                } else if (!Formatos.isNumeric(numeroIngresado)) {
                    Toast.makeText(MainActivity.this, "El valor ingresado debe ser numerico", Toast.LENGTH_LONG).show();
                } else {

                    DEPOSITO_NROVENDEDOR = numeroIngresado;

                    EscribeLinea("\t" + DEPOSITO_NROVENDEDOR, "AVISO DEPOSITO");

                    txtValor_panel.setText("");

                    EscribeLinea("Ingrese su PIN:", "AVISO DEPOSITO");

                    avisoDepoStep = 14;
                }
            }
            break;*/
            case 14: {
                String pinIngresado = txtValor_panel.getText().toString().trim();

                //validamos el pin ingresado
                if (pinIngresado == null || pinIngresado.equals("")) {
                    Toast.makeText(MainActivity.this, "Debe ingresar su numero de PIN para continuar", Toast.LENGTH_LONG).show();
                } else if (pinIngresado.length() > 4) {
                    Toast.makeText(MainActivity.this, "El numero de PIN no puede superar 4 caracteres", Toast.LENGTH_LONG).show();
                } else if (!Formatos.isNumeric(pinIngresado)) {
                    Toast.makeText(MainActivity.this, "El valor ingresado debe ser numerico", Toast.LENGTH_LONG).show();
                } else {

                    DEPOSITO_NROPIN = pinIngresado;

                    //EscribeLinea("\t" + DEPOSITO_NROPIN, "AVISO DEPOSITO");
                    EscribeLinea("\t" + pinIngresado.replaceAll(".", "*"), "AVISO DEPOSITO");

                    txtValor_panel.setText("");

                    if (DEPOSITO_TIPO.equals("CHE")) {
                        new AlertDialog.Builder(this)
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .setTitle("AVISO")
                                .setMessage("Su Abono se ejecutará dentro de las próximas 48 hs, una vez que los fondos sean liberados en la cuenta corriente de JJD Comunicaciones")
                                .setPositiveButton("OK", null)
                                .show();
                    }
                    EscribeLinea("Presione OK para confirmar...", "AVISO DEPOSITO");

                    avisoDepoStep = 15;
                }
            }
            break;
            case 15: {
                EnviaMensajeAvisoDepositoVendedor("AvisoDepositoResponse");
            }
            break;
            default:
                btnCancelar.performClick();
                break;
        }
    }
    //endregion

    //region SECCION CONSULTAS
    public void ConsultaComercial(String funcion) {
        try {

            if (funcion.equals("PRODUCTOS"))
                flagObtieneProductos = true;

            EnviaMensajeConsultaComercial("ConsultaComercialResponse", funcion);

        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, ex.getMessage().toString(), Toast.LENGTH_LONG).show();

            flagObtieneProductos = false;
        }
    }

    public void ConsultaUltimasOperaciones() {
        try {

            EnviaMensajeUltimasOperaciones("ConsultaUltOperacionesResponse");

        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, ex.getMessage().toString(), Toast.LENGTH_LONG).show();
        }
    }

    public void ConsultaUltimosAbonos() {
        try {

            EnviaMensajeUltimosAbonos("ConsultaUltimosAbonosResponse");

        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, ex.getMessage().toString(), Toast.LENGTH_LONG).show();
        }
    }

    public void ConsultaUltimaRecarga() {
        try {

            EnviaMensajeUltRecarga("ConsultaUltRecargaResponse");

        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, ex.getMessage().toString(), Toast.LENGTH_LONG).show();
        }
    }

    public void ConsultaVentas(String funcion) {
        try {

            EnviaMensajeVentas("ConsultaVentasResponse", funcion);

        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, ex.getMessage().toString(), Toast.LENGTH_LONG).show();
        }
    }

    public void Recarga() {
        try {

            EnviaMensajeRecarga("RecargaResponse");

        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, ex.getMessage().toString(), Toast.LENGTH_LONG).show();
        }
    }

    public void ConsultaMensajes() {
        try {

            EnviaMensajeConsultaMensajes("ConsultaMensajesResponse");

        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, ex.getMessage().toString(), Toast.LENGTH_LONG).show();
        }
    }

    public void EnvioMensajesTermInit() {
        try {

            //1.Mostramos los tipos de mensajes que tenemos

            //ocultamos la lista
            this.lstMenu.setVisibility(View.GONE);

            this.txtComentario_panel.setText("ENVIO DE MENSAJES");
            this.txtComentario_panel.setVisibility(View.VISIBLE);

            this.lineasLayout = null;

            this.recarga_btnVolver.setVisibility(View.VISIBLE);

            EscribeLinea("Seleccione el mensaje:", "ENVIO DE MENSAJES");

            if(listadoMensajesTerminal == null || listadoMensajesTerminal.isEmpty()){
                EscribeLinea("Sin mensajes", "ENVIO DE MENSAJES");
            }
            else {
                this.recarga_lstResultados.setAdapter(new ListadoMensajesTermAdapter(MainActivity.this, listadoMensajesTerminal));
                this.recarga_lstResultados.setVisibility(View.VISIBLE);
                this.recarga_lstResultados.setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        MsgsTermMSGPOSRecarga item = ((ListadoMensajesTermAdapter) recarga_lstResultados.getAdapter()).datos.get(position);

                        recarga_lstResultados.setVisibility(View.GONE);

                        //2.Luego generamos la transaccion
                        EnviaMensajeDesdeTerminal("EnvioMensajesTermResponse", item.getCodigo());
                    }
                });
            }
        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, ex.getMessage().toString(), Toast.LENGTH_LONG).show();
        }
    }

    //endregion

    //region SECCION IMPRESION
    private void ImpresionResultados() {
        try {

            if (lastmensajeRecarga.getRoot_CodigoProceso().equals("201105") ||
                    lastmensajeRecarga.getRoot_CodigoProceso().equals("301105")) {

                //si el mensaje es de consulta ultima recarga no tengo el tipo de producto consumido
                //por eso lo tomamos del ultimo mensaje de recarga enviado
                MensajeProductoRecarga productoRecarga = ObtieneProductoByCode(lastRecarga.getTipoProductoConsumido());

                GeneraTicketRecarga(lastmensajeRecarga, productoRecarga, "DUPLICADO");

            } else {

                try {
                    if (_printer == null)
                        _printer = Printer.getInstance(config.PRINTER_MODEL);

                    if (!_printer.initialize()) {
                        Toast.makeText(MainActivity.this, "Error al inicializar", Toast.LENGTH_SHORT).show();
                        _printer.end();
                    } else {

                        _printer.lineFeed();

                        TextView txtTitulo = (TextView) findViewById(R.id.recarga_txt_titulo);
                        if (txtTitulo.getVisibility() == View.VISIBLE) {
                            _printer.format(2, 2, ALINEACION.CENTER);
                            _printer.printLine(txtTitulo.getText().toString());
                            _printer.cancelCurrentFormat();
                        }

                        _printer.cancelCurrentFormat();

                        if (((TextView) findViewById(R.id.recarga_txt1)).getVisibility() == View.VISIBLE) {
                            _printer.sendSeparadorHorizontal();
                            _printer.printLine(((TextView) findViewById(R.id.recarga_txt1)).getText().toString());
                        }
                        if (((TextView) findViewById(R.id.recarga_txt2)).getVisibility() == View.VISIBLE) {
                            _printer.printLine(((TextView) findViewById(R.id.recarga_txt2)).getText().toString());
                        }
                        if (((TextView) findViewById(R.id.recarga_txt3)).getVisibility() == View.VISIBLE) {
                            _printer.printLine(((TextView) findViewById(R.id.recarga_txt3)).getText().toString());
                        }
                        if (((TextView) findViewById(R.id.recarga_txt4)).getVisibility() == View.VISIBLE) {
                            _printer.printLine(((TextView) findViewById(R.id.recarga_txt4)).getText().toString());
                        }
                        if (((TextView) findViewById(R.id.recarga_txt5)).getVisibility() == View.VISIBLE) {
                            _printer.printLine(((TextView) findViewById(R.id.recarga_txt5)).getText().toString());
                        }
                        if (((TextView) findViewById(R.id.recarga_txt6)).getVisibility() == View.VISIBLE) {
                            _printer.printLine(((TextView) findViewById(R.id.recarga_txt6)).getText().toString());
                        }
                        if (((TextView) findViewById(R.id.recarga_txt7)).getVisibility() == View.VISIBLE) {
                            _printer.printLine(((TextView) findViewById(R.id.recarga_txt7)).getText().toString());
                        }
                        if (((TextView) findViewById(R.id.recarga_txt8)).getVisibility() == View.VISIBLE) {
                            _printer.printLine(((TextView) findViewById(R.id.recarga_txt8)).getText().toString());
                        }
                        if (((TextView) findViewById(R.id.recarga_txt9)).getVisibility() == View.VISIBLE) {
                            _printer.printLine(((TextView) findViewById(R.id.recarga_txt9)).getText().toString());
                        }
                        if (((TextView) findViewById(R.id.recarga_txt10)).getVisibility() == View.VISIBLE) {
                            _printer.printLine(((TextView) findViewById(R.id.recarga_txt10)).getText().toString());
                        }

                        if (this.recarga_lstResultados.getVisibility() == View.VISIBLE) {

                            ResultadosMenuAdapter resultadosMenuAdapter = (ResultadosMenuAdapter) this.recarga_lstResultados.getAdapter();

                            if (resultadosMenuAdapter.datos.size() > 0) {

                                _printer.sendSeparadorHorizontal();

                                for (ResultadoMenu item : resultadosMenuAdapter.datos) {
                                    _printer.printLine(item.get_descripcion());
                                    _printer.printLine("     " + item.get_subdescripcion());
                                }
                            }
                        }

                        _printer.footer();
                    }
                } catch (Exception ex) {
                    Toast.makeText(MainActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void ImprimeAvisoDeposito() {
        try {
            if (_printer == null)
                _printer = Printer.getInstance(config.PRINTER_MODEL);

            if (!_printer.initialize()) {
                Toast.makeText(MainActivity.this, "Error al inicializar", Toast.LENGTH_SHORT).show();
                _printer.end();
            } else {

                _printer.lineFeed();

                _printer.format(2, 2, ALINEACION.CENTER);
                _printer.printLine("AVISO DEPOSITO");
                _printer.cancelCurrentFormat();
                _printer.sendSeparadorHorizontal();

                if (DEPOSITO_CLI_VEND.equals("CLI"))
                    _printer.printLine("Comercio     : " + lastmensajeRecarga.getRoot_NumeroComercio());
                else
                    _printer.printLine("Vendedor     : " + DEPOSITO_NROVENDEDOR);

                _printer.printLine("ID.Terminal  : " + lastmensajeRecarga.getIdentTerminal());

                Date fechaHora = Utils.Util_FechaHora(lastmensajeRecarga.getFechaHora());
                _printer.printLine("Fecha y Hora : " + Formatos.FormateaDate(fechaHora, Formatos.FullDateTimeFormat));

                _printer.lineFeed();

                _printer.printLine("Tipo de Dep. : " + DEPOSITO_TIPO);

                _printer.printLine("Nro.Comprob. : " + lastmensajeRecarga.getNroComprobante());

                _printer.printLine("Monto        : $ " + lastmensajeRecarga.getMontoDepositado());

                _printer.printLine("Lugar de Dep.: " + lastmensajeRecarga.getLugarDeDeposito());

                Date fechaHoraDep = Utils.Util_FechaHora(lastmensajeRecarga.getFechaDeposito());
                _printer.printLine("Fecha y Hora : " + Formatos.FormateaDate(fechaHoraDep, Formatos.FullDateTimeFormatNoSeconds));

                if (DEPOSITO_TIPO.equals("CHE")) {
                    _printer.printLine("Banco Cheque : " + DEPOSITO_BANCO_CHEQUE);

                    _printer.printLine("Nro. Cheque  : " + DEPOSITO_CHEQUE);

                    _printer.sendSeparadorHorizontal();

                    _printer.printLine("Su Abono se ejecutara dentro de ");
                    _printer.printLine("las proximas 48 hs, una vez que ");
                    _printer.printLine("los fondos sean liberados en la ");
                    _printer.printLine("cuenta cte de JJD Comunicaciones");

                    _printer.sendSeparadorHorizontal();

                }

                _printer.footer();
            }
        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void ImprimeVentaAbono() {

        try {
            if (_printer == null)
                _printer = Printer.getInstance(config.PRINTER_MODEL);

            if (!_printer.initialize()) {
                Toast.makeText(MainActivity.this, "Error al inicializar", Toast.LENGTH_SHORT).show();
                _printer.end();
            } else {

                _printer.lineFeed();

                _printer.format(2, 2, ALINEACION.CENTER);
                _printer.printLine("COMPROBANTE");
                _printer.cancelCurrentFormat();
                _printer.format(1, 1, ALINEACION.CENTER);
                _printer.printLine("VENTA DE ABONO");
                _printer.cancelCurrentFormat();
                _printer.sendSeparadorHorizontal();

                _printer.printLine("Comercio         : " + lastmensajeRecarga.getRoot_NumeroComercio());

                _printer.printLine("ID.Transaccion   : " + lastmensajeRecarga.getNroTransaccion());

                _printer.printLine("Cod. Autorizac.  : " + lastmensajeRecarga.getCodAutorizacion());

                _printer.printLine("ID.Terminal      : " + lastmensajeRecarga.getIdentTerminal());

                Date fechaHora = Utils.Util_FechaHora(lastmensajeRecarga.getFechaHora());
                _printer.printLine("Fecha y Hora     : " + Formatos.FormateaDate(fechaHora, Formatos.FullDateTimeFormat));

                //mSerialPort.sendString("Operadora        : " + lastmensajeRecarga.getMontoDepositado());
                //mSerialPort.sendLineFeed();

                _printer.printLine("Saldo Inicial    : " + VENTAABONO_SALDOANT);

                _printer.printLine("Monto Venta      : " + Formatos.FormateaDecimal(lastmensajeRecarga.getMontoAbono(), Formatos.DecimalFormat, "$"));

                _printer.printLine("Monto Acreditado : " + Formatos.FormateaDecimal(lastmensajeRecarga.getAbono(), Formatos.DecimalFormat, "$"));

                _printer.printLine("Saldo            : " + Formatos.FormateaDecimal(lastmensajeRecarga.getSaldoComercio(), Formatos.DecimalFormat, "$"));

                _printer.footer();
            }
        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void ImprimeCierreTurno() {
        try {
            if (_printer == null)
                _printer = Printer.getInstance(config.PRINTER_MODEL);

            if (!_printer.initialize()) {
                Toast.makeText(MainActivity.this, "Error al inicializar", Toast.LENGTH_SHORT).show();
                _printer.end();
            } else {

                _printer.lineFeed();

                _printer.format(2, 1, ALINEACION.CENTER);
                _printer.printLine("TOTALES POR TURNO");
                _printer.cancelCurrentFormat();
                _printer.sendSeparadorHorizontal();

                _printer.printLine("Nro.Comercio: " + lastmensajeRecarga.getRoot_NumeroComercio());

                _printer.printLine("ID Terminal : " + lastmensajeRecarga.getIdentTerminal());


                Date fechaHoraInicio = Utils.Util_FechaHora(CIERRE_FECHA_INICIO);
                Date fechaHoraFin = Utils.Util_FechaHora(CIERRE_FECHA_CIERRE);

                _printer.printLine("Fecha Inicio : " + Formatos.FormateaDate(fechaHoraInicio, Formatos.DayMonthFormat));
                _printer.printLine("Hora  Inicio : " + Formatos.FormateaDate(fechaHoraInicio, Formatos.ShortTimeFormat));

                _printer.printLine("Fecha y Hora Fin: " + Formatos.FormateaDate(fechaHoraFin, Formatos.DayMonthShortTimeFormat));

                _printer.lineFeed();


                if (lastmensajeRecarga.getProductos().size() > 0) {

                    _printer.format(1, 1, ALINEACION.CENTER);
                    _printer.printLine("OPERADORAS");
                    _printer.cancelCurrentFormat();

                    for (MensajeProductoRecarga producto : lastmensajeRecarga.getProductos()) {

                        _printer.printLine("Cod.Operadora  : " + producto.getCodigo());
                        _printer.printLine("Nombre         : " + producto.getDescripcion());
                        _printer.printLine("Cant.Recargas  : " + producto.getCantidadOperaciones());
                        _printer.printLine("Total Recargas : $ " + producto.getMontoOperaciones());

                        _printer.lineFeed();
                    }
                }

                _printer.sendSeparadorHorizontal();

                _printer.format(1, 1, ALINEACION.CENTER);
                _printer.printLine("TOTALES");
                _printer.cancelCurrentFormat();

                _printer.printLine("Cant. Operaciones : " + lastmensajeRecarga.getTotalOperaciones());

                _printer.printLine("Total Operaciones : $ " + lastmensajeRecarga.getTotalMonto());

                if (Integer.parseInt(lastmensajeRecarga.getCantProductos()) > 0) {
                    _printer.printLine("Saldo        : $ " + lastmensajeRecarga.getProductos().get(0).getSaldoProducto());

                    _printer.printLine("Lim. Credito : $ " + lastmensajeRecarga.getProductos().get(0).getLimiteCreditoProducto());
                }

                _printer.footer();
            }
        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void ImprimeTotales() {
        try {
            if (_printer == null)
                _printer = Printer.getInstance(config.PRINTER_MODEL);

            if (!_printer.initialize()) {
                Toast.makeText(MainActivity.this, "Error al inicializar", Toast.LENGTH_SHORT).show();
                _printer.end();
            } else {

                _printer.lineFeed();

                _printer.format(2, 2, ALINEACION.CENTER);
                _printer.printLine("TOTALES DEL DIA");
                _printer.cancelCurrentFormat();
                _printer.sendSeparadorHorizontal();

                _printer.printLine("Nro.Comercio: " + lastmensajeRecarga.getRoot_NumeroComercio());

                _printer.printLine("ID Terminal : " + lastmensajeRecarga.getIdentTerminal());

                Date fechaHora = Utils.Util_FechaHora(lastmensajeRecarga.getFechaHora());
                _printer.printLine("Fecha y Hora: " + Formatos.FormateaDate(fechaHora, Formatos.FullDateTimeFormat));

                _printer.lineFeed();

                for (MensajeProductoRecarga producto : lastmensajeRecarga.getProductos()) {

                    _printer.format(2, 1, ALINEACION.CENTER);
                    _printer.printLine(producto.getDescripcion());
                    _printer.cancelCurrentFormat();

                    _printer.printLine("Cod.Operadora  : " + producto.getCodigo());

                    _printer.printLine("Cant.Recargas  : " + producto.getCantidadOperaciones());

                    _printer.printLine("Total Recargas : $" + producto.getMontoOperaciones());

                    _printer.printLine("Total Ganancias: $" + producto.getMontoGanancia());

                    _printer.printLine("Saldo Producto : $" + producto.getSaldoProducto());

                    _printer.printLine("Limite Credito : $" + producto.getLimiteCreditoProducto());

                    _printer.lineFeed();
                }

                _printer.footer();
            }
        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void ImprimeConfiguracion() {
        try {
            if (_printer == null)
                _printer = Printer.getInstance(config.PRINTER_MODEL);

            if (!_printer.initialize()) {
                Toast.makeText(MainActivity.this, "Error al inicializar", Toast.LENGTH_SHORT).show();
                _printer.end();
            } else {

                _printer.lineFeed();

                _printer.format(2, 2, ALINEACION.CENTER);
                _printer.printLine("CONFIGURACION");
                _printer.cancelCurrentFormat();
                _printer.sendSeparadorHorizontal();

                _printer.printLine("Nro.Comercio: " + Preferences.getString(MainActivity.this, Preferences.KEY_CFG_COD_COM));

                _printer.printLine("ID Terminal : " + Preferences.getString(MainActivity.this, Preferences.KEY_CFG_ID_TERM));

                _printer.printLine("IP Servidor : " + Preferences.getString(MainActivity.this, Preferences.KEY_CFG_IP_SERV));

                _printer.printLine("Port Servid.: " + Preferences.getString(MainActivity.this, Preferences.KEY_CFG_PORT_SERV));

                _printer.footer();
            }
        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //endregion

    //region SECCION RESPUESTAS

    public void EnvioMensajesTermResponse() {
        try {

            this.recarga_btnVolver.setVisibility(View.VISIBLE);

            List<String> lineas = new ArrayList<String>();
            lineas.add("");
            lineas.add("@C@" + lastmensajeRecarga.getMensajeLinea1());
            lineas.add("@C@" + lastmensajeRecarga.getMensajeLinea2());

            EscribeLineas(lineas, "ENVIO DE MENSAJES");

        }
        catch (Exception ex) {
            Toast.makeText(MainActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void ConsultaComercialResponse(String funcion) {
        try {

            if (TransaccionAprobada(lastmensajeRecarga)) {

                if (funcion.equals("PRODUCTOS")) {

                    flagObtieneProductos = false;

                    //copiamos los productos
                    if (Integer.parseInt(lastmensajeRecarga.getCantProductos()) > 0) {
                        //guardamos una copia de todos los productos obtenidos en la consulta comercial
                        listadoProductos = new ArrayList<MensajeProductoRecarga>(lastmensajeRecarga.getProductos());
                        //Collections.copy(listadoProductos, lastmensajeRecarga.getProductos());
                    }

                    //guardamos el IdExterno del cliente para el aviso de deposito
                    IDExtCliente = lastmensajeRecarga.getIDExtCliente();
                    GuardarPreferencias();

                    //llamamos nuevamente a la generacion del menu
                    GeneraMenu();

                    Toast.makeText(MainActivity.this, "Se ha actualizado la informacion de sus productos habilitados. Por favor ingrese nuevamente al menu RECARGA", Toast.LENGTH_SHORT).show();

                } else if (funcion.equals("DATOS")) {

                    flagDatosComercio = true;

                    List<String> lineas = new ArrayList<String>();
                    lineas.add("Nro. Comercio: " + lastmensajeRecarga.getRoot_NumeroComercio());
                    lineas.add("ID Terminal: " + lastmensajeRecarga.getIdentTerminal());

                    Date fechaHora = Utils.Util_FechaHora(lastmensajeRecarga.getFechaHora());

                    lineas.add("Fecha y Hora: " + Formatos.FormateaDate(fechaHora, Formatos.FullDateTimeFormat));
                    lineas.add("ID Externo: " + lastmensajeRecarga.getIDExtCliente());
                    lineas.add("Razon Social: " + lastmensajeRecarga.getRazonSocialCliente());
                    lineas.add("RUT: " + lastmensajeRecarga.getRucCliente());
                    lineas.add("Domicilio: " + lastmensajeRecarga.getDomicilioSucursal());
                    lineas.add("Comuna: " + lastmensajeRecarga.getComunaSucursal());
                    lineas.add("Region: " + lastmensajeRecarga.getRegionSucursal());

                    EscribeLineas(lineas, "DATOS COMERCIO");

                    this.recarga_btnImprimirResultados.setVisibility(View.VISIBLE);
                    this.recarga_btnVolver.setVisibility(View.VISIBLE);

                    //vamos a llamar a la consulta de datos referenciales
                    EnviaMensajeConsultaDatosReferenciales("ConsultaDatosReferencialesResponse");

                } else if (funcion.equals("SALDO")) {

                    flagDatosComercio = true;

                    List<String> lineas = new ArrayList<String>();

                    String mensaje = "Su saldo es ";

                    if (lastmensajeRecarga.getProductos().size() > 0)
                        //mensaje = mensaje + "$ " + lastmensajeRecarga.getProductos().get(0).getSaldoProducto();
                        mensaje = mensaje + Formatos.FormateaDecimal(lastmensajeRecarga.getProductos().get(0).getSaldoProducto(), Formatos.DecimalFormat, "$");
                    else
                        mensaje = mensaje + "$ 0";

                    lineas.add(mensaje);

                    EscribeLineas(lineas, "SALDO DEL COMERCIO");

                    this.recarga_btnVolver.setVisibility(View.VISIBLE);

                } else {
                    Toast.makeText(MainActivity.this, "Funcion " + funcion + " no implementada", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(MainActivity.this, "Transaccion no aprobada: " + TransaccionMensajeError(lastmensajeRecarga), Toast.LENGTH_LONG).show();

                flagObtieneProductos = false;
            }

        } catch (Exception ex) {

            Toast.makeText(MainActivity.this, ex.getMessage().toString(), Toast.LENGTH_LONG).show();

            flagObtieneProductos = false;
        }
    }

    public void ConsultaMensajesResponse() {
        try {

            if (TransaccionAprobada(lastmensajeRecarga)) {

                if (lastmensajeRecarga.getCantMS().equals("0")) {
                    List<String> lineas = new ArrayList<String>();
                    lineas.add("No se encontraron mensajes");
                    EscribeLineas(lineas, "MENSAJES");

                    this.recarga_btnVolver.setVisibility(View.VISIBLE);
                } else {
                    List<ResultadoMenu> resultadoMenus = new ArrayList<ResultadoMenu>();

                    for (MensajeMSRecarga mensaje : lastmensajeRecarga.getMensajes()) {
                        resultadoMenus.add(new ResultadoMenu(mensaje.getFecha() + " " + mensaje.getDe(), mensaje.getTexto(), mensaje));
                    }

                    EscribeLineas(new ArrayList<String>(), "MENSAJES");

                    ResultadosMenuAdapter resultadosMenuAdapter = new ResultadosMenuAdapter(MainActivity.this, resultadoMenus);
                    this.recarga_lstResultados.setAdapter(resultadosMenuAdapter);
                    //(findViewById(R.id.recarga_tr_resultados)).setVisibility(View.VISIBLE);
                    this.recarga_lstResultados.setVisibility(View.VISIBLE);
                    this.recarga_btnVolver.setVisibility(View.VISIBLE);
                /*
                this.recarga_lstResultados.setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        ResultadoMenu item = ((ResultadosMenuAdapter) recarga_lstResultados.getAdapter()).datos.get(position);

                        MensajeMSRecarga mensaje = (MensajeMSRecarga) item.get_item();

                        List<String> lineas = new ArrayList<String>();
                        lineas.add(mensaje.getFecha() + " " + mensaje.getDe());
                        lineas.add(mensaje.getTexto());

                        EscribeLineas(lineas, "MENSAJES");

                        btnCancelar.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View arg0) {
                                ConsultaMensajesResponse();

                                //restauramos la funcion cancelar
                                btnCancelar.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View arg0) {
                                        Cancelar();
                                    }
                                });

                            }
                        });

                        recarga_btnVolver.setVisibility(View.VISIBLE);
                        recarga_btnVolver.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View arg0) {
                                ConsultaMensajesResponse();

                                //restauramos la funcion cancelar
                                recarga_btnVolver.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View arg0) {
                                        Cancelar();
                                    }
                                });

                            }
                        });

                    }
                });
                */

                }

                this.lstMenu.setVisibility(View.GONE);

            } else {
                Toast.makeText(MainActivity.this, "Transaccion no aprobada: " + TransaccionMensajeError(lastmensajeRecarga), Toast.LENGTH_LONG).show();
            }


        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, ex.getMessage().toString(), Toast.LENGTH_LONG).show();
        }
    }

    public void ConsultaUltimosAbonosResponse() {
        try {

            if (TransaccionAprobada(lastmensajeRecarga)) {

                flagDatosComercio = true;

                if (lastmensajeRecarga.getCantTX().equals("0")) {
                    List<String> lineas = new ArrayList<String>();
                    lineas.add("No tiene abonos");
                    EscribeLineas(lineas, "ULTIMAS ABONOS");

                    this.recarga_btnVolver.setVisibility(View.VISIBLE);

                } else {
                    List<ResultadoMenu> resultadoMenus = new ArrayList<ResultadoMenu>();

                    for (MensajeTXRecarga operacion : lastmensajeRecarga.getTXs()) {
                        resultadoMenus.add(new ResultadoMenu(operacion.getMsgLn(), operacion.getMsgAux(), operacion));
                    }


                    EscribeLineas(new ArrayList<String>(), "ULTIMOS ABONOS");
                    ResultadosMenuAdapter resultadosMenuAdapter = new ResultadosMenuAdapter(MainActivity.this, resultadoMenus);
                    this.recarga_lstResultados.setAdapter(resultadosMenuAdapter);
                    //(findViewById(R.id.recarga_tr_resultados)).setVisibility(View.VISIBLE);
                    this.recarga_lstResultados.setVisibility(View.VISIBLE);
                    this.recarga_btnVolver.setVisibility(View.VISIBLE);
                    this.recarga_btnImprimirResultados.setVisibility(View.VISIBLE);
                /*
                this.recarga_lstResultados.setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        ResultadoMenu item = ((ResultadosMenuAdapter) recarga_lstResultados.getAdapter()).datos.get(position);

                        MensajeTXRecarga operacion = (MensajeTXRecarga) item.get_item();

                        List<String> lineas = new ArrayList<String>();
                        lineas.add(operacion.getMsgLn());
                        lineas.add(operacion.getMsgAux());

                        EscribeLineas(lineas, "ULTIMOS ABONOS");

                        btnCancelar.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View arg0) {
                                ConsultaUltimosAbonosResponse();

                                //restauramos la funcion cancelar
                                btnCancelar.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View arg0) {
                                        Cancelar();
                                    }
                                });

                            }
                        });

                        recarga_btnVolver.setVisibility(View.VISIBLE);
                        recarga_btnVolver.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View arg0) {
                                ConsultaUltimosAbonosResponse();

                                //restauramos la funcion cancelar
                                recarga_btnVolver.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View arg0) {
                                        Cancelar();
                                    }
                                });

                            }
                        });

                    }
                });*/

                }

                this.lstMenu.setVisibility(View.GONE);

            } else {
                Toast.makeText(MainActivity.this, "Transaccion no aprobada: " + TransaccionMensajeError(lastmensajeRecarga), Toast.LENGTH_LONG).show();
            }

        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, ex.getMessage().toString(), Toast.LENGTH_LONG).show();
        }
    }

    public void ConsultaUltOperacionesResponse() {
        try {

            if (TransaccionAprobada(lastmensajeRecarga)) {

                if (lastmensajeRecarga.getCantTX().equals("0")) {
                    List<String> lineas = new ArrayList<String>();
                    lineas.add("No se registran operaciones");
                    EscribeLineas(lineas, "ULTIMAS OPERACIONES");

                    this.recarga_btnVolver.setVisibility(View.VISIBLE);
                } else {

                    List<ResultadoMenu> resultadoMenus = new ArrayList<ResultadoMenu>();

                    for (MensajeTXRecarga operacion : lastmensajeRecarga.getTXs()) {
                        resultadoMenus.add(new ResultadoMenu(operacion.getMsgLn(), operacion.getMsgAux(), operacion));
                    }

                    EscribeLineas(new ArrayList<String>(), "ULTIMAS OPERACIONES");

                    ResultadosMenuAdapter resultadosMenuAdapter = new ResultadosMenuAdapter(MainActivity.this, resultadoMenus);
                    this.recarga_lstResultados.setAdapter(resultadosMenuAdapter);

                    this.recarga_lstResultados.setVisibility(View.VISIBLE);
                    this.recarga_btnVolver.setVisibility(View.VISIBLE);
                /*
                this.recarga_lstResultados.setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        ResultadoMenu item = ((ResultadosMenuAdapter) recarga_lstResultados.getAdapter()).datos.get(position);

                        MensajeTXRecarga operacion = (MensajeTXRecarga) item.get_item();

                        List<String> lineas = new ArrayList<String>();
                        lineas.add(operacion.getMsgLn());
                        lineas.add(operacion.getMsgAux());

                        EscribeLineas(lineas, "ULTIMAS OPERACIONES");

                        btnCancelar.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View arg0) {
                                ConsultaUltOperacionesResponse();

                                //restauramos la funcion cancelar
                                btnCancelar.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View arg0) {
                                        Cancelar();
                                    }
                                });

                            }
                        });

                        recarga_btnVolver.setVisibility(View.VISIBLE);
                        recarga_btnVolver.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View arg0) {
                                ConsultaUltOperacionesResponse();

                                //restauramos la funcion cancelar
                                recarga_btnVolver.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View arg0) {
                                        Cancelar();
                                    }
                                });

                            }
                        });

                    }
                });
                */
                }

                this.lstMenu.setVisibility(View.GONE);
            } else {
                Toast.makeText(MainActivity.this, "Transaccion no aprobada: " + TransaccionMensajeError(lastmensajeRecarga), Toast.LENGTH_LONG).show();
            }
        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, ex.getMessage().toString(), Toast.LENGTH_LONG).show();
        }
    }

    public void ConsultaUltRecargaResponse() {
        try {

            List<String> lineas = new ArrayList<String>();

            if (lastmensajeRecarga.getRoot_CodigoProceso().equals("000000")) {
                Date fechaHora = Utils.Util_FechaHora(lastmensajeRecarga.getFechaHora());

                lineas.add("Fecha y Hora: " + Formatos.FormateaDate(fechaHora, Formatos.FullDateTimeFormat));

                String numeroOriginal = "";
                MensajeProductoRecarga productoOriginal = ObtieneProductoByCode(lastRecarga.getTipoProductoConsumido());
                if (!productoOriginal.getPrefijo().equals("")) {
                    //si tiene prefijo se lo sacamos al numero de la recarga enviada
                    numeroOriginal = lastRecarga.getNroTarjeta().replaceFirst(productoOriginal.getPrefijo(), "");
                } else {
                    //como no tiene prefijo, tomamos directamente el numero
                    numeroOriginal = lastRecarga.getNroTarjeta();
                }


                lineas.add(productoOriginal.getDescIngreso() + ": " + numeroOriginal); // en este caso no vienen estos datos, los tomamos del original
                lineas.add("Operadora: " + productoOriginal.getDescripcion()); //no viene el producto, lo tomamos del original
                lineas.add("Monto: " + Formatos.FormateaDecimal(lastRecarga.getMontoTransaccion(), Formatos.DecimalFormat, "$"));
                //lineas.add("Medio de pago: " + RECARGA_MEDIOPAGO);
                lineas.add("@C@" + lastmensajeRecarga.getMensajeLinea1());
                lineas.add("@C@" + lastmensajeRecarga.getMensajeLinea2());

            } else {

                Date fechaHora = Utils.Util_FechaHora(lastmensajeRecarga.getFechaHora());

                lineas.add("Fecha y Hora: " + Formatos.FormateaDate(fechaHora, Formatos.FullDateTimeFormat));

                String numeroOriginal = "";
                MensajeProductoRecarga productoOriginal = ObtieneProductoByCode(lastRecarga.getTipoProductoConsumido());
                if (!productoOriginal.getPrefijo().equals("")) {
                    //si tiene prefijo se lo sacamos al numero de la recarga enviada
                    numeroOriginal = lastmensajeRecarga.getNroTarjeta().replaceFirst(productoOriginal.getPrefijo(), "");
                } else {
                    //como no tiene prefijo, tomamos directamente el numero
                    numeroOriginal = lastmensajeRecarga.getNroTarjeta();
                }

                //lineas.add("Nro. Tel.: " + lastmensajeRecarga.getNroTarjeta());
                lineas.add(productoOriginal.getDescIngreso() + ": " + numeroOriginal);
                lineas.add("Operadora: " + productoOriginal.getDescripcion()); //no viene el producto, lo tomamos del original
                lineas.add("Monto: " + Formatos.FormateaDecimal(lastmensajeRecarga.getMontoTransaccion(), Formatos.DecimalFormat, "$"));
                //lineas.add("Medio de pago: " + RECARGA_MEDIOPAGO);
                lineas.add("@C@" + lastmensajeRecarga.getMensajeLinea1());
                lineas.add("@C@" + lastmensajeRecarga.getMensajeLinea2());

                if (TransaccionAprobada(lastmensajeRecarga)) {
                    this.recarga_btnImprimirResultados.setVisibility(View.VISIBLE);
                    this.recarga_btnReintentarRecarga.setVisibility(View.GONE);
                } else {
                    this.recarga_btnImprimirResultados.setVisibility(View.GONE);
                    this.recarga_btnReintentarRecarga.setVisibility(View.VISIBLE);

                    //lineas.add("Presione OK para reintentar...");

                    recargaStep = 6;

                    //volvemos a cargar los parametros para el reenvio de la RECARGA
                    RECARGA_NROTEL = lastRecarga.getNroTarjeta(); //numero telefono
                    RECARGA_MONTO = lastRecarga.getMontoTransaccion(); //monto a recargar
                    RECARGA_MEDIOPAGO = lastRecarga.getFormaPago().equals("66") ? "EFECTIVO" : "TARJETA CREDITO";
                    RECARGA_PRODUCTO_OBJ = ObtieneProductoByCode(lastRecarga.getTipoProductoConsumido());

                    //RECARGA_IDPRODUCTO = lastRecarga.getTipoProductoConsumido(); //producto

                }
            }

            EscribeLineas(lineas, "ULTIMA RECARGA");
            this.recarga_btnVolver.setVisibility(View.VISIBLE);

        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, ex.getMessage().toString(), Toast.LENGTH_LONG).show();
        }
    }

    public void ConsultaVentasResponse(String funcion) {
        try {

            if (TransaccionAprobada(lastmensajeRecarga)) {

                if (funcion.equals("CIERRE_TURNO")) {

                    flagDatosComercio = true;

                    cierreTurnoStep = 0;

                    List<String> lineas = new ArrayList<String>();

                    //lineas.add("Nro.Comercio: " + lastmensajeRecarga.getRoot_NumeroComercio());
                    //lineas.add("ID Terminal : " + lastmensajeRecarga.getIdentTerminal());

                    Date fechaHoraInicio = Utils.Util_FechaHora(CIERRE_FECHA_INICIO);
                    Date fechaHoraFin = Utils.Util_FechaHora(CIERRE_FECHA_CIERRE);

                    //lineas.add("Fecha Inicio     : " + Formatos.FormateaDate(fechaHoraInicio, Formatos.DayMonthFormat));
                    //lineas.add("Hora Inicio      : " + Formatos.FormateaDate(fechaHoraInicio, Formatos.ShortTimeFormat));
                    lineas.add("Fecha y Hora Inicio: " + Formatos.FormateaDate(fechaHoraInicio, Formatos.DayMonthShortTimeFormat));

                    lineas.add("Fecha y Hora Fin    : " + Formatos.FormateaDate(fechaHoraFin, Formatos.DayMonthShortTimeFormat));

                    lineas.add("Cant. Operaciones: " + lastmensajeRecarga.getTotalOperaciones());
                    lineas.add("Total Operaciones: " + Formatos.FormateaDecimal(lastmensajeRecarga.getTotalMonto(), Formatos.DecimalFormat, "$"));

                    if (Integer.parseInt(lastmensajeRecarga.getCantProductos()) > 0) {
                        lineas.add("Saldo            : " + Formatos.FormateaDecimal(lastmensajeRecarga.getProductos().get(0).getSaldoProducto(), Formatos.DecimalFormat, "$"));
                        lineas.add("Limite           : " + Formatos.FormateaDecimal(lastmensajeRecarga.getProductos().get(0).getLimiteCreditoProducto(), Formatos.DecimalFormat, "$"));
                    }

                    EscribeLineas(lineas, "TOTALES POR TURNO");


                    List<ResultadoTotalesMenu> resultadoMenus = new ArrayList<ResultadoTotalesMenu>();

                    for (MensajeProductoRecarga producto : lastmensajeRecarga.getProductos()) {

                        ResultadoTotalesMenu resultadoMenu = new ResultadoTotalesMenu(
                                producto.getDescripcion(),
                                producto.getCantidadOperaciones(),
                                "$ " + producto.getMontoOperaciones(),
                                "$ " + producto.getMontoGanancia(),
                                "$ " + Formatos.FormateaDecimal(producto.getSaldoProducto(), Formatos.DecimalFormat),
                                "$ " + producto.getLimiteCreditoProducto(),
                                producto.getCodigo()
                        );

                        resultadoMenus.add(resultadoMenu);

                    }

                    ResultadosCierreTurnoMenuAdapter resultadosMenuAdapter = new ResultadosCierreTurnoMenuAdapter(MainActivity.this, resultadoMenus);
                    this.recarga_lstResultados.setAdapter(resultadosMenuAdapter);

                    this.recarga_lstResultados.setVisibility(View.VISIBLE);
                    this.recarga_btnVolver.setVisibility(View.VISIBLE);
                    this.recarga_btnImprimirResultados.setVisibility(View.VISIBLE);
                    this.recarga_btnImprimirResultados.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ImprimeCierreTurno();
                        }
                    });

                } else if (funcion.equals("GANANCIAS")) {

                    flagDatosComercio = true;

                    List<String> lineas = new ArrayList<String>();

                    lineas.add("Nro.Comercio: " + lastmensajeRecarga.getRoot_NumeroComercio());
                    lineas.add("ID Terminal: " + lastmensajeRecarga.getIdentTerminal());

                    Date fechaHora = Utils.Util_FechaHora(lastmensajeRecarga.getFechaHora());
                    lineas.add("Fecha y Hora: " + Formatos.FormateaDate(fechaHora, Formatos.FullDateTimeFormat));

                    lineas.add("Total Operaciones Dia: " + lastmensajeRecarga.getTotalOperaciones());
                    lineas.add("Total Monto Dia: $ " + lastmensajeRecarga.getTotalMonto());
                    lineas.add("Total Ganancia Dia: $ " + lastmensajeRecarga.getTotalGanancia());

                    lineas.add("Total Operaciones Mes: " + lastmensajeRecarga.getTotalOperacionesMes());
                    lineas.add("Total Monto Mes: $ " + lastmensajeRecarga.getTotalMontoMes());
                    lineas.add("Total Ganancia Mes: $ " + lastmensajeRecarga.getTotalGananciaMes());

                    EscribeLineas(lineas, "GANANCIAS");

                    this.recarga_btnImprimirResultados.setVisibility(View.VISIBLE);
                    this.recarga_btnVolver.setVisibility(View.VISIBLE);


                } else if (funcion.equals("TOTALES")) {

                    flagDatosComercio = true;


                    List<String> lineas = new ArrayList<String>();

                    lineas.add("Nro.Comercio: " + lastmensajeRecarga.getRoot_NumeroComercio());
                    lineas.add("ID Terminal: " + lastmensajeRecarga.getIdentTerminal());

                    Date fechaHora = Utils.Util_FechaHora(lastmensajeRecarga.getFechaHora());
                    lineas.add("Fecha y Hora: " + Formatos.FormateaDate(fechaHora, Formatos.FullDateTimeFormat));

                    //lineas.add("Imprimir para ver detalle por producto...");

                    EscribeLineas(lineas, "TOTALES DIARIO");

                    List<ResultadoTotalesMenu> resultadoMenus = new ArrayList<ResultadoTotalesMenu>();

                    for (MensajeProductoRecarga producto : lastmensajeRecarga.getProductos()) {

                        ResultadoTotalesMenu resultadoMenu = new ResultadoTotalesMenu(
                                producto.getDescripcion(),
                                producto.getCantidadOperaciones(),
                                "$" + producto.getMontoOperaciones(),
                                "$" + producto.getMontoGanancia(),
                                "$" + Formatos.FormateaDecimal(producto.getSaldoProducto(), Formatos.DecimalFormat),
                                "$" + producto.getLimiteCreditoProducto(),
                                producto.getCodigo()
                        );

                        resultadoMenus.add(resultadoMenu);

                    }

                    ResultadosTotalesMenuAdapter resultadosMenuAdapter = new ResultadosTotalesMenuAdapter(MainActivity.this, resultadoMenus);
                    this.recarga_lstResultados.setAdapter(resultadosMenuAdapter);

                    //findViewById(R.id.recarga_tr_resultados).setVisibility(View.VISIBLE);
                    this.recarga_lstResultados.setVisibility(View.VISIBLE);
                    this.recarga_btnVolver.setVisibility(View.VISIBLE);
                    this.recarga_btnImprimirResultados.setVisibility(View.VISIBLE);
                    this.recarga_btnImprimirResultados.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ImprimeTotales();
                        }
                    });

                } else {
                    Toast.makeText(MainActivity.this, "Funcion " + funcion + " no implementada", Toast.LENGTH_LONG).show();
                }


                this.lstMenu.setVisibility(View.GONE);
            } else {
                Toast.makeText(MainActivity.this, "Transaccion no aprobada: " + TransaccionMensajeError(lastmensajeRecarga), Toast.LENGTH_LONG).show();
            }

        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, ex.getMessage().toString(), Toast.LENGTH_LONG).show();
        }
    }

    public void RecargaResponse() {
        try {

            if (TransaccionAprobada(lastmensajeRecarga)) {

                //Toast.makeText(MainActivity.this, lastmensajeRecarga.getMensajeLinea1() + " " + lastmensajeRecarga.getMensajeLinea2() + ". #" + lastmensajeRecarga.getNroTransaccion(), Toast.LENGTH_LONG).show();

                //this.recarga_btnImprimirResultados.setVisibility(View.VISIBLE);
                this.recarga_btnReintentarRecarga.setVisibility(View.GONE);
                this.recarga_btnVolver.setVisibility(View.VISIBLE);

                this.recargaStep = 0;

                List<String> lineas = new ArrayList<String>();
                lineas.add("");
                lineas.add("@C@" + lastmensajeRecarga.getMensajeLinea1());
                lineas.add("@C@" + lastmensajeRecarga.getMensajeLinea2());
                lineas.add("@B@Saldo actual: " + Formatos.FormateaDecimal(lastmensajeRecarga.getSaldoComercio(), Formatos.DecimalFormat, "$"));


                EscribeLineas(lineas, "RECARGA");

                lastTicket++; //incrementamos el proximo ticket por si es necesario para la proxima recarga
                GuardarPreferencias();

                RegistrarVenta(lastmensajeRecarga);

                GeneraTicketRecarga(lastmensajeRecarga, RECARGA_PRODUCTO_OBJ, "ORIGINAL");

                //this.btnCancelar.performClick();

            } else {
                //Toast.makeText(MainActivity.this, "Recarga no aprobada: " + TransaccionMensajeError(lastmensajeRecarga), Toast.LENGTH_LONG).show();

                //en caso de error mostramos el error de forma fija en pantalla
                this.recarga_btnReintentarRecarga.setVisibility(View.GONE);
                this.recarga_btnVolver.setVisibility(View.VISIBLE);

                this.recargaStep = 0;

                List<String> lineas = new ArrayList<String>();
                lineas.add("");
                lineas.add("@C@" + lastmensajeRecarga.getMensajeLinea1());
                lineas.add("@C@" + lastmensajeRecarga.getMensajeLinea2());

                EscribeLineas(lineas, "RECARGA");

            }


        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, ex.getMessage().toString(), Toast.LENGTH_LONG).show();
        }
    }

    public void AvisoDepositoResponse() {
        try {

            if (TransaccionAprobada(lastmensajeRecarga)) {

                ImprimeAvisoDeposito();

                //this.recarga_btnImprimirResultados.setVisibility(View.VISIBLE);
                this.recarga_btnVolver.setVisibility(View.VISIBLE);

                this.avisoDepoStep = 0;

                List<String> lineas = new ArrayList<String>();
                lineas.add("");
                lineas.add("@C@" + lastmensajeRecarga.getMensajeLinea1());
                lineas.add("@C@" + lastmensajeRecarga.getMensajeLinea2());

                EscribeLineas(lineas, "AVISO DEPOSITO");

                //this.btnCancelar.performClick();
                if (DEPOSITO_CLI_VEND.equals("CLI"))
                    Logger.RegistrarEvento(MainActivity.this, "i", "AVISO DEPOSITO CLIENTE", "Aviso informado OK");
                else
                    Logger.RegistrarEvento(MainActivity.this, "i", "AVISO DEPOSITO VENDEDOR", "Aviso informado OK");

            } else {

                this.avisoDepoStep = 0; //cancelamos para que el volver envie al menu principal

                Toast.makeText(MainActivity.this, "Deposito no aprobado: " + TransaccionMensajeError(lastmensajeRecarga), Toast.LENGTH_LONG).show();
            }


        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, ex.getMessage().toString(), Toast.LENGTH_LONG).show();
        }
    }

    public void VentaAbonoResponse() {
        try {

            if (TransaccionAprobada(lastmensajeRecarga)) {

                //ImprimeVentaAbono(); //no se imprime ticket en la venta de abono: solicitado por el cliente

                //this.recarga_btnImprimirResultados.setVisibility(View.VISIBLE);
                this.recarga_btnVolver.setVisibility(View.VISIBLE);

                this.ventaAbonoStep = 0;

                List<String> lineas = new ArrayList<String>();
                lineas.add("");
                lineas.add("@C@" + lastmensajeRecarga.getMensajeLinea1());
                lineas.add("@C@" + lastmensajeRecarga.getMensajeLinea2());

                EscribeLineas(lineas, "AVISO DEPOSITO");

                //this.btnCancelar.performClick();
                Logger.RegistrarEvento(MainActivity.this, "i", "VENTA ABONO", "Monto solicitado: $" + lastmensajeRecarga.getAbono());

            } else {
                Toast.makeText(MainActivity.this, "Venta Abono no aprobada: " + TransaccionMensajeError(lastmensajeRecarga), Toast.LENGTH_LONG).show();
            }


        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, ex.getMessage().toString(), Toast.LENGTH_LONG).show();
        }
    }

    public void RegistroVisitaVendedorResponse() {
        try {

            if (TransaccionAprobada(lastmensajeRecarga)) {

                //this.recarga_btnImprimirResultados.setVisibility(View.VISIBLE);
                this.recarga_btnVolver.setVisibility(View.VISIBLE);

                this.visitaVendStep = 0;

                List<String> lineas = new ArrayList<String>();
                lineas.add("");
                lineas.add("@C@" + lastmensajeRecarga.getMensajeLinea1());
                lineas.add("@C@" + lastmensajeRecarga.getMensajeLinea2());

                EscribeLineas(lineas, "VISITA VENDEDOR");

                //this.btnCancelar.performClick();

                Logger.RegistrarEvento(MainActivity.this, "i", "REGISTRO VISITA VENDEDOR", "Vendedor #" + REGISTRO_NROVENDEDOR + " - Registro OK");

            } else {
                Toast.makeText(MainActivity.this, "Transaccion no aprobada: " + TransaccionMensajeError(lastmensajeRecarga), Toast.LENGTH_LONG).show();
            }


        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, ex.getMessage().toString(), Toast.LENGTH_LONG).show();
        }
    }

    public void ConsultaDatosReferencialesResponse(){
        //actualizamos las listas

        //vamos a recuperar los bancos
        ObtenerListadoBancos();

        //vamos a recuperar los mensajes desde el terminal
        ObtenerListadoMensajesTerminal();

        //vamos a recuperar los datos de aviso
        ObtenerListadoPropsDeposito();

    }

    private boolean TransaccionAprobada(MensajeRecarga mensajeRecarga) {
        return lastmensajeRecarga.getMensajeLinea1().equals("TRANSACCION") && lastmensajeRecarga.getMensajeLinea2().equals("APROBADA");
    }

    private String TransaccionMensajeError(MensajeRecarga mensajeRecarga) {
        return lastmensajeRecarga.getMensajeLinea1() + " " + lastmensajeRecarga.getMensajeLinea2();
    }

    public void LeerRespuesta(String xmlResponseInput) throws Exception {
        try {
            XmlRecargaParser parser = new XmlRecargaParser();

            lastmensajeRecarga = parser.parse(new ByteArrayInputStream(xmlResponseInput.getBytes(XmlRecargaParser.ENCODING)));

        } catch (XmlPullParserException xppe) {
            throw new Exception("Mensaje no valido: " + xppe.getMessage());
        } catch (Exception e) {
            throw e;
        }

    }

    //endregion

    //region SECCION MENSAJERIA
    private void ParamsComunes(MensajeRecarga mensajeRecarga, String codigoProceso) {

        mensajeRecarga.setRoot_CodigoProceso(codigoProceso);
        mensajeRecarga.setRoot_TipoMensaje(MessageType.REQUEST);
        //mensajeRecarga.setRoot_NumeroComercio(RECARGA_NROCOMERCIO);
        mensajeRecarga.setRoot_NumeroComercio(Preferences.getString(MainActivity.this, Preferences.KEY_CFG_COD_COM));

        mensajeRecarga.setNroAuditoriaTerminal(String.valueOf(proxNroAudit)); //incrementable en cada trx
        mensajeRecarga.setFechaHoraTerminal(Utils.Util_FechaHoraTerminal()); //"2013-12-20T12:31:00"
        mensajeRecarga.setModoEntradaTerminal(RECARGA_MODOENTRADA);
        mensajeRecarga.setCondicionTerminal(RECARGA_CONDTERMINAL);
        //mensajeRecarga.setIdentTerminal(RECARGA_IDENTTERMINAL);
        mensajeRecarga.setIdentTerminal(Preferences.getString(MainActivity.this, Preferences.KEY_CFG_ID_TERM));
        //mensajeRecarga.setIMEITerminal(RECARGA_IMEITERMINAL);
        mensajeRecarga.setIMEITerminal(this.config.IMEI());//IMEI terminal
        mensajeRecarga.setVapp(RECARGA_VAPP);

        //formateamos el ticket para el proximo uso
        TicketFormatearProx();
        mensajeRecarga.setNroTickets(RECARGA_NROTICKET);//ttttlll : #ticket#lote
    }

    private void EnviaMensajeConsultaComercial(String accionInvoca, String paramsAccionInvoca) {

        //creamos un objeto mensajeRecarga con una consultaComercial de prueba
        MensajeRecarga mensajeRecarga = new MensajeRecarga();

        ParamsComunes(mensajeRecarga, ProcessingCode.CONSULTA_COMERCIAL);

        //mensajeRecarga.setIMSITerminal(RECARGA_IMSITERMINAL); //Nro SIM
        mensajeRecarga.setIMSITerminal(this.config.IMSI); //Nro SIM
        mensajeRecarga.setNroSerieTerminal(""); //NroSerie terminal
        //mensajeRecarga.setNroSerieSIM(RECARGA_SERIALSIM);  //nroSerie SIM
        mensajeRecarga.setNroSerieSIM(this.config.SIM_SERIALNUMBER); //nroSerie SIM

        EnviarMensaje(mensajeRecarga, accionInvoca, paramsAccionInvoca);
    }

    private void EnviaMensajeVentaAbonoComercio(String accionInvoca) {

        //creamos un objeto mensajeRecarga con una transaccion de registro de visita vendedor
        MensajeRecarga mensajeRecarga = new MensajeRecarga();

        ParamsComunes(mensajeRecarga, ProcessingCode.VENTA_ABONO_COMERCIO);

        mensajeRecarga.setNroVendedor(VENTAABONO_NROVENDEDOR);
        mensajeRecarga.setPINVendedor(VENTAABONO_NROPIN);
        mensajeRecarga.setMoneda(RECARGA_MONEDA);

        String numFC = "0000000000" + VENTAABONO_NROFACTURA;

        String tipoPago = "";
        if (VENTAABONO_TIPOPAGO.equals("EFECTIVO"))
            tipoPago = "EFE";
        else if (VENTAABONO_TIPOPAGO.equals("CHEQUE"))
            tipoPago = "CHE";
        else if (VENTAABONO_TIPOPAGO.equals("TARJETA CREDITO"))
            tipoPago = "CRD";

        //CHE Cheque
        //CRD Credito
        //EFE Efectivo
        //NCR Nota de Crédito

        mensajeRecarga.setDatosAdicionales(tipoPago + numFC.substring(numFC.length() - 10));

        mensajeRecarga.setTipoProductoConsumido("10");//RECARGA GENERICA
        mensajeRecarga.setMontoAbono(VENTAABONO_MONTO);

        EnviarMensaje(mensajeRecarga, accionInvoca, "");
    }

    private void EnviaMensajeRegistroVisitaVendedor(String accionInvoca) {

        //creamos un objeto mensajeRecarga con una transaccion de registro de visita vendedor
        MensajeRecarga mensajeRecarga = new MensajeRecarga();

        ParamsComunes(mensajeRecarga, ProcessingCode.VISITA_VENDEDOR);

        mensajeRecarga.setNroVendedor(REGISTRO_NROVENDEDOR);
        mensajeRecarga.setPINVendedor(REGISTRO_NROPIN);

        EnviarMensaje(mensajeRecarga, accionInvoca, "");
    }

    private void EnviaMensajeUltRecarga(String accionInvoca) {

        if (lastRecarga != null) {

            lastRecarga.setRoot_CodigoProceso(ProcessingCode.CONSULTA_TRANSACCION_RECARGA_NORMAL); //cambiamos el codigo para hacer la consulta de ult recarga

            EnviarMensaje(lastRecarga, accionInvoca, "");

        } else {
            List<String> lineas = new ArrayList<String>();
            lineas.add("No se encontró el registro original");

            EscribeLineas(lineas, "ULTIMA RECARGA");

            this.recarga_btnVolver.setVisibility(View.VISIBLE);
        }

    }

    private void EnviaMensajeConsultaMensajes(String accionInvoca) {
        //creamos un objeto mensajeRecarga con una consulta de mensajes de prueba
        MensajeRecarga mensajeRecarga = new MensajeRecarga();

        ParamsComunes(mensajeRecarga, ProcessingCode.CONSULTA_MENSAJES_LEIDOS);

        EnviarMensaje(mensajeRecarga, accionInvoca, "");
    }

    private void EnviaMensajeUltimosAbonos(String accionInvoca) {
        //creamos un objeto mensajeRecarga con una consulta de ultimos abonos
        MensajeRecarga mensajeRecarga = new MensajeRecarga();

        ParamsComunes(mensajeRecarga, ProcessingCode.CONSULTA_ULTIMOS_ABONOS);

        EnviarMensaje(mensajeRecarga, accionInvoca, "");
    }

    private void EnviaMensajeVentas(String accionInvoca, String funcion) {

        //creamos un objeto mensajeRecarga con una consulta de ultimas ventas
        MensajeRecarga mensajeRecarga = new MensajeRecarga();

        ParamsComunes(mensajeRecarga, ProcessingCode.CONSULTA_VENTAS);

        if (funcion.equals("CIERRE_TURNO")) {
            mensajeRecarga.setCorteInicio(CIERRE_FECHA_INICIO);
            mensajeRecarga.setCorteFin(CIERRE_FECHA_CIERRE);

            //mensajeRecarga.setCorteInicio(Util_FechaTerminal() + CIERRE_FECHA_INICIO.substring(0, 2) + ":" + CIERRE_FECHA_INICIO.substring(2, 4) + ":00"); //desde las cero horas de hoy YYYY-MM-DDTHH:MI:SS
            //mensajeRecarga.setCorteFin(Util_FechaTerminal() + CIERRE_FECHA_CIERRE.substring(0, 2) + ":" + CIERRE_FECHA_CIERRE.substring(2, 4) + ":00"); //hasta el fin del dia de hoy
        }

        EnviarMensaje(mensajeRecarga, accionInvoca, funcion);
    }

    private void EnviaMensajeUltimasOperaciones(String accionInvoca) {
        //creamos un objeto mensajeRecarga con una consulta de ultima recarga de prueba
        MensajeRecarga mensajeRecarga = new MensajeRecarga();

        ParamsComunes(mensajeRecarga, ProcessingCode.CONSULTA_ULTIMAS_RECARGAS);

        EnviarMensaje(mensajeRecarga, accionInvoca, "");
    }

    private void EnviaMensajeAvisoDepositoCliente(String accionInvoca) {

        //creamos un objeto mensajeRecarga con una transaccion de aviso de deposito de cliente
        MensajeRecarga mensajeRecarga = new MensajeRecarga();

        ParamsComunes(mensajeRecarga, ProcessingCode.AVISO_DEPOSITO_CLIENTE);

        mensajeRecarga.setFechaDeposito(DEPOSITO_FECHA);
        mensajeRecarga.setLugarDeDeposito(DEPOSITO_BANCO);
        mensajeRecarga.setMontoDepositado(DEPOSITO_MONTO);
        mensajeRecarga.setNroComprobante(DEPOSITO_NUMERO);
        mensajeRecarga.setTipoDeposito(DEPOSITO_TIPO);
        mensajeRecarga.setOrigenCheque(DEPOSITO_BANCO_CHEQUE);
        mensajeRecarga.setNroCheque(DEPOSITO_CHEQUE);
        mensajeRecarga.setTipoProductoConsumido("10"); //RECARGA GENERICA

        EnviarMensaje(mensajeRecarga, accionInvoca, "");
    }

    private void EnviaMensajeAvisoDepositoVendedor(String accionInvoca) {

        //creamos un objeto mensajeRecarga con una transaccion de aviso de deposito de cliente
        MensajeRecarga mensajeRecarga = new MensajeRecarga();

        ParamsComunes(mensajeRecarga, ProcessingCode.AVISO_DEPOSITO_VENDEDOR);

        mensajeRecarga.setNroVendedor(DEPOSITO_NROVENDEDOR);
        mensajeRecarga.setPINVendedor(DEPOSITO_NROPIN);

        mensajeRecarga.setFechaDeposito(DEPOSITO_FECHA);
        mensajeRecarga.setLugarDeDeposito(DEPOSITO_BANCO);
        mensajeRecarga.setMontoDepositado(DEPOSITO_MONTO);
        mensajeRecarga.setNroComprobante(DEPOSITO_NUMERO);
        mensajeRecarga.setTipoDeposito(DEPOSITO_TIPO);
        mensajeRecarga.setOrigenCheque(DEPOSITO_BANCO_CHEQUE);
        mensajeRecarga.setNroCheque(DEPOSITO_CHEQUE);

        EnviarMensaje(mensajeRecarga, accionInvoca, "");
    }

    private void EnviaMensajeRecarga(String accionInvoca) {

        //creamos un objeto mensajeRecarga con una consultaComercial de prueba
        MensajeRecarga mensajeRecarga = new MensajeRecarga();

        ParamsComunes(mensajeRecarga, ProcessingCode.RECARGA_NORMAL);

        mensajeRecarga.setNroTarjeta(RECARGA_PRODUCTO_OBJ.getPrefijo() + RECARGA_NROTEL); //sumamos el prefijo del producto + numero telefono
        mensajeRecarga.setMontoTransaccion(RECARGA_MONTO); //monto a recargar
        mensajeRecarga.setTipoProductoConsumido(RECARGA_PRODUCTO_OBJ.getCodigo()); //producto
        mensajeRecarga.setMoneda(RECARGA_MONEDA);

        String cod_medio = "66";
        if (RECARGA_MEDIOPAGO.equals("TARJETA CREDITO"))
            cod_medio = "77";

        mensajeRecarga.setDatosAdicionales(cod_medio + "000000000000"); //66 efvo o 77 tarjeta + "000000000000"

        //guardamos la informacion de la ultima recarga enviada para la "Consulta de Ult Recarga"
        lastRecarga = new MensajeRecarga(mensajeRecarga);
        GuardarLastMensajeRecarga(lastRecarga);//guardamos para poder recuperarlo en la reapertura de la aplicacion

        EnviarMensaje(mensajeRecarga, accionInvoca, "");
    }

    private void EnviaMensajeConsultaDatosReferenciales(String accionInvoca) {

        //creamos un objeto mensajeRecarga con una consulta de datos referenciales
        MensajeRecarga mensajeRecarga = new MensajeRecarga();

        ParamsComunes(mensajeRecarga, ProcessingCode.CONSULTA_DATOS_REFERENCIALES);

        EnviarMensaje(mensajeRecarga, accionInvoca, "");
    }

    private void EnviaMensajeDesdeTerminal(String accionInvoca, String codigoSeleccionado) {

        //creamos un objeto mensajeRecarga
        MensajeRecarga mensajeRecarga = new MensajeRecarga();

        ParamsComunes(mensajeRecarga, ProcessingCode.MENSAJES_TERMINAL);

        mensajeRecarga.setCodigoMensaje(codigoSeleccionado);

        EnviarMensaje(mensajeRecarga, accionInvoca, "");
    }

    private void EnviarMensaje(MensajeRecarga mensajeRecarga, String proximaAccion, String paramsProximaAccion) {
        try {

            if (!ConnectivityUtils.isOnline(MainActivity.this)) {
                throw new Exception("No hay una conexion disponible. Intente mas tarde.");
            } else {

                StringWriter buffer = new StringWriter();
                mensajeRecarga.toXML(buffer);

                //incrementamos el proximo numero
                proxNroAudit = proxNroAudit < 999999 ? proxNroAudit + 1 : proxNroAudit - 999999; //da hasta 999999, cuando llegue hasta 999999 arranca en 0 de nuevo
                GuardarPreferencias();

                new ExecuteRequestTask(proximaAccion, paramsProximaAccion).execute(buffer.toString());

            }
        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();

            flagObtieneProductos = false;
        }

    }
    //endregion

    //region SECCION CONECTIVIDAD
    private class ExecuteRequestTask extends AsyncTask<String, Void, String> {

        private String nextAction = "";
        private String paramsNextAction = "";

        //constructor
        public ExecuteRequestTask(String NextAction, String ParamsNextAction) {
            this.nextAction = NextAction;
            this.paramsNextAction = ParamsNextAction;
        }

        // onPreExecute used to setup the AsyncTask.
        @Override
        protected void onPreExecute() {
            showProgress(true);
            _recargaStatusMessageView.setText(flagObtieneProductos ? "Consultando productos habilitados..." : "Enviando mensaje...");
        }

        @Override
        protected String doInBackground(String... messages) {

            // params comes from the execute() call: params[0] is the message.
            try {
                WebServiceRequest webServiceRequest = new WebServiceRequest();

                //String response = webServiceRequest.callWebService("http://200.69.238.23:80/PAT/Core/PortPOS64/FidPort.asp", messages[0]);
                //String response = webServiceRequest.callWebService("http://" + Preferences.getString(MainActivity.this, Preferences.KEY_CFG_IP_SERV) + ":" + Preferences.getString(MainActivity.this, Preferences.KEY_CFG_PORT_SERV) + "/PAT/Core/PortPOS64/FidPort.asp", messages[0]);
                //NUEVA URL - SEGURIDAD PARA APP RECARGA BPOS4
                //UKey_Android:UKey16@
                String response = webServiceRequest.callHttpsWebService("https://" + Preferences.getString(MainActivity.this, Preferences.KEY_CFG_IP_SERV) + ":" + Preferences.getString(MainActivity.this, Preferences.KEY_CFG_PORT_SERV) + "/PAT/Core/PortAd/FidPort.asp", messages[0]);

                publishProgress();

                LeerRespuesta(response);

                //guardamos la informacion para un posterior uso (y evitar consultar constatemente)
                //guardamos solo si la transaccion está aprobada
                if(TransaccionAprobada(lastmensajeRecarga)) {
                    if (flagObtieneProductos)
                        Preferences.setString(MainActivity.this, Preferences.KEY_CONS_COMERC, response);
                    //si es CONSULTA DE DATOS REFERENCIALES => GUARDAMOS ENTIDADES
                    else if (lastmensajeRecarga.getRoot_CodigoProceso().equals(ProcessingCode.CONSULTA_DATOS_REFERENCIALES)) {
                        //vamos a guardar los XMLs de las entidades
                        InputStream in = new ByteArrayInputStream(response.getBytes(XmlRecargaParser.ENCODING));

                        //guardamos los mensajes del terminal:
                        Preferences.setString(MainActivity.this, Preferences.KEY_LIST_MENSAJES, XmlRecargaParser.extractXML(in, "FID_TM_RES/TM0210305105/MSGPOS", false));

                        in.reset();

                        //guardamos los bancos:
                        Preferences.setString(MainActivity.this, Preferences.KEY_LIST_BANCOS, XmlRecargaParser.extractXML(in, "FID_TM_RES/TM0210305105/BANCOS", false));

                        in.reset();

                        //guardamos los avisos de deposito:
                        Preferences.setString(MainActivity.this, Preferences.KEY_LIST_DEP_AVISO, XmlRecargaParser.extractXML(in, "FID_TM_RES/TM0210305105/DAVISO", true));
                    }
                }
                return "";

            } catch (IOException e) {
                flagObtieneProductos = false;

                return "No es posible conectarse. Intente mas tarde.";
            } catch (Exception e) {
                flagObtieneProductos = false;

                return "Error: " + e.getMessage();
            }
        }

        @Override
        protected void onProgressUpdate(Void... v) {
            _recargaStatusMessageView.setText("Leyendo respuesta...");
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            try {
                showProgress(false);

                if (result.equals("") && !this.nextAction.equals("")) {

                    if (!this.paramsNextAction.equals("")) {
                        //si tiene param
                        Method accion = MainActivity.this.getClass().getMethod(this.nextAction, this.paramsNextAction.getClass());

                        accion.invoke(MainActivity.this, this.paramsNextAction);
                    } else {
                        //sino tiene param
                        Method accion = MainActivity.this.getClass().getMethod(this.nextAction, null);

                        accion.invoke(MainActivity.this, null);
                    }


                } else {
                    flagObtieneProductos = false;

                    Toast.makeText(MainActivity.this, result.toString(), Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {

                flagObtieneProductos = false;

                Toast.makeText(MainActivity.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }


        @Override
        protected void onCancelled() {
            showProgress(false);
        }
    }


    /**
     * Shows the progress UI and hides the main layout.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            _recargaStatusView.setVisibility(View.VISIBLE);
            _recargaStatusView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(show ? 1 : 0)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            _recargaStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
                        }
                    });

            _recargaView.setVisibility(View.VISIBLE);
            _recargaView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(show ? 0 : 1)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            _recargaView.setVisibility(show ? View.GONE : View.VISIBLE);
                        }
                    });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            _recargaStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
            _recargaView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    //endregion

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/


    public void SolicitarClaveVendedor(String proxAccion) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        final String _proxAccion = proxAccion;

        // Get the layout inflater
        LayoutInflater inflater = MainActivity.this.getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(inflater.inflate(R.layout.dialog_clave_vend, null))
                // Add action buttons
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        try {

                            String passIngresada = ((EditText) ((AlertDialog) dialog).findViewById(R.id.dialog_clave_vend_password)).getText().toString();

                            if (passIngresada.equals(RECARGA_CLAVEVENDEDOR)) {
                                // iniciamos la accion
                                Method accion = MainActivity.this.getClass().getMethod(_proxAccion, null);

                                accion.invoke(MainActivity.this, null);
                            } else {
                                Toast.makeText(MainActivity.this, "Clave ingresada erronea", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            Toast.makeText(MainActivity.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        AlertDialog dialog = builder.create();

        dialog.setTitle("Ingrese Clave de Vendedor");
        dialog.setCanceledOnTouchOutside(true);
        dialog.setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {

            }
        });

        dialog.show();
    }


}