package com.pds.recarga.conectividad;

import android.util.Base64;

import com.pds.common.SII.vale.util.ValeConst;
import com.pds.common.network.MySSLSocketFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.NTCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.KeyStore;

/**
 * Created by Hernan on 02/01/14.
 */
public class WebServiceRequest {
    public static final int CONNECTION_TIMEOUT = 20000;//the timeout until a connection is established
    public static final int SOCKET_TIMEOUT = 30000; //is the timeout for waiting for data

    //EL TIMEOUT DE 30 SEGUNDOS ES EL RECOMENDADO EN LA DOCUMENTACION DE RECARGA

    public String callHttpsWebService(String url, String mensaje) throws IOException, Exception {

        // 1. create HttpClient
        HttpClient httpClient = new DefaultHttpClient();
        HttpParams params = httpClient.getParams();
        HttpConnectionParams.setConnectionTimeout(params, CONNECTION_TIMEOUT);
        HttpConnectionParams.setSoTimeout(params, SOCKET_TIMEOUT);
        HttpProtocolParams.setUseExpectContinue(httpClient.getParams(), true);

        // 2. create HttpsClient
        KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
        trustStore.load(null, null);

        MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
        sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

        ClientConnectionManager ccm = httpClient.getConnectionManager();

        SchemeRegistry sr = ccm.getSchemeRegistry();
        sr.register(new Scheme("https", sf, 443));

        DefaultHttpClient httpsClient = new DefaultHttpClient(ccm, httpClient.getParams());

        // 3. make POST request to the given URL
        HttpPost httpPost = new HttpPost(url);

        // 4. build POST content
        StringEntity se = new StringEntity(mensaje, HTTP.UTF_8);

        // 5. set httpPost Entity
        httpPost.setEntity(se);

        // 6. Set some headers to authenticate and inform server about the type of the content
        httpPost.addHeader("Content-Type", "text/xml; charset=utf-8");

        String basicAuth = "Basic " + Base64.encodeToString("UKey_Android:UKey16".getBytes(), Base64.NO_WRAP);
        httpPost.addHeader("Authorization", basicAuth);

        // 7. Execute POST request to the given URL
        String responseString = null;

        try {
            //
            HttpEntity entity = new StringEntity(mensaje, HTTP.UTF_8);
            httpPost.setEntity(entity);

            ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
                @Override
                public String handleResponse(HttpResponse httpResponse) throws ClientProtocolException, IOException {
                    HttpEntity entity = httpResponse.getEntity();

                    //modificado porque no se interpretaban correctamente ñ y acentos
                    String response = EntityUtils.toString(entity);

                    return response;

                    //StringBuffer out = new StringBuffer();
                    //byte[] b = EntityUtils.toByteArray(entity);
                    //out.append(new String(b, 0, b.length));

                    //return out.toString();
                }
            };

            responseString = httpsClient.execute(httpPost, responseHandler);
        } catch (UnsupportedEncodingException uee) {
            throw new Exception(uee);
        } catch (ClientProtocolException cpe) {
            throw new Exception(cpe);
        } catch (IOException ioe) {
            throw ioe;
        } finally {
            //cerramos la conexion
            httpClient.getConnectionManager().shutdown();
        }
        return responseString;
    }


    public String callWebService(String url, String mensaje) throws IOException, Exception {
        final DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpParams params = httpClient.getParams();
        HttpConnectionParams.setConnectionTimeout(params, CONNECTION_TIMEOUT);
        HttpConnectionParams.setSoTimeout(params, SOCKET_TIMEOUT);

        HttpProtocolParams.setUseExpectContinue(httpClient.getParams(), true);

        // POST
        HttpPost httpPost = new HttpPost(url);
        httpPost.setHeader("Content-Type", "text/xml; charset=utf-8");

        String responseString = null;

        try {
            //
            HttpEntity entity = new StringEntity(mensaje, HTTP.UTF_8);
            httpPost.setEntity(entity);

            ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
                @Override
                public String handleResponse(HttpResponse httpResponse) throws ClientProtocolException, IOException {
                    HttpEntity entity = httpResponse.getEntity();

                    //modificado porque no se interpretaban correctamente ñ y acentos
                    String response = EntityUtils.toString(entity);

                    return response;

                    //StringBuffer out = new StringBuffer();
                    //byte[] b = EntityUtils.toByteArray(entity);
                    //out.append(new String(b, 0, b.length));

                    //return out.toString();
                }
            };

            responseString = httpClient.execute(httpPost, responseHandler);
        } catch (UnsupportedEncodingException uee) {
            throw new Exception(uee);
        } catch (ClientProtocolException cpe) {
            throw new Exception(cpe);
        } catch (IOException ioe) {
            throw ioe;
        } finally {
            //cerramos la conexion
            httpClient.getConnectionManager().shutdown();
        }
        return responseString;
    }
}
