package com.pds.contador;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Usuario;

public class MainActivity extends Activity {

    private Usuario _user;
    private View ingresoCompraBoletaE;
    private View consultaCompraBoletaE;
    private View consultaVentaBoletaE;
    private View consultaCafBoletaE;
    private View formulario29BoletaE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {

            ((TextView) findViewById(R.id.txtVersion)).setText("Version:" + getPackageManager().getPackageInfo(this.getPackageName().toString(), 0).versionName);

            //logueo Usuario *****************************************************
            //obtenemos los parametros pasamos a la actividad
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                //tomamos el usuario
                this._user = (Usuario) extras.getSerializable("current_user");
            }

            //no tenemos user logueado...volvemos a EntryPoint
            if (_user == null) {
                Toast.makeText(this, R.string.unknown_user, Toast.LENGTH_LONG).show();
                this.finish();
            } else {

                //********* USER LOGUEADO OK *********
                Init_Views();
                Init_Eventos();
            }

        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    private void Init_Views(){
        ingresoCompraBoletaE = findViewById(R.id.contador_ingresar_compras);
        consultaCompraBoletaE = findViewById(R.id.contador_consulta_compras);
        consultaVentaBoletaE = findViewById(R.id.contador_consulta_ventas);
        consultaCafBoletaE = findViewById(R.id.contador_consulta_caf);
        formulario29BoletaE = findViewById(R.id.contador_formulario_29);
    }

    private void Init_Eventos(){
        ingresoCompraBoletaE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent i = new Intent(MainActivity.this, CargaFcActivity.class);
                //startActivity(i);
            }
        });

        consultaCompraBoletaE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent i = new Intent(MainActivity.this, CargaFcActivity.class);
                //startActivity(i);
            }
        });

        consultaVentaBoletaE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent i = new Intent(MainActivity.this, CargaFcActivity.class);
                //startActivity(i);
            }
        });

        consultaCafBoletaE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent i = new Intent(MainActivity.this, CargaFcActivity.class);
                //startActivity(i);
            }
        });

        formulario29BoletaE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent i = new Intent(MainActivity.this, CargaFcActivity.class);
                //startActivity(i);
            }
        });
    }
}
