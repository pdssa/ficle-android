package com.pds.contador;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Formatos;
import com.pds.common.dao.ProductDao;
import com.pds.common.model.CompraDetalle;
import com.pds.common.model.Product;

import java.text.Normalizer;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AddFcItemFragment.FcItemFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AddFcItemFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddFcItemFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "item_edit";

    private CompraDetalle itemEdit;

    private FcItemFragmentInteractionListener mListener;
    private Button btnGrabar;
    private Button btnCancelar;
    private EditText txtCantidad;
    private EditText txtCodigo;
    private EditText txtDescripcion;
    private EditText txtPrecio;
    private EditText txtSubtotal;
    private ImageButton btnFindProd;

    private Product productEdit;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param itemEdit item a editar.
     * @return A new instance of fragment AddFcItemFragment.
     */
    public static AddFcItemFragment newInstance(CompraDetalle itemEdit) {
        AddFcItemFragment fragment = new AddFcItemFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, itemEdit);
        fragment.setArguments(args);
        return fragment;
    }

    public AddFcItemFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            itemEdit = getArguments().getParcelable(ARG_PARAM1);
            if(itemEdit != null) productEdit = itemEdit.getProducto();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_fc_item, container, false);

        Init_Views(view);

        Init_Eventos(view);

        if(itemEdit != null){
            txtCantidad.setText(String.valueOf(itemEdit.getCantidad()));
            txtCodigo.setText(itemEdit.getProducto().getCode());
            txtDescripcion.setText(itemEdit.getProducto().getDescription());
            txtPrecio.setText(Formatos.FormateaDecimal(itemEdit.getPrecio(), Formatos.DecimalFormat_CH));
            txtSubtotal.setText(Formatos.FormateaDecimal(itemEdit.getTotal(), Formatos.DecimalFormat_CH));
        }

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 2) {//requerimos un product
            if(resultCode == Activity.RESULT_OK){
                Object result = data.getParcelableExtra("result");
                SelectProduct((Product) result);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                //SelectProvider(null);
            }
        }
    }

    public void SelectProduct(Product _product){
        if(_product != null){
            productEdit = _product;
            txtCodigo.setError(null);
            txtCodigo.setText(_product.getCode());
            txtDescripcion.setText(_product.getDescription());
            txtPrecio.setText(Formatos.FormateaDecimal(_product.getPurchasePrice(), Formatos.DecimalFormat_CH));
        }
        else{
            //TODO: product not found
            productEdit = null;
            txtDescripcion.setText(null);
            txtPrecio.setText(null);
            txtCodigo.setError("Producto no existente");
        }
    }

    private void Init_Views (View view){
        btnGrabar = (Button) view.findViewById(R.id.frg_add_item_btn_grabar);
        btnCancelar = (Button) view.findViewById(R.id.frg_add_item_btn_cancelar);
        txtCantidad = (EditText) view.findViewById(R.id.frg_add_item_txt_cantidad);
        txtCodigo = (EditText) view.findViewById(R.id.frg_add_item_txt_codigo);
        txtDescripcion = (EditText) view.findViewById(R.id.frg_add_item_txt_descripcion);
        txtPrecio = (EditText) view.findViewById(R.id.frg_add_item_txt_precio);
        txtSubtotal = (EditText) view.findViewById(R.id.frg_add_item_txt_subtotal);
        btnFindProd = (ImageButton) view.findViewById(R.id.fc_add_item_btn_find_prod);
    }

    private void Init_Eventos (View view){
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cerrar();
            }
        });
        btnGrabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GrabarItem();
            }
        });
        view.findViewById(R.id.img_dummy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cerrar();
            }
        });
        view.findViewById(R.id.frg_add_item_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {            }
        });
        btnFindProd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClassName("com.pds.ficle.gestprod", "com.pds.ficle.gestprod.activity.ProductManagerActivity");
                intent.putExtra("search", true);
                startActivityForResult(intent, 2);
            }
        });
        txtCodigo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(!hasFocus)
                    Toast.makeText(getActivity(), ((EditText)view).getText(), Toast.LENGTH_SHORT).show();
            }
        });
        txtCantidad.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(!hasFocus){
                    if(!TextUtils.isEmpty(txtCantidad.getText().toString()) && !TextUtils.isEmpty(txtPrecio.getText().toString())){
                        double cantidad = Formatos.ParseaDecimal(txtCantidad.getText().toString(), Formatos.DecimalFormat_CH);
                        double precio = Formatos.ParseaDecimal(txtPrecio.getText().toString(), Formatos.DecimalFormat_CH);

                        double subtotal = cantidad * precio;

                        txtSubtotal.setText(Formatos.FormateaDecimal(subtotal, Formatos.DecimalFormat_CH));
                    }
                }

            }
        });
    }

    private void Cerrar(){
        FragmentManager fm = getActivity().getFragmentManager();

        Fragment fragment = fm.findFragmentById(R.id.frgAddEditItem);

        FragmentTransaction ft = fm.beginTransaction();

        ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);

        ft.hide(fragment);

        ft.commit();
    }

    public void GrabarItem() {
        try {
            boolean newItem = itemEdit == null;

            if (newItem)
                itemEdit = new CompraDetalle();


            int cantidad = Integer.parseInt(txtCantidad.getText().toString());
            double precio = Formatos.ParseaDecimal(txtPrecio.getText().toString(), Formatos.DecimalFormat_CH);

            itemEdit.setIdProducto(productEdit.getId());
            itemEdit.setProducto(productEdit);
            itemEdit.setCantidad(cantidad);
            itemEdit.setPrecio(precio);

            double subtotal = itemEdit.getPrecio() * itemEdit.getCantidad();
            itemEdit.setSubtotal_exento(0);
            itemEdit.setSubtotal_neto_grav(0);
            itemEdit.setSubtotal_impuesto(0);

            if(productEdit.getTax().getId() == 4 ) //EXENTO
                itemEdit.setSubtotal_exento(subtotal);
            else //GRAVADO, NO GRAVADO, INCLUIDO
            {
                double iva = productEdit.getIva() / 100;

                itemEdit.setSubtotal_neto_grav(subtotal);
                itemEdit.setSubtotal_impuesto(subtotal * iva);
            }

            itemEdit.setTotal(itemEdit.getSubtotal_exento() + itemEdit.getSubtotal_neto_grav() + itemEdit.getSubtotal_impuesto());

            if (mListener != null) {
                if (newItem)
                    mListener.onItemCreated(itemEdit);
                else
                    mListener.onItemEdited(itemEdit);
            }

            Cerrar();

        } catch (Exception ex) {
            Toast.makeText(AddFcItemFragment.this.getActivity(), "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (FcItemFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement FcItemFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface FcItemFragmentInteractionListener {
        public void onItemCreated(CompraDetalle new_item);
        public void onItemEdited(CompraDetalle edited_item);
    }

    public void setFcItemFragmentInteractionListener(FcItemFragmentInteractionListener mListener) {
        this.mListener = mListener;
    }
}
