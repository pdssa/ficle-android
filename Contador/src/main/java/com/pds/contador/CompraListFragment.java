package com.pds.contador;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.pds.common.Formatos;
import com.pds.common.dao.CompraDao;
import com.pds.common.model.Compra;

import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * <p/>
 */
public class CompraListFragment extends ListFragment {

    CompraArrayAdapter listAdapter;
    List<Compra> list;


    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CompraListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        list = getList();
        listAdapter = new CompraArrayAdapter(getActivity(), list);
        setListAdapter(listAdapter);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        list.clear();
        list.addAll(getList());
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Compra compra = (Compra) getListAdapter().getItem(position);

        Intent intent = new Intent(getActivity(), CargaFcActivity.class);
        intent.putExtra("id_compra", compra.getId());
        getActivity().startActivity(intent);
        getActivity().overridePendingTransition(0, 0);
    }


    private List<Compra> getList() {
        return new CompraDao(this.getActivity().getContentResolver()).list(null, null, "_id DESC");
    }

    public class CompraArrayAdapter extends ArrayAdapter<Compra> {
        private final Context context;
        private List<Compra> compras;

        public CompraArrayAdapter(Context context, List compras) {
            super(context, R.layout.list_compra, compras);
            this.context = context;
            this.compras = compras;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View rowView = inflater.inflate(R.layout.list_compra, parent, false);
            if (position % 2 != 0)
                rowView.setBackgroundColor(Color.LTGRAY);
            else
                rowView.setBackgroundColor(Color.WHITE);

            ((TextView) rowView.findViewById(R.id.list_compra_fecha)).setText(Formatos.FormateaDate(compras.get(position).getFecha(), Formatos.DateFormatSlash));
            ((TextView) rowView.findViewById(R.id.list_compra_proveedor)).setText(compras.get(position).getProveedor().getName());
            ((TextView) rowView.findViewById(R.id.list_compra_numero)).setText(compras.get(position).getNumero());
            ((TextView) rowView.findViewById(R.id.list_compra_total)).setText(Formatos.FormateaDecimal(compras.get(position).getTotal(), Formatos.DecimalFormat_CH, "$"));

            return rowView;
        }

    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.

     public interface OnFragmentInteractionListener {
     // TODO: Update argument type and name
     public void onFragmentInteraction(String id);
     }
     */
}
