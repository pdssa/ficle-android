package com.pds.contador;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.DateTimePicker;
import com.pds.common.Formatos;
import com.pds.common.dao.CompraDao;
import com.pds.common.dao.CompraDetalleDao;
import com.pds.common.dao.ProductDao;
import com.pds.common.dao.ProviderDao;
import com.pds.common.model.Compra;
import com.pds.common.model.CompraDetalle;
import com.pds.common.model.Product;
import com.pds.common.model.Provider;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class CargaFcActivity extends Activity implements AddFcItemFragment.FcItemFragmentInteractionListener{

    private Button btnGrabar;
    private Button btnCancelar;
    private EditText txtFecha;
    private EditText txtNumero;
    private ImageButton btnFindProveedor;
    private Button btnAddItem;
    private EditText txtTaxId;
    private TextView txtRazonSocial;
    private Compra _cabecera;
    private List<CompraDetalle> _items;
    private ListView lstDetalleCompra;
    private ItemsArrayAdapter listAdapter;
    private TextView txtExento;
    private TextView txtNeto;
    private TextView txtImpuesto;
    private TextView txtTotal;

    private int editPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carga_fc);

        AsignarViews();
        AsignarEventos();

        Intent i = getIntent();
        long idCompra = i.getLongExtra("id_compra", 0);
        if(idCompra != 0){
            ViewCompra(idCompra);
        }
        else {
            NuevaCompra();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {//requerimos un provider
            if(resultCode == RESULT_OK){
                Object result = data.getParcelableExtra("result");
                SelectProvider((Provider)result);
            }
            if (resultCode == RESULT_CANCELED) {
                //Write your code if there's no result
                //SelectProvider(null);
            }
        }
    }

    private void AsignarViews() {
        btnGrabar = (Button) findViewById(R.id.fc_btn_grabar);
        btnCancelar = (Button) findViewById(R.id.fc_btn_cancelar);
        btnFindProveedor = (ImageButton) findViewById(R.id.fc_ibtn_find_prov);
        btnAddItem = (Button) findViewById(R.id.fc_btn_add_item);
        txtFecha = (EditText) findViewById(R.id.fc_txt_fecha);
        txtNumero = (EditText) findViewById(R.id.fc_txt_numero);
        txtTaxId = (EditText) findViewById(R.id.fc_txt_tax_id);
        txtRazonSocial = (TextView) findViewById(R.id.fc_txt_proveedor);
        lstDetalleCompra = (ListView) findViewById(R.id.compras_item_list_container);
        //totales
        txtExento = (TextView) findViewById(R.id.fc_txt_exento);
        txtNeto = (TextView) findViewById(R.id.fc_txt_neto);
        txtImpuesto = (TextView) findViewById(R.id.fc_txt_iva);
        txtTotal = (TextView) findViewById(R.id.fc_txt_total);
    }

    private void AsignarEventos() {
        btnGrabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    _cabecera.setNumero(txtNumero.getText().toString().trim());

                    if (!new CompraDao(getContentResolver()).save(_cabecera)) {
                        Toast.makeText(CargaFcActivity.this, "Error al grabar la fc ", Toast.LENGTH_SHORT).show();
                    }
                    else {

                        CompraDetalleDao compraDetalleDao = new CompraDetalleDao(getContentResolver());
                        ProductDao productDao = new ProductDao(getContentResolver(),new Config(CargaFcActivity.this).SYNC_STOCK ? 1 : 0);

                        for(CompraDetalle det : _items){

                            //asociamos la cabecera y grabamos
                            det.setCompra(_cabecera);

                            compraDetalleDao.save(det);

                            //actualizamos el producto con la info de la compra
                            Product _prd = det.getProducto();

                            _prd.setStock(_prd.getStock() + det.getCantidad());
                            _prd.setPurchasePrice(det.getPrecio());

                            productDao.saveOrUpdate(_prd);
                        }

                        Toast.makeText(CargaFcActivity.this, "Compra generada correctamente!", Toast.LENGTH_SHORT).show();

                        NuevaCompra();
                    }


                } catch (Exception ex) {
                    Toast.makeText(CargaFcActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IniciarFragmentAddEditItem();
            }
        });

        btnFindProveedor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClassName("com.pds.ficle.gestprod", "com.pds.ficle.gestprod.activity.ProviderManagerActivity");
                intent.putExtra("search", true);
                startActivityForResult(intent, 1);
                overridePendingTransition(0, 0);
            }
        });

        lstDetalleCompra.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CompraDetalle det = (CompraDetalle)parent.getItemAtPosition(position);

                editPosition = position;

                IniciarFragmentAddEditItem(det);
            }
        });


        txtTaxId.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(!hasFocus) {
                    List<Provider> providers = new ProviderDao(getContentResolver()).list("tax_id = '" +  ((EditText)view).getText().toString() + "'", null, null);

                    if(providers.size() > 0){
                        SelectProvider(providers.get(0));
                    }
                    else{
                        SelectProvider(null);
                    }
                }
            }
        });

        txtFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PickDate();
            }
        });
        txtFecha.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) { if(b) PickDate();}
        });
    }

    public void PickDate(){
        Calendar hoy = Calendar.getInstance();
        DatePickerDialog datePicker = new DatePickerDialog(CargaFcActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                Calendar fecha = Calendar.getInstance();
                fecha.set(year, monthOfYear, dayOfMonth);

                _cabecera.setFecha(fecha.getTime());

                txtFecha.setText(Formatos.FormateaDate(_cabecera.getFecha(), Formatos.DateFormatSlash));
            }
        }, hoy.get(Calendar.YEAR), hoy.get(Calendar.MONTH), hoy.get(Calendar.DAY_OF_MONTH));
        datePicker.show();
    }

    public void SelectProvider(Provider _provider){
        if(_provider != null){
            _cabecera.setProveedor(_provider);
            txtRazonSocial.setText(_provider.getName());
            txtTaxId.setError(null);
            txtTaxId.setText(_provider.getTax_id());
        }
        else{
            //TODO: provider not found
            txtRazonSocial.setText("");
            txtTaxId.setError("Proveedor no existente");
        }
    }

    private void NuevaCompra(){
        this._cabecera = new Compra();

        editPosition  = 0;

        if(_items == null) {
            _items = new ArrayList<CompraDetalle>();
            listAdapter = new ItemsArrayAdapter(this, _items);
            lstDetalleCompra.setAdapter(listAdapter);
        }
        else {
            this._items.clear();
            listAdapter.notifyDataSetChanged();
        }

        txtFecha.setText(null);
        txtFecha.setError(null);
        txtTaxId.setText(null);
        txtTaxId.setError(null);
        txtRazonSocial.setText(null);
        txtNumero.setText(null);
        txtNumero.setError(null);

        ActualizarTotal();
    }

    private void ViewCompra(long idCompra){
        this._cabecera = new CompraDao(getContentResolver()).find(idCompra);

        txtFecha.setText(Formatos.FormateaDate(this._cabecera.getFecha(), Formatos.DateFormatSlash));
        txtTaxId.setText(this._cabecera.getProveedor().getTax_id());
        txtRazonSocial.setText(this._cabecera.getProveedor().getName());
        txtNumero.setText(this._cabecera.getNumero());

        this._items = new CompraDetalleDao(getContentResolver()).list(String.format("id_compra = %d", idCompra), null, null);
        listAdapter = new ItemsArrayAdapter(this, _items);
        lstDetalleCompra.setAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();

        btnAddItem.setEnabled(false);
        btnFindProveedor.setEnabled(false);
        btnGrabar.setVisibility(View.GONE);
        btnCancelar.setText("VOLVER");

        txtFecha.setEnabled(false);
        txtTaxId.setEnabled(false);
        txtNumero.setEnabled(false);

        lstDetalleCompra.setOnItemClickListener(null);

        ActualizarTotal();
    }


    @Override
    public void onItemCreated(CompraDetalle new_item) {
        this._items.add(new_item);
        //listAdapter = new ItemsArrayAdapter(this, _items);
        listAdapter.notifyDataSetChanged();

        ActualizarTotal();
    }

    @Override
    public void onItemEdited(CompraDetalle edited_item) {
        _items.set(editPosition, edited_item);

        listAdapter.notifyDataSetChanged();

        ActualizarTotal();
    }

    private void ActualizarTotal(){

        double exento = 0, neto = 0, iva = 0, total = 0;

        for(CompraDetalle det : _items){
            exento += det.getSubtotal_exento();
            neto += det.getSubtotal_neto_grav();
            iva += det.getSubtotal_impuesto();
            total += det.getTotal();
        }

        //asociamos a la cabecera
        _cabecera.setSubtotal_exento(exento);
        _cabecera.setSubtotal_impuesto(iva);
        _cabecera.setSubtotal_neto_grav(neto);
        _cabecera.setTotal(total);

        //escribimos los totales
        txtExento.setText(Formatos.FormateaDecimal(exento, Formatos.DecimalFormat_CH, "$"));
        txtNeto.setText(Formatos.FormateaDecimal(neto, Formatos.DecimalFormat_CH, "$"));
        txtImpuesto.setText(Formatos.FormateaDecimal(iva, Formatos.DecimalFormat_CH, "$"));
        txtTotal.setText(Formatos.FormateaDecimal(total, Formatos.DecimalFormat_CH, "$"));

    }

    public void IniciarFragmentAddEditItem() {
        IniciarFragmentAddEditItem(null);
    }
    public void IniciarFragmentAddEditItem(CompraDetalle item_edit) {
        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.frgAddEditItem) != null) {

            // Create a new Fragment to be placed in the activity layout
            AddFcItemFragment fragment = new AddFcItemFragment().newInstance(item_edit);

            //AddFcItemFragment fragment = new AddFcItemFragment();
            fragment.setFcItemFragmentInteractionListener(this);

            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.frgAddEditItem, fragment);
            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            ft.commit();
        }
    }

    public class ItemsArrayAdapter extends ArrayAdapter<CompraDetalle> {
        private final Context context;
        private List<CompraDetalle> items;

        public ItemsArrayAdapter(Context context, List<CompraDetalle> items) {
            super(context, R.layout.list_item_compra, items);
            this.context = context;
            this.items = items;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View rowView = inflater.inflate(R.layout.list_item_compra, parent, false);
            if (position % 2 != 0)
                rowView.setBackgroundColor(Color.LTGRAY);
            else
                rowView.setBackgroundColor(Color.WHITE);

            ((TextView) rowView.findViewById(R.id.list_item_compra_codigo)).setText(items.get(position).getProducto().getCode());
            ((TextView) rowView.findViewById(R.id.list_item_compra_descripcion)).setText(items.get(position).getProducto().getDescription());
            ((TextView) rowView.findViewById(R.id.list_item_compra_cantidad)).setText(String.valueOf(items.get(position).getCantidad()));
            ((TextView) rowView.findViewById(R.id.list_item_compra_precio)).setText(Formatos.FormateaDecimal(items.get(position).getPrecio(), Formatos.DecimalFormat_CH, "$"));
            ((TextView) rowView.findViewById(R.id.list_item_compra_subtotal)).setText(Formatos.FormateaDecimal(items.get(position).getTotal(),Formatos.DecimalFormat_CH, "$"));

            return rowView;
        }

    }

}
