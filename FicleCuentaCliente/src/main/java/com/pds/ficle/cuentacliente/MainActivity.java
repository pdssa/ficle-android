package com.pds.ficle.cuentacliente;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Caja;
import com.pds.common.Config;
import com.pds.common.Formato;
import com.pds.common.Formatos;
import com.pds.common.Logger;
import com.pds.common.Usuario;
import com.pds.common.activity.TimerActivity;
import com.pds.common.dao.ClienteDao;
import com.pds.common.dao.CuentaCteDao;
import com.pds.common.dao.CuentaCteTotalesDao;
import com.pds.common.dialog.ConfirmDialog;
import com.pds.common.dialog.Dialog;
import com.pds.common.fragment.PadFragment;
import com.pds.common.model.Cliente;
import com.pds.common.model.CuentaCte;
import com.pds.common.model.CuentaCteTotal;
import com.pds.common.util.Window;

import java.text.DecimalFormat;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android_serialport_api.Printer;


public class MainActivity extends TimerActivity implements PadFragment.PadListener {

    private BroadcastReceiver _broadcastReceiver;
    private Usuario _user;
    private Config config;
    private Printer _printer;
    private ImageButton btnCalc;
    private ImageView dummy_view;
    private TextView txtFecha_panel;
    private TextView txtHora_panel;
    private TextView txtValor_panel;
    private TextView txtFuncion_panel;
    private TextView txtComentario_panel;
    private TextView txtUser_panel;
    private TextView txtWelcome_panel;
    private ImageView imgIcono_panel;
    private Button btnConfirmar_mp;
    private Button btnCancelar_mp;
    private TextView txtTotal_mp;
    private RadioGroup rbtMedios_mp;
    private View ccView;
    private List<Cliente> _list_clientes;
    private ListView lstClientes;
    private Button btnNuevoCliente;
    private Button btnCancelar;
    private Button btnImprimir;
    private Button btnImprimirEstadoCuenta;
    private List<String> lineasLayout;
    private List<String> totalesLayout;
    private Cliente cliente_edit;
    private EditText searchText;
    private ClientesArrayAdapter adapter;
    private Formato.Decimal_Format FORMATO_DECIMAL;
    private String FORMATO_SIGNO_MONEDA;

    @Override
    public void onStart() {
        super.onStart();
        _broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().compareTo(Intent.ACTION_TIME_TICK) == 0) {
                    Date now = new Date();
                    ((TextView) findViewById(R.id.panel_txt_fecha)).setText(Formatos.FormateaDate(now, Formatos.DateFormat));
                    ((TextView) findViewById(R.id.panel_txt_hora)).setText(Formatos.FormateaDate(now, Formatos.ShortTimeFormat));
                }
            }
        };

        registerReceiver(_broadcastReceiver, new IntentFilter(Intent.ACTION_TIME_TICK));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {

            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);//evitamos el sleep durante la ejecucion de esta aplicacion

            ((TextView) findViewById(R.id.txtVersion)).setText("Version:" + getPackageManager().getPackageInfo(this.getPackageName().toString(), 0).versionName);

            //logueo Usuario *****************************************************
            //obtenemos los parametros pasamos a la actividad
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                //tomamos el usuario
                this._user = (Usuario) extras.getSerializable("current_user");
            }

            //no tenemos user logueado...volvemos a EntryPoint
            if (_user == null) {
                Toast.makeText(this, R.string.unknown_user, Toast.LENGTH_LONG).show();
                this.finish();
            } else {

                //********* USER LOGUEADO OK *********

                //asignamos los view
                Init_AsignarViews();

                //asignamos los eventos
                Init_AsignarEventos();

                this.txtUser_panel.append(this._user.getNombre());

                this.txtWelcome_panel.setVisibility(View.INVISIBLE);
                this.imgIcono_panel.setImageResource(R.drawable.cuenta_cliente_min);
                this.imgIcono_panel.setVisibility(View.VISIBLE);

                config = new Config(this);

                //setTimer(config.INACTIVITY_TIME);
                //FORMATO_DECIMAL = Formatos.DecimalFormat_CH;
                FORMATO_DECIMAL = Formato.getDecimal_Format(config.PAIS);
                FORMATO_SIGNO_MONEDA = Formato.getCurrencySymbol(FORMATO_DECIMAL);

                PadFragment _padFragment = ((PadFragment) getFragmentManager().findFragmentById(R.id.frg_pad));
                _padFragment.setPadListener(this);
                _padFragment.setFunctionConfig(PadFragment.FnButton.FUNCTION_1, "FICHA CLIENTE", true, R.drawable.botones_main_azul);
                _padFragment.setFunctionConfig(PadFragment.FnButton.FUNCTION_2, "PRESTAR", true, R.drawable.btn_verde);
                _padFragment.setFunctionConfig(PadFragment.FnButton.FUNCTION_4, "COBRAR", true, R.drawable.botones_main_orange);
                _padFragment.setDecimalPoint(Formato.getDecimalFormat(config.PAIS).getDecimalFormatSymbols().getDecimalSeparator());

            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume() {
        try {
            super.onResume();

            config = new Config(this);

            this.txtFecha_panel.setText(Formatos.FormateaDate(new Date(), Formatos.DateFormat));
            this.txtHora_panel.setText(Formatos.FormateaDate(new Date(), Formatos.ShortTimeFormat));

            Cancelar();

        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        if (_broadcastReceiver != null)
            unregisterReceiver(_broadcastReceiver);
    }

    @Override
    protected void onDestroy() {
        if (_printer != null)
            _printer.end();

        super.onDestroy();
    }

    private String ObtieneTextoPanel() {
        String texto = this.txtValor_panel.getText().toString().trim();

        if (TextUtils.isEmpty(texto)) {
            return "0";
        } else
            return texto;
    }

    @Override
    public void onNumberPressed(String numberPressed) {
        AgregarTextoPanel(numberPressed);
    }

    @Override
    public void onFunction1Pressed() {
        //**************FICHA CLIENTE****************
        if (cliente_edit != null) {
            Intent intent = new Intent(MainActivity.this, clientes.class);
            intent.putExtra("cliente", cliente_edit);
            MainActivity.this.startActivity(intent);
            MainActivity.this.overridePendingTransition(0, 0);
        }
    }

    @Override
    public void onFunction2Pressed() {
        //**************PRESTAR DINERO****************

        String montoIngresado = txtValor_panel.getText().toString().trim();

        //validamos el monto ingresado
        if (TextUtils.isEmpty(montoIngresado)) {
            Toast.makeText(MainActivity.this, "Debe ingresar un monto a prestar para continuar", Toast.LENGTH_SHORT).show();
        } else if (Formato.ParseaDecimal(ObtieneTextoPanel(), FORMATO_DECIMAL) == 0) {
            Toast.makeText(MainActivity.this, "Debe ingresar un monto a prestar para continuar", Toast.LENGTH_SHORT).show();
        } else {
            final double montoPrestamo = Formato.ParseaDecimal(ObtieneTextoPanel(), FORMATO_DECIMAL);

            if (!cliente_edit.tieneSaldoDisponible(montoPrestamo, getContentResolver())) {
                Toast.makeText(MainActivity.this, "Error: el cliente no posee saldo disponible suficiente", Toast.LENGTH_SHORT).show();
            } else {

                new ConfirmDialog(MainActivity.this, "PRESTAMO", "¿Confirma el prestamo de dinero?")
                        .set_positiveButtonText("ACEPTAR")
                        .set_positiveButtonAction(new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    CuentaCte cuentaCte = new CuentaCte();
                                    cuentaCte.setMonto(montoPrestamo);
                                    cuentaCte.setTipoMov(CuentaCte.TipoMovimientoCtaCte.DEUDA);
                                    cuentaCte.setFechaHora(new Date());
                                    cuentaCte.setCliente(cliente_edit);
                                    cuentaCte.setIdVenta(CuentaCte.ID_PRESTAMO);//PRESTAMO

                                    new CuentaCteDao(getContentResolver()).save(cuentaCte);

                                    //enviamos a caja el retiro de dinero para el prestamo
                                    Caja.RegistrarMovimientoCaja(MainActivity.this, Caja.TipoMovimientoCaja.RETIRO, cuentaCte.getMonto().doubleValue(), "EFECTIVO", "", -1, "");

                                    Logger.RegistrarEvento(MainActivity.this, "i", "PRESTAMO " + cliente_edit.toString(), "Total: " + Formato.FormateaDecimal(cuentaCte.getMonto(), FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA));

                                    Toast.makeText(MainActivity.this, "Prestamo realizado correctamente", Toast.LENGTH_SHORT).show();

                                    txtValor_panel.setText("");

                                    ImprimirDetalleCliente(true);

                                    MostrarInfoCliente(cliente_edit);

                                } catch (Exception ex) {
                                    Toast.makeText(MainActivity.this, "Error al cobrar: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .set_negativeButtonText("CANCELAR")
                        .show();
            }
        }

    }

    //ya no pide mas fechas, imprime por default los ultimos 100 dias
    public void _onFunction2Pressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        LayoutInflater inflater = MainActivity.this.getLayoutInflater();

        final View vw = inflater.inflate(R.layout.dialog_detalle, null);

        vw.findViewById(R.id.dialog_detalle_btn_fec_desde).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PickDate(vw.findViewById(R.id.dialog_detalle_txt_fec_desde));
            }
        });
        /*vw.findViewById(R.id.dialog_detalle_txt_fec_desde).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {if (b) PickDate(view);}
        });*/
        vw.findViewById(R.id.dialog_detalle_btn_fec_hasta).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PickDate(vw.findViewById(R.id.dialog_detalle_txt_fec_hasta));
            }
        });
        /*vw.findViewById(R.id.dialog_detalle_txt_fec_hasta).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {if (b) PickDate(view);}
        });*/

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -1);

        ((EditText) vw.findViewById(R.id.dialog_detalle_txt_fec_desde)).setText(Formatos.FormateaDate(cal.getTime(), Formatos.DateFormatSlash));//default: desde 1 mes atras
        ((EditText) vw.findViewById(R.id.dialog_detalle_txt_fec_hasta)).setText(Formatos.FormateaDate(new Date(), Formatos.DateFormatSlash));//default: hasta hoy

        builder.setView(vw)
                // Add action buttons
                .setPositiveButton("Imprimir", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        try {

                            Date fecDesde = Formatos.ObtieneDate(((EditText) ((AlertDialog) dialog).findViewById(R.id.dialog_detalle_txt_fec_desde)).getText().toString(), Formatos.DateFormatSlash);
                            Date fecHasta = Formatos.ObtieneDate(((EditText) ((AlertDialog) dialog).findViewById(R.id.dialog_detalle_txt_fec_hasta)).getText().toString(), Formatos.DateFormatSlash);

                            //ImprimirDetalleCliente(cliente_edit, fecDesde, fecHasta);

                        } catch (Exception e) {
                            Toast.makeText(MainActivity.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

        AlertDialog dialog = builder.create();

        dialog.setTitle("Detalle de movimientos");
        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
            }
        });

        dialog.show();

    }

    @Override
    public void onFunction3Pressed() {
        //**************BACKSPACE****************
        String texto = txtValor_panel.getText().toString();

        if (!texto.isEmpty()) {
            txtValor_panel.setText(texto.substring(0, texto.length() - 1));
        }
    }

    @Override
    public void onFunction4Pressed() {
        //**************COBRAR DEUDA****************

        String montoIngresado = txtValor_panel.getText().toString().trim();

        //validamos el monto ingresado
        if (TextUtils.isEmpty(montoIngresado)) {
            Toast.makeText(MainActivity.this, "Debe ingresar un monto a cobrar para continuar", Toast.LENGTH_SHORT).show();
        } else if (Formato.ParseaDecimal(ObtieneTextoPanel(), FORMATO_DECIMAL) == 0) {
            Toast.makeText(MainActivity.this, "Debe ingresar un monto a cobrar para continuar", Toast.LENGTH_SHORT).show();
        } else {
            new ConfirmDialog(MainActivity.this, "COBRO", "¿Confirma el registro del cobro?")
                    .set_positiveButtonText("ACEPTAR")
                    .set_positiveButtonAction(new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            try {
                                CuentaCte cuentaCte = new CuentaCte();
                                cuentaCte.setMonto(Formato.ParseaDecimal(ObtieneTextoPanel(), FORMATO_DECIMAL));
                                cuentaCte.setTipoMov(CuentaCte.TipoMovimientoCtaCte.COBRO);
                                cuentaCte.setFechaHora(new Date());
                                cuentaCte.setCliente(cliente_edit);

                                new CuentaCteDao(getContentResolver()).save(cuentaCte);

                                //enviamos a caja todos los cobros, independiente del medio de pago
                                Caja.RegistrarMovimientoCaja(MainActivity.this, Caja.TipoMovimientoCaja.DEPOSITO, cuentaCte.getMonto().doubleValue(), "EFECTIVO", "", -1, "");

                                Logger.RegistrarEvento(MainActivity.this, "i", "COBRO " + cliente_edit.toString(), "Total: " + Formato.FormateaDecimal(cuentaCte.getMonto(), FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA), "Medio Pago: EFECTIVO");

                                Toast.makeText(MainActivity.this, "Cobro realizado correctamente", Toast.LENGTH_SHORT).show();

                                txtValor_panel.setText("");

                                ImprimirDetalleCliente(true);

                                MostrarInfoCliente(cliente_edit);
                            } catch (Exception ex) {
                                Toast.makeText(MainActivity.this, "Error al cobrar: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    })
                    .set_negativeButtonText("CANCELAR")
                    .show();

        }

    }

    public void PickDate(final View _view) {
        Calendar hoy = Calendar.getInstance();
        DatePickerDialog datePicker = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                Calendar fecha = Calendar.getInstance();
                fecha.set(year, monthOfYear, dayOfMonth);

                ((EditText) _view).setText(Formatos.FormateaDate(fecha.getTime(), Formatos.DateFormatSlash));
            }
        }, hoy.get(Calendar.YEAR), hoy.get(Calendar.MONTH), hoy.get(Calendar.DAY_OF_MONTH));
        datePicker.show();
    }

    private void Init_AsignarViews() {

        this.btnCalc = (ImageButton) findViewById(R.id.cc_btnCalc);

        this.dummy_view = (ImageView) findViewById(R.id.cc_dummy);

        this.ccView = findViewById(R.id.cc_main_layout);

        this.lstClientes = (ListView) findViewById(R.id.cc_lstClientes);
        this.lstClientes.setEmptyView(findViewById(android.R.id.empty));
        this.btnNuevoCliente = (Button) findViewById(R.id.cc_btn_nuevo);
        this.btnCancelar = (Button) findViewById(R.id.cc_btn_cancelar);

        this.btnImprimir = (Button) findViewById(R.id.cc_btn_imprimir);
        this.btnImprimirEstadoCuenta = (Button) findViewById(R.id.cc_btn_imprimir_estado_cta);
        //this.btnReintentar = (Button) findViewById(R.id.cc_btn_reintentar);
        //this.btnVolver = (Button) findViewById(R.id.cc_btn_volver);

        //controles del panel
        this.imgIcono_panel = (ImageView) findViewById(R.id.panel_img_icono);
        this.txtWelcome_panel = (TextView) findViewById(R.id.panel_txt_welcome);
        this.txtUser_panel = (TextView) findViewById(R.id.panel_txt_user);
        this.txtComentario_panel = (TextView) findViewById(R.id.panel_txt_comentario);
        this.txtFuncion_panel = (TextView) findViewById(R.id.panel_txt_funcion);
        this.txtValor_panel = (TextView) findViewById(R.id.panel_txt_valor);
        this.txtFecha_panel = (TextView) findViewById(R.id.panel_txt_fecha);
        this.txtHora_panel = (TextView) findViewById(R.id.panel_txt_hora);

        //controles del popup medio pago
        this.btnConfirmar_mp = (Button) findViewById(R.id.medio_pago_btn_aceptar);
        this.btnCancelar_mp = (Button) findViewById(R.id.medio_pago_btn_cancelar);
        this.txtTotal_mp = (TextView) findViewById(R.id.medio_pago_txt_total);
        this.rbtMedios_mp = (RadioGroup) findViewById(R.id.medio_pago_medios);

        this.searchText = (EditText) findViewById(R.id.cc_search_txt_criteria);

    }

    void showHideViews(boolean hide) {
        FragmentManager fm = getFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.frg_pad);

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        /*ft.setCustomAnimations(android.R.animator.fade_in,
                android.R.animator.fade_out);*/


        final View panel = findViewById(R.id.cc_panel);

        /*Animation animFadeOut = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.fade_out);
        animFadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                panel.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        Animation animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.fade_in);
        animFadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                panel.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });*/

        if (hide) {
            ft.hide(fragment);
            ft.commit();
            //panel.setAnimation(animFadeOut);
            panel.setVisibility(View.GONE);
        } else {
            //panel.setAnimation(animFadeIn);
            panel.setVisibility(View.VISIBLE);
            ft.show(fragment);
            ft.commit();
        }


    }

    private void Init_AsignarEventos() {

        lstClientes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    //al seleccionar un cliente mostramos su detalle
                    cliente_edit = (Cliente) lstClientes.getItemAtPosition(position);

                    CancelarSearch(false);

                    MostrarInfoCliente(cliente_edit);

                } catch (Exception ex) {
                    Toast.makeText(MainActivity.this, "Error al obtener informacion del cliente: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        });

        this.btnNuevoCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_user.getId_perfil() != 1) {
                    Toast.makeText(MainActivity.this, "Error: Usuario no autorizado a crear cuenta cliente", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(MainActivity.this, clientes.class);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                }
            }
        });

        this.btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cancelar();
            }
        });

        this.btnImprimir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {

                    if (_printer == null)
                        _printer = Printer.getInstance(config.PRINTER_MODEL, MainActivity.this);

                    _printer.printerCallback = new Printer.PrinterCallback() {
                        @Override
                        public void onPrinterReady() {
                            Double limite = 0d;

                            //sumamos el limte de todos los clientes
                            for (Cliente _cliente : _list_clientes) {
                                limite += _cliente.getLimite();
                            }

                            //sumamos la deuda de todos los clientes
                            Double deuda = CalculaExposicionTotal();

                            _printer.lineFeed();

                            _printer.format(2, 2, Printer.ALINEACION.CENTER);
                            _printer.printLine("TOTALES");
                            _printer.cancelCurrentFormat();
                            _printer.sendSeparadorHorizontal();

                            _printer.printLine("Fecha y Hora:     " + Formatos.FormateaDate(new Date(), Formatos.FullDateTimeFormatNoSeconds));

                            _printer.printLine("Limite de Credito: " + Formato.FormateaDecimal(limite, FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA));

                            _printer.printLine("Exposicion Actual: " + Formato.FormateaDecimal(deuda, FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA));

                            _printer.footer();
                        }
                    };

                    //iniciamos la printer
                    if (!_printer.initialize()) {
                        Toast.makeText(MainActivity.this, "Error al inicializar", Toast.LENGTH_SHORT).show();
                        _printer.end();
                    }

                } catch (Exception ex) {
                    Toast.makeText(MainActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        });

        this.btnImprimirEstadoCuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new ConfirmDialog(MainActivity.this, "ESTADO DE CUENTA", "¿Desea imprimir los movimientos del cliente de los últimos 60 días?")
                        .set_positiveButtonText("ACEPTAR")
                        .set_positiveButtonAction(new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ImprimirDetalleCliente(false);
                            }
                        })
                        .set_negativeButtonText("CANCELAR")
                        .show();

            }
        });

        //APERTURA DE CALCULADORA
        btnCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_MAIN);
                i.setComponent(new ComponentName("com.android.calculator2", "com.android.calculator2.Calculator"));


                try {
                    MainActivity.this.startActivity(i);
                } catch (ActivityNotFoundException noSuchActivity) {
                    // si el calculator intent no está registrado, probamos con un nombre alternativo
                    Intent i2 = new Intent();
                    i2.setAction(Intent.ACTION_MAIN);
                    i2.setComponent(new ComponentName("com.sec.android.app.popupcalculator", "com.sec.android.app.popupcalculator.Calculator"));
                    try {
                        MainActivity.this.startActivity(i2);
                    } catch (ActivityNotFoundException noSuchActivity2) {
                        // calculator intent not found
                        Toast.makeText(MainActivity.this, "Acceso a calculadora no encontrado", Toast.LENGTH_LONG).show();
                    }
                }

            }
        });

        this.dummy_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dummy_view.setBackgroundColor(Color.TRANSPARENT);
                dummy_view.setVisibility(View.INVISIBLE);
                (findViewById(R.id.cc_medio_pago)).setVisibility(View.INVISIBLE);
            }
        });

        (findViewById(R.id.cc_medio_pago)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //dejamos el evento click seteado en la vista para que no se vaya al click del dummyview
            }
        });

        this.btnConfirmar_mp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dummy_view.setBackgroundColor(Color.TRANSPARENT);
                dummy_view.setVisibility(View.INVISIBLE);
                findViewById(R.id.cc_medio_pago).setVisibility(View.INVISIBLE);

                /*
                if (bipStep > 0) {
                    CARGA_MEDIOPAGO = ((RadioButton) rbtMedios_mp.findViewById(rbtMedios_mp.getCheckedRadioButtonId())).getText().toString();
                    //registramos el cobro
                    bipStep = 5;
                    Proceso();
                }*/
            }
        });

        this.btnCancelar_mp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dummy_view.setBackgroundColor(Color.TRANSPARENT);
                dummy_view.setVisibility(View.INVISIBLE);
                findViewById(R.id.cc_medio_pago).setVisibility(View.INVISIBLE);
            }
        });

        searchText.setFilters(new InputFilter[]{new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++)
                    if (!Character.isLetter(source.charAt(i)) &&
                            !Character.isSpaceChar(source.charAt(i)) &&
                            !Character.isDigit(source.charAt(i))
                            ) //solo aceptamos: letras, numeros y espacio
                        return "";

                return null;
            }
        }});

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    ((ImageView) findViewById(R.id.cc_search_btn_find)).setImageResource(R.drawable.ic_action_cancel);
                    FiltrarClientes(s);
                } else
                    ((ImageView) findViewById(R.id.cc_search_btn_find)).setImageResource(R.drawable.lupa);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        findViewById(R.id.cc_search_btn_find).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SwichSearchMode(0);
            }
        });

        findViewById(R.id.cc_find_by_clave_fiscal).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SwichSearchMode(1);
            }
        });

        findViewById(R.id.cc_find_by_name).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SwichSearchMode(2);
            }
        });
    }

    private void SwichSearchMode(int find_id) {
        if (find_id == 1) {
            searchText.setHint("Ingrese RUT...");
            searchText.setInputType(InputType.TYPE_CLASS_NUMBER);
            findViewById(R.id.cc_find_text_section).setVisibility(View.VISIBLE);
            findViewById(R.id.cc_find_type_section).setVisibility(View.GONE);
            Window.FocusViewShowSoftKeyboard(MainActivity.this, searchText);
        } else if (find_id == 2) {
            searchText.setHint("Ingrese nombre...");
            searchText.setInputType(InputType.TYPE_CLASS_TEXT);
            findViewById(R.id.cc_find_text_section).setVisibility(View.VISIBLE);
            findViewById(R.id.cc_find_type_section).setVisibility(View.GONE);
            Window.FocusViewShowSoftKeyboard(MainActivity.this, searchText);
        } else {
            CancelarSearch(true);
        }
    }

    private void FiltrarClientes(CharSequence filter) {
        if (lstClientes == null)
            lstClientes = (ListView) findViewById(R.id.cc_lstClientes);

        //cargamos los clientes resultantes del filtro de texto
        if (adapter == null) {
            _list_clientes = getListClientes(filter);
            adapter = new ClientesArrayAdapter(MainActivity.this, _list_clientes);
            lstClientes.setAdapter(adapter);
        } else {
            _list_clientes.clear();
            _list_clientes.addAll(getListClientes(filter));
            adapter.notifyDataSetChanged();
        }
    }

    private List<Cliente> getListClientes(CharSequence filter) {
        String cond = "";

        if (!TextUtils.isEmpty(filter))
            cond = "(upper(nombre) like '%" + filter.toString().toUpperCase() + "%' or upper(apellido) like '%" + filter.toString().toUpperCase() + "%' or clave_fiscal like '%" + filter.toString().toUpperCase() + "%') and ";

        return new ClienteDao(this.getContentResolver()).list(cond + " baja = 0", null, "apellido, nombre");
    }

    private void CancelarSearch(boolean reloadClientes) {
        searchText.setText("");

        adapter = null;

        ((ImageView) findViewById(R.id.cc_search_btn_find)).setImageResource(R.drawable.lupa);

        //reiniciamos la lista de clientes
        if (reloadClientes)
            CargarClientes();

        //volvemos a modo sin busqueda
        findViewById(R.id.cc_find_text_section).setVisibility(View.GONE);
        findViewById(R.id.cc_find_type_section).setVisibility(View.VISIBLE);

        //cerramos el teclado
        Window.CloseSoftKeyboard(MainActivity.this);
    }

    private void MostrarInfoCliente(Cliente cliente) {
        //Exposicion total
        //Double _deudaTotal = CalculaExposicionTotal();

        vistaCliente = true;

        //info del cliente : VALORES DEFAULT
        Double deuda = 0d, disponible = cliente_edit.getLimite(); //sin deuda, y con limite disponible
        String fecha_ult_pago = "No registra", monto_ult_pago = "No registra";

        CuentaCteTotal _total = new CuentaCteTotalesDao(getContentResolver()).find(cliente_edit.getId());

        if (_total != null) {  //si hay registros del cliente
            deuda = _total.getSaldo();
            disponible = _total.getDisponible();
            if (_total.getFecha_ult_pago() != null)
                fecha_ult_pago = Formatos.FormateaDate(_total.getFecha_ult_pago(), Formatos.DateFormat);
            if (_total.getMonto_ult_pago() != 0)
                monto_ult_pago = Formato.FormateaDecimal(_total.getMonto_ult_pago(), FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA);
        }

        btnImprimir.setVisibility(View.GONE);
        btnImprimirEstadoCuenta.setVisibility(View.VISIBLE);

        //btnNuevoCliente.setVisibility(View.GONE);
        findViewById(R.id.cc_search_layout).setVisibility(View.GONE);
        //btnCancelar.setVisibility(View.VISIBLE);
        findViewById(R.id.cc_footer_layout).setVisibility(View.VISIBLE);
        (findViewById(R.id.cc_rlineas)).setVisibility(View.GONE);

        lineasLayout = null;

        EscribeLinea(cliente_edit.toString());
        EscribeLinea("RUT: " + (TextUtils.isEmpty(cliente_edit.getClaveFiscal()) ? "No Registra" : cliente_edit.getClaveFiscal()));
        EscribeLinea("");
        EscribeLinea("Limite: " + Formato.FormateaDecimal(cliente_edit.getLimite(), FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA));
        EscribeLinea("Deuda actual: " + Formato.FormateaDecimal(deuda, FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA));
        EscribeLinea("");
        EscribeLinea("Último pago");
        EscribeLinea("    Fecha: " + fecha_ult_pago);
        EscribeLinea("    Monto: " + monto_ult_pago);
        EscribeLinea("");
        EscribeLinea("Disponible: " + Formato.FormateaDecimal(disponible, FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA));
//        EscribeLinea("");
//        EscribeLinea("Exposicion TOTAL: " + Formatos.FormateaDecimal(_deudaTotal, FORMATO_DECIMAL, "$"));

        showHideViews(false);

        this.txtComentario_panel.setText("Ingrese monto a cobrar o prestar...");
        this.txtComentario_panel.setVisibility(View.VISIBLE);
    }

    private void AgregarTextoPanel(String texto) {
        this.txtValor_panel.setText(this.txtValor_panel.getText().toString() + texto);
        this.txtValor_panel.setVisibility(View.VISIBLE);
    }

    private void EscribeLinea(String linea) {
        EscribeLinea(linea, "");
    }

    private void EscribeLinea(String linea, String titulo) {

        if (this.lineasLayout == null)
            this.lineasLayout = new ArrayList<String>();

        this.lineasLayout.add(linea);

        EscribeLineas(this.lineasLayout, titulo);
    }

    private void EscribeLineas(List<String> lineas, String titulo) {

        //ocultamos la lista de clientes
        (findViewById(R.id.cc_lstClientes)).setVisibility(View.GONE);
        //mostramos el layout de lineas
        (findViewById(R.id.cc_lineas)).setVisibility(View.VISIBLE);

        if (!titulo.equals("")) {
            TextView txtTitulo = (TextView) findViewById(R.id.cc_txt_titulo);
            txtTitulo.setVisibility(View.VISIBLE);
            txtTitulo.setText(titulo);
        } else {
            TextView txtTitulo = (TextView) findViewById(R.id.cc_txt_titulo);
            txtTitulo.setVisibility(View.GONE);
            txtTitulo.setText("");
        }

        //blanqueamos
        ((TextView) findViewById(R.id.cc_txt1)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt2)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt3)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt4)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt5)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt6)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt7)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt8)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt9)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt10)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt11)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt12)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt13)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt14)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt15)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt16)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt17)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt18)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt19)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt20)).setVisibility(View.GONE);

        for (int i = 0; i < lineas.size(); i++) {

            TextView txtLinea = null;

            switch (i + 1) {
                case 1:
                    txtLinea = (TextView) findViewById(R.id.cc_txt1);
                    break;
                case 2:
                    txtLinea = (TextView) findViewById(R.id.cc_txt2);
                    break;
                case 3:
                    txtLinea = (TextView) findViewById(R.id.cc_txt3);
                    break;
                case 4:
                    txtLinea = (TextView) findViewById(R.id.cc_txt4);
                    break;
                case 5:
                    txtLinea = (TextView) findViewById(R.id.cc_txt5);
                    break;
                case 6:
                    txtLinea = (TextView) findViewById(R.id.cc_txt6);
                    break;
                case 7:
                    txtLinea = (TextView) findViewById(R.id.cc_txt7);
                    break;
                case 8:
                    txtLinea = (TextView) findViewById(R.id.cc_txt8);
                    break;
                case 9:
                    txtLinea = (TextView) findViewById(R.id.cc_txt9);
                    break;
                case 10:
                    txtLinea = (TextView) findViewById(R.id.cc_txt10);
                    break;
                case 11:
                    txtLinea = (TextView) findViewById(R.id.cc_txt11);
                    break;
                case 12:
                    txtLinea = (TextView) findViewById(R.id.cc_txt12);
                    break;
                case 13:
                    txtLinea = (TextView) findViewById(R.id.cc_txt13);
                    break;
                case 14:
                    txtLinea = (TextView) findViewById(R.id.cc_txt14);
                    break;
                case 15:
                    txtLinea = (TextView) findViewById(R.id.cc_txt15);
                    break;
                case 16:
                    txtLinea = (TextView) findViewById(R.id.cc_txt16);
                    break;
                case 17:
                    txtLinea = (TextView) findViewById(R.id.cc_txt17);
                    break;
                case 18:
                    txtLinea = (TextView) findViewById(R.id.cc_txt18);
                    break;
                case 19:
                    txtLinea = (TextView) findViewById(R.id.cc_txt19);
                    break;
                case 20:
                    txtLinea = (TextView) findViewById(R.id.cc_txt20);
                    break;

            }

            String texto = lineas.get(i);
            if (texto.startsWith("@C@")) {
                texto = texto.replace("@C@", "");
                txtLinea.setGravity(Gravity.CENTER);
            } else {
                txtLinea.setGravity(Gravity.LEFT);
            }
            if (texto.startsWith("@B@")) {
                texto = texto.replace("@B@", "");
                txtLinea.setTypeface(null, Typeface.BOLD);
            } else {
                txtLinea.setTypeface(null, Typeface.NORMAL);
            }

            txtLinea.setText(texto);
            txtLinea.setVisibility(View.VISIBLE);
        }

        //envia las lineas al fondo para visualizar siempre lo ultimo

        ((ScrollView) findViewById(R.id.scrollView)).post(new Runnable() {
            public void run() {
                ((ScrollView) findViewById(R.id.scrollView)).fullScroll(View.FOCUS_DOWN);
            }
        });
    }

    private void EscribeTotales(String linea, String titulo) {

        if (this.totalesLayout == null)
            this.totalesLayout = new ArrayList<String>();

        this.totalesLayout.add(linea);

        EscribeLineasTotales(this.totalesLayout, titulo);
    }

    private void EscribeLineasTotales(List<String> lineas, String titulo) {

        //ocultamos la lista de clientes
        //(findViewById(R.id.cc_lstClientes)).setVisibility(View.GONE);
        //mostramos el layout de lineas
        (findViewById(R.id.cc_rlineas)).setVisibility(View.VISIBLE);

        if (!titulo.equals("")) {
            TextView txtTitulo = (TextView) findViewById(R.id.cc_txt_rtitulo);
            txtTitulo.setVisibility(View.VISIBLE);
            txtTitulo.setText(titulo);
        } else {
            TextView txtTitulo = (TextView) findViewById(R.id.cc_txt_rtitulo);
            txtTitulo.setVisibility(View.GONE);
            txtTitulo.setText("");
        }

        //blanqueamos
        ((TextView) findViewById(R.id.cc_txt_r1)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt_r2)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt_r3)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt_r4)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt_r5)).setVisibility(View.GONE);

        for (int i = 0; i < lineas.size(); i++) {

            TextView txtLinea = null;

            switch (i + 1) {
                case 1:
                    txtLinea = (TextView) findViewById(R.id.cc_txt_r1);
                    break;
                case 2:
                    txtLinea = (TextView) findViewById(R.id.cc_txt_r2);
                    break;
                case 3:
                    txtLinea = (TextView) findViewById(R.id.cc_txt_r3);
                    break;
                case 4:
                    txtLinea = (TextView) findViewById(R.id.cc_txt_r4);
                    break;
                case 5:
                    txtLinea = (TextView) findViewById(R.id.cc_txt_r5);
                    break;
            }

            String texto = lineas.get(i);
            if (texto.startsWith("@C@")) {
                texto = texto.replace("@C@", "");
                txtLinea.setGravity(Gravity.CENTER);
            } else {
                txtLinea.setGravity(Gravity.LEFT);
            }
            if (texto.startsWith("@B@")) {
                texto = texto.replace("@B@", "");
                txtLinea.setTypeface(null, Typeface.BOLD);
            } else {
                txtLinea.setTypeface(null, Typeface.NORMAL);
            }

            txtLinea.setText(texto);
            txtLinea.setVisibility(View.VISIBLE);
        }
    }

    private boolean vistaCliente = false;

    private void Cancelar() {

        vistaCliente = false;

        btnImprimir.setVisibility(View.VISIBLE);
        btnImprimirEstadoCuenta.setVisibility(View.GONE);

        //this.btnNuevoCliente.setVisibility(View.VISIBLE);
        findViewById(R.id.cc_search_layout).setVisibility(View.VISIBLE);
        //this.btnCancelar.setVisibility(View.GONE);
        findViewById(R.id.cc_footer_layout).setVisibility(View.GONE);

        (findViewById(R.id.cc_lstClientes)).setVisibility(View.VISIBLE);
        (findViewById(R.id.cc_lineas)).setVisibility(View.GONE);

        this.txtValor_panel.setText("");

        CancelarSearch(false);

        CargarClientes();

        showHideViews(true);

        CargarTotales();
    }

    private void CargarClientes() {

        FiltrarClientes("");

        /*_list_clientes = new ClienteDao(this.getContentResolver()).list(String.format("baja = %d", 0), null, "apellido, nombre");

        //iniciamos la carga de los clientes
        lstClientes.setAdapter(new ClientesArrayAdapter(MainActivity.this, _list_clientes));*/
    }

    private void CargarTotales() {

        Double limite = 0d, deuda = 0d;

        //sumamos el limte de todos los clientes
        for (Cliente _cliente : _list_clientes) {
            limite += _cliente.getLimite();
        }

        //sumamos la deuda de todos los clientes
        deuda = CalculaExposicionTotal();

        totalesLayout = null;

        EscribeTotales("Límite de Crédito: " + Formato.FormateaDecimal(limite, FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA), "TOTALES");
        EscribeTotales("", "TOTALES");
        EscribeTotales("Exposición Actual: " + Formato.FormateaDecimal(deuda, FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA), "TOTALES");
    }

    private Double CalculaExposicionTotal() {
        //Exposicion total
        Double _deudaTotal = 0d;

        List<CuentaCteTotal> _deudores = new CuentaCteTotalesDao(getContentResolver()).list();

        for (CuentaCteTotal _deuda : _deudores) {
            //no traemos deudas de clientes eliminados
            if (_deuda.getCliente().getBaja() == 0)
                _deudaTotal += _deuda.getSaldo();
        }

        return _deudaTotal;
    }

    private void ImprimirDetalleCliente(boolean openCashdrawer) {
        try {

            Calendar c = Calendar.getInstance();

            Date fecHasta = c.getTime();//NOW

            c.add(Calendar.DATE, -60);
            Date fecDesde = c.getTime();//ultimos dos meses

            ImprimirDetalleCliente(cliente_edit, fecDesde, fecHasta, openCashdrawer);

        } catch (Exception e) {
            Toast.makeText(MainActivity.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void ImprimirDetalleCliente(final Cliente cliente, final Date fechaDesde, final Date fechaHasta, final boolean openCashdrawer) {
        try {
            if (_printer == null)
                _printer = Printer.getInstance(config.PRINTER_MODEL, MainActivity.this);

            _printer.printerCallback = new Printer.PrinterCallback() {
                @Override
                public void onPrinterReady() {
                    Double deuda = 0d, disponible = cliente.getLimite(); //por default, tiene como limite el disponible y no tiene deuda

                    CuentaCteTotal _total = new CuentaCteTotalesDao(getContentResolver()).find(cliente.getId());

                    if (_total != null) {  //si hay registros del cliente
                        deuda = _total.getSaldo();
                        disponible = _total.getDisponible();
                    }

                    _printer.format(2, 2, Printer.ALINEACION.CENTER);
                    _printer.printLine("ESTADO CUENTA");
                    _printer.sendSeparadorHorizontal();

                    _printer.printLine("CLIENTE    : " + cliente.toString());
                    if (!TextUtils.isEmpty(cliente.getClaveFiscal()))
                        _printer.printLine("RUT        : " + cliente.getClaveFiscal());
                    _printer.printLine("LIMITE     : " + Formato.FormateaDecimal(cliente.getLimite(), FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA));
                    _printer.printLine("DISPONIBLE : " + Formato.FormateaDecimal(disponible, FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA));
                    _printer.printLine("FEC. DESDE : " + Formatos.FormateaDate(fechaDesde, Formatos.DateFormat));
                    _printer.printLine("FEC. HASTA : " + Formatos.FormateaDate(fechaHasta, Formatos.DateFormat));

                    _printer.sendSeparadorHorizontal(false);
                    _printer.printOpposite("FECHA  CONCEPTO ", " IMPORTE  ");
                    //_printer.print("01-10 VT#00001253   5000    6000");

                    //List<CuentaCte> _movimientos = new CuentaCteDao(getContentResolver()).list("id_cliente = " + String.valueOf(cliente.getId()) + " and fecha >= '" + Formatos.FormateaDate(fechaDesde, Formatos.DbDate_Format) + " 00:00:00' and fecha <= '" + Formatos.FormateaDate(fechaHasta, Formatos.DbDate_Format) + " 23:59:59'" , null, "_id");
                    List<CuentaCte> _movimientos = new CuentaCteDao(getContentResolver()).list("id_cliente = " + String.valueOf(cliente.getId()), null, "_id");

                    double saldo = 0d; //el saldo del primer movimiento es la deuda actual
                    boolean printSaldoInit = false;
                    for (CuentaCte _mov : _movimientos) {//traemos todos los movimientos, a los anteriores a la fechaDesde los sumamos como SALDO de inicio

                        double monto = _mov.getTipoMov() == CuentaCte.TipoMovimientoCtaCte.COBRO.ordinal() ? _mov.getMonto() * (-1) : _mov.getMonto();

                        if (_mov.getFechaHora().compareTo(fechaDesde) < 0)
                            saldo += monto;//acumulamos el saldo como SALDO inicial
                        else {

                            if (!printSaldoInit) {
                                String _saldo = Formato.FormateaDecimal(saldo, FORMATO_DECIMAL);
                                _printer.printOpposite("SALDO INICIAL", _saldo);

                                printSaldoInit = true;
                            }

                            String concepto = "";
                            if (_mov.getTipoMov() == CuentaCte.TipoMovimientoCtaCte.DEUDA.ordinal() && _mov.getIdVenta() == CuentaCte.ID_PRESTAMO) {
                                // es un PRESTAMO
                                concepto = "PRESTAMO   "; //long 11
                            } else if (_mov.getTipoMov() == CuentaCte.TipoMovimientoCtaCte.DEUDA.ordinal()) {
                                String nroVenta = "00000000" + String.valueOf(_mov.getIdVenta());
                                nroVenta = nroVenta.substring(nroVenta.length() - 8);
                                String tipoVta = TextUtils.isEmpty(_mov.getTipoVenta()) ? "VT" : _mov.getTipoVenta().substring(0, 2).toUpperCase();
                                concepto = tipoVta + "#" + nroVenta; //long 3+8 = 11
                            } else {
                                // es un COBRO
                                concepto = "PAGO       "; //long 11
                            }

                            String _monto = Formato.FormateaDecimal(monto, FORMATO_DECIMAL);
                            //String _monto = "      " + Formato.FormateaDecimal(monto, FORMATO_DECIMAL);
                            //_monto = _monto.substring(_monto.length() - 6); // long 6
                            //String _saldo = "       " + Formato.FormateaDecimal(saldo, FORMATO_DECIMAL);
                            //_saldo = _saldo.substring(_saldo.length() - 7); // long 7

                            //_printer.print(Formatos.FormateaDate(_mov.getFechaHora(), Formatos.DayMonthFormat) + " " + concepto + " " + _monto + " " + _saldo);
                            _printer.printOpposite(Formatos.FormateaDate(_mov.getFechaHora(), Formatos.DayMonthFormat) + " " + concepto, _monto);

                            saldo += monto;
                        }
                    }

                    int k = (_printer.get_longitudLinea() - 5) / 2; //(SALDO)

                    //_printer.print("-------------SALDO--------------");
                    _printer.print(_printer.getPadding(k, "-") + "SALDO" + _printer.getPadding(k + 1, "-"));

                    _printer.format(2, 1, Printer.ALINEACION.CENTER);
                    if (deuda >= 0)
                        _printer.printLine("DEUDA ACTUAL : " + Formato.FormateaDecimal(deuda, FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA));
                    else
                        _printer.printLine("SALDO A FAVOR : " + Formato.FormateaDecimal(deuda, FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA));

                    _printer.sendSeparadorHorizontal(true);

                    _printer.format(1, 1, Printer.ALINEACION.CENTER);
                    //_printer.printLine("      " + Formatos.FormateaDate(new Date(), Formatos.DateFormat) + " " + Formatos.FormateaDate(new Date(), Formatos.TimeFormat));
                    _printer.printLine(Formatos.FormateaDate(new Date(), Formatos.DateFormat) + " " + Formatos.FormateaDate(new Date(), Formatos.TimeFormat));

                    //_printer.printLine("Exposicion Actual: " + Formatos.FormateaDecimal(deuda, Formatos.DecimalFormat_CH, "$"));

                    _printer.footer();

                    if (_printer.isCashDrawerActivated() && openCashdrawer) {
                        _printer.openCashDrawer();
                    }
                }
            };

            //iniciamos la printer
            if (!_printer.initialize()) {
                Toast.makeText(MainActivity.this, "Error al inicializar", Toast.LENGTH_SHORT).show();
                _printer.end();
            }

        } catch (Exception ex) {
            Toast.makeText(this, "Error al imprimir detalle: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

/*
    private void _ImprimirComprobante(final Cliente cliente, final CuentaCte movimiento, final boolean copiaCliente) {
        try {
            if (_printer == null)
                _printer = Printer.getInstance(config.PRINTER_MODEL, MainActivity.this);

            _printer.printerCallback = new Printer.PrinterCallback() {
                @Override
                public void onPrinterReady() {
                    Double deuda = 0d, disponible = cliente.getLimite(); //por default, tiene como limite el disponible y no tiene deuda

                    CuentaCteTotal _total = new CuentaCteTotalesDao(getContentResolver()).find(cliente.getId());

                    if (_total != null) {  //si hay registros del cliente
                        deuda = _total.getSaldo();
                        disponible = _total.getDisponible();
                    }

                    _printer.sendSeparadorHorizontal();

                    //nombre comercio
                    _printer.format(2, 1, Printer.ALINEACION.CENTER);
                    _printer.printLine(config.COMERCIO);
                    _printer.lineFeed();

                    //direccion
                    _printer.cancelCurrentFormat();
                    _printer.printLine(config.DIRECCION);

                    //clave fiscal
                    _printer.printLine("RUT: " + config.CLAVEFISCAL);
                    _printer.printOpposite("Terminal:  ", config.ID_COMERCIO);
                    _printer.printOpposite(Formatos.FormateaDate(new Date(), Formatos.DateFormat), Formatos.FormateaDate(new Date(), Formatos.TimeFormat));

                    _printer.sendSeparadorHorizontal();

                    _printer.printLine("CLIENTE : " + cliente.toString());
                    if (!TextUtils.isEmpty(cliente.getClaveFiscal()))
                        _printer.printLine("RUT     : " + cliente.getClaveFiscal());
                    _printer.printLine("LIMITE  : " + Formato.FormateaDecimal(cliente.getLimite(), FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA));

                    _printer.sendSeparadorHorizontal();

                    _printer.format(2, 1, Printer.ALINEACION.CENTER);

                    double montoNeto = 0;
                    if (movimiento.getTipoMov() == CuentaCte.TipoMovimientoCtaCte.DEUDA.ordinal() && movimiento.getIdVenta() == CuentaCte.ID_PRESTAMO) {
                        // es un PRESTAMO
                        _printer.format(2, 2, Printer.ALINEACION.CENTER);
                        _printer.printLine("PRESTAMO");
                        montoNeto = movimiento.getMonto();
                    } else {
                        // es un COBRO
                        _printer.format(2, 2, Printer.ALINEACION.CENTER);
                        _printer.printLine("PAGO");
                        montoNeto = (-1) * movimiento.getMonto();
                    }

                    _printer.format(2, 1, Printer.ALINEACION.CENTER);
                    _printer.printLine("TOTAL: " + Formato.FormateaDecimal(movimiento.getMonto(), FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA));

                    _printer.sendSeparadorHorizontal();

                    _printer.printLine("DEUDA ANT. : " + Formato.FormateaDecimal(deuda - montoNeto, FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA));
                    _printer.printLine("DEUDA ACT. : " + Formato.FormateaDecimal(deuda, FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA));
                    _printer.printLine("DISPONIBLE : " + Formato.FormateaDecimal(disponible, FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA));


                    if (!copiaCliente) {
                        _printer.lineFeed();
                        _printer.lineFeed();
                        _printer.lineFeed();

                        _printer.printLine("Firma:");
                        _printer.sendSeparadorHorizontal();
                        _printer.lineFeed();

                        _printer.printLine("Aclaracion:");
                        _printer.sendSeparadorHorizontal();
                        _printer.lineFeed();

                        _printer.format(2, 1, Printer.ALINEACION.CENTER);
                        _printer.printLine("ORIGINAL - COMERCIO");
                        _printer.sendSeparadorHorizontal();
                    } else {
                        _printer.lineFeed();
                        _printer.format(2, 1, Printer.ALINEACION.CENTER);
                        _printer.printLine("COPIA - CLIENTE");
                        _printer.sendSeparadorHorizontal();
                    }

                    _printer.footer();

                    if (_printer.isCashDrawerActivated() && !copiaCliente) {
                        _printer.openCashDrawer();
                    }
                }
            };

            //iniciamos la printer
            if (!_printer.initialize()) {
                Toast.makeText(MainActivity.this, "Error al inicializar", Toast.LENGTH_SHORT).show();
                _printer.end();
            }

            if (!copiaCliente) {
                //consultamos si imprimir copia cliente
                new ConfirmDialog(MainActivity.this, "COPIA CLIENTE", "¿Desea imprimir una copia del comprobante para el cliente?")
                        .set_positiveButtonText("ACEPTAR")
                        .set_positiveButtonAction(new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //ImprimirComprobante(cliente, movimiento, true);
                            }
                        })
                        .set_negativeButtonText("CANCELAR")
                        .set_icon(android.R.drawable.ic_menu_help)
                        .show();
            }

        } catch (Exception ex) {
            Toast.makeText(this, "Error al imprimir comprobante: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }*/

    @Override
    public void onBackPressed() {
        if (vistaCliente)//si está en vista cliente, vuelve a pantalla principal
            Cancelar();
        else
            super.onBackPressed();
    }

    public class ClientesArrayAdapter extends ArrayAdapter<Cliente> {
        private final Context context;
        private List<Cliente> list;

        public ClientesArrayAdapter(Context context, List<Cliente> clientes) {
            super(context, R.layout.list_item_cliente, clientes);
            this.context = context;
            this.list = clientes;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(context)
                        .inflate(R.layout.list_item_cliente, parent, false);
            }

            TextView nombre = (TextView) convertView.findViewById(R.id.lblListItem);
            nombre.setText(list.get(position).toString());

            TextView limite = (TextView) convertView.findViewById(R.id.lblListItem2);
            limite.setText(Formato.FormateaDecimal(list.get(position).getLimite(), FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA));

            return convertView;
        }

    }
}
