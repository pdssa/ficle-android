package com.pds.ficle.cuentacliente;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Formatos;
import com.pds.common.SII.vale.util.ValeUtils;
import com.pds.common.activity.TimerActivity;
import com.pds.common.dao.ClienteDao;
import com.pds.common.dao.CuentaCteTotalesDao;
import com.pds.common.db.ClienteTable;
import com.pds.common.dialog.ConfirmDialog;
import com.pds.common.integration.android.ScanIntegrator2;
import com.pds.common.integration.android.ScanResult2;
import com.pds.common.model.Cliente;
import com.pds.common.model.CuentaCteTotal;
import com.pds.common.util.Window;

import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class clientes extends TimerActivity {

    static final int REQUEST_IMAGE_CAPTURE = 1;

    private boolean edit;
    private Cliente cliente;

    private TextView txtNombre;
    private TextView txtApellido;
    private TextView txtLimite;
    private TextView txtTelefono;
    private TextView txtObservaciones;
    private TextView txtTitle;
    private TextView txtClaveFiscal;
    private ImageView imgPhoto;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clientes);

        txtTitle = (TextView) findViewById(R.id.cliente_txtTitle);
        txtClaveFiscal = (TextView) findViewById(R.id.cliente_txtClaveFiscal);
        txtNombre = (TextView) findViewById(R.id.cliente_txtNombre);
        txtApellido = (TextView) findViewById(R.id.cliente_txtApellido);
        txtTelefono = (TextView) findViewById(R.id.cliente_txtTelefono);
        txtObservaciones = (TextView) findViewById(R.id.cliente_txtObservacion);
        txtLimite = (TextView) findViewById(R.id.cliente_txtLimite);
        imgPhoto = (ImageView) findViewById(R.id.cliente_image);

        findViewById(R.id.cliente_btnScan).setOnClickListener(EscanearCedula);
        findViewById(R.id.cliente_btnGrabar).setOnClickListener(Grabar);
        findViewById(R.id.cliente_btnEliminar).setOnClickListener(Eliminar);
        findViewById(R.id.cliente_btnCancelar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(mCurrentPhotoPath)) {
                    //como
                    File file = new File(mCurrentPhotoPath);
                    file.delete();
                }
                onBackPressed();
            }
        });

        findViewById(R.id.cliente_btnTakePhoto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                // Asegurarse que hay una camara
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    // Crear el archivo donde ira la foto
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {
                        // Error occurred while creating the File
                        Toast.makeText(clientes.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    // Continuar solo si el File fue creado correctamente
                    if (photoFile != null) {
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                }

            }
        });

        Intent intent = getIntent();
        cliente = intent.getParcelableExtra("cliente");
        if (cliente == null) {
            cliente = new Cliente();
            imgPhoto.setImageResource(R.drawable.user_default_photo);
        } else {
            edit = true;

            findViewById(R.id.cliente_btnScan).setVisibility(View.GONE);
            findViewById(R.id.cliente_btnEliminar).setVisibility(View.VISIBLE);

            txtClaveFiscal.setText(cliente.getClaveFiscal());
            txtNombre.setText(cliente.getNombre());
            txtApellido.setText(cliente.getApellido());
            txtTelefono.setText(cliente.getTelefono());
            txtObservaciones.setText(cliente.getObservacion());
            txtLimite.setText(Formatos.FormateaDecimal(cliente.getLimite(), Formatos.DecimalFormat_US));

            if (!TextUtils.isEmpty(cliente.getPathPhoto())) {
                imgPhoto.setImageURI(Uri.fromFile(new File(getStorageDir(), cliente.getPathPhoto())));
            } else {
                imgPhoto.setImageResource(R.drawable.user_default_photo);
            }
        }

        //seteamos el titulo segun edit o no
        txtTitle.setText(edit ? R.string.clientes_edit : R.string.clientes_add);

        if (savedInstanceState != null) {
            mCurrentPhotoPath = savedInstanceState.getString("mCurrentPhotoPath");
            mCurrentPhotoFileName = savedInstanceState.getString("mCurrentPhotoFileName");
        }
    }

    private View.OnClickListener EscanearCedula = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
                try {
                    //instanciamos la clase de integracion con el scan
                    ScanIntegrator2 scanIntegrator = new ScanIntegrator2(clientes.this);
                    scanIntegrator.addExtra("SCAN_MODE", "QR_CODE_MODE");
                    scanIntegrator.addExtra("SCAN_CAMERA_ID", 1);
                    scanIntegrator.initiateScan();

            } catch (Exception ex) {
                Toast.makeText(clientes.this, "Error al intentar escanear cedula: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    };

    private View.OnClickListener Grabar = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            try {

                boolean error = false;

                //vamos a revisar los obligatorios
                String claveFiscal = txtClaveFiscal.getText().toString().trim();
                String nombre = txtNombre.getText().toString().trim();
                String apellido = txtApellido.getText().toString().trim();
                String limite = txtLimite.getText().toString().trim();

                if (TextUtils.isEmpty(claveFiscal)) {
                    error = true;
                    txtClaveFiscal.setError("RUT obligatorio");
                }
                else if(!ValeUtils.ValidateRUT(claveFiscal) ){
                    error = true;
                    txtClaveFiscal.setError("RUT no válido");
                }
                else if (!unicidadClaveFiscal(cliente, claveFiscal)) {
                    error = true;
                    txtClaveFiscal.setError("RUT ya asociado");
                }

                if (TextUtils.isEmpty(nombre)) {
                    error = true;
                    txtNombre.setError("Nombre obligatorio");
                }

                if (TextUtils.isEmpty(apellido)) {
                    error = true;
                    txtApellido.setError("Apellido obligatorio");
                }

                if (TextUtils.isEmpty(limite)) {
                    error = true;
                    txtLimite.setError("Limite obligatorio");
                }

                if (!error) {
                    cliente.setClaveFiscal(claveFiscal);
                    cliente.setNombre(nombre);
                    cliente.setApellido(apellido);
                    cliente.setTelefono(txtTelefono.getText().toString());
                    cliente.setObservacion(txtObservaciones.getText().toString());
                    cliente.setLimite(Double.parseDouble(limite));

                    if (!TextUtils.isEmpty(mCurrentPhotoFileName))
                        cliente.setPathPhoto(mCurrentPhotoFileName);

                    new ClienteDao(getContentResolver()).saveOrUpdate(cliente);

                    Toast.makeText(clientes.this, "Cliente grabado con exito", Toast.LENGTH_SHORT).show();

                    onBackPressed();
                }
            } catch (Exception ex) {
                Toast.makeText(clientes.this, "Error al intentar grabar el cliente: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    };

    private View.OnClickListener Eliminar = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            try {

                //verificamos si el cliente tiene deuda
                Double deuda = 0d;
                CuentaCteTotal _total = new CuentaCteTotalesDao(getContentResolver()).find(cliente.getId());

                if (_total != null) {  //si hay registros del cliente
                    deuda = _total.getSaldo();
                }

                String mensaje = "¿Desea eliminar el cliente?";

                if(deuda > 0)
                    mensaje = "<b>Atención: El cliente posee saldo deudor.</b><br/>¿Igualmente desea eliminarlo?";


                new ConfirmDialog(clientes.this, "Eliminar Cliente", mensaje)
                        .set_positiveButtonText("SI")
                        .set_positiveButtonAction(new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                cliente.setBaja(1);

                                new ClienteDao(getContentResolver()).saveOrUpdate(cliente);

                                Toast.makeText(clientes.this, "Cliente eliminado con exito", Toast.LENGTH_SHORT).show();

                                onBackPressed();
                            }
                        })
                        .show();

            } catch (Exception ex) {
                Toast.makeText(clientes.this, "Error al intentar eliminar el cliente: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    };

    public boolean unicidadClaveFiscal(Cliente cliente, String code) {
        if (TextUtils.isEmpty(code))
            return true;

        List<Cliente> check = new ClienteDao(getContentResolver()).list(String.format(ClienteTable.COLUMN_CLAVE_FISCAL + "='" + code + "' and _id <> %d and baja = 0", cliente.getId()), null, null);

        return check.size() == 0;
    }

    String mCurrentPhotoPath;
    String mCurrentPhotoFileName;

    private File getStorageDir() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
    }

    private File createImageFile() throws IOException {
        // Crear un file de imagen
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "CC_" + timeStamp + "_";
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                getStorageDir()      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        //mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        mCurrentPhotoPath = image.getAbsolutePath();
        mCurrentPhotoFileName = image.getName();
        return image;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        outState.putString("mCurrentPhotoPath", mCurrentPhotoPath);
        outState.putString("mCurrentPhotoFileName", mCurrentPhotoFileName);

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            //Bundle extras = data.getExtras();
            //Bitmap imageBitmap = (Bitmap) extras.get("data");
            //imgPhoto.setImageBitmap(imageBitmap);

            File file = new File(mCurrentPhotoPath);
            imgPhoto.setImageURI(Uri.fromFile(file));
        }
        else if(requestCode == ScanIntegrator2.REQUEST_CODE){
            //obtenemos el resultado del scan
            ScanResult2 scanContent = ScanIntegrator2.parseActivityResult(requestCode, resultCode, data);

            if(scanContent != null) {
                Log.d("RESULT", scanContent.getContents());

                //https://portal.sidiv.registrocivil.cl/docstatus?RUN=21940319-4&type=CEDULA_EXT&serial=200269295&mrz=200269295170020141612114
                if(scanContent.getContents().toUpperCase().contains("RUN")){
                    String[] urlParts = scanContent.getContents().toUpperCase().split("\\?");
                    String[] params = urlParts[1].split("&");//tomamos los parametros de la URL

                    for(String p : params){
                        if(p.startsWith("RUN=")) {
                            //cargamos el RUT leido
                            txtClaveFiscal.setText(p.replace("RUN=", "").replace("-", "").replace(".", ""));

                            Window.FocusViewShowSoftKeyboard(clientes.this, txtNombre);

                            break;
                        }
                    }
                }

                //Toast.makeText(clientes.this, scanContent.getContents(), Toast.LENGTH_SHORT).show();

            }else
                Toast.makeText(clientes.this, "No se recibieron datos del scan!", Toast.LENGTH_SHORT).show();
        }
    }


}
