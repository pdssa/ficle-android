package com.pds.ficle.dashboard.view;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Formato;
import com.pds.common.Formatos;
import com.pds.common.Usuario;
import com.pds.common.VentasHelper;
import com.pds.common.activity.TimerMainActivity;
import com.pds.common.dao.ComprobanteDao;
import com.pds.common.dao.VentaDao;
import com.pds.common.db.ComprobanteTable;
import com.pds.common.model.Comprobante;
import com.pds.common.model.Venta;
import com.pds.common.model.VentaAbstract;
import com.pds.ficle.dashboard.R;
import com.pds.ficle.dashboard.adapter.UltimasVentasAdapter;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;


public class DashboardMainActivity extends TimerMainActivity {

    private List<VentaAbstract> ventasRecientesList;
    private ListView lstUltimasVentas;
    private Button btnVerTodasVentas;
    private Button btnVistaAyer;
    private Button btnVistaHoy;
    private Button btnVistaMensual;
    private TextView txtFrecuencia;
    private boolean vistaDiaria;
    private Button btnRptDetVentas;
    private Config config;
    private DecimalFormat FORMATO_DECIMAL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {

            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_dashboard_main);

            Config config = new Config(this);

            //Configuracion de formatos segun PAIS
            Init_CustomConfigPais(config.PAIS);

            AsignarViews();

            CambiarVista(TIPO_VISTA.DIA_HOY);

        } catch (Exception ex) {
            Toast.makeText(DashboardMainActivity.this, ex.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    private void Init_CustomConfigPais(String codePais) {

        FORMATO_DECIMAL = codePais.equals("AR") ? Formatos.DecimalFormat : Formatos.DecimalFormat_CH;

        View dash_total = findViewById(R.id.dash_vw_total);
        View dash_promedios = findViewById(R.id.dash_vw_promedios);
        View dash_totales = findViewById(R.id.dash_vw_totales);

        if (codePais.equals(("AR"))) {
            dash_total.findViewById(R.id.vw_simple_txt_3).setVisibility(View.VISIBLE);
            dash_promedios.findViewById(R.id.vw_simple_txt_3).setVisibility(View.VISIBLE);
            dash_totales.findViewById(R.id.vw_simple_txt_3).setVisibility(View.VISIBLE);
        } else {
            dash_total.findViewById(R.id.vw_simple_txt_3).setVisibility(View.INVISIBLE);
            dash_promedios.findViewById(R.id.vw_simple_txt_3).setVisibility(View.INVISIBLE);
            dash_totales.findViewById(R.id.vw_simple_txt_3).setVisibility(View.INVISIBLE);
        }

        //FORMATO_DECIMAL_SECUND = codePais.equals("AR") ? Formatos.DecimalFormat : Formatos.DecimalFormat_US;
        //private DecimalFormat FORMATO_DECIMAL;FORMATO_FECHA

    }

    private enum TIPO_VISTA {
        DIA_HOY,
        DIA_AYER,
        MES
    }

    private void AsignarViews() {
        txtFrecuencia = (TextView) findViewById(R.id.dash_txt_frecuencia);
        lstUltimasVentas = (ListView) findViewById(R.id.dash_lv_ult_vtas);
        //lstUltimasVentas.setEmptyView(findViewById(R.id.));
        btnVerTodasVentas = (Button) findViewById(R.id.dash_btn_ult_vtas);
        btnVerTodasVentas.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ObtenerDataUltimasVentasDia(new Date(), 999999999);
            }
        });
        btnVistaMensual = (Button) findViewById(R.id.dash_btn_vista_mes);
        btnVistaMensual.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                CambiarVista(TIPO_VISTA.MES);
            }
        });
        btnRptDetVentas = (Button) findViewById(R.id.dash_btn_rpt_detventas);
        btnRptDetVentas.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(DashboardMainActivity.this, RptDetalleVentas.class);
                startActivity(i);
            }
        });
        btnVistaAyer = (Button) findViewById(R.id.dash_btn_vista_ayer);
        btnVistaAyer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                CambiarVista(TIPO_VISTA.DIA_AYER);
            }
        });
        btnVistaHoy = (Button) findViewById(R.id.dash_btn_vista_hoy);
        btnVistaHoy.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                CambiarVista(TIPO_VISTA.DIA_HOY);
            }
        });
    }

    private void CambiarVista(TIPO_VISTA tipo_vista) {
        switch (tipo_vista) {
            case DIA_HOY: {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, -1);

                txtFrecuencia.setText(Formato.FormateaDate(new Date(), Formato.DateTwoDigFormatSlash));

                vistaDiaria = true;

                GenerarInformes(new Date(), cal.getTime());//hoy vs ayer
            }
            break;
            case DIA_AYER: {
                Calendar cal = Calendar.getInstance();
                Calendar cal2 = Calendar.getInstance();
                cal.add(Calendar.DATE, -1);
                cal2.add(Calendar.DATE, -2);

                txtFrecuencia.setText(Formato.FormateaDate(cal.getTime(), Formato.DateTwoDigFormatSlash));

                vistaDiaria = true;

                GenerarInformes(cal.getTime(), cal2.getTime());//ayer vs antes de ayer
            }
            break;
            case MES: {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.MONTH, -1);

                txtFrecuencia.setText(Formato.FormateaDate(new Date(), Formato.MonthYearFormat));

                vistaDiaria = false;

                GenerarInformes(new Date(), cal.getTime());//este mes vs mes anterior

            }
            break;
        }



        /*if (vistaDiaria) {//si estamos en vista Diaria vamos a pasar a MES al presionar el boton


            txtFrecuencia.setText("MES");

            btnVistaMensual.setText("Vista Diaria");

            vistaDiaria = false;

            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.MONTH, -1);

            GenerarInformes(new Date(), cal.getTime());
        } else {//sino pasamos a la vista DIA
            txtFrecuencia.setText("HOY");

            btnVistaMensual.setText("Vista Mensual");

            vistaDiaria = true;

            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -1);

            GenerarInformes(new Date(), cal.getTime());
        }*/
    }

    private void GenerarInformes(Date fecha, Date fechaRef) {

        CargarValoresPorMedio(fecha);

        GenerarInformeValoresVentas(fecha, fechaRef);

        GenerarInformeVtasDiariaPorHora(fecha);

        GenerarInformeVtasDiariaPorApp(fecha);

        GenerarInformeVtasDiariaPorVendedor(fecha);

        //ObtenerDataUltimasVentasDia(fecha, 5); //ultimas 5 ventas

    }

    private List<VentaAbstract> GetVentasFecha(Date fecha) {

        List<VentaAbstract> ventasList = null;
        if (vistaDiaria) {

            // traemos solo las ventas del dia
            List<Venta> ventas = new VentaDao(getContentResolver()).list(VentasHelper.VENTA_FECHA + " = '" + Formatos.FormateaDate(fecha, Formatos.DateFormat) + "' ", null, " _id DESC");
            List<Comprobante> comprobantes = new ComprobanteDao(getContentResolver()).list(ComprobanteTable.COMPROBANTE_FECHA + " = '" + Formatos.FormateaDate(fecha, Formatos.DateFormat) + "' ", null, " _id DESC");

            ventasList = new ArrayList<VentaAbstract>();
            ventasList.addAll(ventas);
            ventasList.addAll(comprobantes);

        } else {

            // traemos solo las ventas del mes
            List<Venta> ventas = new VentaDao(getContentResolver()).list("substr(" + VentasHelper.VENTA_FECHA + ",4) ='" + Formatos.FormateaDate(fecha, Formatos.MonthYearFormat) + "'", null, " _id DESC");
            List<Comprobante> comprobantes = new ComprobanteDao(getContentResolver()).list("substr(" + ComprobanteTable.COMPROBANTE_FECHA + ",4) ='" + Formatos.FormateaDate(fecha, Formatos.MonthYearFormat) + "'", null, " _id DESC");

            ventasList = new ArrayList<VentaAbstract>();
            ventasList.addAll(ventas);
            ventasList.addAll(comprobantes);

        }

        //ordenamos las ventas por fecha
        Collections.sort(ventasList, new Comparator<VentaAbstract>() {
            @Override
            public int compare(VentaAbstract lhs, VentaAbstract rhs) {
                return lhs.compareTo(rhs);
            }
        });

        return ventasList;
    }

    //region INFORME VENTAS POR HORARIO

    private void GenerarInformeVtasDiariaPorHora(Date fecha) {

        int width = ((Double) (GetWindowWidth() * 0.95)).intValue();
        int height = ((Double) (GetWindowHeigth() * 0.21)).intValue();

        WebView wv_ventas_por_aplicacion = (WebView) findViewById(R.id.dash_wv_vtas_hora);
        wv_ventas_por_aplicacion.getSettings().setJavaScriptEnabled(true);
        String html = "<html><head><script src='chart2.js'></script>";
        html += "</head><body>";
        html += "<center><canvas id='myChart' width='" + String.valueOf(width) + "' height='" + String.valueOf(height) + "'></canvas></center>";
        html += "<script type='text/javascript'>var ctx = document.getElementById('myChart').getContext('2d');";

        /*html +=  "Pie.defaults = {" +
                "        segmentShowStroke : true," +
                "        segmentStrokeColor : '#fff'," +
                "        segmentStrokeWidth : 2," +
                "        animation : true," +
                "        animationSteps : 100," +
                "        animationEasing : 'easeOutBounce'," +
                "        animateRotate : true," +
                "        animateScale : false," +
                "        onAnimationComplete : null};";*/

//        html += "var data = {" +
//                " labels : ['6 AM','7 AM','8 AM','9 AM','10 AM','11 AM','12 PM','1 PM','2 PM','3 PM','4 PM','5 PM','6 PM','7 PM','8 PM','9 PM','10 PM']," +
//                " datasets : [" +
//                "{" +
//                "     fillColor : 'rgba(220,220,220,0.5)'," +
//                "     strokeColor : 'rgba(220,220,220,1)'," +
//                "     data : [12,10,30,65,59,90,81,56,55,40,35,25,50,12,10,30,65]" +
//        "},"+
//        "{"+
//        "     fillColor : 'rgba(151,187,205,0.5)',"+
//        "             strokeColor : 'rgba(151,187,205,1)',"+
//        "         data : [28,48,40,19,96,27,100]"+
//                "}" +
//                "]" +
//                "};";

        html += "var data = {" + ObtenerDataVentasDiaPorHora(fecha) + "};";

        String graphOptions = "{" +
                "animation : false," +
                "scaleShowLabels : false" +
                "}";

        html += "var myPie = new Chart(ctx).Bar(data, " + graphOptions + ");</script>";

        html += "</body></html>";

        wv_ventas_por_aplicacion.loadDataWithBaseURL("file:///android_asset/", html, "text/html", "utf-8", null);
    }

    private String ObtenerDataVentasDiaPorHora(Date dia) {
        String result = "";

        try {

            List<VentaAbstract> ventaList = GetVentasFecha(dia);

            //Array de ventas por hora
            Hora[] ventasPorHora = new Hora[24];
            ventasPorHora[0] = new Hora("00", "0 AM", 0, true);
            ventasPorHora[1] = new Hora("01", "1 AM", 0, true);
            ventasPorHora[2] = new Hora("02", "2 AM", 0, true);
            ventasPorHora[3] = new Hora("03", "3 AM", 0, true);
            ventasPorHora[4] = new Hora("04", "4 AM", 0, true);
            ventasPorHora[5] = new Hora("05", "5 AM", 0, true);
            ventasPorHora[6] = new Hora("06", "6 AM", 0, true);
            ventasPorHora[7] = new Hora("07", "7 AM", 0, true);
            ventasPorHora[8] = new Hora("08", "8 AM", 0, true);
            ventasPorHora[9] = new Hora("09", "9 AM", 0, true);
            ventasPorHora[10] = new Hora("10", "10 AM", 0, true);
            ventasPorHora[11] = new Hora("11", "11 AM", 0, true);
            ventasPorHora[12] = new Hora("12", "12 PM", 0, true);
            ventasPorHora[13] = new Hora("13", "13 PM", 0, true);
            ventasPorHora[14] = new Hora("14", "14 PM", 0, true);
            ventasPorHora[15] = new Hora("15", "15 PM", 0, true);
            ventasPorHora[16] = new Hora("16", "16 PM", 0, true);
            ventasPorHora[17] = new Hora("17", "17 PM", 0, true);
            ventasPorHora[18] = new Hora("18", "18 PM", 0, true);
            ventasPorHora[19] = new Hora("19", "19 PM", 0, true);
            ventasPorHora[20] = new Hora("20", "20 PM", 0, true);
            ventasPorHora[21] = new Hora("21", "21 PM", 0, true);
            ventasPorHora[22] = new Hora("22", "22 PM", 0, true);
            ventasPorHora[23] = new Hora("23", "23 PM", 0, true);
            //

            // recorremos los items
            if (ventaList.size() > 0) {
                for (VentaAbstract vta : ventaList) {

                    String hora = vta.getHora().substring(0, 2);

                    ventasPorHora[Integer.parseInt(hora)].sumMontoVenta(vta.getTotal());

                }
            }


            String label = "";
            String data = "";

            for (Hora hora : ventasPorHora) {
                if (hora.visible) {

                    if (!label.equals(""))
                        label += ",";
                    label += "'" + hora.hora_desc + "'";

                    if (!data.equals(""))
                        data += ",";
                    data += hora.monto;

                }
            }

            result = "labels : [" + label + "]," +
                    " datasets : [{" +
                    "  fillColor : 'rgba(220,220,220,0.5)'," +
                    "  strokeColor : 'rgba(220,220,220,1)'," +
                    "  data : [" + data + "]}]";

        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }

        return result;
    }

    //endregion

    //region INFORMES VALORES DE VENTAS

    private void GenerarInformeValoresVentas(Date dia, Date diaReferencia) {

        try {

            //**obtenemos informacion del dia**
            List<VentaAbstract> ventasList = GetVentasFecha(dia);

            int cantVentasDia = 0;
            double totalVentasDia = 0;

            // recorremos los items contando ventas y sumando totales
            if (ventasList.size() > 0) {
                for (VentaAbstract vta : ventasList) {
                    totalVentasDia += vta.getTotal();
                    cantVentasDia++;
                }
            }

            //**obtenemos informacion del dia de referencia**
            List<VentaAbstract> ventasReferenciaList = GetVentasFecha(diaReferencia);

            int cantVentasDiaReferencia = 0;
            double totalVentasDiaReferencia = 0;

            // recorremos los items contando ventas y sumando totales
            if (ventasReferenciaList.size() > 0) {
                for (VentaAbstract vta : ventasReferenciaList) {
                    totalVentasDiaReferencia += vta.getTotal();
                    cantVentasDiaReferencia++;
                }
            }

            //*llamamos a los informes*
            CargarValorTotal(totalVentasDia, totalVentasDiaReferencia, totalVentasDia, 0, 0);
            CargarValoresQ(cantVentasDia, cantVentasDiaReferencia);
            CargarValoresPromedios(
                    cantVentasDia != 0 ? totalVentasDia / cantVentasDia : 0,
                    cantVentasDiaReferencia != 0 ? totalVentasDiaReferencia / cantVentasDiaReferencia : 0);

        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }


    }

    private void CargarValorTotal(double total, double totalReferencia, double subtotal, double descuentos, double impuestos) {
        View vw_totales = findViewById(R.id.dash_vw_total);

        ((TextView) vw_totales.findViewById(R.id.vw_simple_txt_head)).setText("TOTAL");
        vw_totales.findViewById(R.id.vw_simple_txt_1).setVisibility(View.VISIBLE);
        ((TextView) vw_totales.findViewById(R.id.vw_simple_txt_1)).setText("$");

        //valor
        String str_valor = Formatos.FormateaDecimal(total, FORMATO_DECIMAL);
        vw_totales.findViewById(R.id.vw_simple_txt_2).setVisibility(View.VISIBLE);
        if (total > 9999999)//ajustamos el font
            ((TextView) vw_totales.findViewById(R.id.vw_simple_txt_2)).setTextSize(25);
        else
            ((TextView) vw_totales.findViewById(R.id.vw_simple_txt_2)).setTextSize(40);


        if (!str_valor.contains(",")) {//sino tiene decimales
            ((TextView) vw_totales.findViewById(R.id.vw_simple_txt_2)).setText(str_valor);
            //vw_totales.findViewById(R.id.vw_simple_txt_3).setVisibility(View.VISIBLE);
            ((TextView) vw_totales.findViewById(R.id.vw_simple_txt_3)).setText(",00");
        } else {//si tiene decimales
            ((TextView) vw_totales.findViewById(R.id.vw_simple_txt_2)).setText(str_valor.split(",")[0]);
            //vw_totales.findViewById(R.id.vw_simple_txt_3).setVisibility(View.VISIBLE);
            String decimales = str_valor.split(",")[1];
            ((TextView) vw_totales.findViewById(R.id.vw_simple_txt_3)).setText("," + (decimales.length() > 2 ? decimales.substring(0, 2) : decimales));
        }

        //indicador comparativo
        if (total > totalReferencia) {
            ((ImageView) vw_totales.findViewById(R.id.vw_simple_icon)).setImageResource(R.drawable.up_green);
            ((TextView) vw_totales.findViewById(R.id.vw_simple_txt_perc)).setTextColor(getResources().getColor(R.color.boton_verde));
        } else if (total < totalReferencia) {
            ((ImageView) vw_totales.findViewById(R.id.vw_simple_icon)).setImageResource(R.drawable.down_red);
            ((TextView) vw_totales.findViewById(R.id.vw_simple_txt_perc)).setTextColor(getResources().getColor(R.color.boton_rojo));
        } else {
            ((ImageView) vw_totales.findViewById(R.id.vw_simple_icon)).setImageResource(R.drawable.equal_blue);
            ((TextView) vw_totales.findViewById(R.id.vw_simple_txt_perc)).setTextColor(getResources().getColor(R.color.boton_azul));
        }
        ((TextView) vw_totales.findViewById(R.id.vw_simple_txt_perc)).setText(CalculaPorcentajeDiferencia(total, totalReferencia) + "%");

        //detalle adicional
        //((TextView) vw_totales.findViewById(R.id.vw_simple_ext_txt_subtotal)).setText("Subtotal  : $" + Formatos.FormateaDecimal(subtotal, Formatos.DecimalFormat));
        //((TextView) vw_totales.findViewById(R.id.vw_simple_ext_txt_descuentos)).setText("Descuentos: $" + Formatos.FormateaDecimal(descuentos, Formatos.DecimalFormat));
        //((TextView) vw_totales.findViewById(R.id.vw_simple_ext_txt_impuestos)).setText("Impuestos : $" + Formatos.FormateaDecimal(impuestos, Formatos.DecimalFormat));

    }

    private void CargarValoresQ(int cantidad, int cantidadReferencia) {
        View vw_totales = findViewById(R.id.dash_vw_totales);

        ((TextView) vw_totales.findViewById(R.id.vw_simple_txt_head)).setText("CDAD TRX");
        vw_totales.findViewById(R.id.vw_simple_txt_1).setVisibility(View.INVISIBLE);
        vw_totales.findViewById(R.id.vw_simple_txt_2).setVisibility(View.VISIBLE);
        ((TextView) vw_totales.findViewById(R.id.vw_simple_txt_2)).setText(String.valueOf(cantidad));
        vw_totales.findViewById(R.id.vw_simple_txt_3).setVisibility(View.GONE);

        //comparamos con el valor de referencia
        if (cantidad > cantidadReferencia) {
            ((ImageView) vw_totales.findViewById(R.id.vw_simple_icon)).setImageResource(R.drawable.up_green);
            ((TextView) vw_totales.findViewById(R.id.vw_simple_txt_perc)).setTextColor(getResources().getColor(R.color.boton_verde));
        } else if (cantidad < cantidadReferencia) {
            ((ImageView) vw_totales.findViewById(R.id.vw_simple_icon)).setImageResource(R.drawable.down_red);
            ((TextView) vw_totales.findViewById(R.id.vw_simple_txt_perc)).setTextColor(getResources().getColor(R.color.boton_rojo));
        } else {
            ((ImageView) vw_totales.findViewById(R.id.vw_simple_icon)).setImageResource(R.drawable.equal_blue);
            ((TextView) vw_totales.findViewById(R.id.vw_simple_txt_perc)).setTextColor(getResources().getColor(R.color.boton_azul));
        }

        ((TextView) vw_totales.findViewById(R.id.vw_simple_txt_perc)).setText(CalculaPorcentajeDiferencia(cantidad, cantidadReferencia) + "%");

        //((TextView) vw_totales.findViewById(R.id.vw_simple_txt_comp)).setText("(150)");
        vw_totales.findViewById(R.id.vw_simple_txt_comp).setVisibility(View.GONE);
    }

    private void CargarValoresPromedios(double valor, double valorReferencia) {
        View vw_promedios = findViewById(R.id.dash_vw_promedios);

        ((TextView) vw_promedios.findViewById(R.id.vw_simple_txt_head)).setText("TRX. PROM.");
        vw_promedios.findViewById(R.id.vw_simple_txt_1).setVisibility(View.VISIBLE);
        ((TextView) vw_promedios.findViewById(R.id.vw_simple_txt_1)).setText("$");

        String str_valor = Formatos.FormateaDecimal(valor, FORMATO_DECIMAL);
        vw_promedios.findViewById(R.id.vw_simple_txt_2).setVisibility(View.VISIBLE);
        if (valor > 9999999)//ajustamos el font
            ((TextView) vw_promedios.findViewById(R.id.vw_simple_txt_2)).setTextSize(25);
        else
            ((TextView) vw_promedios.findViewById(R.id.vw_simple_txt_2)).setTextSize(40);

        if (!str_valor.contains(",")) {//sino tiene decimales
            ((TextView) vw_promedios.findViewById(R.id.vw_simple_txt_2)).setText(str_valor);
            //vw_promedios.findViewById(R.id.vw_simple_txt_3).setVisibility(View.VISIBLE);
            ((TextView) vw_promedios.findViewById(R.id.vw_simple_txt_3)).setText(",00");
        } else {//si tiene decimales
            ((TextView) vw_promedios.findViewById(R.id.vw_simple_txt_2)).setText(str_valor.split(",")[0]);
            //vw_promedios.findViewById(R.id.vw_simple_txt_3).setVisibility(View.VISIBLE);
            String decimales = str_valor.split(",")[1];
            ((TextView) vw_promedios.findViewById(R.id.vw_simple_txt_3)).setText("," + (decimales.length() > 2 ? decimales.substring(0, 2) : decimales));
        }

        if (valor > valorReferencia) {
            ((ImageView) vw_promedios.findViewById(R.id.vw_simple_icon)).setImageResource(R.drawable.up_green);
            ((TextView) vw_promedios.findViewById(R.id.vw_simple_txt_perc)).setTextColor(getResources().getColor(R.color.boton_verde));
        } else if (valor < valorReferencia) {
            ((ImageView) vw_promedios.findViewById(R.id.vw_simple_icon)).setImageResource(R.drawable.down_red);
            ((TextView) vw_promedios.findViewById(R.id.vw_simple_txt_perc)).setTextColor(getResources().getColor(R.color.boton_rojo));
        } else {
            ((ImageView) vw_promedios.findViewById(R.id.vw_simple_icon)).setImageResource(R.drawable.equal_blue);
            ((TextView) vw_promedios.findViewById(R.id.vw_simple_txt_perc)).setTextColor(getResources().getColor(R.color.boton_azul));
        }

        ((TextView) vw_promedios.findViewById(R.id.vw_simple_txt_perc)).setText(CalculaPorcentajeDiferencia(valor, valorReferencia) + "%");
        //((TextView) vw_promedios.findViewById(R.id.vw_simple_txt_comp)).setText("(-$10.20)");
        vw_promedios.findViewById(R.id.vw_simple_txt_comp).setVisibility(View.GONE);
    }

    private String CalculaPorcentajeDiferencia(int a, int b) {
        if (b == 0 && a == 0)
            return "0";
        else if ((b == 0 && a > 0))
            return "100";
        else {

            double dif = ((a * 100) / b);
            dif = dif - 100;

            return String.valueOf(Formatos.FormateaDecimal(dif, Formatos.DecimalFormat));
        }
    }

    private String CalculaPorcentajeDiferencia(double a, double b) {
        if (b == 0 && a == 0)
            return "0";
        else if ((b == 0 && a > 0))
            return "100";
        else {

            double dif = ((a * 100) / b);
            dif = dif - 100;

            return String.valueOf(Formatos.FormateaDecimal(dif, Formatos.DecimalFormat));
        }
    }

    //endregion

    //region INFORME VENTAS POR MEDIO PAGO

    private void CargarValoresPorMedio(Date fecha) {
        View vw_promedios = findViewById(R.id.dash_vw_total_medio_pago);
        ((TextView) vw_promedios.findViewById(R.id.vw_grafico_txt_head)).setText("TIPOS PAGOS");

        WebView wv_ventas_por_medio = (WebView) vw_promedios.findViewById(R.id.vw_grafico_wv_chart);
        wv_ventas_por_medio.getSettings().setJavaScriptEnabled(true);
        String html = "<html><head><script src='chart2.js'></script>";
        html += "</head><body>";
        html += "<center><canvas id='myChart' width='70' height='70'></canvas></center>";
        html += "<script type='text/javascript'>var ctx = document.getElementById('myChart').getContext('2d');";

        /*html +=  "Pie.defaults = {" +
                "        segmentShowStroke : true," +
                "        segmentStrokeColor : '#fff'," +
                "        segmentStrokeWidth : 2," +
                "        animation : true," +
                "        animationSteps : 100," +
                "        animationEasing : 'easeOutBounce'," +
                "        animateRotate : true," +
                "        animateScale : false," +
                "        onAnimationComplete : null};";*/
        /*
        html += "var data = [" +
                "{" +
                "    value: 100," +
                "            color:'#3E855C'" +
                "}," +
                "{" +
                "    value : 50," +
                "            color : '#689ABE'" +
                "}" +
                "];";
        */

        String graphOptions = "{" +
                "animation : false" +
                "}";

        html += "var data = [" + ObtenerDataVentasDiaPorMedio(fecha) + "];";
        html += "var myPie = new Chart(ctx).Pie(data, " + graphOptions + ");</script>";

        html += "</body></html>";

        wv_ventas_por_medio.loadDataWithBaseURL("file:///android_asset/", html, "text/html", "utf-8", null);
    }

    private String ObtenerDataVentasDiaPorMedio(Date dia) {
        String result = "";

        try {

            List<VentaAbstract> ventaList = GetVentasFecha(dia);

            double totalEfectivo = 0;
            double totalTarjeta = 0;
            double totalCheque = 0;
            double totalOtros = 0;

            // recorremos los items sumando por medio de pago
            if (ventaList.size() > 0) {
                for (VentaAbstract vta : ventaList) {

                    if (vta.getMedio_pago().equals("EFECTIVO"))
                        totalEfectivo += vta.getTotal();
                    else if (vta.getMedio_pago().contains("TARJETA"))
                        totalTarjeta += vta.getTotal();
                    else if (vta.getMedio_pago().equals("CHEQUE"))
                        totalCheque += vta.getTotal();
                    else
                        totalOtros += vta.getTotal();

                }
            }

            //iniciamos el formato de los datos para el pie chart
            result = "{" +
                    "value: " + totalEfectivo +
                    ",color:'#3E855C'" +
                    "}," +
                    "{" +
                    "value : " + totalTarjeta +
                    ",color : '#689ABE'" +
                    "}," +
                    "{" +
                    "value : " + totalCheque +
                    ",color : '#F1F5FB'" +
                    "}," +
                    "{" +
                    "value : " + totalOtros +
                    ",color : '#B24AA1'" +
                    "}";

            //armamos la lista con los totales por medio de pago
            View vw_medios = findViewById(R.id.dash_vw_total_medio_pago);

            List<Venta> ventasPorMedio = new ArrayList<Venta>();

            ventasPorMedio.add(new Venta(0, totalEfectivo, 0, "", "", "", 0, "EFECTIVO", 0, 0));
            ventasPorMedio.add(new Venta(0, totalTarjeta, 0, "", "", "", 0, "TARJETA CREDITO", 0, 0));

            ((ListView) vw_medios.findViewById(R.id.vw_grafico_lst_list)).setAdapter(new TotalMediosAdapter(this, ventasPorMedio));

            //armamos la lista con los totales por medio de pago
            View vw_medios_2 = findViewById(R.id.dash_vw_total_medio_pago);

            List<Venta> ventasPorMedio_2 = new ArrayList<Venta>();

            ventasPorMedio_2.add(new Venta(0, totalCheque, 0, "", "", "", 0, "CHEQUE", 0, 0));
            ventasPorMedio_2.add(new Venta(0, totalOtros, 0, "", "", "", 0, "OTRO", 0, 0));

            ((ListView) vw_medios_2.findViewById(R.id.vw_grafico_lst_list_2)).setAdapter(new TotalMediosAdapter(this, ventasPorMedio_2));


        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }

        return result;
    }

    private void ObtenerDataUltimasVentasDia(Date dia, int cantVentasMostrar) {
        try {

            List<VentaAbstract> ventasList = GetVentasFecha(dia);

            this.ventasRecientesList = new ArrayList<VentaAbstract>();

            // recorremos los items
            if (ventasList.size() > 0) {
                for (int i = 0; i < ventasList.size(); i++) {
                    if (cantVentasMostrar <= 0)
                        break;
                    this.ventasRecientesList.add(ventasList.get(i));

                    cantVentasMostrar--;
                }
            }

            if (ventasRecientesList.size() > 0) //hay resultados para mostrar
                findViewById(R.id.dash_txt_no_results).setVisibility(View.GONE);
            else
                //sin informacion
                findViewById(R.id.dash_txt_no_results).setVisibility(View.VISIBLE);

            lstUltimasVentas.setAdapter(new UltimasVentasAdapter(this, ventasRecientesList, vistaDiaria, FORMATO_DECIMAL));


        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    //endregion

    //region INFORME VENTAS POR APP

    private String colorPorApp(String app) {
        if (app.equals("RCG"))
            return "#F38630";

        if (app.equals("PDV"))
            return "#E0E4CC";

        if (app.equals("PAG"))
            return "#69D2E7";

        if (app.equals("DEP"))
            return "#3F9F3F";

        if (app.equals("BIP"))
            return "#3F9F3F";

        return "#FBFCF7";
    }

    private void GenerarInformeVtasDiariaPorApp(Date fecha) {

        int width = ((Double) (GetWindowWidth() * 0.3)).intValue();
        int height = ((Double) (GetWindowHeigth() * 0.4)).intValue();

        WebView wv_ventas_por_aplicacion = (WebView) findViewById(R.id.dash_wv_vtas_app);
        wv_ventas_por_aplicacion.getSettings().setJavaScriptEnabled(true);
        String html = "<html><head><script src='chart2.js'></script>";
        html += "</head><body>";

        String data = ObtenerDataVtasDiariaPorApp(fecha);

        if (!data.equals("")) {//si hay informacion para mostrar

            html += "<center><canvas id='myChart' width='" + String.valueOf(width) + "' height='" + String.valueOf(height) + "'></canvas></center>";
            html += "<script type='text/javascript'>var ctx = document.getElementById('myChart').getContext('2d');";

        /*html +=  "Pie.defaults = {" +
                "        segmentShowStroke : true," +
                "        segmentStrokeColor : '#fff'," +
                "        segmentStrokeWidth : 2," +
                "        animation : true," +
                "        animationSteps : 100," +
                "        animationEasing : 'easeOutBounce'," +
                "        animateRotate : true," +
                "        animateScale : false," +
                "        onAnimationComplete : null};";*/
        /*
        html += "var data = [" +
                "{" +
                "    value: 30," +
                "            color:'#F38630'," +
                "            label:'test'" +
                "}," +
                "{" +
                "    value : 50," +
                "            color : '#E0E4CC'" +
                "}," +
                "{" +
                "    value : 100," +
                "            color : '#69D2E7'" +
                "}" +
                "];";
        */

            String graphOptions = "{" +
                    "animation : false" +
                    "}";

            html += "var data = [" + data + "];";
            html += "var myPie = new Chart(ctx).Pie(data, " + graphOptions + ");</script>";
        } else {
            //sin resultados
            html += "<center><br/><br/><br/><p>Sin resultados</p></center>";
        }
        html += "</body></html>";

        wv_ventas_por_aplicacion.loadDataWithBaseURL("file:///android_asset/", html, "text/html", "utf-8", null);
    }

    private String ObtenerDataVtasDiariaPorApp(Date dia) {
        String dataReporte = "";

        try {

            Cursor result = null;
            if (vistaDiaria) {
                result = getContentResolver().query(
                        Uri.parse("content://com.pds.ficle.ep.ventas.contentprovider/ventas_app"),
                        VentasHelper.columnas_vw_ventas_dia_app,
                        VentasHelper.VENTA_FECHA + "='" + Formatos.FormateaDate(dia, Formatos.DateFormat) + "'",
                        new String[]{},
                        "");
            } else {
                result = getContentResolver().query(
                        Uri.parse("content://com.pds.ficle.ep.ventas.contentprovider/ventas_app_mes"),
                        VentasHelper.columnas_vw_ventas_dia_app,
                        VentasHelper.VENTA_FECHA + "='" + Formatos.FormateaDate(dia, Formatos.MonthYearFormat) + "'",
                        new String[]{},
                        "");
            }

            //iniciamos el formato de los datos
            if (result.moveToFirst()) {
                do {

                    if (!dataReporte.equals(""))
                        dataReporte += ",";

                    dataReporte += "{" +
                            "value:" + result.getDouble(VentasHelper.VENTA_IX_VW_TOTAL) +
                            ",color:'" + colorPorApp(result.getString(VentasHelper.VENTA_IX_VW_CODIGO)) + "'" +
                            ",label:'" + result.getString(VentasHelper.VENTA_IX_VW_CODIGO) + " ($ " + Formatos.FormateaDecimal(result.getDouble(VentasHelper.VENTA_IX_VW_TOTAL), Formatos.DecimalFormat) + ")'" +
                            "}";

                } while (result.moveToNext());
            }

            result.close();


        } catch (Exception ex) {
            Toast.makeText(DashboardMainActivity.this, "Error en ObtenerDataVtasDiariaPorApp: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }

        return dataReporte;
    }
    //endregion

    //region INFORME VENTAS POR VENDEDOR

    private void GenerarInformeVtasDiariaPorVendedor(Date fecha) {

        int width = ((Double) (GetWindowWidth() * 0.3)).intValue();
        int height = ((Double) (GetWindowHeigth() * 0.4)).intValue();

        WebView wv_ventas_por_vendedor = (WebView) findViewById(R.id.dash_wv_vtas_vddor);
        wv_ventas_por_vendedor.getSettings().setJavaScriptEnabled(true);
        String html = "<html><head><script src='chart2.js'></script>";
        html += "</head><body>";

        String data = ObtenerDataVtasDiariaPorVendedor(fecha);

        if (!data.equals("")) { //si hay info para mostrar

            html += "<center><canvas id='myChart' width='" + String.valueOf(width) + "' height='" + String.valueOf(height) + "'></canvas></center>";
            html += "<script type='text/javascript'>function randomColor () {";
            //html += "var max = 0xffffff;";
            //html += "return '#' + Math.round( Math.random() * max ).toString( 16 );";
            html += "var allowed = '0369cf'.split( '' ), s = '#';";
            html += "while ( s.length < 4 ) {";
            html += "       s += allowed.splice( Math.floor( ( Math.random() * allowed.length ) ), 1 );";
            html += "} return s;";
            html += "}</script>";

            html += "<script type='text/javascript'>var ctx = document.getElementById('myChart').getContext('2d');";

        /*html +=  "Pie.defaults = {" +
                "        segmentShowStroke : true," +
                "        segmentStrokeColor : '#fff'," +
                "        segmentStrokeWidth : 2," +
                "        animation : true," +
                "        animationSteps : 100," +
                "        animationEasing : 'easeOutBounce'," +
                "        animateRotate : true," +
                "        animateScale : false," +
                "        onAnimationComplete : null};";*/
        /*
        html += "var data = [" +
                "{" +
                "    value: 30," +
                "            color:'#F38630'," +
                "            label:'test'" +
                "}," +
                "{" +
                "    value : 50," +
                "            color : '#E0E4CC'" +
                "}," +
                "{" +
                "    value : 100," +
                "            color : '#69D2E7'" +
                "}" +
                "];";
        */

            String graphOptions = "{" +
                    "animation : false" +
                    "}";


            html += "var data = [" + data + "];";
            html += "var myPie = new Chart(ctx).Pie(data, " + graphOptions + ");</script>";
        } else {
            //sin resultados
            html += "<center><br/><br/><br/><p>Sin resultados</p></center>";
        }
        html += "</body></html>";

        wv_ventas_por_vendedor.loadDataWithBaseURL("file:///android_asset/", html, "text/html", "utf-8", null);
    }

    private String ObtenerDataVtasDiariaPorVendedor(Date dia) {
        String dataReporte = "";

        try {

            Cursor result = null;
            if (vistaDiaria) {
                result = getContentResolver().query(
                        Uri.parse("content://com.pds.ficle.ep.ventas.contentprovider/ventas_vddor"),
                        VentasHelper.columnas_vw_ventas_dia_vddor,
                        VentasHelper.VENTA_FECHA + "='" + Formatos.FormateaDate(dia, Formatos.DateFormat) + "'",
                        new String[]{},
                        "");
            } else {
                result = getContentResolver().query(
                        Uri.parse("content://com.pds.ficle.ep.ventas.contentprovider/ventas_vddor_mes"),
                        VentasHelper.columnas_vw_ventas_dia_vddor,
                        VentasHelper.VENTA_FECHA + "='" + Formatos.FormateaDate(dia, Formatos.MonthYearFormat) + "'",
                        new String[]{}, // traemos solo las ventas del mes
                        "");
            }

            //iniciamos el formato de los datos
            if (result.moveToFirst()) {
                do {

                    if (!dataReporte.equals(""))
                        dataReporte += ",";

                    dataReporte += "{" +
                            "value:" + result.getDouble(VentasHelper.VENTA_IX_VW_V_TOTAL) +
                            ",color: randomColor()" +
                            ",label:'" + result.getString(VentasHelper.VENTA_IX_VW_V_USER) + " ($ " + Formatos.FormateaDecimal(result.getDouble(VentasHelper.VENTA_IX_VW_V_TOTAL), Formatos.DecimalFormat) + ")'" +
                            "}";

                } while (result.moveToNext());
            }

            result.close();


        } catch (Exception ex) {
            Toast.makeText(DashboardMainActivity.this, "Error en ObtenerDataVtasDiariaPorVendedor: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }

        return dataReporte;
    }

    //endregion

    //region region INFORME VENTAS RECIENTES

    class TotalMediosAdapter extends ArrayAdapter<Venta> {
        private Context context;
        private List<Venta> datos;

        public TotalMediosAdapter(Context context, List<Venta> datos) {
            super(context, android.R.layout.simple_list_item_1, datos);

            this.context = context;
            this.datos = datos;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // En primer lugar "inflamos" una nueva vista, que será la que se
            // mostrará en la celda del ListView. Para ello primero creamos el
            // inflater, y después inflamos la vista.
            LayoutInflater inflater = LayoutInflater.from(context);
            View item = inflater.inflate(R.layout.ventas_recientes_layout, null);

            Venta row = datos.get(position);

            // A partir de la vista, recogeremos los controles que contiene para
            // poder manipularlos.
            // Recogemos los TextView para mostrar datos

            item.setPadding(0, 0, 0, 0);

            //((TextView) item.findViewById(R.id.venta_reciente_monto)).setText(Formatos.FormateaDecimal(row.getTotal(), Formatos.DecimalFormat, "$"));
            ((TextView) item.findViewById(R.id.venta_reciente_monto)).setText(Formatos.FormateaDecimal(row.getTotal(), FORMATO_DECIMAL, "$"));
            ((TextView) item.findViewById(R.id.venta_reciente_monto)).setTextSize(15);
            TextView tv = (TextView) item.findViewById(R.id.venta_reciente_monto);

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) tv.getLayoutParams();
            params.setMargins(0, 0, 0, 0);
            tv.setLayoutParams(params);


            ((TextView) item.findViewById(R.id.venta_reciente_hora)).setVisibility(View.GONE);

            String medioPago = row.getMedio_pago();
            if (medioPago.equals("EFECTIVO"))
                ((ImageView) item.findViewById(R.id.venta_reciente_icon)).setImageResource(R.drawable.cash_icon);
            else if (medioPago.contains("TARJETA"))
                ((ImageView) item.findViewById(R.id.venta_reciente_icon)).setImageResource(R.drawable.card_icon);
            else if (medioPago.equals("CHEQUE"))
                ((ImageView) item.findViewById(R.id.venta_reciente_icon)).setImageResource(R.drawable.bank_check_icon);
            else
                ((ImageView) item.findViewById(R.id.venta_reciente_icon)).setImageResource(R.drawable.information_icon);

            // Devolvemos la vista para que se muestre en el ListView.
            return item;

        }
    }


    //endregion

    class Hora {
        public String hora;
        public String hora_desc;
        public double monto;
        public boolean visible;

        public Hora(String Hora, String Descr, double Monto, boolean Visible) {
            this.hora = Hora;
            this.hora_desc = Descr;
            this.monto = Monto;
            this.visible = Visible;
        }

        public void sumMontoVenta(double Monto) {
            this.monto += Monto;
        }
    }

    public double GetWindowWidth() {
        android.view.Display display = getWindowManager().getDefaultDisplay();
        android.graphics.Point size = new android.graphics.Point();
        display.getSize(size);

        return ((Integer) size.x).doubleValue();
    }


    public double GetWindowHeigth() {
        android.view.Display display = getWindowManager().getDefaultDisplay();
        android.graphics.Point size = new android.graphics.Point();
        display.getSize(size);

        return ((Integer) size.y).doubleValue();
    }
}
