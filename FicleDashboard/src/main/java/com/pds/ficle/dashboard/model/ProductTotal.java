package com.pds.ficle.dashboard.model;

import com.google.gson.annotations.SerializedName;
import com.pds.common.model.Product;

import java.util.Comparator;

/**
 * Created by Hernan on 14/08/2016.
 */
public class ProductTotal {
    @SerializedName("product")
    private String nombre;

    private double stock;

    @SerializedName("quantity")
    private double cantidad;

    private double total;

    @SerializedName("profit")
    private double ganancia;

    private double margin;

    public ProductTotal() {

    }

    public ProductTotal(Product product){
        this.cantidad = 0;
        this.total = 0;
        this.ganancia = 0;

        if(product != null){
            this.nombre = product.getName();
            this.margin = product.getSalePrice() - product.getPurchasePrice();
            this.stock = product.getStock();
        }
        else{
            this.nombre = "GENERICO";
            this.margin = 0;
            this.stock = 0;
        }
    }

    public String getNombre() {
        return nombre;
    }

    public double getStock() {
        return stock;
    }

    public double getCantidad() {
        return cantidad;
    }

    public double getTotal() {
        return total;
    }

    public double getGanancia() {
        return ganancia;
    }

    public ProductTotal addCantidad(double cantidad) {
        this.cantidad += cantidad;

        this.ganancia = this.margin * this.cantidad;

        return this;
    }

    public ProductTotal addTotal(double total) {
        this.total += total;
        return this;
    }

}
