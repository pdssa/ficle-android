package com.pds.ficle.dashboard.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pds.common.Formatos;
import com.pds.common.model.VentaAbstract;
import com.pds.ficle.dashboard.R;
import com.pds.ficle.dashboard.model.SalesItem;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by Hernan on 03/08/2016.
 */
public class SalesAdapter extends ArrayAdapter<SalesItem> {
    private Context context;
    private List<SalesItem> datos;
    private boolean vistaDiaria;
    private DecimalFormat FORMATO_DECIMAL;

    public SalesAdapter(Context context, List<SalesItem> datos, boolean vistaDiaria, DecimalFormat decimalFormat) {
        super(context, android.R.layout.simple_list_item_1, datos);

        this.context = context;
        this.datos = datos;
        this.vistaDiaria = vistaDiaria;
        this.FORMATO_DECIMAL = decimalFormat;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // En primer lugar "inflamos" una nueva vista, que será la que se
        // mostrará en la celda del ListView. Para ello primero creamos el
        // inflater, y después inflamos la vista.
        LayoutInflater inflater = LayoutInflater.from(context);
        View item = inflater.inflate(R.layout.venta_reciente_layout, null);

        SalesItem row = datos.get(position);

        // A partir de la vista, recogeremos los controles que contiene para
        // poder manipularlos.
        // Recogemos los TextView para mostrar datos

        ((TextView) item.findViewById(R.id.venta_reciente_monto)).setText(Formatos.FormateaDecimal(row.getTotal(), FORMATO_DECIMAL));

        ((TextView) item.findViewById(R.id.venta_reciente_hora)).setText(row.getHour());

        ((TextView) item.findViewById(R.id.venta_reciente_ticket)).setText(row.getTicket());

        ((ImageView) item.findViewById(R.id.venta_reciente_icon)).setImageResource(row.getPaymentIcon());

        // Devolvemos la vista para que se muestre en el ListView.
        return item;

    }
}

