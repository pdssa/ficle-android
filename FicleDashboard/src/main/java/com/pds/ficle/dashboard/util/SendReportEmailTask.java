package com.pds.ficle.dashboard.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.text.Html;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.pds.common.Config;
import com.pds.common.dialog.ConfirmDialog;
import com.pds.common.util.ConnectivityUtils;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Hernan on 03/08/2016.
 */
public class SendReportEmailTask extends AsyncTask<String, String, String> {

    private Context context;
    private ProgressDialog dialog;
    private Config config;
    private REPORT_TYPE reportType;
    private boolean RESULT;

    final int CONNECTION_TIMEOUT = 7000;//the timeout until a connection is established
    final int SOCKET_TIMEOUT = 30000; //is the timeout for waiting for data

    public enum REPORT_TYPE {
        DAILY,
        MONTHLY
    }

    public SendReportEmailTask(Context context, REPORT_TYPE reportType) {
        this.context = context;
        this.reportType = reportType;
        this.config = new Config(context);
    }

    public void start() {
        AlertDialog.Builder askEmail = new AlertDialog.Builder(context);
        final EditText input = new EditText(context);
        input.setHint("Email de destino...");
        input.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        input.setText(getRegisteredEmail());
        askEmail.setIcon(android.R.drawable.ic_dialog_email);
        if (reportType.equals(REPORT_TYPE.DAILY))
            askEmail.setTitle("Envio Reporte Diario");
        else
            askEmail.setTitle("Envio Reporte Mensual");
        askEmail.setMessage("Ingrese el Email de destino del reporte:");
        askEmail.setView(input);
        askEmail.setPositiveButton("ACEPTAR", null);
        askEmail.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        final AlertDialog askEmailDialog = askEmail.create();
        askEmailDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        input.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    String email = input.getText().toString().trim();

                    if (TextUtils.isEmpty(email)) {
                        input.setError("Email obligatorio");
                    } else {
                        input.setError(null);

                        execute(email);

                        askEmailDialog.dismiss();

                    }
                    return true;
                }
                return false;
            }
        });

        askEmailDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                input.requestFocus();
            }
        });

        askEmailDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
            }
        });

        askEmailDialog.show();

        askEmailDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = input.getText().toString().trim();

                if (TextUtils.isEmpty(email)) {
                    input.setError("Email obligatorio");
                } else {
                    input.setError(null);

                    execute(input.getText().toString().trim());

                    askEmailDialog.dismiss();
                }
            }
        });

    }

    private String getRegisteredEmail() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("com.pds.ficle.dashboard", Context.MODE_PRIVATE);
        return sharedPreferences.getString("registered_email", "");
    }

    private void saveRegisteredEmail(String email) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("com.pds.ficle.dashboard", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("registered_email", email);
        editor.apply();
    }

    @Override
    protected void onPreExecute() {
        //iniciamos un mensaje para el usuario
        dialog = new ProgressDialog(context);
        if (reportType.equals(REPORT_TYPE.DAILY))
            dialog.setMessage("Enviando reporte diario. Aguarde por favor...");
        else
            dialog.setMessage("Enviando reporte mensual. Aguarde por favor...");
        dialog.setIndeterminate(false);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }


    @Override
    protected String doInBackground(String... params) {
        try {

            //guardamos el ultimo mail utilizado
            saveRegisteredEmail(params[0]);

            if (!ConnectivityUtils.isOnline(context)) {
                return ConnectivityUtils.SIN_CONEXION;
            }

            //enviamos la peticion al servidor
            String response = postDataToServer(params[0], reportType);

            RESULT = true;

            return response;

        } catch (Exception e) {

            RESULT = false;

            return e.getMessage();
        }
    }

    private String postDataToServer(String email, REPORT_TYPE reportType) throws Exception {

        // 1. create HttpClient
        HttpClient httpclient = new DefaultHttpClient();
        HttpParams params = httpclient.getParams();
        HttpConnectionParams.setConnectionTimeout(params, CONNECTION_TIMEOUT);
        HttpConnectionParams.setSoTimeout(params, SOCKET_TIMEOUT);

        // 2. make POST request to the given URL
        String servidor =  this.config.CLOUD_SERVER_HOST.trim() + "api/DashboardSendReport";


        // 4. convert JSONObject to JSON to String
        /*JSONObject jsonObject = new JSONObject();

        jsonObject.put("deviceId", this.config.ANDROID_ID);
        jsonObject.put("sendTo", email);
        jsonObject.put("reportType", reportType.ordinal());

        String json = "{j:'" + jsonObject.toString().replace("'", "") + "'}";*/

        String query = "?deviceId=" + this.config.ANDROID_ID + "&sendTo=" + email + "&reportType=" + reportType.ordinal();
        HttpPost httpPost = new HttpPost(servidor + query);

        // 5. set json to StringEntity
        StringEntity se = new StringEntity(query, "UTF-8");

        // 6. set httpPost Entity
        httpPost.setEntity(se);

        // 7. Set some headers to inform server about the type of the content
        //httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/x-www-form-urlencoded");

        // 8. Execute POST request to the given URL
        HttpResponse httpResponse = httpclient.execute(httpPost);

        // 9. receive response as inputStream
        InputStream inputStream = null;
        inputStream = httpResponse.getEntity().getContent();

        // 10. convert inputstream to string
        String result = "";
        if (inputStream != null) {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            result = "";
            while ((line = bufferedReader.readLine()) != null)
                result += line;

            inputStream.close();
        } else
            result = "No ha sido posible el envio al servidor";

        return result;

    }

    @Override
    protected void onProgressUpdate(String... values) {
        dialog.setMessage(values[0]);
    }

    @Override
    protected void onPostExecute(String result) {
        if (dialog.isShowing())
            dialog.dismiss();

        if (!RESULT) {
            AlertMessage(result);//mostramos mensaje de error
        } else {
            ConfirmMessage();
        }
    }

    @Override
    protected void onCancelled(String s) {
        AlertMessage(s);
    }

    private void AlertMessage(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        if (reportType.equals(REPORT_TYPE.DAILY))
            builder.setTitle("Envio Reporte Diario");
        else
            builder.setTitle("Envio Reporte Mensual");

        builder.setMessage(Html.fromHtml("Se ha producido un error al intentar realizar el envio:" + "<br/><b>" + message + "</b>"));
        builder.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface _dialog, int which) {
                _dialog.dismiss();
            }
        });

        builder.setCancelable(false);
        builder.show();
    }

    private void ConfirmMessage() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(android.R.drawable.ic_dialog_email);
        if (reportType.equals(REPORT_TYPE.DAILY))
            builder.setTitle("Envio Reporte Diario");
        else
            builder.setTitle("Envio Reporte Mensual");

        builder.setMessage(Html.fromHtml("Envio realizado correctamente"));
        builder.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface _dialog, int which) {
                _dialog.dismiss();
            }
        });

        builder.setCancelable(false);
        builder.show();
    }

}
