package com.pds.ficle.dashboard.util;

public interface ResultListener<T> {
    void finish(T result);
    void error(String message);
}