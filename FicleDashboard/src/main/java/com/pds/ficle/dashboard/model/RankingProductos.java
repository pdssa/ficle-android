package com.pds.ficle.dashboard.model;

import com.pds.common.model.Product;

/**
 * Created by Hernan on 09/08/2016.
 */
public class RankingProductos{
    private int idProducto;
    private String sku;
    private String nombre;
    private double precioVenta;
    private double precioCompra;
    private double stock;
    private double cantidad;
    private double total;


    public RankingProductos addCantidad(double cantidad) {
        this.cantidad += cantidad;
        return this;
    }

    public RankingProductos addTotal(double total) {
        this.total += total;
        return this;
    }

    public double getTotal() {
        return total;
    }

    public String getSku() {
        return sku;
    }

    public String getNombre() {
        return nombre;
    }

    public double getStock() {
        return stock;
    }

    public double getCantidad() {
        return cantidad;
    }

    public double getPrecioCompra() {
        return precioCompra;
    }

    public double getPrecioVenta() {
        return precioVenta;
    }

    public double getGanancia(){
        return (this.precioVenta - this.precioCompra) * this.cantidad;
    }

    public RankingProductos(int idProducto, String sku, Product product) {
        this.idProducto = idProducto;
        this.sku = sku;
        this.cantidad = 0;
        this.total = 0;

        if(product != null){
            this.nombre = product.getName();
            this.precioVenta = product.getSalePrice();
            this.stock = product.getStock();
            this.precioCompra = product.getPurchasePrice();
        }
        else{
            this.nombre = "GENERICO";
            this.precioVenta = 0;
            this.stock = 0;
            this.precioCompra = 0;
        }

    }
}
