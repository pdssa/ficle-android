package com.pds.ficle.dashboard.model;

import com.google.gson.annotations.SerializedName;
import com.pds.common.Formato;

import java.util.Date;
import java.util.List;

/**
 * Created by Hernan on 14/08/2016.
 */
public class DailyTotal implements Comparable<DailyTotal>{
    private String label;
    private double total;
    private String id;

    @SerializedName("products")
    private List<ProductTotal> productsTotals;

    @SerializedName("sales")
    private List<SalesItem> salesList;

    public DailyTotal(){

    }

    public DailyTotal(String id, String label){
        this.id = id;
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public double getTotal() {
        return total;
    }

    public String getId() {
        return id;
    }

    public List<ProductTotal> getProductsTotals() {
        return productsTotals;
    }

    public List<SalesItem> getSalesList() {
        return salesList;
    }

    public void addTotal(double total) {
        this.total += total;
    }

    public Date getDate(){
        try {
            return Formato.ObtieneDate(getId(), Formato.DbDateFormat);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new Date();
    }

    @Override
    public int compareTo(DailyTotal o) {
        return this.id.compareTo(o.id);
    }
}
