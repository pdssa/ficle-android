package com.pds.ficle.dashboard.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pds.common.Formatos;
import com.pds.ficle.dashboard.R;
import com.pds.ficle.dashboard.model.ProductTotal;
import com.pds.ficle.dashboard.model.RankingProductos;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by Hernan on 03/08/2016.
 */
public class RankingProductosAdapter extends ArrayAdapter<ProductTotal> {
    private Context context;
    private List<ProductTotal> datos;
    private DecimalFormat FORMATO_DECIMAL;

    public RankingProductosAdapter(Context context, List<ProductTotal> datos, DecimalFormat decimalFormat) {
        super(context, android.R.layout.simple_list_item_1, datos);

        this.context = context;
        this.datos = datos;
        this.FORMATO_DECIMAL = decimalFormat;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // En primer lugar "inflamos" una nueva vista, que será la que se
        // mostrará en la celda del ListView. Para ello primero creamos el
        // inflater, y después inflamos la vista.
        LayoutInflater inflater = LayoutInflater.from(context);
        View item = inflater.inflate(R.layout.ranking_productos_layout, null);

        ProductTotal row = datos.get(position);

        ((TextView) item.findViewById(R.id.ranking_productos_nombre)).setText(row.getNombre());

        ((TextView) item.findViewById(R.id.ranking_productos_cantidad)).setText(String.valueOf(row.getCantidad()));

        ((TextView) item.findViewById(R.id.ranking_productos_ganancia)).setText(Formatos.FormateaDecimal(row.getGanancia(), FORMATO_DECIMAL));

        ((TextView) item.findViewById(R.id.ranking_productos_total)).setText(Formatos.FormateaDecimal(row.getTotal(), FORMATO_DECIMAL));

        ((TextView) item.findViewById(R.id.ranking_productos_stock)).setText(String.valueOf(row.getStock()));

        // Devolvemos la vista para que se muestre en el ListView.
        return item;

    }
}

