package com.pds.ficle.dashboard.view;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Formatos;
import com.pds.common.activity.TimerActivity;
import com.pds.common.dao.CierreDao;
import com.pds.common.model.Cierre;
import com.pds.ficle.dashboard.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android_serialport_api.Printer;


public class RptDetalleVentas extends TimerActivity {

    private EditText txtFechaDesde;
    private EditText txtFechaHasta;
    private ImageView imgFind;
    private Button btnPrint;
    private Button btnBack;
    private ListView lstDetIzq;
    private ListView lstDetDer;
    private List<Row> detalles_izq;
    private List<Row> detalles_der;
    private Printer _printer;
    private TextView txtMensaje;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_rpt_detalle_ventas);

            AsignarViews();

            AsignarEventos();

            //cerramos el teclado
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(txtFechaDesde.getWindowToken(), 0);

            //por default ejecutamos el reporte para el ultimo cierre
            Date ult_cierre = GetLastCierre();

            if (ult_cierre == null) {
                lstDetIzq.setVisibility(View.GONE);
                lstDetDer.setVisibility(View.GONE);
                txtMensaje.setText("No hay cierres registrados");
                txtMensaje.setVisibility(View.VISIBLE);
                btnPrint.setVisibility(View.INVISIBLE);
            } else {

                btnPrint.setVisibility(View.VISIBLE);

                txtFechaDesde.setText(Formatos.FormateaDate(ult_cierre, Formatos.DateFormatSlash));
                txtFechaHasta.setText(Formatos.FormateaDate(ult_cierre, Formatos.DateFormatSlash));

                EjecutarReporte(ult_cierre, ult_cierre);
            }
        } catch (Exception ex) {
            Toast.makeText(RptDetalleVentas.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }


    private void AsignarViews() {
        imgFind = (ImageView) findViewById(R.id.rpt_detventas_btn_find);
        btnPrint = (Button) findViewById(R.id.rpt_detventas_btn_imprimir);
        btnBack = (Button) findViewById(R.id.rpt_detventas_btn_back);
        txtFechaDesde = (EditText) findViewById(R.id.rpt_detventas_txt_fecdesde);
        txtFechaHasta = (EditText) findViewById(R.id.rpt_detventas_txt_fechasta);
        lstDetIzq = (ListView) findViewById(R.id.rpt_detventas_lst_izq);
        lstDetDer = (ListView) findViewById(R.id.rpt_detventas_lst_der);
        txtMensaje = (TextView) findViewById(R.id.rpt_detventas_txt_mensaje);
    }

    private void AsignarEventos() {
        txtFechaDesde.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PickDate(txtFechaDesde);
            }
        });
        txtFechaDesde.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) PickDate(txtFechaDesde);
            }
        });
        txtFechaHasta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PickDate(txtFechaHasta);
            }
        });
        txtFechaHasta.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) PickDate(txtFechaHasta);
            }
        });
        imgFind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EjecutarReporte();
            }
        });
        btnPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImprimirReporte();
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private Date GetLastCierre() {
        List<Cierre> cierres = new CierreDao(getContentResolver()).list(null, null, "fecha DESC", null, null, "1");
        Date ultimo_cierre = null;
        if (cierres.size() > 0)
            ultimo_cierre = cierres.get(0).getFecha();

        return ultimo_cierre;
    }

    private void EjecutarReporte() {
        String _fecDesde = txtFechaDesde.getText().toString();
        String _fecHasta = txtFechaHasta.getText().toString();

        Date fecDesde = null;
        Date fecHasta = null;

        if (!TextUtils.isEmpty(_fecDesde))
            try {
                fecDesde = Formatos.ObtieneDate(_fecDesde, Formatos.DateFormatSlash);
            } catch (Exception e) {
                e.printStackTrace();
            }

        if (!TextUtils.isEmpty(_fecHasta))
            try {
                fecHasta = Formatos.ObtieneDate(_fecHasta, Formatos.DateFormatSlash);
            } catch (Exception e) {
                e.printStackTrace();
            }

        EjecutarReporte(fecDesde, fecHasta);
    }

    private void EjecutarReporte(Date fecDesde, Date fecHasta) {

        if (fecDesde == null || fecHasta == null) {
            Toast.makeText(RptDetalleVentas.this, "Error al obtener las fechas de consulta ingresadas", Toast.LENGTH_SHORT).show();
            return;
        }

        /*if (fecDesde != null && fecHasta != null) {
            if (!Formatos.FormateaDate(fecDesde, Formatos.MonthYearFormat).equals(Formatos.FormateaDate(fecHasta, Formatos.MonthYearFormat))) {
                Toast.makeText(RptDetalleVentas.this, "Error, por favor para consultar asegúrese que ambas fechas pertenezcan al mismo mes", Toast.LENGTH_SHORT).show();
                return;
            }
        }*/

        String filter = "", filterDesde = "", filterHasta = "";

        //if (fecDesde != null)
            filterDesde = "julianday(fecha) >= julianday('" + Formatos.FormateaDate(fecDesde, Formatos.DbDateTimeFormat) + "')";

        //if (fecHasta != null)
            filterHasta = "julianday(fecha) <= julianday('" + Formatos.FormateaDate(fecHasta, Formatos.DbDateTimeFormat).replace("00:00:00", "23:59:59") + "')";

        //filter = filterDesde + (fecDesde != null ? " AND " : "") + filterHasta;

        filter = filterDesde + " AND " + filterHasta;

        List<Cierre> cierres = new CierreDao(getContentResolver()).list(filter, null, "_id");

        if (cierres.size() > 0) {

            //int afectas_q = 0, exentas_q = 0, menores_q = 0, otros_q = 0;
            int cant_operaciones = 0;
            double afectas_t = 0, exentas_t = 0, menores_t = 0, otros_t = 0, iva_acumulado = 0;

            //acumulamos por el período consultado
            for (Cierre cierre : cierres) {
                cant_operaciones += cierre.getOperaciones_cant();
                //afectas_q += cierre.getAfectas_cant();
                afectas_t += cierre.getAfectas_total();
                //exentas_q += cierre.getExentas_cant();
                exentas_t += cierre.getExentas_total();
                //menores_q += cierre.getMenores_cant();
                menores_t += cierre.getMenores_total();
                //otros_q += cierre.getOtros_cant();
                otros_t += cierre.getOtros_total();
                //iva += cierre.getIva();
                iva_acumulado += cierre.getIva();
            }

            //el IVA se cuenta desde 1 del MES a la fecha hasta (corte)
            /*List<Cierre> cierres_mes = new CierreDao(getContentResolver()).list(filterHasta + " AND substr(fecha,1,7) = '" + Formatos.FormateaDate(fecHasta, Formatos.YearMonthFormat) + "'", null, null);

            double iva_acumulado = 0;
            for (Cierre cierre : cierres_mes) {
                iva_acumulado += cierre.getIva();
            }*/

            detalles_izq = new ArrayList<Row>();
            detalles_der = new ArrayList<Row>();

            /*if (fecHasta == null || fecDesde.equals(fecHasta)) {
                detalles_izq.add(new Row("Fecha", Formatos.FormateaDate(fecDesde, Formatos.DateFormatSlash)));
                detalles_der.add(new Row("", ""));
            } else {
                detalles_izq.add(new Row("Fecha Desde", Formatos.FormateaDate(fecDesde, Formatos.DateFormatSlash)));
                detalles_der.add(new Row("Fecha Hasta", Formatos.FormateaDate(fecHasta, Formatos.DateFormatSlash)));
            }*/

            //inicio del primer cierre, fin del ultimo cierre
            detalles_izq.add(new Row("Inicio", Formatos.FormateaDate(cierres.get(0).getFechaDesde(), Formatos.DateFormatSlashAndTime)));
            detalles_der.add(new Row("Cierre", Formatos.FormateaDate(cierres.get(cierres.size() - 1).getFecha(), Formatos.DateFormatSlashAndTime)));

            detalles_izq.add(new Row("Cdad. Operaciones", String.format("%d", cant_operaciones)));
            detalles_der.add(new Row("Total Vtas. Menores", Formatos.FormateaDecimal(menores_t, Formatos.DecimalFormat_CH, "$")));
            detalles_izq.add(new Row("Total Vtas. Afectas", Formatos.FormateaDecimal(afectas_t, Formatos.DecimalFormat_CH, "$")));
            detalles_der.add(new Row("Total Vtas. Exentas", Formatos.FormateaDecimal(exentas_t, Formatos.DecimalFormat_CH, "$")));
            detalles_izq.add(new Row("Total Otros", Formatos.FormateaDecimal(otros_t, Formatos.DecimalFormat_CH, "$")));
            //detalles_izq.add(new Row("Total IVA", Formatos.FormateaDecimal(iva, Formatos.DecimalFormat_CH, "$")));
            detalles_der.add(new Row("IVA Acumulado", Formatos.FormateaDecimal(iva_acumulado, Formatos.DecimalFormat_CH, "$")));

            lstDetIzq.setAdapter(new RowAdapter(RptDetalleVentas.this, detalles_izq));
            lstDetDer.setAdapter(new RowAdapter(RptDetalleVentas.this, detalles_der));

            lstDetIzq.setVisibility(View.VISIBLE);
            lstDetDer.setVisibility(View.VISIBLE);
            txtMensaje.setVisibility(View.GONE);

        } else {
            lstDetIzq.setVisibility(View.GONE);
            lstDetDer.setVisibility(View.GONE);
            txtMensaje.setText("No hay cierres registrados en las fechas ingresadas");
            txtMensaje.setVisibility(View.VISIBLE);
        }
    }

    private void ImprimirReporte() {
        if (detalles_izq != null && detalles_der != null) {
            if (detalles_izq.size() > 0 && detalles_der.size() > 0) {
                //hay informacion para imprimir

                Config config = new Config(RptDetalleVentas.this);


                try {
                    if (_printer == null)
                        _printer = Printer.getInstance(config.PRINTER_MODEL, RptDetalleVentas.this);

                    _printer.printerCallback = new Printer.PrinterCallback() {
                        @Override
                        public void onPrinterReady() {
                            //titulo
                            _printer.format(1, 1, Printer.ALINEACION.CENTER);
                            _printer.printLine("DETALLE DE VENTAS");

                            _printer.lineFeed();

                            //fecha o fechas
                            _printer.cancelCurrentFormat();

                            if (TextUtils.isEmpty(detalles_der.get(0).concepto)) {
                                _printer.printLine("Fecha : " + detalles_izq.get(0).valor);
                            } else {
                                _printer.printLine("Inicio : " + detalles_izq.get(0).valor);
                                _printer.printLine("Cierre : " + detalles_der.get(0).valor);
                            }

                            _printer.lineFeed();

                            //vamos de a uno de los detalles
                            for (int i = 1; i < detalles_izq.size(); i++) { //hay la misma cantidad de items en ambas listas
                                _printer.printLine(detalles_izq.get(i).concepto + " : " + detalles_izq.get(i).valor);
                                _printer.printLine(detalles_der.get(i).concepto + " : " + detalles_der.get(i).valor);
                            }

                            _printer.sendSeparadorHorizontal();

                            _printer.footer();
                        }
                    };

                    //iniciamos la printer
                    if (!_printer.initialize()) {
                        Toast.makeText(RptDetalleVentas.this, "Error al inicializar", Toast.LENGTH_SHORT).show();
                        _printer.end();
                    }

                } catch (Exception ex) {
                    Toast.makeText(this, "Error al imprimir detalle: " + ex.getMessage(), Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(RptDetalleVentas.this, "No hay información disponible para imprimir", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(RptDetalleVentas.this, "No hay información disponible para imprimir", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        if (_printer != null)
            _printer.end();

        super.onDestroy();
    }

    public void PickDate(final EditText editText) {
        Calendar hoy = Calendar.getInstance();
        DatePickerDialog datePicker = new DatePickerDialog(RptDetalleVentas.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                Calendar fecha = Calendar.getInstance();
                fecha.set(year, monthOfYear, dayOfMonth);

                editText.setText(Formatos.FormateaDate(fecha.getTime(), Formatos.DateFormatSlash));
            }
        }, hoy.get(Calendar.YEAR), hoy.get(Calendar.MONTH), hoy.get(Calendar.DAY_OF_MONTH));
        datePicker.show();
    }

    class Row {
        public String concepto;
        public String valor;

        public Row(String _concepto, String _valor) {
            this.concepto = _concepto;
            this.valor = _valor;
        }
    }

    class RowAdapter extends ArrayAdapter<Row> {
        private Context context;
        private List<Row> datos;

        public RowAdapter(Context context, List<Row> datos) {
            super(context, android.R.layout.simple_list_item_1, datos);

            this.context = context;
            this.datos = datos;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // En primer lugar "inflamos" una nueva vista, que será la que se
            // mostrará en la celda del ListView. Para ello primero creamos el
            // inflater, y después inflamos la vista.
            LayoutInflater inflater = LayoutInflater.from(context);
            View item = inflater.inflate(R.layout.rpt_detventas_row, null);

            Row row = datos.get(position);

            // A partir de la vista, recogeremos los controles que contiene para
            // poder manipularlos.
            // Recogemos los TextView para mostrar datos

            ((TextView) item.findViewById(R.id.rpt_detventas_concepto)).setText(row.concepto);
            ((TextView) item.findViewById(R.id.rpt_detventas_valor)).setText(row.valor);

            // Devolvemos la vista para que se muestre en el ListView.
            return item;

        }
    }

    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.rpt_detalle_ventas, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    */
}
