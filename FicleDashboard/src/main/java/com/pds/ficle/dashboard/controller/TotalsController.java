package com.pds.ficle.dashboard.controller;

import android.content.Context;

import com.pds.common.util.ConnectivityUtils;
import com.pds.ficle.dashboard.dao.MonthTotalsDAO;
import com.pds.ficle.dashboard.model.DailyTotal;
import com.pds.ficle.dashboard.model.MonthTotal;
import com.pds.ficle.dashboard.util.ResultListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Hernan on 15/08/2016.
 */
public class TotalsController {
    public void getMonthsTotals(Context context, final MonthTotal currentMonthTotal, final ResultListener<List<MonthTotal>> listener) {
        if (!ConnectivityUtils.isOnline(context)) {
            listener.error(ConnectivityUtils.SIN_CONEXION);
        }
        else {
            new MonthTotalsDAO().getMonthsTotals(context, new ResultListener<List<MonthTotal>>() {
                @Override
                public void finish(List<MonthTotal> result) {
                    //asignamos los totales del mes en curso
                    for (MonthTotal month : result) {
                        if (month.equals(currentMonthTotal)) {
                            month.setTotal(currentMonthTotal.getTotal());
                            break;
                        }
                    }

                    if (isLastYearEmpty(result)) {
                        List<MonthTotal> monthTotalList = new ArrayList<MonthTotal>();
                        String lastYear = getLastYearString();

                        //limpiamos los totales del año anterior, dado que están todos en cero
                        for (MonthTotal month : result) {
                            if (!month.getId().substring(0, 4).equals(lastYear)) {
                                monthTotalList.add(month);
                            }
                        }

                        listener.finish(monthTotalList);
                    } else {
                        listener.finish(result);
                    }
                }

                @Override
                public void error(String message) {
                    listener.error(message);
                }
            });
        }
    }

    private boolean isLastYearEmpty(List<MonthTotal> monthTotalList){
        int isLastYearEmpty = 0;

        String lastYear = getLastYearString();

        for (MonthTotal month : monthTotalList) {

            //si es un mes del año pasado en cero...
            if(month.getId().substring(0,4).equals(lastYear) && month.getTotal() == 0){
                isLastYearEmpty ++;
            }

        }

        return isLastYearEmpty == 12;
    }

    private String getLastYearString(){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, -1);

        return String.valueOf(calendar.get(Calendar.YEAR));
    }

    public void getMonthTotal(Context context, String monthId, ResultListener<MonthTotal> listener){
        if (!ConnectivityUtils.isOnline(context)) {
            listener.error(ConnectivityUtils.SIN_CONEXION);
        }
        else {
            new MonthTotalsDAO().getMonthTotal(context, monthId, listener);
        }
    }

    public void getDailyTotal(Context context, String dateId, ResultListener<DailyTotal> listener){
        if (!ConnectivityUtils.isOnline(context)) {
            listener.error(ConnectivityUtils.SIN_CONEXION);
        }
        else {
            new MonthTotalsDAO().getDailyTotal(context, dateId, listener);
        }
    }
}
