package com.pds.ficle.dashboard.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CalendarView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Formato;
import com.pds.common.Formatos;
import com.pds.common.model.VentaAbstract;
import com.pds.common.model.VentaDetalleAbstract;
import com.pds.ficle.dashboard.R;
import com.pds.ficle.dashboard.adapter.DailyTotalsAdapter;
import com.pds.ficle.dashboard.adapter.RankingProductosAdapter;
import com.pds.ficle.dashboard.comp.ComparatorCantidad;
import com.pds.ficle.dashboard.comp.ComparatorGanancia;
import com.pds.ficle.dashboard.comp.ComparatorNombre;
import com.pds.ficle.dashboard.comp.ComparatorStock;
import com.pds.ficle.dashboard.comp.ComparatorTotal;
import com.pds.ficle.dashboard.controller.TotalsController;
import com.pds.ficle.dashboard.dao.ReportDAO;
import com.pds.ficle.dashboard.model.DailyTotal;
import com.pds.ficle.dashboard.model.MonthTotal;
import com.pds.ficle.dashboard.model.ProductTotal;
import com.pds.ficle.dashboard.util.ResultListener;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class VistaMensualActivity extends Activity {

    public static final String ARGUMENT_MONTH = "__MONTH";
    private DecimalFormat FORMATO_DECIMAL;
    private ProgressBar progressBarRanking, progressBarDaily;

    private List<ProductTotal> rankingProductosList;
    private RankingProductosAdapter rankingProductosAdapter;

    private List<DailyTotal> dailyTotalList;
    private DailyTotalsAdapter dailyTotalsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vista_mensual);

        if (getIntent().hasExtra(ARGUMENT_MONTH)) {
            MonthTotal month = getIntent().getExtras().getParcelable(ARGUMENT_MONTH);

            Config config = new Config(this);

            //Configuracion de formatos segun PAIS
            Init_CustomConfigPais(config.PAIS);

            loadMonthInfo(month);
        }
    }

    private void Init_CustomConfigPais(String codePais) {
        FORMATO_DECIMAL = codePais.equals("AR") ? Formatos.DecimalFormat : Formatos.DecimalFormat_CH;
    }

    private void loadMonthInfo(MonthTotal month) {

        progressBarRanking = (ProgressBar) findViewById(R.id.dash_mensual_progress);
        progressBarDaily = (ProgressBar) findViewById(R.id.dash_mensual_progress_daily);
        progressBarRanking.setVisibility(View.VISIBLE);
        progressBarDaily.setVisibility(View.VISIBLE);

        String currentMonth = Formatos.FormateaDate(new Date(), Formatos.DbDateFormat).substring(0, 6);

        if (month.getId().equals(currentMonth)) {
            loadCurrentMonth(month);
        } else {
            loadOldMonth(month);
        }


        ((ListView) findViewById(R.id.dash_mensual_lst_daily)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Date selectedDay = ((DailyTotal) parent.getItemAtPosition(position)).getDate();

                openDailyView(selectedDay);
            }
        });
    }

    private void loadOldMonth(MonthTotal month) {
        try {

            String headerString = "Total del mes " + month.getLabel();

            headerString += (" - " + Formatos.FormateaDecimal(month.getTotal(), FORMATO_DECIMAL) + " $");

            ((TextView) findViewById(R.id.dash_mensual_txt_header)).setText(headerString);


            new TotalsController().getMonthTotal(this, month.getId(), new ResultListener<MonthTotal>() {
                @Override
                public void finish(MonthTotal result) {
                    progressBarRanking.setVisibility(View.GONE);
                    loadProductsList(result.getProductsTotals());

                    progressBarDaily.setVisibility(View.GONE);
                    loadDaysTotalsList(result.getDailyTotals());
                }

                @Override
                public void error(String message) {
                    progressBarRanking.setVisibility(View.GONE);
                    progressBarDaily.setVisibility(View.GONE);

                    Toast.makeText(VistaMensualActivity.this, message, Toast.LENGTH_SHORT).show();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadCurrentMonth(MonthTotal month) {
        try {

            String headerString = "Total del mes " + month.getLabel();

            headerString += (" - " + Formatos.FormateaDecimal(month.getTotal(), FORMATO_DECIMAL) + " $");

            ((TextView) findViewById(R.id.dash_mensual_txt_header)).setText(headerString);

            Date date = Formato.ObtieneDate(month.getId() + "01", Formato.DbDateFormat);

            ReportDAO reportDAO = new ReportDAO();

            //**obtenemos informacion del mes**
            List<VentaAbstract> ventasList = reportDAO.getVentasMonth(getContentResolver(), date, true);

            progressBarRanking.setVisibility(View.GONE);
            loadProductsList(getProductsList(ventasList));

            progressBarDaily.setVisibility(View.GONE);
            loadDaysTotalsList(getDaysTotals(ventasList));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Collection<DailyTotal> getDaysTotals(List<VentaAbstract> ventasList) {
        HashMap<String, DailyTotal> dailyTotalHashMap = new HashMap<String, DailyTotal>();

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DATE, 1);

        int currentMonth = calendar.get(Calendar.MONTH);

        while (currentMonth == calendar.get(Calendar.MONTH)) {

            String fecId = Formato.FormateaDate(calendar.getTime(), Formato.DbDateFormat);
            String fecLabel = Formato.FormateaDate(calendar.getTime(), Formato.ShortDateFormatNameOfDay).toUpperCase();

            dailyTotalHashMap.put(fecId, new DailyTotal(fecId, fecLabel));

            calendar.add(Calendar.DATE, 1);
        }

        //totalizamos por dia
        for (VentaAbstract vta : ventasList) {
            String fecId = Formato.FormateaDate(vta.getFechaHora_Date(), Formato.DbDateFormat);

            if (dailyTotalHashMap.containsKey(fecId))
                dailyTotalHashMap.get(fecId).addTotal(vta.getTotal());
        }

        return dailyTotalHashMap.values();
    }

    private Collection<ProductTotal> getProductsList(List<VentaAbstract> ventasList) {
        HashMap<Integer, ProductTotal> rankingProductos = new HashMap<Integer, ProductTotal>();

        //buscamos los detalles de las ventas
        for (VentaAbstract vta : ventasList) {
            vta.addDetalles(getContentResolver());

            for (VentaDetalleAbstract det : vta.getDetallesAbstract()) {

                Long idProducto = det.getProducto() != null ? det.getProducto().getId() : -1;

                if (!rankingProductos.containsKey(idProducto.intValue()))
                    rankingProductos.put(idProducto.intValue(),
                            new ProductTotal(
                                    det.getProducto()
                            ));

                rankingProductos.get(idProducto.intValue())
                        .addCantidad(det.getCantidad())
                        .addTotal(det.getTotal());

            }
        }

        return rankingProductos.values();
    }

    private void loadProductsList(Collection<ProductTotal> productTotalList) {

        if (rankingProductosList == null) {
            rankingProductosList = new ArrayList<ProductTotal>();
            rankingProductosList.addAll(productTotalList);
            rankingProductosAdapter = new RankingProductosAdapter(this, rankingProductosList, FORMATO_DECIMAL);
            ((ListView) findViewById(R.id.dash_mensual_lst_ranking)).setEmptyView(findViewById(R.id.dash_mensual_txt_ranking_empty));
            ((ListView) findViewById(R.id.dash_mensual_lst_ranking)).setAdapter(rankingProductosAdapter);
        } else {
            rankingProductosList.clear();
            rankingProductosList.addAll(productTotalList);
            rankingProductosAdapter.notifyDataSetChanged();
        }
    }

    private void loadDaysTotalsList(Collection<DailyTotal> _dailyTotalsList) {

        if (dailyTotalList == null) {
            dailyTotalList = new ArrayList<DailyTotal>();
            dailyTotalList.addAll(_dailyTotalsList);
            Collections.sort(dailyTotalList);
            dailyTotalsAdapter = new DailyTotalsAdapter(this, dailyTotalList, FORMATO_DECIMAL);

            ((ListView) findViewById(R.id.dash_mensual_lst_daily)).setEmptyView(findViewById(R.id.dash_mensual_txt_daily_empty));
            ((ListView) findViewById(R.id.dash_mensual_lst_daily)).setAdapter(dailyTotalsAdapter);
        } else {
            dailyTotalList.clear();
            dailyTotalList.addAll(_dailyTotalsList);
            Collections.sort(dailyTotalList);
            dailyTotalsAdapter.notifyDataSetChanged();
        }
    }

    private void openDailyView(Date date) {
        Intent intent = new Intent(this, VistaDiariaActivity.class);
        intent.putExtra(VistaDiariaActivity.ARGUMENT_DATE, date);
        startActivity(intent);
    }

    public void backClick(View view) {
        finish();
    }

    public void sortRankingList(View view){

        Comparator<ProductTotal> comparator = null;

        switch (view.getId()){
            case R.id.dash_mensual_title_ranking_producto:{
                comparator = new ComparatorNombre();
            }break;
            case R.id.dash_mensual_title_ranking_ganancia:{
                comparator = new ComparatorGanancia();
            }break;
            case R.id.dash_mensual_title_ranking_cantidad:{
                comparator = new ComparatorCantidad();
            }break;
            case R.id.dash_mensual_title_ranking_stock:{
                comparator = new ComparatorStock();
            }break;
            case R.id.dash_mensual_title_ranking_total:{
                comparator = new ComparatorTotal();
            }break;
        }

        //si ya esta seleccionado invertimos el orden
        boolean reverse = false;

        ColorDrawable background = (ColorDrawable)view.getBackground();

        if(background != null && background.getColor() == getResources().getColor(R.color.amarillo))
            reverse = true;

        //clear
        clearSorts();

        //select sorted
        view.setBackgroundColor(getResources().getColor(R.color.amarillo));

        if(comparator != null) {
            if(reverse)
                comparator = Collections.reverseOrder(comparator);
            Collections.sort(rankingProductosList, comparator);
            rankingProductosAdapter.notifyDataSetChanged();
        }
    }

    private void clearSorts(){
        findViewById(R.id.dash_mensual_title_ranking_producto).setBackgroundColor(getResources().getColor(R.color.transparent));
        findViewById(R.id.dash_mensual_title_ranking_ganancia).setBackgroundColor(getResources().getColor(R.color.transparent));
        findViewById(R.id.dash_mensual_title_ranking_cantidad).setBackgroundColor(getResources().getColor(R.color.transparent));
        findViewById(R.id.dash_mensual_title_ranking_stock).setBackgroundColor(getResources().getColor(R.color.transparent));
        findViewById(R.id.dash_mensual_title_ranking_total).setBackgroundColor(getResources().getColor(R.color.transparent));
    }

    /*private void setUpCalendar(MonthTotal month){
        try {
            Date date = Formato.ObtieneDate(month.getId() + "01", Formato.DbDateFormat);

            //seteamos el mes al calendar
            Calendar firstDay = Calendar.getInstance();
            firstDay.setTimeInMillis(date.getTime());
            firstDay.set(Calendar.DAY_OF_MONTH, 1);

            Calendar secondDay = Calendar.getInstance();
            secondDay.setTimeInMillis(date.getTime());
            secondDay.add(Calendar.MONTH, 1);
            secondDay.set(Calendar.DAY_OF_MONTH, 1);
            secondDay.add(Calendar.DATE, -1);

            CalendarView calendarView = (CalendarView) findViewById(R.id.dash_mensual_calendar);
            calendarView.setMinDate(firstDay.getTimeInMillis());
            calendarView.setMaxDate(secondDay.getTimeInMillis());
            calendarView.setDate(date.getTime());
            calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                @Override
                public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(year, month, dayOfMonth);

                    openDailyView(calendar.getTime());
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }*/

}
