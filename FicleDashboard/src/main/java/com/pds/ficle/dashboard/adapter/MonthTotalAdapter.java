package com.pds.ficle.dashboard.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pds.common.Formatos;
import com.pds.ficle.dashboard.R;
import com.pds.ficle.dashboard.model.MonthTotal;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Hernan on 12/08/2016.
 */
public class MonthTotalAdapter extends RecyclerView.Adapter {

    private List<MonthTotal> monthTotalList;
    private Context context;
    private DecimalFormat FORMATO_DECIMAL;
    private MonthTotalListener monthTotalListener;
    private String currentYear;

    public MonthTotalAdapter(Context context, List<MonthTotal> monthTotalList, DecimalFormat decimalFormat, MonthTotalListener monthTotalListener){
        this.context = context;
        this.monthTotalList = monthTotalList;
        this.FORMATO_DECIMAL = decimalFormat;
        this.monthTotalListener = monthTotalListener;
        this.currentYear = Formatos.FormateaDate(new Date(), Formatos.DbDateFormat).substring(0, 4);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.month_total_item, parent, false);

        int height = parent.getMeasuredHeight() / 4;
        view.setMinimumHeight(height);

        return new MonthTotalViewHolder(view, monthTotalListener, currentYear);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        MonthTotalViewHolder monthTotalViewHolder = (MonthTotalViewHolder)holder;
        monthTotalViewHolder.bindMonthTotal(monthTotalList.get(position));
    }

    @Override
    public int getItemCount() {
        return monthTotalList.size();
    }

    public interface MonthTotalListener{
        void onMonthTotalSelected(MonthTotal monthTotal);
    }

    private class MonthTotalViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView txtMonthLabel;
        private TextView txtMonthTotal;
        private MonthTotalListener listener;
        private MonthTotal monthTotal;
        private String currentYear;

        public MonthTotalViewHolder(View itemView, MonthTotalListener monthTotalListener, String currentYear) {
            super(itemView);

            listener = monthTotalListener;

            itemView.setOnClickListener(this);

            txtMonthLabel = (TextView)itemView.findViewById(R.id.dash_main_old_month_label);
            txtMonthTotal = (TextView)itemView.findViewById(R.id.dash_main_old_month_total);

            this.currentYear = currentYear;
        }

        public void bindMonthTotal(MonthTotal monthTotal){
            this.monthTotal = monthTotal;
            txtMonthLabel.setText(monthTotal.getLabel());
            txtMonthTotal.setText(Formatos.FormateaDecimal(monthTotal.getTotal(), FORMATO_DECIMAL) + " $");

            if(monthTotal.getId().startsWith(currentYear)) {
                //itemView.setBackground(context.getResources().getDrawable(R.drawable.groupbox));
                itemView.setBackgroundResource(R.drawable.groupbox_selectable);
            }
            else{
                //itemView.setBackground(context.getResources().getDrawable(R.drawable.groupbox_grey));
                itemView.setBackgroundResource(R.drawable.groupbox_grey_selectable);
            }
        }

        @Override
        public void onClick(View v) {
            if(listener != null && monthTotal != null){
                listener.onMonthTotalSelected(monthTotal);
            }
        }
    }
}
