package com.pds.ficle.dashboard.dao;

import android.content.ContentResolver;

import com.pds.common.Formatos;
import com.pds.common.VentasHelper;
import com.pds.common.dao.ComprobanteDao;
import com.pds.common.dao.VentaDao;
import com.pds.common.db.ComprobanteTable;
import com.pds.common.model.Comprobante;
import com.pds.common.model.Venta;
import com.pds.common.model.VentaAbstract;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by Hernan on 03/08/2016.
 */
public class ReportDAO {
    public List<VentaAbstract> getVentasDate(ContentResolver contentResolver, Date fecha, boolean sort) {

        List<VentaAbstract> ventasList = null;

        // traemos solo las ventas del dia
        List<Venta> ventas = new VentaDao(contentResolver).list(VentasHelper.VENTA_FECHA + " = '" + Formatos.FormateaDate(fecha, Formatos.DateFormat) + "' ", null, " _id DESC");
        List<Comprobante> comprobantes = new ComprobanteDao(contentResolver).list(ComprobanteTable.COMPROBANTE_FECHA + " = '" + Formatos.FormateaDate(fecha, Formatos.DateFormat) + "' ", null, " _id DESC");

        ventasList = new ArrayList<VentaAbstract>();
        ventasList.addAll(ventas);
        ventasList.addAll(comprobantes);

        if (sort) {
            //ordenamos las ventas por fecha
            Collections.sort(ventasList, new Comparator<VentaAbstract>() {
                @Override
                public int compare(VentaAbstract lhs, VentaAbstract rhs) {
                    return lhs.compareTo(rhs);
                }
            });
        }

        return ventasList;
    }

    public List<VentaAbstract> getVentasMonth(ContentResolver contentResolver, Date fecha, boolean sort) {

        List<VentaAbstract> ventasList = null;

        // traemos solo las ventas del mes
        List<Venta> ventas = new VentaDao(contentResolver).list("substr(" + VentasHelper.VENTA_FECHA + ",4) ='" + Formatos.FormateaDate(fecha, Formatos.MonthYearFormat) + "'", null, " _id DESC");
        List<Comprobante> comprobantes = new ComprobanteDao(contentResolver).list("substr(" + ComprobanteTable.COMPROBANTE_FECHA + ",4) ='" + Formatos.FormateaDate(fecha, Formatos.MonthYearFormat) + "'", null, " _id DESC");

        ventasList = new ArrayList<VentaAbstract>();
        ventasList.addAll(ventas);
        ventasList.addAll(comprobantes);

        if (sort) {
            //ordenamos las ventas por fecha
            Collections.sort(ventasList, new Comparator<VentaAbstract>() {
                @Override
                public int compare(VentaAbstract lhs, VentaAbstract rhs) {
                    return lhs.compareTo(rhs);
                }
            });
        }

        return ventasList;
    }

    public String getMonthLabel(int monthNumber, int year) {
        String[] monthNames = {"Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"};
        return monthNames[monthNumber] + " " + String.valueOf(year);
    }
}
