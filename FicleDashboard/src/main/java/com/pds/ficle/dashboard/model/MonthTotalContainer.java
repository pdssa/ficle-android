package com.pds.ficle.dashboard.model;

import com.google.gson.annotations.SerializedName;
import com.pds.ficle.dashboard.model.MonthTotal;

import java.util.List;

/**
 * Created by Hernan on 12/08/2016.
 */
public class MonthTotalContainer extends ApiResponse {

    @SerializedName("monthTotals")
    private List<MonthTotal> monthTotalList;

    public List<MonthTotal> getMonthTotalList() {
        return monthTotalList;
    }

    public MonthTotalContainer(){

    }
}
