package com.pds.ficle.dashboard.util;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;

/**
 * Created by Hernan on 19/08/2016.
 */
public class CustomGridLayoutManager extends GridLayoutManager {
    private int mParentHeight;
    private int mItemHeight;

    public CustomGridLayoutManager(Context context, int spanCount) {
        super(context, spanCount);
        //mParentHeight = parentWidth;
        //mItemHeight = itemWidth;
    }

    @Override
    public int getPaddingTop() {
        return Math.round(mParentHeight / 2f - mItemHeight / 2f);
    }

    @Override
    public int getPaddingBottom() {
        return getPaddingTop();
    }
}
