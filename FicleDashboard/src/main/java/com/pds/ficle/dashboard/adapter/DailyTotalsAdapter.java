package com.pds.ficle.dashboard.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pds.common.Formatos;
import com.pds.ficle.dashboard.R;
import com.pds.ficle.dashboard.model.DailyTotal;
import com.pds.ficle.dashboard.model.ProductTotal;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by Hernan on 03/08/2016.
 */
public class DailyTotalsAdapter extends ArrayAdapter<DailyTotal> {
    private Context context;
    private List<DailyTotal> datos;
    private DecimalFormat FORMATO_DECIMAL;

    public DailyTotalsAdapter(Context context, List<DailyTotal> datos, DecimalFormat decimalFormat) {
        super(context, android.R.layout.simple_list_item_1, datos);

        this.context = context;
        this.datos = datos;
        this.FORMATO_DECIMAL = decimalFormat;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // En primer lugar "inflamos" una nueva vista, que será la que se
        // mostrará en la celda del ListView. Para ello primero creamos el
        // inflater, y después inflamos la vista.
        LayoutInflater inflater = LayoutInflater.from(context);
        View item = inflater.inflate(R.layout.daily_total_layout, null);

        DailyTotal row = datos.get(position);

        ((TextView) item.findViewById(R.id.daily_date)).setText(row.getLabel());

        ((TextView) item.findViewById(R.id.daily_total)).setText(Formatos.FormateaDecimal(row.getTotal(), FORMATO_DECIMAL));

        // Devolvemos la vista para que se muestre en el ListView.
        return item;

    }
}

