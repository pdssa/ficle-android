package com.pds.ficle.dashboard.model;

import com.pds.common.model.VentaAbstract;
import com.pds.ficle.dashboard.R;

/**
 * Created by Hernan on 14/08/2016.
 */
public class SalesItem {
    private double total;
    private String hour;
    private String ticket;
    private String payment;

    public SalesItem() {

    }

    public SalesItem(VentaAbstract venta){
        this.total = venta.getTotal();
        this.hour = venta.getHora();//.substring(0, 5);
        this.ticket = venta.getNroVenta();
        this.payment = venta.getMedio_pago().substring(0, 3);
    }

    public double getTotal() {
        return total;
    }

    public String getHour() {
        return hour;
    }

    public String getTicket() {
        return ticket;
    }

    public String getPayment() {
        return payment;
    }

    public int getPaymentIcon(){
        if (payment.equals("EFE"))
            return R.drawable.cash_icon;
        else if (payment.startsWith("TAR"))
            return R.drawable.card_icon;
        else if (payment.equals("CHE"))
            return R.drawable.bank_check_icon;
        else
            return R.drawable.information_icon;
    }
}
