package com.pds.ficle.dashboard.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Hernan on 12/08/2016.
 */
public class MonthTotal implements Parcelable {
    @SerializedName("label")
    private String label;
    @SerializedName("total")
    private double total;
    @SerializedName("id")
    private String id;

    @SerializedName("products")
    private List<ProductTotal> productsTotals;

    @SerializedName("days")
    private List<DailyTotal> dailyTotals;

    public MonthTotal(){

    }

    public MonthTotal(String label, double total, String id) {
        this.label = label;
        this.total = total;
        this.id = id;
    }

    protected MonthTotal(Parcel in) {
        label = in.readString();
        total = in.readDouble();
        id = in.readString();
    }

    public static final Creator<MonthTotal> CREATOR = new Creator<MonthTotal>() {
        @Override
        public MonthTotal createFromParcel(Parcel in) {
            return new MonthTotal(in);
        }

        @Override
        public MonthTotal[] newArray(int size) {
            return new MonthTotal[size];
        }
    };

    public String getLabel() {
        return label;
    }

    public double getTotal() {
        return total;
    }

    public String getId() {
        return id;
    }

    public List<ProductTotal> getProductsTotals() {
        return productsTotals;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public List<DailyTotal> getDailyTotals() {
        return dailyTotals;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(label);
        dest.writeDouble(total);
        dest.writeString(id);
    }

    @Override
    public boolean equals(Object o) {
        if(o != null && o instanceof MonthTotal){
            MonthTotal other = (MonthTotal)o;

            return this.getId().equals(other.getId());
        }

        return false;
    }
}
