package com.pds.ficle.dashboard.util;

import android.content.Context;
import android.os.AsyncTask;
import android.util.JsonReader;
import android.util.Log;

import com.google.gson.Gson;
import com.pds.common.Config;
import com.pds.ficle.dashboard.dao.MonthTotalsRequest;
import com.pds.ficle.dashboard.helpers.StringHelper;
import com.pds.ficle.dashboard.model.JsonWrapper;
import com.pds.ficle.dashboard.model.MonthTotal;
import com.pds.ficle.dashboard.model.MonthTotalContainer;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.List;

/**
 * Created by Hernan on 15/08/2016.
 */
public class GetMonthsTotalsTask<T> extends AsyncTask<String, String, T> {

    private static final String TAG = GetMonthsTotalsTask.class.getSimpleName();
    private ResultListener<T> listener;
    private Config config;

    public GetMonthsTotalsTask(Context context, ResultListener<T> listener) {
        this.listener = listener;
        this.config = new Config(context);
    }

    @Override
    protected T doInBackground(String[] params) {

        return getDataFromServer(params[0]);
    }

    @Override
    protected void onPostExecute(T input) {
        if (input != null)
            this.listener.finish(input);
        else
            this.listener.error("Se ha producido un error");
    }

    final int CONNECTION_TIMEOUT = 10000;//the timeout until a connection is established
    final int SOCKET_TIMEOUT = 30000; //is the timeout for waiting for data

    private T getDataFromServer(String monthToRequest) {
        try {

            // 1. create HttpClient
            HttpClient httpclient = new DefaultHttpClient();
            HttpParams params = httpclient.getParams();
            HttpConnectionParams.setConnectionTimeout(params, CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(params, SOCKET_TIMEOUT);

            // 2. make POST request to the given URL
            String servidor = this.config.CLOUD_SERVER_HOST.trim() + "api/DashboardGetMonthTotals";

            HttpGet httpGet = new HttpGet(servidor + "?deviceId=" + this.config.ANDROID_ID + "&month=" + monthToRequest);
            //HttpGet httpGet = new HttpGet(servidor + "/GetMonthsTotals?deviceId='4D171560'&month='" + monthToRequest + "'");

            // 7. Set some headers to inform server about the type of the content
            httpGet.setHeader("Accept", "application/json");
            httpGet.setHeader("Content-type", "application/json");

            // 8. Execute POST request to the given URL
            HttpResponse httpResponse = httpclient.execute(httpGet);

            // 9. receive response as inputStream
            InputStream inputStream = null;
            inputStream = httpResponse.getEntity().getContent();

            String s = StringHelper.Convert(inputStream, Charset.forName("UTF-8"));
            // 10. parse response
        //BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            MonthTotalContainer result = null;
            if (inputStream != null) {
                result = new Gson().fromJson(s, MonthTotalContainer.class);
                inputStream.close();
            }

            if (monthToRequest.equals("0")) {
                return (T) result.getMonthTotalList();
            } else {
                return (T) result.getMonthTotalList().get(0);
            }

          /*  JsonReader reader = new JsonReader(new InputStreamReader(inputStream, "UTF-8"));
            reader.beginObject();

            String d = reader.nextName();

            if (d.equals("d")) {

                String jsonResponse = reader.nextString();

                Gson gson = new Gson();

                if (monthToRequest.equals("0")) {
                    MonthTotalContainer object = gson.fromJson(jsonResponse, MonthTotalContainer.class);

                    return (T) object.getMonthTotalList();
                } else {
                    return (T) gson.fromJson(jsonResponse, MonthTotal.class);
                }
            }
*/


        } catch (Exception ex) {
            Log.e(TAG, "Error", ex);
        }

        return null;
    }



}
