package com.pds.ficle.dashboard.model;

import com.google.gson.annotations.SerializedName;

public class DashboardGetDailyTotalsResponse extends ApiResponse {
    @SerializedName("report")
    private DailyTotal mReport;

    public DailyTotal getReport() {
        return mReport;
    }
}
