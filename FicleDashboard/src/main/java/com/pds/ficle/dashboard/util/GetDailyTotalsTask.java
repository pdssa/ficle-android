package com.pds.ficle.dashboard.util;

import android.content.Context;
import android.os.AsyncTask;
import android.util.JsonReader;
import android.util.Log;

import com.google.gson.Gson;
import com.pds.common.Config;
import com.pds.ficle.dashboard.helpers.StringHelper;
import com.pds.ficle.dashboard.model.DailyTotal;
import com.pds.ficle.dashboard.model.DashboardGetDailyTotalsResponse;
import com.pds.ficle.dashboard.model.MonthTotal;
import com.pds.ficle.dashboard.model.MonthTotalContainer;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

/**
 * Created by Hernan on 15/08/2016.
 */
public class GetDailyTotalsTask extends AsyncTask<String, String, DailyTotal> {

    private ResultListener<DailyTotal> listener;
    private Config config;

    public GetDailyTotalsTask(Context context, ResultListener<DailyTotal> listener) {
        this.listener = listener;
        this.config = new Config(context);
    }

    @Override
    protected DailyTotal doInBackground(String[] params) {

        return getDataFromServer(params[0]);
    }

    @Override
    protected void onPostExecute(DailyTotal input) {
        if (input != null)
            this.listener.finish(input);
        else
            this.listener.error("Se ha producido un error");
    }

    final int CONNECTION_TIMEOUT = 10000;//the timeout until a connection is established
    final int SOCKET_TIMEOUT = 30000; //is the timeout for waiting for data

    private DailyTotal getDataFromServer(String dayToRequest) {
        try {

            // 1. create HttpClient
            HttpClient httpclient = new DefaultHttpClient();
            HttpParams params = httpclient.getParams();
            HttpConnectionParams.setConnectionTimeout(params, CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(params, SOCKET_TIMEOUT);

            // 2. make POST request to the given URL
            //String servidor = "http://200.69.238.37:39878/WS.asmx";
            String servidor = this.config.CLOUD_SERVER_HOST.trim() + "api/DashboardGetDailyTotals";

            HttpGet httpGet = new HttpGet(servidor + "?deviceId=" + this.config.ANDROID_ID + "&date=" + dayToRequest);
            //HttpGet httpGet = new HttpGet(servidor + "/GetDateTotal?deviceId='4D171560'&date='" + dayToRequest + "'");

            // 7. Set some headers to inform server about the type of the content
            httpGet.setHeader("Accept", "application/json");
            httpGet.setHeader("Content-type", "application/json");

            // 8. Execute POST request to the given URL
            HttpResponse httpResponse = httpclient.execute(httpGet);

            // 9. receive response as inputStream
            InputStream inputStream = null;
            inputStream = httpResponse.getEntity().getContent();

            // 10. parse response
           /* JsonReader reader = new JsonReader(new InputStreamReader(inputStream, "UTF-8"));
            reader.beginObject();

            String d = reader.nextName();

            if (d.equals("d")) {

                String jsonResponse = reader.nextString();

                Gson gson = new Gson();

                return gson.fromJson(jsonResponse, DailyTotal.class);
            }*/
            String s = StringHelper.Convert(inputStream, Charset.forName("UTF-8"));
            DashboardGetDailyTotalsResponse result = null;
            if (inputStream != null) {
                result = new Gson().fromJson(s, DashboardGetDailyTotalsResponse.class);
                inputStream.close();
            }

            return result.getReport();

        } catch (Exception ex) {
            Log.e("GetDailyTotalsTask", ex.getMessage());
        }

        return null;
    }
}
