package com.pds.ficle.dashboard.view;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Formato;
import com.pds.common.Formatos;
import com.pds.common.model.VentaAbstract;
import com.pds.common.model.VentaDetalleAbstract;
import com.pds.ficle.dashboard.R;
import com.pds.ficle.dashboard.adapter.RankingProductosAdapter;
import com.pds.ficle.dashboard.adapter.SalesAdapter;
import com.pds.ficle.dashboard.adapter.UltimasVentasAdapter;
import com.pds.ficle.dashboard.comp.ComparatorCantidad;
import com.pds.ficle.dashboard.comp.ComparatorGanancia;
import com.pds.ficle.dashboard.comp.ComparatorNombre;
import com.pds.ficle.dashboard.comp.ComparatorStock;
import com.pds.ficle.dashboard.comp.ComparatorTotal;
import com.pds.ficle.dashboard.controller.TotalsController;
import com.pds.ficle.dashboard.dao.MonthTotalsDAO;
import com.pds.ficle.dashboard.dao.ReportDAO;
import com.pds.ficle.dashboard.model.DailyTotal;
import com.pds.ficle.dashboard.model.ProductTotal;
import com.pds.ficle.dashboard.model.RankingProductos;
import com.pds.ficle.dashboard.model.SalesItem;
import com.pds.ficle.dashboard.util.ResultListener;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class VistaDiariaActivity extends Activity {

    public static final String ARGUMENT_DATE = "__DATE";
    private DecimalFormat FORMATO_DECIMAL;
    private List<SalesItem> ventasRecientesList;
    private SalesAdapter ventasRecientesAdapter;
    private ProgressBar progressBarSales, progressBarProducts;

    private List<ProductTotal> rankingProductosList;
    private RankingProductosAdapter rankingProductosAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vista_diaria);

        if(getIntent().hasExtra(ARGUMENT_DATE)){
            Date targetDate = (Date)getIntent().getExtras().getSerializable(ARGUMENT_DATE);

            Config config = new Config(this);

            //Configuracion de formatos segun PAIS
            Init_CustomConfigPais(config.PAIS);

            loadDateInfo(targetDate);
        }
    }

    private void Init_CustomConfigPais(String codePais) {
        FORMATO_DECIMAL = codePais.equals("AR") ? Formatos.DecimalFormat : Formatos.DecimalFormat_CH;
    }

    private void loadDateInfo(Date date){

        progressBarSales = (ProgressBar)findViewById(R.id.dash_diario_progress_ventas);
        progressBarProducts = (ProgressBar)findViewById(R.id.dash_diario_progress_ranking);

        progressBarSales.setVisibility(View.VISIBLE);
        progressBarProducts.setVisibility(View.VISIBLE);

        String selectedMonth = Formato.FormateaDate(date, Formato.DbDateFormat).substring(0, 6);
        String currentMonth = Formato.FormateaDate(new Date(), Formato.DbDateFormat).substring(0, 6);

        //si es un dia del mes en curso:
        if(selectedMonth.equals(currentMonth)){
            loadMonthDateInfo(date);
        }
        else{
            loadOldDateInfo(date);
        }
    }

    private void loadMonthDateInfo(Date date){

        ReportDAO reportDAO = new ReportDAO();

        //**obtenemos informacion del dia**
        List<VentaAbstract> ventasList = reportDAO.getVentasDate(getContentResolver(), date, true);

        double totalVentasDia = 0;

        // recorremos los items contando ventas y sumando totales
        if (ventasList.size() > 0) {
            for (VentaAbstract vta : ventasList) {
                totalVentasDia += vta.getTotal();
            }
        }

        String headerString = "Total del día " + Formato.FormateaDate(date, Formato.DateTwoDigFormatSlash);

        headerString += (" - " + Formatos.FormateaDecimal(totalVentasDia, FORMATO_DECIMAL) + " $");

        ((TextView) findViewById(R.id.dash_diario_txt_header)).setText(headerString);

        loadVentasList(getSalesList(ventasList));

        loadProductsList(getProductsList(ventasList));
    }

    private void loadOldDateInfo(Date date){
        new TotalsController().getDailyTotal(this, Formato.FormateaDate(date, Formato.DbDateFormat), new ResultListener<DailyTotal>() {
            @Override
            public void finish(DailyTotal dailyTotal) {
                String headerString = "Total del día " + dailyTotal.getLabel();

                headerString += (" - " + Formatos.FormateaDecimal(dailyTotal.getTotal(), FORMATO_DECIMAL) + " $");

                ((TextView) findViewById(R.id.dash_diario_txt_header)).setText(headerString);

                loadVentasList(dailyTotal.getSalesList());

                loadProductsList(dailyTotal.getProductsTotals());
            }

            @Override
            public void error(String message) {
                progressBarProducts.setVisibility(View.GONE);
                progressBarSales.setVisibility(View.GONE);
                Toast.makeText(VistaDiariaActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });

    }

    private List<SalesItem> getSalesList(List<VentaAbstract> ventasList){
        List<SalesItem> salesItems = new ArrayList<>();

        for (VentaAbstract vta : ventasList) {
            salesItems.add(new SalesItem(vta));
        }

        return salesItems;
    }

    private Collection<ProductTotal> getProductsList(List<VentaAbstract> ventasList){
        HashMap<Integer, ProductTotal> rankingProductos = new HashMap<Integer, ProductTotal>();

        //buscamos los detalles de las ventas
        for (VentaAbstract vta : ventasList) {
            vta.addDetalles(getContentResolver());

            for (VentaDetalleAbstract det : vta.getDetallesAbstract()) {

                Long idProducto = det.getProducto() != null ? det.getProducto().getId() : -1;

                if (!rankingProductos.containsKey(idProducto.intValue()))
                    rankingProductos.put(idProducto.intValue(),
                            new ProductTotal(
                                    det.getProducto()
                            ));

                rankingProductos.get(idProducto.intValue())
                        .addCantidad(det.getCantidad())
                        .addTotal(det.getTotal());

            }
        }

        return rankingProductos.values();
    }

    private void loadVentasList(List<SalesItem> salesList){

        if(ventasRecientesList == null) {
            this.ventasRecientesList = new ArrayList<SalesItem>();
            ventasRecientesList.addAll(salesList);
            ventasRecientesAdapter = new SalesAdapter(this, ventasRecientesList, true, FORMATO_DECIMAL);
            ((ListView) findViewById(R.id.dash_diario_lst_ventas)).setEmptyView(findViewById(R.id.dash_diario_txt_ventas_empty));
            ((ListView) findViewById(R.id.dash_diario_lst_ventas)).setAdapter(ventasRecientesAdapter);
        }
        else {
            ventasRecientesList.clear();
            ventasRecientesList.addAll(salesList);
            ventasRecientesAdapter.notifyDataSetChanged();
        }

        progressBarSales.setVisibility(View.GONE);
    }

    private void loadProductsList(Collection<ProductTotal> productTotalList){

        if(rankingProductosList == null) {
            rankingProductosList = new ArrayList<ProductTotal>();
            rankingProductosList.addAll(productTotalList);
            rankingProductosAdapter = new RankingProductosAdapter(this, rankingProductosList, FORMATO_DECIMAL);
            ((ListView) findViewById(R.id.dash_diario_lst_ranking)).setEmptyView(findViewById(R.id.dash_diario_txt_ranking_empty));
            ((ListView) findViewById(R.id.dash_diario_lst_ranking)).setAdapter(rankingProductosAdapter);
        }
        else {
            rankingProductosList.clear();
            rankingProductosList.addAll(productTotalList);
            rankingProductosAdapter.notifyDataSetChanged();
        }

        progressBarProducts.setVisibility(View.GONE);
    }

    public void backClick(View view){
        finish();
    }

    public void sortRankingList(View view){

        Comparator<ProductTotal> comparator = null;

        switch (view.getId()){
            case R.id.dash_diario_title_ranking_producto:{
                comparator = new ComparatorNombre();
            }break;
            case R.id.dash_diario_title_ranking_ganancia:{
                comparator = new ComparatorGanancia();
            }break;
            case R.id.dash_diario_title_ranking_cantidad:{
                comparator = new ComparatorCantidad();
            }break;
            case R.id.dash_diario_title_ranking_stock:{
                comparator = new ComparatorStock();
            }break;
            case R.id.dash_diario_title_ranking_total:{
                comparator = new ComparatorTotal();
            }break;
        }

        //si ya esta seleccionado invertimos el orden
        boolean reverse = false;

        ColorDrawable background = (ColorDrawable)view.getBackground();

        if(background != null && background.getColor() == getResources().getColor(R.color.amarillo))
            reverse = true;

        //clear
        clearSorts();

        //select sorted
        view.setBackgroundColor(getResources().getColor(R.color.amarillo));

        if(comparator != null) {
            if(reverse)
                comparator = Collections.reverseOrder(comparator);
            Collections.sort(rankingProductosList, comparator);
            rankingProductosAdapter.notifyDataSetChanged();
        }
    }

    private void clearSorts(){
        findViewById(R.id.dash_diario_title_ranking_producto).setBackgroundColor(getResources().getColor(R.color.transparent));
        findViewById(R.id.dash_diario_title_ranking_ganancia).setBackgroundColor(getResources().getColor(R.color.transparent));
        findViewById(R.id.dash_diario_title_ranking_cantidad).setBackgroundColor(getResources().getColor(R.color.transparent));
        findViewById(R.id.dash_diario_title_ranking_stock).setBackgroundColor(getResources().getColor(R.color.transparent));
        findViewById(R.id.dash_diario_title_ranking_total).setBackgroundColor(getResources().getColor(R.color.transparent));
    }
}
