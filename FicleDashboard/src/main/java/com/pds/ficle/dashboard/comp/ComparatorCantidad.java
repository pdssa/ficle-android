package com.pds.ficle.dashboard.comp;

import com.pds.ficle.dashboard.model.ProductTotal;

import java.util.Comparator;

/**
 * Created by Hernan on 23/08/2016.
 */
public class ComparatorCantidad implements Comparator<ProductTotal> {
    @Override
    public int compare(ProductTotal a, ProductTotal b) {
        return Double.compare(a.getCantidad(), b.getCantidad());
    }
}
