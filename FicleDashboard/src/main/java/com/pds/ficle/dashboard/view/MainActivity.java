package com.pds.ficle.dashboard.view;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Formato;
import com.pds.common.Formatos;
import com.pds.common.Usuario;
import com.pds.common.model.VentaAbstract;
import com.pds.ficle.dashboard.adapter.MonthTotalAdapter;
import com.pds.ficle.dashboard.R;
import com.pds.ficle.dashboard.controller.TotalsController;
import com.pds.ficle.dashboard.util.ResultListener;
import com.pds.ficle.dashboard.util.SendReportEmailTask;
import com.pds.ficle.dashboard.dao.MonthTotalsDAO;
import com.pds.ficle.dashboard.dao.ReportDAO;
import com.pds.ficle.dashboard.model.MonthTotal;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MainActivity extends Activity implements MonthTotalAdapter.MonthTotalListener {

    private List<MonthTotal> totals;
    private MonthTotalAdapter monthTotalAdapter;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;

    private DecimalFormat FORMATO_DECIMAL;
    private Usuario _user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            ((TextView) findViewById(R.id.txtVersion)).setText("Version:" + getPackageManager().getPackageInfo(this.getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        //logueo Usuario *****************************************************
        //obtenemos los parametros pasamos a la actividad
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            //tomamos el id de usuario
            //_id_user_login = extras.getInt("_id_user_login");
            this._user = (Usuario) extras.getSerializable("current_user");
        }

        //no tenemos user logueado...volvemos a EntryPoint
        if (_user == null) {
            Toast.makeText(this, R.string.unknown_user, Toast.LENGTH_LONG).show();
            this.finish();
        } else {

            Config config = new Config(this);

            //Configuracion de formatos segun PAIS
            Init_CustomConfigPais(config.PAIS);

            MonthTotal currentMonthTotal = loadTotals();

            loadOldMonthsTotals(currentMonthTotal);
        }
    }

    private void Init_CustomConfigPais(String codePais) {
        FORMATO_DECIMAL = codePais.equals("AR") ? Formatos.DecimalFormat : Formatos.DecimalFormat_CH;
    }

    @Override
    public void onMonthTotalSelected(MonthTotal monthTotal) {
        openMonthlyView(monthTotal);
    }

    private MonthTotal loadTotals() {

        ReportDAO reportDAO = new ReportDAO();

        //**obtenemos informacion del dia**
        List<VentaAbstract> ventasList = reportDAO.getVentasDate(getContentResolver(), new Date(), false);

        //int cantVentasDia = 0;
        double totalVentasDia = 0;

        // recorremos los items contando ventas y sumando totales
        if (ventasList.size() > 0) {
            for (VentaAbstract vta : ventasList) {
                totalVentasDia += vta.getTotal();
                //cantVentasDia++;
            }
        }

        loadDayTotal(totalVentasDia);

        //**obtenemos informacion del mes**
        ventasList = reportDAO.getVentasMonth(getContentResolver(), new Date(), false);

        double totalVentasMes = 0;

        // recorremos los items contando ventas y sumando totales
        if (ventasList.size() > 0) {
            for (VentaAbstract vta : ventasList) {
                totalVentasMes += vta.getTotal();
            }
        }

        loadMonthTotal(totalVentasMes);

        int cantidadDias = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

        loadMonthAverage(totalVentasMes / cantidadDias);

        String monthId = Formato.FormateaDate(new Date(), Formato.DbDateFormat).substring(0, 6);

        return new MonthTotal("", totalVentasMes, monthId);
    }

    private void loadDayTotal(double total) {
        //valor
        TextView txtTotalDiario = (TextView) findViewById(R.id.dash_main_hoy_total);

        String str_valor = Formatos.FormateaDecimal(total, FORMATO_DECIMAL) + " $";

        if (total > 9999999)//ajustamos el font
            txtTotalDiario.setTextSize(25);
        else
            txtTotalDiario.setTextSize(40);

        if (!str_valor.contains(",")) {//sino tiene decimales
            txtTotalDiario.setText(str_valor);
        } else {//si tiene decimales
            txtTotalDiario.setText(str_valor.split(",")[0]);
            //String decimales = str_valor.split(",")[1];
            //((TextView) vw_totales.findViewById(R.id.vw_simple_txt_3)).setText("," + (decimales.length() > 2 ? decimales.substring(0, 2) : decimales));
        }
    }

    private void loadMonthTotal(double total) {
        //valor
        TextView txtTotalMes = (TextView) findViewById(R.id.dash_main_mes_total);

        String str_valor = Formatos.FormateaDecimal(total, FORMATO_DECIMAL) + " $";

        if (total > 9999999)//ajustamos el font
            txtTotalMes.setTextSize(25);
        else
            txtTotalMes.setTextSize(40);

        if (!str_valor.contains(",")) {//sino tiene decimales
            txtTotalMes.setText(str_valor);
        } else {//si tiene decimales
            txtTotalMes.setText(str_valor.split(",")[0]);
            //String decimales = str_valor.split(",")[1];
            //((TextView) vw_totales.findViewById(R.id.vw_simple_txt_3)).setText("," + (decimales.length() > 2 ? decimales.substring(0, 2) : decimales));
        }
    }

    private void loadMonthAverage(double average) {
        //valor
        TextView txtPromedioMes = (TextView) findViewById(R.id.dash_main_hoy_promedio);

        String str_valor = Formatos.FormateaDecimal(average, FORMATO_DECIMAL) + " $";

        if (average > 9999999)//ajustamos el font
            txtPromedioMes.setTextSize(25);
        else
            txtPromedioMes.setTextSize(40);

        if (!str_valor.contains(",")) {//sino tiene decimales
            txtPromedioMes.setText(str_valor);
        } else {//si tiene decimales
            txtPromedioMes.setText(str_valor.split(",")[0]);
            //String decimales = str_valor.split(",")[1];
            //((TextView) vw_totales.findViewById(R.id.vw_simple_txt_3)).setText("," + (decimales.length() > 2 ? decimales.substring(0, 2) : decimales));
        }
    }

    private void loadOldMonthsTotals(MonthTotal currentMonthTotal) {

        progressBar = (ProgressBar)findViewById(R.id.dash_main_progress);
        progressBar.setVisibility(View.VISIBLE);

        totals = new ArrayList<>();
        monthTotalAdapter = new MonthTotalAdapter(this, totals, FORMATO_DECIMAL, this);
        recyclerView = (RecyclerView) findViewById(R.id.dash_main_recycler_meses);
        recyclerView.setAdapter(monthTotalAdapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 6));

        new TotalsController().getMonthsTotals(this, currentMonthTotal, new ResultListener<List<MonthTotal>>() {
            @Override
            public void finish(List<MonthTotal> result) {
                totals.clear();
                totals.addAll(result);
                monthTotalAdapter.notifyDataSetChanged();
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void error(String message) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    /*private void loadMonthData(Date date, double total, int labelId, int totalId) {

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(date.getTime());

        ((TextView)findViewById(labelId)).setText(new ReportDAO().getMonthLabel(cal.get(Calendar.MONTH), cal.get(Calendar.YEAR)));
        ((TextView)findViewById(totalId)).setText(Formatos.FormateaDecimal(total, FORMATO_DECIMAL) + " $");
    }*/

    public void enviarInfDiarioClick(View view) {
        new SendReportEmailTask(this, SendReportEmailTask.REPORT_TYPE.DAILY).start();
    }

    public void enviarInfSemanalClick(View view) {
        new SendReportEmailTask(this, SendReportEmailTask.REPORT_TYPE.MONTHLY).start();
    }

    public void abrirGraficosClick(View view) {
        Intent i = new Intent(MainActivity.this, DashboardMainActivity.class);
        startActivity(i);
    }

    public void closeClick(View view) {
        finish();
    }

    public void totalHoyClick(View view) {
        openDailyView(new Date());
    }

    public void totalMesClick(View view) {

        String currentMonthId = Formato.FormateaDate(new Date(), Formato.DbDateFormat).substring(0, 6);

        if(totals != null){
            for (MonthTotal monthTotal : totals) {
                if(monthTotal.getId().equals(currentMonthId))
                    openMonthlyView(monthTotal);
            }
        }
    }

    public void rptVentasClick(View view){
        Intent i = new Intent(MainActivity.this, RptDetalleVentas.class);
        startActivity(i);
    }

    private void openDailyView(Date date) {
        Intent intent = new Intent(this, VistaDiariaActivity.class);
        intent.putExtra(VistaDiariaActivity.ARGUMENT_DATE, date);
        startActivity(intent);
    }

    private void openMonthlyView(MonthTotal month) {
        Intent intent = new Intent(this, VistaMensualActivity.class);
        intent.putExtra(VistaMensualActivity.ARGUMENT_MONTH, month);
        startActivity(intent);
    }
}
