package com.pds.ficle.dashboard.dao;

import android.content.Context;
import android.content.res.AssetManager;

import com.google.gson.Gson;
import com.pds.ficle.dashboard.model.DailyTotal;
import com.pds.ficle.dashboard.model.MonthTotal;
import com.pds.ficle.dashboard.model.MonthTotalContainer;
import com.pds.ficle.dashboard.util.GetDailyTotalsTask;
import com.pds.ficle.dashboard.util.GetMonthsTotalsTask;
import com.pds.ficle.dashboard.util.ResultListener;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

/**
 * Created by Hernan on 12/08/2016.
 */
public class MonthTotalsDAO {

    public void getMonthsTotals(Context context, ResultListener<List<MonthTotal>> listener) {
        new GetMonthsTotalsTask<List<MonthTotal>>(context, listener).execute("0");
    }

    public List<MonthTotal> _getMonthsTotals(Context context) {
        MonthTotalContainer monthTotalContainer = (MonthTotalContainer) getObjectJSON(context, MonthTotalContainer.class, "monthsTotals.json");
        return monthTotalContainer.getMonthTotalList();
    }

    public void getMonthTotal(Context context, String monthId, ResultListener<MonthTotal> listener){
        new GetMonthsTotalsTask<MonthTotal>(context, listener).execute(monthId);
    }

    public MonthTotal _getMonthTotal(Context context, String monthId){
        MonthTotal monthTotal = (MonthTotal) getObjectJSON(context, MonthTotal.class, "monthTotal.json");
        return monthTotal;
    }

    public void getDailyTotal(Context context, String dateId, ResultListener<DailyTotal> listener){
        new GetDailyTotalsTask(context, listener).execute(dateId);
    }

    public DailyTotal _getDailyTotal(Context context, String dateId){
        DailyTotal dailyTotal = (DailyTotal) getObjectJSON(context, DailyTotal.class, "dailyTotal.json");
        return dailyTotal;
    }

    private Object getObjectJSON(Context context, Class aClass, String fileName){

        Object object = null;
        try{

            AssetManager manager = context.getAssets();
            InputStream inputStream = manager.open(fileName);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            Gson gson = new Gson();
            object = gson.fromJson(bufferedReader, aClass);
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return object;
    }
}
