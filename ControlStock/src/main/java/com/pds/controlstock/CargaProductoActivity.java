package com.pds.controlstock;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Formatos;
import com.pds.common.Usuario;
import com.pds.common.activity.TimerMainActivity;
import com.pds.common.dao.HistMovStockDao;
import com.pds.common.dao.ProductDao;
import com.pds.common.db.ProductTable;
import com.pds.common.model.HistMovStock;
import com.pds.common.model.Product;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by EMEKA on 19/01/2015.
 */
public class CargaProductoActivity extends TimerMainActivity {
    String codigoIngresado;
    //control de fechas
    BroadcastReceiver _broadcastReceiver;
    //controles del panel
    ImageView imgIcono_panel;
    TextView txtWelcome_panel;
    TextView txtComentario_panel;
    TextView txtFuncion_panel;
    TextView txtValor_panel;
    TextView txtPanel;
    TextView txtUser_panel;
    TextView txtHora_panel;
    TextView txtFecha_panel;
    TextView txtProductName;
    TextView txtProductPrice;
    ImageView imvProductImage;
    ListView listViewHistorialStock;
    String signo;
    private Usuario _user;
    private ImageButton btnCalc;
    private Product _product;
    private DecimalFormat FORMATO_DECIMAL;
    private Config config;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carga_producto);
        try {

            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);//evitamos el sleep durante la ejecucion de esta aplicacion

            ((TextView) findViewById(R.id.txtVersion)).setText("Version:" + getPackageManager().getPackageInfo(this.getPackageName().toString(), 0).versionName);

            //logueo Usuario *****************************************************
            //obtenemos los parametros pasamos a la actividad
            Bundle extras = getIntent().getExtras();
                if (extras != null) {
                    //tomamos el id de usuario
                    //_id_user_login = extras.getInt("_id_user_login");
                    this._user = (Usuario) extras.getSerializable("current_user");
                }

                //no tenemos user logueado...volvemos a EntryPoint
                if (_user == null) {
                    Toast.makeText(this, R.string.unknown_user, Toast.LENGTH_LONG).show();
                    this.finish();
                } else {

                codigoIngresado = "";
                config = new Config(this);

                listViewHistorialStock = (ListView) findViewById(R.id.lv_ultimas_ventas);
                listViewHistorialStock.setEmptyView(findViewById(android.R.id.empty));

                //customizar por pais
                Init_CustomConfigPais(config.PAIS);

                //Lo llama desde el onResume()
                //LeerHistMovStock();
            }

        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        LeerHistMovStock();
    }

    private void Init_CustomConfigPais(String codePais) {

        FORMATO_DECIMAL = codePais.equals("AR") ? Formatos.DecimalFormat : Formatos.DecimalFormat_CH;
    }

    public void abrirActivityProducto(Product product) {
        Intent intent = new Intent(CargaProductoActivity.this, MainActivity.class);
        intent.putExtra("product", product);
        intent.putExtra("current_user", _user);
        startActivity(intent);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {

        if (event.isPrintingKey() || keyCode == KeyEvent.KEYCODE_ENTER) {

            if (keyCode == KeyEvent.KEYCODE_ENTER) {

                IngresaProductoByCode(codigoIngresado);

                codigoIngresado = "";

            } else {
                codigoIngresado += String.valueOf(event.getDisplayLabel());
            }

            return true;

        } else {
            return super.onKeyUp(keyCode, event);
        }
    }

    private void IngresaProductoByCode(String codigoProducto) {
        try {

            Product p = null;

            p = ObtenerProductoByCode(codigoProducto);

            //si encuentra un producto con el codigo, abre la pantalla con los datos del producto
            if (p != null) {
                //Como pudo leer, envio el producto a la pantalla controlstock.MainActivity
                abrirActivityProducto(p);

            } else {
                //No se encontro el producto (mensaje en pantalla)
                Toast.makeText(this, "No se encontro el producto", Toast.LENGTH_SHORT).show();
            }

        } catch (Exception ex) {
            Toast.makeText(this, "Error:" + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private Product ObtenerProductoByCode(String code) {
        Product producto = null;

        try {
            List<Product> productos = new ProductDao(getContentResolver()).list(ProductTable.COLUMN_CODE + " = '" + code + "' AND removed = 0", null, null);

            if (productos.size() > 0) {
                producto = productos.get(0);
            }

        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }

        return producto;
    }

    /*public void LeerVentas() {
        List<VentaDetalleAbstract> list = ObtieneUltimasVentasDetalle(ObtieneUltimasVentas(5));

        VentasDetalleAdapter adapter = new VentasDetalleAdapter(this, list);
        listViewVentas.setAdapter(adapter);
    }*/

    /*class VentasDetalleAdapter extends ArrayAdapter<VentaDetalleAbstract> {
        private Context context;
        private List<VentaDetalleAbstract> datos;

        public VentasDetalleAdapter(Context context, List<VentaDetalleAbstract> datos) {
            super(context, android.R.layout.simple_list_item_1, datos);

            this.context = context;
            this.datos = datos;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // En primer lugar "inflamos" una nueva vista, que será la que se
            // mostrará en la celda del ListView. Para ello primero creamos el
            // inflater, y después inflamos la vista.
            if (convertView == null) {
                convertView = LayoutInflater.from(context)
                        .inflate(R.layout.item_ultima_venta, parent, false);
            }

            VentaDetalleAbstract row = datos.get(position);

            // A partir de la vista, recogeremos los controles que contiene para
            // poder manipularlos.
            // Recogemos los TextView para mostrar datos
            ((TextView) convertView.findViewById(R.id.cs_item_product)).setText(row.getDescripcion());
            ((TextView) convertView.findViewById(R.id.cs_item_precio)).setText(Formatos.FormateaDecimal(row.getPrecio(), FORMATO_DECIMAL, "$"));

            TextView cantidadText = (TextView) convertView.findViewById(R.id.cs_item_cant);
            if (row.esItemPesable()) {
                //reducimos el font para que entre el peso en kg
                //cantidadText.setText(String.valueOf(row.get_cantidad()) + " kg");
                if (row.getCantidad() == 0) {
                    cantidadText.setText("0");
                } else {
                    cantidadText.setText(Formatos.FormateaDecimal(row.getCantidad(), Formatos.DecimalFormat_US) + "K");
                }
            } else {
                cantidadText.setText(Formatos.FormateaDecimal(row.getCantidad(), Formatos.DecimalFormat_US));
            }
            return convertView;
        }
    }*/

    public void LeerHistMovStock() {
        List<HistMovStock> list = ObtieneHistorialSotck(5);

        HistMovStockAdapter adapter = new HistMovStockAdapter(this, list);
        listViewHistorialStock.setAdapter(adapter);
    }

    private List<HistMovStock> ObtieneHistorialSotck(int cantMostrada) {
        List<HistMovStock> list = new HistMovStockDao(getContentResolver()).list(null, null, "_id DESC", null, null, String.valueOf(cantMostrada));

        return list;
    }

    /*public List<VentaAbstract> ObtieneUltimasVentas(int cantVentas) {
        List<VentaAbstract> list = VentaAbstract.ventasList(getContentResolver(), "codigo='PDV'", null, "_id DESC", null, null, String.valueOf(cantVentas));
        Collections.sort(list, new Comparator<VentaAbstract>() {
            @Override
            public int compare(VentaAbstract lhs, VentaAbstract rhs) {
                return lhs.compareTo(rhs);
            }
        });
        return list.subList(0, cantVentas > list.size() ? list.size() : cantVentas);
    }*/

    /*public List<VentaDetalleAbstract> ObtieneUltimasVentasDetalle(List<VentaAbstract> list) {
        List<VentaDetalleAbstract> listVDA = new ArrayList<VentaDetalleAbstract>();

        for (VentaAbstract va : list) {
            va.addDetalles(getContentResolver());
            listVDA.addAll(va.getDetallesAbstract());
        }
        return listVDA;
    }*/

    private Product ObtieneProductoPorId(Long productoid) {
        Product producto = null;

        try {
            List<Product> productos = new ProductDao(getContentResolver()).list(ProductTable.COLUMN_ID + " = '" + String.valueOf(productoid) + "' AND removed = 0", null, null, null, null, null);

            if (productos.size() > 0) {
                producto = productos.get(0);
            }

        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }

        return producto;
    }

    class HistMovStockAdapter extends ArrayAdapter<HistMovStock> {
        private Context context;
        private List<HistMovStock> datos;

        public HistMovStockAdapter(Context context, List<HistMovStock> datos) {
            super(context, android.R.layout.simple_list_item_1, datos);

            this.context = context;
            this.datos = datos;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // En primer lugar "inflamos" una nueva vista, que será la que se
            // mostrará en la celda del ListView. Para ello primero creamos el
            // inflater, y después inflamos la vista.
            if (convertView == null) {
                convertView = LayoutInflater.from(context)
                        .inflate(R.layout.item_ultima_venta, parent, false);
            }

            HistMovStock row = datos.get(position);

            Product p = ObtieneProductoPorId(row.getProductoid());

            //cargo la variable signo dependiento de la operación realizada
            signo = row.getOperacion().equals("I") ? "+" : "-";

            // A partir de la vista, recogeremos los controles que contiene para
            // poder manipularlos.
            // Recogemos los TextView para mostrar datos
            ((TextView) convertView.findViewById(R.id.cs_item_product)).setText(p.getName());
            ((TextView) convertView.findViewById(R.id.cs_item_stock)).setText(String.valueOf((int) row.getStock()));
            if (row.getStock() <= 0){
                ((TextView) convertView.findViewById(R.id.cs_item_stock)).setTextColor(getResources().getColor(R.color.boton_rojo));
            }else{
                ((TextView) convertView.findViewById(R.id.cs_item_stock)).setTextColor(getResources().getColor(R.color.texto_black));
            }

            ((TextView) convertView.findViewById(R.id.cs_item_cant)).setText(String.valueOf(signo + row.getCantidad()));

            if (position == 0) {
                ((TextView) convertView.findViewById(R.id.cs_item_product)).setTextSize(40);
                ((TextView) convertView.findViewById(R.id.cs_item_product)).setTypeface(Typeface.DEFAULT_BOLD);
                ((TextView) convertView.findViewById(R.id.cs_item_stock)).setTextSize(40);
                ((TextView) convertView.findViewById(R.id.cs_item_stock)).setTypeface(Typeface.DEFAULT_BOLD);
                ((TextView) convertView.findViewById(R.id.cs_item_cant)).setTextSize(40);
                ((TextView) convertView.findViewById(R.id.cs_item_cant)).setTypeface(Typeface.DEFAULT_BOLD);
            }

            return convertView;
        }
    }
}