package com.pds.controlstock;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Caja;
import com.pds.common.Config;
import com.pds.common.Formatos;
import com.pds.common.Logger;
import com.pds.common.Usuario;
import com.pds.common.VentaDetalleHelper;
import com.pds.common.activity.TimerActivity;
import com.pds.common.dao.HistMovStockDao;
import com.pds.common.dao.ProductDao;
import com.pds.common.dao.VentaDao;
import com.pds.common.db.ProductTable;
import com.pds.common.fragment.PadFragment;
import com.pds.common.fragment.ProductosFragment;
import com.pds.common.model.HistMovStock;
import com.pds.common.model.Product;
import com.pds.common.model.Venta;
import com.pds.common.util.UriUtils;

import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends TimerActivity implements PadFragment.PadListener {

    //control de fechas
    BroadcastReceiver _broadcastReceiver;
    //controles del panel
    ImageView imgIcono_panel;
    TextView txtWelcome_panel;
    TextView txtComentario_panel;
    TextView txtFuncion_panel;
    TextView txtValor_panel;
    TextView txtPanel;
    TextView txtUser_panel;
    TextView txtHora_panel;
    TextView txtFecha_panel;
    TextView txtProductName;
    TextView txtProductPrice;
    TextView txtProductStock;
    ImageView imvProductImage;
    Timer timer;
    MyTimerTask myTimerTask;
    int tiempoTimer = 6000;//el tiempo esta en ms
    private Usuario _user;
    private ImageButton btnCalc;
    private Product _product;
    private String codigoIngresado;
    private Config config;
    private DecimalFormat FORMATO_DECIMAL;
    private boolean flag;
    private boolean flagFinish;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
            //Obtengo el producto que viene por parametro en el intent
            Bundle extras = getIntent().getExtras();
            this._product = (Product) extras.getParcelable("product");

            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);//evitamos el sleep durante la ejecucion de esta aplicacion

            ((TextView) findViewById(R.id.txtVersion)).setText("Version:" + getPackageManager().getPackageInfo(this.getPackageName().toString(), 0).versionName);

            //logueo Usuario *****************************************************
            //obtenemos los parametros pasamos a la actividad
            if (extras != null) {
                //tomamos el id de usuario
                //_id_user_login = extras.getInt("_id_user_login");
                this._user = (Usuario) extras.getSerializable("current_user");
            }

            //no tenemos user logueado...volvemos a EntryPoint
            if (_user == null) {
                Toast.makeText(this, R.string.unknown_user, Toast.LENGTH_LONG).show();
                this.finish();
            } else {
                //********* USER LOGUEADO OK *********

                //asignamos los view
                Init_AsignarViews();

                //asignamos los eventos
                Init_AsignarEventos();
                this.txtUser_panel.append(this._user.getNombre());

                config = new Config(this);
                //customizar por pais
                Init_CustomConfigPais(config.PAIS);

                //this.txtWelcome_panel.setText("CASHDRAWER");
                this.txtWelcome_panel.setVisibility(View.INVISIBLE);
                this.imgIcono_panel.setImageResource(R.drawable.app_stock_min);
                this.imgIcono_panel.setVisibility(View.VISIBLE);

                //Seteo los datos del producto
                seteaValoresProducto(_product);

                PadFragment padFragment = (PadFragment) getFragmentManager().findFragmentById(R.id.frg_pag);
                padFragment.setPadListener(this);
                padFragment.setFunctionConfig(PadFragment.FnButton.FUNCTION_1, "ING. STOCK", true, R.drawable.botones_teclado, 35);
                padFragment.setFunctionConfig(PadFragment.FnButton.FUNCTION_2, "CANCELAR", true, R.drawable.botones_main_izq_red, 40);
                padFragment.setFunctionConfig(PadFragment.FnButton.FUNCTION_4, "VALIDAR", true, R.drawable.botones_main_izq);
            }

        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void Init_CustomConfigPais(String codePais) {

        FORMATO_DECIMAL = codePais.equals("AR") ? Formatos.DecimalFormat : Formatos.DecimalFormat_CH;
    }

    //Setea en el panel_txt_fecha la hora del SO
    @Override
    public void onStart() {
        super.onStart();
        _broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().compareTo(Intent.ACTION_TIME_TICK) == 0) {
                    Date now = new Date();
                    ((TextView) findViewById(R.id.panel_txt_fecha)).setText(Formatos.FormateaDate(now, Formatos.DateFormat));
                    ((TextView) findViewById(R.id.panel_txt_hora)).setText(Formatos.FormateaDate(now, Formatos.ShortTimeFormat));
                }
            }
        };

        registerReceiver(_broadcastReceiver, new IntentFilter(Intent.ACTION_TIME_TICK));
    }

    //Setea la fecha y hora al volver a la activity
    @Override
    protected void onResume() {
        super.onResume();
        this.txtFecha_panel.setText(Formatos.FormateaDate(new Date(), Formatos.DateFormat));
        this.txtHora_panel.setText(Formatos.FormateaDate(new Date(), Formatos.ShortTimeFormat));
    }

    //Setea el broadcast de la hora en null al salir de la activity
    @Override
    public void onStop() {
        super.onStop();
        if (_broadcastReceiver != null)
            unregisterReceiver(_broadcastReceiver);
    }

    private void Init_AsignarViews() {

        this.btnCalc = (ImageButton) findViewById(R.id.cash_btnCalc);

        this.txtProductName = (TextView) findViewById(R.id.cs_product_name);
        this.txtProductPrice = (TextView) findViewById(R.id.cs_product_price);
        this.txtProductStock = (TextView) findViewById(R.id.cs_product_stock);
        this.imvProductImage = (ImageView) findViewById(R.id.cs_product_image);

        this.txtPanel = (TextView) findViewById(R.id.panel_txt_valor);
        codigoIngresado = "";


        //controles del panel
        this.imgIcono_panel = (ImageView) findViewById(R.id.panel_img_icono);
        this.txtWelcome_panel = (TextView) findViewById(R.id.panel_txt_welcome);
        this.txtUser_panel = (TextView) findViewById(R.id.panel_txt_user);
        this.txtComentario_panel = (TextView) findViewById(R.id.panel_txt_comentario);
        this.txtFuncion_panel = (TextView) findViewById(R.id.panel_txt_funcion);
        this.txtValor_panel = (TextView) findViewById(R.id.panel_txt_valor);
        this.txtFecha_panel = (TextView) findViewById(R.id.panel_txt_fecha);
        this.txtHora_panel = (TextView) findViewById(R.id.panel_txt_hora);
    }

    private void Init_AsignarEventos() {

        btnCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_MAIN);
                i.setComponent(new ComponentName("com.android.calculator2", "com.android.calculator2.Calculator"));


                try {
                    MainActivity.this.startActivity(i);
                } catch (ActivityNotFoundException noSuchActivity) {
                    // si el calculator intent no está registrado, probamos con un nombre alternativo
                    Intent i2 = new Intent();
                    i2.setAction(Intent.ACTION_MAIN);
                    i2.setComponent(new ComponentName("com.sec.android.app.popupcalculator", "com.sec.android.app.popupcalculator.Calculator"));
                    try {
                        MainActivity.this.startActivity(i2);
                    } catch (ActivityNotFoundException noSuchActivity2) {
                        // calculator intent not found
                        Toast.makeText(MainActivity.this, "Acceso a calculadora no encontrado", Toast.LENGTH_LONG).show();
                    }
                }

            }
        });
    }

    @Override
    public void onNumberPressed(String numberPressed) {
        //Si presionan un punto, al ser cantidades enteras, no debería hacer nada
        if (!numberPressed.equals(".")) {
            if (flag) {
                this.txtPanel.setText("");
                AgregarTextoPanel(numberPressed);
                flag = false;
            } else {
                AgregarTextoPanel(numberPressed);
            }
            //Resetea el timer
            reseteaTimer();
        }
    }

    private void AgregarTextoPanel(String texto) {
        this.txtPanel.setText(this.txtPanel.getText().toString() + texto);
        this.txtPanel.setVisibility(View.VISIBLE);
    }

    @Override
    public void onFunction1Pressed() {
        if (!TextUtils.isEmpty(this.txtPanel.getText())) {
            //Ing. Stock -> Suma la cantidad al Stock
            _product.setStock(_product.getStock() + obtieneCantIngresada());
            ProductDao prodDao = new ProductDao(getContentResolver());
            prodDao.saveOrUpdate(_product);
            registrarMoviemientoStock(_product.getId(), obtieneCantIngresada(), (double) (obtieneCantIngresada() * _product.getSalePrice()), "I", (int)_product.getStock());
            Toast.makeText(this, "Stock ingresado OK", Toast.LENGTH_SHORT).show();
            onBackPressed();
        }else{
            Toast.makeText(getApplicationContext(), "Debe ingresar un valor", Toast.LENGTH_LONG).show();
            reseteaTimer();
        }
    }

    @Override
    public void onFunction2Pressed() {
        onBackPressed();
    }

    @Override
    public void onFunction3Pressed() {
        //CLEAR OPTION
        String texto = txtPanel.getText().toString();

        if (!texto.isEmpty()) {
            txtPanel.setText(texto.substring(0, texto.length() - 1));
        }
        reseteaTimer();
    }

    @Override
    public void onFunction4Pressed() {
        if (!TextUtils.isEmpty(this.txtPanel.getText())) {
            //Valida -> Descuenta Stock
            _product.setStock(_product.getStock() - obtieneCantIngresada());
            ProductDao prodDao = new ProductDao(getContentResolver());
            prodDao.saveOrUpdate(_product);
            registrarMoviemientoStock(_product.getId(), obtieneCantIngresada(), -1 * (obtieneCantIngresada() * _product.getSalePrice()), "V", (int)_product.getStock());
            realizarVentaYRegistroCaja();
            Toast.makeText(this, "Stock validado OK", Toast.LENGTH_SHORT).show();
            if (flagFinish) {
                onBackPressed();
            }
        }else{
            Toast.makeText(getApplicationContext(), "Debe ingresar un valor", Toast.LENGTH_LONG).show();
            reseteaTimer();
        }
    }

    public void seteaValoresProducto(Product product) {
        _product = product;
        txtProductName.setText(product.getName());
        txtProductStock.setText(String.valueOf((int)product.getStock()));
        if (product.getStock() <= 0) {
            txtProductStock.setTextColor(getResources().getColor(R.color.boton_rojo));
        }else{
            txtProductStock.setTextColor(getResources().getColor(R.color.texto_black));
        }
        txtProductPrice.setText(Formatos.FormateaDecimal(product.getSalePrice(), FORMATO_DECIMAL, "$"));
        imvProductImage.setImageURI(UriUtils.resolveUri(this, product.getImage_uri()));
        this.txtPanel.setText("");
        onNumberPressed("1");
        flag = true;
        flagFinish = true;
        //Resetea el Timer
        reseteaTimer();
    }

    private void reseteaTimer() {
        if (timer != null) {
            timer.cancel();
        }
        timer = new Timer();
        myTimerTask = new MyTimerTask();
        timer.schedule(myTimerTask, tiempoTimer);

    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {

        if (event.isPrintingKey() || keyCode == KeyEvent.KEYCODE_ENTER) {

            if (keyCode == KeyEvent.KEYCODE_ENTER) {

                //Al ingresar un nuevo producto debe validar el anterior y no salir de la pantalla
                flagFinish = false;

                //Rebaja el stock al pistolear un producto
                onFunction4Pressed();

                IngresaProductoByCode(codigoIngresado);

                codigoIngresado = "";


            } else {
                codigoIngresado += String.valueOf(event.getDisplayLabel());
            }

            return true;

        } else {
            return super.onKeyUp(keyCode, event);
        }
    }

    private void IngresaProductoByCode(String codigoProducto) {
        try {

            Product p = null;

            p = ObtenerProductoByCode(codigoProducto);

            //si encuentra un producto con el codigo, abre la pantalla con los datos del producto
            if (p != null) {
                //Como pudo leer, envio el producto a la pantalla controlstock.MainActivity
                seteaValoresProducto(p);

            } else {
                //No se encontro el producto (mensaje en pantalla)
                Toast.makeText(this, "No se encontro el producto", Toast.LENGTH_SHORT).show();
                onBackPressed();
            }
        } catch (Exception ex) {
            Toast.makeText(this, "Error:" + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private Product ObtenerProductoByCode(String code) {
        Product producto = null;

        try {
            List<Product> productos = new ProductDao(getContentResolver()).list(ProductTable.COLUMN_CODE + " = '" + code + "' AND removed = 0", null, null);

            if (productos.size() > 0) {
                producto = productos.get(0);
            }

        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }

        return producto;
    }

    private int obtieneCantIngresada() {

        if (!TextUtils.isEmpty(this.txtPanel.getText())) {
            return Integer.parseInt(this.txtPanel.getText().toString());
        } else {
            Toast.makeText(getApplicationContext(), "Debe ingresar un valor", Toast.LENGTH_LONG).show();
            return 0;
        }
    }

    //Registra en la tabla HistMovStock la cantidad + o - segun la operación
    private void registrarMoviemientoStock(Long productoid, int cantidad, double monto, String operacion, int stock) {
        try {
            HistMovStock hist = new HistMovStock();
            hist.setProductoid(productoid);
            hist.setCantidad(cantidad);
            hist.setMonto(monto);
            hist.setOperacion(operacion);
            hist.setStock(stock);

            HistMovStockDao histDao = new HistMovStockDao(getContentResolver());
            histDao.save(hist);

        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        timer.cancel();
        timer = null;
    }

    class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    Log.d("Timer Run", "llega a 3 seg");
                    onFunction4Pressed();
                }
            });
        }

    }

    private void realizarVentaYRegistroCaja() {

        Date now = new Date();
        double neto_venta = 0, iva_venta = 0, alic_iva = 0;
        double total_venta = obtieneCantIngresada() * _product.getSalePrice();

        //DESGLOSAMOS EL TOTAL SEGUN EL IMPUESTO DEL PRODUCTO
        if (_product.getIdTax() != 4) //SI NO es EXENTO
            alic_iva = _product.getIva() / 100;

        neto_venta += Formatos.RedondeaDecimal(total_venta / (1 + alic_iva), FORMATO_DECIMAL);
        iva_venta += Formatos.RedondeaDecimal(total_venta * alic_iva / (1 + alic_iva), FORMATO_DECIMAL);

        //VENTA
        Venta _venta = new com.pds.common.model.Venta(
                0,
                total_venta,//va el total de la venta
                1,//cantidad de items de la venta
                Formatos.FormateaDate(now, Formatos.DateFormat),
                Formatos.FormateaDate(now, Formatos.TimeFormat),
                "STK",
                this._user.getId(),
                "EFECTIVO",
                neto_venta,
                iva_venta
        );
        //grabar
        new VentaDao(getContentResolver()).save(_venta);

        //DETALLES
        ContentValues cvDetalle = new ContentValues();

        cvDetalle.put(VentaDetalleHelper.VENTA_DET_VENTA_ID, _venta.getId());
        cvDetalle.put(VentaDetalleHelper.VENTA_DET_PRODUCTO_ID, _product.getId());
        cvDetalle.put(VentaDetalleHelper.VENTA_DET_CANTIDAD, obtieneCantIngresada());
        cvDetalle.put(VentaDetalleHelper.VENTA_DET_PRECIO, _product.getSalePrice());
        cvDetalle.put(VentaDetalleHelper.VENTA_DET_TOTAL, total_venta);
        cvDetalle.put(VentaDetalleHelper.VENTA_DET_SKU, _product.getCode());
        cvDetalle.put(VentaDetalleHelper.VENTA_DET_NETO, neto_venta);
        cvDetalle.put(VentaDetalleHelper.VENTA_DET_IVA, iva_venta);

        getContentResolver().insert(Uri.parse("content://com.pds.ficle.ep.venta_detalles.contentprovider/venta_detalles"), cvDetalle);

        //Hace el registro de CAJA
        Caja.RegistrarMovimientoCaja(MainActivity.this, Caja.TipoMovimientoCaja.DEPOSITO, _venta.getTotal(), _venta.getMedio_pago(), "", _venta.getId(), "VTA");

        Logger.RegistrarEvento(this, "i", _venta.getType(config.PAIS) + " #" + _venta.getNroVenta(), "Total: " + Formatos.FormateaDecimal(_venta.getTotal(), FORMATO_DECIMAL, "$"), "Medio Pago: " + _venta.getMedio_pago());
    }

}
