package com.pds.scan;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.pdf417.encoder.Dimensions;

import java.util.EnumMap;
import java.util.Map;

/**
 * Created by Hernan on 01/12/2014.
 */
public abstract class PDF417 {

    private static final int WHITE = 0xFFFFFFFF;
    private static final int BLACK = 0xFF000000;

    public static void encode(String contents){
        com.google.zxing.pdf417.encoder.PDF417 pdf417 = new com.google.zxing.pdf417.encoder.PDF417();
        //pdf417.generateBarcodeLogic();
    }

    public static Bitmap encodeAsBitmap(String contents, BarcodeFormat format, int img_width, int img_height, boolean resize) throws WriterException {
        String contentsToEncode = contents;
        if (contentsToEncode == null) {
            return null;
        }
        Map<EncodeHintType, Object> hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
        String encoding = guessAppropriateEncoding(contentsToEncode);
        if (encoding != null) {
            hints.put(EncodeHintType.CHARACTER_SET, encoding);
        }
        if(format == BarcodeFormat.PDF_417) {
            hints.put(EncodeHintType.PDF417_DIMENSIONS, new Dimensions(2, 30, 2, 30));
            hints.put(EncodeHintType.MARGIN, 0);
            hints.put(EncodeHintType.ERROR_CORRECTION, 5);
        }
        MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix result;
        try {
            result = writer.encode(contentsToEncode, format, img_width, img_height, hints);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);

        if(!resize)
            return bitmap;
        else
            return getResizedBitmap(bitmap, img_height, img_width);
    }

    private static Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth){
        int width = bm.getWidth();
        int height = bm.getHeight();
        //float scaleWidth = ((float) newWidth) / width;
        //float scaleHeight = ((float) newHeight) / height;

        // CREATE A MATRIX FOR THE MANIPULATION
        //Matrix matrix = new Matrix();

        // RESIZE THE BIT MAP
        //matrix.postScale(scaleWidth, scaleHeight);

        // RECREATE THE NEW BITMAP
        //Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, newWidth, newHeight, matrix, false);
        //return resizedBitmap;
        //return Bitmap.createScaledBitmap(bm, width < newWidth ? width : newWidth, height < newHeight ? height : newHeight, false);
        return Bitmap.createScaledBitmap(bm, newWidth,  newHeight, false);

    }

    private static String guessAppropriateEncoding(CharSequence contents) {
        // Very crude at the moment
        for (int i = 0; i < contents.length(); i++) {
            if (contents.charAt(i) > 0xFF) {
                return "UTF-8";
            }
        }
        return null;
    }
}
