package com.exavore.PDF417;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Generate PDF417 2D barcodes.
 * @author Ethan Allison (e@exavore.com)<br /> Originally by Sergey Ushakov (sergey@verify.kg) 2003-2005
 * @version 1.0
 * @since 2011-06-24
 */
public class PDF417 {
	private PDF417_data data;
	private PDF417_size size;
	private PDF417_eccCodewords eccCW;
	private boolean truncated;
	private int aspectRatio;
	private int z;
	private byte[][][] pattern;
	protected PDF417_eccLevel eccLevel;
	protected PDF417_dataCodewords dataCW;
	protected int[] codewords;

	/**
	 * Sets up a new PDF417 barcode.
	 * 
	 * Use {@link #encode()} to output it as a bitmap.
	 * 
	 * @param content The string you want to encode as a PDF417
	 * @param rows Manually set the number of rows. Specify 0 for auto.
	 * @param cols Manually set the number of columns. Specify 0 for auto.
	 * @param zoom Zoom factor
	 * @param heightToWidthRatio Rectangle height compared to width. 3-5 is normal.
	 * @param truncate Enables a truncated PDF417, which has no right side control codes.
	 */
	public PDF417(String content, int rows, int cols, int zoom,
			int heightToWidthRatio, boolean truncate) {
		data = new PDF417_data(content.getBytes());
		if (rows > 0 ^ cols > 0)
			throw new PDF417EncodeException(String.format(
					"Invalid size configuration: %d rows and %d columns", rows,
					cols));
		if (rows > 0 & cols > 0)
			size = new PDF417_size(rows, cols);
		else
			size = new PDF417_size();
		dataCW = new PDF417_dataCodewords();
		eccLevel = new PDF417_eccLevel();
		eccCW = new PDF417_eccCodewords();
		z = zoom;
		aspectRatio = heightToWidthRatio;
		truncated = truncate;
	}

	/**
	 * Sets up a new PDF417 barcode with default options.
	 * 
	 * Use {@link #encode()} to output it as a bitmap.
	 * Defaults: automatic rows and columns, no zoom, 3:1 height to width, not truncated
	 * 
	 * @param content The content you want to encode as a PDF417.
	 */
	public PDF417(String content) {
		this(content, 0, 0, 1, 3, false);
	}

	private void HLE() {
		// A.1
		dataCW.data = PDF417_utils.binaryModeEncode(data.data);

		// A.2
		int len = dataCW.data.length + 1;

		// A.3
		eccLevel.detect(len);

		// A.4
		size.detect(len, eccLevel.getEccLevel());

		// A.5
		codewords = new int[size.getHeight() * size.getWidth()];
		int[] datawords = new int[codewords.length - eccLevel.getEccLenght()];
		datawords[0] = datawords.length;
		System.arraycopy(dataCW.data, 0, datawords, 1, dataCW.data.length);
		for (int i = dataCW.data.length + 1; i < datawords.length; i++)
			datawords[i] = PDF417_utils.reminderByteCompactLatch;
		dataCW.data = datawords;

		// A.6
		eccCW = new PDF417_eccCodewords(dataCW.data, eccLevel);

		// A.7
		System.arraycopy(dataCW.data, 0, codewords, 0, dataCW.data.length);
		System.arraycopy(eccCW.data, 0, codewords, dataCW.data.length,
				eccCW.data.length);
	}

	private void LLE() {
		int pw, ph, i, j;
		ph = size.getHeight();
		pw = size.getWidth();

		if (truncated)
			pattern = new byte[ph][pw + 2][];
		else
			pattern = new byte[ph][pw + 4][];

		for (i = 0; i < ph; i++) {
			pattern[i][0] = PDF417_utils.startPattern;
			pattern[i][1] = PDF417_utils.getPattern(i, lri(i));
			if (!truncated) {
				pattern[i][pw + 3] = PDF417_utils.stopPattern;
				pattern[i][pw + 2] = PDF417_utils.getPattern(i, rri(i));
			}
		}
		int l = 0;
		for (i = 0; i < ph; i++)
			for (j = 2; j < pw + 2; j++)
				pattern[i][j] = PDF417_utils.getPattern(i, codewords[l++]);

		if (l != codewords.length)
			throw new PDF417EncodeException("Internal error.");

	}

	protected int lri(int row) {
		int c = row % 3;
		switch (c) {
		case 0:
			return 30 * (row / 3) + ((size.getHeight() - 1) / 3);
		case 1:
			return 30 * (row / 3) + eccLevel.getEccLevel() * 3
					+ ((size.getHeight() - 1) % 3);
		case 2:
			return 30 * (row / 3) + (size.getWidth() - 1);
		}
		return -1;
	}

	protected int rri(int row) {
		int c = row % 3;
		switch (c) {
		case 0:
			return 30 * (row / 3) + (size.getWidth() - 1);
		case 1:
			return 30 * (row / 3) + ((size.getHeight() - 1) / 3);
		case 2:
			return 30 * (row / 3) + eccLevel.getEccLevel() * 3
					+ ((size.getHeight() - 1) % 3);
		}
		throw new PDF417EncodeException(
				"Assertion failed while calculating right-row indicator.");
	}

	/**
	 * @return The bitmap that contains the barcode.
	 */
	public Bitmap encode() {
		HLE();
		LLE();
		return image();
	}

	private Bitmap image() {
		int w, h, l, cx, cy;
		l = pattern[0].length;
		h = z * (pattern.length + 4) * aspectRatio;
		w = l * 17;
		if (pattern[0][l - 1].length == 9)
			w++;
		w += 4;
		w *= z;
		// Set up drawing tools
		Bitmap img = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
		Canvas c = new Canvas(img);
		Paint p = new Paint();
		p.setColor(Color.WHITE);
		// Draw the barcode
		c.drawColor(Color.WHITE);
		for (int i = 0; i < pattern.length; i++) {
			byte[][] b = pattern[i];
			cx = 2 * z;
			cy = z * ((i + 2) * aspectRatio);
			for (int ii = 0; ii < b.length; ii++) {
				byte[] bb = b[ii];
				for (int iii = 0; iii < bb.length; iii++) {
					int bbb = bb[iii] * z;
					if (iii % 2 == 0)
						p.setColor(Color.BLACK);
					else
						p.setColor(Color.WHITE);
					c.drawRect(
							new Rect(cx, cy + z * aspectRatio, cx + bbb, cy),
							p);
					cx += bbb;
				}
				p.setColor(Color.WHITE);
				c.drawRect(new Rect(cx, cy + aspectRatio * z, w, cy), p);
			}
		}
		p.setColor(Color.WHITE);
		c.drawRect(new Rect(0, h, w, (pattern.length + 2) * aspectRatio), p);
		return img;
	}

	public void setData(PDF417_data data) {
		this.data = data;
	}

	public void setData(byte[] data) {
		this.data = new PDF417_data(data);
	}

	public void setSize(PDF417_size size) {
		this.size = size;
	}

	public void setEccLevel(PDF417_eccLevel eccLevel) {
		this.eccLevel = eccLevel;
	}

	public void setTruncated(boolean truncated) {
		this.truncated = truncated;
	}

    public void setAspectRatio(int aspectRatio){this.aspectRatio = aspectRatio;}
}
