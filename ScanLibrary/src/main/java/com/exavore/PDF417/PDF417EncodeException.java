package com.exavore.PDF417;


class PDF417EncodeException extends RuntimeException {

	private static final long serialVersionUID = -1521204932271080219L;

public PDF417EncodeException() {
  }

  public PDF417EncodeException(String s) {
    super(s);
  }
}
