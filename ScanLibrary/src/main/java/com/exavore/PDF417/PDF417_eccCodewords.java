package com.exavore.PDF417;


class PDF417_eccCodewords {
  int[] data;

  public PDF417_eccCodewords() {
    data= new int[0];
  }

  public PDF417_eccCodewords(int[] datacw,PDF417_eccLevel level) {
    this.data = PDF417_utils.calculateEcc(datacw,level);
  }
}
