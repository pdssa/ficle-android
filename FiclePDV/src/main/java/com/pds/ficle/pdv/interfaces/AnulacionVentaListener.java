package com.pds.ficle.pdv.interfaces;

/**
 * Created by Hernan on 20/07/2016.
 */
public interface AnulacionVentaListener {
    void onAnulacionOk();

    void onAnulacionError(String title, String message);
}
