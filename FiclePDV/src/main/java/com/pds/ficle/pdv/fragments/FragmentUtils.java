package com.pds.ficle.pdv.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;

import com.pds.ficle.pdv.R;

/**
 * Created by Hernan on 02/07/2015.
 */
public abstract class FragmentUtils {

    public static void showHideFragment(boolean hide, FragmentManager fragmentManager) {
        FragmentManager fm = fragmentManager;
        Fragment fragment = fm.findFragmentById(R.id.frgProductos);

        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.setCustomAnimations(android.R.animator.fade_in,
                android.R.animator.fade_out);
        if (hide) {
            ft.hide(fragment);
        } else {
            ft.show(fragment);
        }
        ft.commit();
    }
}
