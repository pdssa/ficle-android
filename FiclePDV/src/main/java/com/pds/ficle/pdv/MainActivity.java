package com.pds.ficle.pdv;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Messenger;
import android.os.RemoteException;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Caja;
import com.pds.common.Caja.TipoMovimientoCaja;
import com.pds.common.Config;
import com.pds.common.Formato;
import com.pds.common.Formatos;
import com.pds.common.Logger;
//import com.pds.common.UsuarioHelper;
import com.pds.common.SII.vale.listener.AnulacionListener;
import com.pds.common.SII.vale.model.AnulacionReq;
import com.pds.common.SII.vale.model.Vale;
import com.pds.common.SII.vale.services.AnulacionTask;
import com.pds.common.SII.vale.services.InformarValeOnlineTask;
import com.pds.common.SII.vale.util.ValeAnuladoTicketBuilder;
import com.pds.common.SII.vale.util.ValeTicketBuilder;
import com.pds.common.SII.vale.util.ValeUtils;
import com.pds.common.Usuario;
import com.pds.common.UsuarioHelper;
import com.pds.common.activity.TimerActivity;
import com.pds.common.dao.ComprobanteDao;
import com.pds.common.dao.ComprobanteDetalleDao;
import com.pds.common.dao.CuentaCteTotalesDao;
import com.pds.common.dao.HistMovStockDao;
import com.pds.common.dao.SubDepartmentDao;
import com.pds.common.dao.TaxDao;
import com.pds.common.dao.TipoDocDao;
import com.pds.common.dao.VentaAnuladaDao;
import com.pds.common.dao.VentaAnuladaDetalleDao;
import com.pds.common.dialog.Dialog;
import com.pds.common.display.Message;
import com.pds.common.display.server.ServerDisplayService;
import com.pds.common.fragment.BaseProductosFragment;
import com.pds.common.fragment.ProductoFragmentListener;
import com.pds.common.hardware.ScannerPOWA;
import com.pds.common.model.Comprobante;
import com.pds.common.model.ComprobanteDetalle;
import com.pds.common.model.CuentaCteTotal;
import com.pds.common.model.HistMovStock;
import com.pds.common.model.SubDepartment;
import com.pds.common.model.Tax;
import com.pds.common.model.TipoDoc;
import com.pds.common.model.Venta;
import com.pds.common.VentaDetalleHelper;
import com.pds.common.dao.CuentaCteDao;
import com.pds.common.dao.ProductDao;
import com.pds.common.dao.VentaDao;
import com.pds.common.dao.VentaDetalleDao;
import com.pds.common.db.ProductTable;
import com.pds.common.dialog.ConfirmDialog;
import com.pds.common.integration.android.ScanIntegrator;
import com.pds.common.fragment.ProductosFragment;
import com.pds.common.media.PlaySound;
import com.pds.common.model.Cliente;
import com.pds.common.model.CuentaCte;
import com.pds.common.model.Product;
import com.pds.common.model.VentaAbstract;
import com.pds.common.model.VentaAnulada;
import com.pds.common.model.VentaAnuladaDetalle;
import com.pds.common.model.VentaDetalle;
import com.pds.common.model.VentaDetalleAbstract;
import com.pds.common.pref.PDVConfig;
import com.pds.common.util.ConnectivityUtils;
import com.pds.common.util.Sku_Utils;
import com.pds.common.util.Window;
import com.pds.ficle.pdv.adapters.PedidoAdapter;
import com.pds.ficle.pdv.adapters.ProductoRow;
import com.pds.ficle.pdv.adapters.VentasAdapter;
import com.pds.ficle.pdv.interfaces.AnulacionVentaListener;

import android_serialport_api.CardReader;
import android_serialport_api.CardReader.OnReadSerialPortDataListener;
import android_serialport_api.CardReader.SerialPortData;
import android_serialport_api.LineaTicket;
import android_serialport_api.Printer;

import java.io.File;
import java.lang.reflect.Method;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android_serialport_api.PrinterPOWA;
import android_serialport_api.TicketBuilder;

public class MainActivity extends TimerActivity
        implements ProductoFragmentListener,
        CheckoutFragment.CheckoutInteractionListener,
        CtaClienteFragment.OnCtaCteInteractionListener//,
//        ComboSelectFragment.OnComboSelectFragmentInteractionListener
{

    private Printer _printer;
    private CardReader _lector;
    private Config config;

    private Usuario _user;

    private SimpleDateFormat FORMATO_FECHA;
    private Formato.Decimal_Format FORMATO_DECIMAL_ID;
    private DecimalFormat FORMATO_DECIMAL;
    private DecimalFormat FORMATO_DECIMAL_SECUND;
    private String FORMATO_SIGNO_MONEDA;

    private Tax IVA_GRAVADO;

    //private int _id_user_login = -1;
    //private String _nombre_user_login;

    ImageButton btnCalc;

    List<Pedido> pedido = null;
    Pedido pedido_item_edit = null;

    //List<String> listDataHeader;
    //HashMap<String, List<String>> listDataChild;

    //ListView lstProductos;
    List<ProductoRow> productosList;

    ListView lstVentas;
    List<VentaAbstract> ventasList;
    VentaAbstract ventaHistEdit;

    Venta ventaCurrent;

    TicketBuilder ticket = null;

    ImageView dummy_view;
    ImageView dummy_view_b;

    String funcion_activa = "";

    //bara inferior
    //ImageButton btnExit;

    //barra superior derecha
    //Button btnEditar;
//    Button btnProductosPropios;
//    Button btnCombos;
    Button btnCatalogo;


    //barra superior izquierda
    ImageButton btnMenu;
    ImageButton btnNuevo;
    ImageButton btnEliminarPedido;
    TextView txtTotalPedido;

    //controles del panel
    TextView txtWelcome_panel;
    ImageView imgIcono_panel;
    TextView txtComentario_panel;
    TextView txtFuncion_panel;
    TextView txtValor_panel;
    //TextView txtPanel;
    TextView txtUser_panel;
    TextView txtHora_panel;
    TextView txtFecha_panel;
    Button btnDeshacer_panel;
    ImageButton btnEliminar_panel;
    Button btnDescripcion_panel;
    Button btnValidar_panel;

    //teclado
    Button btnCantidad;
//    Button btnPrecio;
    Button btnEnter;
    ImageButton btnBackspace;
    Button btn0;
    Button btn1;
    Button btn2;
    Button btn3;
    Button btn4;
    Button btn5;
    Button btn6;
    Button btn7;
    Button btn8;
    Button btn9;
    Button btnPunto;

    Button btnLoadMore;//boton que figura al pie del listado de ventas historicas

    //controles del pedido
    Button btnImprimir;
    ListView lstPedido;
    TextView lblNuevoPedido;
    TextView txtNroPedido;
    TextView lblPedidoRegistrado;

    //controles del popup de medio de pago
    Button btnConfirmar_mp;
    Button btnCancelar_mp;
    TextView txtTotal_mp;
    RadioGroup rbtMedios_mp;

    //control de fechas
    BroadcastReceiver _broadcastReceiver;

    //control de scan
    boolean scanIniciado;
    String codigoIngresado;

    //control del lector
    String CARD_NUMBER;
    private boolean cardInput = false;


    private View pdvView;
    private View pdvStatusView;
    private TextView pdvStatusMessageView;
    private Button btnAnular;
    private boolean primeroLimpiarPanel = false;
    private Button btnComprobante;
    private Button btnVenta;
    private Button btnCobrar;
    private Cliente cliente_selected;

    private boolean registarComprobante;
    private float sizeText = 65, normalSizeText = 40;
    //private Button btnProductUndo;
    //private Product last_producto;

    //private boolean modoDemoFM = false;
    //private boolean printBoleta = false;
    private String nombreProdGenerico = "GENERICO";
    private String PREF_PIE_TICKET;
    private String PREF_MEDIO_DEFAULT;
    private Boolean PREF_AJUSTE_SENCILLO, PREF_SHOW_IVA, PREF_IMPRIMIR_AUTOMATIC, PREF_SHOW_CHECKOUT, PREF_SHOW_PROD_SIN_PRECIO, PREF_EMITIR_VALE, PREF_COMPROB_HABILIT, PREF_ACTUALIZ_PRECIO_LISTA;
    private Boolean PREF_CIERRE_Z_REQUERIDO;
    private Integer PREF_TIEMPO_CONFIRM;

    private ScannerPOWA _scanner;

    private boolean DISPLAY_ACTIVATED = false; //para activar la funcionalidad de display remoto


    private String TEXT_ORIGINAL = "ORIGINAL";
    private String TEXT_DUPLICADO = "DUPLICADO";
    private String TEXT_DUPLICADO_ANULADO = "DUPLICADO - ANULACION";

    private Integer CANT_MAX = 99;
    private Double SUBT_MAX = 999999d;

    @Override
    public void onBackPressed() {

        if (primeroLimpiarPanel) {
            primeroLimpiarPanel = false;
            txtValor_panel.setText("");
            return;
        }

        //si esta abierto el fragment de productos, en el back lo ocultamos en lugar de salir del pdv
        if (getFragmentManager().findFragmentById(R.id.frgProductos) != null) {
            accionCloseFragmentProductos.onClick(null);
        } else if (pedido != null) {
            DescartarVentaEnCurso(new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    pedido = null;
                    pedido_item_edit = null;

                    CloseTicketInDisplay();

                    onBackPressed();
                }
            });
        } else {
            //como vamos a finalizar => detenemos el timer y salimos
            Timer_Cancel();
            super.stopInactivityTimer();
            super.onFinish();
        }
    }

    /*
    @Override
    public void onStart() {
        super.onStart();
        _broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().compareTo(Intent.ACTION_TIME_TICK) == 0) {
                    Date now = new Date();
                    ((TextView) findViewById(R.id.panel_txt_fecha)).setText(Formatos.FormateaDate(now, Formatos.DateFormat));
                    ((TextView) findViewById(R.id.panel_txt_hora)).setText(Formatos.FormateaDate(now, Formatos.ShortTimeFormat));
                }
            }
        };

        registerReceiver(_broadcastReceiver, new IntentFilter(Intent.ACTION_TIME_TICK));

    }
    */


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);//evitamos el sleep durante la ejecucion de esta aplicacion

            ((TextView) findViewById(R.id.txtVersion)).setText("Version:" + getPackageManager().getPackageInfo(this.getPackageName().toString(), 0).versionName);

            //obtenemos los parametros pasamos a la actividad
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                //tomamos el id de usuario
                //_id_user_login = extras.getInt("_id_user_login");
                this._user = (Usuario) extras.getSerializable("current_user");
            }

            //**********************************

            //no tenemos user logueado...volvemos a EntryPoint
            if (_user == null) {
                Toast.makeText(this, R.string.unknown_user, Toast.LENGTH_LONG).show();
                this.finish();
                return;
            } else {

                //********* USER LOGUEADO OK *********

                //asignamos los view
                Init_AsignarViews();

                //preparamos menu
                //PrepararMenu();

                config = new Config(this);

                //customizar por pais
                Init_CustomConfigPais(config.PAIS);

                //asignamos comportamiento
                Init_AsignarEventos();

                this.txtUser_panel.append(this._user.getNombre());

                scanIniciado = false;
                codigoIngresado = "";

                //printBoleta = config.PERFIL.equals(Perfiles.PERFIL_FACTURA_MOVIL) || modoDemoFM; // || config.PERFIL.equals(Perfiles.PERFIL_IQCORP);
                //printBoleta = false;

                //leemos las configuraciones del usuario
                SharedPreferences pref = getSharedPreferences(PDVConfig.SHARED_PREF_NAME, 0);
                PREF_PIE_TICKET = pref.getString(PDVConfig.PDV_PIE_TICKET, "GRACIAS POR SU VISITA");
                PREF_MEDIO_DEFAULT = pref.getString(PDVConfig.PDV_MEDIO_DEFAULT, CheckoutFragment.MEDIO_EFECTIVO_ID);
                PREF_MEDIO_DEFAULT = (TextUtils.isEmpty(PREF_MEDIO_DEFAULT) ? CheckoutFragment.MEDIO_EFECTIVO_ID : PREF_MEDIO_DEFAULT);
                PREF_AJUSTE_SENCILLO = Boolean.parseBoolean(pref.getString(PDVConfig.PDV_AJUSTE_SENCILLO, "false"));
                PREF_IMPRIMIR_AUTOMATIC = Boolean.parseBoolean(pref.getString(PDVConfig.PDV_IMPRIMIR_AUTOMATICO, "true"));
                PREF_SHOW_IVA = Boolean.parseBoolean(pref.getString(PDVConfig.PDV_MOSTRAR_DESGLOSE_IMPUESTOS, "true"));
                PREF_SHOW_CHECKOUT = Boolean.parseBoolean(pref.getString(PDVConfig.PDV_SOLICITAR_CHECKOUT, "true"));
                PREF_SHOW_PROD_SIN_PRECIO = Boolean.parseBoolean(pref.getString(PDVConfig.PDV_MOSTRAR_PRODUCTOS_SIN_PRECIO, "true"));
                PREF_TIEMPO_CONFIRM = Integer.parseInt(pref.getString(PDVConfig.PDV_TIEMPO_CONFIRMACION, "0"));
                //PREF_EMITIR_VALE = Boolean.parseBoolean(pref.getString(PDVConfig.PDV_EMITIR_VALE, "false"));
                PREF_EMITIR_VALE = config.isValeInicializado();
                PREF_COMPROB_HABILIT = Boolean.parseBoolean(pref.getString(PDVConfig.PDV_COMPROB_HABILIT, "false"));
                if (PREF_EMITIR_VALE) {
                    PREF_COMPROB_HABILIT = true; //forzamos si VALE está habilitado, para que tenga la opcion de "REGISTRO MANUAL" siempre disponible
                }
                PREF_ACTUALIZ_PRECIO_LISTA = Boolean.parseBoolean(pref.getString(PDVConfig.PDV_ACTUALIZ_PRECIO_LISTA, "true"));
                PREF_CIERRE_Z_REQUERIDO = ValeUtils.isCierreZRequired(MainActivity.this);

                //obtenemos el IVA
                IVA_GRAVADO = new TaxDao(getContentResolver()).list("name = 'Gravado'", null, null).get(0);


                //iniciamos el timer de confirmacion automatica (si corresponde)
                Timer_Init();

                //si tenemos una printer POWA, entonces iniciamos el scanner POWA S10
                if (config.PRINTER_MODEL != null && config.PRINTER_MODEL.equals("POWA")) {

                    _printer = Printer.getInstance(config.PRINTER_MODEL, MainActivity.this);

                    if (_printer instanceof PrinterPOWA) {
                        ((PrinterPOWA) _printer).addScanner(new PrinterPOWA.ScannerCallback() {
                            @Override
                            public void onScannerRead(String text) {
                                IngresaProductoBySKU(text, false);
                            }
                        });
                    }

                    /*_scanner = new ScannerPOWA(this);
                    _scanner._scannerCallback = new ScannerPOWA.ScannerCallback() {
                        @Override
                        public void onScannerRead(String text) {
                            IngresaProductoByCode(text);
                        }
                    };*/
                }


                //SECCION PARA CONEXION CON DISPLAY CLIENTE
                if (DISPLAY_ACTIVATED) {
                    bindDisplayServerService();
                }
                //....


                if (PREF_EMITIR_VALE)
                    modoFiscal();

            }

        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void modoFiscal() {
        btnComprobante.setText(getString(R.string.btn_registro_manual));

        if (PREF_CIERRE_Z_REQUERIDO) {
            btnVenta.setEnabled(false);
            btnVenta.setTypeface(Typeface.DEFAULT);
            btnVenta.setText(Html.fromHtml(getString(R.string.btn_vale_electronico_disabled)));
            btnVenta.setBackgroundResource(R.drawable.backrounded_grey_dark);
        } else
            btnVenta.setText(getString(R.string.btn_vale_electronico));
    }

    //region DISPLAY REMOTO

    /**
     * Class for interacting with the main interface of the service.
     */
    private ServiceConnection mConnection;
    private boolean isBound = false;
    private Messenger mService;

    private void bindDisplayServerService() {

        /** Defines callbacks for service binding, passed to bindService() */
        mConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName className, IBinder service) {
                // This is called when the connection with the service has been
                // established, giving us the object we can use to
                // interact with the service.
                // Cast the IBinder and get LocalService instance
                //mService = ((ServerDisplayService.ServiceBinder) service).getService();


                // This is called when the connection with the service has been
                // established, giving us the object we can use to
                // interact with the service.  We are communicating with the
                // service using a Messenger, so here we get a client-side
                // representation of that from the raw IBinder object.
                mService = new Messenger(service);

                isBound = true;
            }

            @Override
            public void onServiceDisconnected(ComponentName className) {
                // This is called when the connection with the service has been
                // unexpectedly disconnected -- that is, its process crashed.
                mService = null;
                isBound = false;
            }
        };

        // Bind to LocalService
        if (!isBound) {
            Intent i = new Intent();
            i.setClassName("com.pds.remote.server", "com.pds.common.display.server.ServerDisplayService");
            //isBound = bindService(i, mConnection, Context.BIND_AUTO_CREATE);
            bindService(i, mConnection, Context.BIND_AUTO_CREATE);
        }
    }

    private void SendTicketToDisplay() {

        if (DISPLAY_ACTIVATED) {

            if (pedido != null && pedido.size() > 0 && isBound && mService != null) {

                Message msg = new Message();
                msg.type = Message.MessageType.SHOW_SCREEN_TYPE;
                msg.timeout = 1;


                Message.ShowScreenRequest request = msg.new ShowScreenRequest();

                request.screen = "TICKET";
                request.data = new ArrayList<Message.ShowScreenViewContent>();

                //*****************TOTAL DEL PEDIDO*****************
                Message.ShowScreenViewContent item = msg.new ShowScreenViewContent();
                item.idView = "txt_total_monto";
                item.classTypeView = "android.widget.TextView";
                item.viewContentType = "java.lang.CharSequence";
                item.viewContent = ((TextView) findViewById(R.id.pedido_txt_total_monto)).getText().toString();

                request.data.add(item);

                //*****************CANTIDAD DE ITEMS DEL PEDIDO*****************
                item = msg.new ShowScreenViewContent();
                item.idView = "txt_total_cant";
                item.classTypeView = "android.widget.TextView";
                item.viewContentType = "java.lang.CharSequence";
                item.viewContent = ((TextView) findViewById(R.id.pedido_txt_total_cant)).getText().toString();

                request.data.add(item);

                //*****************ENCABEZADO (#PEDIDO o HORA)*****************
                item = msg.new ShowScreenViewContent();
                item.idView = "txt_pedido";
                item.classTypeView = "android.widget.TextView";
                item.viewContentType = "java.lang.CharSequence";
                item.viewContent = txtNroPedido.getText().toString();

                request.data.add(item);

                //*****************ITEMS PEDIDO********************
                //agregamos una lista, para ello, primero creamos la lista de ITEMS
                List<Message.ShowScreenListContent> viewContentList = new ArrayList<Message.ShowScreenListContent>();

                for (Pedido p : pedido) {

                    //cada item, es una lista de views
                    Message.ShowScreenListContent listItem = msg.new ShowScreenListContent();

                    listItem.item = new ArrayList<Message.ShowScreenViewContent>();

                    //*****************ITEM PEDIDO: PRODUCTO********************
                    item = msg.new ShowScreenViewContent();
                    item.idView = "item_txt_producto";
                    item.classTypeView = "android.widget.TextView";
                    item.viewContentType = "java.lang.CharSequence";
                    item.viewContent = p.get_descripcion();

                    listItem.item.add(item);

                    //*****************ITEM PEDIDO: CANTIDAD********************
                    item = msg.new ShowScreenViewContent();
                    item.idView = "item_txt_cantidad";
                    item.classTypeView = "android.widget.TextView";
                    item.viewContentType = "java.lang.CharSequence";
                    item.viewContent = p.esItemPesable() ? Formatos.FormateaDecimal(p.get_cantidad(), Formatos.DecimalFormat_US) + "K" : Formatos.FormateaDecimal(p.get_cantidad(), Formatos.DecimalFormat_US);

                    listItem.item.add(item);

                    //*****************ITEM PEDIDO: PRECIO********************
                    item = msg.new ShowScreenViewContent();
                    item.idView = "item_txt_precio";
                    item.classTypeView = "android.widget.TextView";
                    item.viewContentType = "java.lang.CharSequence";
                    item.viewContent = Formatos.FormateaDecimal(p.get_precio(), FORMATO_DECIMAL_SECUND);

                    listItem.item.add(item);

                    //*****************ITEM PEDIDO: SUBTOTAL********************
                    item = msg.new ShowScreenViewContent();
                    item.idView = "item_txt_total";
                    item.classTypeView = "android.widget.TextView";
                    item.viewContentType = "java.lang.CharSequence";
                    item.viewContent = Formatos.FormateaDecimal(p.get_total(), FORMATO_DECIMAL_SECUND);

                    listItem.item.add(item);

                    viewContentList.add(listItem);
                    //fin item
                }

                item = msg.new ShowScreenViewContent();
                item.idView = "lst_pedido";
                item.classTypeView = "android.widget.ListView";
                item.viewContentType = "pedido_item";
                item.setListContent(viewContentList);

                request.data.add(item);
                //

                msg.content = request.toString();

                //mService.showScreen(msg);

                // Create and send a message to the service, using a supported 'what' value
                android.os.Message _msg = android.os.Message.obtain(null, ServerDisplayService.MSG_SHOW_SCREEN);

                Bundle data = new Bundle();
                data.putParcelable("d", msg);

                _msg.setData(data);

            /*android.os.Message _msg = android.os.Message.obtain(null, ServerDisplayService.MSG_SHOW_TEXT);

            Bundle data = new Bundle();
            data.putString("text", ((TextView) findViewById(R.id.pedido_txt_total_monto)).getText().toString());

            _msg.setData(data);*/

                try {
                    mService.send(_msg);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void CloseTicketInDisplay() {

        if (DISPLAY_ACTIVATED) {

            if (isBound && mService != null) {
                // Create and send a message to the service, using a supported 'what' value

                android.os.Message _msg = android.os.Message.obtain(null, ServerDisplayService.MSG_CLOSE_SCREEN);

                try {
                    mService.send(_msg);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //endregion

    @Override
    public void onStop() {
        super.onStop();

        if (DISPLAY_ACTIVATED) {
            // Unbind from the service
            if (isBound) {
                unbindService(mConnection);
                mService = null;
                isBound = false;
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();

        config = new Config(this);

        //this.txtFecha_panel.setText(Formatos.FormateaDate(new Date(), Formatos.DateFormat));
        //this.txtHora_panel.setText(Formatos.FormateaDate(new Date(), Formatos.ShortTimeFormat));

        //leemos las ventas
        LeerVentas();

        //establecemos permisos segun perfil (productos)
        //ValidaSeguridadPerfil();

        //si NO se inicio scan y no hay un pedido activo, reiniciamos pantalla
        if (!scanIniciado && pedido == null) {
            EstadoPanelEdicion(false);
            accionClickNuevo.onClick(null);
        } else {
            scanIniciado = false;
        }

    }

    /*
    @Override
    public void onStop() {
        super.onStop();
        if (_broadcastReceiver != null)
            unregisterReceiver(_broadcastReceiver);

    }
    */


    @Override
    protected void onDestroy() {
        if (_printer != null) {
            _printer.end();
            _printer = null;
        }

        if (_lector != null)
            _lector.end();

        if (_scanner != null) {
            _scanner.end();
        }

        Timer_Cancel();

        super.onDestroy();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        try {

            super.onActivityResult(requestCode, resultCode, intent);

            if (scanIniciado) {
                if (intent != null) {

                    //obtenemos el resultado del scan
                    String scanContent = ScanIntegrator.parseActivityResult(requestCode, resultCode, intent);
                    //String scanContent = "555351313";
                    //revisamos si es un valor valido
                    if (scanContent != null) {
                        //lo asignamos
                        this.txtValor_panel.setText(scanContent);
                        this.txtValor_panel.setVisibility(View.VISIBLE);
                        this.txtFuncion_panel.setVisibility(View.VISIBLE);

                        this.txtComentario_panel.setVisibility(View.VISIBLE);

                        IngresaProductoBySKU(scanContent, false);

                        funcion_activa = "";
                    } else {

                        //datos invalidos o scan cancelado
                        Toast.makeText(MainActivity.this,
                                "No se recibieron datos del scan!", Toast.LENGTH_SHORT).show();

                        this.txtFuncion_panel.setText("");
                        this.txtFuncion_panel.setVisibility(View.INVISIBLE);

                    }

                } else {

                    //datos invalidos o scan cancelado
                    Toast.makeText(MainActivity.this,
                            "No se recibieron datos del scan!", Toast.LENGTH_SHORT).show();

                    this.txtFuncion_panel.setText("");
                    this.txtFuncion_panel.setVisibility(View.INVISIBLE);
                }

                scanIniciado = false;
            }
        } catch (Exception ex) {
            Toast.makeText(MainActivity.this,
                    "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {

        if (event.isPrintingKey() || keyCode == KeyEvent.KEYCODE_ENTER) {

            if (keyCode == KeyEvent.KEYCODE_ENTER) {

                //String codigoIngresado = txtValor_panel.getText().toString();

                IngresaProductoBySKU(codigoIngresado, false);

                codigoIngresado = "";

            } else {
                codigoIngresado += String.valueOf(event.getDisplayLabel());

                //AgregarTextoPanel(String.valueOf(event.getDisplayLabel()));
                //AgregarTextoPanel(KeyEvent.keyCodeToString(keyCode));
            }

            return true;

        } else {
            return super.onKeyUp(keyCode, event);
        }
    }

    private void IngresaProductoBySKU(String codigoProducto, boolean validaSkuSiNoExiste) {
        try {

            if (primeroLimpiarPanel) {
                primeroLimpiarPanel = false;
                txtValor_panel.setText("");
            }

            if (pedido == null) {
                //Toast.makeText(getApplicationContext(), getString(R.string.error_venta_no_activa), Toast.LENGTH_SHORT).show();
                ShowMessage(getString(R.string.title_sin_venta), getString(R.string.error_venta_no_activa));
            } else {

                //si encuentra un producto con el codigo lo agrega al pedido, sino lo carga como generico
                if (!ObtenerProductoBySKU(codigoProducto)) {

                    Toast.makeText(this, "El product escaneado no existe. Por favor ingresarlo por la Web para poder usarlo en ventas", Toast.LENGTH_SHORT).show();

                }

                txtValor_panel.setText("");
            }
        } catch (Exception ex) {
            //Toast.makeText(this, "Error:" + ex.getMessage(), Toast.LENGTH_SHORT).show();
            ShowMessage("Error", "Error: " + ex.getMessage());

        }
    }

    /*
        private void ValidaSeguridadPerfil() {
            try {

                if (_user.getId_perfil() == 1) { //si es ADMIN agregamos un producto
                    //leemos los productos
                    LeerProductos(true);
                } else {
                    LeerProductos(false);
                }

            } catch (Exception ex) {
                Toast.makeText(this, ex.getMessage().toString(), Toast.LENGTH_LONG).show();
            }
        }

        private void PrepararMenu() {
            // preparing list data
            prepareListData();

            listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);

            // setting list adapter
            expListView.setAdapter(listAdapter);

            expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

                @Override
                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                    try {
                        String item = listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition);

                        Intent intent = null;

                        if (item == "Clientes") {
                            intent = new Intent(v.getContext(), product_list_activity.class);
                            expListView.setVisibility(View.INVISIBLE);
                            dummy_view.setVisibility(View.INVISIBLE);
                        }

                        if (item == "Productos") {
                            intent = new Intent(v.getContext(), product_list_activity.class);
                            expListView.setVisibility(View.INVISIBLE);
                            dummy_view.setVisibility(View.INVISIBLE);
                        }

                        if (item == "Usuarios") {
                            intent = new Intent(v.getContext(), product_list_activity.class);
                        }

                        startActivityForResult(intent, 0);

                        return true;
                    } catch (Exception ex) {
                        Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG);

                    } finally {
                        return true;
                    }
                }
            }
            );

        }*/
    public void DescartarVentaEnCurso(DialogInterface.OnClickListener accionPositiva) {
        if (pedido.size() > 0) {
            new ConfirmDialog(MainActivity.this, "Eliminar venta", "La venta en curso sera descartada. Desea continuar?")
                    .set_positiveButtonText("SI")
                    .set_positiveButtonAction(accionPositiva)
                    .set_negativeButtonText("NO")
                    .show();
        } else {
            accionPositiva.onClick(null, 0);
        }
    }

    public void NuevaVenta() {

        CloseTicketInDisplay();

        SalirCheckout(); //por si estaba abierto

        txtTotalPedido.setText(FORMATO_SIGNO_MONEDA + " 0");
        pedido = new ArrayList<Pedido>();
        pedido_item_edit = null;
        lblNuevoPedido.setVisibility(View.VISIBLE);
        lblNuevoPedido.setText("Nueva venta");
        lblPedidoRegistrado.setVisibility(View.GONE);
        lstPedido.setVisibility(View.GONE);
        btnAnular.setVisibility(View.GONE);
        btnImprimir.setVisibility(View.GONE);
        this.txtValor_panel.setTextSize(sizeText);
        findViewById(R.id.main_pad).setVisibility(View.VISIBLE);

        EstadoPanelEdicion(false);

        ActualizarPedido();

        txtNroPedido.setText(Formatos.FormateaDate(new Date(), Formatos.ShortTimeFormat) + " - *");
        txtNroPedido.setVisibility(View.VISIBLE);

        //limpiamos lo que haya en el panel
        //txtValor_panel.setText("");
    }

    public void VentaHistorica(int position) throws Exception {

        SalirCheckout(); //por si estaba abierto

        //limpiamos lo que haya en el panel
        txtValor_panel.setText("");

        ventaHistEdit = ventasList.get(position);

        //txtNroPedido.setText(ventaHistEdit.getHora().substring(0, 5) + " - " + ventaHistEdit.getNroVenta());
        String textAnulada = ventaHistEdit.isAnulada() ? "&nbsp;&nbsp;&nbsp;&nbsp;<font color='red'>ANULADA</font>" : "";
        txtNroPedido.setText(Html.fromHtml(ventaHistEdit.getHora().substring(0, 5) + " - " + ventaHistEdit.getNroVenta() + textAnulada));
        txtNroPedido.setVisibility(View.VISIBLE);

        lblPedidoRegistrado.setVisibility(View.VISIBLE);
        String type = "";
        if (ventaHistEdit instanceof Venta)
            type = "VENTA";
        else
            type = getStringFromResource(config.PAIS, "btn_presupuesto_min");

        lblPedidoRegistrado.setText(type + " DEL " + ventaHistEdit.getFecha());
        btnAnular.setVisibility(ventaHistEdit.isAnulada() ? View.GONE : View.VISIBLE);
        btnImprimir.setVisibility(View.VISIBLE);
        btnImprimir.setText("REIMPRIMIR");

        //si la venta llega a tener un VALE asociado, ocultamos la opcion de REIMPRIMIR
        /*if(ventaHistEdit instanceof Venta)
            if(((Venta)ventaHistEdit).tieneValeAsociado())
                btnImprimir.setVisibility(View.INVISIBLE);*/

        findViewById(R.id.main_pad).setVisibility(View.GONE);

        List<Pedido> items = ObtieneDetalleVta(ventaHistEdit);

        //agregamos linea al detalle
        lblNuevoPedido.setVisibility(View.GONE);
        lstPedido.setAdapter(new PedidoAdapter(getBaseContext(), items, FORMATO_DECIMAL_SECUND));
        lstPedido.setVisibility(View.VISIBLE);

        if (ventaHistEdit.getMedio_pago().equals("CTACTE")) {
            List<CuentaCte> _ctacte = new CuentaCteDao(getContentResolver()).list("tipoVta = '" + ventaHistEdit.getDbType() + "' and id_venta =" + String.valueOf(ventaHistEdit.getId()), null, null);
            if (_ctacte.size() > 0)
                ventaHistEdit.setCliente(_ctacte.get(0).getCliente());
        }
        GeneraTicketVenta(ventaHistEdit, items, ventaHistEdit.isAnulada() ? TEXT_DUPLICADO_ANULADO : TEXT_DUPLICADO);
        //blanqueamos el pedido actual
        EstadoPanelEdicion(false);
        pedido = null;
        pedido_item_edit = null;

        ActualizarPedido();
        ((TextView) findViewById(R.id.pedido_txt_total_cant)).setText(String.valueOf(ventaHistEdit.getCantidad()));
        ((TextView) findViewById(R.id.pedido_txt_total_monto)).setText(Formatos.FormateaDecimal(ventaHistEdit.getTotal(), FORMATO_DECIMAL));
        txtTotalPedido.setText(Formatos.FormateaDecimal(ventaHistEdit.getTotal(), FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA));

        lstVentas.setVisibility(View.INVISIBLE);
        dummy_view.setVisibility(View.INVISIBLE);
    }

    public void iniciarAnulacionVenta(final VentaAbstract venta, final AnulacionVentaListener anulacionListener) {
        if (venta.isAnulada()) {
            anulacionListener.onAnulacionError("Error", "Venta ya anulada");
            return;
        }

        if (venta.getType(config.PAIS).equals("VENTA")) {
            VentaDao _vtaDao = new VentaDao(getContentResolver());
            Venta vta = _vtaDao.find(venta.getId());

            if (vta.tieneValeAsociado()) {
                //Toast.makeText(MainActivity.this, "Error: no es posible anular un VALE ", Toast.LENGTH_SHORT).show();
                //ShowMessage("Error", "Error: no es posible anular un VALE ");
                AnulacionReq anulacionVale = new AnulacionReq(config, config.SII_MERCHANT_ID, ValeUtils.GetUserID(config.SII_TERMINAL_ID, getContentResolver()), vta.getFc_folio());
                AnulacionTask anulacionTask = new AnulacionTask(MainActivity.this, new AnulacionListener() {
                    @Override
                    public void onAnulacionCompletada() {
                        //anulacion de VALE ok
                        AnularVenta(venta, anulacionListener);
                    }

                    @Override
                    public void onAnulacionError() {
                        anulacionListener.onAnulacionError("Error", "Error anulacion vale");
                    }
                });
                anulacionTask.execute(anulacionVale);
                return;
            }
        }

        //anulacion de VENTA sin VALE o de COMPROOBANTES
        AnularVenta(venta, anulacionListener);

    }

    public void AnularVenta(VentaAbstract venta, AnulacionVentaListener anulacionListener) {

        if (venta.getType(config.PAIS).equals("VENTA")) {

            try {//0 - definimos los DAO
                VentaDao _vtaDao = new VentaDao(getContentResolver());
                VentaAnuladaDetalleDao vtaAnuladaDetDao = new VentaAnuladaDetalleDao(getContentResolver());
                ProductDao productDao = new ProductDao(getContentResolver(), config.SYNC_STOCK ? 1 : 0);
                HistMovStockDao histStockDao = new HistMovStockDao(getContentResolver());
                CuentaCteDao cuentaCteDao = new CuentaCteDao(getContentResolver());

                //1 - obtenemos la venta
                Venta vta = _vtaDao.find(venta.getId());

                //2 - grabamos la anulacion de la venta
                VentaAnulada vtaAnulada = new VentaAnulada(vta);
                boolean result = new VentaAnuladaDao(getContentResolver()).save(vtaAnulada);

                if (!result)
                    throw new Exception("No se ha podido grabar la anulación tabla VENTA_ANULADA");

                //3 - anulamos los items
                vta.addDetalles(getContentResolver());

                for (VentaDetalle det : vta.getDetalles()) {
                    //3.1 - grabamos la anulacion de los items
                    result = vtaAnuladaDetDao.save(new VentaAnuladaDetalle(vtaAnulada.getId(), det));

                    if (result) {

                        if (det.getProducto() != null) {

                            //3.2 - actualizamos el stock de los productos
                            Product p = det.getProducto();

                            //3.3 - grabamos en el historico de stock las devoluciones
                            AnularVentaItem(p, det.getCantidad(), "ANULACION DE VENTA #" + vta.getNroVenta());

                        }
                    } else
                        throw new Exception("No se ha podido grabar los detalles de la anulación tabla VENTA_ANULADA_DETALLE");

                }

                if (!vta.getMedio_pago().equals("CTACTE")) {
                    //4 - grabamos la salida de CAJA
                    Caja.RegistrarMovimientoCaja(MainActivity.this, TipoMovimientoCaja.RETIRO, vta.getTotal(), vta.getMedio_pago(), "ANULACION DE VENTA #" + vta.getNroVenta(), vta.getId(), vta.getDbType());
                } else {
                    //4 - grabamos la devolucion de la DEUDA
                    //obtenemos la deuda generada para conocer el cliente
                    CuentaCte _deuda = cuentaCteDao.first(String.format("id_venta = %d AND tipoVta = 'VTA'", vta.getId()), null, null);

                    //registramos en CTACTE
                    if (_deuda != null) {
                        CuentaCte cuentaCte = new CuentaCte();
                        cuentaCte.setMonto(vta.getTotal());
                        cuentaCte.setTipoMov(CuentaCte.TipoMovimientoCtaCte.COBRO);
                        cuentaCte.setFechaHora(new Date());
                        cuentaCte.setCliente(_deuda.getCliente());
                        cuentaCte.setIdVenta(vta.getId());
                        cuentaCte.setTipoVenta(vta.getDbType());
                        cuentaCte.setObservacion("ANULACION DE VENTA #" + vta.getNroVenta());

                        new CuentaCteDao(getContentResolver()).save(cuentaCte);
                    }
                }

                //5 - actualizamos el estado de la VENTA
                vta.setSync("N");
                vta.setAnulada(true);

                _vtaDao.saveOrUpdate(vta);

                //6 - grabamos log de anulacion
                Logger.RegistrarEvento(getContentResolver(), "i", "ANULACION DE VENTA", "#" + vta.getNroVenta(), "");

                //Toast.makeText(MainActivity.this, "Venta anulada correctamente", Toast.LENGTH_SHORT).show();
                ShowMessage("Anulacion", "Venta anulada correctamente");

                if (vta.tieneValeAsociado()) {
                    ValeAnuladoTicketBuilder valeTicketBuilder = new ValeAnuladoTicketBuilder(MainActivity.this, config);
                    valeTicketBuilder.LoadVenta(vta, FORMATO_DECIMAL_ID);
                    valeTicketBuilder.Print(config.PRINTER_MODEL);
                } else {
                    ImprimirVenta(vta, TEXT_DUPLICADO_ANULADO);
                }

                anulacionListener.onAnulacionOk();

            } catch (Exception ex) {
                //Toast.makeText(MainActivity.this, "Error al intentar anular venta: " + ex.toString(), Toast.LENGTH_SHORT).show();
                //ShowMessage("Anulacion", "Error al intentar anular venta: " + ex.toString());
                anulacionListener.onAnulacionError("Anulacion", "Error al intentar anular venta: " + ex.toString());
            }
        } else {

            try {//0 - definimos los DAO
                ComprobanteDao _comprobDao = new ComprobanteDao(getContentResolver());
                VentaAnuladaDetalleDao vtaAnuladaDetDao = new VentaAnuladaDetalleDao(getContentResolver());
                ProductDao productDao = new ProductDao(getContentResolver(), config.SYNC_STOCK ? 1 : 0);
                HistMovStockDao histStockDao = new HistMovStockDao(getContentResolver());
                CuentaCteDao cuentaCteDao = new CuentaCteDao(getContentResolver());

                //1 - obtenemos la venta
                Comprobante comprob = _comprobDao.find(venta.getId());
                String observacion = getStringFromResource(config.PAIS, "presup_anulado") + " #" + comprob.getNroVenta();

                //2 - grabamos la anulacion de la venta
                VentaAnulada vtaAnulada = new VentaAnulada(comprob);
                boolean result = new VentaAnuladaDao(getContentResolver()).save(vtaAnulada);

                if (!result)
                    throw new Exception("No se ha podido grabar la anulación tabla VENTA_ANULADA");

                //3 - anulamos los items
                comprob.addDetalles(getContentResolver());

                for (ComprobanteDetalle det : comprob.getDetalles()) {
                    //3.1 - grabamos la anulacion de los items
                    result = vtaAnuladaDetDao.save(new VentaAnuladaDetalle(vtaAnulada.getId(), det));

                    if (result) {

                        if (det.getProducto() != null) {

                            //3.2 - actualizamos el stock de los productos
                            Product p = det.getProducto();

                            //3.3 - grabamos en el historico de stock las devoluciones
                            AnularVentaItem(p, det.getCantidad(), observacion);

                        }
                    } else
                        throw new Exception("No se ha podido grabar los detalles de la anulación tabla VENTA_ANULADA_DETALLE");

                }

                if (!comprob.getMedio_pago().equals("CTACTE")) {
                    //4 - grabamos la salida de CAJA
                    Caja.RegistrarMovimientoCaja(MainActivity.this, TipoMovimientoCaja.RETIRO, comprob.getTotal(), comprob.getMedio_pago(), observacion, comprob.getId(), comprob.getDbType());
                } else {
                    //4 - grabamos la devolucion de la DEUDA
                    //obtenemos la deuda generada para conocer el cliente
                    CuentaCte _deuda = cuentaCteDao.first(String.format("id_venta = %d AND tipoVta = 'CPB'", comprob.getId()), null, null);

                    //registramos en CTACTE
                    if (_deuda != null) {
                        CuentaCte cuentaCte = new CuentaCte();
                        cuentaCte.setMonto(comprob.getTotal());
                        cuentaCte.setTipoMov(CuentaCte.TipoMovimientoCtaCte.COBRO);
                        cuentaCte.setFechaHora(new Date());
                        cuentaCte.setCliente(_deuda.getCliente());
                        cuentaCte.setIdVenta(comprob.getId());
                        cuentaCte.setTipoVenta(comprob.getDbType());
                        cuentaCte.setObservacion(observacion);

                        new CuentaCteDao(getContentResolver()).save(cuentaCte);
                    }
                }

                //5 - actualizamos el estado de la VENTA
                comprob.setSync("N");
                comprob.setAnulada(true);

                _comprobDao.saveOrUpdate(comprob);

                //6 - grabamos log de anulacion
                Logger.RegistrarEvento(getContentResolver(), "i", getStringFromResource(config.PAIS, "presup_anulado"), "#" + comprob.getNroVenta(), "");

                //Toast.makeText(MainActivity.this, getStringFromResource(config.PAIS, "presup_anulado_toast"), Toast.LENGTH_SHORT).show();
                ShowMessage("Anulacion", getStringFromResource(config.PAIS, "presup_anulado_toast"));

                ImprimirVenta(comprob, TEXT_DUPLICADO_ANULADO);

                anulacionListener.onAnulacionOk();

            } catch (Exception ex) {
                //Toast.makeText(MainActivity.this, getStringFromResource(config.PAIS, "presup_error_registro_toast") + " " + ex.toString(), Toast.LENGTH_SHORT).show();
                //ShowMessage("Anulacion", getStringFromResource(config.PAIS, "presup_error_registro_toast") + ": " + ex.toString());
                anulacionListener.onAnulacionError("Anulacion", getStringFromResource(config.PAIS, "presup_error_registro_toast") + ": " + ex.toString());
            }
        }

    }

    private void AnularVentaItem(Product item, Double cantidad, String observacion) {
        item.setStock(item.getStock() + cantidad);

        new ProductDao(getContentResolver()).saveOrUpdate(item);

        //3.3 - grabamos en el historico de stock las devoluciones
        HistMovStock s = new HistMovStock();
        s.setFecha(new Date());
        s.setUsuario(_user.getLogin());
        s.setObservacion(observacion);
        s.setStock(((Double) item.getStock()).intValue());
        s.setCantidad((cantidad).intValue());
        s.setMonto(0);
        s.setOperacion("I");
        s.setProductoid(item.getId());

        new HistMovStockDao(getContentResolver()).save(s);
    }

    private void Init_AsignarEventos() {

        /*btnCalc.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_MAIN);
                i.setComponent(new ComponentName("com.android.calculator2", "com.android.calculator2.Calculator"));


                try {
                    MainActivity.this.startActivity(i);
                } catch (ActivityNotFoundException noSuchActivity) {
                    // si el calculator intent no está registrado, probamos con un nombre alternativo
                    Intent i2 = new Intent();
                    i2.setAction(Intent.ACTION_MAIN);
                    i2.setComponent(new ComponentName("com.sec.android.app.popupcalculator", "com.sec.android.app.popupcalculator.Calculator"));
                    try {
                        MainActivity.this.startActivity(i2);
                    } catch (ActivityNotFoundException noSuchActivity2) {
                        // calculator intent not found
                        Toast.makeText(MainActivity.this, "Acceso a calculadora no encontrado", Toast.LENGTH_LONG).show();
                    }
                }

            }
        });*/

        /*this.btnExit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.finish();
            }
        });*/

        this.btnBackspace.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                //si habia que limpiar el panel, lo hacemos y volvemos
                if (primeroLimpiarPanel) {
                    AgregarTextoPanel("");
                    return;
                }

                String texto = txtValor_panel.getText().toString();
                if (!TextUtils.isEmpty(texto)) {
                    txtValor_panel.setText(texto.substring(0, texto.length() - 1));
                }
            }
        });

        this.btnBackspace.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                //si habia que limpiar el panel, lo hacemos y volvemos
                if (primeroLimpiarPanel) {
                    AgregarTextoPanel("");
                    return false;
                }

                txtValor_panel.setText("");
                return false;
            }
        });


//        this.btnPrecio.setOnClickListener(accionIngresoPrecio);

        this.btnCantidad.setOnClickListener(accionCantidadNormal);

        this.btnEnter.setOnClickListener(accionClickEnter);

        (findViewById(R.id.main_medio_pago)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //dejamos el evento click seteado en la vista para que no se vaya al click del dummyview
            }
        });

        ((RadioGroup) findViewById(R.id.medio_pago_medios)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.medio_pago_rbt_tc) {
                    findViewById(R.id.medio_pago_lbl_deslice).setVisibility(View.VISIBLE);
                    btnConfirmar_mp.setEnabled(false);

                    HabilitarLector();
                } else {
                    findViewById(R.id.medio_pago_lbl_deslice).setVisibility(View.GONE);
                    btnConfirmar_mp.setEnabled(true);
                }
            }
        });

        this.btnConfirmar_mp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                OcultarPopupMedioPago();
                CerrarVenta();
                LeerVentas();
            }
        });

        this.btnCancelar_mp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                OcultarPopupMedioPago();
            }
        });

        this.btn0.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("0");
            }
        });

        this.btn1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("1");
            }
        });

        this.btn2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("2");
            }
        });

        this.btn3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("3");
            }
        });

        this.btn4.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("4");
            }
        });

        this.btn5.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("5");
            }
        });

        this.btn6.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("6");
            }
        });

        this.btn7.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("7");
            }
        });

        this.btn8.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("8");
            }
        });

        this.btn9.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("9");
            }
        });

        this.btnPunto.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel(String.valueOf(FORMATO_DECIMAL.getDecimalFormatSymbols().getDecimalSeparator()));
            }
        });

//        this.btnProductosPropios.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if (primeroLimpiarPanel) {
//                    primeroLimpiarPanel = false;
//                    txtValor_panel.setText("");
//                }
//
//                if (pedido == null) {
//                    //Toast.makeText(getApplicationContext(), getString(R.string.error_venta_no_activa), Toast.LENGTH_SHORT).show();
//                    ShowMessage(getString(R.string.title_sin_venta), getString(R.string.error_venta_no_activa));
//                } else {
//                    IniciarFragmentProducto(1);
//                }
//            }
//        });

        this.btnCatalogo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if (primeroLimpiarPanel) {
                    primeroLimpiarPanel = false;
                    txtValor_panel.setText("");
                }

                if (pedido == null) {
                    //Toast.makeText(getApplicationContext(), getString(R.string.error_venta_no_activa), Toast.LENGTH_SHORT).show();
                    ShowMessage(getString(R.string.title_sin_venta), getString(R.string.error_venta_no_activa));

                } else {
                    IniciarFragmentProducto(2);
                }
            }
        });

//        this.btnCombos.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (primeroLimpiarPanel) {
//                    primeroLimpiarPanel = false;
//                    txtValor_panel.setText("");
//                }
//
//                if (pedido == null) {
//                    //Toast.makeText(getApplicationContext(), getString(R.string.error_venta_no_activa), Toast.LENGTH_SHORT).show();
//                    ShowMessage(getString(R.string.title_sin_venta), getString(R.string.error_venta_no_activa));
//
//                } else {
//                    IniciarFragmentProducto(0);
//                }
//            }
//        });

        /*this.btnEditar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (pedido == null) {
                    Toast.makeText(getApplicationContext(), getString(R.string.error_venta_no_activa), Toast.LENGTH_SHORT).show();
                } else if (pedido.size() == 0) {
                    Toast.makeText(getApplicationContext(), getString(R.string.error_venta_sin_productos), Toast.LENGTH_SHORT).show();
                } else {
                    //agregamos la ultima linea
                    EditarLineaPanel(pedido.get(pedido.size() - 1));
                }
            }
        });*/

        this.btnMenu.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if (lstVentas.getVisibility() != View.VISIBLE) {
                    dummy_view.setVisibility(View.VISIBLE);
                    //dummy_view.setBackgroundColor(getResources().getColor(R.color.boton_rojo));
                    //expListView.setVisibility(View.VISIBLE);

                    lstVentas.setVisibility(View.VISIBLE);
                } else {
                    dummy_view.setBackgroundColor(Color.TRANSPARENT);
                    dummy_view.setVisibility(View.INVISIBLE);

                    lstVentas.setVisibility(View.INVISIBLE);
                }

            }
        });

        this.btnCobrar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Double total = getTotalVenta();

                //info del cliente : VALORES DEFAULT
                double disponible = cliente_selected.getLimite(); //con limite disponible
                CuentaCteTotal _total = new CuentaCteTotalesDao(getContentResolver()).find(cliente_selected.getId());
                if (_total != null) { //si hay registros del cliente
                    disponible = _total.getDisponible();
                }

                if (total > disponible)
                    //Toast.makeText(MainActivity.this, "Error: el cliente no posee saldo disponible suficiente", Toast.LENGTH_SHORT).show();
                    ShowMessage("Cuenta Cliente", "Error: el cliente no posee saldo disponible suficiente");
                else
                    onPagoFinalizado(total, "CTACTE", 0d);
            }
        });

        this.btnVenta.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentCheckoutInstance().IniciarFormaPagoSelected();

                //cambiamos el valor de esta variable
                registarComprobante = false;

            }
        });

        this.btnComprobante.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                //cambiamos el valor de esta variable
                //registarComprobante = !registarComprobante;
                registarComprobante = true;

                getFragmentCheckoutInstance().IniciarFormaPagoSelected();


                //cambiamos el aspecto a un boton pressed
                /*if (registarComprobante) {
                    btnComprobante.setTextColor(getResources().getColor(android.R.color.black));
                    btnComprobante.setBackgroundResource(R.drawable.backrounded_orange);
                } else {
                    btnComprobante.setTextColor(getResources().getColor(R.color.blanco));
                    btnComprobante.setBackgroundResource(R.drawable.backrounded_grey);
                }*/
            }
        });

        this.btnAnular.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                //vamos a solicitar la clave de admin para anular la venta
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                // Get the layout inflater
                LayoutInflater inflater = MainActivity.this.getLayoutInflater();

                // Inflate and set the layout for the dialog
                // Pass null as the parent view because its going in the dialog layout
                View vw = inflater.inflate(R.layout.dialog_anular_venta, null);
                ((TextView) vw.findViewById(R.id.dialog_anular_venta_txtNroVta)).setText("#" + ventaHistEdit.getNroVenta());
                ((TextView) vw.findViewById(R.id.dialog_anular_venta_txtFecha)).setText(ventaHistEdit.getFechaHoraCorta());
                ((TextView) vw.findViewById(R.id.dialog_anular_venta_txtTotal)).setText(Formatos.FormateaDecimal(ventaHistEdit.getTotal(), FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA));
                builder.setView(vw)
                        // Add action buttons
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                try {

                                    String passIngresada = ((EditText) ((AlertDialog) dialog).findViewById(R.id.dialog_anular_venta_password)).getText().toString();
                                    String passAdmin = UsuarioHelper.ObtenerUsuarioById(getContentResolver(), 1).getPassword();

                                    if (passIngresada.equals(passAdmin)) {

                                        iniciarAnulacionVenta(ventaHistEdit, new AnulacionVentaListener() {
                                            @Override
                                            public void onAnulacionOk() {
                                                //recargamos la lista de ventas historicas
                                                LeerVentas();

                                                //limpiamos la pantalla
                                                accionClickNuevo.onClick(null);
                                            }

                                            @Override
                                            public void onAnulacionError(String title, String message) {
                                                ShowMessage(title, message);
                                            }
                                        });

                                    } else {
                                        Toast.makeText(MainActivity.this, "Clave ingresada erronea", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (Exception e) {
                                    Toast.makeText(MainActivity.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }
                        })
                        .setNegativeButton("Cancelar", null);

                AlertDialog dialog = builder.create();

                dialog.setTitle("Anular Venta");
                dialog.setCanceledOnTouchOutside(false);
                dialog.setCancelable(false);
                dialog.setOnCancelListener(null);

                dialog.show();
            }
        });

        this.btnNuevo.setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (primeroLimpiarPanel) {
                            primeroLimpiarPanel = false;
                            txtValor_panel.setText("");
                        }

                        accionClickNuevo.onClick(v);
                    }
                }
        );

        this.btnEliminarPedido.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if (primeroLimpiarPanel) {
                    primeroLimpiarPanel = false;
                    txtValor_panel.setText("");
                }

                if (pedido == null) {
                    //Toast.makeText(getApplicationContext(), getString(R.string.error_venta_no_activa), Toast.LENGTH_SHORT).show();
                    ShowMessage("Eliminar", getString(R.string.error_venta_no_activa));
                } else if (pedido_item_edit != null) {//si hay un item en edicion, eliminamos dicho item

                    pedido.remove(pedido_item_edit);
                    ActualizarPedido();
                    txtWelcome_panel.setVisibility(View.INVISIBLE);
                    txtComentario_panel.setVisibility(View.VISIBLE);
                    txtComentario_panel.setText("Eliminada línea " + Formatos.FormateaDecimal(pedido_item_edit.get_cantidad(), FORMATO_DECIMAL_SECUND) + (pedido_item_edit.esItemPesable() ? "K x " : " x ") + pedido_item_edit.get_descripcion());
                    pedido_item_edit = null;

                } else { //eliminamos el pedido completo sino

                    accionClickNuevo.onClick(null);

                    /*
                    DescartarVentaEnCurso(new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                            SalirCheckout();

                            txtTotalPedido.setText(FORMATO_SIGNO_MONEDA + " 0");
                            pedido = null;
                            pedido_item_edit = null;
                            lblNuevoPedido.setVisibility(View.VISIBLE);
                            lblNuevoPedido.setText("Venta eliminada");
                            lblPedidoRegistrado.setVisibility(View.GONE);
                            lstPedido.setVisibility(View.GONE);
                            btnAnular.setVisibility(View.GONE);
                            btnImprimir.setVisibility(View.GONE);
                            findViewById(R.id.main_pad).setVisibility(View.VISIBLE);
                            EstadoPanelEdicion(false);
                            ActualizarPedido();

                            txtNroPedido.setVisibility(View.GONE);

                            txtComentario_panel.setText("Cree un nueva venta");

                            //limpiamos lo que haya en el panel
                            txtValor_panel.setText("");

                        }
                    });
                    */
                }
            }
        });
/*
        this.lstProductos.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //limpiamos lo que haya en el panel
                txtValor_panel.setText("");

                ProductoRow item = productosList.get(position);

                if (item.get_id() == 0) {
                    startActivityForResult(new Intent(MainActivity.this, product_list_activity.class), 0);
                } else {
                    if (pedido == null) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_venta_no_activa), Toast.LENGTH_SHORT).show();
                    } else {
                        AgregarProductoAlPedido(
                                item.get_id(),
                                item.get_nombre(),
                                1,
                                item.get_precio(),
                                item.get_precio()
                        );
                    }
                }
                lstProductos.setVisibility(View.INVISIBLE);
                dummy_view.setVisibility(View.INVISIBLE);
            }
        });
*/

        this.lstPedido.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //view.setBackgroundColor(getResources().getColor(android.R.color.holo_purple));

                //limpiamos lo que haya en el panel
                txtValor_panel.setText("");

                if (pedido != null) {
                    EditarLineaPanel(pedido.get(position));
                } else {
                    lstPedido.clearChoices();
                }
                //Toast.makeText(getApplicationContext(), pedido.get(position).get_descripcion(), Toast.LENGTH_LONG).show();
            }
        });

        this.lstVentas.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    final int _position = position;

                    if (pedido != null) {
                        DescartarVentaEnCurso(new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    VentaHistorica(_position);
                                } catch (Exception ex) {
                                    Toast.makeText(getApplicationContext(), "Error:" + ex.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                    } else {
                        VentaHistorica(_position);
                    }
                } catch (Exception ex) {
                    Toast.makeText(getApplicationContext(), "Error:" + ex.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });

        this.btnValidar_panel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                EstadoPanelEdicion(false);
                lstPedido.clearChoices();
            }
        });

        this.btnEliminar_panel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                EstadoPanelEdicion(false);
                pedido.remove(pedido_item_edit);
                ActualizarPedido();
                txtWelcome_panel.setVisibility(View.INVISIBLE);
                txtComentario_panel.setVisibility(View.VISIBLE);
                txtComentario_panel.setText("Eliminada línea " + Formatos.FormateaDecimal(pedido_item_edit.get_cantidad(), FORMATO_DECIMAL_SECUND) + (pedido_item_edit.esItemPesable() ? "K x " : " x ") + pedido_item_edit.get_descripcion());
                pedido_item_edit = null;

            }
        });

        /*this.btnProductUndo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if (last_producto != null && pedido_item_edit != null) {

                    if (pedido_item_edit.get_cantidad() > 1) {
                        pedido_item_edit.set_cantidad(pedido_item_edit.get_cantidad() - 1);
                        pedido_item_edit.actualiza_total(PREF_AJUSTE_SENCILLO);
                    } else {
                        pedido.remove(pedido_item_edit);
                    }

                    ActualizarPedido();

                    getFragmentProductoInstance().decreaseProduct(last_producto);

                    last_producto = null;

                } else {
                    Toast.makeText(MainActivity.this, "No hay acción para deshacer", Toast.LENGTH_SHORT).show();
                }
            }
        });*/

        this.dummy_view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                OcultarPopupMedioPago();

                //showHideFragment(true);

                //lstProductos.setVisibility(View.INVISIBLE);
                lstVentas.setVisibility(View.INVISIBLE);
            }
        });

        this.dummy_view_b.setOnClickListener(accionCloseFragmentProductos);

        this.btnImprimir.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                try {

                    if (PREF_EMITIR_VALE && ticket == null) {
                        if (ventaCurrent != null)
                            PrintVale(ventaCurrent);
                    } else {

                        final boolean reimpresion = !ticket.get_header().equalsIgnoreCase(TEXT_ORIGINAL);

                        if (_printer == null)
                            _printer = Printer.getInstance(config.PRINTER_MODEL, MainActivity.this);

                        _printer.delayActivated = true;
                        _printer.printerCallback = new Printer.PrinterCallback() {
                            @Override
                            public void onPrinterReady() {
                                ticket.Print(_printer);

                                if (!reimpresion && _printer.isCashDrawerActivated()) {
                                    _printer.openCashDrawer();
                                }

                                //TODO: Recordar...esto se habia agregado para la impresion de vales, para probar
                                //si cerrando el port al finalizar, mejoraba los problemas de impresion al emitir varios seguidos
                                //causa problemas con POWA, al cerrar la printer mientras está imprimiendo...
                                //lo comentamos por ahora, en la version 1.58 del PDV
                                _printer.end();
                                _printer = null;
                            }
                        };

                        //iniciamos la printer
                        if (!_printer.initialize()) {
                            Toast.makeText(MainActivity.this, "Error al inicializar", Toast.LENGTH_SHORT).show();

                            _printer.end();
                            _printer = null;
                        } else {

                            //if (modoDemoFM)
                            //    ticket.PrintBoletaElectronica(_printer);
                            //else
                            //    ticket.PrintTicket(_printer);
                        }

                        //si es un ticket ORIGINAL , es decir recien hecha la venta, rapidamente pasamos a una nueva venta
                        /*if (ticket.get_header().equals("ORIGINAL"))
                            btnNuevo.performClick();*/
                    }
                } catch (Exception ex) {
                    Toast.makeText(MainActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private OnClickListener accionCloseFragmentProductos = new OnClickListener() {
        @Override
        public void onClick(View view) {

            //ocultamos el selector de productos
            Fragment fragment = getFragmentManager().findFragmentById(R.id.frgProductos);
            if (fragment != null) {
                getFragmentManager().beginTransaction()
                        .hide(fragment)
                        .remove(fragment)
                        .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                        .commit();
            }
            //btnProductUndo.setVisibility(View.GONE);

            dummy_view_b.setBackgroundColor(Color.TRANSPARENT);
            dummy_view_b.setVisibility(View.INVISIBLE);
        }
    };


    private void ShowMessage(String title, String message) {
        Dialog.Alert(MainActivity.this, title, message, android.R.drawable.ic_dialog_alert);
    }

    //flujo normal de tecla cantidad
    private OnClickListener accionCantidadNormal = new OnClickListener() {
        @Override
        public void onClick(View view) {
            if (pedido == null) {
                //Toast.makeText(getApplicationContext(), getString(R.string.error_venta_no_activa), Toast.LENGTH_SHORT).show();
                ShowMessage(getString(R.string.title_sin_venta), getString(R.string.error_venta_no_activa));
            } else {

                //si habia que limpiar el panel, lo hacemos y volvemos
                if (primeroLimpiarPanel) {
                    AgregarTextoPanel("");
                    return;
                }

                //leemos el valor ingresado
                double cant = 0;
                try {
                    //cant = Double.parseDouble(ObtieneTextoPanel());
                    cant = Formatos.ParseaDecimal(ObtieneTextoPanel(), FORMATO_DECIMAL);
                } catch (NumberFormatException ex) {
                    //Toast.makeText(MainActivity.this, getString(R.string.error_cantidad_no_valida), Toast.LENGTH_SHORT).show();
                    ShowMessage(getString(R.string.title_ingresa_cantidad), getString(R.string.error_cantidad_no_valida));
                    return;
                }

                if (cant > 0) {

                    if (pedido_item_edit == null) {
                        //producto normal: validacion para limitar hasta cantidad de dos digitos
                        if (cant > CANT_MAX) {
                            //Toast.makeText(MainActivity.this, getString(R.string.error_cantidad_no_valida), Toast.LENGTH_SHORT).show();
                            ShowMessage(getString(R.string.title_ingresa_cantidad), getString(R.string.error_cantidad_no_valida));
                            return;
                        }


                        txtValor_panel.setText("");
                    } else {
                        //si hay un item en edicion, actualizamos su cantidad
                        if (pedido_item_edit.isFromCombo()) {
                            //Toast.makeText(MainActivity.this, getString(R.string.error_cantidad_item_combo), Toast.LENGTH_SHORT).show();
                            ShowMessage(getString(R.string.title_ingresa_cantidad), getString(R.string.error_cantidad_item_combo));
                            return;
                        } else if (pedido_item_edit.esItemPesable() && pedido_item_edit.get_producto().getUnit().equalsIgnoreCase("lt")) {

                            //es un producto a granel por litros

                            // => leemos el PESO en LITROS
                            //cant = cant; //expresamos la cantidad en LT
                        } else if (pedido_item_edit.esItemPesable()) {

                            //es un producto a granel por peso

                            // => leemos el PESO en GRAMOS
                            cant = cant / 1000; //expresamos la cantidad en KG
                        } else {
                            //producto normal: validacion para limitar hasta cantidad de dos digitos
                            if (cant > CANT_MAX) {
                                //Toast.makeText(MainActivity.this, getString(R.string.error_cantidad_no_valida), Toast.LENGTH_SHORT).show();
                                ShowMessage(getString(R.string.title_ingresa_cantidad), getString(R.string.error_cantidad_no_valida));
                                return;
                            }
                        }


                        if (!SubtotalItemSuperaMax(cant, pedido_item_edit.get_precio())) {
                            pedido_item_edit.set_cantidad(cant);
                            pedido_item_edit.actualiza_total(PREF_AJUSTE_SENCILLO);

                            ActualizarPedido(false);

                            txtWelcome_panel.setVisibility(View.INVISIBLE);
                            txtComentario_panel.setVisibility(View.VISIBLE);
                            txtComentario_panel.setText("Editada línea " + Formatos.FormateaDecimal(pedido_item_edit.get_cantidad(), FORMATO_DECIMAL_SECUND) + (pedido_item_edit.esItemPesable() ? "K x " : " x ") + pedido_item_edit.get_descripcion());

                            btnEnter.setText("OK / +");
                            btnEnter.setEnabled(true);
                            btnEnter.setBackgroundResource(R.drawable.botones_teclado);


                            /*
                            if (pedido_item_edit.get_total() > 0) { //como se considera que está OK el item habilitamos el boton Enter
                                btnEnter.setText("OK / +");
                                btnEnter.setEnabled(true);
                                btnEnter.setBackgroundColor(getResources().getColor(R.color.boton_gris));
                            } else {
                                btnEnter.setText("");
                                btnEnter.setEnabled(false);
                                //btnEnter.setBackgroundColor(getResources().getColor(R.color.boton_negro));
                                btnEnter.setBackgroundResource(R.drawable.logo_bpos);
                            }
                            */

                            //si estabamos editando algún item => cerramos su edicion
                            FinalizaEdicionItem();

                            //validamos luego de editar
                            ValidarVenta(false);

                            txtValor_panel.setText("");
                        } else {
                            //
                            //Toast.makeText(MainActivity.this, getString(R.string.error_subtotal_supera), Toast.LENGTH_SHORT).show();
                            ShowMessage(getString(R.string.title_control_item), getString(R.string.error_subtotal_supera));
                            return;
                        }

                    }
                } else {
                    //Toast.makeText(MainActivity.this, getString(R.string.error_cantidad_no_valida), Toast.LENGTH_SHORT).show();
                    ShowMessage(getString(R.string.title_control_item), getString(R.string.error_cantidad_no_valida));
                    return;
                }
            }
        }
    };

    /*private OnClickListener accionIngresoPrecio = new OnClickListener() {
        @Override
        public void onClick(View view) {
            if (pedido == null) {
                //Toast.makeText(getApplicationContext(), getString(R.string.error_venta_no_activa), Toast.LENGTH_SHORT).show();
                ShowMessage(getString(R.string.title_sin_venta), getString(R.string.error_venta_no_activa));
            } else {

                //si habia que limpiar el panel, lo hacemos y volvemos
                if (primeroLimpiarPanel) {
                    AgregarTextoPanel("");
                    return;
                }

                if (pedido_item_edit != null) {
                    //solo podemos editar el precio de un item de producto GENERICO

                    if (pedido_item_edit.isFromCombo()) {
                        //Toast.makeText(MainActivity.this, getString(R.string.error_precio_item_combo), Toast.LENGTH_SHORT).show();
                        ShowMessage(getString(R.string.title_ingresa_precio), getString(R.string.error_precio_item_combo));

                        //} else if (!pedido_item_edit.puedeEditarPrecio()) {
                    } else if (!pedido_item_edit.puedeEditarPrecio(PREF_ACTUALIZ_PRECIO_LISTA)) {
                        //Toast.makeText(MainActivity.this, getString(R.string.error_precio_lista_producto), Toast.LENGTH_SHORT).show();
                        ShowMessage(getString(R.string.title_ingresa_precio), getString(R.string.error_precio_lista_producto));
                    } else {
                        //aplicamos la funcion sobre el item en edicion
                        double precio = Formatos.ParseaDecimal(ObtieneTextoPanel(), FORMATO_DECIMAL);

                        if (precio == 0 || precio > SUBT_MAX) {
                            //Toast.makeText(MainActivity.this, getString(R.string.error_precio_supera), Toast.LENGTH_SHORT).show();
                            ShowMessage(getString(R.string.title_ingresa_precio), getString(R.string.error_precio_supera));
                            return;
                        } else {

                            if (!SubtotalItemSuperaMax(pedido_item_edit.get_cantidad(), Formatos.ParseaDecimal(ObtieneTextoPanel(), FORMATO_DECIMAL))) {
                                pedido_item_edit.set_precio(Formatos.ParseaDecimal(ObtieneTextoPanel(), FORMATO_DECIMAL));
                                pedido_item_edit.actualiza_total(PREF_AJUSTE_SENCILLO);

                                ActualizarPedido(false);

                                txtWelcome_panel.setVisibility(View.INVISIBLE);
                                txtComentario_panel.setVisibility(View.VISIBLE);
                                //txtComentario_panel.setText("Editada línea " + String.valueOf(pedido_item_edit.get_cantidad()) + " x " + pedido_item_edit.get_descripcion());


                                btnEnter.setText("OK / +");
                                btnEnter.setEnabled(true);
                                btnEnter.setBackgroundResource(R.drawable.botones_teclado);

                            *//*
                            if (pedido_item_edit.get_total() > 0) { //como se considera que está OK el item habilitamos el boton Enter
                                btnEnter.setText("OK / +");
                                btnEnter.setEnabled(true);
                                btnEnter.setBackgroundColor(getResources().getColor(R.color.boton_gris));
                            } else {
                                btnEnter.setText("");
                                btnEnter.setEnabled(false);
                                //btnEnter.setBackgroundColor(getResources().getColor(R.color.boton_negro));
                                btnEnter.setBackgroundResource(R.drawable.logo_bpos);
                            }
                            *//*

                                //si estabamos editando algún item => cerramos su edicion
                                FinalizaEdicionItem();

                                //validamos luego de editar
                                ValidarVenta(false);

                                txtValor_panel.setText("");
                            } else {
                                //
                                //Toast.makeText(MainActivity.this, getString(R.string.error_subtotal_supera), Toast.LENGTH_SHORT).show();
                                ShowMessage(getString(R.string.title_control_item), getString(R.string.error_subtotal_supera));
                                return;
                            }

                        }
                    }
                } else {
                    //como no hay item en edicion, creamos uno generico
                    double precio = Formatos.ParseaDecimal(ObtieneTextoPanel(), FORMATO_DECIMAL);

                    if (precio == 0 || precio > SUBT_MAX) {
                        //Toast.makeText(MainActivity.this, getString(R.string.error_precio_supera), Toast.LENGTH_SHORT).show();
                        ShowMessage(getString(R.string.title_control_item), getString(R.string.error_subtotal_supera));
                        return;
                    } else {
                        AgregarProductoAlPedido(-1, nombreProdGenerico, 1, precio, precio, null, false, true, false);

                        txtValor_panel.setText("");
                    }
                }
            }
        }
    };*/

    private void Init_AsignarViews() {
        this.dummy_view = (ImageView) findViewById(R.id.main_dummy);
        this.dummy_view_b = (ImageView) findViewById(R.id.main_dummy_b);
        //this.btnExit = (ImageButton) findViewById(R.id.main_btn_exit);
        //this.btnProductUndo = (Button) findViewById(R.id.main_btn_product_undo);

        this.pdvView = findViewById(R.id.main_layoutup);
        this.pdvStatusView = findViewById(R.id.pdv_status);
        this.pdvStatusMessageView = (TextView) findViewById(R.id.pdv_status_message);

        //this.txtValor_panel = (TextView) findViewById(R.id.main_txtPanel);
        //this.txtValor_panel = (TextView) findViewById(R.id.panel_txt_valor);
        this.btnCantidad = (Button) findViewById(R.id.pad_btnCantidad);
//        this.btnPrecio = (Button) findViewById(R.id.pad_btnPrecio);
        this.btnEnter = (Button) findViewById(R.id.pad_btnEnter);
        this.btnBackspace = (ImageButton) findViewById(R.id.pad_btnClear);
        this.btn0 = (Button) findViewById(R.id.pad_btnNro0);
        this.btn1 = (Button) findViewById(R.id.pad_btnNro1);
        this.btn2 = (Button) findViewById(R.id.pad_btnNro2);
        this.btn3 = (Button) findViewById(R.id.pad_btnNro3);
        this.btn4 = (Button) findViewById(R.id.pad_btnNro4);
        this.btn5 = (Button) findViewById(R.id.pad_btnNro5);
        this.btn6 = (Button) findViewById(R.id.pad_btnNro6);
        this.btn7 = (Button) findViewById(R.id.pad_btnNro7);
        this.btn8 = (Button) findViewById(R.id.pad_btnNro8);
        this.btn9 = (Button) findViewById(R.id.pad_btnNro9);
        this.btnPunto = (Button) findViewById(R.id.pad_btnPunto);
        this.btnCatalogo = (Button) findViewById(R.id.main_btnBrowseCatalog);
//        this.btnProductosPropios = (Button) findViewById(R.id.main_btnBrowse);
//        this.btnCombos = (Button) findViewById(R.id.main_btnCombos);
        //this.btnEditar = (Button) findViewById(R.id.main_btnEdit);
        this.btnMenu = (ImageButton) findViewById(R.id.main_btnMenu);
        this.btnEliminarPedido = (ImageButton) findViewById(R.id.main_btnEliminar);
        this.btnNuevo = (ImageButton) findViewById(R.id.main_btnNuevo);
        this.txtTotalPedido = (TextView) findViewById(R.id.main_txtTotal);

        this.lstVentas = (ListView) findViewById(R.id.main_lstVentas);
        //this.lstProductos = (ListView) findViewById(R.id.main_lstProductos);

        //controles del panel
        this.imgIcono_panel = (ImageView) findViewById(R.id.panel_img_icono);
        this.txtWelcome_panel = (TextView) findViewById(R.id.panel_txt_welcome);
        this.txtUser_panel = (TextView) findViewById(R.id.panel_txt_user);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) this.txtUser_panel.getLayoutParams();
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        this.txtUser_panel.setLayoutParams(params);
        this.txtComentario_panel = (TextView) findViewById(R.id.panel_txt_comentario);
        this.txtFuncion_panel = (TextView) findViewById(R.id.panel_txt_funcion);
        this.txtValor_panel = (TextView) findViewById(R.id.panel_txt_valor);
        this.txtValor_panel.setTextSize(sizeText);
        this.txtFecha_panel = (TextView) findViewById(R.id.panel_txt_fecha);
        this.txtFecha_panel.setVisibility(View.GONE);
        this.txtHora_panel = (TextView) findViewById(R.id.panel_txt_hora);
        this.txtHora_panel.setVisibility(View.GONE);
        this.btnDeshacer_panel = (Button) findViewById(R.id.panel_btn_deshacer);
        this.btnEliminar_panel = (ImageButton) findViewById(R.id.panel_btn_eliminar);
        this.btnDescripcion_panel = (Button) findViewById(R.id.panel_btn_descripcion);
        this.btnValidar_panel = (Button) findViewById(R.id.panel_btn_validar);

        //controles del pedido
        this.btnCobrar = (Button) findViewById(R.id.btnCobrar);
        this.btnComprobante = (Button) findViewById(R.id.btnComprobante);
        this.btnVenta = (Button) findViewById(R.id.btnVenta);
        this.btnAnular = (Button) findViewById(R.id.btnAnular);
        this.btnImprimir = (Button) findViewById(R.id.btnImprimir);//(Button) findViewById(R.id.pedido_btn_imprimir);
        this.lstPedido = (ListView) findViewById(R.id.pedido_lst_pedido);
        this.lblNuevoPedido = (TextView) findViewById(R.id.pedido_lbl_nuevo_pedido);
        this.txtNroPedido = (TextView) findViewById(R.id.pedido_txt_pedido);
        this.lblPedidoRegistrado = (TextView) findViewById(R.id.pedido_lbl_pedido_registrado);

        //controles del popup medio pago
        this.btnConfirmar_mp = (Button) findViewById(R.id.medio_pago_btn_aceptar);
        this.btnCancelar_mp = (Button) findViewById(R.id.medio_pago_btn_cancelar);
        this.txtTotal_mp = (TextView) findViewById(R.id.medio_pago_txt_total);
        this.rbtMedios_mp = (RadioGroup) findViewById(R.id.medio_pago_medios);

        //this.btnCalc = (ImageButton) findViewById(R.id.main_btnCalc);
    }

    private void Init_CustomConfigPais(String codePais) {


        //este formato se usa en...
        if (codePais.equals("AR")) {
            FORMATO_DECIMAL = Formatos.DecimalFormat;
            FORMATO_DECIMAL_ID = Formato.Decimal_Format.AR;
        } else if (codePais.equals("PE")) {
            FORMATO_DECIMAL = Formatos.DecimalFormat_PE;
            FORMATO_DECIMAL_ID = Formato.Decimal_Format.PE;
        } else {
            FORMATO_DECIMAL = Formatos.DecimalFormat_CH;
            FORMATO_DECIMAL_ID = Formato.Decimal_Format.CH;
        }

        FORMATO_SIGNO_MONEDA = Formato.getCurrencySymbol(FORMATO_DECIMAL_ID);//codePais.equals("PE") ? Formatos.CurrencySymbol_PE : Formatos.CurrencySymbol;

        //este formato secundario se usa en...
        FORMATO_DECIMAL_SECUND = codePais.equals("AR") ? Formatos.DecimalFormat : Formatos.DecimalFormat_US;
        //private DecimalFormat FORMATO_DECIMAL;FORMATO_FECHA

        btnComprobante.setText(getStringFromResource(codePais, "btn_presupuesto"));

    }

    private void Timer_Init() {

        Timer_Reset();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();

        //cada vez que el user interactúa reseteamos el timer
        Timer_Reset();
    }

    private Timer timer;//timer para control de la tarea de confirmacion automatica
    private ConfirmacionAutoTimerTask confirmAutoTask;//tarea asociada al timer para confirmacion automatica

    private void Timer_Reset() {
        //si corresponde reseteamos el timer:
        if (PREF_TIEMPO_CONFIRM != 0) {
            if (timer != null) {
                timer.cancel();
            }

            Log.d("TimerTask", "Set");

            timer = new Timer();
            confirmAutoTask = new ConfirmacionAutoTimerTask();
            timer.schedule(confirmAutoTask, PREF_TIEMPO_CONFIRM * 1000, PREF_TIEMPO_CONFIRM * 1000);
        }
    }


    private void Timer_Cancel() {

        if (timer != null) {
            Log.d("TimerTask", "Cancel");

            timer.cancel();
            timer = null;
        }
    }




    class ConfirmacionAutoTimerTask extends TimerTask {

        @Override
        public void run() {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    Log.d("ConfirmaAutoTimerTask", "Cumplido");

                    if (pedido != null) {
                        if (pedido.size() > 0) {
                            if (ValidarVenta(true)) {                    //reglas de validacion de una venta
                                ConfirmarVentaAuto(getTotalVenta());
                            }
                        }
                    }
                }
            });
        }

    }

    private Double getTotalVenta() {
        return Formatos.ParseaDecimal(txtTotalPedido.getText().toString().replace(FORMATO_SIGNO_MONEDA, " ").trim(), FORMATO_DECIMAL);
    }

    private String getStringFromResource(String codePais, String idResource) {
        String sufijo = codePais.equals("AR") ? "_AR" : "";
        //String pack = codePais.equals("AR") ? "strings_AR" : "strings";
        int resId = getResources().getIdentifier(idResource + sufijo, "string", getPackageName());
        return getString(resId);
    }

    public OnClickListener accionClickNuevo = new OnClickListener() {
        @Override
        public void onClick(View view) {

            lstVentas.setVisibility(View.INVISIBLE);

            if (pedido != null) {
                DescartarVentaEnCurso(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        NuevaVenta();
                    }
                });
            } else {
                NuevaVenta();
            }
        }
    };

    public OnClickListener accionClickEnter = new OnClickListener() {

        @Override
        public void onClick(View view) {
            try {
                if (pedido == null) {
                    //Toast.makeText(getApplicationContext(), getString(R.string.error_venta_no_activa), Toast.LENGTH_SHORT).show();
                    ShowMessage(getString(R.string.title_sin_venta), getString(R.string.error_venta_no_activa));
                /*} else if (pedido.size() == 0) {
                    Toast.makeText(getApplicationContext(), getString(R.string.error_venta_sin_productos), Toast.LENGTH_SHORT).show();
                */
                } else {

                    //si habia que limpiar el panel, lo hacemos y volvemos
                    if (primeroLimpiarPanel) {
                        AgregarTextoPanel("");
                        return;
                    }

                    if (cardInput) {
                        //si estamos solicitando los ultimos 4 numeros de la tarjeta
                        ValidaTarjeta();
                    } else {
                        if (pedido_item_edit != null) {

                            if (pedido_item_edit.esItemPesable() && pedido_item_edit.get_cantidad() == 0) {
                                //es un pesable aún sin el peso establecido, entonces establecemos el peso ingresado por teclado
                                btnCantidad.performClick();

                                //reactivamos modalidad de selección de productos
                                //***No aplica mas, dado que no volvemos al fragment de productos luego de ingresar uno***
                                /*dummy_view_b.setVisibility(View.VISIBLE);
                                if (getFragmentProductoInstance() != null) { //puede ser que el producto haya venido por code => no hay frg productos
                                    getFragmentProductoInstance().continueAfterWeightableProduct();
                                    return;
                                }*/
                            }

                            //si estabamos editando un item cerramos su edicion
                            FinalizaEdicionItem();

                            txtValor_panel.setText("");

                            //validamos venta al ir ingresando los items...
                            ValidarVenta(false);
                        } else {

                            //si hay algo cargado en el visor, lo usamos para traer un producto por codigo rapido, sino seguimos con el cierre de la venta
                            if (!TextUtils.isEmpty(txtValor_panel.getText().toString())) {
                                String code = txtValor_panel.getText().toString();

                                txtValor_panel.setText("");

                                if (code.length() <= 3) {
                                    //tomamos como un codigo rapido
                                    code = "000" + code;//armamos un padding de 3
                                    code = code.substring(code.length() - 3);

                                    if (!obtenerProductoOrComboByIspCode(code))
                                        //Toast.makeText(MainActivity.this, "Codigo rápido " + code + " no asociado a un producto/combo", Toast.LENGTH_SHORT).show();
                                        ShowMessage("Codigo Rapido", "Codigo rápido " + code + " no asociado a un producto/combo");

                                    return;
                                } /*else if(code.length() == 8 || code.length() == 13) {
                                    //lo consideramos un SKU, validando el EAN
                                    IngresaProductoBySKU(code, true);

                                    return;
                                }*/ else {
                                    /*Toast.makeText(MainActivity.this, "Ingreso no válido", Toast.LENGTH_SHORT).show();
                                    return;*/

                                    //vamos a intentar considerarlo un SKU, validando el EAN
                                    IngresaProductoBySKU(code, true);

                                    return;
                                }
                            }


                            //reglas de validacion de una venta
                            if (ValidarVenta(true)) {
                                //reiniciamos esta variable
                                registarComprobante = false;
                                btnComprobante.setTextColor(getResources().getColor(R.color.blanco));
                                btnComprobante.setBackgroundResource(R.drawable.backrounded_grey);

                                IniciarCheckout(getTotalVenta());

                                /*
                                //como no estabamos editando un item, cerramos la venta
                                dummy_view.setBackgroundColor(getResources().getColor(R.color.blur));
                                dummy_view.setVisibility(View.VISIBLE);
                                findViewById(R.id.main_medio_pago).setVisibility(View.VISIBLE);
                                txtTotal_mp.setText(txtTotalPedido.getText());
                                //seteamos por default en EFECTIVO
                                rbtMedios_mp.check(R.id.medio_pago_rbt_efvo);

                                txtValor_panel.setText("");*/
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                //Toast.makeText(getApplicationContext(), "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
                ShowMessage("Error", "Error: " + ex.getMessage());
            }
        }
    };

    private void OcultarPopupMedioPago() {
        dummy_view.setBackgroundColor(Color.TRANSPARENT);
        dummy_view.setVisibility(View.INVISIBLE);
        findViewById(R.id.medio_pago_lbl_deslice).setVisibility(View.GONE);
        findViewById(R.id.main_medio_pago).setVisibility(View.INVISIBLE);
    }

    private void HabilitarLector() {

        try {
            if (_lector == null)
                _lector = CardReader.getInstance();

            _lector.initialize();

            _lector.setOnReadDataListener(new OnReadSerialPortDataListener() {
                @Override
                public void onReadSerialPortData(SerialPortData serialPortData) {
                    final String str = serialPortData.getCardNumber();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            CARD_NUMBER = str;
                            OcultarPopupMedioPago();

                            txtComentario_panel.setVisibility(View.VISIBLE);
                            txtComentario_panel.setText("Ingrese 4 ultimos digitos...");

                            //Toast.makeText(MainActivity.this, CARD_NUMBER, Toast.LENGTH_LONG).show();

                            cardInput = true;
                        }
                    });
                }
            });

            //limpiamos el ultimo numero leido
            CARD_NUMBER = "";

            _lector.read();

        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    //region VALIDACION_DE_TARJETA_DEMO_MINI_POS
    private void ValidaTarjeta() {
        String _digitosIngresados = ObtieneTextoPanel().trim();

        if (_digitosIngresados.equals(CARD_NUMBER.substring(CARD_NUMBER.length() - 4))) {
            cardInput = false;

            SolicitarAutorizacion();
        } else {
            this.txtValor_panel.setText("");
        }
    }

    public void SolicitarAutorizacion() {
        try {

            if (!ConnectivityUtils.isOnline(MainActivity.this)) {
                Toast.makeText(this, "Error: No hay una conexion disponible. Intente mas tarde.", Toast.LENGTH_LONG).show();
            } else {
                new ExecuteRequestTask("SolicitarAutorizacionResponse", "").execute();
            }

        } catch (Exception ex) {
            Toast.makeText(this, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void SolicitarAutorizacionResponse() {
        txtComentario_panel.setText("TRANSACCION APROBADA");
        //String numero_masked =  new String(new char[CARD_NUMBER.length()-4]).replace("\0", "X") + CARD_NUMBER.substring(CARD_NUMBER.length()-4);
        String numero_masked = "XXXXXXXX" + CARD_NUMBER.substring(CARD_NUMBER.length() - 4);
        txtValor_panel.setText("TC " + numero_masked);

        btnConfirmar_mp.performClick();
    }
    //endregion

    public ProductosFragment getFragmentProductoInstance() {
        if (findViewById(R.id.frgProductos) != null) {
            return (ProductosFragment) getFragmentManager().findFragmentById(R.id.frgProductos);
        }
        return null;
    }

    public CheckoutFragment getFragmentCheckoutInstance() {
        if (findViewById(R.id.fragment_checkout_container) != null) {
            return (CheckoutFragment) getFragmentManager().findFragmentById(R.id.fragment_checkout_container);
        }
        return null;
    }

    public void IniciarFragmentProducto(int modo) {
        IniciarFragmentProducto(false, modo);
    }

    public void IniciarFragmentProducto(boolean solo_sin_code, int mode) {
        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.frgProductos) != null) {

            /*long[] prods = new long[pedido.size()];
            for (int i = 0; i < pedido.size(); i++)
                prods[i] = pedido.get(i).get_id_producto();*/

            HashMap<Integer, Integer> prods_cant = new HashMap<Integer, Integer>(pedido.size());
            for (Pedido p : pedido)
                prods_cant.put(p.get_id_producto(), p.esItemPesable() ? 1 : ((Double) p.get_cantidad()).intValue());

            // Create a new Fragment to be placed in the activity layout
            boolean solo_combos = (mode == 0);
            boolean solo_propios = (mode == 1);
            boolean solo_catalogo = (mode == 2);

            BaseProductosFragment frgProductos = ProductosFragment.newInstance(true, prods_cant, FORMATO_DECIMAL, PREF_SHOW_PROD_SIN_PRECIO, FORMATO_SIGNO_MONEDA, solo_sin_code, solo_propios, solo_catalogo, solo_combos);
            frgProductos.setProductoListener(this);

            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.frgProductos, frgProductos);
            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            ft.commit();
        }

        /* HC 25-10
        dummy_view_b.setBackgroundColor(getResources().getColor(R.color.blur_min));
        dummy_view_b.setVisibility(View.VISIBLE);
        */

        //btnProductUndo.setVisibility(View.VISIBLE);
        //lstProductos.setVisibility(View.VISIBLE);
        //lstProductos.requestFocusFromTouch();

        //ProductosFragment frgProductos = (ProductosFragment) getFragmentManager().findFragmentById(R.id.frgProductos);
        //frgProductos.set

        //showHideFragment(false);
    }

    @Override
    public void onFragmentProductoClose() {
        //cerramos el fragment de productos
        accionCloseFragmentProductos.onClick(null);

        //cerramos el teclado por si quedó abierto en una busqueda de producto x nombre
        Window.CloseSoftKeyboard(MainActivity.this);
    }

    @Override
    public void onProductoSeleccionado(Product p) {

        if (pedido == null) {
            //Toast.makeText(getApplicationContext(), getString(R.string.error_venta_no_activa), Toast.LENGTH_SHORT).show();
            ShowMessage(getString(R.string.title_sin_venta), getString(R.string.error_venta_no_activa));
        } else {
            //last_producto = p;

          /*  if (p.getType().equals("P")) {
                //es un producto primario de un combo => mostramos campo de seleccion de combos
                IniciarFragmentSelectCombo(p);
            } else {*/
                AddProductToPedido(p);
//            }
        }

        //cerramos el fragment de productos
        accionCloseFragmentProductos.onClick(null);

    }


    @Override
    public void onMedioSeleccionado(String descripcion_medio) {
        vistaOpcionesComprobantes(View.GONE);
        txtComentario_panel.setText(descripcion_medio);
        findViewById(R.id.main_panel).setVisibility(View.VISIBLE); //mostramos el VISOR celeste
        findViewById(R.id.pdv_header_r_layout).setVisibility(View.VISIBLE); //mostramos la barra de botones superior
    }

    private void AddProductToPedido(Product p) {
        AgregarProductoAlPedido(
                (int) p.getId(),
                p.getPrintingName(), //usamos descripcion, sino el nombre
                p.isWeighable() ? 0 : 1, //cargamos en 0 el peso, de ser pesable
                p.getSalePrice(),
                p.isWeighable() ? 0 : p.getSalePrice(),//cargamos en 0 el total, de ser pesable
                p,
                false,
                false,
                false
        );
    }

    private boolean VentaEnCheckout;

    public void SalirCheckout() {

        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        Fragment fragment = getFragmentManager().findFragmentById(R.id.fragment_checkout_container);
        if (fragment != null) {
            //ocultamos el checkout
            getFragmentManager().beginTransaction()
                    .remove(fragment)
                    .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                    .commit();
        }


        findViewById(R.id.main_panel).setVisibility(View.VISIBLE); //mostramos el VISOR celeste
        findViewById(R.id.pdv_header_r_layout).setVisibility(View.VISIBLE); //mostramos la barra de botones superior
        findViewById(R.id.main_pad).setVisibility(View.VISIBLE); //mostramos el PAD
        findViewById(R.id.pdv_cliente_image).setVisibility(View.GONE);
        vistaOpcionesComprobantes(View.GONE);
        btnCobrar.setVisibility(View.GONE);
        btnCantidad.setEnabled(true);
        btnCantidad.setBackgroundResource(R.drawable.botones_teclado);
        btnCantidad.setText("CANTIDAD");
        btnCantidad.setOnClickListener(accionCantidadNormal);
//        btnPrecio.setEnabled(true);
//        btnPrecio.setBackgroundResource(R.drawable.botones_teclado);
//        btnPrecio.setText("PRECIO");
        this.txtValor_panel.setTextSize(sizeText);

        this.btnEnter.setOnClickListener(accionClickEnter);

        VentaEnCheckout = false;
    }

    @Override
    public void onPagoFinalizado(Double total, String medio, Double vuelto) {

        SalirCheckout();

        if (medio.equals(CheckoutFragment.MEDIO_EFECTIVO_ID)) {
            //como es EFECTIVO demoramos el clear para que quede visible el VUELTO, hasta el primer input
            this.txtValor_panel.setTextSize(normalSizeText);//como muestra el vuelto, reducimos el font
            primeroLimpiarPanel = true;
        } else {
            //como no es EFECTIVO limpiamos
            this.txtValor_panel.setText("");
        }

        CerrarVenta(registarComprobante ? "CPB" : "VTA", medio, total, vuelto, tc);

    }

    private static final String MSJ_VALIDAR_O_INGRESAR = "Validar venta o Ingresar otro producto / monto / cantidad";

    @Override
    public void onPagoCancelado() {

        SalirCheckout();

        this.txtComentario_panel.setText(MSJ_VALIDAR_O_INGRESAR);
        this.txtValor_panel.setText("");
    }

    @Override
    public void onTecladoInvocado(int inputType) {
        final int _inputType = inputType;

        if (inputType == CheckoutFragment.INPUT_OCULTAR_TECLADO) {
            findViewById(R.id.main_pad).setVisibility(View.GONE);
        } else {

            primeroLimpiarPanel = true;
            findViewById(R.id.main_pad).setVisibility(View.VISIBLE);
            findViewById(R.id.pdv_cliente_image).setVisibility(View.GONE);
            vistaOpcionesComprobantes(View.GONE);
            btnCobrar.setVisibility(View.GONE);
            btnCantidad.setEnabled(false);
            btnCantidad.setBackgroundColor(getResources().getColor(R.color.boton_negro));
            btnCantidad.setText("");
//            btnPrecio.setEnabled(false);
//            btnPrecio.setBackgroundColor(getResources().getColor(R.color.boton_negro));
//            btnPrecio.setText("");

            if (inputType == CheckoutFragment.INPUT_EFECTIVO_MONTO) {
                //si es EFECTIVO, vamos a habilitar la tecla de CANTIDAD para la función de PAGO EXACTO
                btnCantidad.setEnabled(true);
                btnCantidad.setBackgroundColor(getResources().getColor(R.color.amarillo));
                btnCantidad.setText("PAGO EXACTO");
                btnCantidad.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (pedido == null) {
                            //Toast.makeText(getApplicationContext(), getString(R.string.error_venta_no_activa), Toast.LENGTH_SHORT).show();
                            ShowMessage(getString(R.string.title_sin_venta), getString(R.string.error_venta_no_activa));
                        } else {
                            //se confirma la venta
                            ((CheckoutFragment) getFragmentManager().findFragmentById(R.id.fragment_checkout_container)).setPagoExacto();
                        }
                    }
                });
            }

            this.btnEnter.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    String input = ObtieneTextoPanel();
                    txtValor_panel.setText("");
                    ((CheckoutFragment) getFragmentManager().findFragmentById(R.id.fragment_checkout_container)).setMontoIngresado(input, _inputType);
                }
            });
        }
    }

    private void IniciarCheckout(Double montoPagar) {
        IniciarCheckout(montoPagar, false);
    }

    private void IniciarCheckout(Double montoPagar, boolean valeErrorMode) {

        VentaEnCheckout = true;

        if (PREF_SHOW_CHECKOUT) {
            // Check that the activity is using the layout version with
            if (findViewById(R.id.fragment_checkout_container) != null) {

                boolean iniciarFormaPagoSelectedAuto = false;

                //habilitamos opciones de registracion

                if (valeErrorMode) {
                    //si dio un error al intentar emitir un vale, entonces no aplica el pase directo al medio de pago
                    vistaOpcionesComprobantes(View.VISIBLE, true);
                } else {
                    if (vistaOpcionesComprobantes(View.VISIBLE) == 1) {
                        //si solo hay una opcion disponible, pasamos directo al medio de pago
                        iniciarFormaPagoSelectedAuto = true;
                    }
                }

                // Create a new Fragment to be placed in the activity layout
                CheckoutFragment chkoutFragment = CheckoutFragment.newInstance(montoPagar, txtValor_panel.getId(), this._user, PREF_MEDIO_DEFAULT, FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA, iniciarFormaPagoSelectedAuto);

                // Add the fragment to the 'fragment_container' FrameLayout
                getFragmentManager().beginTransaction()
                        .add(R.id.fragment_checkout_container, chkoutFragment)
                        .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                        .commit();

                txtComentario_panel.setText(R.string.mensaje_select_forma_pago);
                findViewById(R.id.main_pad).setVisibility(View.GONE);

                this.txtValor_panel.setTextSize(normalSizeText);
            }
        } else {
            //esta DESACTIVADO el checkout
            ConfirmarVentaAuto(montoPagar);
        }
    }

    private void ConfirmarVentaAuto(Double montoPagar) {
        if (pedido != null) {
            //se confirma la venta, en EFECTIVO (o en el MEDIO_DEFAULT), vuelto 0
            onPagoFinalizado(montoPagar, PREF_MEDIO_DEFAULT.equals("") ? CheckoutFragment.MEDIO_EFECTIVO_ID : PREF_MEDIO_DEFAULT, 0d);
        }
    }

    private int vistaOpcionesComprobantes(int vista) {
        return vistaOpcionesComprobantes(vista, false);
    }

    private int vistaOpcionesComprobantes(int vista, boolean forzarOpcionVentaOculta) {
        int opciones_visibles = 0;

        try {

            //la vista del boton COMPROBANTE depende de la configuracion
            int vistaComprobante = (PREF_COMPROB_HABILIT ? vista : View.GONE);

            //ajustamos
            btnComprobante.setVisibility(vistaComprobante);
            btnVenta.setVisibility(forzarOpcionVentaOculta ? View.GONE : vista);

            //contamos la cantidad de opciones visibles...
            opciones_visibles += btnVenta.getVisibility() == View.VISIBLE ? 1 : 0;
            opciones_visibles += btnComprobante.getVisibility() == View.VISIBLE ? 1 : 0;

        } catch (Exception ex) {
            Log.e("PDV", "vistaOpcionesComprobantes", ex);
        }

        return opciones_visibles;

    }

    @Override
    public void onClienteSeleccionado(Cliente cliente) {

        cliente_selected = cliente;

        if (!TextUtils.isEmpty(cliente.getPathPhoto())) {
            ((ImageView) findViewById(R.id.pdv_cliente_image)).setImageURI(Uri.fromFile(new File(getStorageDir(), cliente.getPathPhoto())));
        } else {
            ((ImageView) findViewById(R.id.pdv_cliente_image)).setImageResource(R.drawable.user_default_photo);
        }

        findViewById(R.id.chkout_btn_nuevo_cliente).setVisibility(View.GONE);

        findViewById(R.id.pdv_cliente_image).setVisibility(View.VISIBLE);
        findViewById(R.id.btnCobrar).setVisibility(View.VISIBLE);
    }

    private File getStorageDir() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
    }

    private class ExecuteRequestTask extends AsyncTask<String, Void, String> {

        private String nextAction = "";
        private String paramsNextAction = "";

        //constructor
        public ExecuteRequestTask(String NextAction, String ParamsNextAction) {
            this.nextAction = NextAction;
            this.paramsNextAction = ParamsNextAction;
        }

        // onPreExecute used to setup the AsyncTask.
        @Override
        protected void onPreExecute() {
            showProgress(true);
            pdvStatusMessageView.setText("Solicitando autorizacion...");
        }

        @Override
        protected String doInBackground(String... messages) {

            // params comes from the execute() call: params[0] is the message.
            try {
                // Simulate network access.
                Thread.sleep(2000);

                publishProgress();

                return "";

                //} catch (IOException e) {
                //    return "No es posible conectarse. Intente mas tarde.";
            } catch (Exception e) {
                return "Error: " + e.getMessage();
            }
        }

        @Override
        protected void onProgressUpdate(Void... v) {
            pdvStatusMessageView.setText("Leyendo respuesta...");
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            try {
                showProgress(false);

                if (result.equals("") && !this.nextAction.equals("")) {

                    if (!this.paramsNextAction.equals("")) {
                        //si tiene param
                        Method accion = MainActivity.class.getClass().getMethod(this.nextAction, this.paramsNextAction.getClass());

                        accion.invoke(MainActivity.this, this.paramsNextAction);
                    } else {
                        //sino tiene param
                        Method accion = MainActivity.class.getClass().getMethod(this.nextAction, null);

                        accion.invoke(MainActivity.this, null);
                    }


                } else {
                    Toast.makeText(MainActivity.this, result.toString(), Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {
                Toast.makeText(MainActivity.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }


        @Override
        protected void onCancelled() {
            showProgress(false);
        }
    }

    /**
     * Shows the progress UI and hides the main layout.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            pdvStatusView.setVisibility(View.VISIBLE);
            pdvStatusView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(show ? 1 : 0)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            pdvStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
                        }
                    });

            pdvView.setVisibility(View.VISIBLE);
            pdvView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(show ? 0 : 1)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            pdvView.setVisibility(show ? View.GONE : View.VISIBLE);
                        }
                    });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            pdvStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
            pdvView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private List<Pedido> ObtieneDetalleVta(VentaAbstract vta) {
        List<Pedido> items = null;

        try {

            if (vta.getType(config.PAIS).equals("VENTA")) {

                List<VentaDetalle> detalle = new VentaDetalleDao(getContentResolver()).list(String.format("id_venta = %d", vta.getId()), null, null);
                items = new ArrayList<Pedido>();

                // recorremos los items
                for (int i = 0; i < detalle.size(); i++) {
                    items.add(new Pedido(
                            i + 1, //nroLinea (inicia en 1)
                            detalle.get(i).getProducto() != null ? detalle.get(i).getIdProducto() : -1,
                            detalle.get(i).getCantidad(),
                            detalle.get(i).getProducto() != null ? detalle.get(i).getProducto().getPrintingName() : (TextUtils.isEmpty(detalle.get(i).getSku()) ? nombreProdGenerico : detalle.get(i).getSku()),
                            detalle.get(i).getPrecio(),
                            detalle.get(i).getTotal(),
                            detalle.get(i).getProducto(),
                            IVA_GRAVADO,
                            false
                    ));
                }

            } else {
                List<ComprobanteDetalle> detalle = new ComprobanteDetalleDao(getContentResolver()).list(String.format("id_comprobante = %d", vta.getId()), null, null);
                items = new ArrayList<Pedido>();

                // recorremos los items
                for (int i = 0; i < detalle.size(); i++) {
                    items.add(new Pedido(
                            i + 1, //nroLinea (inicia en 1)
                            detalle.get(i).getProducto() != null ? detalle.get(i).getIdProducto() : -1,
                            detalle.get(i).getCantidad(),
                            detalle.get(i).getProducto() != null ? detalle.get(i).getProducto().getPrintingName() : (TextUtils.isEmpty(detalle.get(i).getSku()) ? nombreProdGenerico : detalle.get(i).getSku()),
                            detalle.get(i).getPrecio(),
                            detalle.get(i).getTotal(),
                            detalle.get(i).getProducto(),
                            IVA_GRAVADO,
                            false
                    ));
                }

            }

        } catch (Exception ex) {
            //Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
            ShowMessage("Error", "Error: " + ex.getMessage());
        } finally {
            return items;
        }
    }


    private void AgregarTextoPanel(String texto) {
        if (primeroLimpiarPanel) {
            primeroLimpiarPanel = false;
            this.txtValor_panel.setText(texto);
            this.txtValor_panel.setVisibility(View.VISIBLE);
        } else {
            this.txtValor_panel.setText(this.txtValor_panel.getText().toString() + texto);
            this.txtValor_panel.setVisibility(View.VISIBLE);
        }
    }

    private String ObtieneTextoPanel() {
        String texto = this.txtValor_panel.getText().toString();

        if (texto.equals("")) {
            return "0";
        } else
            return texto;
    }

    private void EstadoPanelEdicion(boolean visible) {
        if (visible) {
            this.txtWelcome_panel.setVisibility(View.INVISIBLE);
            this.txtUser_panel.setVisibility(View.INVISIBLE);
            //this.txtFecha_panel.setVisibility(View.INVISIBLE);
            //this.txtHora_panel.setVisibility(View.INVISIBLE);
            this.txtComentario_panel.setVisibility(View.INVISIBLE);
            this.txtFuncion_panel.setVisibility(View.INVISIBLE);
            this.btnDeshacer_panel.setVisibility(View.INVISIBLE);

            ((RelativeLayout) this.txtWelcome_panel.getParent()).setBackgroundColor(Color.WHITE);

            this.btnValidar_panel.setVisibility(View.VISIBLE);
            this.btnEliminar_panel.setVisibility(View.VISIBLE);
            this.btnDescripcion_panel.setVisibility(View.INVISIBLE);
            ((TableLayout) findViewById(R.id.panel_tbl_detalle)).setVisibility(View.VISIBLE);
            //((ImageView) findViewById(R.id.panel_img_imagen)).setVisibility(View.VISIBLE);

            this.btnEnter.setText("-");

        } else {
            //((RelativeLayout) this.txtWelcome_panel.getParent()).setBackgroundResource(R.color.panel_fondo_celeste);
            //((RelativeLayout) this.txtWelcome_panel.getParent()).setBackgroundColor(Color.BLUE);
            ((RelativeLayout) this.txtWelcome_panel.getParent()).setBackgroundColor(getResources().getColor(R.color.panel_fondo_celeste));
            this.txtWelcome_panel.setVisibility(View.INVISIBLE);
            //this.txtWelcome_panel.setText("PDV");
            this.imgIcono_panel.setImageResource(R.drawable.pdv_min);
            this.imgIcono_panel.setVisibility(View.VISIBLE);
            this.txtUser_panel.setVisibility(View.VISIBLE);
            //this.txtFecha_panel.setVisibility(View.VISIBLE);
            //this.txtHora_panel.setVisibility(View.VISIBLE);
            this.txtFuncion_panel.setVisibility(View.INVISIBLE);
            this.btnDeshacer_panel.setVisibility(View.INVISIBLE);

            this.txtComentario_panel.setVisibility(View.VISIBLE);
            this.txtComentario_panel.setText("Ingrese producto / precio / cantidad");

            this.btnValidar_panel.setVisibility(View.INVISIBLE);
            this.btnEliminar_panel.setVisibility(View.INVISIBLE);
            this.btnDescripcion_panel.setVisibility(View.INVISIBLE);
            ((TableLayout) findViewById(R.id.panel_tbl_detalle)).setVisibility(View.INVISIBLE);
            ((ImageView) findViewById(R.id.panel_img_imagen)).setVisibility(View.INVISIBLE);

            //TODO: x3
            btnEnter.setText("OK / +");
            //this.btnEnter.setText("");
            //this.btnEnter.setEnabled(false);
            //this.btnEnter.setBackgroundResource(R.drawable.logo_bpos);
            //this.btnEnter.setBackgroundColor(getResources().getColor(R.color.boton_negro));
        }
    }

    private void IniciaEdicionItem(Pedido p) {
        //guardamos el item en edicion

        if (pedido_item_edit != null) //si habia otro en edit, lo limpiamos
            pedido_item_edit.setIsInEditMode(false);

        p.setIsInEditMode(true);
        pedido_item_edit = p;

        this.btnEnter.setText("OK / +");
        if (p.esItemPesable()) {
            this.btnCantidad.setText("PESO");
            this.btnCantidad.setBackgroundColor(getResources().getColor(R.color.amarillo));
        } else {
            this.btnCantidad.setText("CANTIDAD");
            this.btnCantidad.setBackgroundColor(getResources().getColor(R.color.boton_gris));
        }
    }

    private void FinalizaEdicionItem() {
        try {
            //limpiamos el item en edicion
            if (pedido_item_edit != null) {
                pedido_item_edit.setIsInEditMode(false);
                pedido_item_edit.hasError(false);
                pedido_item_edit = null;

                ((PedidoAdapter) lstPedido.getAdapter()).notifyDataSetChanged();

                //this.btnEnter.setText("+");
                this.txtComentario_panel.setVisibility(View.VISIBLE);
                this.txtComentario_panel.setText("Validar venta o Ingresar otro producto / monto / cantidad");

                //se estaba editando un item => sonamos
                EmitirSonidoItem();
            }

            //restauramos el boton CANTIDAD
            btnCantidad.setText("CANTIDAD");
            btnCantidad.setBackgroundColor(getResources().getColor(R.color.boton_gris));


        } catch (Exception ex) {
            //Toast.makeText(this, "Error al finalizar edicion de item: " + ex.getMessage(), Toast.LENGTH_LONG).show();
            ShowMessage("Error", "Error al finalizar edicion de item: " + ex.getMessage());

        }
    }

    private void EmitirSonidoItem() {
        try {
            new PlaySound(this).PlaySound(R.raw.beep_07);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void EditarLineaPanel(Pedido p) {

        //EstadoPanelEdicion(true);
        //((TextView) findViewById(R.id.panel_txt_descripcion)).setText(p.get_descripcion());
        //((TextView) findViewById(R.id.panel_txt_cantidad)).setText(String.valueOf(p.get_cantidad()));
        //((TextView) findViewById(R.id.panel_txt_precio)).setText(String.valueOf(new DecimalFormat("0.00").format(p.get_precio())));
        //((TextView) findViewById(R.id.panel_txt_total)).setText(String.valueOf(new DecimalFormat("0.00").format(p.get_total())));

        //limpiamos lo que haya en el panel
        txtValor_panel.setText("");

        //guardamos el pedido en edicion
        IniciaEdicionItem(p);

        ((PedidoAdapter) lstPedido.getAdapter()).notifyDataSetChanged();
    }

    private boolean obtenerProductoOrComboByIspCode(String ispcode) {
        try {


            return getProductByIspCode(ispcode);


        } catch (Exception ex) {
            ex.printStackTrace();
            //Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
            ShowMessage("Error", "Error: " + ex.getMessage());

            return false;
        }
    }



    private boolean ObtenerProductoBySKU(String code) {
        try {

            return ObtenerProductoByColumn(ProductTable.COLUMN_CODE, code);

        } catch (Exception ex) {
            //Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
            ShowMessage("Error", "Error: " + ex.getMessage());

            return false;
        }
    }

    private boolean getProductByIspCode(String code) {
        try {

            return ObtenerProductoByColumn(ProductTable.COLUMN_ISP_CODE, code);

        } catch (Exception ex) {
            ex.printStackTrace();
            ShowMessage("Error", "Error: " + ex.getMessage());

            return false;
        }
    }

    private boolean ObtenerProductoByColumn(String column_field, String value) {
        try {
            boolean pudoLeer = true;

            Product producto = new ProductDao(MainActivity.this.getContentResolver()).first(column_field + " = '" + value + "' AND removed = 0", null, null);

            if (producto != null) {
                onProductoSeleccionado(producto);
            } else {
                pudoLeer = false;
            }

            return pudoLeer;

        } catch (Exception ex) {
            //Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
            ShowMessage("Error", "Error: " + ex.getMessage());

            return false;
        }
    }

   /* private void IniciarFragmentSelectCombo(Product p) {
        if (findViewById(R.id.fragment) != null) {

            // Create a new Fragment to be placed in the activity layout
            ComboSelectFragment fragment = ComboSelectFragment.newInstance(p, FORMATO_DECIMAL);

            // Add the fragment to the 'fragment_container' FrameLayout
            getFragmentManager().beginTransaction()
                    .add(R.id.fragment, fragment)
                    .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                    .commit();
        }
    }
*/
    /*private void LeerProductos(Boolean agregaAccesoAdminProducto) {
        try {

            Cursor productos = getContentResolver().query(
                    Uri.parse("content://com.pds.ficle.ep.productos.contentprovider/productos"),
                    ProductoHelper.columnas,
                    "",
                    new String[]{},
                    "");

            this.productosList = new ArrayList<ProductoRow>();

            // recorremos los items
            if (productos.moveToFirst()) {
                do {

                    this.productosList.add(new ProductoRow(
                            productos.getInt(ProductoHelper.PRODUCTO_IX_ID),
                            productos.getString(ProductoHelper.PRODUCTO_IX_NOMBRE),
                            productos.getDouble(ProductoHelper.PRODUCTO_IX_PRECIO),
                            //getId(productos.getString(ProductoHelper.PRODUCTO_IX_IMAGEN), R.drawable.class)
                            //productos.getInt(ProductoHelper.PRODUCTO_IX_IMAGEN),
                            //R.drawable.ic_launcher
                            getResourceId("drawable/" + productos.getString(ProductoHelper.PRODUCTO_IX_IMAGEN), "drawable", getPackageName())
                    ));

                } while (productos.moveToNext());
            }

            if (agregaAccesoAdminProducto) {
                this.productosList.add(new ProductoRow(0, "Administracion", 0, R.drawable.ic_action_edit));
            }

            ProductoAdapter adapter = new ProductoAdapter(this, productosList);
            lstProductos.setAdapter(adapter);

            productos.close();

        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    public int getResourceId(String pVariableName, String pResourcename, String pPackageName) {
        try {
            return getResources().getIdentifier(pVariableName, null, pPackageName);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }*/

    private void LeerVentas() {
        LeerVentas(100);
    }

    private void LeerVentas(int max) {
        try {

            this.ventasList = VentaAbstract.ventasList(getContentResolver(), "codigo in ('PDV','STK') ", null, " _id DESC", null, null, max);

            /*List<Venta> ventas = new VentaDao(getContentResolver()).list("codigo = 'PDV' ", null, " _id DESC");
            List<Comprobante> comprobantes = new ComprobanteDao(getContentResolver()).list("codigo = 'PDV' ", null, " _id DESC");

            this.ventasList = new ArrayList<VentaAbstract>();
            this.ventasList.addAll(ventas);
            this.ventasList.addAll(comprobantes);*/

            // Creamos el boton - Cargar más
            if (btnLoadMore == null) {
                btnLoadMore = new Button(this);
                btnLoadMore.setText("Visualizar todas las ventas históricas");
                btnLoadMore.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        // get listview current position - used to maintain scroll position
                        int currentPosition = lstVentas.getFirstVisiblePosition();

                        LeerVentas(0);//todas

                        // Setting new scroll position
                        lstVentas.setSelectionFromTop(currentPosition + 1, 0);
                    }
                });
            }

            // Agregamos el boton como footer del listview
            if (max > 0 && ventasList.size() == max)
                lstVentas.addFooterView(btnLoadMore);
            else
                lstVentas.removeFooterView(btnLoadMore);

            //Agregamos el adapter con los datos a la lista
            VentasAdapter adapter = new VentasAdapter(this, ventasList, FORMATO_DECIMAL, config.PAIS, FORMATO_SIGNO_MONEDA);
            lstVentas.setAdapter(adapter);
            /*adapter.sort(new Comparator<VentaAbstract>() {
                @Override
                public int compare(VentaAbstract lhs, VentaAbstract rhs) {
                    return lhs.compareTo(rhs);
                }
            });*/

        } catch (Exception ex) {
            //Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
            ShowMessage("Error", "Error: " + ex.getMessage());

        }

    }

    private void CerrarItemAbierto() {
        //si el último item, tiene cantidad en 0 => asumimos cantidad 1
        if (pedido.size() > 0) {
            //hay items, tomamos el último
            if (pedido.get(pedido.size() - 1).get_precio() > 0) {
                //si tiene precio cargado
                if (pedido.get(pedido.size() - 1).get_cantidad() == 0) {
                    //como el último item no tiene cantidad, asumimos cantidad 1
                    pedido.get(pedido.size() - 1).set_cantidad(1);
                    pedido.get(pedido.size() - 1).actualiza_total(PREF_AJUSTE_SENCILLO);

                    //actualizamos los totales
                    ActualizarPedido();
                }
            }
        }
    }

    private void AgregarProductoAlPedido(
            int id_producto,
            String descripcion_producto,
            double cantidad_item,
            double precio_producto,
            double total_item,
            Product producto,
            boolean isFromCombo,
            boolean ingresoPrecio,
            boolean ingresoCantidad) {


        if (pedido == null) {
            //Toast.makeText(getApplicationContext(), getString(R.string.error_venta_no_activa), Toast.LENGTH_SHORT).show();
            ShowMessage(getString(R.string.title_sin_venta), getString(R.string.error_venta_no_activa));

        } else if (!VentaEnCheckout) {

            EstadoPanelEdicion(false);

            this.txtWelcome_panel.setVisibility(View.INVISIBLE);
            this.txtComentario_panel.setVisibility(View.VISIBLE);
            //this.btnDeshacer_panel.setVisibility(View.VISIBLE);

            Pedido p = null;//item a agregar o editar

            //buscamos si ya fue agregado el mismo producto en el pedido
            //* no aplica para: id = -1 y nombre GENERICO
            //* no aplica para: genericos y PESABLES
            //* no aplica para: productos ingresados por combos
            if (id_producto != -1 && !producto.isWeighable() && !producto.isGeneric() && !isFromCombo) {
                for (Pedido _item : pedido) {
                    if (_item.get_id_producto() == id_producto && !_item.isFromCombo()) {//no acumulamos con productos provenientes de combos
                        p = _item;
                        break;
                    }
                }
            }
            //agregamos nueva busqueda para los ingresados via sku pero que no están en catalogo (no es el GENERICO normal)
            if (id_producto == -1 && !descripcion_producto.equals(nombreProdGenerico) && !isFromCombo) {
                for (Pedido _item : pedido) {
                    if (_item.get_descripcion().equals(descripcion_producto) && !_item.isFromCombo()) {//no acumulamos con productos provenientes de combos
                        p = _item;
                        break;
                    }
                }
            }

            if (p == null) {//hay que crear uno

                //NUEVA REGLA PARA INGRESO DE ITEMS...
                //SI HAY OTRO:
                //TIENE PRECIO CERO?
                //  => SI => OMITIR INGRESO
                //  => NO => TIENE CANTIDAD CERO?
                //              => SI => EDITARLO CON Q = 1 Y PERMITIR NUEVO ITEM
                //              => NO => PERMITIR NUEVO ITEM

                boolean permitir_nuevo_item = false;

                if (pedido.size() > 0) {
                    //hay otro
                    if (pedido.get(pedido.size() - 1).get_precio() > 0) {
                        //es producto pesable o tiene precio cargado
                        if (pedido.get(pedido.size() - 1).esItemPesable() || pedido.get(pedido.size() - 1).get_cantidad() > 1) {
                            //tiene cantidad => permitimos ingresar
                            permitir_nuevo_item = true;
                        } else if (ingresoCantidad) {
                            if (!SubtotalItemSuperaMax(cantidad_item, pedido.get(pedido.size() - 1).get_precio())) {
                                //como el último item tiene cantidad 0 o 1, tomamos la ingresada para él
                                pedido.get(pedido.size() - 1).set_cantidad(cantidad_item);
                                pedido.get(pedido.size() - 1).actualiza_total(PREF_AJUSTE_SENCILLO);
                            } else {
                                //
                                //Toast.makeText(MainActivity.this, getString(R.string.error_subtotal_supera), Toast.LENGTH_SHORT).show();
                                ShowMessage(getString(R.string.title_control_item), getString(R.string.error_subtotal_supera));

                                return;
                            }
                        } else {
                            if (!SubtotalItemSuperaMax(1, pedido.get(pedido.size() - 1).get_precio())) {
                                //asumimos cantidad 1
                                pedido.get(pedido.size() - 1).set_cantidad(1);
                                pedido.get(pedido.size() - 1).actualiza_total(PREF_AJUSTE_SENCILLO);
                                //permitimos ingresar
                                permitir_nuevo_item = true;
                            } else {
                                //
                                //Toast.makeText(MainActivity.this, getString(R.string.error_subtotal_supera), Toast.LENGTH_SHORT).show();
                                ShowMessage(getString(R.string.title_control_item), getString(R.string.error_subtotal_supera));

                                return;
                            }
                        }
                    } else if (ingresoPrecio) {
                        if (!SubtotalItemSuperaMax(pedido.get(pedido.size() - 1).get_cantidad(), precio_producto)) {
                            //como el último item no tiene precio, tomamos el ingresado para él
                            pedido.get(pedido.size() - 1).set_precio(precio_producto);
                            pedido.get(pedido.size() - 1).actualiza_total(PREF_AJUSTE_SENCILLO);
                        } else {
                            //
                            //Toast.makeText(MainActivity.this, getString(R.string.error_subtotal_supera), Toast.LENGTH_SHORT).show();
                            ShowMessage(getString(R.string.title_control_item), getString(R.string.error_subtotal_supera));

                            return;
                        }
                    }

                } else
                    permitir_nuevo_item = true;


                if (permitir_nuevo_item) {
                    if (!SubtotalItemSuperaMax(cantidad_item, precio_producto)) {
                        p = new Pedido(pedido.size() + 1, id_producto, cantidad_item, descripcion_producto, precio_producto, total_item, producto, IVA_GRAVADO, isFromCombo);

                        pedido.add(p);

                        this.txtComentario_panel.setText("Añadida línea " + Formatos.FormateaDecimal(cantidad_item, FORMATO_DECIMAL_SECUND) + " x " + descripcion_producto);

                        this.lblNuevoPedido.setVisibility(View.GONE);

                        //se ingresó un item => sonamos
                        EmitirSonidoItem();
                    } else {
                        //
                        //Toast.makeText(MainActivity.this, getString(R.string.error_subtotal_supera), Toast.LENGTH_SHORT).show();
                        ShowMessage(getString(R.string.title_control_item), getString(R.string.error_subtotal_supera));

                        return;
                    }
                }

            } else {

                if (p.get_cantidad() + Math.max(cantidad_item, 1) > CANT_MAX) {
                    //Toast.makeText(MainActivity.this, getString(R.string.error_cantidad_no_valida), Toast.LENGTH_SHORT).show();
                    ShowMessage(getString(R.string.title_ingresa_cantidad), getString(R.string.error_cantidad_no_valida));
                    return;
                } else if (!SubtotalItemSuperaMax(p.get_cantidad() + Math.max(cantidad_item, 1), p.get_precio())) {
                    //hay que editarlo
                    p.set_cantidad(p.get_cantidad() + Math.max(cantidad_item, 1)); //<<--- como ahora los ingresos son con q == 0, es necesario forzar a sumar uno si encontró el producto
                    p.actualiza_total(PREF_AJUSTE_SENCILLO);
                } else {
                    //
                    //Toast.makeText(MainActivity.this, getString(R.string.error_subtotal_supera), Toast.LENGTH_SHORT).show();
                    ShowMessage(getString(R.string.title_control_item), getString(R.string.error_subtotal_supera));

                    return;
                }

            }


            if (p != null && p.esItemPesable()) {
                //si es pesbale, vamos a mantenerlo en edicion

                //si es pesable, vamos a ocultar el dummy para que podamos usar el teclado para ingresar el peso
                String unidad = p.get_producto().getUnit().equalsIgnoreCase("lt") ? "litros" : "gramos";
                txtComentario_panel.setText("Ingresar " + unidad + " y tecla 'OK / +'");
                dummy_view_b.setVisibility(View.GONE);

                IniciaEdicionItem(p);
            } else {
                //si estabamos editando algún item => cerramos su edicion
                FinalizaEdicionItem();
            }

            //IniciaEdicionItem(p);

            //actualizamos los totales
            ActualizarPedido();

            //validamos la venta al ir ingresando los items...
            ValidarVenta(false);

            this.btnEnter.setText("OK / +");
            this.btnEnter.setEnabled(true);
            this.btnEnter.setBackgroundResource(R.drawable.botones_teclado);

        /*
        if (total_item > 0) { //como se considera que está OK el item habilitamos el boton Enter
            this.btnEnter.setText("OK / +");
            this.btnEnter.setEnabled(true);
            this.btnEnter.setBackgroundColor(getResources().getColor(R.color.boton_gris));
        } else {
            this.btnEnter.setText("");
            this.btnEnter.setEnabled(false);
            //this.btnEnter.setBackgroundColor(getResources().getColor(R.color.boton_negro));
            btnEnter.setBackgroundResource(R.drawable.logo_bpos);
        }
        */
        }
    }

    private boolean SubtotalItemSuperaMax(double cantidad_item, double precio_producto) {
        return (cantidad_item * precio_producto) > SUBT_MAX;
    }

    private void ActualizarPedido() {
        ActualizarPedido(true);
    }

    private void ActualizarPedido(boolean scroll_to_bottom) {
        if (pedido != null) {

            //agregamos linea al detalle
            this.lstPedido.setAdapter(new PedidoAdapter(this.getBaseContext(), pedido, FORMATO_DECIMAL_SECUND));
            this.lstPedido.setVisibility(View.VISIBLE);

            if (scroll_to_bottom) {
                //envia las lineas al fondo para visualizar siempre lo ultimo
                lstPedido.post(new Runnable() {
                    public void run() {
                        lstPedido.setSelection(lstPedido.getCount() - 1);
                    }
                });
            }

            double total_pedido = 0;
            int cantidad_pedido = 0;
            double neto_pedido = 0;
            double iva_pedido = 0;

            //totalizamos
            for (int i = 0; i < pedido.size(); i++) {
                total_pedido += pedido.get(i).get_total();
                cantidad_pedido += pedido.get(i).esItemPesable() ? 1 : pedido.get(i).get_cantidad(); //si es un pesable, suma 1, no el peso
                neto_pedido += pedido.get(i).getNeto(FORMATO_DECIMAL); //calculamos el IVA por producto
                iva_pedido += pedido.get(i).getIVA(FORMATO_DECIMAL);
            }

            //this.txtTotalPedido.setText("$ " + new DecimalFormat("0.00").format(total_pedido));
            this.txtTotalPedido.setText(Formatos.FormateaDecimal(total_pedido, FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA));

            ((TextView) findViewById(R.id.pedido_txt_total_cant)).setText(String.valueOf(cantidad_pedido));
            //((TextView) findViewById(R.id.pedido_txt_total_monto)).setText(new DecimalFormat("0.00").format(total_pedido));
            ((TextView) findViewById(R.id.pedido_txt_total_monto)).setText(Formatos.FormateaDecimal(total_pedido, FORMATO_DECIMAL));

            showDesgloseImpuestos();

            //((TextView) findViewById(R.id.pedido_txt_subtotal)).setText(new DecimalFormat("0.00").format(total_pedido * 0.79));
            //usamos IVA del 19%
            //((TextView) findViewById(R.id.pedido_txt_subtotal)).setText(Formatos.FormateaDecimal(total_pedido / 1.19, Formatos.DecimalFormat_CH));
            //((TextView) findViewById(R.id.pedido_txt_impuesto)).setText(Formatos.FormateaDecimal(total_pedido * 0.19 / 1.19, Formatos.DecimalFormat_CH));

            ((TextView) findViewById(R.id.pedido_txt_subtotal)).setText(Formatos.FormateaDecimal(neto_pedido, FORMATO_DECIMAL));
            ((TextView) findViewById(R.id.pedido_txt_impuesto)).setText(Formatos.FormateaDecimal(iva_pedido, FORMATO_DECIMAL));


            SendTicketToDisplay();//actualizamos la vista del display
        } else {

            CloseTicketInDisplay();

            ((TextView) findViewById(R.id.pedido_txt_total_cant)).setText("0");
            ((TextView) findViewById(R.id.pedido_txt_total_monto)).setText("0");

            showDesgloseImpuestos(false);
        }
    }

    private void showDesgloseImpuestos() {
        showDesgloseImpuestos(PREF_SHOW_IVA);
    }

    private void showDesgloseImpuestos(boolean visible) {
        int visibility = visible ? View.VISIBLE : View.GONE;

        findViewById(R.id.pedido_lyt_impuestos).setVisibility(visibility);
        //((TextView) findViewById(R.id.pedido_lbl_desglose)).setVisibility(visibility);
        findViewById(R.id.pedido_lbl_impuesto).setVisibility(visibility);
        findViewById(R.id.pedido_txt_subtotal).setVisibility(visibility);
        findViewById(R.id.pedido_txt_impuesto).setVisibility(visibility);
    }

    private boolean ValidarVenta(boolean validacionEnCierre) {

        boolean ok = true;

        try {

            if (pedido.size() == 0) {
                throw new Exception(getString(R.string.error_venta_sin_productos));
            }

            if (validacionEnCierre) {
                //si estamos cerrando la venta => cerramos el ultimo item abierto (por si quedó con precio, pero con cantidad 0)
                CerrarItemAbierto();
            }

            //1 => que no haya TOTALES en $0
            //no aplica para: productos ingresados via combos
            double totalVenta = 0;

            for (Pedido p : pedido) {

                totalVenta += p.get_total();

                if (validacionEnCierre) {
                    if (p.get_total() == 0 && !p.isFromCombo()) {
                        p.hasError(true);

                        //refrescamos antes de salir por la excepcion
                        ((PedidoAdapter) lstPedido.getAdapter()).notifyDataSetChanged();
                        throw new Exception("Error: el item " + p.get_descripcion() + " posee un monto igual a cero");

                    } else
                        p.hasError(false);

                } else
                    p.hasError(false);

                ((PedidoAdapter) lstPedido.getAdapter()).notifyDataSetChanged();
            }

            //2 => control de total de la venta
            if (totalVenta > SUBT_MAX) {
                throw new Exception("Monto Total de la Venta supera el máximo permitido");
            }

            //2 => control de stock (NO PARA GENERICO)
            /*ProductDao productDao = new ProductDao(getContentResolver());
            for (Pedido p : pedido) {
                if (p.get_id_producto() != -1) {
                    double stock = productDao.find(p.get_id_producto()).getStock();

                    if (stock - p.get_cantidad() < 0)
                        throw new Exception("Error: el item " + p.get_descripcion() + " no posee unidades en stock suficientes");
                }
            }*/

        } catch (Exception ex) {
            ok = false;

            //Toast.makeText(MainActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
            ShowMessage("Error", "Error: " + ex.getMessage());


        } finally {
            return ok;
        }

    }


    private void GrabarVenta(Venta _venta, long fc_id, String fc_type, String fc_folio, String fc_validation, String fc_stamp) {

        //asociamos los datos de la FC recien generada
        _venta.setFC(fc_id, fc_type, fc_folio, fc_validation, fc_stamp);

        //VENTA -> grabar
        new VentaDao(getContentResolver()).save(_venta);

        //DETALLES -> grabar
        ProductDao productDao = new ProductDao(getContentResolver(), config.SYNC_STOCK ? 1 : 0);
        for (VentaDetalle det : _venta.getDetalles()) {
            ContentValues cvDetalle = new ContentValues();

            cvDetalle.put(VentaDetalleHelper.VENTA_DET_VENTA_ID, _venta.getId());
            cvDetalle.put(VentaDetalleHelper.VENTA_DET_PRODUCTO_ID, det.getIdProducto());
            cvDetalle.put(VentaDetalleHelper.VENTA_DET_CANTIDAD, det.getCantidad());
            cvDetalle.put(VentaDetalleHelper.VENTA_DET_PRECIO, det.getPrecio());
            cvDetalle.put(VentaDetalleHelper.VENTA_DET_TOTAL, det.getTotal());
            cvDetalle.put(VentaDetalleHelper.VENTA_DET_SKU, det.getSku());
            cvDetalle.put(VentaDetalleHelper.VENTA_DET_NETO, det.getNeto());
            cvDetalle.put(VentaDetalleHelper.VENTA_DET_IVA, det.getIva());

            getContentResolver().insert(Uri.parse("content://com.pds.ficle.ep.venta_detalles.contentprovider/venta_detalles"), cvDetalle);

            Stock.ProcesarProductoItem(det, productDao, getContentResolver());
        }

        FinalizarVenta(_venta);
    }

    private void CerrarVenta() {

        String medio_pago = ((RadioButton) this.rbtMedios_mp.findViewById(this.rbtMedios_mp.getCheckedRadioButtonId())).getText().toString();

        double monto = getTotalVenta();

        CerrarVenta("VTA", medio_pago, monto, 0, null);
    }

    private void CerrarVenta(String tipoVta, String medio_pago, Double total_venta, double vuelto, TipoDoc _tipoDoc) {

        Date now = new Date();


        //VentaAbstract venta = null;
        try {

            //CONTROL DE CORRELATIVIDAD DE VENTAS
            if (!ControlCorrelatividadVentas(tipoVta, now)) {
                throw new Exception("La fecha del comprobante que intenta registrar es anterior al último emitido");
            }

            double neto_venta = 0, iva_venta = 0;
            int total_items = 0;
            for (int i = 0; i < pedido.size(); i++) {
                total_items += pedido.get(i).esItemPesable() ? 1 : pedido.get(i).get_cantidad(); //si es un pesable, suma 1, no el peso
                neto_venta += pedido.get(i).getNeto(FORMATO_DECIMAL);
                iva_venta += pedido.get(i).getIVA(FORMATO_DECIMAL);
            }

            if (tipoVta.equals("VTA")) {

                //VENTA
                Venta _venta = new com.pds.common.model.Venta(
                        0,
                        total_venta,
                        total_items,
                        Formatos.FormateaDate(now, Formatos.DateFormat),
                        Formatos.FormateaDate(now, Formatos.TimeFormat),
                        "PDV",
                        this._user.getId(),
                        medio_pago,
                        neto_venta,
                        iva_venta
                );
                _venta.setVuelto(vuelto);
                //new VentaDao(getContentResolver()).save(_venta);

                //DETALLES
                List<VentaDetalle> detalles = new ArrayList<VentaDetalle>();
                //ProductDao productDao = new ProductDao(getContentResolver());
                for (int i = 0; i < pedido.size(); i++) {

                    /************/
                    VentaDetalle det = new VentaDetalle();

                    det.setProducto(pedido.get(i).get_producto());
                    det.setIdProducto(pedido.get(i).get_id_producto());
                    det.setCantidad(pedido.get(i).get_cantidad());
                    det.setPrecio(pedido.get(i).get_precio());
                    det.setTotal(pedido.get(i).get_total());
                    //det.setSku(pedido.get(i).get_producto() != null ? pedido.get(i).get_producto().getCode() : "");
                    if (pedido.get(i).get_producto() != null) {//si tengo producto
                        det.setSku(pedido.get(i).get_producto().getCode());
                    } else {//si es un GENERICO, vemos si es un producto ingresado via SKU que no está en catalogo
                        det.setSku(!pedido.get(i).get_descripcion().equals(nombreProdGenerico) ? pedido.get(i).get_descripcion() : "");
                    }
                    det.setNeto(pedido.get(i).getNeto(FORMATO_DECIMAL));
                    det.setIva(pedido.get(i).getIVA(FORMATO_DECIMAL));

                    detalles.add(det);
                    /************/

                    /*
                    ContentValues cvDetalle = new ContentValues();

                    cvDetalle.put(VentaDetalleHelper.VENTA_DET_VENTA_ID, _venta.getId());
                    cvDetalle.put(VentaDetalleHelper.VENTA_DET_PRODUCTO_ID, pedido.get(i).get_id_producto());
                    cvDetalle.put(VentaDetalleHelper.VENTA_DET_CANTIDAD, pedido.get(i).get_cantidad());
                    cvDetalle.put(VentaDetalleHelper.VENTA_DET_PRECIO, pedido.get(i).get_precio());
                    cvDetalle.put(VentaDetalleHelper.VENTA_DET_TOTAL, pedido.get(i).get_total());

                    getContentResolver().insert(Uri.parse("content://com.pds.ficle.ep.venta_detalles.contentprovider/venta_detalles"), cvDetalle);

                    //DESCONTAMOS EN STOCK (SINO ES EL GENERICO)
                    if (!pedido.get(i).esItemGenerico() && !pedido.get(i).esItemPesable()) {
                        Product _producto = productDao.find(pedido.get(i).get_id_producto());
                        _producto.setStock(_producto.getStock() - pedido.get(i).get_cantidad());
                        productDao.saveOrUpdate(_producto);
                    }
                    */
                }

                _venta.setDetalles(detalles);

                CrearProductosFueraCatalogo(_venta);

                //venta = _venta;

                if (PREF_EMITIR_VALE)
                    GenerarVale(_venta);
                else
                    GrabarVenta(_venta, 0, "", "", "", "");


            } else if (tipoVta.equals("CPB")) {

                Comprobante _comprobante = new Comprobante(
                        0,
                        total_venta,
                        total_items,
                        Formatos.FormateaDate(now, Formatos.DateFormat),
                        Formatos.FormateaDate(now, Formatos.TimeFormat),
                        "PDV",
                        this._user.getId(),
                        medio_pago,
                        neto_venta,
                        iva_venta
                );
                _comprobante.setVuelto(vuelto);

                new ComprobanteDao(getContentResolver()).save(_comprobante);

                //DETALLES
                ComprobanteDetalleDao comprobanteDetalleDao = new ComprobanteDetalleDao(getContentResolver());
                ProductDao productDao = new ProductDao(getContentResolver(), config.SYNC_STOCK ? 1 : 0);
                List<ComprobanteDetalle> detalles = new ArrayList<ComprobanteDetalle>();
                for (int i = 0; i < pedido.size(); i++) {

                    ComprobanteDetalle comprobanteDetalle = new ComprobanteDetalle();
                    comprobanteDetalle.setComprobante(_comprobante);
                    comprobanteDetalle.setProducto(pedido.get(i).get_producto());
                    comprobanteDetalle.setIdProducto(pedido.get(i).get_id_producto());
                    comprobanteDetalle.setCantidad(pedido.get(i).get_cantidad());
                    comprobanteDetalle.setPrecio(pedido.get(i).get_precio());
                    comprobanteDetalle.setTotal(pedido.get(i).get_total());
                    //comprobanteDetalle.setSku(pedido.get(i).get_producto() != null ? pedido.get(i).get_producto().getCode() : "");
                    if (pedido.get(i).get_producto() != null) {//si tengo producto
                        comprobanteDetalle.setSku(pedido.get(i).get_producto().getCode());
                    } else {//si es un GENERICO, vemos si es un producto ingresado via SKU que no está en catalogo
                        comprobanteDetalle.setSku(!pedido.get(i).get_descripcion().equals(nombreProdGenerico) ? pedido.get(i).get_descripcion() : "");
                    }
                    comprobanteDetalle.setNeto(pedido.get(i).getNeto(FORMATO_DECIMAL));
                    comprobanteDetalle.setIva(pedido.get(i).getIVA(FORMATO_DECIMAL));

                    detalles.add(comprobanteDetalle);

                    //ACTUALIZAMOS STOCK Y PRECIO (SINO ES GENERICO o PESABLE)
                    /*
                    if (!pedido.get(i).esItemGenerico()) {
                        Product _producto = productDao.find(pedido.get(i).get_id_producto());

                        if (!pedido.get(i).esItemPesable()) { //actualizamos stock en el caso que no sea un pesable
                            _producto.setStock(_producto.getStock() - pedido.get(i).get_cantidad());
                        }

                        if (_producto.getSalePrice() == 0) {//actualizamos el precio en el caso de que no tenga precio
                            _producto.setSalePrice(pedido.get(i).get_precio());
                        }

                        productDao.saveOrUpdate(_producto);
                    }*/
                }

                _comprobante.setDetalles(detalles);

                CrearProductosFueraCatalogo(_comprobante);

                for (ComprobanteDetalle det : _comprobante.getDetalles()) {
                    comprobanteDetalleDao.save(det);

                    Stock.ProcesarProductoItem(det, productDao, getContentResolver());
                }

                FinalizarVenta(_comprobante);
            }


        } catch (Exception ex) {
            //Toast.makeText(getApplicationContext(), "Se ha producido un error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
            ShowMessage("Error", "Se ha producido un error: " + ex.getMessage());

            CancelarVenta();
        }
    }

    private void FinalizarVenta(VentaAbstract venta) {
        try {

            String tipoVta = "";
            boolean comprobante = false;

            if (venta instanceof Venta)
                tipoVta = "VTA";
            else if (venta instanceof Comprobante) {
                tipoVta = "CPB";
                comprobante = true;
            }

            txtNroPedido.setText(Formato.FormateaDate(new Date(), Formato.ShortTimeFormat) + " - " + venta.getNroVenta());

            //boolean comprobante = venta.getType().equals(getStringFromResource(config.PAIS, "btn_presupuesto_min"));

            lblPedidoRegistrado.setVisibility(View.VISIBLE);
            lblPedidoRegistrado.setText(comprobante ? getStringFromResource(config.PAIS, "presup_registrado") : "VENTA REGISTRADA");
            btnImprimir.setVisibility(View.VISIBLE);
            btnImprimir.setText("IMPRIMIR");

            //ocultamos el botón imprimir en el caso de la emisión de un vale, dado que se imprime automatico
            if (venta instanceof Venta && ((Venta) venta).tieneValeAsociado())
                btnImprimir.setVisibility(View.INVISIBLE);

            findViewById(R.id.main_pad).setVisibility(View.GONE);

            if (!venta.getMedio_pago().equals("CTACTE")) {
                //registramos en caja, siempre que no sea en CTACTE del medio de pago
                Caja.RegistrarMovimientoCaja(MainActivity.this, TipoMovimientoCaja.DEPOSITO, venta.getTotal(), venta.getMedio_pago(), "", venta.getId(), tipoVta);
            } else {
                venta.setCliente(cliente_selected);

                //registramos en CTACTE
                CuentaCte cuentaCte = new CuentaCte();
                cuentaCte.setMonto(venta.getTotal());
                cuentaCte.setTipoMov(CuentaCte.TipoMovimientoCtaCte.DEUDA);
                cuentaCte.setFechaHora(new Date());
                cuentaCte.setCliente(cliente_selected);
                cuentaCte.setIdVenta(venta.getId());
                cuentaCte.setTipoVenta(tipoVta);

                new CuentaCteDao(getContentResolver()).save(cuentaCte);
            }

            Logger.RegistrarEvento(this, "i", venta.getType(config.PAIS) + " #" + venta.getNroVenta(), "Total: " + Formatos.FormateaDecimal(venta.getTotal(), FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA), "Medio Pago: " + venta.getMedio_pago());

            new PlaySound(this).PlaySound(R.raw.cash_reg1);

            Toast.makeText(this, comprobante ? getStringFromResource(config.PAIS, "presup_registrado_toast") : "Venta registrada con exito", Toast.LENGTH_SHORT).show();

            //si se emitio un COMPROBANTE (PRESUPUESTO) o no estamos en modo BOLETA => preparamos el ticket
            if (comprobante) {
                GeneraTicketVenta(venta, pedido, TEXT_ORIGINAL);

                postVenta(venta);

            } else if (PREF_EMITIR_VALE && ((Venta) venta).getFc_id() != 0) {
                ticket = null;//con esto evitamos imprimir de nuevo con la opcion de IMPRESION_AUTO activada
                ventaCurrent = (Venta) venta;
                PrintVale((Venta) venta);
                //como es async el printVale, hacemos un sleep de unos segundos
                //para que el vuelto permanezca visible
                final VentaAbstract ventaF = venta;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        postVenta(ventaF);
                    }
                }, 5000);

            } else {
                GeneraTicketVenta(venta, pedido, TEXT_ORIGINAL);

                postVenta(venta);
            }

            /*if (comprobante || !printBoleta) {
                if (comprobante) {
                    GeneraTicketVenta(venta, pedido, TEXT_ORIGINAL);
                } else if (PREF_EMITIR_VALE && ((Venta) venta).getFc_id() != 0) {
                    ticket = null;//con esto evitamos imprimir de nuevo con la opcion de IMPRESION_AUTO activada
                    ventaCurrent = (Venta) venta;
                    PrintVale((Venta) venta, "");
                } else {
                    GeneraTicketVenta(venta, pedido, TEXT_ORIGINAL);
                }
            } else {
                ticket = null;//con esto evitamos imprimir de nuevo con la opcion de IMPRESION_AUTO activada
                ventaCurrent = (Venta) venta;
            }*/

            /*if (printVale) {
                PrintVale((Venta) venta, "");
            } else if (comprobante || !printBoleta) {
                GeneraTicketVenta(venta, pedido, "ORIGINAL");
            } else {
                ticket = null;
                ventaCurrent = (Venta) venta;
            }*/

            /*if (PREF_IMPRIMIR_AUTOMATIC) {

                if (ticket != null)
                    btnImprimir.performClick(); // no vamos a imprimir la boleta, dado que ya la imprime el fragment

                txtComentario_panel.setText("Generar nueva venta para continuar...");
                pedido = null;


                //nueva venta
                accionClickNuevo.onClick(null);
            } else {
                txtComentario_panel.setText("Generar nueva venta para continuar...");
                pedido = null;

                if (!PREF_SHOW_CHECKOUT)
                    //usamos comportamiento que si está marcado el evitar checkout, reiniciamos la venta
                    //nueva venta
                    accionClickNuevo.onClick(null);
            }

            LeerVentas();*/

        } catch (Exception ex) {
            //Toast.makeText(getApplicationContext(), "Se ha producido un error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
            ShowMessage("Error", "Se ha producido un error: " + ex.getMessage());
        }

    }

    private void postVenta(VentaAbstract venta) {

        if (PREF_IMPRIMIR_AUTOMATIC) {

            if (ticket != null)
                btnImprimir.performClick(); // no vamos a imprimir la boleta, dado que ya la imprime el fragment

            txtComentario_panel.setText("Generar nueva venta para continuar...");
            pedido = null;


            //nueva venta
            accionClickNuevo.onClick(null);
        } else {
            txtComentario_panel.setText("Generar nueva venta para continuar...");
            pedido = null;

            if (!PREF_SHOW_CHECKOUT)
                //usamos comportamiento que si está marcado el evitar checkout, reiniciamos la venta
                //nueva venta
                accionClickNuevo.onClick(null);
        }

        LeerVentas();


    }

    private void CancelarVenta() {
        this.txtComentario_panel.setText(MSJ_VALIDAR_O_INGRESAR);
        this.txtValor_panel.setText("");
    }

    private ArrayList<Product> candidatos = new ArrayList<Product>();

    private void CrearProductosFueraCatalogo(VentaAbstract venta) {
        SubDepartmentDao subDepartmentDao = new SubDepartmentDao(getContentResolver());
        ProductDao productDao = new ProductDao(getContentResolver());
        SubDepartment SIN_CATEGORIZAR = null;


        candidatos = new ArrayList<Product>();

        Tax _gravado = new TaxDao(getContentResolver()).list("name = 'Gravado'", null, null).get(0);

        for (VentaDetalleAbstract det : venta.getDetallesAbstract()) {
            //si no tiene producto (GENERICO) y tiene SKU => es producto ingresado via lector que no está en catalogo...
            if (det.getProducto() == null && !TextUtils.isEmpty(det.getSku())) {

                //necesito traer el depto/subdepto "SIN CATEGORIZAR"
                if (SIN_CATEGORIZAR == null) {
                    //primero tomamos el "SIN CATEGORIZAR" de la nueva categorizacion
                    SIN_CATEGORIZAR = subDepartmentDao.first("codigo = '111001'", null, null);
                    //sino...intentamos tomar de la vieja categorizacion
                    if (SIN_CATEGORIZAR == null)
                        SIN_CATEGORIZAR = subDepartmentDao.first("codigo = '0001010000'", null, null);

                }


                Product p = new Product();
                p.setName(det.getSku());
                p.setCode(det.getSku());
                p.setSalePrice(det.getPrecio());
                p.setAutomatic(Product.TYPE_AUTOM.PROD_ALTA_MANUAL);//flag de producto manual
                p.setIvaEnabled(true);
                p.setTax(_gravado);
                p.setIva(_gravado.getAmount());

                //sino tengo el subdepto SIN CATEGORIZAR entonces no creo el producto
                if (SIN_CATEGORIZAR != null) {
                    p.setIdDepartment(SIN_CATEGORIZAR.getDepartment().getId());
                    p.setDepartment(SIN_CATEGORIZAR.getDepartment());
                    p.setIdSubdepartment(SIN_CATEGORIZAR.getId());
                    p.setSubDepartment(SIN_CATEGORIZAR);
                } else {
                    p.setIdDepartment(-1);
                    p.setDepartment(null);
                    p.setIdSubdepartment(-1);
                    p.setSubDepartment(null);
                }

                //damos de alta el producto primero
                productDao.save(p);

                Logger.RegistrarEvento(getContentResolver(), "i", "ALTA RAPIDA PRODUCTO", "SKU: " + p.getCode(), "id: " + String.valueOf(p.getId()));

                //agregamos el producto a la lista
                det.setProducto(p);
                det.setIdProducto(((Long) p.getId()).intValue());

                //p.setAutomatic(2); //esta es un flag temporal para determinar si se crea o no , luego al crear se pone en 0

                //agregamos el producto a la lista
                candidatos.add(p);
            }
        }
    }

   /* private void IdentificarProductosFueraCatalogo(VentaAbstract venta) {
        if (candidatos != null && candidatos.size() > 0) {
            //...proponemos alta automatica
            if (findViewById(R.id.fragment) != null) {

                // Create a new Fragment to be placed in the activity layout
                AltaRapidaFragment fragment = AltaRapidaFragment.newInstance(candidatos, R.id.fragment, FORMATO_DECIMAL);

                // Add the fragment to the 'fragment_container' FrameLayout
                getFragmentManager().beginTransaction()
                        .add(R.id.fragment, fragment)
                        .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                        .commit();
            }
        }
    }*/

    private boolean ControlCorrelatividadVentas(String tipoVta, Date fechaEmision) {

        if (tipoVta.equals("VTA")) {
            //controlamos contra la tabla de ventas
            return new VentaDao(getContentResolver()).ControlaCorrelatividadEmision(fechaEmision);
        } else if (tipoVta.equals("CPB")) {
            //controlamos contra la tabla de comprobantes
            return new ComprobanteDao(getContentResolver()).ControlaCorrelatividadEmision(fechaEmision);
        }

        return false;
    }

    public void GenerarVale(final Venta _venta) throws Exception {

        //----------------------------------
        //emision de VALE ONLINE
        Vale vale = new Vale();

        String version = ValeUtils.getAppVersion(this);

        double monto_afecto = 0d, monto_exento = 0d;
        //sumamos los items afectos y exentos
        for (VentaDetalle det : _venta.getDetalles()) {
            if (det.esGravado())
                monto_afecto += det.getTotal();
            else
                monto_exento += det.getTotal();
        }

        vale.setDatosGenerales(config, version, ValeUtils.GetUserID(config.SII_TERMINAL_ID, getContentResolver()), config.SII_MERCHANT_ID);
        vale.setDatosOperacion(_venta.getFechaHora_Date(), monto_afecto, monto_exento,
                ValeUtils.GetUltimoCodOperacion(MainActivity.this, getContentResolver()),
                ValeUtils.MapearMedioPago(_venta.getMedio_pago()));


        try {
            //Log.d("pdv", "exec task");
            new InformarValeOnlineTask(MainActivity.this, new InformarValeOnlineTask.InformarValeOnlineListener() {
                @Override
                public void onValeGenerado(String tipo_doc, String nro_folio, long cod_operacion, String nro_unico_vale, String hash_trx) {
                    //grabamos la venta asociando datos del vale
                    GrabarVenta(_venta, cod_operacion, tipo_doc, nro_unico_vale, hash_trx, nro_folio);
                }

                @Override
                public void onValeError() {
                    //error al emitir vale
                    //deshabilitamos la opcion de VALE ELECTRONICO, para que el usuario pueda decidir:
                    // si emitir REGISTRO MANUAL
                    // o cancelar la venta
                    //GrabarVenta(_venta, 0, "", "", "", "");
                    IniciarCheckout(_venta.getTotal(), true);
                }
            }).execute(vale);

        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, "Error al enviar vale:" + ex.toString(), Toast.LENGTH_LONG).show();
        }
        //----------------------------------
    }

    /*public void GenerarDocumento(TipoDoc tipoDoc, final Venta _venta) {

        if (findViewById(R.id.fragment) != null) {

            NuevaBoletaFragment fragment = NuevaBoletaFragment.newInstance(_venta, tipoDoc, R.id.fragment, NuevaBoletaFragment.FUNCION.EMISION, PREF_IMPRIMIR_AUTOMATIC, config.PRINTER_MODEL);
            fragment.setNuevaBoletaListener(new NuevaBoletaFragment.NuevaBoletaListener() {
                @Override
                public void onBoletaGenerada(long id, String doc_type, String folio, String v, String stamp) {

                    GrabarVenta(_venta, id, doc_type, folio, v, stamp);

                    //Toast.makeText(MainActivity.this, String.format("boleta %d  %s  %s", id, folio, v), Toast.LENGTH_LONG).show();
                }

                @Override
                public void onError(String mensaje) {
                    //Toast.makeText(MainActivity.this, mensaje, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onWarning(String mensaje) {
                    //Toast.makeText(MainActivity.this, mensaje, Toast.LENGTH_SHORT).show();
                }
            });

            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.fragment, fragment);
            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            ft.commit();

        }
    }*/

    private void ImprimirVenta(VentaAbstract venta, String header) {
        try {
            if (venta.getDetallesAbstract() == null) {
                venta.addDetalles(getContentResolver());
            }

            final TicketBuilder _ticket = new TicketBuilder(config);
            _ticket.set_timbre(null);
            _ticket.set_header(header);
            if (venta.getType(config.PAIS).equals(getStringFromResource(config.PAIS, "btn_presupuesto_min")))
                _ticket.set_subheader(getStringFromResource(config.PAIS, "btn_presupuesto"));
            _ticket.set_numero(venta.getNroVenta());
            _ticket.set_fecha(venta.getFecha());
            _ticket.set_hora(venta.getHora());
            //ticket.set_total(new DecimalFormat("0.00").format(venta.getTotal()));
            _ticket.set_total("TOTAL " + FORMATO_SIGNO_MONEDA + " " + Formatos.FormateaDecimal(venta.getTotal(), FORMATO_DECIMAL));

            //si es OTRO no pasamos el medio de pago
            if (!venta.getMedio_pago().equals("OTRO")) {
                if (venta.getMedio_pago().equals("CTACTE"))
                    _ticket.set_medio_pago(venta.getMedio_pago() + ": " + venta.getCliente().toString());
                else if (venta.getMedio_pago().equals(CheckoutFragment.MEDIO_EFECTIVO_ID)) {
                    String vuelto = "      Vuelto: " + FORMATO_SIGNO_MONEDA + " " + Formatos.FormateaDecimal(venta.getVuelto(), FORMATO_DECIMAL);
                    _ticket.set_medio_pago(venta.getMedio_pago() + (header.equals("ORIGINAL") ? vuelto : "")); //solo mostramos el vuelto en la original
                } else
                    _ticket.set_medio_pago(venta.getMedio_pago());
            }

            //ticket.set_usuario((new UsuarioHelper(this)).getById(this, venta.getUserid()).getNombre());
            Usuario user = UsuarioHelper.ObtenerUsuarioById(getContentResolver(), venta.getUserid());
            if (user != null)
                _ticket.set_usuario(user.getNombre());

            _ticket.set_leyenda_pie(PREF_PIE_TICKET);

            if (venta.getDetallesAbstract() != null) {
                for (int i = 0; i < venta.getDetallesAbstract().size(); i++) {
                    _ticket.add_linea(new LineaTicket(
                            FormateaCantidad(venta.getDetallesAbstract().get(i).getCantidad()),
                            venta.getDetallesAbstract().get(i).getDescripcion(),
                            Formatos.FormateaDecimal(venta.getDetallesAbstract().get(i).getPrecio(), FORMATO_DECIMAL),
                            Formatos.FormateaDecimal(venta.getDetallesAbstract().get(i).getTotal(), FORMATO_DECIMAL)
                    ));
                }
            }

            if (_printer == null)
                _printer = Printer.getInstance(config.PRINTER_MODEL, MainActivity.this);

            _printer.delayActivated = true;
            _printer.printerCallback = new Printer.PrinterCallback() {
                @Override
                public void onPrinterReady() {
                    _ticket.Print(_printer);

                    //TODO: Recordar...esto se habia agregado para la impresion de vales, para probar
                    //si cerrando el port al finalizar, mejoraba los problemas de impresion al emitir varios seguidos
                    //causa problemas con POWA, al cerrar la printer mientras está imprimiendo...
                    //lo comentamos por ahora, en la version 1.58 del PDV
                    _printer.end();
                    _printer = null;
                }
            };

            //iniciamos la printer
            if (!_printer.initialize()) {
                Toast.makeText(MainActivity.this, "Error al inicializar", Toast.LENGTH_SHORT).show();

                _printer.end();
                _printer = null;
            }


        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, "Error al imprimir: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    public void GeneraTicketVenta(VentaAbstract venta, List<Pedido> items, String header, Bitmap timbre) throws Exception {
        try {
            ticket = new TicketBuilder(config);
            ticket.set_timbre(timbre != null ? timbre : BitmapFactory.decodeResource(MainActivity.this.getResources(), R.drawable.powapos_img));
            ticket.set_header(header);
            if (venta.getType(config.PAIS).equals(getStringFromResource(config.PAIS, "btn_presupuesto_min")))
                ticket.set_subheader(getStringFromResource(config.PAIS, "btn_presupuesto"));
            ticket.set_numero(venta.getNroVenta());
            ticket.set_fecha(venta.getFecha());
            ticket.set_hora(venta.getHora());
            //ticket.set_total(new DecimalFormat("0.00").format(venta.getTotal()));
            ticket.set_total("TOTAL " + FORMATO_SIGNO_MONEDA + " " + Formatos.FormateaDecimal(venta.getTotal(), FORMATO_DECIMAL));

            //si es OTRO no pasamos el medio de pago
            if (!venta.getMedio_pago().equals("OTRO")) {
                if (venta.getMedio_pago().equals("CTACTE"))
                    ticket.set_medio_pago(venta.getMedio_pago() + ": " + venta.getCliente().toString());
                else if (venta.getMedio_pago().equals(CheckoutFragment.MEDIO_EFECTIVO_ID)) {
                    String vuelto = "      Vuelto: " + FORMATO_SIGNO_MONEDA + " " + Formatos.FormateaDecimal(venta.getVuelto(), FORMATO_DECIMAL);
                    ticket.set_medio_pago(venta.getMedio_pago() + (header.equals("ORIGINAL") ? vuelto : "")); //solo mostramos el vuelto en la original
                } else
                    ticket.set_medio_pago(venta.getMedio_pago());
            }

            //ticket.set_usuario((new UsuarioHelper(this)).getById(this, venta.getUserid()).getNombre());
            Usuario user = UsuarioHelper.ObtenerUsuarioById(getContentResolver(), venta.getUserid());
            if (user != null)
                ticket.set_usuario(user.getNombre());

            ticket.set_leyenda_pie(PREF_PIE_TICKET);

            if (items != null) {
                for (int i = 0; i < items.size(); i++) {
                    ticket.add_linea(new LineaTicket(
                            FormateaCantidad(items.get(i).get_cantidad()),
                            items.get(i).get_descripcion(),
                            Formatos.FormateaDecimal(items.get(i).get_precio(), FORMATO_DECIMAL),
                            Formatos.FormateaDecimal(items.get(i).get_total(), FORMATO_DECIMAL)
                    ));
                }
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    private String FormateaCantidad(double cantidad) {

        if (cantidad % 1 == 0)
            return String.valueOf(((Double) cantidad).intValue());
        else
            return Formatos.FormateaDecimal(cantidad, FORMATO_DECIMAL_SECUND);
    }

    private TipoDoc tc;
    private OnClickListener onTipoDocClick = new OnClickListener() {
        @Override
        public void onClick(View view) {
            int idTipoDoc = view.getId();

            tc = new TipoDocDao(getContentResolver()).find(idTipoDoc);

            getFragmentCheckoutInstance().IniciarFormaPagoSelected();

            //cambiamos el valor de esta variable
            registarComprobante = false;
        }
    };

    public void GeneraTicketVenta(VentaAbstract venta, List<Pedido> items, String header) throws Exception {

        if (venta instanceof Venta) {
            //venta
            if (((Venta) venta).getFc_id() != 0 && PREF_EMITIR_VALE) {
                if (PREF_EMITIR_VALE) {
                    if (header.equals(TEXT_ORIGINAL) && ((Venta) venta).tieneValeAsociado()) {
                        //ticket VALE
                        //PrintVale((Venta) venta);
                        ticket = null;
                    } else {
                        //ticket comun (reimpresion de vale)
                        GeneraTicketVenta(venta, items, header, null);
                        ticket.set_numero(String.valueOf(((Venta) venta).getFc_id()));
                    }
                }
            } else {
                //ticket comun
                GeneraTicketVenta(venta, items, header, null);
            }

        } else {
            //comprobante
            GeneraTicketVenta(venta, items, header, null);
        }

        //GeneraTicketVenta(venta, items, header, null);
    }

   /* private void PrintBoleta(final Venta _venta) {
        if (findViewById(R.id.fragment) != null) {

            NuevaBoletaFragment fragment = NuevaBoletaFragment.newInstance(_venta, null, R.id.fragment, NuevaBoletaFragment.FUNCION.IMPRESION, PREF_IMPRIMIR_AUTOMATIC, config.PRINTER_MODEL);
            fragment.setNuevaBoletaListener(new NuevaBoletaFragment.NuevaBoletaListener() {
                @Override
                public void onBoletaGenerada(long id, String doc_type, String folio, String v, String stamp) {
                }

                @Override
                public void onError(String mensaje) {
                    Toast.makeText(MainActivity.this, mensaje, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onWarning(String mensaje) {
                    Toast.makeText(MainActivity.this, mensaje, Toast.LENGTH_SHORT).show();
                }
            });

            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.fragment, fragment);
            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            ft.commit();

        }
    }*/

    private void PrintVale(Venta venta) {

        try {

            ValeTicketBuilder ticket = new ValeTicketBuilder(MainActivity.this, config, ValeUtils.getAppVersion(MainActivity.this), config.SII_MERCHANT_ID);

            if (venta.getDetalles() == null)
                venta.addDetalles(getContentResolver());

            ticket.LoadVenta(venta, FORMATO_DECIMAL_ID);

            /*if (_printer == null)
                _printer = Printer.getInstance(config.PRINTER_MODEL, MainActivity.this);*/

            ticket.Print(config.PRINTER_MODEL);

        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, "Error al imprimir vale: " + ex.toString(), Toast.LENGTH_SHORT).show();
        }
    }



}
