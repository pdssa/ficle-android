package com.pds.ficle.pdv.adapters;

/**
 * Created by Hernan on 24/06/2015.
 */
public class ProductoRow {
    private int _id;
    private String _nombre;
    private double _precio;
    private int _imagen;

    public void set_id(int _id) {
        this._id = _id;
    }

    public void set_nombre(String _nombre) {
        this._nombre = _nombre;
    }

    public void set_precio(double _precio) {
        this._precio = _precio;
    }

    public int get_id() {
        return _id;
    }

    public String get_nombre() {
        return _nombre;
    }

    public double get_precio() {
        return _precio;
    }

    public void set_imagen(int _imagen) {
        this._imagen = _imagen;
    }

    public int get_imagen() {
        return _imagen;

    }

    public ProductoRow(int id, String nombre, double precio, int imagen) {
        this._id = id;
        this._nombre = nombre;
        this._precio = precio;
        this._imagen = imagen;
    }
}
