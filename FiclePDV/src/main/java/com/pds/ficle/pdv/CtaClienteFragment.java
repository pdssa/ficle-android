package com.pds.ficle.pdv;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.app.Fragment;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Formato;
import com.pds.common.Formatos;
import com.pds.common.dao.ClienteDao;
import com.pds.common.dao.CuentaCteTotalesDao;
import com.pds.common.model.Cliente;
import com.pds.common.model.CuentaCteTotal;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CtaClienteFragment.OnCtaCteInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CtaClienteFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CtaClienteFragment extends Fragment {

    private static final String ARG_FORMATO = "ARG_FORMATO";
    private static final String ARG_MONEDA = "ARG_MONEDA";

    private OnCtaCteInteractionListener mListener;
    private Cliente cliente_edit;
    private List<Cliente> _list_clientes;
    private List<String> lineasLayout;
    private ListView lstClientes;

    private DecimalFormat FORMATO_DECIMAL;
    private String FORMATO_SIGNO_MONEDA;

    public View findViewById(int id) {
        return getView().findViewById(id);
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment CtaClienteFragment.
     */
    public static CtaClienteFragment newInstance( DecimalFormat _formato, String _moneda) {
        CtaClienteFragment fragment = new CtaClienteFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_FORMATO, _formato);
        args.putString(ARG_MONEDA, _moneda);
        fragment.setArguments(args);
        return fragment;
    }

    public CtaClienteFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            FORMATO_DECIMAL = (DecimalFormat)getArguments().getSerializable(ARG_FORMATO);
            FORMATO_SIGNO_MONEDA = getArguments().getString(ARG_MONEDA);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ctacliente, container, false);

        lstClientes = ((ListView) view.findViewById(R.id.cc_lstClientes));
        lstClientes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    //al seleccionar un cliente mostramos su detalle
                    cliente_edit = (Cliente) parent.getItemAtPosition(position);

                    MostrarInfoCliente(cliente_edit);

                    if (mListener != null) {
                        mListener.onClienteSeleccionado(cliente_edit);
                    }

                } catch (Exception ex) {
                    Toast.makeText(CtaClienteFragment.this.getActivity(), "Error al obtener informacion del cliente: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        });

        CargarClientes();

        return view;

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnCtaCteInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " debe implementar OnCtaCteInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        CargarClientes();
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void CargarClientes() {

        _list_clientes = new ClienteDao(getActivity().getContentResolver()).list(String.format("baja = %d", 0), null, "apellido, nombre");

        //iniciamos la carga de los clientes
        lstClientes.setAdapter(new ClientesArrayAdapter(getActivity(), _list_clientes));
    }

    /*private Double CalculaExposicionTotal() {
        //Exposicion total
        Double _deudaTotal = 0d;
        //TODO: no traer las deudas de clientes eliminados
        List<CuentaCteTotal> _deudores = new CuentaCteTotalesDao(getActivity().getContentResolver()).list();

        for (CuentaCteTotal _deuda : _deudores) {
            _deudaTotal += _deuda.getSaldo();
        }

        return _deudaTotal;
    }*/

    private void MostrarInfoCliente(Cliente cliente) {
        //Exposicion total
        //Double _deudaTotal = CalculaExposicionTotal();

        //info del cliente : VALORES DEFAULT
        Double deuda = 0d, disponible = cliente_edit.getLimite(); //sin deuda, y con limite disponible
        String fecha_ult_pago = "No registra", monto_ult_pago = "No registra";

        CuentaCteTotal _total = new CuentaCteTotalesDao(getActivity().getContentResolver()).find(cliente_edit.getId());

        if (_total != null) {  //si hay registros del cliente
            deuda = _total.getSaldo();
            disponible = _total.getDisponible();
            if (_total.getFecha_ult_pago() != null)
                fecha_ult_pago = Formatos.FormateaDate(_total.getFecha_ult_pago(), Formatos.DateFormat);
            if (_total.getMonto_ult_pago() != 0)
                monto_ult_pago = Formatos.FormateaDecimal(_total.getMonto_ult_pago(), FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA);
        }

        lineasLayout = null;

        EscribeLinea(cliente_edit.toString());
        EscribeLinea("RUT: " + (TextUtils.isEmpty(cliente_edit.getClaveFiscal()) ? "No Registra" : cliente_edit.getClaveFiscal()));
        EscribeLinea("");
        EscribeLinea("Limite: " + Formatos.FormateaDecimal(cliente_edit.getLimite(), FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA));
        EscribeLinea("Deuda actual: " + Formatos.FormateaDecimal(deuda, FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA));
        EscribeLinea("");
        EscribeLinea("Último pago");
        EscribeLinea("    Fecha: " + fecha_ult_pago);
        EscribeLinea("    Monto: " + monto_ult_pago);
        EscribeLinea("");
        EscribeLinea("Disponible: " + Formatos.FormateaDecimal(disponible, FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA));
        //EscribeLinea("");
        //EscribeLinea("Exposicion TOTAL: " + Formatos.FormateaDecimal(_deudaTotal, Formatos.DecimalFormat_CH, "$"));

        findViewById(R.id.cc_lista).setVisibility(View.GONE);
    }

    private void EscribeLinea(String linea) {
        EscribeLinea(linea, "");
    }

    private void EscribeLinea(String linea, String titulo) {

        if (this.lineasLayout == null)
            this.lineasLayout = new ArrayList<String>();

        this.lineasLayout.add(linea);

        EscribeLineas(this.lineasLayout, titulo);
    }

    private void EscribeLineas(List<String> lineas, String titulo) {

        //ocultamos la lista de clientes
        (findViewById(R.id.cc_lstClientes)).setVisibility(View.GONE);
        //mostramos el layout de lineas
        (findViewById(R.id.cc_lineas)).setVisibility(View.VISIBLE);

        if (!titulo.equals("")) {
            TextView txtTitulo = (TextView) findViewById(R.id.cc_txt_titulo);
            txtTitulo.setVisibility(View.VISIBLE);
            txtTitulo.setText(titulo);
        } else {
            TextView txtTitulo = (TextView) findViewById(R.id.cc_txt_titulo);
            txtTitulo.setVisibility(View.GONE);
            txtTitulo.setText("");
        }

        //blanqueamos
        ((TextView) findViewById(R.id.cc_txt1)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt2)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt3)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt4)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt5)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt6)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt7)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt8)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt9)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt10)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt11)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt12)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt13)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt14)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt15)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt16)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt17)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt18)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt19)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cc_txt20)).setVisibility(View.GONE);

        for (int i = 0; i < lineas.size(); i++) {

            TextView txtLinea = null;

            switch (i + 1) {
                case 1:
                    txtLinea = (TextView) findViewById(R.id.cc_txt1);
                    break;
                case 2:
                    txtLinea = (TextView) findViewById(R.id.cc_txt2);
                    break;
                case 3:
                    txtLinea = (TextView) findViewById(R.id.cc_txt3);
                    break;
                case 4:
                    txtLinea = (TextView) findViewById(R.id.cc_txt4);
                    break;
                case 5:
                    txtLinea = (TextView) findViewById(R.id.cc_txt5);
                    break;
                case 6:
                    txtLinea = (TextView) findViewById(R.id.cc_txt6);
                    break;
                case 7:
                    txtLinea = (TextView) findViewById(R.id.cc_txt7);
                    break;
                case 8:
                    txtLinea = (TextView) findViewById(R.id.cc_txt8);
                    break;
                case 9:
                    txtLinea = (TextView) findViewById(R.id.cc_txt9);
                    break;
                case 10:
                    txtLinea = (TextView) findViewById(R.id.cc_txt10);
                    break;
                case 11:
                    txtLinea = (TextView) findViewById(R.id.cc_txt11);
                    break;
                case 12:
                    txtLinea = (TextView) findViewById(R.id.cc_txt12);
                    break;
                case 13:
                    txtLinea = (TextView) findViewById(R.id.cc_txt13);
                    break;
                case 14:
                    txtLinea = (TextView) findViewById(R.id.cc_txt14);
                    break;
                case 15:
                    txtLinea = (TextView) findViewById(R.id.cc_txt15);
                    break;
                case 16:
                    txtLinea = (TextView) findViewById(R.id.cc_txt16);
                    break;
                case 17:
                    txtLinea = (TextView) findViewById(R.id.cc_txt17);
                    break;
                case 18:
                    txtLinea = (TextView) findViewById(R.id.cc_txt18);
                    break;
                case 19:
                    txtLinea = (TextView) findViewById(R.id.cc_txt19);
                    break;
                case 20:
                    txtLinea = (TextView) findViewById(R.id.cc_txt20);
                    break;

            }

            String texto = lineas.get(i);
            if (texto.startsWith("@C@")) {
                texto = texto.replace("@C@", "");
                txtLinea.setGravity(Gravity.CENTER);
            } else {
                txtLinea.setGravity(Gravity.LEFT);
            }
            if (texto.startsWith("@B@")) {
                texto = texto.replace("@B@", "");
                txtLinea.setTypeface(null, Typeface.BOLD);
            } else {
                txtLinea.setTypeface(null, Typeface.NORMAL);
            }

            txtLinea.setText(texto);
            txtLinea.setVisibility(View.VISIBLE);
        }

        //envia las lineas al fondo para visualizar siempre lo ultimo

        ((ScrollView) findViewById(R.id.scrollView)).post(new Runnable() {
            public void run() {
                ((ScrollView) findViewById(R.id.scrollView)).fullScroll(View.FOCUS_DOWN);
            }
        });
    }

    public class ClientesArrayAdapter extends ArrayAdapter<Cliente> {
        private final Context context;
        private List<Cliente> list;

        public ClientesArrayAdapter(Context context, List<Cliente> clientes) {
            super(context, R.layout.list_item_cliente, clientes);
            this.context = context;
            this.list = clientes;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(context)
                        .inflate(R.layout.list_item_cliente, parent, false);
            }

            TextView nombre = (TextView) convertView.findViewById(R.id.lblListItem);
            nombre.setText(list.get(position).toString());

            TextView limite = (TextView) convertView.findViewById(R.id.lblListItem2);
            limite.setText(Formatos.FormateaDecimal(list.get(position).getLimite(), FORMATO_DECIMAL, FORMATO_SIGNO_MONEDA));

            return convertView;
        }

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnCtaCteInteractionListener {
        public void onClienteSeleccionado(Cliente cliente);
    }

}
