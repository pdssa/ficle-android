package com.pds.ficle.pdv.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pds.ficle.pdv.R;

import java.util.List;

/**
 * Created by Hernan on 24/06/2015.
 */
public class ProductoAdapter extends ArrayAdapter<ProductoRow> {
    private Context context;
    private List<ProductoRow> datos;

    public ProductoAdapter(Context context, List<ProductoRow> datos) {
        super(context, android.R.layout.simple_list_item_1, datos);

        this.context = context;
        this.datos = datos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // En primer lugar "inflamos" una nueva vista, que será la que se
        // mostrará en la celda del ListView. Para ello primero creamos el
        // inflater, y después inflamos la vista.
        LayoutInflater inflater = LayoutInflater.from(context);
        View item = inflater.inflate(R.layout.producto_row, null);

        ProductoRow row = datos.get(position);

        // A partir de la vista, recogeremos los controles que contiene para
        // poder manipularlos.
        // Recogemos el ImageView y le asignamos una foto.
        ImageView imagen = (ImageView) item.findViewById(R.id.producto_row_icon);
        imagen.setImageResource(row.get_imagen());

        // Recogemos los TextView para mostrar datos
        ((TextView) item.findViewById(R.id.producto_row_nombre)).setText(row.get_nombre());
        (item.findViewById(R.id.producto_row_precio)).setVisibility(View.INVISIBLE);

        // Devolvemos la vista para que se muestre en el ListView.
        return item;

    }
}
