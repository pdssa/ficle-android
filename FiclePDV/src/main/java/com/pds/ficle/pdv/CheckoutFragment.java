package com.pds.ficle.pdv;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Formatos;
import com.pds.common.Usuario;
import com.pds.common.model.Cliente;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CheckoutFragment.CheckoutInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CheckoutFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CheckoutFragment extends Fragment  {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_MONTO_TOTAL = "ARG_MONTO_TOTAL";
    private static final String ARG_TEXTVIEW_PANEL = "ARG_TEXTVIEW_PANEL";
    private static final String ARG_USER = "ARG_USER";
    private static final String ARG_MEDIO_DEFAULT = "ARG_MEDIODEFAULT";
    private static final String ARG_FORMATO = "ARG_FORMATO";
    private static final String ARG_MONEDA = "ARG_MONEDA";
    private static final String ARG_INICIAR_AUTO = "ARG_INICIAR_AUTO";


    public static String MEDIO_EFECTIVO_ID = "EFECTIVO";
    private static String MEDIO_TARJETA_ID = "TARJETA CREDITO";
    private static String MEDIO_CHEQUE_ID = "CHEQUE";
    private static String MEDIO_CTACTE_ID = "CTACTE";
    private static String MEDIO_OTRO_ID = "OTRO";

    private String MEDIO_ACTUAL;

    private Double _montoTotal;

    public Double get_montoTotal() {
        return _montoTotal;
    }

    private TextView _textViewPanel;
    private Usuario _usuario;
    private boolean _iniciarFormaPagoSelectedAuto;

    //private LinkedHashMap<String, String> _medios;
    private ArrayList<MedioRow> _medios;

    private CheckoutInteractionListener mListener;
    private TextView txtMensaje;

    private String MEDIO_DEFAULT_ID = "";

    private DecimalFormat FORMATO;

    private String SIMBOLO_MONEDA;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param MontoTotal      Monto Total a cobrar.
     * @param TextViewPanelId Referencia al textview donde enviar mensajes.
     * @return A new instance of fragment CheckoutFragment.
     */
    public static CheckoutFragment newInstance(Double MontoTotal, int TextViewPanelId, Usuario User, String medioDefault, DecimalFormat _formato, String _moneda, boolean _iniciarFormaPagoAuto) {
        CheckoutFragment fragment = new CheckoutFragment();
        Bundle args = new Bundle();
        args.putDouble(ARG_MONTO_TOTAL, MontoTotal);
        args.putInt(ARG_TEXTVIEW_PANEL, TextViewPanelId);
        args.putSerializable(ARG_USER, User);
        args.putString(ARG_MEDIO_DEFAULT, medioDefault);
        args.putSerializable(ARG_FORMATO, _formato);
        args.putString(ARG_MONEDA, _moneda);
        args.putBoolean(ARG_INICIAR_AUTO, _iniciarFormaPagoAuto);
        fragment.setArguments(args);
        return fragment;
    }

    public CheckoutFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            _montoTotal = getArguments().getDouble(ARG_MONTO_TOTAL);
            int textViewId = getArguments().getInt(ARG_TEXTVIEW_PANEL);
            _textViewPanel = (TextView) getActivity().findViewById(textViewId);
            _textViewPanel.setVisibility(View.VISIBLE);
            _usuario = (Usuario) getArguments().getSerializable(ARG_USER);
            MEDIO_DEFAULT_ID = getArguments().getString(ARG_MEDIO_DEFAULT, "");
            FORMATO = (DecimalFormat)getArguments().getSerializable(ARG_FORMATO);
            SIMBOLO_MONEDA = getArguments().getString(ARG_MONEDA);
            _iniciarFormaPagoSelectedAuto = getArguments().getBoolean(ARG_INICIAR_AUTO);
        }

        //llenamos el array de medios de pago
        _medios = new ArrayList<MedioRow>();
        _medios.add(new MedioRow(MEDIO_EFECTIVO_ID, "Efectivo", MEDIO_DEFAULT_ID.equals(MEDIO_EFECTIVO_ID)));
        _medios.add(new MedioRow(MEDIO_TARJETA_ID, "Tarjeta Credito", MEDIO_DEFAULT_ID.equals(MEDIO_TARJETA_ID)));
        _medios.add(new MedioRow(MEDIO_CHEQUE_ID, "Cheque", MEDIO_DEFAULT_ID.equals(MEDIO_CHEQUE_ID)));
        _medios.add(new MedioRow(MEDIO_CTACTE_ID, "Cuenta Cliente", MEDIO_DEFAULT_ID.equals(MEDIO_CTACTE_ID)));
        _medios.add(new MedioRow(MEDIO_OTRO_ID, "Otro", MEDIO_DEFAULT_ID.equals(MEDIO_OTRO_ID)));
        /*_medios = new LinkedHashMap<String, String>();
        _medios.put(MEDIO_EFECTIVO_ID, "Efectivo");
        _medios.put(MEDIO_TARJETA_ID, "Tarjeta Credito");
        _medios.put(MEDIO_CHEQUE_ID, "Cheque");
        _medios.put(MEDIO_CTACTE_ID, "Cuenta Cliente");
        _medios.put(MEDIO_OTRO_ID, "Otro");*/
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_checkout, container, false);

        txtMensaje = (TextView) view.findViewById(R.id.chkout_txt_msj);
        ((TextView) view.findViewById(R.id.chkout_txt_total)).setText(Formatos.FormateaDecimal(_montoTotal, FORMATO, SIMBOLO_MONEDA));

        view.findViewById(R.id.chkout_btn_cancelar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MEDIO_ACTUAL = "";

                if (mListener != null) {
                    mListener.onPagoCancelado();
                }
            }
        });

        view.findViewById(R.id.chkout_btn_nuevo_cliente).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(_usuario.getId_perfil() != 1){
                    Toast.makeText(getActivity(), "Error: Usuario no autorizado a crear cuenta cliente", Toast.LENGTH_SHORT).show();
                }
                else {
                    Intent intent = new Intent();
                    intent.setClassName("com.pds.ficle.cuentacliente", "com.pds.ficle.cuentacliente.clientes");
                    startActivity(intent);
                }
            }
        });

        //((ListView) view.findViewById(R.id.chkout_lst_medios)).setOnItemClickListener(medioSeleccionado);

        ((ListView) view.findViewById(R.id.chkout_lst_medios)).setAdapter(new MediosAdapter(this.getActivity(), _medios, MEDIO_DEFAULT_ID));

        return view;
    }

    public void IniciarSiAutomatico(){
        if(_iniciarFormaPagoSelectedAuto)
            IniciarFormaPagoSelected();
    }
    public void IniciarFormaPagoSelected(){

        String medioSelected = ((MediosAdapter)((ListView) getView().findViewById(R.id.chkout_lst_medios)).getAdapter()).MEDIO_SELECTED;

        if(TextUtils.isEmpty(medioSelected)){
            Toast.makeText(getActivity(), "Debe seleccionar una forma de pago para continuar", Toast.LENGTH_SHORT).show();
        }
        else {
            procesaMedio(medioSelected);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (CheckoutInteractionListener) activity;

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " debe implementar CheckoutInteractionListener");
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(_iniciarFormaPagoSelectedAuto){
            IniciarFormaPagoSelected();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /*public AdapterView.OnItemClickListener medioSeleccionado = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            if(mListener != null){
                mListener.onMedioSeleccionado((((MediosAdapter) parent.getAdapter()).getItem(position).getName()));
            }

            parent.setVisibility(View.GONE);

            seleccionaMedio((((MediosAdapter) parent.getAdapter()).getItem(position).getName()));
        }
    };*/

    private void procesaMedio(String medioSelected){
        MEDIO_ACTUAL = medioSelected;

        if(mListener != null){
            mListener.onMedioSeleccionado(MEDIO_ACTUAL);
        }

        if (MEDIO_ACTUAL.equals(MEDIO_EFECTIVO_ID)) {
            //tenemos que solicitar opcionalmente que se ingrese con cuanto paga
            if(!_iniciarFormaPagoSelectedAuto) {
                //solo activamos estas opciones si es manual
                txtMensaje.setVisibility(View.VISIBLE);
                txtMensaje.setText("Ingrese pago...");
            }

            _textViewPanel.setText("Paga con " + Formatos.FormateaDecimal(_montoTotal, FORMATO, SIMBOLO_MONEDA) + " ?");

            if (mListener != null) {
                mListener.onTecladoInvocado(INPUT_EFECTIVO_MONTO);
            }
        } else if (MEDIO_ACTUAL.equals(MEDIO_CHEQUE_ID)) {
            //tenemos que solicitar datos opcionales, como son el numero y el monto del cheque
            txtMensaje.setVisibility(View.VISIBLE);
            txtMensaje.setText("Ingrese numero de cheque...");

            if (mListener != null) {
                mListener.onTecladoInvocado(INPUT_CHEQUE_NUMERO);
            }
        } else if (MEDIO_ACTUAL.equals(MEDIO_CTACTE_ID)) {
            //mostrar los clientes
            getView().findViewById(R.id.chkout_btn_nuevo_cliente).setVisibility(View.VISIBLE);
            _textViewPanel.setText("Total " + Formatos.FormateaDecimal(_montoTotal, FORMATO, SIMBOLO_MONEDA));

            if (getView().findViewById(R.id.chkout_fragment_container) != null) {

                getView().findViewById(R.id.chkout_fragment_container).setVisibility(View.VISIBLE);

                // Create a new Fragment to be placed in the activity layout
                CtaClienteFragment ctaClienteFragment = CtaClienteFragment.newInstance(FORMATO, SIMBOLO_MONEDA);

                // Add the fragment to the 'fragment_container' FrameLayout
                getFragmentManager().beginTransaction()
                        .add(R.id.chkout_fragment_container, ctaClienteFragment)
                        .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                        .commit();
            }

            getView().findViewById(R.id.chkout_lst_medios).setVisibility(View.GONE);

            if (mListener != null) {
                mListener.onTecladoInvocado(INPUT_OCULTAR_TECLADO);
            }

        } else if (MEDIO_ACTUAL.equals(MEDIO_TARJETA_ID)) {
            //por el momento...finalizamos directamente
            if (mListener != null) {
                mListener.onPagoFinalizado(_montoTotal, MEDIO_TARJETA_ID, 0d);
            }
        } else if (MEDIO_ACTUAL.equals(MEDIO_OTRO_ID)) {
            //finalizamos directamente
            if (mListener != null) {
                mListener.onPagoFinalizado(_montoTotal, MEDIO_OTRO_ID, 0d);
            }
        }
    }

    public static int INPUT_OCULTAR_TECLADO = 0;
    public static int INPUT_EFECTIVO_MONTO = 1;
    public static int INPUT_CHEQUE_NUMERO = 2;
    public static int INPUT_CHEQUE_MONTO = 3;


    public void setPagoExacto(){
        Double vuelto = 0d;

        _textViewPanel.setText("Vuelto " + Formatos.FormateaDecimal(vuelto, FORMATO, SIMBOLO_MONEDA));

        if (mListener != null) {
            mListener.onPagoFinalizado(_montoTotal, MEDIO_ACTUAL, vuelto);
        }

        MEDIO_ACTUAL = "";
    }

    public void setMontoIngresado(String valorIngresado, int inputType) {
        if (MEDIO_ACTUAL.equals(MEDIO_EFECTIVO_ID)) {

            String PAGA_CON =  "Paga con " + SIMBOLO_MONEDA;
            String valorIngresadoLimpio = valorIngresado;
            if(valorIngresado.contains(PAGA_CON)){
                valorIngresadoLimpio = valorIngresado.replace(PAGA_CON, " ").replace("?","").trim();//.replace(".","").replace(",",".").trim();
            }

            Double valor = Formatos.ParseaDecimal(valorIngresadoLimpio, FORMATO);

            if(valor < _montoTotal){
                Toast.makeText(getActivity(), "Error: El monto ingresado es inferior al total de la venta", Toast.LENGTH_SHORT).show();
            }
            else {

                Double vuelto = valor - _montoTotal;

                _textViewPanel.setText("Vuelto " + Formatos.FormateaDecimal(vuelto, FORMATO, SIMBOLO_MONEDA));

                if (mListener != null) {
                    mListener.onPagoFinalizado(_montoTotal, MEDIO_ACTUAL, vuelto);
                }

                MEDIO_ACTUAL = "";
            }
        } else if (MEDIO_ACTUAL.equals(MEDIO_CHEQUE_ID)) {

            if (inputType == INPUT_CHEQUE_NUMERO) {
                txtMensaje.setText("Ingrese monto del cheque...");

                String numeroCheque = ((String) valorIngresado);

                if (mListener != null) {
                    mListener.onTecladoInvocado(INPUT_CHEQUE_MONTO);
                }
            } else if (inputType == INPUT_CHEQUE_MONTO) {
                Double vuelto = Double.valueOf(valorIngresado) - _montoTotal;

                if (mListener != null) {
                    mListener.onPagoFinalizado(_montoTotal, MEDIO_ACTUAL, 0d);
                }

                MEDIO_ACTUAL = "";
            }
        }

    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface CheckoutInteractionListener {

        public void onMedioSeleccionado(String medio);

        public void onPagoFinalizado(Double total, String medio, Double vuelto);

        public void onPagoCancelado();

        public void onTecladoInvocado(int inputType);
    }

    class MedioRow {
        private boolean cheched;
        private String id;
        private String name;

        public boolean isCheched() {
            return cheched;
        }

        public void setCheched(boolean cheched) {
            this.cheched = cheched;
        }

        public String id() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTag() {
            return this.id;
        }

        MedioRow(String medioID, String medioName) {
            this(medioID, medioName, false);
        }

        MedioRow(String medioID, String medioName, boolean _cheched) {
            this.cheched = _cheched;
            this.id = medioID;
            this.name = medioName;
        }
    }

    public class MediosAdapter extends ArrayAdapter<MedioRow> {

        private Context context;
        private List<MedioRow> datos;
        public String MEDIO_SELECTED;

        public MediosAdapter(Context _context, ArrayList<MedioRow> _datos, String _medioDefault) {
            super(_context, 0, _datos);
            this.context = _context;
            //this.datos = new ArrayList();
            //this.datos.addAll(_datos.entrySet());
            this.datos = _datos;
            this.MEDIO_SELECTED = _medioDefault; //asignamos el seleccionado como el default
        }

        @Override
        public int getCount() {
            return datos.size();
        }

        /*@Override
        public Map.Entry<String, String> getItem(int position) {
            return (Map.Entry) datos.get(position);
        }*/

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final View result;
            // En primer lugar "inflamos" una nueva vista, que será la que se
            // mostrará en la celda del ListView. Para ello primero creamos el
            // inflater, y después inflamos la vista.
            if (convertView == null) {
                result = LayoutInflater.from(context).inflate(R.layout.chkout_list_item, parent, false);
            } else {
                result = convertView;
            }

            //Map.Entry<String, String> item = getItem(position);
            final MedioRow item =  getItem(position);

            // A partir de la vista, recogeremos los controles que contiene para
            // poder manipularlos.
            // Recogemos los TextView para mostrar datos
            ((TextView) result.findViewById(android.R.id.text1)).setText(item.getName());

            ((CheckBox) result.findViewById(R.id.chkSelected)).setTag(item.getTag());
            ((CheckBox) result.findViewById(R.id.chkSelected)).setChecked(item.isCheched());
            //result.findViewById(R.id.chkSelected).setFocusable(false);
            //result.findViewById(R.id.chkSelected).setClickable(false);

            /*((CheckBox) result.findViewById(R.id.chkSelected)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton view, boolean isChecked) {

                    if(isChecked)
                        clearChoices(); //limpiamos todos

                    if (item.getTag().equals(view.getTag().toString())) {
                        item.setCheched(isChecked);
                        MEDIO_SELECTED = isChecked ? item.id() : "";
                    }

                    notifyDataSetChanged();

                }
            });*/

            result.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clearChoices();

                    if (!item.isCheched()){
                        item.setCheched(true);
                        MEDIO_SELECTED = item.id();
                    }

                    notifyDataSetChanged();

                    //seleccionamos y si está activada la opcion manual, pasamos...
                    IniciarSiAutomatico();
                }
            });

            // Devolvemos la vista para que se muestre en el ListView.
            return result;
        }

        public void clearChoices(){
            for(MedioRow row : datos)
                row.setCheched(false);

            this.MEDIO_SELECTED = "";
        }
    }

}
