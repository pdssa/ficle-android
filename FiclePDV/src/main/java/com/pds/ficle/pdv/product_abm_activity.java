package com.pds.ficle.pdv;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Logger;
import com.pds.common.ProductoHelper;
import com.pds.common.integration.android.ScanIntegrator;
import com.pds.common.integration.android.ScanIntegrator2;
import com.pds.common.integration.android.ScanResult2;

public class product_abm_activity extends Activity {
    private TextView txtNombre;
    private TextView txtDescripcion;
    private TextView txtPrecio;
    private TextView txtCodigo;
    private Spinner spnCategoria;
    private Button btnGrabar;
    private Button btnCancelar;
    private Button btnEliminar;
    private ImageButton btnScan;
    private boolean modo_edit = false;
    private int id_edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {

            super.onCreate(savedInstanceState);
            setContentView(R.layout.product_abm);

            //asignamos los view
            this.txtNombre = (TextView) findViewById(R.id.product_abm_txt_nombre);
            this.txtDescripcion = (TextView) findViewById(R.id.product_abm_txt_descripcion);
            this.txtCodigo = (TextView) findViewById(R.id.product_abm_txt_codigo);
            this.txtPrecio = (TextView) findViewById(R.id.product_abm_txt_precio);
            this.spnCategoria = (Spinner) findViewById(R.id.product_abm_spn_categoria);
            this.btnGrabar = (Button) findViewById(R.id.product_abm_btn_grabar);
            this.btnCancelar = (Button) findViewById(R.id.product_abm_btn_cancelar);
            this.btnEliminar = (Button) findViewById(R.id.product_abm_btn_eliminar);
            this.btnScan = (ImageButton) findViewById(R.id.product_abm_btn_scan);


            //obtenemos los parametros pasamos a la actividad
            Bundle extras = getIntent().getExtras();

            int idProducto = -1;

            if (extras != null) {
                //vemos si es modo de edicion
                modo_edit = extras.getBoolean("modo_edit", false);

                idProducto = extras.getInt("_id");
            }

            //ajustamos los controles segun el modo de la actividad
            if (modo_edit) {

                CargarProducto(idProducto);

                btnGrabar.setText(R.string.product_abm_btn_actualizar_txt);

                if (this.id_edit == 1) {
                    //producto generico: no se puede eliminar ni actualizar
                    btnEliminar.setVisibility(View.GONE);
                    btnGrabar.setVisibility(View.GONE);
                } else {
                    btnEliminar.setVisibility(View.VISIBLE);
                }

            } else {
                btnGrabar.setText(R.string.product_abm_btn_grabar_txt);
                btnEliminar.setVisibility(View.INVISIBLE);
            }


            btnGrabar.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        grabar();
                    } catch (Exception e) {
                        Toast.makeText(v.getContext(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            });

            btnEliminar.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        int result = getContentResolver().delete(Uri.parse("content://com.pds.ficle.ep.productos.contentprovider/productos/" + String.valueOf(id_edit)), "", new String[]{});

                        if (result > 0)
                            Toast.makeText(view.getContext(), "Producto eliminado con exito", Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(view.getContext(), "No se pudo eliminar el producto!", Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        Toast.makeText(view.getContext(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            });


            btnCancelar.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish(); //cancela la actividad actual y retorna a la anterior
                }
            });


            btnScan.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        //instanciamos la clase de integracion con el scan
                        ScanIntegrator scanIntegrator = new ScanIntegrator(product_abm_activity.this);
                        //iniciamos scan
                        scanIntegrator.initiateScan();

                    } catch (Exception e) {
                        Toast.makeText(view.getContext(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            });


        /*
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }*/
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
            //Dialog.Alert(getApplicationContext(), "Error!" , e.getMessage());
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        try {

            super.onActivityResult(requestCode, resultCode, intent);

            if (intent != null) {

                //obtenemos el resultado del scan
                String scanningResult = ScanIntegrator.parseActivityResult(requestCode, resultCode, intent);
                //String scanContent = "555351313";
                //revisamos si es un valor valido
                if (scanningResult != null) {
                    //lo asignamos
                    this.txtCodigo.setText(scanningResult);

                } else {

                    //datos invalidos o scan cancelado
                    Toast.makeText(product_abm_activity.this,
                            "No se recibieron datos del scan!", Toast.LENGTH_SHORT).show();

                }

            }
            else{

                //datos invalidos o scan cancelado
                Toast.makeText(product_abm_activity.this,
                        "No se recibieron datos del scan!", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception ex) {
            Toast.makeText(product_abm_activity.this,
                    "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void CargarProducto(int idProducto) {
        try {

            Cursor productos = getContentResolver().query(
                    Uri.parse("content://com.pds.ficle.ep.productos.contentprovider/productos/" + String.valueOf(idProducto)),
                    ProductoHelper.columnas,
                    "",
                    new String[]{},
                    "");

            // recorremos los items
            if (productos.moveToFirst()) {
                do {

                    this.id_edit = productos.getInt(ProductoHelper.PRODUCTO_IX_ID);

                    this.txtNombre.setText(productos.getString(ProductoHelper.PRODUCTO_IX_NOMBRE));
                    this.txtCodigo.setText(productos.getString(ProductoHelper.PRODUCTO_IX_CODIGO));
                    this.txtPrecio.setText(productos.getString(ProductoHelper.PRODUCTO_IX_PRECIO));
                    this.txtDescripcion.setText(productos.getString(ProductoHelper.PRODUCTO_IX_DESCRIPCION));


                } while (productos.moveToNext());
            }

            productos.close();

        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    private void grabar() throws Exception {
        try {

            //reseteamos los errores
            txtNombre.setError(null);
            //spnCategoria.setError(null);
            txtPrecio.setError(null);

            boolean cancel = false;
            View focusView = null;

            // Validamos los datos de entrada
            // Revisamos el nombre
            if (TextUtils.isEmpty(txtNombre.getText().toString())) {
                txtNombre.setError(getString(R.string.error_field_required));
                focusView = txtNombre;
                cancel = true;
            }

            // Revisamos el precio
            if (TextUtils.isEmpty(txtPrecio.getText().toString())) {
                txtPrecio.setError(getString(R.string.error_field_required));
                focusView = txtPrecio;
                cancel = true;
            } else if (Double.parseDouble(txtPrecio.getText().toString()) < 0) {
                txtPrecio.setError("Ingrese un precio");
                focusView = txtPrecio;
                cancel = true;
            }


            if (cancel) {
                //hubo un error mostramos los errores en pantalla
                focusView.requestFocus();
            } else {

                ContentValues cv = new ContentValues();

                cv.put(ProductoHelper.PRODUCTO_NOMBRE, txtNombre.getText().toString());
                cv.put(ProductoHelper.PRODUCTO_DESCRIPCION, txtDescripcion.getText().toString());
                cv.put(ProductoHelper.PRODUCTO_PRECIO, txtPrecio.getText().toString());
                cv.put(ProductoHelper.PRODUCTO_CODIGO, txtCodigo.getText().toString());
                cv.put(ProductoHelper.PRODUCTO_CATEGORIA, spnCategoria.getSelectedItemId());

                if (modo_edit) {
                    int result = getContentResolver().update(Uri.parse("content://com.pds.ficle.ep.productos.contentprovider/productos/" + String.valueOf(this.id_edit)), cv, "", new String[]{});

                    if (result > 0){
                        Toast.makeText(this, "Producto actualizado con exito", Toast.LENGTH_SHORT).show();

                        finish(); //cancela la actividad actual y retorna a la anterior
                    }
                    else
                        Toast.makeText(this, "No se pudo actualizar el producto!", Toast.LENGTH_SHORT).show();

                } else {

                    Uri newProdUri = getContentResolver().insert(Uri.parse("content://com.pds.ficle.ep.productos.contentprovider/productos"), cv);

                    Logger.RegistrarEvento(this, "i", "Alta Producto", "Producto id:" + newProdUri.getPathSegments().get(1));

                    Toast.makeText(this, "Producto creado con exito", Toast.LENGTH_SHORT).show();

                    finish(); //cancela la actividad actual y retorna a la anterior
                }
            }


        } catch (Exception ex) {
            throw ex;
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.product_abm_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    /*
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.product_abm, container, false);
            return rootView;
        }
    }
    */

}
