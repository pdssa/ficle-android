package com.pds.ficle.pdv;

import com.pds.common.Formatos;
import com.pds.common.model.Product;
import com.pds.common.model.Tax;

import java.text.DecimalFormat;

/**
 * Created by Hernan on 23/11/13.
 */
public class Pedido {
    private int _nro_linea;
    private int _id_producto;
    private double _cantidad;
    private String _descripcion;
    private double _precio;
    private double _total;
    private Product _producto;
    private boolean _fromCombo;

    private Tax IVA_GRAVADO;

    private boolean _inEditMode;
    private boolean _hasError;

    public boolean isFromCombo() {
        return _fromCombo;
    }

    public void setFromCombo(boolean _fromCombo) {
        this._fromCombo = _fromCombo;
    }

    public boolean isInEditMode() {
        return _inEditMode;
    }

    public void setIsInEditMode(boolean _inEditMode) {
        this._inEditMode = _inEditMode;
    }

    public boolean hasError(){
        return _hasError;
    }

    public void hasError(boolean _hasError) {
        this._hasError = _hasError;
    }

    public Product get_producto() {
        return _producto;
    }

    public void set_producto(Product _producto) {
        this._producto = _producto;
    }

    public void set_precio(double _precio) {
        this._precio = _precio;
    }

    public void set_cantidad(double _cantidad) {
        this._cantidad = _cantidad;
    }

    public void set_descripcion(String _descripcion) {
        this._descripcion = _descripcion;
    }

    public void set_id_producto(int _id_producto) {
        this._id_producto = _id_producto;
    }

    public void set_nro_linea(int _nro_linea) {
        this._nro_linea = _nro_linea;
    }

    public void set_total(double _total) {
        this._total = _total;
    }

    public double get_precio() {
        return _precio;
    }

    public double get_total() {
        return _total;
    }

    public double get_cantidad() {
        return _cantidad;
    }

    public int get_id_producto() {
        return _id_producto;
    }

    public int get_nro_linea() {
        return _nro_linea;
    }

    public String get_descripcion() {
        return _descripcion;
    }

    public void actualiza_total(boolean ajusta_sencillo) {
        double total = this._cantidad * this._precio;

        if(esItemPesable() && ajusta_sencillo)
            this._total = total - total % 10;//por ejemplo, total = 372 => total = 370 (ajusta al entero menor desde 1 al 9, ej: 371 a 379)
        else
            this._total = total;

    }

    public boolean puedeEditarPrecio(boolean PREF_ACTUAL_PRECIO) {

        if (_id_producto != -1) {
            if (_producto != null) {
                if( _producto.isGeneric())
                    return true;
                else
                    return PREF_ACTUAL_PRECIO;
            } else
                return false;
        } else
            return true;

        /*
        if (_id_producto != -1) {
            if (_producto != null) {
                return _producto.isGeneric() || _producto.getSalePrice() == 0;
            } else
                return false;
        } else
            return true;
        */
    }

    public boolean esItemGenerico() {
        if (_producto != null)
            return _producto.isGeneric();
        else
            return false;
    }

    public boolean esItemPesable() {
        if (_producto != null)
            return _producto.isWeighable();
        else
            return false;
    }

    public double getAlicIVA() {
        if (_id_producto == -1) {
            //asumimos AFECTO, en 19%
            return IVA_GRAVADO.getAmount().doubleValue() / 100 ;//return 0.19;
        } else if (_producto != null) {
            if (_producto.getIdTax() == 4) //EXENTO
                return 0;
            else //GRAVADO, NO GRAVADO, INCLUIDO
            {
                return _producto.getIva() / 100;
            }
        }
        else{
            return 0;
        }
    }

    public double getIVA(DecimalFormat format){
        return Formatos.RedondeaDecimal(get_total() * getAlicIVA() / (1 + getAlicIVA()), format);
    }

    public double getNeto(DecimalFormat format){
        return Formatos.RedondeaDecimal(get_total() / (1 + getAlicIVA()), format);
    }

    public Pedido(int nro_linea,
                  int id_producto,
                  double cantidad,
                  String descripcion,
                  double precio,
                  double total,
                  Product producto,
                  Tax iva_gravado,
                  boolean isFromCombo) {
        this._nro_linea = nro_linea;
        this._id_producto = id_producto;
        this._cantidad = cantidad;
        this._descripcion = descripcion;
        this._precio = precio;
        this._total = total;
        this._producto = producto;
        this.IVA_GRAVADO = iva_gravado;
        this._fromCombo = isFromCombo;
    }


}
