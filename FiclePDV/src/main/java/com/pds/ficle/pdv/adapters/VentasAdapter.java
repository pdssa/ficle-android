package com.pds.ficle.pdv.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pds.common.Formatos;
import com.pds.common.model.VentaAbstract;
import com.pds.ficle.pdv.R;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by Hernan on 24/06/2015.
 */
public class VentasAdapter extends ArrayAdapter<VentaAbstract> {
    private Context context;
    private List<VentaAbstract> datos;
    private DecimalFormat formato;
    private String moneda;
    private String pais;

    public VentasAdapter(Context context, List<VentaAbstract> datos, DecimalFormat formato, String pais, String moneda) {
        super(context, android.R.layout.simple_list_item_1, datos);

        this.context = context;
        this.datos = datos;
        this.formato = formato;
        this.pais = pais;
        this.moneda = moneda;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // En primer lugar "inflamos" una nueva vista, que será la que se
        // mostrará en la celda del ListView. Para ello primero creamos el
        // inflater, y después inflamos la vista.
        LayoutInflater inflater = LayoutInflater.from(context);
        View item = inflater.inflate(R.layout.venta_list_item, null);

        VentaAbstract row = datos.get(position);

        // A partir de la vista, recogeremos los controles que contiene para
        // poder manipularlos.
        // Recogemos los TextView para mostrar datos
        ((TextView) item.findViewById(R.id.venta_list_item_numero)).setText(row.getType(pais) + " " + row.getNroVenta());



        if(row.isAnulada()) {
            ((TextView) item.findViewById(R.id.venta_list_item_numero)).setTextColor(this.context.getResources().getColor(R.color.boton_rojo));
            ((TextView) item.findViewById(R.id.venta_list_item_subtext)).setText(" ANULADA ");
            ((TextView) item.findViewById(R.id.venta_list_item_subtext)).setVisibility(View.VISIBLE);
            ((TextView) item.findViewById(R.id.venta_list_item_subtext)).setTextColor(this.context.getResources().getColor(R.color.boton_rojo));

        }
        else {
            ((TextView) item.findViewById(R.id.venta_list_item_numero)).setTextColor(this.context.getResources().getColor(R.color.texto_black));
            ((TextView) item.findViewById(R.id.venta_list_item_subtext)).setText("");
            ((TextView) item.findViewById(R.id.venta_list_item_subtext)).setVisibility(View.GONE);
        }

        ((TextView) item.findViewById(R.id.venta_list_item_fecha)).setText(row.getFecha() + " " + row.getHora());

        ((TextView) item.findViewById(R.id.venta_list_item_cantidad)).setText("Items: " + String.valueOf(row.getCantidad()));

        ((TextView) item.findViewById(R.id.venta_list_item_total)).setText("Total: " + Formatos.FormateaDecimal(row.getTotal(), formato, moneda));

        ((TextView) item.findViewById(R.id.venta_list_item_medio)).setText(row.getMedio_pago());

        // Devolvemos la vista para que se muestre en el ListView.
        return item;

    }

}
