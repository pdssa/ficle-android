/*
package com.pds.ficle.pdv;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.pds.common.Formatos;
import com.pds.common.model.Combo;
import com.pds.common.model.Product;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


*/
/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ComboSelectFragment.OnComboSelectFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ComboSelectFragment#newInstance} factory method to
 * create an instance of this fragment.
 *//*

public class ComboSelectFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "product";
    private static final String ARG_PARAM2 = "formato";

    private Product product;
    private DecimalFormat FORMATO;

    private ListView lstItems;
    private OnComboSelectFragmentInteractionListener mListener;

    public void setOnComboSelectFragmentInteractionListener(OnComboSelectFragmentInteractionListener mListener) {
        this.mListener = mListener;
    }

    */
/**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param _product Producto primario de algun combo
     * @return A new instance of fragment ComboSelectFragment.
     *//*

    public static ComboSelectFragment newInstance(Product _product, DecimalFormat _formato) {
        ComboSelectFragment fragment = new ComboSelectFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, _product);
        args.putSerializable(ARG_PARAM2, _formato);
        fragment.setArguments(args);
        return fragment;
    }

    public ComboSelectFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            product = getArguments().getParcelable(ARG_PARAM1);
            FORMATO = (DecimalFormat) getArguments().getSerializable(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_combo_select, container, false);

        view.findViewById(R.id.frg_combo_btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cerrar();
            }
        });

        lstItems = (ListView) view.findViewById(R.id.frg_combo_lst_items);

        lstItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object item = lstItems.getAdapter().getItem(position);

                onItemSelected(item);
            }
        });

        List<Object> items = getListItems(product);

        if (items.size() == 1)
            onItemSelected(items.get(0));//si tuviese un solo registro asociado, devolvemos dicho registro automaticamente
        else
            lstItems.setAdapter(new ObjectArrayAdapter(getActivity(), items));

        return view;
    }

    public void onItemSelected(Object item) {
        if (mListener != null) {

            if (item instanceof Product)
                mListener.onProductSelected((Product) item);

            if (item instanceof Combo)
                mListener.onComboSelected((Combo) item);

            Cerrar();
        }
    }

    private void Cerrar() {

        FragmentManager fm = getActivity().getFragmentManager();

        Fragment fragment = fm.findFragmentById(R.id.fragment);

        FragmentTransaction ft = fm.beginTransaction();

        ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);

        ft.remove(fragment);

        ft.commit();
    }

    private List<Object> getListItems(Product _product) {

        List<Object> items = new ArrayList<Object>();

        //agregamos los combos donde tengan como producto principal al producto seleccionado
        items.addAll(new ComboDao(getActivity().getContentResolver()).list(String.format("producto_ppal_id = %d and baja = 0", _product.getId()), null, null));

        //agregamos el producto individual
        items.add(0, _product);

        return items;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnComboSelectFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " debe implementar OnComboSelectFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public class ObjectArrayAdapter extends ArrayAdapter<Object> {
        private final Context context;
        private List<Object> items;

        public ObjectArrayAdapter(Context context, List<Object> _items) {
            super(context, com.pds.common.R.layout.list_item_combo, _items);
            this.context = context;
            this.items = _items;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(context)
                        .inflate(com.pds.common.R.layout.list_item_combo, parent, false);
            }

            Object item = items.get(position);

            TextView nameTextView = (TextView) convertView.findViewById(com.pds.common.R.id.lblListItem);
            TextView priceTextView = (TextView) convertView.findViewById(com.pds.common.R.id.lblListItemPrice);

            if (item instanceof Product) {
                nameTextView.setText(((Product) item).getName());
                priceTextView.setText(Formatos.FormateaDecimal(((Product) item).getSalePrice(), FORMATO, "$"));
            }

            if (item instanceof Combo) {
                nameTextView.setText(((Combo) item).getNombre());
                priceTextView.setText(Formatos.FormateaDecimal(((Combo) item).getPrecio(getActivity().getContentResolver()), FORMATO, "$"));
            }

            return convertView;
        }

    }

    */
/**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     *//*

    public interface OnComboSelectFragmentInteractionListener {
        public void onComboSelected(Combo c);

        public void onProductSelected(Product p);
    }

}
*/
