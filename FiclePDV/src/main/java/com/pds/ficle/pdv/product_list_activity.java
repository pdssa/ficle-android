package com.pds.ficle.pdv;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.os.Build;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.ProductoHelper;
import com.pds.ficle.pdv.adapters.ProductoRow;

import java.util.ArrayList;
import java.util.List;

public class product_list_activity extends Activity {
    private ListView lstProductos;
    private List<ProductoRow> productosList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_list);

        Button btnNuevo = (Button) findViewById(R.id.product_list_btn_nuevo);
        btnNuevo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), product_abm_activity.class);
                startActivity(i);
            }
        });

        Button btnCancelar = (Button) findViewById(R.id.product_list_btn_cancelar);
        btnCancelar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish(); //cancela la actividad actual y retorna a la anterior
            }
        });

        // migramos el metodo al evento onResume() para refrescar la lista tanto al crear como al retornar
        lstProductos = (ListView) findViewById(R.id.product_list_lst_view);
        DefaultEmptyList();
        lstProductos.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent i = new Intent(getApplicationContext(), product_abm_activity.class);

                i.putExtra("modo_edit", true);
                i.putExtra("_id", productosList.get(position).get_id());

                startActivity(i);
            }
        });



        /*
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }*/
    }

    private void CargarProductos() {
        try {

            Cursor productos = getContentResolver().query(
                    Uri.parse("content://com.pds.ficle.ep.productos.contentprovider/productos"),
                    ProductoHelper.columnas,
                    "",
                    new String[]{},
                    "");

            // recorremos los items
            this.productosList = new ArrayList<ProductoRow>();

            // recorremos los items
            if (productos.moveToFirst()) {
                do {

                    this.productosList.add(new ProductoRow(
                            productos.getInt(ProductoHelper.PRODUCTO_IX_ID),
                            productos.getString(ProductoHelper.PRODUCTO_IX_NOMBRE),
                            "$ " + productos.getString(ProductoHelper.PRODUCTO_IX_PRECIO),
                            1
                    ));

                } while (productos.moveToNext());
            }

            productos.close();

        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }

    }


    private void DefaultEmptyList(){
        TextView emptyView = new TextView(getApplicationContext());
        emptyView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        emptyView.setTextColor(getResources().getColor(R.color.texto_black));
        emptyView.setText("Sin resultados");
        emptyView.setTextSize(20);
        emptyView.setVisibility(View.GONE);
        emptyView.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);

        ((ViewGroup)lstProductos.getParent()).addView(emptyView);
        lstProductos.setEmptyView(emptyView);
    }

    @Override
    protected void onResume() {
        try {
            super.onResume();

            //actualizamos la lista
            CargarProductos();

            ProductoAdapter adapter = new ProductoAdapter(this, productosList);
            lstProductos.setAdapter(adapter);

        } catch (Exception ex) {
            Toast.makeText(this, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.product_list_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    class ProductoRow {
        private int _id;
        private String _nombre;
        private String _precio;
        private int _imagen;

        public void set_id(int _id) {
            this._id = _id;
        }

        public void set_nombre(String _nombre) {
            this._nombre = _nombre;
        }

        public void set_precio(String _precio) {
            this._precio = _precio;
        }

        public int get_id() {
            return _id;
        }

        public String get_nombre() {
            return _nombre;
        }

        public String get_precio() {
            return _precio;
        }

        public void set_imagen(int _imagen) {
            this._imagen = _imagen;
        }

        public int get_imagen() {
            //return _imagen;
            return R.drawable.ic_launcher;
        }

        public ProductoRow(int id, String nombre, String precio, int imagen){
            this._id = id;
            this._nombre = nombre;
            this._precio = precio;
            this._imagen = imagen;
        }
    }


    class ProductoAdapter extends ArrayAdapter<ProductoRow> {
        private Context context;
        private List<ProductoRow> datos;

        public ProductoAdapter(Context context, List<ProductoRow> datos) {
            super(context, android.R.layout.simple_list_item_1, datos);

            this.context = context;
            this.datos = datos;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // En primer lugar "inflamos" una nueva vista, que será la que se
            // mostrará en la celda del ListView. Para ello primero creamos el
            // inflater, y después inflamos la vista.
            LayoutInflater inflater = LayoutInflater.from(context);
            View item = inflater.inflate(R.layout.producto_row, null);

            ProductoRow row = datos.get(position);

            // A partir de la vista, recogeremos los controles que contiene para
            // poder manipularlos.
            // Recogemos el ImageView y le asignamos una foto.
            ImageView imagen = (ImageView) item.findViewById(R.id.producto_row_icon);
            imagen.setImageResource(row.get_imagen());

            // Recogemos los TextView para mostrar datos
            ((TextView) item.findViewById(R.id.producto_row_nombre)).setText(row.get_nombre());
            ((TextView) item.findViewById(R.id.producto_row_precio)).setText(row.get_precio());

            // Devolvemos la vista para que se muestre en el ListView.
            return item;

        }
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    /*
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.product_list, container, false);
            return rootView;
        }
    }*/

}
