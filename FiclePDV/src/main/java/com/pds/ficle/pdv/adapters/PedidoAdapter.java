package com.pds.ficle.pdv.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pds.common.Formatos;
import com.pds.ficle.pdv.Pedido;
import com.pds.ficle.pdv.R;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by Hernan on 24/06/2015.
 */
public class PedidoAdapter extends ArrayAdapter<Pedido> {
    private Context context;
    private List<Pedido> datos;
    private DecimalFormat formato;

    public PedidoAdapter(Context context, List<Pedido> datos, DecimalFormat formato) {
        super(context, android.R.layout.simple_list_item_1, datos);

        this.context = context;
        this.datos = datos;
        this.formato = formato;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // En primer lugar "inflamos" una nueva vista, que será la que se
        // mostrará en la celda del ListView. Para ello primero creamos el
        // inflater, y después inflamos la vista.
        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.pedido_item, parent, false);
        }

        Pedido row = datos.get(position);

        // A partir de la vista, recogeremos los controles que contiene para
        // poder manipularlos.
        // Recogemos los TextView para mostrar datos
        ((TextView) convertView.findViewById(R.id.pedido_item_txt_producto)).setText(row.get_descripcion());

        TextView cantidadText = (TextView) convertView.findViewById(R.id.pedido_item_txt_cantidad);
        if (row.esItemPesable()) {
            //reducimos el font para que entre el peso en kg
            //cantidadText.setText(String.valueOf(row.get_cantidad()) + " kg");
            if (row.get_cantidad() == 0) {
                cantidadText.setText("0");
                cantidadText.setTextSize(25);
            } else {

                double valor = row.get_cantidad();

                String unidad = row.get_producto().getUnit().equalsIgnoreCase("lt") ? "L" : "K";

                if(unidad.equalsIgnoreCase("K") && row.get_cantidad() < 1){
                    //mostramos solo gramos
                    valor = valor * 1000;
                    unidad = "g";
                }

                cantidadText.setText(Formatos.FormateaDecimal(valor, Formatos.DecimalFormat_US) + unidad);
                cantidadText.setTextSize(16);
            }
        } else {
            cantidadText.setText(Formatos.FormateaDecimal(row.get_cantidad(), Formatos.DecimalFormat_US));
            cantidadText.setTextSize(25);
        }

        ((TextView) convertView.findViewById(R.id.pedido_item_txt_precio)).setText(Formatos.FormateaDecimal(row.get_precio(), formato));

        ((TextView) convertView.findViewById(R.id.pedido_item_txt_total)).setText(Formatos.FormateaDecimal(row.get_total(), formato));

        if (row.isInEditMode())
            //convertView.setBackgroundResource(R.drawable.item_back);
            convertView.setBackgroundColor(Color.parseColor("#E2E2E2"));
        else if (row.hasError()) {
            convertView.setBackgroundColor(Color.parseColor("#FF8080"));
        }
        else
            convertView.setBackgroundResource(R.drawable.bg_normal);

        // Devolvemos la vista para que se muestre en el ListView.
        return convertView;

    }
}
