package com.pds.ficle.pdv;

import android.content.ContentResolver;
import android.text.TextUtils;

import com.pds.common.dao.ProductDao;
import com.pds.common.model.Combo;
import com.pds.common.model.ComboItem;
import com.pds.common.model.Product;
import com.pds.common.model.VentaAbstract;
import com.pds.common.model.VentaDetalleAbstract;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hernan on 24/06/2015.
 */
public abstract class Stock {

    public static void ProcesarProductoItem(VentaDetalleAbstract detalle, ProductDao productDao, ContentResolver contentResolver) {
        //ACTUALIZAMOS STOCK Y PRECIO (SINO ES GENERICO)
        if (!detalle.esItemGenerico()) {
            Product _producto = detalle.getProducto();


            if (!detalle.esItemPesable()) { //actualizamos stock en el caso que no sea un pesable
                _producto.setStock(_producto.getStock() - detalle.getCantidad());
            }

            //if (_producto.getSalePrice() == 0) {//actualizamos el precio en el caso de que no tenga precio
            //if(actualizaPrecioLista){
            _producto.setSalePrice(detalle.getPrecio());
            //}

            productDao.saveOrUpdate(_producto);


        }
    }


}
