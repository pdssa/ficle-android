package com.pds.ficle.cashdrawer;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Caja;
import com.pds.common.Caja.TipoMovimientoCaja;
import com.pds.common.CajaHelper;
import com.pds.common.Config;
//import com.pds.common.Formatos;
import com.pds.common.Formato;
import com.pds.common.Logger;
import com.pds.common.Usuario;
import com.pds.common.Venta.TipoMedioPago;
import com.pds.common.activity.TimerActivity;
import com.pds.common.dialog.ConfirmDialog;
import com.pds.common.fragment.PadFragment;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import android_serialport_api.Printer;
import android_serialport_api.Printer.ALINEACION;

public class CashMainActivity extends TimerActivity implements PadFragment.PadListener {

    //declaro variables de los controles
    private Usuario _user;
    private View _cashStatusView;
    private TextView _cashStatusMessageView;
    private View _cashView;
    ImageView dummy_view;
    private ImageButton btnCalc;
    private List<String> lineasLayout;
    private int arqueoStep;
    private double ARQUEO_MONTO;
    private SeccionedAdapter adapter;
    private ListView lstMovimientosCaja;
    private List<MovimientoRow> movimientosCaja_list;
    private Button btnImprimir;
    private double totalEfvo;
    private TextView txtTotalEfvo;

    private int _MAX = 100; //cantidad de movimientos a mostrar en pantalla

    //controles del panel
    ImageView imgIcono_panel;
    TextView txtWelcome_panel;
    TextView txtComentario_panel;
    TextView txtFuncion_panel;
    TextView txtValor_panel;
    TextView txtPanel;
    TextView txtUser_panel;
    TextView txtHora_panel;
    TextView txtFecha_panel;

    //teclado
    Button btnScan;
    /*Button btnRetiro;
    Button btnArqueo;
    Button btnDeposito;
    ImageButton btnBackspace;
    Button btn0;
    Button btn1;
    Button btn2;
    Button btn3;
    Button btn4;
    Button btn5;
    Button btn6;
    Button btn7;
    Button btn8;
    Button btn9;
    Button btnPunto;*/

    //controles del popup de medio de pago
    Button btnConfirmar_mp;
    Button btnCancelar_mp;
    TextView txtTotal_mp;
    RadioGroup rbtMedios_mp;


    //control de fechas
    BroadcastReceiver _broadcastReceiver;

    private Printer _printer;

    private Config config;

    private Formato.Decimal_Format FORMATO_DECIMAL;

    private String COMPROB_DESC;


    @Override
    public void onStart() {
        super.onStart();
        _broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().compareTo(Intent.ACTION_TIME_TICK) == 0) {
                    Date now = new Date();
                    ((TextView) findViewById(R.id.panel_txt_fecha)).setText(Formato.FormateaDate(now, Formato.DateFormat));
                    ((TextView) findViewById(R.id.panel_txt_hora)).setText(Formato.FormateaDate(now, Formato.ShortTimeFormat));
                }
            }
        };

        registerReceiver(_broadcastReceiver, new IntentFilter(Intent.ACTION_TIME_TICK));
    }

    @Override
    public void onStop() {
        super.onStop();
        if (_broadcastReceiver != null)
            unregisterReceiver(_broadcastReceiver);

    }

    ProgressDialog pd = null;

    /**
     * ProgressDialog pd = new ProgressDialog(MainActivity.this);
     * pd.setTitle("Iniciando CashDrawer...");
     * pd.setMessage("Aguarde por favor");
     * pd.setCancelable(false);
     * pd.setIndeterminate(true);
     * <p/>
     * i.putExtra("progress_dialog", (Serializable)pd);
     */

    /*
    private void Crear() {
        try {
            setContentView(R.layout.activity_cash_main);

            ((TextView) findViewById(R.id.txtVersion)).setText("Version:" + getPackageManager().getPackageInfo(this.getPackageName().toString(), 0).versionName);


            //********* USER LOGUEADO OK *********

            //asignamos los view
            Init_AsignarViews();

            //asignamos los eventos
            Init_AsignarEventos();
            //this.txtUser_panel.append("admin");
            this.txtUser_panel.append(this._user.getNombre());

            //this.txtWelcome_panel.setText("CASHDRAWER");
            this.txtWelcome_panel.setVisibility(View.INVISIBLE);
            this.imgIcono_panel.setImageResource(R.drawable.cash_min);
            this.imgIcono_panel.setVisibility(View.VISIBLE);

            /*this.btnRetiro.setText(TipoMovimientoCaja.RETIRO.name());
            this.btnRetiro.setBackgroundResource(R.drawable.botones_main_izq_red);
            this.btnRetiro.setTextSize(40);

            this.btnArqueo.setText(TipoMovimientoCaja.ARQUEO.name());
            this.btnArqueo.setTextSize(40);

            this.btnDeposito.setText(TipoMovimientoCaja.DEPOSITO.name());
            this.btnDeposito.setBackgroundResource(R.drawable.botones_main_izq);*
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }
    */
    private void Resumir() {

        ComentarioInicial();

        this.txtFecha_panel.setText(Formato.FormateaDate(new Date(), Formato.DateFormat));
        this.txtHora_panel.setText(Formato.FormateaDate(new Date(), Formato.ShortTimeFormat));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_cash_main);

            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);//evitamos el sleep durante la ejecucion de esta aplicacion

            ((TextView) findViewById(R.id.txtVersion)).setText("Version:" + getPackageManager().getPackageInfo(this.getPackageName().toString(), 0).versionName);

            //logueo Usuario *****************************************************
            //obtenemos los parametros pasamos a la actividad
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                //tomamos el id de usuario
                //_id_user_login = extras.getInt("_id_user_login");
                this._user = (Usuario) extras.getSerializable("current_user");
            }

            //no tenemos user logueado...volvemos a EntryPoint
            if (_user == null) {
                Toast.makeText(this, R.string.unknown_user, Toast.LENGTH_LONG).show();
                this.finish();
            } else {

                if (_user.getId_perfil() != 1) {
                    Toast.makeText(this, "Usuario " + this._user.getNombre() + " PERMISO DENEGADO", Toast.LENGTH_LONG).show();
                    this.finish();
                } else {


                    //********* USER LOGUEADO OK *********

                    //asignamos los view
                    Init_AsignarViews();

                    //asignamos los eventos
                    Init_AsignarEventos();
                    //this.txtUser_panel.append("admin");
                    this.txtUser_panel.append(this._user.getNombre());

                    //this.txtWelcome_panel.setText("CASHDRAWER");
                    this.txtWelcome_panel.setVisibility(View.INVISIBLE);
                    this.imgIcono_panel.setImageResource(R.drawable.cash_min);
                    this.imgIcono_panel.setVisibility(View.VISIBLE);

                    /*
                    this.btnRetiro.setText(TipoMovimientoCaja.RETIRO.name());
                    this.btnRetiro.setBackgroundResource(R.drawable.botones_main_izq_red);
                    this.btnRetiro.setTextSize(40);

                    this.btnArqueo.setText(TipoMovimientoCaja.ARQUEO.name());
                    this.btnArqueo.setTextSize(40);

                    this.btnDeposito.setText(TipoMovimientoCaja.DEPOSITO.name());
                    this.btnDeposito.setBackgroundResource(R.drawable.botones_main_izq);
                    */
                    PadFragment padFragment = (PadFragment) getFragmentManager().findFragmentById(R.id.frg_pag);
                    padFragment.setPadListener(this);
                    padFragment.setFunctionConfig(PadFragment.FnButton.FUNCTION_1, TipoMovimientoCaja.RETIRO.name(), true, R.drawable.botones_main_izq_red, 40);
                    padFragment.setFunctionConfig(PadFragment.FnButton.FUNCTION_2, TipoMovimientoCaja.ARQUEO.name(), true, R.drawable.botones_teclado, 40);
                    padFragment.setFunctionConfig(PadFragment.FnButton.FUNCTION_4, TipoMovimientoCaja.DEPOSITO.name(), true, R.drawable.botones_main_izq);

                    config = new Config(this);

                    //customizar por pais
                    Init_CustomConfigPais(config.PAIS);

                    VisualizarMovimientosCaja();

                    //setTimer(config.INACTIVITY_TIME);

                    // Crear();
                /*
                    pd = new ProgressDialog(this);
                    pd.setTitle("Iniciando CashDrawer...");
                    pd.setMessage("Aguarde por favor");
                    pd.setCancelable(false);
                    pd.setIndeterminate(true);
                    pd.show();

                    new Thread() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Crear();
                                    Resumir();
                                    pd.dismiss();
                                    pd = null;
                                }
                            });
                        }
                    }.start();



                new Thread() {

                    @Override
                    public void run() {
                        Crear();
                        Resumir();
                        pd.dismiss();
                        pd = null;
                    }

                }.start();
                */

                }
            }

        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            config = new Config(this);

            //if (pd == null)
            Resumir();
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    @Override
    protected void onDestroy() {
        if (_printer != null)
            _printer.end();
        super.onDestroy();
    }

    private void Init_CustomConfigPais(String codePais) {

        //FORMATO_DECIMAL = codePais.equals("AR") ? Formatos.DecimalFormat : Formatos.DecimalFormat_CH;
        FORMATO_DECIMAL = Formato.getDecimal_Format(codePais);
        COMPROB_DESC = getStringFromResource(config.PAIS, "btn_presupuesto_min");
        //btnComprobante.setText(getStringFromResource(codePais, "btn_presupuesto"));
    }

    private String getStringFromResource(String codePais, String idResource) {
        String sufijo = codePais.equals("AR") ? "_AR" : "";
        //String pack = codePais.equals("AR") ? "strings_AR" : "strings";
        int resId = getResources().getIdentifier(idResource + sufijo, "string", getPackageName());
        return getString(resId);
    }
/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.cash_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
*/


    private void Init_AsignarViews() {

        this.btnCalc = (ImageButton) findViewById(R.id.cash_btnCalc);
        this.btnImprimir = (Button) findViewById(R.id.cash_btn_imprimir);

        this.lstMovimientosCaja = (ListView) findViewById(R.id.cash_lstMovimientos);
        this.lstMovimientosCaja.setEmptyView(findViewById(android.R.id.empty));
        this.dummy_view = (ImageView) findViewById(R.id.cash_dummy);

        this._cashView = findViewById(R.id.cash_main_layout);
        //this._cashStatusView = findViewById(R.id.cash_status);
        //this._cashStatusMessageView = (TextView) findViewById(R.id.cash_status_message);

        this.txtPanel = (TextView) findViewById(R.id.panel_txt_valor);

        this.txtTotalEfvo = (TextView) findViewById(R.id.cash_txt_total_efvo);


        //this.recarga_btnImprimirResultados = (Button) findViewById(R.id.recarga_btn_imprimir);
        //this.recarga_btnVolver = (Button) findViewById(R.id.recarga_btn_volver);

        //controles del pad
        this.btnScan = (Button) findViewById(R.id.cash_btnScan);
        /*this.btnRetiro = (Button) findViewById(R.id.pad_btnCantidad);
        this.btnArqueo = (Button) findViewById(R.id.pad_btnPrecio);
        this.btnDeposito = (Button) findViewById(R.id.pad_btnEnter);
        this.btnBackspace = (ImageButton) findViewById(R.id.pad_btnClear);
        this.btn0 = (Button) findViewById(R.id.pad_btnNro0);
        this.btn1 = (Button) findViewById(R.id.pad_btnNro1);
        this.btn2 = (Button) findViewById(R.id.pad_btnNro2);
        this.btn3 = (Button) findViewById(R.id.pad_btnNro3);
        this.btn4 = (Button) findViewById(R.id.pad_btnNro4);
        this.btn5 = (Button) findViewById(R.id.pad_btnNro5);
        this.btn6 = (Button) findViewById(R.id.pad_btnNro6);
        this.btn7 = (Button) findViewById(R.id.pad_btnNro7);
        this.btn8 = (Button) findViewById(R.id.pad_btnNro8);
        this.btn9 = (Button) findViewById(R.id.pad_btnNro9);
        this.btnPunto = (Button) findViewById(R.id.pad_btnPunto);*/

        //controles del panel
        this.imgIcono_panel = (ImageView) findViewById(R.id.panel_img_icono);
        this.txtWelcome_panel = (TextView) findViewById(R.id.panel_txt_welcome);
        this.txtUser_panel = (TextView) findViewById(R.id.panel_txt_user);
        this.txtComentario_panel = (TextView) findViewById(R.id.panel_txt_comentario);
        this.txtFuncion_panel = (TextView) findViewById(R.id.panel_txt_funcion);
        this.txtValor_panel = (TextView) findViewById(R.id.panel_txt_valor);
        this.txtFecha_panel = (TextView) findViewById(R.id.panel_txt_fecha);
        this.txtHora_panel = (TextView) findViewById(R.id.panel_txt_hora);

        //controles del popup medio pago
        this.btnConfirmar_mp = (Button) findViewById(R.id.medio_pago_btn_aceptar);
        this.btnCancelar_mp = (Button) findViewById(R.id.medio_pago_btn_cancelar);
        this.txtTotal_mp = (TextView) findViewById(R.id.medio_pago_txt_total);
        this.rbtMedios_mp = (RadioGroup) findViewById(R.id.medio_pago_medios);


    }

    private void Init_AsignarEventos() {

        btnCalc.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_MAIN);
                i.setComponent(new ComponentName("com.android.calculator2", "com.android.calculator2.Calculator"));


                try {
                    CashMainActivity.this.startActivity(i);
                } catch (ActivityNotFoundException noSuchActivity) {
                    // si el calculator intent no está registrado, probamos con un nombre alternativo
                    Intent i2 = new Intent();
                    i2.setAction(Intent.ACTION_MAIN);
                    i2.setComponent(new ComponentName("com.sec.android.app.popupcalculator", "com.sec.android.app.popupcalculator.Calculator"));
                    try {
                        CashMainActivity.this.startActivity(i2);
                    } catch (ActivityNotFoundException noSuchActivity2) {
                        // calculator intent not found
                        Toast.makeText(CashMainActivity.this, "Acceso a calculadora no encontrado", Toast.LENGTH_LONG).show();
                    }
                }

            }
        });

        /*
        this.btnBackspace.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                String texto = txtPanel.getText().toString();

                if (!texto.isEmpty()) {
                    txtPanel.setText(texto.substring(0, texto.length() - 1));
                }
            }
        });

        this.btnBackspace.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                txtPanel.setText("");

                return false;
            }
        });

        this.btn0.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("0");
            }
        });

        this.btn1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("1");
            }
        });

        this.btn2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("2");
            }
        });

        this.btn3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("3");
            }
        });

        this.btn4.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("4");
            }
        });

        this.btn5.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("5");
            }
        });

        this.btn6.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("6");
            }
        });

        this.btn7.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("7");
            }
        });

        this.btn8.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("8");
            }
        });

        this.btn9.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel("9");
            }
        });

        this.btnPunto.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarTextoPanel(".");
            }
        });
        */
        this.dummy_view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                dummy_view.setBackgroundColor(Color.TRANSPARENT);
                dummy_view.setVisibility(View.INVISIBLE);
                (findViewById(R.id.cash_medio_pago)).setVisibility(View.INVISIBLE);
            }
        });

        (findViewById(R.id.cash_medio_pago)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //dejamos el evento click seteado en la vista para que no se vaya al click del dummyview
            }
        });

        /*
        this.btnDeposito.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (arqueoStep == 0) {
                    MovimientoCaja(TipoMovimientoCaja.DEPOSITO.name());
                } else {
                    //actua como OK del ingreso del ajuste
                    //if (!ARQUEO_MONTO.equals("0.00")) {
                    RegistrarMovimientoCaja(TipoMovimientoCaja.ARQUEO.name(), ARQUEO_MONTO);

                    Toast.makeText(CashMainActivity.this, "Ajuste registrado OK", Toast.LENGTH_SHORT).show();
                    //}
                    Cancelar();
                }
            }
        });


        this.btnRetiro.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (arqueoStep == 0) {
                    MovimientoCaja(TipoMovimientoCaja.RETIRO.name());
                } else {
                    //actua como CANCELAR del ingreso del ajuste
                    Cancelar();
                }
            }
        });

        this.btnArqueo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                txtComentario_panel.setText("CONTROL DE CAJA");

                arqueoStep = 1;

                ProcesoArqueo();

            }
        });
        */
        this.btnImprimir.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ImprimirMovimientos();
            }
        });
    }

    private void ImprimirMovimientos() {

        try {
            if (_printer == null)
                _printer = Printer.getInstance(config.PRINTER_MODEL, CashMainActivity.this);

            if (movimientosCaja_list.size() >= _MAX)//si son varios movimientos => activamos delay en la impresora para que no "deforme"
                _printer.delayActivated = true;

            _printer.printerCallback = new Printer.PrinterCallback() {
                @Override
                public void onPrinterReady() {
                    _printer.lineFeed();

                    _printer.format(2, 2, ALINEACION.CENTER);
                    _printer.printLine("MOVIMIENTOS CAJA");

                    _printer.format(1, 1, ALINEACION.NORMAL);
                    _printer.sendSeparadorHorizontal(false);

                    String fechaActual = "";

                    _printer.printLine("TOTAL EFECTIVO: " + Formato.FormateaDecimal(totalEfvo, FORMATO_DECIMAL, Formato.getCurrencySymbol(FORMATO_DECIMAL)));
                    _printer.lineFeed();

                    //recorremos cada movimiento
                    int _i = 0;
                    //for (MovimientoRow movimiento : ((MovimientoAdapter) lstMovimientosCaja.getAdapter()).datos) {
                    for (MovimientoRow movimiento : movimientosCaja_list) {

                        //corte de control por fecha
                        if (!fechaActual.equals(movimiento.get_fecha_corta())) {

                            if (!fechaActual.equals("")) //sino es el primer registro
                                _printer.lineFeed(); //separamos el ultimo movimiento de la fecha anterior, del titulo de la nueva fecha

                            fechaActual = movimiento.get_fecha_corta();

                            _printer.printLine(fechaActual);//10-12-14
                        }

                        //seleccionamos el tipo de movimiento de CAJA o la descripcion de VENTA
                        String tipoMov = movimiento.get_tipoMov();

                        if (!movimiento.get_ventaDesc().equals(""))
                            tipoMov = movimiento.get_ventaDesc();

                        tipoMov = (tipoMov + "          ").substring(0, 10); //ajustamos a 10 posiciones, el mas largo es "PAGO SERV."

                        String montoAux = "           " + movimiento.get_monto_format();
                        String monto = montoAux.substring(montoAux.length() - 11); //rellenamos 11

                        _printer.printOpposite(movimiento.get_hora_corta() + " " + tipoMov + " " + movimiento.get_medioPago_Corto(), monto);

                        if (movimiento._tipoMovId == TipoMovimientoCaja.ARQUEO.ordinal())
                            break; //imprimimos hasta el ultimo arqueo

                        _i++;

                        if (_i % 20 == 0 && _printer.delayActivated) {
                            try {
                                Thread.sleep(300);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    _printer.footer();
                }
            };

            if (!_printer.initialize()) {
                Toast.makeText(CashMainActivity.this, "Error al inicializar", Toast.LENGTH_SHORT).show();

                _printer.end();
            }
        } catch (Exception ex) {
            Toast.makeText(CashMainActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void OpenCashDrawer(){

        try {

            if (_printer == null)
                _printer = Printer.getInstance(config.PRINTER_MODEL, CashMainActivity.this);

            _printer.printerCallback = new Printer.PrinterCallback() {
                @Override
                public void onPrinterReady() {
                    if(_printer.isCashDrawerActivated()){
                        _printer.openCashDrawer();
                    }
                }
            };

            if (!_printer.initialize()) {
                Toast.makeText(CashMainActivity.this, "Error al inicializar", Toast.LENGTH_SHORT).show();

                _printer.end();
            }

        } catch (Exception ex) {
            Toast.makeText(CashMainActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private boolean ValidarMontoIngresado() {
        String montoContado = txtValor_panel.getText().toString().trim();

        //validamos el monto ingresado
        return !(montoContado == null || montoContado.equals(""));
    }

    private void ProcesoArqueo() {
        switch (arqueoStep) {
            case 1: {

                //validamos el monto ingresado
                if (!ValidarMontoIngresado()) {
                    Toast.makeText(CashMainActivity.this, R.string.msj_arqueo_sin_monto, Toast.LENGTH_LONG).show();
                } else {
                    String montoContado = txtValor_panel.getText().toString().trim();
                    txtValor_panel.setText("");

                    this.lstMovimientosCaja.setVisibility(View.GONE);
                    this.btnImprimir.setVisibility(View.GONE);
                    this.txtTotalEfvo.setVisibility(View.GONE);

                    //obtenemos el monto en double y lo formateamos moneda local
                    double montoIngresado = Formato.ParseaDecimal(montoContado, FORMATO_DECIMAL);
                    montoContado = Formato.FormateaDecimal(montoIngresado, FORMATO_DECIMAL, Formato.getCurrencySymbol(FORMATO_DECIMAL));
                    //montoContado = new DecimalFormat("0.00").format(Double.valueOf(montoContado));

                    //String montoRegistrado = new DecimalFormat("0.00").format(totalEfvo);
                    String montoRegistrado = Formato.FormateaDecimal(totalEfvo, FORMATO_DECIMAL, Formato.getCurrencySymbol(FORMATO_DECIMAL));

                    EscribeLinea("MONTO CONTADO", "ARQUEO");
                    EscribeLinea("\t" + montoContado, "ARQUEO");
                    EscribeLinea("MONTO REGISTRADO", "ARQUEO");
                    EscribeLinea("\t" + montoRegistrado, "ARQUEO");
                    EscribeLinea("", "ARQUEO");


                    //double diferencia = Double.parseDouble(montoContado) - Double.parseDouble(montoRegistrado);
                    double diferencia = montoIngresado - totalEfvo;

                    //ARQUEO_MONTO = new DecimalFormat("0.00").format(Double.valueOf(diferencia));
                    //ARQUEO_MONTO = Formato.FormateaDecimal(diferencia, FORMATO_DECIMAL);
                    ARQUEO_MONTO = diferencia;

                    if (diferencia == 0) {

                        EscribeLinea("@C@@B@NO SE ENCONTRO DIFERENCIA", "ARQUEO");

                    } else {

                        EscribeLinea("@C@@B@DIFERENCIA ENCONTRADA", "ARQUEO");
                        EscribeLinea("@C@Desea Ajustar?", "ARQUEO");
                    }

                    //cambiamos los controles
//                    this.btnRetiro.setText("CANCEL");
//                    this.btnDeposito.setText("OK");
//                    this.btnArqueo.setText("");
//                    this.btnArqueo.setEnabled(false);
//                    this.btnArqueo.setBackgroundColor(getResources().getColor(R.color.boton_negro));


                    PadFragment padFragment = (PadFragment) getFragmentManager().findFragmentById(R.id.frg_pag);
                    padFragment.setFunctionConfig(PadFragment.FnButton.FUNCTION_1, "CANCEL", true, R.drawable.botones_main_izq_red, 40);
                    padFragment.setFunctionConfig(PadFragment.FnButton.FUNCTION_2, "", false, R.drawable.btn_disabled);
                    padFragment.setFunctionConfig(PadFragment.FnButton.FUNCTION_4, "OK", true, R.drawable.botones_main_izq);

                }

            }
            break;
            default:
                Cancelar();
                break;
        }
    }

    private void Cancelar() {
        this.arqueoStep = 0;

//        this.btnRetiro.setText(TipoMovimientoCaja.RETIRO.name());
//        this.btnDeposito.setText(TipoMovimientoCaja.DEPOSITO.name());
//        this.btnArqueo.setText(TipoMovimientoCaja.ARQUEO.name());
//        this.btnArqueo.setEnabled(true);
//        this.btnArqueo.setBackgroundColor(getResources().getColor(R.color.boton_gris));

        PadFragment padFragment = (PadFragment) getFragmentManager().findFragmentById(R.id.frg_pag);
        padFragment.setFunctionConfig(PadFragment.FnButton.FUNCTION_1, TipoMovimientoCaja.RETIRO.name(), true, R.drawable.botones_main_izq_red, 40);
        padFragment.setFunctionConfig(PadFragment.FnButton.FUNCTION_2, TipoMovimientoCaja.ARQUEO.name(), true, R.drawable.botones_teclado, 40);
        padFragment.setFunctionConfig(PadFragment.FnButton.FUNCTION_4, TipoMovimientoCaja.DEPOSITO.name(), true, R.drawable.botones_main_izq);


        ComentarioInicial();

        this.lineasLayout = null;
        (findViewById(R.id.cash_lineas)).setVisibility(View.GONE);

        this.lstMovimientosCaja.setVisibility(View.VISIBLE);
        //this.btnImprimir.setVisibility(View.VISIBLE);
        EstadoImpresion();
        this.txtTotalEfvo.setVisibility(View.VISIBLE);
    }

    private void EscribeLinea(String linea) {
        EscribeLinea(linea, "");
    }

    private void EscribeLinea(String linea, String titulo) {

        if (this.lineasLayout == null)
            this.lineasLayout = new ArrayList<String>();

        this.lineasLayout.add(linea);

        EscribeLineas(this.lineasLayout, titulo);
    }

    private void EscribeLineas(List<String> lineas) {
        EscribeLineas(lineas, "");
    }

    private void EscribeLineas(List<String> lineas, String titulo) {

        //ocultamos el menu y la lista de resultados
        (findViewById(R.id.cash_lstMovimientos)).setVisibility(View.GONE);

        //mostramos el layout de lineas
        (findViewById(R.id.cash_lineas)).setVisibility(View.VISIBLE);

        if (!titulo.equals("")) {

            TextView txtTitulo = (TextView) findViewById(R.id.cash_txt_titulo);
            txtTitulo.setVisibility(View.VISIBLE);
            txtTitulo.setText(titulo);
        } else {
            TextView txtTitulo = (TextView) findViewById(R.id.cash_txt_titulo);
            txtTitulo.setVisibility(View.GONE);
            txtTitulo.setText("");
        }

        //blanqueamos
        ((TextView) findViewById(R.id.cash_txt1)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cash_txt2)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cash_txt3)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cash_txt4)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cash_txt5)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cash_txt6)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cash_txt7)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cash_txt8)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cash_txt9)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cash_txt10)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.cash_txt11)).setVisibility(View.GONE);

        for (int i = 0; i < lineas.size(); i++) {

            TextView txtLinea = null;

            switch (i + 1) {
                case 1:
                    txtLinea = (TextView) findViewById(R.id.cash_txt1);
                    break;
                case 2:
                    txtLinea = (TextView) findViewById(R.id.cash_txt2);
                    break;
                case 3:
                    txtLinea = (TextView) findViewById(R.id.cash_txt3);
                    break;
                case 4:
                    txtLinea = (TextView) findViewById(R.id.cash_txt4);
                    break;
                case 5:
                    txtLinea = (TextView) findViewById(R.id.cash_txt5);
                    break;
                case 6:
                    txtLinea = (TextView) findViewById(R.id.cash_txt6);
                    break;
                case 7:
                    txtLinea = (TextView) findViewById(R.id.cash_txt7);
                    break;
                case 8:
                    txtLinea = (TextView) findViewById(R.id.cash_txt8);
                    break;
                case 9:
                    txtLinea = (TextView) findViewById(R.id.cash_txt9);
                    break;
                case 10:
                    txtLinea = (TextView) findViewById(R.id.cash_txt10);
                    break;
                case 11:
                    txtLinea = (TextView) findViewById(R.id.cash_txt11);
                    break;

            }

            String texto = lineas.get(i);
            if (texto.startsWith("@C@")) {
                texto = texto.replace("@C@", "");
                txtLinea.setGravity(Gravity.CENTER);
            } else {
                txtLinea.setGravity(Gravity.LEFT);
            }

            if (texto.startsWith("@B@")) {
                texto = texto.replace("@B@", "");
                txtLinea.setTypeface(null, Typeface.BOLD);
            } else {
                txtLinea.setTypeface(null, Typeface.NORMAL);
            }


            txtLinea.setText(texto);
            txtLinea.setVisibility(View.VISIBLE);
        }

    }

    private void MovimientoCaja(String tipoMovimiento) {

        String montoIngresado = txtValor_panel.getText().toString().trim();

        //validamos el monto ingresado
        if (montoIngresado == null || montoIngresado.equals("")) {
            Toast.makeText(CashMainActivity.this, "Debe ingresar un monto para continuar", Toast.LENGTH_LONG).show();
        } else {

            //montoIngresado = Formato.FormateaDecimal(Formato.ParseaDecimal(montoIngresado, Formatos.DecimalFormat_US), FORMATO_DECIMAL);

            double _montoIngresado = Formato.ParseaDecimal(montoIngresado, FORMATO_DECIMAL);

            RegistrarMovimientoCaja(tipoMovimiento, _montoIngresado);

            txtValor_panel.setText("");

            Toast.makeText(CashMainActivity.this, "Movimiento registrado OK", Toast.LENGTH_SHORT).show();

            ComentarioInicial();
        }
    }

    private void RegistrarMovimientoCaja(String tipoMovimiento, double montoIngresado) {

        Caja.RegistrarMovimientoCaja(
                CashMainActivity.this,
                TipoMovimientoCaja.valueOf(tipoMovimiento),
                //new DecimalFormat("0.00").format(Double.valueOf(montoIngresado)),
                //Formatos.ParseaFormateaDecimal(montoIngresado, Formatos.DecimalFormat, Formatos.DecimalFormat_US),
                Formato.FormateaDecimal(montoIngresado, Formato.Decimal_Format.US),
                TipoMedioPago.EFECTIVO.name(),
                "");

        Logger.RegistrarEvento(
                CashMainActivity.this,
                "i",
                tipoMovimiento + " CAJA",
                //"Monto: $ " + new DecimalFormat("0.00").format(Double.valueOf(montoIngresado)));
                //"Monto: $ " + Formatos.ParseaFormateaDecimal(montoIngresado, Formatos.DecimalFormat, FORMATO_DECIMAL));
                "Monto: " + Formato.FormateaDecimal(montoIngresado, FORMATO_DECIMAL, Formato.getCurrencySymbol(FORMATO_DECIMAL)));

        LeerMovimientosCaja();//refrescamos la lista en pantalla
        LeerTotalEfvo();//refrescamos el total en efvo
    }

    private void AgregarTextoPanel(String texto) {
        this.txtPanel.setText(this.txtPanel.getText().toString() + texto);
        this.txtPanel.setVisibility(View.VISIBLE);
    }

    private void ComentarioInicial() {
        this.txtComentario_panel.setText("Ingrese monto y funcion para continuar");
        this.txtComentario_panel.setVisibility(View.VISIBLE);
    }

    private void VisualizarMovimientosCaja() {
        //btnImprimir.setVisibility(View.VISIBLE);
        EstadoImpresion();

        (findViewById(R.id.cash_lineas)).setVisibility(View.GONE);

        this.lstMovimientosCaja.setVisibility(View.VISIBLE);

        this.txtTotalEfvo.setVisibility(View.VISIBLE);

        LeerMovimientosCaja();

        LeerTotalEfvo();

    }

    private void EstadoImpresion(){
        if(movimientosCaja_list != null && movimientosCaja_list.size() > 0)
            btnImprimir.setVisibility(View.VISIBLE);
        else
            btnImprimir.setVisibility(View.GONE);
    }

    private void LeerTotalEfvo() {
        new LeerEfvoTask(CashMainActivity.this).execute();
    }

    private void LeerTotalEfvo_() {

        double total = 0;

        try {

            Cursor movimientos = getContentResolver().query(
                    Uri.parse("content://com.pds.ficle.ep.caja.contentprovider/caja"),
                    CajaHelper.columnas,
                    CajaHelper.CAJA_MEDIO_PAGO + "=?",
                    new String[]{"EFECTIVO"},
                    null);

            // recorremos los items solo EFECTIVO sumando el monto
            if (movimientos.moveToFirst()) {
                do {

                    if (movimientos.getInt(CajaHelper.CAJA_IX_TIPO_MOV) == TipoMovimientoCaja.RETIRO.ordinal())
                        total += (-1) * movimientos.getDouble(CajaHelper.CAJA_IX_MONTO);
                    else
                        total += movimientos.getDouble(CajaHelper.CAJA_IX_MONTO);

                } while (movimientos.moveToNext());
            }

            totalEfvo = total;

            //this.txtTotalEfvo.setText("TOTAL EFECTIVO: $ " + String.valueOf(totalEfvo));
            //this.txtTotalEfvo.setText("TOTAL EFECTIVO: $ " + Formatos.FormateaDecimal(totalEfvo, FORMATO_DECIMAL));
            this.txtTotalEfvo.setText("TOTAL EFECTIVO: " + Formato.FormateaDecimal(totalEfvo, FORMATO_DECIMAL, Formato.getCurrencySymbol(FORMATO_DECIMAL)));

            movimientos.close();

        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }


    }

    public void LeerMovimientosCaja() {
        LeerMovimientosCaja(_MAX);
    }

    public void LeerMovimientosCaja(int max) {
        new LeerMovimientosTask(CashMainActivity.this).execute(max);
    }

    @Override
    public void onNumberPressed(String numberPressed) {
        if(numberPressed.equals("."))//reemplazamos el punto
            numberPressed = Formato.getDecimalSymbol(FORMATO_DECIMAL);
        AgregarTextoPanel(numberPressed);
    }

    @Override
    public void onFunction1Pressed() {
        if (arqueoStep == 0) {
            MovimientoCaja(TipoMovimientoCaja.RETIRO.name());

            OpenCashDrawer();
        } else {
            //actua como CANCELAR del ingreso del ajuste
            Cancelar();
        }
    }

    @Override
    public void onFunction2Pressed() {

        OpenCashDrawer();

        //validamos el monto ingresado
        if (!ValidarMontoIngresado()) {
            Toast.makeText(CashMainActivity.this, R.string.msj_arqueo_sin_monto, Toast.LENGTH_LONG).show();
        } else {

            //preguntamos que tipo de ARQUEO desea realizar

            AlertDialog.Builder builder = new AlertDialog.Builder(CashMainActivity.this);
            // Add action buttons
            builder.setPositiveButton("CIERRE DIARIO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {

                    new ConfirmDialog(CashMainActivity.this, "CIERRE DIARIO", "A continuación se le solicitará la fecha de cierre. Desea continuar?")
                            .set_positiveButtonText("ACEPTAR")
                            .set_positiveButtonAction(new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ProcesoCierre();
                                }
                            })
                            .set_negativeButtonText("CANCELAR")
                            .set_negativeButtonAction(new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();

                }
            })
                    .setNegativeButton("ARQUEO PARCIAL", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            IniciarProcesoArqueoParcial();
                        }
                    })
                    .setTitle("CONTROL DE CAJA - ARQUEO")
                    .setMessage(Html.fromHtml("Seleccione la opción de ARQUEO que desea realizar:<br/><b>ARQUEO PARCIAL:</b> realiza un arqueo del efectivo de caja en un cierre de turno<br/><b>CIERRE DIARIO:</b> realiza un cierre y resumen de las operaciones del día"));

            AlertDialog dialog = builder.create();
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }
    }

    @Override
    public void onFunction3Pressed() {
        //CLEAR OPTION
        String texto = txtPanel.getText().toString();

        if (!texto.isEmpty()) {
            txtPanel.setText(texto.substring(0, texto.length() - 1));
        }
    }

    @Override
    public void onFunction4Pressed() {
        //DEPOSITO OPTION
        if (arqueoStep == 0) {
            MovimientoCaja(TipoMovimientoCaja.DEPOSITO.name());

            OpenCashDrawer();
        } else {
            //actua como OK del ingreso del ajuste
            //if (!ARQUEO_MONTO.equals("0.00")) {
            RegistrarMovimientoCaja(TipoMovimientoCaja.ARQUEO.name(), ARQUEO_MONTO);

            Toast.makeText(CashMainActivity.this, "Ajuste registrado OK", Toast.LENGTH_SHORT).show();
            //}
            Cancelar();
        }
    }



    public void IniciarProcesoArqueoParcial(){
        txtComentario_panel.setText("CONTROL DE CAJA");

        arqueoStep = 1;

        ProcesoArqueo();
    }

    private void ProcesoCierre() {

        //validamos el monto ingresado
        if (!ValidarMontoIngresado()) {
            Toast.makeText(CashMainActivity.this, R.string.msj_arqueo_sin_monto, Toast.LENGTH_LONG).show();
        } else {
            new CierreDiarioTask(CashMainActivity.this);
            //.execute();
        }
        //new SyncDataTask(CashMainActivity.this, null, false, true).execute();
    }

    class MovimientoRow {
        private int _id;
        private int _tipoMovId;
        private String _fecha;
        private String _hora;
        private double _monto;
        private String _observacion;
        private int _ventaId;
        private String _ventaDesc;
        private String _acumulado;
        private String _medioPago;
        private String _tipoVenta;

        public String get_ventaDesc() {

            if (_ventaDesc.equals("RCG"))
                return "RECARGA";

            if (_ventaDesc.equals("PDV"))
                return _tipoVenta.equals("CPB") ? COMPROB_DESC : "VENTA";

            if (_ventaDesc.equals("DEP"))
                return "DEPOSITO";

            if (_ventaDesc.equals("PAG"))
                return "PAGO SERV.";

            if (_ventaDesc.equals("STK"))
                return "STOCK";

            return _ventaDesc;
        }

        public void set_ventaDesc(String _ventaDesc) {
            if (_ventaDesc == null)
                this._ventaDesc = "";
            else
                this._ventaDesc = _ventaDesc;
        }

        public String get_acumulado() {
            return _acumulado;
        }

        public void set_acumulado(String _acumulado) {
            this._acumulado = _acumulado;
        }

        public String get_medioPago() {

            if (_medioPago.equals("TARJETA CREDITO"))
                return "TARJ. CRED.";
            else
                return _medioPago;
        }

        public String get_medioPago_Corto() {

            if (_medioPago.equals("TARJETA CREDITO"))
                return "TCR";
            else if (_medioPago.equals("EFECTIVO"))
                return "EFE";
            else if (_medioPago.equals("CHEQUE"))
                return "CHE";
            else
                return "OTR";
        }

        public void set_medioPago(String _medioPago) {
            this._medioPago = _medioPago;
        }

        public MovimientoRow(int id, int tipoMovId, String fecha, String hora, double monto, String observacion, int ventaId, String ventaDesc, String acumulado, String medioPago, String tipo_vta) {
            this._id = id;
            this._tipoMovId = tipoMovId;
            this._fecha = fecha;
            this._hora = hora;
            this._monto = monto;
            this._observacion = observacion;
            this._ventaId = ventaId;
            this._acumulado = acumulado;
            this._medioPago = medioPago;
            this._tipoVenta = tipo_vta;

            set_ventaDesc(ventaDesc);
        }

        public int get_id() {
            return _id;
        }

        public void set_id(int _id) {
            this._id = _id;
        }

        public int get_tipoMovId() {
            return _tipoMovId;
        }

        public void set_tipoMovId(int _tipoMovId) {
            this._tipoMovId = _tipoMovId;
        }

        public String get_tipoMov() {
            return Caja.TipoMovimientoCaja.values()[_tipoMovId].name();

        }


        public String get_fecha() {
            return _fecha;
        }

        public void set_fecha(String _fecha) {
            this._fecha = _fecha;
        }

        public String get_hora() {
            return _hora;
        }

        public void set_hora(String _hora) {
            this._hora = _hora;
        }

        public String get_fechahora_corta() {
            return this.get_fecha().substring(0, 6) + this.get_fecha().substring(8) + " " + this.get_hora().substring(0, 5);
        }

        public String get_fecha_corta() {
            return this.get_fecha().substring(0, 6) + this.get_fecha().substring(8);
        }

        public String get_hora_corta() {
            return this.get_hora().substring(0, 5);
        }


        public double get_monto() {
            return _monto;
        }

        public String get_monto_format() {
            //return new DecimalFormat("0.00").format(Double.parseDouble(_monto));

            if (this._tipoMovId == TipoMovimientoCaja.RETIRO.ordinal())
                return Formato.getCurrencySymbol(FORMATO_DECIMAL) + " -" + Formato.FormateaDecimal(_monto, FORMATO_DECIMAL);
            else
                return Formato.FormateaDecimal(_monto, FORMATO_DECIMAL, Formato.getCurrencySymbol(FORMATO_DECIMAL));

        }

        public double get_monto_double() {
            return _monto;
            //return Formatos.ParseaDecimal(_monto);
            //return Double.parseDouble(_monto);
        }

        public void set_monto(double _monto) {
            this._monto = _monto;
        }

        public String get_observacion() {
            return _observacion;
        }

        public void set_observacion(String _observacion) {
            this._observacion = _observacion;
        }

        public int get_ventaId() {
            return _ventaId;
        }

        public void set_ventaId(int _ventaId) {
            this._ventaId = _ventaId;
        }
    }

    class MovimientoAdapter extends ArrayAdapter<MovimientoRow> {
        private Context context;
        private List<MovimientoRow> movimientoRowList;

        public MovimientoAdapter(Context context, List<MovimientoRow> MovimientoRowList) {
            super(context, android.R.layout.simple_list_item_1, MovimientoRowList);
            this.context = context;
            this.movimientoRowList = MovimientoRowList;
        }

        @Override
        public int getCount() {
            if (movimientoRowList != null)
                return movimientoRowList.size();

            return 0;
        }

        @Override
        public MovimientoRow getItem(int position) {
            return movimientoRowList.get(position);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View item = null;

            try {

                MovimientoRow row = movimientoRowList.get(position);

                item = convertView;
                if (item == null) {
                    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    item = inflater.inflate(R.layout.cash_mov_item, null);
                }

                // Recogemos los TextView para mostrar datos
                ((TextView) item.findViewById(R.id.txt_cash_mov_item_fecha)).setText(row.get_hora_corta());
                ((TextView) item.findViewById(R.id.txt_cash_mov_item_tipo_mov)).setText(row.get_tipoMov());
                ((TextView) item.findViewById(R.id.txt_cash_mov_item_medio_pago)).setText(row.get_medioPago_Corto());
                ((TextView) item.findViewById(R.id.txt_cash_mov_item_monto)).setText(row.get_monto_format());
                ((TextView) item.findViewById(R.id.txt_cash_mov_item_acumulado)).setText(Formato.getCurrencySymbol(FORMATO_DECIMAL) + " " + row.get_acumulado());

                if (row == movimientosCaja_list.get(0)) { //destacamos el saldo actual
                    ((TextView) item.findViewById(R.id.txt_cash_mov_item_acumulado)).setTypeface(null, Typeface.BOLD);
                    item.setBackgroundColor(getResources().getColor(R.color.texto_gris));
                    //((TextView) item.findViewById(R.id.txt_cash_mov_item_acumulado)).setBackgroundColor(getResources().getColor(R.color.boton_gris));
                } else {
                    ((TextView) item.findViewById(R.id.txt_cash_mov_item_acumulado)).setTypeface(null, Typeface.NORMAL);
                    item.setBackgroundColor(getResources().getColor(R.color.fondo_blanco));
                    //((TextView) item.findViewById(R.id.txt_cash_mov_item_acumulado)).setBackgroundColor(getResources().getColor(R.color.cash_cell));
                }

                if (!row.get_ventaDesc().equals(""))
                    ((TextView) item.findViewById(R.id.txt_cash_mov_item_tipo_mov)).setText(row.get_ventaDesc());

                if (row.get_tipoMovId() == TipoMovimientoCaja.ARQUEO.ordinal() || row.get_tipoMovId() == TipoMovimientoCaja.CIERRE.ordinal()) {
                    //si es ajuste ponemos bold los campos
                    ((TextView) item.findViewById(R.id.txt_cash_mov_item_fecha)).setTypeface(null, Typeface.BOLD);
                    ((TextView) item.findViewById(R.id.txt_cash_mov_item_tipo_mov)).setTypeface(null, Typeface.BOLD);
                    ((TextView) item.findViewById(R.id.txt_cash_mov_item_monto)).setTypeface(null, Typeface.BOLD);
                    ((TextView) item.findViewById(R.id.txt_cash_mov_item_acumulado)).setTypeface(null, Typeface.BOLD);
                    ((TextView) item.findViewById(R.id.txt_cash_mov_item_medio_pago)).setTypeface(null, Typeface.BOLD);

                }

                //if(row.get_monto().equals(""))
                //    ((TextView) item.findViewById(R.id.txt_cash_mov_item_acumulado)).set


            } catch (Exception e) {
                e.printStackTrace();
            }

            // Devolvemos la vista para que se muestre en el ListView.
            return item;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


    }

    class SeccionedAdapter extends BaseAdapter {
        public final Map<String, Adapter> sections = new LinkedHashMap<String, Adapter>();
        public final ArrayAdapter<String> headers;
        public final static int TYPE_SECTION_HEADER = 0;

        public SeccionedAdapter(Context context) {
            headers = new ArrayAdapter<String>(context, R.layout.list_item_header);
        }

        public void addSection(String section, Adapter adapter) {
            this.headers.add(section);
            this.sections.put(section, adapter);
        }

        public Object getItem(int position) {
            for (Object section : this.sections.keySet()) {
                Adapter adapter = sections.get(section);
                int size = adapter.getCount() + 1;

                // check if position inside this section
                if (position == 0) return section;
                if (position < size) return adapter.getItem(position - 1);

                // otherwise jump into next section
                position -= size;
            }
            return null;
        }

        public int getCount() {
            // total together all sections, plus one for each section header
            int total = 0;
            for (Adapter adapter : this.sections.values())
                total += adapter.getCount() + 1;
            return total;
        }

        @Override
        public int getViewTypeCount() {
            // assume that headers count as one, then total all sections
            int total = 1;
            for (Adapter adapter : this.sections.values())
                total += adapter.getViewTypeCount();
            return total;
        }

        @Override
        public int getItemViewType(int position) {
            int type = 1;
            for (Object section : this.sections.keySet()) {
                Adapter adapter = sections.get(section);
                int size = adapter.getCount() + 1;

                // check if position inside this section
                if (position == 0) return TYPE_SECTION_HEADER;
                if (position < size) return type + adapter.getItemViewType(position - 1);

                // otherwise jump into next section
                position -= size;
                type += adapter.getViewTypeCount();
            }
            return -1;
        }

        public boolean areAllItemsSelectable() {
            return false;
        }

        @Override
        public boolean isEnabled(int position) {
            return (getItemViewType(position) != TYPE_SECTION_HEADER);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            int sectionnum = 0;
            for (Object section : this.sections.keySet()) {
                Adapter adapter = sections.get(section);
                int size = adapter.getCount() + 1;

                // check if position inside this section
                if (position == 0) return headers.getView(sectionnum, convertView, parent);
                if (position < size) return adapter.getView(position - 1, convertView, parent);

                // otherwise jump into next section
                position -= size;
                sectionnum++;
            }
            return null;
        }

        public long getItemId(int position) {
            return position;
        }

    }


    private class LeerEfvoTask extends AsyncTask<Void, Void, Double> {

        private Context _context;

        public LeerEfvoTask(Context context) {
            _context = context;
        }


        @Override
        protected void onPreExecute() {
            txtTotalEfvo.setText("TOTAL EFECTIVO: consultando...");
        }

        @Override
        protected Double doInBackground(Void... urls) {
            double total = 0;

            try {

                //INGRESOS - EGRESOS
                Cursor movimientos = getContentResolver().query(
                        Uri.parse("content://com.pds.ficle.ep.caja.contentprovider/caja"),
                        new String[]{"sum(case when " + CajaHelper.CAJA_TIPO_MOV + " = 1 then " + CajaHelper.CAJA_MONTO + " * -1 else " + CajaHelper.CAJA_MONTO + " end)" },
                        CajaHelper.CAJA_MEDIO_PAGO + "=?",
                        new String[]{"EFECTIVO"},
                        null);

                // recorremos los items solo EFECTIVO sumando el monto
                if (movimientos.moveToFirst()) {
                    do {

                        total += movimientos.getDouble(0);

                        /*if (movimientos.getInt(CajaHelper.CAJA_IX_TIPO_MOV) == TipoMovimientoCaja.RETIRO.ordinal())
                            total += (-1) * movimientos.getDouble(CajaHelper.CAJA_IX_MONTO);
                        else
                            total += movimientos.getDouble(CajaHelper.CAJA_IX_MONTO);*/

                    } while (movimientos.moveToNext());
                }

                movimientos.close();

            } catch (Exception ex) {
                Toast.makeText(_context, ex.getMessage(), Toast.LENGTH_LONG).show();
            }

            return total;

        }

        @Override
        protected void onPostExecute(Double total) {

            totalEfvo = total;

            txtTotalEfvo.setText("TOTAL EFECTIVO: " + Formato.FormateaDecimal(totalEfvo, FORMATO_DECIMAL, Formato.getCurrencySymbol(FORMATO_DECIMAL)));
        }


    }

    private Button btnLoadMore;

    private class LeerMovimientosTask extends AsyncTask<Integer, Void, Void> {
        private Context _context;


        private SeccionedAdapter _adapter;
        private ArrayList<MovimientoRow> _movimientosCaja_list;

        private int _max;

        public LeerMovimientosTask(Context context) {
            _context = context;
        }

        @Override
        protected void onPreExecute() {

            lstMovimientosCaja.setVisibility(View.GONE);

            ((TextView) findViewById(android.R.id.empty)).setText("Consultando últimos movimientos...");
            findViewById(android.R.id.empty).setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Integer... max) {
            try {

                _max = max[0];

                String _limit = _max > 0 ? ("LIMIT " + String.valueOf(_max)) : "";

                Cursor movimientos = getContentResolver().query(
                        Uri.parse("content://com.pds.ficle.ep.caja.contentprovider/caja"),
                        CajaHelper.columnas,
                        "_id in (select _id from caja order by julianday(substr(fecha, 7)||'-'||substr(fecha,4,2)||'-'||substr(fecha,1,2)||'T'||hora) DESC " + _limit + ")",
                        new String[]{},
                        " julianday(substr(fecha, 7)||'-'||substr(fecha,4,2)||'-'||substr(fecha,1,2)||'T'||hora) DESC " + _limit);

                //iniciamos el adapter
                _adapter = new SeccionedAdapter(_context);

                List<MovimientoRow> movimientos_list = new ArrayList<MovimientoRow>(); //lista temporal para el corte de control

                _movimientosCaja_list = new ArrayList<MovimientoRow>(); //lista completa

                String fechaActual = "";

                // recorremos los items
                if (movimientos.moveToFirst()) {
                    do {

                        MovimientoRow movimientoRow = new MovimientoRow(
                                movimientos.getInt(CajaHelper.CAJA_IX_ID),
                                movimientos.getInt(CajaHelper.CAJA_IX_TIPO_MOV),
                                movimientos.getString(CajaHelper.CAJA_IX_FECHA),
                                movimientos.getString(CajaHelper.CAJA_IX_HORA),
                                movimientos.getDouble(CajaHelper.CAJA_IX_MONTO),
                                movimientos.getString(CajaHelper.CAJA_IX_OBSERVACION),
                                movimientos.getInt(CajaHelper.CAJA_IX_VENTA_ID),
                                movimientos.getString(CajaHelper.CAJA_IX_VENTA_DESC),
                                Formato.FormateaDecimal(movimientos.getDouble(CajaHelper.CAJA_IX_ACUMULADO), FORMATO_DECIMAL),
                                movimientos.getString(CajaHelper.CAJA_IX_MEDIO_PAGO),
                                movimientos.getString(CajaHelper.CAJA_IX_TIPO_VTA)
                        );
                    /*
                    if (movimientoRow.get_ventaId() != 0) {//si esta asociado a venta
                        movimientoRow.set_ventaDesc(ObtieneVentaAplicacion(movimientoRow.get_ventaId()));
                    }
                    */

                        //corte de control por fecha
                        if (!fechaActual.equals(movimientoRow.get_fecha_corta())) {

                            if (!fechaActual.equals("")) {//si no es el primero
                                _adapter.addSection(fechaActual, new MovimientoAdapter(_context, movimientos_list));
                            }

                            fechaActual = movimientoRow.get_fecha_corta();

                            movimientos_list = new ArrayList<MovimientoRow>();//reiniciamos la lista

                            movimientos_list.add(movimientoRow);

                        } else {
                            movimientos_list.add(movimientoRow);
                        }

                        _movimientosCaja_list.add(movimientoRow);

                    } while (movimientos.moveToNext());
                }

                //agregamos la ultima fecha
                _adapter.addSection(fechaActual, new MovimientoAdapter(_context, movimientos_list));

                movimientos.close();

            } catch (Exception ex) {
                Toast.makeText(_context, ex.getMessage(), Toast.LENGTH_LONG).show();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            adapter = _adapter;

            movimientosCaja_list = _movimientosCaja_list;

            //if (lstMovimientosCaja.getFooterViewsCount() < 1) {//si ya tiene el boton al pie, lo sacamos para luego volver a poner

            if(btnLoadMore == null)
                btnLoadMore = new Button(CashMainActivity.this);

            btnLoadMore.setText("Visualizar más movimientos históricos");
            btnLoadMore.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {

                    // get listview current position - used to maintain scroll position
                    int currentPosition = lstMovimientosCaja.getFirstVisiblePosition();

                    LeerMovimientosCaja(_max * 2);//el doble de movimientos

                    // Setting new scroll position
                    lstMovimientosCaja.setSelectionFromTop(currentPosition + 1, 0);
                }
            });

            // Agregamos el boton como footer del listview
            if (_max > 0 && movimientosCaja_list.size() == _max)
                lstMovimientosCaja.addFooterView(btnLoadMore);
            else
                lstMovimientosCaja.removeFooterView(btnLoadMore);

            //}

            lstMovimientosCaja.setAdapter(adapter);

            if (movimientosCaja_list.size() == 0)
                ((TextView) findViewById(android.R.id.empty)).setText("No se encontraron registros");
            else
                lstMovimientosCaja.setVisibility(View.VISIBLE);

            //lstMovimientosCaja.setAdapter(new MovimientoAdapter(this, movimientos_list));

            EstadoImpresion();//habilitamos la opcion de impresion solo si hay items para imprimir
        }

    }

/*
    class MovimientoAdapter extends ArrayAdapter<MovimientoRow> {
        private Context context;
        private List<MovimientoRow> datos;
        //private String fechaActual = "";

        public MovimientoAdapter(Context context, List<MovimientoRow> datos) {
            super(context, android.R.layout.simple_list_item_1, datos);

            this.context = context;
            this.datos = datos;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // En primer lugar "inflamos" una nueva vista, que será la que se
            // mostrará en la celda del ListView. Para ello primero creamos el
            // inflater, y después inflamos la vista.
            LayoutInflater inflater = LayoutInflater.from(context);
            View item = inflater.inflate(R.layout.cash_mov_item, null);

            MovimientoRow row = datos.get(position);

            // A partir de la vista, recogeremos los controles que contiene para
            // poder manipularlos.


            // Recogemos los TextView para mostrar datos
            ((TextView) item.findViewById(R.id.txt_cash_mov_item_fecha)).setText(row.get_hora_corta());
            ((TextView) item.findViewById(R.id.txt_cash_mov_item_tipo_mov)).setText(row.get_tipoMov());
            ((TextView) item.findViewById(R.id.txt_cash_mov_item_medio_pago)).setText(row.get_medioPago_Corto());
            ((TextView) item.findViewById(R.id.txt_cash_mov_item_monto)).setText(row.get_monto_format());
            ((TextView) item.findViewById(R.id.txt_cash_mov_item_acumulado)).setText(Formatos.FormateaDecimal(row.get_acumulado(), Formatos.DecimalFormat, "$"));

            if (position == 0) { //destacamos el saldo actual
                ((TextView) item.findViewById(R.id.txt_cash_mov_item_acumulado)).setTypeface(null, Typeface.BOLD);
                ((TextView) item.findViewById(R.id.txt_cash_mov_item_acumulado)).setBackgroundColor(getResources().getColor(R.color.boton_gris));
            } else {
                ((TextView) item.findViewById(R.id.txt_cash_mov_item_acumulado)).setTypeface(null, Typeface.NORMAL);
                //((TextView) item.findViewById(R.id.txt_cash_mov_item_acumulado)).setBackgroundColor(getResources().getColor(R.color.cash_cell));
            }

            if (!row.get_ventaDesc().equals(""))
                ((TextView) item.findViewById(R.id.txt_cash_mov_item_tipo_mov)).setText(row.get_ventaDesc());

            if (row.get_tipoMovId() == TipoMovimientoCaja.ARQUEO.ordinal()) {
                //si es ajuste ponemos bold los campos
                ((TextView) item.findViewById(R.id.txt_cash_mov_item_fecha)).setTypeface(null, Typeface.BOLD);
                ((TextView) item.findViewById(R.id.txt_cash_mov_item_tipo_mov)).setTypeface(null, Typeface.BOLD);
                ((TextView) item.findViewById(R.id.txt_cash_mov_item_monto)).setTypeface(null, Typeface.BOLD);
                ((TextView) item.findViewById(R.id.txt_cash_mov_item_acumulado)).setTypeface(null, Typeface.BOLD);
                ((TextView) item.findViewById(R.id.txt_cash_mov_item_medio_pago)).setTypeface(null, Typeface.BOLD);

            }

            //if(row.get_monto().equals(""))
            //    ((TextView) item.findViewById(R.id.txt_cash_mov_item_acumulado)).set

            // Devolvemos la vista para que se muestre en el ListView.
            return item;

        }
    }
*/


    /*
    private String ObtieneVentaAplicacion(int idVenta) {
        String aplicacionVenta = null;

        try {

            Cursor venta = getContentResolver().query(
                    Uri.parse("content://com.pds.ficle.ep.ventas.contentprovider/ventas"),
                    new String[]{VentasHelper.VENTA_CODIGO},
                    VentasHelper.VENTA_ID + "=?",
                    new String[]{String.valueOf(idVenta)},
                    VentasHelper.VENTA_ID + " ASC");


            // recorremos los items
            if (venta.moveToFirst()) {
                do {

                    aplicacionVenta = venta.getString(0);

                    if (aplicacionVenta.equals("RCG"))
                        aplicacionVenta = "RECARGA";

                    if (aplicacionVenta.equals("PDV"))
                        aplicacionVenta = "VENTA";

                    if (aplicacionVenta.equals("DEP"))
                        aplicacionVenta = "DEPOSITO";

                    if (aplicacionVenta.equals("PAG"))
                        aplicacionVenta = "PAGO SERV.";


                } while (venta.moveToNext());
            }

            venta.close();

        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
        } finally {
            return aplicacionVenta;
        }
    }
    */
}
