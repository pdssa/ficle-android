package com.pds.ficle.cashdrawer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.pds.common.Caja;
import com.pds.common.Formato;
import com.pds.common.Logger;
import com.pds.common.dao.ComprobanteDao;
import com.pds.common.model.Venta;
import com.pds.common.dao.CierreDao;
import com.pds.common.dao.VentaDao;
import com.pds.common.model.Cierre;
import com.pds.common.model.Comprobante;
import com.pds.common.model.VentaAbstract;
import com.pds.common.model.VentaDetalleAbstract;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Hernan on 10/12/2014.
 */
public class CierreDiarioTask extends AsyncTask<Date, CharSequence, String> {
    private ProgressDialog dialog;
    private Context context;


    public CierreDiarioTask(Context _context) {
        context = _context;
        dialog = new ProgressDialog(context);

        SolicitarFechaCierre();
    }

    private void SolicitarFechaCierre() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();

        final View vw = inflater.inflate(R.layout.dialog_cierre, null);

        vw.findViewById(R.id.dialog_cierre_btn_fec_hasta).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PickDate(vw.findViewById(R.id.dialog_cierre_txt_fec_hasta));
            }
        });

        final Date last_cierre = GetUltimoCierre();
        fechaCierreIngresada = Calendar.getInstance();//fecha / hora actual

        ((EditText) vw.findViewById(R.id.dialog_cierre_txt_fec_desde)).setText(last_cierre != null ? Formato.FormateaDate(last_cierre, Formato.DateFormatSlashAndTime) : "No registra");//ultimo cierre
        ((EditText) vw.findViewById(R.id.dialog_cierre_txt_fec_hasta)).setText(Formato.FormateaDate(fechaCierreIngresada.getTime(), Formato.DateFormatSlashAndTime));//default: hasta ahora

        builder.setView(vw)
                // Add action buttons
                .setPositiveButton("ACEPTAR", null)
                .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

        final AlertDialog dialog = builder.create();

        dialog.setTitle("CIERRE DIARIO");
        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
            }
        });
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface d) {
                Button b = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {

                            //Date fecCierre = Formatos.ObtieneDate(((EditText) ((AlertDialog) dialog).findViewById(R.id.dialog_cierre_txt_fec_hasta)).getText().toString(), Formatos.DateFormatSlash);

                            Date fecCierre = fechaCierreIngresada.getTime();

                            //controlamos que no sea una fecha futura
                            if (!fecCierre.after(new Date())) {
                                //controlamos que no ingresen una fecha anterior al del ultimo cierre
                                if(last_cierre == null || !fecCierre.before(last_cierre)){
                                    dialog.dismiss();

                                    execute(fecCierre);
                                }
                                else {
                                    Toast.makeText(context, "La fecha de cierre no puede ser anterior a la fecha del ultimo cierre", Toast.LENGTH_SHORT).show();
                                }

                            } else {
                                Toast.makeText(context, "La fecha de cierre no puede ser posterior a la fecha actual", Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            Toast.makeText(context, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });

        dialog.show();
    }

    private Date GetUltimoCierre() {
        //obtenemos la fecha del ultimo cierre
        Cierre ultimo_cierre = new CierreDao(context.getContentResolver()).first(null, null, "fecha DESC");

        Date fecha_ultimo_cierre = null;
        if (ultimo_cierre != null)
            fecha_ultimo_cierre  = ultimo_cierre.getFecha();

        return fecha_ultimo_cierre;
    }


    Calendar fechaCierreIngresada = null; //esta es la fecha que tomaremos como "corte" para el cierre a generar

    private void PickDate(final View _view) {
        Calendar hoy = Calendar.getInstance();

        DatePickerDialog datePicker = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                fechaCierreIngresada = Calendar.getInstance();
                fechaCierreIngresada.set(year, monthOfYear, dayOfMonth);

                //PickHour(_view);

            }
        }, hoy.get(Calendar.YEAR), hoy.get(Calendar.MONTH), hoy.get(Calendar.DAY_OF_MONTH));
        datePicker.setTitle("Seleccione fecha de cierre");
        datePicker.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                //al cerrar pedimos la hora
                PickHour(_view);
           }
        });
        datePicker.show();

    }

    private void PickHour(final View _view){
        Calendar hoy = Calendar.getInstance();

        TimePickerDialog timePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                fechaCierreIngresada.set(Calendar.HOUR_OF_DAY, hourOfDay);
                fechaCierreIngresada.set(Calendar.MINUTE, minute);
                //si el usuario selecciona hora/minuto colocamos 00 segundos, es simil HASTA EL INICIO DE ESE MINUTO
                //si toma la fecha/hora default, se usarán los segundos del momento
                fechaCierreIngresada.set(Calendar.SECOND, 00);

                ((EditText) _view).setText(Formato.FormateaDate(fechaCierreIngresada.getTime(), Formato.DateFormatSlashAndTime));

            }
        }, hoy.get(Calendar.HOUR_OF_DAY), hoy.get(Calendar.MINUTE), true);
        timePicker.setTitle("Seleccione hora de cierre");
        timePicker.show();
    }

    @Override
    protected void onPreExecute() {
        //iniciamos un mensaje para el usuario
        dialog = new ProgressDialog(context);
        dialog.setMessage("Obteniendo información de ventas. Aguarde por favor...");
        dialog.setIndeterminate(false);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    @Override
    protected String doInBackground(Date... params) {
        try {

            Thread.sleep(2000);

            //obtenemos la fecha del ultimo cierre
            Date ultimo_cierre = GetUltimoCierre();

            //obtenemos la fecha de cierre
            Date fecha_cierre = params[0];

            String filter = "";

            //si es el primer cierre => traemos todas las ventas sin anular
            if (ultimo_cierre == null)
                filter = "";
            else
                filter = "anulada = 0 AND julianday(substr(fecha, 7)||'-'||substr(fecha,4,2)||'-'||substr(fecha,1,2)||' '||hora) > julianday('" + Formato.FormateaDate(ultimo_cierre, Formato.DbDateTimeFormat) + "') AND ";

            //agregamos la fecha de corte del cierre
            filter += "julianday(substr(fecha, 7)||'-'||substr(fecha,4,2)||'-'||substr(fecha,1,2)||' '||hora) <= julianday('" + Formato.FormateaDate(fecha_cierre, Formato.DbDateTimeFormat) + "') ";

            //obtener ventas y comprobantes posteriores al ultimo cierre
            List<Venta> _ventas = new VentaDao(context.getContentResolver()).list(filter, null, "_id");
            List<Comprobante> _comprobantes = new ComprobanteDao(context.getContentResolver()).list(filter, null, "_id");

            //tratamos ventas y comprobantes de la misma forma
            List<VentaAbstract> ventas = new ArrayList<VentaAbstract>();
            ventas.addAll(_ventas);
            ventas.addAll(_comprobantes);

            publishProgress(Html.fromHtml(String.format("Cantidad de ventas generadas: %d<br/>Cantidad de comprobantes generados: %d<br/><br/>Generando cierre. Aguarde por favor...", _ventas.size(), _comprobantes.size())));

            Thread.sleep(3000);

            //generamos el cierre
            //si es el primer cierre, tomamos como "fecha desde" temporalmente la fecha de hoy, al recorrer ventas vamos a asignar la fecha de la mas antigua
            boolean calcularFecha = false;
            if(ultimo_cierre == null)//hay que obtener la fecha desde en base a las fechas de venta
                calcularFecha = true;

            Cierre cierre = new Cierre(
                    calcularFecha ? new Date() : ultimo_cierre , //luego vamos a ponerle la fecha de la primer venta
                    fecha_cierre);

            //procesamos las ventas para obtener el detalle del cierre
            for (VentaAbstract venta : ventas) {

                //vemos si necesitamos calcular la fecha desde del cierre en base a las ventas
                if(calcularFecha && venta.getFechaHora_Date().before(cierre.getFechaDesde())){
                    cierre.setFechaDesde(venta.getFechaHora_Date());
                }

                //incrementamos una operacion
                cierre.setOperaciones_cant(cierre.getOperaciones_cant() + 1);

                //si la venta es menor a 180$ es una venta MENOR
                if (venta.getTotal() < 180) {
                    cierre.setMenores_total(cierre.getMenores_total() + venta.getTotal());
                } else {
                    //ahora a revisar los detalles de cada venta
                    venta.addDetalles(context.getContentResolver());
                    for (VentaDetalleAbstract det : venta.getDetallesAbstract()) {

                        //producto generico => lo consideramos venta AFECTA
                        if(det.getProducto() == null){
                            cierre.setAfectas_total(cierre.getAfectas_total() + det.getTotal());
                        } else if(det.getProducto().getIdTax() == 4) {
                            //venta EXENTA
                            cierre.setExentas_total(cierre.getExentas_total() + det.getTotal());
                        } else {
                            //venta AFECTA
                            cierre.setAfectas_total(cierre.getAfectas_total() + det.getTotal());
                        }

                        //venta OTROS: por ahora nada
                        cierre.setOtros_total(0);

                        //producto generico => lo consideramos venta AFECTA
                        /*if (det.getIdProducto() == -1 || (det.getProducto() != null && det.getProducto().getIva() > 0)) {
                            cierre.setAfectas_total(cierre.getAfectas_total() + det.getTotal());
                        }

                        //ventas sin IVA
                        if (det.getIdProducto() != -1 && det.getProducto() != null && det.getProducto().getIva() == 0) {
                            //venta EXENTA
                            if (det.getProducto().getIdTax() == 4) {
                                cierre.setExentas_total(cierre.getExentas_total() + det.getTotal());
                            } else {
                                //venta AFECTA SIN IVA
                                cierre.setOtros_total(cierre.getOtros_total() + det.getTotal());
                            }
                        }*/
                    }
                }


            }

            //grabamos el cierre
            CierreDao cierreDao = new CierreDao(context.getContentResolver());

            //redondeamos valores
            cierre.setAfectas_total(Formato.RedondeaDecimal(cierre.getAfectas_total()));
            cierre.setExentas_total(Formato.RedondeaDecimal(cierre.getExentas_total()));
            cierre.setMenores_total(Formato.RedondeaDecimal(cierre.getMenores_total()));
            cierre.setOtros_total(Formato.RedondeaDecimal(cierre.getOtros_total()));

            //calculamos el IVA
            cierre.setIva(Formato.RedondeaDecimal((cierre.getAfectas_total() + cierre.getMenores_total()) * 0.19 / 1.19));//es IVA incluido

            boolean result = cierreDao.save(cierre);

            //grabamos TRX de cierre
            if (result) {
                Caja.RegistrarMovimientoCaja(
                        context,
                        Caja.TipoMovimientoCaja.CIERRE,
                        //new DecimalFormat("0.00").format(Double.valueOf(montoIngresado)),
                        //Formatos.ParseaFormateaDecimal("0", Formatos.DecimalFormat, Formatos.DecimalFormat_US),
                        Formato.FormateaDecimal(0, Formato.Decimal_Format.US),
                        "",
                        "");

                Logger.RegistrarEvento(
                        context,
                        "i",
                        Caja.TipoMovimientoCaja.CIERRE.name() + " CAJA",
                        "");
            }

            return result ? "ok" : "error";

        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @Override
    protected void onProgressUpdate(CharSequence... values) {
        dialog.setMessage(values[0]);
    }

    @Override
    protected void onPostExecute(String success) {
        if (dialog.isShowing())
            dialog.dismiss();

        String mensaje = "Cierre diario registrado correctamente.<br/>A continuación, por favor proceda con el arqueo de caja";
        if (!success.equals("ok"))
            mensaje = "Se ha producido un error al intentar registrar el cierre";
        else
            ((CashMainActivity) context).IniciarProcesoArqueoParcial();//continuamos realizando un arqueo parcial utilizando el monto ingresado

        AlertMessage(mensaje);
    }


    @Override
    protected void onCancelled(String s) {
        AlertMessage(s);
    }

    private void AlertMessage(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("CIERRE DIARIO");
        builder.setMessage(Html.fromHtml(message));
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface _dialog, int which) {
                _dialog.dismiss();
            }
        });
        builder.setCancelable(false);
        builder.show();
    }
}
