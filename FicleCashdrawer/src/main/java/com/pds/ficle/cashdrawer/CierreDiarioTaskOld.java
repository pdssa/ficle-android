package com.pds.ficle.cashdrawer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.pds.common.Caja;
import com.pds.common.Formatos;
import com.pds.common.Logger;
import com.pds.common.dao.CierreDao;
import com.pds.common.dao.ComprobanteDao;
import com.pds.common.dao.VentaDao;
import com.pds.common.model.Cierre;
import com.pds.common.model.Comprobante;
import com.pds.common.model.Venta;
import com.pds.common.model.VentaAbstract;
import com.pds.common.model.VentaDetalleAbstract;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

//import com.pds.common.dialog.TimePickerDialog;

/**
 * Created by Hernan on 10/12/2014.
 */
public class CierreDiarioTaskOld extends AsyncTask<Date, CharSequence, String> {
    private ProgressDialog dialog;
    private Context context;


    public CierreDiarioTaskOld(Context _context) {
        context = _context;
        dialog = new ProgressDialog(context);

        SolicitarFechaCierre();
    }

    private void SolicitarFechaCierre() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();

        final View vw = inflater.inflate(R.layout.dialog_cierre, null);

        vw.findViewById(R.id.dialog_cierre_btn_fec_hasta).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PickDate(vw.findViewById(R.id.dialog_cierre_txt_fec_hasta));
            }
        });

        Date last_cierre = GetUltimoCierre();

        ((EditText) vw.findViewById(R.id.dialog_cierre_txt_fec_desde)).setText(last_cierre != null ? Formatos.FormateaDate(last_cierre, Formatos.DateFormatSlashAndTime) : "No registra");//ultimo cierre
        ((EditText) vw.findViewById(R.id.dialog_cierre_txt_fec_hasta)).setText(Formatos.FormateaDate(new Date(), Formatos.DateFormatSlashAndTime));//default: hasta ahora

        builder.setView(vw)
                // Add action buttons
                .setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        try {

                            //Date fecCierre = Formatos.ObtieneDate(((EditText) ((AlertDialog) dialog).findViewById(R.id.dialog_cierre_txt_fec_hasta)).getText().toString(), Formatos.DateFormatSlash);

                            Date fecCierre = fechaCierreIngresada.getTime();

                            execute(fecCierre);

                        } catch (Exception e) {
                            Toast.makeText(context, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

        AlertDialog dialog = builder.create();

        dialog.setTitle("CIERRE DIARIO");
        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
            }
        });

        dialog.show();
    }

    private Date GetUltimoCierre() {
        //obtenemos la fecha del ultimo cierre
        Cierre ultimo_cierre = new CierreDao(context.getContentResolver()).first(null, null, "fecha DESC");

        Date fecha_ultimo_cierre = null;
        if (ultimo_cierre != null)
            fecha_ultimo_cierre = ultimo_cierre.getFecha();

        return fecha_ultimo_cierre;
    }


    Calendar fechaCierreIngresada = null; //esta es la fecha que tomaremos como "corte" para el cierre a generar

    private void PickDate(final View _view) {
        Calendar hoy = Calendar.getInstance();

        DatePickerDialog datePicker = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                fechaCierreIngresada = Calendar.getInstance();
                fechaCierreIngresada.set(year, monthOfYear, dayOfMonth);

                //PickHour(_view);

            }
        }, hoy.get(Calendar.YEAR), hoy.get(Calendar.MONTH), hoy.get(Calendar.DAY_OF_MONTH));
        datePicker.setTitle("Seleccione fecha de cierre");
        datePicker.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                //al cerrar pedimos la hora
                PickHour(_view);
           }
        });
        datePicker.show();

    }

    private void PickHour(final View _view){
        Calendar hoy = Calendar.getInstance();

        TimePickerDialog timePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                fechaCierreIngresada.set(Calendar.HOUR_OF_DAY, hourOfDay);
                fechaCierreIngresada.set(Calendar.MINUTE, minute);
                fechaCierreIngresada.set(Calendar.SECOND, 59);

                ((EditText) _view).setText(Formatos.FormateaDate(fechaCierreIngresada.getTime(), Formatos.DateFormatSlashAndTime));

            }
        }, hoy.get(Calendar.HOUR_OF_DAY), hoy.get(Calendar.MINUTE), true);
        timePicker.setTitle("Seleccione hora de cierre");
        timePicker.show();
    }

    @Override
    protected void onPreExecute() {
        //iniciamos un mensaje para el usuario
        dialog = new ProgressDialog(context);
        dialog.setMessage("Obteniendo información de ventas. Aguarde por favor...");
        dialog.setIndeterminate(false);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    @Override
    protected String doInBackground(Date... params) {
        try {

            Thread.sleep(3000);

            //obtenemos la fecha del ultimo cierre
            Date ultimo_cierre = GetUltimoCierre();

            //obtenemos la fecha de cierre
            Date fecha_cierre = params[0];

            String filter = "";

            //si es el primer cierre => traemos todas las ventas
            if (ultimo_cierre == null)
                filter = "";
            else
                filter = "julianday(substr(fecha, 7)||'-'||substr(fecha,4,2)||'-'||substr(fecha,1,2)||' '||hora) > julianday('" + Formatos.FormateaDate(ultimo_cierre, Formatos.DbDateTimeFormat) + "') AND ";

            //agreamos la fecha de corte del cierre
            filter += "julianday(substr(fecha, 7)||'-'||substr(fecha,4,2)||'-'||substr(fecha,1,2)||' '||hora) <= julianday('" + Formatos.FormateaDate(fecha_cierre, Formatos.DbDateTimeFormat) + "') ";

            //obtener ventas y comprobantes posteriores al ultimo cierre
            List<Venta> _ventas = new VentaDao(context.getContentResolver()).list(filter, null, "_id");
            List<Comprobante> _comprobantes = new ComprobanteDao(context.getContentResolver()).list(filter, null, "_id");

            //tratamos ventas y comprobantes de la misma forma
            List<VentaAbstract> ventas = new ArrayList<VentaAbstract>();
            ventas.addAll(_ventas);
            ventas.addAll(_comprobantes);

            publishProgress(Html.fromHtml(String.format("Cantidad de ventas generadas: %d<br/>Cantidad de comprobantes generados: %d<br/><br/>Generando cierre. Aguarde por favor...", _ventas.size(), _comprobantes.size())));

            Thread.sleep(3000);

            //MM-YYYY -> Cierre
            HashMap<String, Cierre> _cierres = new HashMap<String, Cierre>();

            //procesamos las ventas para obtener el detalle del cierre
            //int afectas_q = 0, exentas_q = 0, menores_q = 0, otros_q = 0;
            //double afectas_t = 0, exentas_t = 0, menores_t = 0, otros_t = 0, iva = 0;
            boolean afecta = false, exenta = false, otro = false;

            //Ventas
            for (VentaAbstract venta : ventas) {

                String mes = Formatos.FormateaDate(venta.getFechaHora_Date(), Formatos.MonthYearFormat);
                boolean mes_actual = mes.equals(Formatos.FormateaDate(new Date(), Formatos.MonthYearFormat));

                //agregamos el mes de la venta
                if (!_cierres.containsKey(mes)) {

                    //si el mes es el que está en curso => fecha de cierre es HOY
                    if (mes_actual)
                        _cierres.put(mes, new Cierre(null, new Date()));//<<---TODO: corregir, debería ser la fecha ingresada por el user
                    else
                        //sino, le ponemos la fecha de una venta del mes
                        _cierres.put(mes, new Cierre(null, venta.getFechaHora_Date()));//<<---TODO: corregir, debería ser el último momento del mes...
                }

                //buscamos el cierre del mes
                Cierre cierre_mes = _cierres.get(mes);

                //vamos a dejar el cierre con la fecha de la ultima venta
                //al cierre de este mes no le cambiamos la fecha, queda con la del dia de hoy
                //TODO: si ya seteamos antes las fechas correctas, no es necesario esto...
                if (!mes_actual && venta.getFechaHora_Date().after(cierre_mes.getFecha())) {
                    cierre_mes.setFecha(venta.getFechaHora_Date());
                }

                afecta = false;
                exenta = false;
                otro = false;

                //si la venta es menor a 180$ es una venta MENOR
                if (venta.getTotal() < 180) {
                    cierre_mes.setMenores_cant(cierre_mes.getMenores_cant() + 1);
                    cierre_mes.setMenores_total(cierre_mes.getMenores_total() + venta.getTotal());
                } else {
                    //ahora a revisar los detalles de cada venta
                    venta.addDetalles(context.getContentResolver());
                    for (VentaDetalleAbstract det : venta.getDetallesAbstract()) {
                        //producto generico => lo consideramos venta AFECTA
                        if (det.getIdProducto() == -1 || (det.getProducto() != null && det.getProducto().getIva() > 0)) {
                            afecta = true;
                            cierre_mes.setAfectas_total(cierre_mes.getAfectas_total() + det.getTotal());
                        }

                        //ventas sin IVA
                        if (det.getIdProducto() != -1 && det.getProducto() != null && det.getProducto().getIva() == 0) {
                            //venta EXENTA
                            if (det.getProducto().getIdTax() == 4) {
                                exenta = true;
                                cierre_mes.setExentas_total(cierre_mes.getExentas_total() + det.getTotal());
                            } else {
                                //venta AFECTA SIN IVA
                                otro = true;
                                cierre_mes.setOtros_total(cierre_mes.getOtros_total() + det.getTotal());
                            }
                        }
                    }

                    //como tiene items AFECTOS => sumamos 1 en cantidad
                    if (afecta)
                        cierre_mes.setAfectas_cant(cierre_mes.getAfectas_cant() + 1);

                    //como tiene items EXENTOS => sumamos 1 en cantidad
                    if (exenta)
                        cierre_mes.setExentas_cant(cierre_mes.getExentas_cant() + 1);

                    //como tiene items OTROS => sumamos 1 en cantidad
                    if (otro)
                        cierre_mes.setOtros_cant(cierre_mes.getOtros_cant() + 1);

                }
            }

            //grabamos los cierres
            boolean result = true;
            CierreDao cierreDao = new CierreDao(context.getContentResolver());
            if (_cierres.values().size() > 0) {
                for (Cierre cierre : _cierres.values()) {
                    //redondeamos valores
                    cierre.setAfectas_total(Formatos.RedondeaDecimal(cierre.getAfectas_total()));
                    cierre.setExentas_total(Formatos.RedondeaDecimal(cierre.getExentas_total()));
                    cierre.setMenores_total(Formatos.RedondeaDecimal(cierre.getMenores_total()));
                    cierre.setOtros_total(Formatos.RedondeaDecimal(cierre.getOtros_total()));

                    //calculamos el IVA
                    cierre.setIva(Formatos.RedondeaDecimal((cierre.getAfectas_total() + cierre.getMenores_total()) * 0.19 / 1.19));//es IVA incluido
                    result = cierreDao.save(cierre);
                }
            } else {
                //grabamos un cierre en 0
                Cierre cierre_en_cero = new Cierre(null, new Date());
                result = cierreDao.save(cierre_en_cero);
            }

            //grabamos TRX de cierre
            if (result) {
                Caja.RegistrarMovimientoCaja(
                        context,
                        Caja.TipoMovimientoCaja.CIERRE,
                        //new DecimalFormat("0.00").format(Double.valueOf(montoIngresado)),
                        Formatos.ParseaFormateaDecimal("0", Formatos.DecimalFormat, Formatos.DecimalFormat_US),
                        "",
                        "");

                Logger.RegistrarEvento(
                        context,
                        "i",
                        Caja.TipoMovimientoCaja.CIERRE.name() + " CAJA",
                        "");
            }

            return result ? "ok" : "error";

        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @Override
    protected void onProgressUpdate(CharSequence... values) {
        dialog.setMessage(values[0]);
    }

    @Override
    protected void onPostExecute(String success) {
        if (dialog.isShowing())
            dialog.dismiss();

        String mensaje = "Cierre registrado correctamente";
        if (!success.equals("ok"))
            mensaje = "Se ha producido un error al intentar registrar el cierre";
        else
            ((CashMainActivity) context).LeerMovimientosCaja();//dado que generamos una TRX de CIERRE, actualizamos los movimientos

        AlertMessage(mensaje);
    }


    @Override
    protected void onCancelled(String s) {
        AlertMessage(s);
    }

    private void AlertMessage(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("ARQUEO DE CIERRE");
        builder.setMessage(message);
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface _dialog, int which) {
                _dialog.dismiss();
            }
        });
        builder.setCancelable(false);
        builder.show();
    }
}
