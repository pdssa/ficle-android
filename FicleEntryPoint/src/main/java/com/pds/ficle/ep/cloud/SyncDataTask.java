package com.pds.ficle.ep.cloud;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.pds.common.CajaHelper;
import com.pds.common.Config;
import com.pds.common.DbHelper;
import com.pds.common.Formatos;
import com.pds.common.LogHelper;
import com.pds.common.Logger;
import com.pds.common.R;
import com.pds.common.UsuarioHelper;
import com.pds.common.VentaDetalleHelper;
import com.pds.common.VentasHelper;
import com.pds.common.dao.ClienteDao;
import com.pds.common.dao.DepartmentDao;
import com.pds.common.dao.ProductDao;
import com.pds.common.dao.ProviderDao;
import com.pds.common.dao.RangoFolioDao;
import com.pds.common.dao.SubDepartmentDao;
import com.pds.common.dao.TaxDao;
import com.pds.common.dao.TipoDocDao;
import com.pds.common.dao.VentaDao;
import com.pds.common.db.ClienteTable;
import com.pds.common.db.ComboItemTable;
import com.pds.common.db.ComboTable;
import com.pds.common.db.CompraDetalleTable;
import com.pds.common.db.CompraTable;
import com.pds.common.db.ComprobanteDetalleTable;
import com.pds.common.db.ComprobanteTable;
import com.pds.common.db.CuentaTable;
import com.pds.common.db.DepartmentTable;
import com.pds.common.db.PedidoDetalleTable;
import com.pds.common.db.PedidoTable;
import com.pds.common.db.ProductTable;
import com.pds.common.db.ProviderTable;
import com.pds.common.db.SubdepartmentTable;
import com.pds.common.db.TipoDocTable;
import com.pds.common.model.Product;
import com.pds.common.model.RangoFolio;
import com.pds.common.model.SubDepartment;
import com.pds.common.model.Tax;
import com.pds.common.model.TipoDoc;
import com.pds.common.model.Venta;
import com.pds.common.pref.PDVConfig;
import com.pds.common.pref.ValeConfig;
import com.pds.common.util.ConnectivityUtils;
import com.pds.common.util.ContentProviders;
import com.pds.ficle.ep.cloud.process_entity_helpers.BuyProcessEntityHelper;
import com.pds.ficle.ep.cloud.process_entity_helpers.CajaProcessEntityHelper;
import com.pds.ficle.ep.cloud.process_entity_helpers.ComprobanteProcessEntityHelper;
import com.pds.ficle.ep.cloud.process_entity_helpers.ProcessEntityHelper;
import com.pds.ficle.ep.cloud.process_entity_helpers.VentaProcessEntityHelper;
import com.pds.ficle.ep.models.syncDataTask.SyncDataTaskResponse;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Created by Hernan on 12/06/2014.
 */
public class SyncDataTask extends AsyncTask<Void, String, String> {
    private static final String TAG = SyncDataTask.class.getSimpleName();
    protected String mAndroidId;
    protected String mImei;

    public boolean _silentMode = false;
    public Context _context;
    public String _callbackMethod;
    public Config _config;
    public boolean _syncingOnExit;

    public boolean _activateSync = true;

    public boolean _hubo_error = false;

    private final int TYPE_LOG = 1;
    private final int TYPE_VENTA = 2;
    private final int TYPE_USER = 3;
    private final int TYPE_CONFIG = 4;
    private final int TYPE_COMPROBANTE = 5;
    private final int TYPE_CAJA = 6;
    private final int TYPE_PRODUCTOS = 7;
    private final int TYPE_DEPARTAMENTOS = 8;
    private final int TYPE_SUBDEPARTAMENTOS = 9;
    private final int TYPE_PROVEEDORES = 10;
    private final int TYPE_COMPRA = 11;
    private final int TYPE_DET_COMPRA = 12;
    private final int TYPE_DET_VENTA = 13;
    private final int TYPE_DET_COMPROBANTE = 14;
    private final int TYPE_TIPO_DOC = 15;
    private final int TYPE_RANGOS = 16;
    private final int TYPE_PEDIDO = 17;
    private final int TYPE_DET_PEDIDO = 18;
    private final int TYPE_PRODS_CAT = 19;
    private final int TYPE_CLIENTE = 20;
    private final int TYPE_CAT_VERS = 21;
    private final int TYPE_COMBO = 22;
    private final int TYPE_COMBO_ITEM = 23;

    private final int TYPE_CUENTA_CTE = 24;

    private boolean CONTINUE = false;

    public void set_context(Context _context) {
        this._context = _context;
    }

    public void set_activateSync(boolean activateSync) {
        this._activateSync = activateSync;
    }

    public boolean isSyncActivated() {
        return this._activateSync;
    }

    public SyncDataTask() {
    }

    public SyncDataTask(Context _context, String callbackMethod, boolean onExit, boolean silentMode) {
        this._context = _context;
        this._callbackMethod = callbackMethod;
        this._config = new Config(_context, true);
        this._syncingOnExit = onExit;
        this._silentMode = silentMode;
        this.mImei = this._config.IMEI();
        this.mAndroidId = this._config.ANDROID_ID;
    }

    //region Entidades -> JSON
    private JSONArray ConfigToJSON() throws Exception {

        JSONArray resultSet = new JSONArray();
        JSONObject rowObject = new JSONObject();

        //evitamos enviar datos que son configurados en la plataforma, no en el terminal

        rowObject.put("androidId", this._config.ANDROID_ID);
        rowObject.put("googleAccount", this._config.GOOGLE_ACCOUNT);
        //rowObject.put("comercio", this._config.COMERCIO);
        //rowObject.put("claveFiscal", this._config.CLAVEFISCAL);
        //rowObject.put("direccion", this._config.DIRECCION);
        rowObject.put("imei", this._config.IMEI());
        rowObject.put("imsi", this._config.IMSI);
        rowObject.put("latitud", this._config.LOCATION_LATITUDE);
        rowObject.put("longitud", this._config.LOCATION_LONGITUDE);
        //rowObject.put("idComercio", this._config.ID_COMERCIO);
        rowObject.put("datetime", Formatos.FormateaDate(new Date(), Formatos.DbDateTimeFormatTimeZone));
        rowObject.put("wifiMac", Config.getMACAddress("wlan0"));
        rowObject.put("ethMac", Config.getMACAddress("eth0"));
        rowObject.put("ip", Config.getIPAddress(true));
        rowObject.put("batLevel", this._config.GetBatteryLevel());
        rowObject.put("batStatus", this._config.GetBatteryStatus());
        rowObject.put("androidVers", this._config.getAndroidVers());
        rowObject.put("modelName", this._config.getModelName());

        //---PARAMETROS DEL SIM---
        JSONObject simParams = new JSONObject();
        simParams.put("SIM_SERIAL_NUMBER", _config.SIM_SERIALNUMBER);
        simParams.put("STATE", _config.SIM_STATE);
        simParams.put("PHONE_TYPE", _config.SIM_PHONE_TYPE);
        simParams.put("COUNTRY_ISO", _config.SIM_COUNTRY);
        simParams.put("PHONE_NUMBER", _config.SIM_LINE1_NRO);
        simParams.put("NETWORK_COUNTRY_ISO", _config.SIM_NETWORK_COUNTRY);
        simParams.put("NETWORK_OPERATOR_CODE", _config.SIM_NETWORK_OPERATOR);
        simParams.put("NETWORK_OPERATOR_NAME", _config.SIM_NETWORK_OPERATOR_NAME);
        simParams.put("NETWORK_TYPE", _config.SIM_NETWORK_TYPE);
        simParams.put("OPERATOR_CODE", _config.SIM_OPERATOR_CODE);
        simParams.put("OPERATOR_NAME", _config.SIM_OPERATOR_NAME);
        simParams.put("VOICE_MAIL_NUMBER", _config.SIM_VOICE_MAIL_NRO);
        simParams.put("VOICE_MAIL_TEXT_ID", _config.SIM_VOICE_MAIL);

        rowObject.put("simParams", simParams);
        //-------------------------

        JSONArray configApps = new JSONArray();
        JSONObject appConfigParam = new JSONObject();

        //CONFIGURACION DE LAS APLICACIONES
        //region **********PDV*****************
        if (ContentProviders.ProviderIsAvailable(_context, PDVConfig.CONTENT_PROVIDER.getAuthority())) {

            Cursor cursor = _context.getContentResolver().query(PDVConfig.CONTENT_PROVIDER, PDVConfig.PROJECTION, null, null, null);

            appConfigParam = new JSONObject();

            appConfigParam.put("appId", "PDV");

            if (cursor.moveToFirst()) {

                JSONArray rowConfig = new JSONArray();
                JSONObject rowConfigParam = new JSONObject();

                int i = cursor.getColumnIndex(PDVConfig.PDV_PIE_TICKET);
                rowConfigParam.put("valId", PDVConfig.PDV_PIE_TICKET);
                rowConfigParam.put("valValue", i != -1 ? cursor.getString(i) : "GRACIAS POR SU VISITA");

                rowConfig.put(rowConfigParam);
                //
                rowConfigParam = new JSONObject();
                i = cursor.getColumnIndex(PDVConfig.PDV_MEDIO_DEFAULT);
                rowConfigParam.put("valId", PDVConfig.PDV_MEDIO_DEFAULT);
                rowConfigParam.put("valValue", i != -1 ? cursor.getString(i) : "");

                rowConfig.put(rowConfigParam);
                //
                rowConfigParam = new JSONObject();
                i = cursor.getColumnIndex(PDVConfig.PDV_AJUSTE_SENCILLO);
                rowConfigParam.put("valId", PDVConfig.PDV_AJUSTE_SENCILLO);
                rowConfigParam.put("valValue", i != -1 ? Boolean.parseBoolean(cursor.getString(i)) : false);

                rowConfig.put(rowConfigParam);
                //
                rowConfigParam = new JSONObject();
                i = cursor.getColumnIndex(PDVConfig.PDV_IMPRIMIR_AUTOMATICO);
                rowConfigParam.put("valId", PDVConfig.PDV_IMPRIMIR_AUTOMATICO);
                rowConfigParam.put("valValue", i != -1 ? Boolean.parseBoolean(cursor.getString(i)) : true);

                rowConfig.put(rowConfigParam);
                //
                rowConfigParam = new JSONObject();
                i = cursor.getColumnIndex(PDVConfig.PDV_SOLICITAR_CHECKOUT);
                rowConfigParam.put("valId", PDVConfig.PDV_SOLICITAR_CHECKOUT);
                rowConfigParam.put("valValue", i != -1 ? Boolean.parseBoolean(cursor.getString(i)) : true);

                rowConfig.put(rowConfigParam);
                //
                rowConfigParam = new JSONObject();
                i = cursor.getColumnIndex(PDVConfig.PDV_MOSTRAR_DESGLOSE_IMPUESTOS);
                rowConfigParam.put("valId", PDVConfig.PDV_MOSTRAR_DESGLOSE_IMPUESTOS);
                rowConfigParam.put("valValue", i != -1 ? Boolean.parseBoolean(cursor.getString(i)) : true);

                rowConfig.put(rowConfigParam);
                //
                rowConfigParam = new JSONObject();
                i = cursor.getColumnIndex(PDVConfig.PDV_TIEMPO_CONFIRMACION);
                rowConfigParam.put("valId", PDVConfig.PDV_TIEMPO_CONFIRMACION);
                rowConfigParam.put("valValue", i != -1 ? cursor.getString(i) : "0");

                rowConfig.put(rowConfigParam);
                //
                rowConfigParam = new JSONObject();
                i = cursor.getColumnIndex(PDVConfig.PDV_MOSTRAR_PRODUCTOS_SIN_PRECIO);
                rowConfigParam.put("valId", PDVConfig.PDV_MOSTRAR_PRODUCTOS_SIN_PRECIO);
                rowConfigParam.put("valValue", i != -1 ? Boolean.parseBoolean(cursor.getString(i)) : true);

                rowConfig.put(rowConfigParam);
                //
                /*rowConfigParam = new JSONObject();
                i = cursor.getColumnIndex(PDVConfig.PDV_EMITIR_VALE);
                rowConfigParam.put("valId", PDVConfig.PDV_EMITIR_VALE);
                rowConfigParam.put("valValue", i != -1 ? Boolean.parseBoolean(cursor.getString(i)) : false);

                rowConfig.put(rowConfigParam);*/
                //
                rowConfigParam = new JSONObject();
                i = cursor.getColumnIndex(PDVConfig.PDV_COMPROB_HABILIT);
                rowConfigParam.put("valId", PDVConfig.PDV_COMPROB_HABILIT);
                rowConfigParam.put("valValue", i != -1 ? Boolean.parseBoolean(cursor.getString(i)) : false);

                rowConfig.put(rowConfigParam);
                //
                rowConfigParam = new JSONObject();
                i = cursor.getColumnIndex(PDVConfig.PDV_ACTUALIZ_PRECIO_LISTA);
                rowConfigParam.put("valId", PDVConfig.PDV_ACTUALIZ_PRECIO_LISTA);
                rowConfigParam.put("valValue", i != -1 ? Boolean.parseBoolean(cursor.getString(i)) : true);

                rowConfig.put(rowConfigParam);

                //
                rowConfigParam = new JSONObject();
                i = cursor.getColumnIndex(PDVConfig.PDV_IMPRESION_VALE_ERROR);
                rowConfigParam.put("valId", PDVConfig.PDV_IMPRESION_VALE_ERROR);
                rowConfigParam.put("valValue", i != -1 ? cursor.getInt(i) : 0);

                rowConfig.put(rowConfigParam);
                //...

                appConfigParam.put("values", rowConfig);

                configApps.put(appConfigParam);

            }
        }
        //endregion
        //*******FIN PDV********

        //region **********VALE*****************
        if (ContentProviders.ProviderIsAvailable(_context, ValeConfig.CONTENT_PROVIDER.getAuthority())) {
            appConfigParam = new JSONObject();

            appConfigParam.put("appId", "VALE");

            JSONArray rowConfig = new JSONArray();
            JSONObject rowConfigParam = new JSONObject();

            Cursor cursor = _context.getContentResolver().query(ValeConfig.CONTENT_PROVIDER, ValeConfig.PROJECTION, null, null, null);

            if (cursor.moveToFirst()) {
                //
                rowConfigParam = new JSONObject();
                int i = cursor.getColumnIndex(ValeConfig.VALE_ULT_CIERRE_Z);
                rowConfigParam.put("valId", ValeConfig.VALE_ULT_CIERRE_Z);
                rowConfigParam.put("valValue", i != -1 ? cursor.getString(i) : "");
                rowConfig.put(rowConfigParam);


                //
                rowConfigParam = new JSONObject();
                i = cursor.getColumnIndex(ValeConfig.VALE_ULT_TRACE);
                rowConfigParam.put("valId", ValeConfig.VALE_ULT_TRACE);
                rowConfigParam.put("valValue", i != -1 ? cursor.getString(i) : "");
                rowConfig.put(rowConfigParam);


                //
                rowConfigParam = new JSONObject();
                i = cursor.getColumnIndex(ValeConfig.VALE_CIERRE_Z_REQUIRED);
                rowConfigParam.put("valId", ValeConfig.VALE_CIERRE_Z_REQUIRED);
                rowConfigParam.put("valValue", i != -1 ? cursor.getInt(i) : 0);
                rowConfig.put(rowConfigParam);
            }

            //
            rowConfigParam = new JSONObject();
            rowConfigParam.put("valId", "TERM_ID");
            rowConfigParam.put("valValue", this._config.SII_TERMINAL_ID);
            rowConfig.put(rowConfigParam);

            //
            rowConfigParam = new JSONObject();
            rowConfigParam.put("valId", "ESTADO");
            rowConfigParam.put("valValue", this._config.SII_STATUS);
            rowConfig.put(rowConfigParam);

            //
            rowConfigParam = new JSONObject();
            rowConfigParam.put("valId", "MODALIDAD");
            rowConfigParam.put("valValue", this._config.SII_MODALIDAD);
            rowConfig.put(rowConfigParam);

            //
            rowConfigParam = new JSONObject();
            rowConfigParam.put("valId", "GIRO1");
            rowConfigParam.put("valValue", this._config.SII_GIRO1);
            rowConfig.put(rowConfigParam);

            //
            rowConfigParam = new JSONObject();
            rowConfigParam.put("valId", "GIRO2");
            rowConfigParam.put("valValue", this._config.SII_GIRO2);
            rowConfig.put(rowConfigParam);

            //
            rowConfigParam = new JSONObject();
            rowConfigParam.put("valId", "GIRO3");
            rowConfigParam.put("valValue", this._config.SII_GIRO3);
            rowConfig.put(rowConfigParam);

            //
            rowConfigParam = new JSONObject();
            rowConfigParam.put("valId", "RES_SII");
            rowConfigParam.put("valValue", this._config.SII_RESOLUCION_VALE);
            rowConfig.put(rowConfigParam);

            //
            rowConfigParam = new JSONObject();
            rowConfigParam.put("valId", "SERV_IP");
            rowConfigParam.put("valValue", this._config.SII_SERVIDOR_IP);
            rowConfig.put(rowConfigParam);

            //
            rowConfigParam = new JSONObject();
            rowConfigParam.put("valId", "SERV_PORT");
            rowConfigParam.put("valValue", this._config.SII_SERVIDOR_PORT);
            rowConfig.put(rowConfigParam);


            //...
            appConfigParam.put("values", rowConfig);

            configApps.put(appConfigParam);
        }
        //endregion
        //*******FIN VALE********

        //region **********EP*****************
        appConfigParam = new JSONObject();

        appConfigParam.put("appId", "ENTRY POINT");

        JSONArray rowConfig = new JSONArray();
        JSONObject rowConfigParam = new JSONObject();
        rowConfigParam.put("valId", "PAIS");
        rowConfigParam.put("valValue", this._config.PAIS);

        rowConfig.put(rowConfigParam);
        //
        rowConfigParam = new JSONObject();
        rowConfigParam.put("valId", "PRINTER");

        String[] printerModelosValues = _context.getResources().getStringArray(R.array.printerModelsValues);
        String[] printerModelos = _context.getResources().getStringArray(R.array.printerModels);
        for (int i = 0; i < printerModelosValues.length; i++) {
            if (printerModelosValues[i].equals(this._config.PRINTER_MODEL)) {
                rowConfigParam.put("valValue", printerModelos[i]);
                break;
            }
        }

        rowConfig.put(rowConfigParam);
        //
        rowConfigParam = new JSONObject();
        rowConfigParam.put("valId", "TIEMPO_INACTIVIDAD");
        rowConfigParam.put("valValue", this._config.INACTIVITY_TIME + " mins");

        rowConfig.put(rowConfigParam);
        //
        rowConfigParam = new JSONObject();
        rowConfigParam.put("valId", "HIST.DATOS:");
        rowConfigParam.put("valValue", this._config.HIST_DAYS + " dias");

        rowConfig.put(rowConfigParam);
        //
        rowConfigParam = new JSONObject();
        rowConfigParam.put("valId", "ULTIMO BORRADO");
        rowConfigParam.put("valValue", _context.getSharedPreferences("com.pds.ficle.ep", Context.MODE_PRIVATE).getString("last_clear_time", "Nunca"));

        rowConfig.put(rowConfigParam);
        //
        rowConfigParam = new JSONObject();
        rowConfigParam.put("valId", "INTERV.SINC.");
        rowConfigParam.put("valValue", this._config.SYNC_TIME + " mins");

        rowConfig.put(rowConfigParam);
        //
        rowConfigParam = new JSONObject();
        rowConfigParam.put("valId", "ULTIMA SINCR.");
        rowConfigParam.put("valValue", _context.getSharedPreferences("com.pds.ficle.ep", Context.MODE_PRIVATE).getString("last_sync_time", "Nunca"));

        rowConfig.put(rowConfigParam);
        //
        rowConfigParam = new JSONObject();
        rowConfigParam.put("valId", "SYNC CAJA");
        rowConfigParam.put("valValue", this._config.SYNC_CAJA);

        rowConfig.put(rowConfigParam);
        //
        rowConfigParam = new JSONObject();
        rowConfigParam.put("valId", "SYNC LOGS");
        rowConfigParam.put("valValue", this._config.SYNC_LOGS);

        rowConfig.put(rowConfigParam);
        //
        rowConfigParam = new JSONObject();
        rowConfigParam.put("valId", "SYNC STOCK");
        rowConfigParam.put("valValue", this._config.SYNC_STOCK);

        rowConfig.put(rowConfigParam);
        //
        rowConfigParam = new JSONObject();
        rowConfigParam.put("valId", "CAT VERS");
        rowConfigParam.put("valValue", _context.getSharedPreferences("com.pds.ficle.ep", Context.MODE_PRIVATE).getInt("version_catalog", 1));

        rowConfig.put(rowConfigParam);
        //...
        appConfigParam.put("values", rowConfig);

        configApps.put(appConfigParam);

        //endregion
        //*******FIN EP********

        rowObject.put("apps_c", configApps);


        //--------VERSIONES DE LAS APLICACIONES---------
        JSONArray rowApps = new JSONArray();
        PackageManager pm = _context.getPackageManager();
        for (ApplicationInfo app : pm.getInstalledApplications(0)) {
            JSONObject rowAppVers = new JSONObject();

            if (app.packageName.startsWith("com.pds.")) {

                rowAppVers = new JSONObject();

                rowAppVers.put("appId", app.packageName);
                rowAppVers.put("appVersion", pm.getPackageInfo(app.packageName, 0).versionName != null ? pm.getPackageInfo(app.packageName, 0).versionName : "");

                rowApps.put(rowAppVers);
            }
        }

        rowObject.put("apps_v", rowApps);
        //-----------FIN VERSIONES APPS-----------------

        resultSet.put(rowObject);

        return resultSet;
    }

    private JSONArray LogsToJSON() throws Exception {

        Cursor logs_cursor = getCursor(
                "content://com.pds.ficle.ep.logs.contentprovider/logs_sync",
                LogHelper.columnas,
                LogHelper.LOG_SYNC_STATUS + "= 'N' ",
                "");
        //LogHelper.LOG_ID + " DESC");

        return CursorToJSON(logs_cursor);
    }

    private JSONArray VentasToJSON() throws Exception {

        Cursor ventas_cursor = getCursor(
                "content://com.pds.ficle.ep.ventas.contentprovider/ventas",
                VentasHelper.columnas,
                VentasHelper.VENTA_SYNC_STATUS + "= 'N' ",
                "");
        //VentasHelper.VENTA_ID + " DESC");

        return CursorToJSON(ventas_cursor);
    }

    private JSONArray VentasDetalleToJSON() throws Exception {

        Cursor cursor = getCursor(
                "content://com.pds.ficle.ep.venta_detalles.contentprovider/venta_detalles",
                VentaDetalleHelper.columnas,
                VentaDetalleHelper.COLUMN_SYNC_STATUS + "= 'N' ",
                "");

        return CursorToJSON(cursor);
    }

    private JSONArray ComprobantesToJSON() throws Exception {

        Cursor comprobantes_cursor = getCursor(
                "content://com.pds.ficle.ep.provider/comprobantes",
                ComprobanteTable.columnas,
                ComprobanteTable.COMPROBANTE_SYNC_STATUS + "= 'N' ",
                "");
        //ComprobanteTable.COMPROBANTE_ID + " DESC");

        return CursorToJSON(comprobantes_cursor);
    }

    private JSONArray ComprobantesDetalleToJSON() throws Exception {

        Cursor comprobantes_cursor = getCursor(
                "content://com.pds.ficle.ep.provider/comprobante_det",
                ComprobanteDetalleTable.columnas,
                ComprobanteDetalleTable.COLUMN_SYNC_STATUS + "= 'N' ",
                "");

        return CursorToJSON(comprobantes_cursor);
    }

    private JSONArray ComprasToJSON() throws Exception {

        Cursor cursor = getCursor(
                "content://com.pds.ficle.ep.provider/compras",
                CompraTable.columnas,
                CompraTable.COMPRA_SYNC_STATUS + "= 'N' ",
                "");

        return CursorToJSON(cursor);
    }

    private JSONArray ComprasDetalleToJSON() throws Exception {

        Cursor cursor = getCursor(
                "content://com.pds.ficle.ep.provider/compra_det",
                CompraDetalleTable.columnas,
                CompraDetalleTable.COLUMN_SYNC_STATUS + "= 'N' ",
                "");


        return CursorToJSON(cursor);
    }

    private JSONArray PedidosToJSON() throws Exception {

        Cursor cursor = getCursor(
                "content://com.pds.ficle.ep.provider/pedido",
                PedidoTable.columnas,
                PedidoTable.PEDIDO_SYNC_STATUS + "= 'N' ",
                "");

        return CursorToJSON(cursor);
    }

    private JSONArray PedidosDetalleToJSON() throws Exception {

        Cursor cursor = getCursor(
                "content://com.pds.ficle.ep.provider/pedido_det",
                PedidoDetalleTable.columnas,
                PedidoDetalleTable.COLUMN_SYNC_STATUS + "= 'N' ",
                "");

        return CursorToJSON(cursor);
    }


    private JSONArray CombosToJSON() throws Exception {

        Cursor cursor = getCursor(
                "content://com.pds.ficle.ep.provider/combos",
                ComboTable.COLUMNAS,
                ComboTable.COLUMN_SYNC_STATUS + "= 'N' ",
                "");

        return CursorToJSON(cursor);
    }

    private JSONArray CombosItemsToJSON() throws Exception {

        Cursor cursor = getCursor(
                "content://com.pds.ficle.ep.provider/combos_item",
                ComboItemTable.COLUMNAS,
                ComboItemTable.COLUMN_SYNC_STATUS + "= 'N' ",
                "");

        return CursorToJSON(cursor);
    }

    private JSONArray CuentaCteToJSON() throws Exception {

        Cursor cursor = getCursor(
                "content://com.pds.ficle.ep.provider/ctacte",
                CuentaTable.COLUMNAS,
                CuentaTable.COLUMN_SYNC_STATUS + "= 'N' ",
                "");

        return CursorToJSON(cursor);
    }

    private JSONArray UsuariosToJSON() throws Exception {

        Cursor users_cursor = getCursor(
                "content://com.pds.ficle.ep.usuarios.contentprovider/usuarios",
                UsuarioHelper.columnas,
                UsuarioHelper.USUARIOS_SYNC_STATUS + "= 'N' ",
                "");

        return CursorToJSON(users_cursor);
    }

   /* private JSONArray CajaToJSON() throws Exception {

        Cursor caja_cursor = getCursor(
                "content://com.pds.ficle.ep.caja.contentprovider/caja",
                CajaHelper.columnas,
                CajaHelper.CAJA_SYNC_STATUS + "= 'N' ",
                "");
        //CajaHelper.CAJA_ID + " DESC");

        return CursorToJSON(caja_cursor);
    }*/

    private JSONArray ProductosToJSON() throws Exception {

        Cursor prods_cursor = getCursor(
                "content://com.pds.ficle.ep.provider/products",
                ProductDao.PROJECTION_ATTRIBUTES,
                ProductTable.COLUMN_SYNC_STATUS + "= 'N' ",
                "");
        //ProductTable.COLUMN_ID + " DESC");

        return CursorToJSON(prods_cursor);
    }

    private JSONArray ProveedoresToJSON() throws Exception {

        Cursor provs_cursor = getCursor(
                "content://com.pds.ficle.ep.provider/providers",
                ProviderDao.PROJECTION_ATTRIBUTES,
                ProviderTable.COLUMN_SYNC_STATUS + "= 'N' ",
                "");
        //ProviderTable.COLUMN_ID + " DESC");

        return CursorToJSON(provs_cursor);
    }

    private JSONArray DepartamentosToJSON() throws Exception {

        Cursor depts_cursor = getCursor(
                "content://com.pds.ficle.ep.provider/departments",
                DepartmentDao.PROJECTION_ATTRIBUTES,
                DepartmentTable.COLUMN_SYNC_STATUS + "= 'N' ",
                "");
        //DepartmentTable.COLUMN_ID + " DESC");

        return CursorToJSON(depts_cursor);
    }

    private JSONArray SubdepartamentosToJSON() throws Exception {

        Cursor subdepts_cursor = getCursor(
                "content://com.pds.ficle.ep.provider/subdepartments",
                SubDepartmentDao.PROJECTION_ATTRIBUTES,
                SubdepartmentTable.COLUMN_SYNC_STATUS + "= 'N' ",
                "");
        //SubdepartmentTable.COLUMN_ID + " DESC");

        return CursorToJSON(subdepts_cursor);
    }

    private JSONArray ClientesToJSON() throws Exception {

        Cursor clientes_cursor = getCursor(
                "content://com.pds.ficle.ep.provider/clientes",
                ClienteDao.PROJECTION_ATTRIBUTES,
                "",//ClienteTable.COLUMN_SYNC_STATUS + "= 'N' ",
                "");

        return CursorToJSON(clientes_cursor);
    }

    private Cursor getCursor(String uri, String[] columnas, String filter, String orderby) {
        String limit = " LIMIT " + String.valueOf(MAX_SEND);

        orderby = TextUtils.isEmpty(orderby) ? "1" : orderby;

        Cursor cursor = _context.getContentResolver().query(
                Uri.parse(uri),
                columnas,
                filter,
                null,
                orderby + limit);

        return cursor;
    }

    //endregion

    private int MAX_SEND = 1000;//cantidad maxima de registros a enviar por envío (performance)

    private JSONArray CursorToJSON(Cursor cursor) throws Exception {

        JSONArray resultSet = new JSONArray();

        int x = 0; //contador de registros

        // recorremos los items
        //boolean start = false;

        /*if (CONTINUE) {
            start = cursor.moveToPosition(MAX_SEND);
        } else {
            start = cursor.moveToFirst();
        }*/


        if (cursor.moveToFirst()) {
            do {

                int totalColumn = cursor.getColumnCount();
                JSONObject rowObject = new JSONObject();

                for (int i = 0; i < totalColumn; i++) {
                    if (cursor.getColumnName(i) != null) {
                        try {

                            switch (cursor.getType(i)) {
                                case Cursor.FIELD_TYPE_FLOAT:
                                    rowObject.put(cursor.getColumnName(i), cursor.getDouble(i));
                                    break;
                                default:
                                    if (cursor.getString(i) != null) {
                                        rowObject.put(cursor.getColumnName(i), cursor.getString(i).replace("\"", "").replace("\\", "/"));
                                    } else {
                                        rowObject.put(cursor.getColumnName(i), "");
                                    }
                                    break;
                            }

                        } catch (Exception ex) {
                            throw ex;
                        }
                    }
                }

                //agregamos en todas las filas el identificador del dispositivo
                rowObject.put("deviceId", this._config.ANDROID_ID);

                resultSet.put(rowObject);

                x++;

                //} while (cursor.moveToNext());
            } while (cursor.moveToNext() && x < MAX_SEND);

            //CONTINUE = cursor.moveToNext();
            CONTINUE = (x == MAX_SEND);//si cortamos, entonces vamos a seguir...

        } else {

            CONTINUE = false;

            return null;
        }

        cursor.close();

        return resultSet;
    }

    final int CONNECTION_TIMEOUT = 7000;//the timeout until a connection is established
    final int SOCKET_TIMEOUT = 30000; //is the timeout for waiting for data

    private SyncDataTaskResponse postDataToServer(JSONArray jsonArray, int tipoDatoEnviado) throws Exception {

        // 1. create HttpClient
        HttpClient httpclient = new DefaultHttpClient();
        HttpParams params = httpclient.getParams();
        HttpConnectionParams.setConnectionTimeout(params, CONNECTION_TIMEOUT);
        HttpConnectionParams.setSoTimeout(params, SOCKET_TIMEOUT);

        // 2. make POST request to the given URL
        String servidor = this._config.CLOUD_SERVER_HOST.trim();

        HttpPost httpPost;
        switch (tipoDatoEnviado) {
            case TYPE_LOG: {
                httpPost = new HttpPost(servidor + "api/SyncLogs");
            }
            break;
            case TYPE_VENTA: {
                httpPost = new HttpPost(servidor + "api/SyncSales");
            }
            break;
            case TYPE_USER: {
                httpPost = new HttpPost(servidor + "api/SyncUsers");
            }
            break;
            case TYPE_CONFIG: {
                httpPost = new HttpPost(servidor + "api/SyncConfig");
            }
            break;
            case TYPE_COMPROBANTE: {
                httpPost = new HttpPost(servidor + "api/SaveComprobantes");
            }
            break;
            case TYPE_CAJA: {
                httpPost = new HttpPost(servidor + "api/SaveCaja");
            }
            break;
            case TYPE_PRODUCTOS: {
                httpPost = new HttpPost(servidor + "api/SaveProductos");
            }
            break;
            case TYPE_PROVEEDORES: {
                httpPost = new HttpPost(servidor + "api/SyncProviders");
            }
            break;
            case TYPE_DEPARTAMENTOS: {
                httpPost = new HttpPost(servidor + "api/SaveDepartments");
            }
            break;
            case TYPE_SUBDEPARTAMENTOS: {
                httpPost = new HttpPost(servidor + "api/SaveSubdepartments");
            }
            break;
            case TYPE_COMPRA: {
                httpPost = new HttpPost(servidor + "api/SyncBuys");
            }
            break;
              /*
            case TYPE_DET_COMPRA: {
                httpPost = new HttpPost(servidor + "api/SaveDetCompras");
            }
            break;*/
            case TYPE_DET_COMPROBANTE: {
                httpPost = new HttpPost(servidor + "api/SaveDetComprobantes");
            }
            break;
            /*case TYPE_DET_VENTA: {
                httpPost = new HttpPost(servidor + "api/SaveDetVentas");
            }
            break;*/
          /*  case TYPE_PEDIDO: {
                httpPost = new HttpPost(servidor + "api/SavePedidos");
            }
            break;
            case TYPE_DET_PEDIDO: {
                httpPost = new HttpPost(servidor + "api/SaveDetPedidos");
            }
            break;*/
            case TYPE_CLIENTE: {
                httpPost = new HttpPost(servidor + "api/SyncCustomers");
            }
            break;
        /*    case TYPE_COMBO: {
                httpPost = new HttpPost(servidor + "api/SaveCombos");
            }
            break;
            case TYPE_COMBO_ITEM: {
                httpPost = new HttpPost(servidor + "api/SaveCombosItems");
            }
            break;*/
           /* case TYPE_CUENTA_CTE: {
                httpPost = new HttpPost(servidor + "api/SaveCuentaCte");
            }
            break;*/
            default: {
                throw new Exception("Tipo de envío no válido");
            }
        }

        // 4. convert JSONObject to JSON to String
        //String json = "{j:'" + jsonArray.get(0).toString() + "'}";

        String json = "{\"IMEI\":\"" + mImei + "\",\"androidID\":\"" + mAndroidId + "\", \"syncInfo\":" + jsonArray.toString().replace("'", "") + "}";

        // 5. set json to StringEntity
        StringEntity se = new StringEntity(json, "UTF-8");

        // 6. set httpPost Entity
        httpPost.setEntity(se);

        // 7. Set some headers to inform server about the type of the content
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");

        // 8. Execute POST request to the given URL
        HttpResponse httpResponse = httpclient.execute(httpPost);//TODO: acá tengo un bug por socket timeout...

        // 9. receive response as inputStream
        InputStream inputStream = null;
        inputStream = httpResponse.getEntity().getContent();

        SyncDataTaskResponse p = new SyncDataTaskResponse(-1, "No ha sido posible el envio al servidor");
        if (inputStream != null) {
            p = new Gson().fromJson(new InputStreamReader(inputStream, "UTF-8"), SyncDataTaskResponse.class);
            inputStream.close();
        }


        return p;
    }

    private String generarListaSKUs(List<Product> prods) {
        String lista = "";

        if (prods.size() > 0) {
            for (Product prd : prods) {
                lista = lista + prd.getCode() + ",";
            }
            //Quito la  última coma
            lista = lista.substring(0, lista.length() - 1);
        }

        return lista;
    }

    private void ReadJsonResponse(int tipoDatoRecibido, InputStream in) throws Exception {

        String jsonString = "";

        //convertimos la respuesta a string
        BufferedReader br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
        StringBuffer sb = new StringBuffer(jsonString);
        while ((jsonString = br.readLine()) != null) {
            sb.append(jsonString);
        }
        jsonString = sb.toString();

        //excluyo el d para el obtener el contenido
        JSONObject d = new JSONObject(jsonString);

        if (d.getString("d").startsWith("[")) {
            //obtenemos el array json
            JSONArray json = new JSONArray(d.getString("d"));

            GetEntity(tipoDatoRecibido, json);
        } else if (d.getString("d").startsWith("{")) {
            //obtenemos el objeto json
            JSONObject json = new JSONObject(d.getString("d"));

            GetEntity(tipoDatoRecibido, json);
        } else {
            throw new Exception("ReadJsonResponse:unknown response " + d.getString("d"));
        }
    }

    private void GetEntity(int entityType, JSONObject json) throws Exception {
        if (json.length() > 0) {
            switch (entityType) {
                case TYPE_CAT_VERS:
//                    {
//                    //process response to update products
//                    ProductDao dao = new ProductDao(_context.getContentResolver());
//                    Tax _gravado = new TaxDao(_context.getContentResolver()).list("name = 'Gravado'", null, null).get(0);
//                    Tax _exento = new TaxDao(_context.getContentResolver()).list("name = 'Exento'", null, null).get(0);//se usa para cigarrillos
//                    SubDepartmentDao subDepartmentDao = new SubDepartmentDao(_context.getContentResolver());
//
//                    JSONArray productos = json.getJSONArray("productos");
//                    int versActual = json.getInt("idcatactual");
//
//                    //recorremos los objetos del array json
//                    boolean error = false;
//
//                    Set<String> altas = new HashSet<String>();
//
//                    for (int j = 0; j < productos.length(); j++) {
//                        try {
//                            long idProd = ProcesaProducto(productos.getJSONObject(j), dao, _gravado, _exento, subDepartmentDao);
//
//                            if (idProd > 0) {
//                                //guardamos el id para usarlo luego en el wizard
//                                altas.add(String.valueOf(idProd));
//                            }
//
//                        } catch (Exception ex) {
//                            //registramos y seguimos
//                            error = true;
//                            Logger.RegistrarEvento(_context.getContentResolver(), "e", "SYNC CATALOG", "Error al obtener datos del producto", "");
//                        }
//                    }
//
//                    //edit.putStringSet("prods_creados", altas);
//
//                    if (!error) {
//                        //actualizamos la version actual del catalogo
//                        SharedPreferences pref = _context.getSharedPreferences("com.pds.ficle.ep", Context.MODE_PRIVATE);
//                        SharedPreferences.Editor edit = pref.edit();
//
//                        edit.putInt("version_catalog", versActual);
//
//                        edit.apply();
//                    }
//
//                }
                    break;
                default:
                    throw new Exception("ProcessEntity:unknown entity's type to process");
            }
        }
    }

    private void GetEntity(int entityType, JSONArray json) throws Exception {
        if (json.length() > 0) {
            switch (entityType) {
                case TYPE_TIPO_DOC: {
                    SQLiteDatabase _db = new DbHelper(_context).getWritableDatabase();

                    _db.beginTransaction();
                    try {
                        //limpiamos la tabla de TIPO DOC
                        TipoDocTable.deleteTable(_db);

                        TipoDocDao dao = new TipoDocDao(_context.getContentResolver());

                        //recorremos los objetos del array json
                        for (int i = 0; i < json.length(); i++) {
                            //TODO: y si ejecutamos dentro de una transaccion? para no eliminar y quedarnos sin nada en caso de error
                            JSONObject object = json.getJSONObject(i);

                            TipoDoc instance = new TipoDoc(object);

                            dao.save(instance);
                        }

                        _db.setTransactionSuccessful();
                    } catch (Exception ex) {

                    } finally {
                        _db.endTransaction();
                    }

                }
                break;
                case TYPE_RANGOS: {

                    RangoFolioDao dao = new RangoFolioDao(_context.getContentResolver());

                    //recorremos los objetos del array json
                    for (int i = 0; i < json.length(); i++) {
                        JSONObject object = json.getJSONObject(i);

                        RangoFolio instance = new RangoFolio(object);

                        if (!ExisteRangoFolio(instance, dao)) {
                            dao.save(instance);
                        }
                    }
                }
                break;
                case TYPE_VENTA: {

                    VentaDao dao = new VentaDao(_context.getContentResolver());

                    //recorremos los objetos del array json
                    for (int i = 0; i < json.length(); i++) {
                        JSONObject object = json.getJSONObject(i);

                        if (!object.isNull("ventaFcId")) {
                            Venta instance = dao.find(object.getInt("ventaIdOriginal"));
                            if (object.getInt("ventaFcId") != -1) { //venta OK
                                instance.setFc_id(object.getInt("ventaFcId"));

                                if (!object.isNull("ventaFcValidation"))//control para no grabar un null
                                    instance.setFc_validation(object.getString("ventaFcValidation"));
                            } else if (!object.isNull("ventaFcValidation")) {//venta no OK
                                instance.setFc_validation(object.getString("ventaFcValidation"));
                            }

                            dao.saveOrUpdate(instance);
                        }

                    }
                }
                break;
                case TYPE_PRODS_CAT:
//                    {
//                    //process response to update products
//                    ProductDao dao = new ProductDao(_context.getContentResolver());
//                    Tax _gravado = new TaxDao(_context.getContentResolver()).list("name = 'Gravado'", null, null).get(0);
//                    Tax _exento = new TaxDao(_context.getContentResolver()).list("name = 'Exento'", null, null).get(0);//se usa para cigarrillos
//                    SubDepartmentDao subDepartmentDao = new SubDepartmentDao(_context.getContentResolver());
//
//                    //recorremos los objetos del array json
//                    for (int j = 0; j < json.length(); j++) {
//                        try {
//                            ProcesaProducto(json.getJSONObject(j), dao, _gravado, _exento, subDepartmentDao);
//                        } catch (Exception ex) {
//                            //registramos y seguimos
//                            Logger.RegistrarEvento(_context.getContentResolver(), "e", "SYNC CATALOG", "Error al obtener datos del producto", "");
//                        }
//                    }
//
//                }
                    break;
                default:
                    throw new Exception("ProcessEntity:unknown entity's type to process");
            }
        }

    }


    private boolean ExisteRangoFolio(RangoFolio rf, RangoFolioDao rfDao) {
        List<RangoFolio> list = rfDao.list("tipo_doc = '" + rf.getTipo_doc() + "' and desde = " + rf.getDesde() + " and hasta = " + rf.getHasta(), null, null);

        return list.size() > 0;
    }

    private void UpdateSyncState(JSONArray jsonArray, int tipoDatoEnviado) throws Exception {
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = (JSONObject) jsonArray.get(i);

            ContentValues cv = new ContentValues();

            switch (tipoDatoEnviado) {
                case TYPE_LOG: {
                    cv.put(LogHelper.LOG_ID, jsonObject.getInt(LogHelper.LOG_ID));
                    cv.put(LogHelper.LOG_SYNC_STATUS, "S");

                    Uri uri = ContentUris.withAppendedId(Uri.parse("content://com.pds.ficle.ep.logs.contentprovider/logs"), jsonObject.getInt(LogHelper.LOG_ID));

                    int result = _context.getContentResolver().update(uri, cv, null, null);
                    Log.i(TAG, "UpdateSyncState: TYPE_LOG  #" + jsonObject.getInt(LogHelper.LOG_ID) + ": " + result);
                }
                break;
                case TYPE_VENTA: {
                    cv.put(VentasHelper.VENTA_ID, jsonObject.getInt(VentasHelper.VENTA_ID));
                    cv.put(VentasHelper.VENTA_SYNC_STATUS, "S");

                    Uri uri = ContentUris.withAppendedId(Uri.parse("content://com.pds.ficle.ep.ventas.contentprovider/ventas"), jsonObject.getInt(VentasHelper.VENTA_ID));

                    _context.getContentResolver().update(uri, cv, null, null);
                }
                break;
                case TYPE_USER: {
                    cv.put(UsuarioHelper.USUARIOS_ID, jsonObject.getInt(UsuarioHelper.USUARIOS_ID));
                    cv.put(UsuarioHelper.USUARIOS_SYNC_STATUS, "S");

                    Uri uri = ContentUris.withAppendedId(Uri.parse("content://com.pds.ficle.ep.usuarios.contentprovider/usuarios"), jsonObject.getInt(UsuarioHelper.USUARIOS_ID));

                    _context.getContentResolver().update(uri, cv, null, null);
                }
                break;
                case TYPE_COMPROBANTE: {
                    cv.put(ComprobanteTable.COMPROBANTE_ID, jsonObject.getInt(ComprobanteTable.COMPROBANTE_ID));
                    cv.put(ComprobanteTable.COMPROBANTE_SYNC_STATUS, "S");

                    Uri uri = ContentUris.withAppendedId(Uri.parse("content://com.pds.ficle.ep.provider/comprobantes"), jsonObject.getInt(ComprobanteTable.COMPROBANTE_ID));

                    _context.getContentResolver().update(uri, cv, null, null);
                }
                break;
                case TYPE_CAJA: {
                    cv.put(CajaHelper.CAJA_ID, jsonObject.getInt(CajaHelper.CAJA_ID));
                    cv.put(CajaHelper.CAJA_SYNC_STATUS, "S");

                    Uri uri = ContentUris.withAppendedId(Uri.parse("content://com.pds.ficle.ep.caja.contentprovider/caja"), jsonObject.getInt(CajaHelper.CAJA_ID));

                    _context.getContentResolver().update(uri, cv, null, null);
                }
                break;
                case TYPE_PRODUCTOS: {
                    cv.put(ProductTable.COLUMN_ID, jsonObject.getInt(ProductTable.COLUMN_ID));
                    cv.put(ProductTable.COLUMN_SYNC_STATUS, "S");

                    Uri uri = ContentUris.withAppendedId(Uri.parse("content://com.pds.ficle.ep.provider/products"), jsonObject.getInt(ProductTable.COLUMN_ID));

                    _context.getContentResolver().update(uri, cv, null, null);
                }
                break;
                case TYPE_PROVEEDORES: {
                    cv.put(ProviderTable.COLUMN_ID, jsonObject.getInt(ProviderTable.COLUMN_ID));
                    cv.put(ProviderTable.COLUMN_SYNC_STATUS, "S");

                    Uri uri = ContentUris.withAppendedId(Uri.parse("content://com.pds.ficle.ep.provider/providers"), jsonObject.getInt(ProviderTable.COLUMN_ID));

                    _context.getContentResolver().update(uri, cv, null, null);
                }
                break;
                case TYPE_DEPARTAMENTOS: {
                    cv.put(DepartmentTable.COLUMN_ID, jsonObject.getInt(DepartmentTable.COLUMN_ID));
                    cv.put(DepartmentTable.COLUMN_SYNC_STATUS, "S");

                    Uri uri = ContentUris.withAppendedId(Uri.parse("content://com.pds.ficle.ep.provider/departments"), jsonObject.getInt(DepartmentTable.COLUMN_ID));

                    _context.getContentResolver().update(uri, cv, null, null);
                }
                break;
                case TYPE_SUBDEPARTAMENTOS: {
                    cv.put(SubdepartmentTable.COLUMN_ID, jsonObject.getInt(SubdepartmentTable.COLUMN_ID));
                    cv.put(SubdepartmentTable.COLUMN_SYNC_STATUS, "S");

                    Uri uri = ContentUris.withAppendedId(Uri.parse("content://com.pds.ficle.ep.provider/subdepartments"), jsonObject.getInt(SubdepartmentTable.COLUMN_ID));

                    _context.getContentResolver().update(uri, cv, null, null);
                }
                break;
               /* case TYPE_COMPRA: {
                    cv.put(CompraTable.COMPRA_ID, jsonObject.getInt(CompraTable.COMPRA_ID));
                    cv.put(CompraTable.COMPRA_SYNC_STATUS, "S");

                    Uri uri = ContentUris.withAppendedId(Uri.parse("content://com.pds.ficle.ep.provider/compras"), jsonObject.getInt(CompraTable.COMPRA_ID));

                    _context.getContentResolver().update(uri, cv, null, null);
                }
                break;
                case TYPE_DET_COMPRA: {
                    cv.put(CompraDetalleTable.COMPRA_DET_ID, jsonObject.getInt(CompraDetalleTable.COMPRA_DET_ID));
                    cv.put(CompraDetalleTable.COLUMN_SYNC_STATUS, "S");

                    Uri uri = ContentUris.withAppendedId(Uri.parse("content://com.pds.ficle.ep.provider/compra_det"), jsonObject.getInt(CompraDetalleTable.COMPRA_DET_ID));

                    _context.getContentResolver().update(uri, cv, null, null);
                }
                break;*/
                case TYPE_DET_COMPROBANTE: {
                    cv.put(ComprobanteDetalleTable.COMPROBANTE_DET_ID, jsonObject.getInt(ComprobanteDetalleTable.COMPROBANTE_DET_ID));
                    cv.put(ComprobanteDetalleTable.COLUMN_SYNC_STATUS, "S");

                    Uri uri = ContentUris.withAppendedId(Uri.parse("content://com.pds.ficle.ep.provider/comprobante_det"), jsonObject.getInt(ComprobanteDetalleTable.COMPROBANTE_DET_ID));

                    _context.getContentResolver().update(uri, cv, null, null);
                }
                break;
                case TYPE_DET_VENTA: {
                    cv.put(VentaDetalleHelper.VENTA_DET_ID, jsonObject.getInt(VentaDetalleHelper.VENTA_DET_ID));
                    cv.put(VentaDetalleHelper.COLUMN_SYNC_STATUS, "S");

                    Uri uri = ContentUris.withAppendedId(Uri.parse("content://com.pds.ficle.ep.venta_detalles.contentprovider/venta_detalles"), jsonObject.getInt(VentaDetalleHelper.VENTA_DET_ID));

                    _context.getContentResolver().update(uri, cv, null, null);
                }
                break;
                case TYPE_PEDIDO: {
                    cv.put(PedidoTable.PEDIDO_ID, jsonObject.getInt(PedidoTable.PEDIDO_ID));
                    cv.put(PedidoTable.PEDIDO_SYNC_STATUS, "S");

                    Uri uri = ContentUris.withAppendedId(Uri.parse("content://com.pds.ficle.ep.provider/pedido"), jsonObject.getInt(PedidoTable.PEDIDO_ID));

                    _context.getContentResolver().update(uri, cv, null, null);
                }
                break;
                case TYPE_DET_PEDIDO: {
                    cv.put(PedidoDetalleTable.PEDIDO_DET_ID, jsonObject.getInt(PedidoDetalleTable.PEDIDO_DET_ID));
                    cv.put(PedidoDetalleTable.COLUMN_SYNC_STATUS, "S");

                    Uri uri = ContentUris.withAppendedId(Uri.parse("content://com.pds.ficle.ep.provider/pedido_det"), jsonObject.getInt(PedidoDetalleTable.PEDIDO_DET_ID));

                    _context.getContentResolver().update(uri, cv, null, null);
                }
                break;
                case TYPE_CLIENTE: {
                    cv.put(ClienteTable.COLUMN_ID, jsonObject.getInt(ClienteTable.COLUMN_ID));
                    cv.put(ClienteTable.COLUMN_SYNC_STATUS, "S");

                    Uri uri = ContentUris.withAppendedId(Uri.parse("content://com.pds.ficle.ep.provider/clientes"), jsonObject.getInt(ClienteTable.COLUMN_ID));

                    _context.getContentResolver().update(uri, cv, null, null);
                }
                break;
                case TYPE_COMBO: {
                    cv.put(ComboTable.COLUMN_ID, jsonObject.getInt(ComboTable.COLUMN_ID));
                    cv.put(ComboTable.COLUMN_SYNC_STATUS, "S");

                    Uri uri = ContentUris.withAppendedId(Uri.parse("content://com.pds.ficle.ep.provider/combos"), jsonObject.getInt(ComboTable.COLUMN_ID));

                    _context.getContentResolver().update(uri, cv, null, null);
                }
                break;
                case TYPE_COMBO_ITEM: {
                    cv.put(ComboItemTable.COLUMN_ID, jsonObject.getInt(ComboItemTable.COLUMN_ID));
                    cv.put(ComboItemTable.COLUMN_SYNC_STATUS, "S");

                    Uri uri = ContentUris.withAppendedId(Uri.parse("content://com.pds.ficle.ep.provider/combos_item"), jsonObject.getInt(ComboItemTable.COLUMN_ID));

                    _context.getContentResolver().update(uri, cv, null, null);
                }
                break;
                case TYPE_CUENTA_CTE: {
                    cv.put(CuentaTable.COLUMN_ID, jsonObject.getInt(CuentaTable.COLUMN_ID));
                    cv.put(CuentaTable.COLUMN_SYNC_STATUS, "S");

                    Uri uri = ContentUris.withAppendedId(Uri.parse("content://com.pds.ficle.ep.provider/ctacte"), jsonObject.getInt(CuentaTable.COLUMN_ID));

                    _context.getContentResolver().update(uri, cv, null, null);
                }
                break;
                default: {
                    throw new Exception("Tipo de envio invalido");
                }
            }
        }
    }

    private void ProcessEntity(int entityType) {
        do {
            switch (entityType) {

                case TYPE_CAJA:
                    ProcessEntityHelper helper_caja = new CajaProcessEntityHelper(_context, _config);
                    helper_caja.processEntity();
                    CONTINUE = helper_caja.hasToContinue();
                    break;

                case TYPE_COMPRA:
                    ProcessEntityHelper helper_buys = new BuyProcessEntityHelper(_context, _config);
                    helper_buys.processEntity();
                    CONTINUE = helper_buys.hasToContinue();
                    break;


                case TYPE_COMPROBANTE: //Incluye TYPE_DET_COMPROBANTE
                    ProcessEntityHelper helper_cmp_detalle = new ComprobanteProcessEntityHelper(_context, _config);
                    helper_cmp_detalle.processEntity();
                    CONTINUE = helper_cmp_detalle.hasToContinue();
                    break;

                case TYPE_VENTA: //Incluye TYPE_DET_COMPROBANTE
                    ProcessEntityHelper helper_vta_detalle = new VentaProcessEntityHelper(_context, _config);
                    helper_vta_detalle.processEntity();
                    CONTINUE = helper_vta_detalle.hasToContinue();
                    break;

                default:
                    ProcessEntityRecursive(entityType);
                    break;
            }
        } while (CONTINUE);
    }

    private void ProcessEntityRecursive(int entityType) {
        try {
            String ok = "{\"d\":true}";

            JSONArray array = null;

            //1-obtenemos data pendiente de enviar
            switch (entityType) {
                case TYPE_CONFIG:
                    array = ConfigToJSON();
                    break;
                case TYPE_LOG:
                    array = LogsToJSON();
                    break;
              /*  case TYPE_COMPRA:
                    array = ComprasToJSON();
                    break;*/
                case TYPE_DET_COMPRA:
                    array = ComprasDetalleToJSON();
                    break;

                case TYPE_DEPARTAMENTOS:
                    array = DepartamentosToJSON();
                    break;
                case TYPE_SUBDEPARTAMENTOS:
                    array = SubdepartamentosToJSON();
                    break;
                case TYPE_PROVEEDORES:
                    array = ProveedoresToJSON();
                    break;
                case TYPE_PRODUCTOS:
                    array = ProductosToJSON();
                    break;
                case TYPE_PEDIDO:
                    array = PedidosToJSON();
                    break;
                case TYPE_DET_PEDIDO:
                    array = PedidosDetalleToJSON();
                    break;
                case TYPE_USER:
                    array = UsuariosToJSON();
                    break;
                case TYPE_CLIENTE:
                    array = ClientesToJSON();
                    break;
                case TYPE_COMBO:
                    array = CombosToJSON();
                    break;
                case TYPE_COMBO_ITEM:
                    array = CombosItemsToJSON();
                    break;
                case TYPE_CUENTA_CTE:
                    array = CuentaCteToJSON();
                    break;
                default:
                    throw new Exception("ProcessEntity:unknown entity's type to process");
            }

            //publishProgress("Enviando XXX al servidor...");

            //2-enviamos al servidor
            if (array != null) {
                SyncDataTaskResponse response = postDataToServer(array, entityType);

                if (response.getResponseCode() != 0)
                    throw new Exception(response.getResponseMessage());

                //3-marcamos como enviados
                if (entityType != TYPE_CONFIG)
                    UpdateSyncState(array, entityType);
            } else {
                CONTINUE = false;
            }
        } catch (Exception ex) {
            CONTINUE = false;
            _hubo_error = true;
            Log.e("SyncProcess", ex.getMessage() + "" + ex.toString(), ex);
            Logger.RegistrarEvento(_context, "e", "SyncProcess", ex.getMessage() + "" + ex.toString(), "Ent:" + String.valueOf(entityType));
        }
    }

    @Override
    protected String doInBackground(Void... params) {

        // params comes from the execute() call: params[0] is the message.
        try {

            if (!_activateSync) {//si no está habilitado el sync no lo hacemos
                return "!ok";
            }

            if (!ConnectivityUtils.isOnline(_context)) {
                return "No hay una conexion disponible";
            }

            if (_syncingOnExit)
                publishProgress("Cerrando aplicaciones...");
            else
                publishProgress("Iniciando aplicaciones...");

            //***GET TIPO DOC****
            //GetDataFromServer(TYPE_TIPO_DOC);

            //***GET RANGOS****
            //GetDataFromServer(TYPE_RANGOS);

            //***GET ESTADO VENTAS****
            //GetDataFromServer(TYPE_VENTA);

            //***GET PRODUCTS DATA FROM MASTER CATALOG***
            //GetDataFromServer(TYPE_PRODS_CAT);

            //********CONFIG***********
            ProcessEntity(TYPE_CONFIG);

            //********VENTAS*********
            ProcessEntity(TYPE_VENTA);

            //********DETALLE VENTAS*********
            /*ProcessEntity(TYPE_DET_VENTA);*/

            //********COMPROBANTES*********
            ProcessEntity(TYPE_COMPROBANTE);

            //********DETALLE COMPROBANTES*********
            //ProcessEntity(TYPE_DET_COMPROBANTE);

            //********CAJA*********
            if (_config.SYNC_CAJA)
                ProcessEntity(TYPE_CAJA);

            //*******USERS***********
            ProcessEntity(TYPE_USER);

            //********PROVEEDORES*********
            ProcessEntity(TYPE_PROVEEDORES);

            //********COMBOS*********
            //ProcessEntity(TYPE_COMBO);

            //********COMBOS ITEMS*********
            //ProcessEntity(TYPE_COMBO_ITEM);

            //********COMPRAS*********
            ProcessEntity(TYPE_COMPRA);

            //********DETALLE COMPRAS*********
            //ProcessEntity(TYPE_DET_COMPRA);

            //********PEDIDOS*********
            //ProcessEntity(TYPE_PEDIDO);

            //********DETALLE PEDIDOS*********
            //ProcessEntity(TYPE_DET_PEDIDO);

            //********CLIENTES*********
            ProcessEntity(TYPE_CLIENTE);

            //********CUENTA CORRIENTE***
            //ProcessEntity(TYPE_CUENTA_CTE);

            //********LOGS***********
            if (_config.SYNC_LOGS)
                ProcessEntity(TYPE_LOG);

            //*****UPGRADE CATALOG*****
//            GetDataFromServer(TYPE_CAT_VERS);


            //--------------------------------------------------
            //return response.equals(ok) ? "ok" : response;
            return !_hubo_error ? "ok" : "mal";


        } catch (Exception e) {
            return "Error: " + e.toString();
        }
    }


}
