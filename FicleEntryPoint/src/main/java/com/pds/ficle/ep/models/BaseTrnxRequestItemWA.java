package com.pds.ficle.ep.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class BaseTrnxRequestItemWA {


    @SerializedName("VentaId")
    public long mVentaId;

    @SerializedName("VentaTotal")
    public double mVentaTotal;

    @SerializedName("VentaCantidad")
    public double mVentaCantidad;

    @SerializedName("VentaFecha")
    public String mVentaFecha;

    @SerializedName("VentaHora")
    public String mVentaHora;

    @SerializedName("VentaMedioPago")
    public String mVentaMedioPago;

    @SerializedName("VentaCodigo")
    public String mVentaCodigo;

    @SerializedName("VentaUserId")
    public int mVentaUserId;

    @SerializedName("VentaTipo")
    public String mVentaTipo;

    @SerializedName("VentaFcType")
    public String mVentaFcType;

    @SerializedName("VentaFcFolio")
    public String mVentaFcFolio;

    @SerializedName("VentaFcValidation")
    public String mVentaFcValidation;

    @SerializedName("VentaNeto")
    public double mVentaNeto;

    @SerializedName("VentaIva")
    public double mVentaIva;

    @SerializedName("VentaTimbre")
    public String mVentaTimbre;

    @SerializedName("VentaAnulada")
    public int mVentaStatus;

    @SerializedName("VentaCliente")
    public String mVentaCliente;

    @SerializedName("VentaFcId")
    private int mVentaFcId;

    public void setId(long id) {
        mVentaId = id;
    }

    public void setFcType(String fcType) {
        mVentaFcType = fcType;
    }

    public void setTotal(double total) {
        mVentaTotal = total;
    }

    public void setCantidad(double cantidad) {
        mVentaCantidad = cantidad;
    }

    public void setFecha(String fecha) {
        mVentaFecha = fecha;
    }

    public void setHora(String hora) {
        mVentaHora = hora;
    }

    public void setMedioPago(String medioPago) {
        mVentaMedioPago = medioPago;
    }

    public void setCodigo(String codigo) {
        mVentaCodigo = codigo;
    }

    public void setUserId(int userId) {
        mVentaUserId = userId;
    }

    public void setNeto(double neto) {
        mVentaNeto = neto;
    }

    public void setIva(double iva) {
        mVentaIva = iva;
    }

    public void setStatus(int status) {
        mVentaStatus = status;
    }

    public void setCliente(String cliente) {
        mVentaCliente = cliente;
    }

    public void setFcId(int fcId) {
        mVentaFcId = fcId;
    }

    public void setTipo(String vta) {
        mVentaTipo = vta;
    }

    public void setFcFolio(String folio) {
        mVentaFcFolio = folio;
    }

    public void setFcValidation(String fcValidation) {
        mVentaFcValidation = fcValidation;
    }

    public void setTimbre(String timbre) {
        mVentaTimbre = timbre;
    }

}
