package com.pds.ficle.ep;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Logger;
import com.pds.common.pref.PDVConfig;
import com.pds.common.util.ContentProviders;
import com.pds.common.util.Perfiles;

public class ConfigPDVActivity extends Activity {

    private Button btnGrabar;
    private Button btnCancelar;

    //CONFIG DE PDV
    private EditText pdv_txtPieTicket;
    private Spinner pdv_spnMedios;
    private String[] mediosValues;
    private CheckBox pdv_chkAjusteSencillo, pdv_chkShowCheckout, pdv_chkShowImpuestos, pdv_chkPrintAutomatic, pdv_chkProdSinPrecios, pdv_chkEmitirComprob, pdv_chkActualizPrecioCatalogo;
    private EditText pdv_txtConfirmTime;
    private Config c;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.config_pdv_activity);

        this.btnGrabar = (Button) findViewById(R.id.config_btn_grabar);
        this.btnCancelar = (Button) findViewById(R.id.config_btn_cancelar);

        //****************CONFIG DE PDV*********************
        pdv_txtPieTicket = (EditText) findViewById(R.id.config_pdv_txt_pie_ticket);
        pdv_spnMedios = (Spinner) findViewById(R.id.config_pdv_ddl_medio);
        ArrayAdapter<CharSequence> medios_adapter = ArrayAdapter.createFromResource(this, R.array.mediosPago, android.R.layout.simple_spinner_item);
        medios_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        pdv_spnMedios.setAdapter(medios_adapter);
        mediosValues = getResources().getStringArray(R.array.mediosPagoValues);
        pdv_chkAjusteSencillo = (CheckBox) findViewById(R.id.config_pdv_chk_sencillo);
        pdv_chkShowCheckout = (CheckBox) findViewById(R.id.config_pdv_chk_checkout);
        pdv_chkPrintAutomatic = (CheckBox) findViewById(R.id.config_pdv_chk_print_auto);
        pdv_chkShowImpuestos = (CheckBox) findViewById(R.id.config_pdv_chk_show_iva);
        pdv_txtConfirmTime = (EditText) findViewById(R.id.config_pdv_txt_confirm_time);
        pdv_chkProdSinPrecios = (CheckBox) findViewById(R.id.config_pdv_chk_prod_solo_precio);
        pdv_chkEmitirComprob = (CheckBox) findViewById(R.id.config_pdv_chk_emitir_comprob);
        pdv_chkActualizPrecioCatalogo = (CheckBox) findViewById(R.id.config_pdv_chk_act_precio_cat);
        //*****************************************************


        this.btnGrabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {

                    boolean huboError = false;
                    String mensajeError = "";

                    //grabamos configuracion de cada modulo
                    if (!SaveConfig()) {
                        huboError = true;
                        mensajeError = "Error al grabar configuracion de Punto de Venta";
                    }

                    Logger.RegistrarEvento(ConfigPDVActivity.this, "i", "Configuracion PDV", "Configuracion actualizada por el usuario");

                    if (!huboError) {
                        Toast.makeText(ConfigPDVActivity.this, "Configuración actualizada con exito", Toast.LENGTH_SHORT).show();

                        finish();//salimos
                    } else
                        Toast.makeText(ConfigPDVActivity.this, mensajeError, Toast.LENGTH_SHORT).show();



                } catch (Exception ex) {
                    Toast.makeText(ConfigPDVActivity.this, "No se pudo actualizar la configuracion: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        this.btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish(); //cerramos la actividad
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        LeerConfig();
    }

    private void LeerConfig() {
        try {

            if (!ContentProviders.ProviderIsAvailable(this, PDVConfig.CONTENT_PROVIDER.getAuthority()))
                throw new Exception("Provider PDV_Config no disponible");

            c = new Config(this, true);

            Cursor cursor = getContentResolver().query(PDVConfig.CONTENT_PROVIDER, PDVConfig.PROJECTION, null, null, null);

            if (cursor.moveToFirst()) {

                int i = cursor.getColumnIndex(PDVConfig.PDV_PIE_TICKET);
                pdv_txtPieTicket.setText(i != -1 ? cursor.getString(i) : "GRACIAS POR SU VISITA");
                //medio...
                i = cursor.getColumnIndex(PDVConfig.PDV_MEDIO_DEFAULT);
                int medioIndex = 0;
                String medioCode = (i != -1 ? cursor.getString(i) : "");
                for (int j = 0; j < mediosValues.length; j++) {
                    if (mediosValues[j].equals(medioCode)) {
                        medioIndex = j;
                        break;
                    }
                }
                pdv_spnMedios.setSelection(medioIndex);
                //
                i = cursor.getColumnIndex(PDVConfig.PDV_AJUSTE_SENCILLO);
                pdv_chkAjusteSencillo.setChecked(i != -1 ? Boolean.parseBoolean(cursor.getString(i)) : false);
                pdv_chkAjusteSencillo.setEnabled(c.PAIS.equals("CH")); //solo habilitado para CH
                //
                i = cursor.getColumnIndex(PDVConfig.PDV_IMPRIMIR_AUTOMATICO);
                pdv_chkPrintAutomatic.setChecked(i != -1 ? Boolean.parseBoolean(cursor.getString(i)) : true);
                //
                i = cursor.getColumnIndex(PDVConfig.PDV_SOLICITAR_CHECKOUT);
                pdv_chkShowCheckout.setChecked(i != -1 ? Boolean.parseBoolean(cursor.getString(i)) : true);
                //
                i = cursor.getColumnIndex(PDVConfig.PDV_MOSTRAR_DESGLOSE_IMPUESTOS);
                pdv_chkShowImpuestos.setChecked(i != -1 ? Boolean.parseBoolean(cursor.getString(i)) : true);
                //
                i = cursor.getColumnIndex(PDVConfig.PDV_TIEMPO_CONFIRMACION);
                pdv_txtConfirmTime.setText(i != -1 ? cursor.getString(i) : "0");
                //
                i = cursor.getColumnIndex(PDVConfig.PDV_MOSTRAR_PRODUCTOS_SIN_PRECIO);
                pdv_chkProdSinPrecios.setChecked(i != -1 ? Boolean.parseBoolean(cursor.getString(i)) : true);
                //
                i = cursor.getColumnIndex(PDVConfig.PDV_COMPROB_HABILIT);
                pdv_chkEmitirComprob.setChecked(i != -1 ? Boolean.parseBoolean(cursor.getString(i)) : false);
                //
                i = cursor.getColumnIndex(PDVConfig.PDV_ACTUALIZ_PRECIO_LISTA);
                pdv_chkActualizPrecioCatalogo.setChecked(i != -1 ? Boolean.parseBoolean(cursor.getString(i)) : true);
            }

        } catch (Exception ex) {
            Toast.makeText(ConfigPDVActivity.this, "Error al leer configuracion de PDV : " + ex.toString(), Toast.LENGTH_SHORT).show();
        }

    }

    private boolean SaveConfig() {

        if (!ContentProviders.ProviderIsAvailable(this, PDVConfig.CONTENT_PROVIDER.getAuthority()))
            return false;

        //vamos a revisar los obligatorios
        boolean error = false;

        String pie_ticket = pdv_txtPieTicket.getText().toString().trim();
        String medio_code = mediosValues[(int) pdv_spnMedios.getSelectedItemId()];
        boolean ajuste_sencillo = pdv_chkAjusteSencillo.isChecked();
        boolean show_checkout = pdv_chkShowCheckout.isChecked();
        boolean show_impuestos = pdv_chkShowImpuestos.isChecked();
        boolean print_auto = pdv_chkPrintAutomatic.isChecked();
        boolean prod_sin_precio = pdv_chkProdSinPrecios.isChecked();
        String confirm_time = pdv_txtConfirmTime.getText().toString().trim();
        boolean emitir_comprob = pdv_chkEmitirComprob.isChecked();
        boolean actualizar_precio_catalogo = pdv_chkActualizPrecioCatalogo.isChecked();

        if (TextUtils.isEmpty(pie_ticket)) {
            error = true;
            pdv_txtPieTicket.setError("Pie de ticket obligatorio");
        }

        if (TextUtils.isEmpty(confirm_time)) {
            confirm_time = "0";
        }

        if (!TextUtils.isDigitsOnly(confirm_time)) {
            error = true;
            pdv_txtConfirmTime.setError("Tiempo de confirmacion no valido");
        }

        if (c.PERFIL.equals(Perfiles.PERFIL_FACTURA_MOVIL)  || c.PERFIL.equals(Perfiles.PERFIL_IQCORP)) {
            //Revisamos la configuaracion de dos parametros del PDV que no son compatibles si se emiten boletas
            if (!show_checkout || !confirm_time.equals("0")) {
                show_checkout = true; //no podemos evitar pasar por el checkout, es necesario seleccionar documento a emitir
                confirm_time = "0";//no podemos cerrar la venta automaticamente, es necesario seleccionar documento a emitir
                error = false;
                Toast.makeText(ConfigPDVActivity.this, "Se detectaron seteos no compatibles con el perfil: No puede evitar el checkout y/o confirmar la venta automaticamente dado que debe seleccionar el tipo de documento a emitir", Toast.LENGTH_LONG).show();
            }
        }

        if (!error) {
            ContentValues cv = new ContentValues();

            cv.put(PDVConfig.PDV_PIE_TICKET, pie_ticket);
            cv.put(PDVConfig.PDV_MEDIO_DEFAULT, medio_code);
            cv.put(PDVConfig.PDV_AJUSTE_SENCILLO, ajuste_sencillo);
            cv.put(PDVConfig.PDV_IMPRIMIR_AUTOMATICO, print_auto);
            cv.put(PDVConfig.PDV_TIEMPO_CONFIRMACION, confirm_time);
            cv.put(PDVConfig.PDV_MOSTRAR_DESGLOSE_IMPUESTOS, show_impuestos);
            cv.put(PDVConfig.PDV_SOLICITAR_CHECKOUT, show_checkout);
            cv.put(PDVConfig.PDV_MOSTRAR_PRODUCTOS_SIN_PRECIO, prod_sin_precio);
            cv.put(PDVConfig.PDV_COMPROB_HABILIT, emitir_comprob);
            cv.put(PDVConfig.PDV_ACTUALIZ_PRECIO_LISTA, actualizar_precio_catalogo);

            getContentResolver().insert(PDVConfig.CONTENT_PROVIDER, cv);

            return true;
        } else
            return false;
    }
}
