package com.pds.ficle.ep.models.syncBuys;

import com.google.gson.annotations.SerializedName;
import com.pds.ficle.ep.models.BaseFicleRequestWA;

import java.util.ArrayList;

public class BuyRequestWA extends BaseFicleRequestWA {
    @SerializedName("Buys")
    public ArrayList<BuyRequestItemWA> mItems;
}
