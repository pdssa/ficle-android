package com.pds.ficle.ep.cloud;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public abstract class BaseUpdatesAsyncTask extends AsyncTask<Void, String, String> {

    protected Context mCtx;

    public BaseUpdatesAsyncTask(Context ctx)
    {
        mCtx = ctx;
    }



    private static final String TAG = BaseUpdatesAsyncTask.class.getSimpleName();


    protected boolean isEntryPoint(String packageName, String fileName) {
        return
                packageName.equalsIgnoreCase("com.pds.ficle.ep") ||
                        fileName.contains("EntryPoint");
    }

    protected void install(String localPath, String packageName, String url) throws Exception {
        try {

            if(!new File(localPath).exists()){
                return;//si el file no existe, salimos
            }

            if (this.isEntryPoint(packageName, getFileName(url))) {
                installByIntent(localPath);//instalamos de esta forma, para evitar conflictos con la APP iniciada
            } else {
                //primero intentamos instalar de modo SILENT
                try {

                    int result = installSilent(localPath);

                    //Log.d("INSTALL", String.valueOf(result));

                    if (result != 0) //instalacion erronea
                        throw new Exception("Failure SilentInstall. Exit code: " + String.valueOf(result));

                } catch (Exception e) {
                    //sino podemos instalamos vía INTENT
                    installByIntent(localPath);
                }
            }
        } catch (Exception e) {
            throw e;
        }

    }

    protected int installSilent(String localPath) throws Exception {
        try {
            String filename = localPath;
            String command = "pm install -r " + filename;
            Process proc = Runtime.getRuntime().exec(new String[]{"su", "-c", command});
            return proc.waitFor();

        } catch (Exception e) {
            throw e;
        }

    }

    protected void installByIntent(String localPath) throws Exception {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(new File(localPath)), "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mCtx.startActivity(intent);
        } catch (Exception e) {
            throw e;
        }

    }

    protected void deleteFileIfExists(String folder, String url) throws Exception {
        try {

            if (!isSdPresent()) {
                throw new Exception("Tarjeta SD no disponible");
            }

            String path = Environment.getExternalStorageDirectory() + folder;
            File downloadFolder = new File(path); // PATH = /mnt/sdcard/download/pds
            if (downloadFolder.exists()) {//entramos si existe
                File apkFile = new File(downloadFolder, getFileName(url));
                if (apkFile.exists()) {//borramos si ya existe
                    apkFile.delete();
                }
            }

        } catch (Exception e) {
            throw e;
        }
    }


    protected String download(String folder, boolean forcedDownload, String urlParam) throws Exception {
        try {

            if (!isSdPresent()) {
                throw new Exception("Tarjeta SD no disponible");
            }

            String localPath = null;
            String filename = getFileName(urlParam);


            URL url = new URL(urlParam);
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(false);
            c.connect();

            Log.v(TAG, "Descargando " + urlParam);

            String path = Environment.getExternalStorageDirectory() + folder;
            File downloadFolder = new File(path); // PATH = /mnt/sdcard/download/pds
            if (!downloadFolder.exists()) {//creamos el dir si no existe
                downloadFolder.mkdirs();
            }
            File apkFile = new File(downloadFolder, filename);

            boolean hasToDownload = forcedDownload;

            if(!forcedDownload && !apkFile.exists())
                hasToDownload = true;

            if (hasToDownload) {//no descargamos si ya existe

                String tempfile = filename + ".temp";
                File outputFile = new File(downloadFolder, tempfile);
                FileOutputStream fos = new FileOutputStream(outputFile);

                InputStream is = c.getInputStream(); // Get from Server and Catch In Input Stream Object.

                byte[] buffer = new byte[1024];
                int len1 = 0;
                while ((len1 = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);// Write In FileOutputStream.
                }
                fos.close();
                is.close();//en este punto el .apk ya deberia estar descargado en el storage

                //rename to apk
                if (outputFile.exists()) {
                    outputFile.renameTo(apkFile);
                    localPath = path + filename;
                }


            }

            return localPath;

        } catch (Exception e) {
            throw e;
        }
    }

    protected String getFileName(String urlParam) {
        //obtenemos el file name como el ultimo componente del url
        String[] separadores = urlParam.split("/");
        return separadores[separadores.length - 1];
    }


    protected boolean isSdPresent() {
        return Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
    }

    protected boolean hasToUpdateApp(String packageName, String packageLastVersion) {

        String packageInstalledVersion = null;

        try {
            packageInstalledVersion = this.getInstalledVersion(packageName);
        } catch (Exception ex) {
            return false;
        }
        //retorna true si son diferentes (entonces corresponde actualizar), si son iguales false
        //convertimos la version en int quitando el punto, ejemplo: 1.23 = > 123
        //y luego comparamos como int para solo actualizar por mayor
        //return !_packageInstalledVersion.equals(_packageLastVersion);

        //pueden pasar dos situaciones, que sean packages nuestros (sabemos que manejamos en versionName = "1.27" por ejemplo
        //o pueden ser third party packages, normalmente usan como descripcion dicho campo
        if (packageName.startsWith("com.pds.ficle")) {
            //OWN PACKAGE
            int installedVersion = Integer.parseInt(packageInstalledVersion.replace(".", ""));
            int lastVersion = Integer.parseInt(packageLastVersion.replace(".", ""));

            return lastVersion > installedVersion;
        } else {
            //THIRD PARTY PACKAGE
            //solo comparamos si coinciden en el versionName => actualizamos si no coinciden
            return packageInstalledVersion.equals(packageLastVersion) == false;
        }
    }

    protected String getInstalledVersion(String packageName) throws Exception {
        try {
            /* Get current Version Name */
            return mCtx.getPackageManager().getPackageInfo(packageName, 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return "0"; //esto lo hacemos para poder instalar packages por primera vez
        } catch (Exception e) {
            throw e;
        }
    }
}
