package com.pds.ficle.ep.utils;


import com.pds.common.Formato;

import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class Utils {
    public static boolean esMismoDia(Date fechaA, Date fechaB) {

        //pasamos las fechas al mismo formato de string para comparar
        String strFechaA = Formato.FormateaDate(fechaA, Formato.ShortDateFormat);
        String strFechaB = Formato.FormateaDate(fechaB, Formato.ShortDateFormat);

        return strFechaA.equals(strFechaB);
    }


    public static Date Util_Fecha(String fechaFormat) {
        String result = "";
        try {
            Date fecha = new SimpleDateFormat("yyyy-MM-dd").parse(fechaFormat);
            return fecha;
        } catch (Exception ex) {
            result = "";
        }
        return null;
    }

    public static Date Util_FechaHora(String fechaHoraTformat) {
        String result = "";
        try {
            Date fechaHora = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(fechaHoraTformat);
            return fechaHora;
        } catch (Exception ex) {
            result = "";
        }
        return null;
    }
    public static String Util_FechaHora(String _fecha,String _hora) {
        try {
            String[] fecha  = _fecha.split("-");
            String[] hora  = _hora.split(":");
            Date fecha_formatted = new Date(Integer.parseInt(fecha[2]),Integer.parseInt(fecha[1]) - 1,Integer.parseInt(fecha[0]),Integer.parseInt(hora[0]),Integer.parseInt(hora[1]),Integer.parseInt(hora[2]));

            //return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").format(fecha_formatted);

            return fecha[2] + "-" + fecha[1] + "-" + fecha[0] + "T" + hora[0] + ":" + hora[1] + ":" + hora[2] + "Z";
        } catch (Exception ex) {
            return  Util_FechaHoraTerminal();
        }
    }

    public static String Util_FechaHoraTerminal() {
        return Util_FechaHoraTerminal(new Date());
    }

    public static String Util_FechaHoraTerminal(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").format(date);
    }

    public static String Util_FechaTerminal() {

        return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").format(new Date());

    }

    public static String Util_FechaTerminal(String day, String month) {
        return (new SimpleDateFormat("yyyy").format(new Date())) + "-" + month + "-" + day;
    }
}
