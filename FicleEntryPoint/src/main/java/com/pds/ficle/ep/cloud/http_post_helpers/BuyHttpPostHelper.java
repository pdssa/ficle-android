package com.pds.ficle.ep.cloud.http_post_helpers;

import com.pds.common.Config;

import org.apache.http.client.methods.HttpPost;

public class BuyHttpPostHelper extends HttpPostHelper {
    public BuyHttpPostHelper(Config config) {
        super(config);
    }

    @Override
    protected HttpPost getHttpPostForThisRequest() {
        return new HttpPost(mServidor + "api/SyncBuys");
    }
}
