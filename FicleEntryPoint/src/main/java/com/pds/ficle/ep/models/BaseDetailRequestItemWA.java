package com.pds.ficle.ep.models;

import com.google.gson.annotations.SerializedName;

public abstract class BaseDetailRequestItemWA {
    @SerializedName("Cantidad")
    public double mCantidad;

    @SerializedName("Id")
    public int mId;

    @SerializedName("OrigId")
    public int mOrigId;

    @SerializedName("Precio")
    public double mPrecio;

    @SerializedName("ProductoId")
    public int mProductoId;

    @SerializedName("Sku")
    public String mSku;

    @SerializedName("Total")
    public double mTotal;

    @SerializedName("Neto")
    private double mNeto;

    @SerializedName("Iva")
    private double mIva;

    public void setId(int id) {
        mId = id;
    }

    public void setOrigId(int origId) {
        mOrigId = origId;
    }

    public void setCantidad(double cantidad) {
        mCantidad = cantidad;
    }

    public void setPrecio(double precio) {
        mPrecio = precio;
    }

    public void setTotal(double total) {
        mTotal = total;
    }

    public void setSku(String sku) {
        mSku = sku;
    }

    public void setProductId(int prodId) {
        mProductoId = prodId;
    }

    public void setNeto(double neto) {
        mNeto = neto;
    }

    public void setIva(double iva) {
        mIva = iva;
    }
}
