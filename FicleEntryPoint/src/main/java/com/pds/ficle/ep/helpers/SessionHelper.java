package com.pds.ficle.ep.helpers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.pds.common.Config;
import com.pds.common.Formatos;

import java.io.DataOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class SessionHelper {

    public static final String TAG = SessionHelper.class.getSimpleName();
    public static final String MAIN_SHARED_PREFERENCES = "MAIN_SHARED_PREFERENCES";
    public static final String SESSION_HELPER_LAST_UPDATE_TIMESTAMP = "SESSION_HELPER_LAST_UPDATE_TIMESTAMP";
    public static final String DATE_FORMAT = "dd/MM/yyyy HH:mm:ss";

    private SharedPreferences mPrefs;
    private static SessionHelper _Instance;
    private Context mCtx;
    private Date mLastUpdateTimeStamp;

    private SessionHelper() {

    }

    public static SessionHelper getInstance() {

        if (_Instance == null) {
            _Instance = new SessionHelper();
        }

        return _Instance;
    }

    public void init(Context ctx) {
        mCtx = ctx;
    }

    public void setLastUpdateTimestamp(Date date) {
        mPrefs = mCtx.getSharedPreferences(MAIN_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(SESSION_HELPER_LAST_UPDATE_TIMESTAMP,new SimpleDateFormat(DATE_FORMAT).format(date));
        editor.commit();
    }

    public Date getLastUpdateTimestamp() throws ParseException {
        mPrefs = mCtx.getSharedPreferences(MAIN_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return new SimpleDateFormat().parse(mPrefs.getString(SESSION_HELPER_LAST_UPDATE_TIMESTAMP, "27/08/1984 13:15:00"));
    }

    public boolean hasToSyncData() {

        //obtenemos la ultima fecha de sync
        String last_sync_time_string = mCtx.getSharedPreferences("com.pds.ficle.ep", Context.MODE_PRIVATE).getString("last_sync_time", "2010-01-01 00:00:00");

        Date last_sync_time = null;
        try {
            last_sync_time = Formatos.ObtieneDate(last_sync_time_string, Formatos.DbDateTimeFormat);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //calculamos la diferencia de tiempo
        long difference = 0;
        Date now = new Date();
        if (now.after(last_sync_time))
            difference = now.getTime() - last_sync_time.getTime();
        else
            difference = last_sync_time.getTime() - now.getTime();

        //sincronizamos solo si la diferencia es mayor a 24 hs
        long x = difference / 1000;
        //long seconds = x % 60;
        x /= 60;
        /*long minutes = x % 60;
        x /= 60;
        long hours = x % 24;
        x /= 24;
        long days = x;*/

        //return days > 0;
        int syncMinutes = new Config(mCtx).SYNC_TIME;
        if(x >= syncMinutes)
        {
            Log.e(TAG, "hasToSyncData:  Ready to sync!");
            return true;
        }
        else
        {
            Log.e(TAG, "hasToSyncData:  "+(syncMinutes-x)+" minutes left...");

            return false;
        }

    }


    public boolean isInitialized() {
        return mCtx != null;
    }
}