/*
package com.pds.ficle.ep.cloud;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.TextUtils;

import com.pds.common.Config;
import com.pds.common.DbHelper;
import com.pds.common.Logger;
import com.pds.common.dao.DepartmentDao;
import com.pds.common.dao.ProductDao;
import com.pds.common.dao.SubDepartmentDao;
import com.pds.common.dao.TaxDao;
import com.pds.common.model.Department;
import com.pds.common.model.Product;
import com.pds.common.model.SubDepartment;
import com.pds.common.model.Tax;
import com.pds.common.util.ConnectivityUtils;
import com.pds.ficle.ep.MainActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

*/
/**
 * Created by Hernan on 30/10/2014.
 *//*

public class GetProductCatalogTask extends AsyncTask<String, String, String> {
    private Context _context;
    private Config _config;
    private String _categorias;
    private ProgressDialog dialog;
    private int RESP_OK = -1, RESP_MAL = -1;


    */
/*private CATALOG_TYPE _type;

    public static enum CATALOG_TYPE {
        PRODUCTOS,
        CATEGORIAS
    }*//*


    public void set_context(Context _context) {
        this._context = _context;
    }

    public GetProductCatalogTask(Context _context, String categorias, String catalog) {
        this._context = _context;
        this._config = new Config(_context);
        this._categorias = categorias;
        this.dialog = new ProgressDialog(_context);
        this.catalogName = catalog;
    }

    public GetProductCatalogTask(Context _context, String categorias, String catalog, int code_resp_ok, int code_resp_mal) {
        this._context = _context;
        this._config = new Config(_context);
        this._categorias = categorias;
        this.dialog = new ProgressDialog(_context);
        this.catalogName = catalog;
        this.RESP_OK = code_resp_ok;
        this.RESP_MAL = code_resp_mal;
    }

    ///FIXME: HZ! Resolver esto
    private List<String> GetLinesFromServer(String _catalogUrl) throws Exception {
        //String _url = "http://" + this._config.CLOUD_SERVER_HOST + ":" + this._config.CLOUD_SERVER_PORT + "/download/csv/" + _catalog + ".csv";
        URL url = new URL(_catalogUrl);

        //BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream(), "ISO-8859-1"));
        BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"));

        List<String> str = new ArrayList<String>();
        String _str;
        _str = in.readLine();
        str.add(_str.charAt(0) == '\uFEFF' ? _str.substring(1) : _str); //remove BOM char 65279
        while ((_str = in.readLine()) != null) {
            str.add(_str.charAt(0) == '\uFEFF' ? _str.substring(1) : _str); //remove BOM char 65279
        }

        in.close();

        return str;
    }

    private static boolean isSdPresent() {
        return Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
    }

    private String processFile(List<String> registros) {
        int cantDeptos = 0, cantSubdeptos = 0, cantProduct = 0;
        int cantDeptosNo = 0, cantSubdeptosNo = 0, cantProductNo = 0, cantProductUpd = 0;
        //long deltaDepto = -1, deltaSubdepto = -1;
        ContentResolver contentResolver = _context.getContentResolver();
        DepartmentDao departmentDao = new DepartmentDao(contentResolver);
        SubDepartmentDao subDepartmentDao = new SubDepartmentDao(contentResolver);
        ProductDao productDao = new ProductDao(contentResolver);
        //CategoriaDao categoriaDao = new CategoriaDao(contentResolver);

        //departamentos
        HashMap<String, Department> tableDeptos = new HashMap<String, Department>();
        //subdepartamentos
        HashMap<String, SubDepartment> tableSubdeptos = new HashMap<String, SubDepartment>();


        Tax _gravado = new TaxDao(contentResolver).list("name = 'Gravado'", null, null).get(0);
        Tax _exento = new TaxDao(contentResolver).list("name = 'Exento'", null, null).get(0);//se usa para cigarrillos

        long idSubDeptoCigarrillos = 0;


        for (String registro : registros) {

            registro = registro.trim();

            */
/*if (registro.startsWith("C;")) {
                //CATEGORIA
                Categoria c = new Categoria(registro);
                categoriaDao.save(c);
            }*//*


            if (registro.startsWith("D;")) {
                //DEPARTAMENTO
                Department d = new Department(registro);
                if (departmentDao.importDepartment(d)) {

                    tableDeptos.put(d.getCodigo(), d);//agregamos el depto a la tabla

                    //if (d.getName().equalsIgnoreCase("TABACO"))//esto es para marcarlos como exentos
                    //    idDeptoCigarrillos = d.getId();


                    if (d.isGenericDept()) {
                        Product p = d.exportDepartmentGenericProduct(_gravado);
                        productDao.save(p);
                    }

                    cantDeptos++;

                } else {
                    cantDeptosNo++;
                }
            }

            if (registro.startsWith("S;")) {
                //SUBDEPARTAMENTO
                SubDepartment sd = new SubDepartment(registro);

                //tomamos el depto de la tabla actual
                sd.setDepartment(tableDeptos.get(sd.getCodigoPadre()));

                if (subDepartmentDao.importSubDepartment(sd)) {

                    tableSubdeptos.put(sd.getCodigo(), sd);//agregamos el subdepto a la tabla

                    */
/*if (deltaSubdepto == -1)
                        deltaSubdepto = sd.getId() - 1;*//*


                    //si es TABACO => los productos son exentoses exento
                    if(sd.getCodigo().equals("009002"))
                        idSubDeptoCigarrillos = sd.getId();

                    if (sd.isGenericSubdept()) {
                        Product p = sd.exportSubDepartmentGenericProduct(idSubDeptoCigarrillos == sd.getId() ? _exento : _gravado);
                        productDao.save(p);
                    }

                    cantSubdeptos++;

                } else {
                    cantSubdeptosNo++;
                }

            }

            if (registro.startsWith("P;")) {
                //PRODUCTO
                Product p = new Product(registro);

                //tomamos los deptos/subdeptos
                p.setSubDepartment(tableSubdeptos.get(p.getCodigoPadre()));
                p.setDepartment(p.getSubDepartment().getDepartment());
                p.setIdSubdepartment(p.getSubDepartment().getId());
                p.setIdDepartment(p.getDepartment().getId());

                //establecemos el tipo de impuesto en base a si es tabaco o no
                if (p.getIdSubdepartment() == idSubDeptoCigarrillos) {
                    p.setIvaEnabled(true);
                    p.setTax(_exento);
                    p.setIva(_exento.getAmount());
                } else {
                    p.setIvaEnabled(true);
                    p.setTax(_gravado);
                    p.setIva(_gravado.getAmount());
                }


                int result = productDao.importProduct(p);
                if (result == 1)
                    cantProduct++;
                else if (result == 2)
                    cantProductUpd++;
                else if (result == 0)
                    cantProductNo++;
            }


        }


        //4-limpiamos departamentos y subdepartamentos que no hayan venido en el catalogo y están sin uso
        DeleteEmptySubdepartments(tableSubdeptos);

        DeleteEmptyDepartments(tableDeptos);

        return String.format("Departamentos creados: %d\nDepartamentos omitidos: %d\nSubdepartamentos creados: %d\nSubdepartamentos omitidos: %d\nProductos creados: %d\nProductos actualizados: %d\nProductos omitidos: %d", cantDeptos, cantDeptosNo, cantSubdeptos, cantSubdeptosNo, cantProduct, cantProductUpd, cantProductNo);



    }

    @Override
    protected void onPreExecute() {
        //iniciamos un mensaje para el usuario
        dialog = new ProgressDialog(_context);
        dialog.setMessage("Iniciando descarga del catalogo. Aguarde por favor...");
        dialog.setIndeterminate(false);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

    }

    private String shortResult = "";
    private String catalogName = "";

    @Override
    protected String doInBackground(String... params) {
        try {

            if (!ConnectivityUtils.isOnline(_context)) {
                return "No hay una conexion disponible";
            }

            String result = "";

            //CATEGORIAS
            //esto queda en desuso desde la version 1.78
            //no se utilizan mas las categorias gs1 localmente
            */
/*if (!_categorias.equals("") && !hayCategorias()) {
                //1-connect and get categorias
                publishProgress("Obteniendo maestro de categorias GS1 del servidor...");

                List<String> lineas_cat = GetLinesFromServer("http://" + this._config.CLOUD_SERVER_HOST + ":" + this._config.CLOUD_SERVER_PORT + "/download/csv/" + _categorias + ".csv");

                //2-fetch response and generate all entities
                publishProgress("Importando categorias. Aguarde por favor...");

                //3-process
                result += processCategoriasFile(lineas_cat) + "\n";
            }*//*


            //PRODUCTOS
            if(!TextUtils.isEmpty(params[0])) {
                //1-connect and get new versions
                publishProgress("Obteniendo lista de productos del servidor...");

                List<String> lineas = GetLinesFromServer(params[0]);

                //2-fetch response and generate all entities
                publishProgress("Importando registros. Aguarde por favor...");

                //3-process
                result += processFile(lineas);
            }

            shortResult = result.replace("\n","-").replace("Departamentos","D").replace("Subdepartamentos","S").replace("creados","c").replace("omitidos","o").replace("actualizados","u").replace("Productos", "P");

            return "Proceso finalizado correctamente: \n" + result;

        } catch (IOException e) {
            //return "No es posible conectarse. Intente mas tarde.";
            return "Error IO: " + e.getMessage();
        } catch (Exception e) {
            return "Error: " + e.getMessage();
        }
    }

    @Override
    protected void onProgressUpdate(String... p) {
        //Toast.makeText(_context, p[0], Toast.LENGTH_LONG).show();
        dialog.setMessage(p[0]);
    }

    // onPostExecute displays the results of the AsyncTask.
    @Override
    protected void onPostExecute(String result) {
        //Toast.makeText(_context, result, Toast.LENGTH_LONG).show();
        if (dialog.isShowing())
            dialog.dismiss();

        if(!result.startsWith("Error:"))
            Logger.RegistrarEvento(_context, "i", "DESCARGA CATALOGO", catalogName, shortResult);
        else
            Logger.RegistrarEvento(_context, "e", "DESCARGA CATALOGO", catalogName, result);

        AlertMessage(result, !result.startsWith("Error:"));
    }

    @Override
    protected void onCancelled(String s) {
        //Toast.makeText(_context, s, Toast.LENGTH_LONG).show();
        AlertMessage(s, false);
    }

    private void AlertMessage(String message, final boolean result) {
        AlertDialog.Builder builder = new AlertDialog.Builder(_context);
        builder.setTitle("IMPORTACIÓN DE CATALOGO");
        builder.setMessage(message);
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface _dialog, int which) {
                _dialog.dismiss();

                Finalizar(result);
            }
        });
        builder.setCancelable(false);
        builder.show();
    }

    private void Finalizar(boolean result){
        if(result && RESP_OK != -1){
            ((MainActivity)_context).Init_Flow(RESP_OK); //MANDAMOS EL CODIGO DE RESULTADO OK RECIBIDO
        }
        else if(RESP_MAL != -1){
            ((MainActivity)_context).Init_Flow(RESP_MAL); //MANDAMOS EL CODIGO DE RESULTADO MAL RECIBIDO
        }
    }

    private void DeleteEmptySubdepartments(HashMap<String, SubDepartment> tableSubdeptos) {
        if(!tableSubdeptos.isEmpty()) {
            SQLiteDatabase db = new DbHelper(_context).getWritableDatabase();

            String codigosSubdeptos = "";

            for (SubDepartment s : tableSubdeptos.values()) {
                codigosSubdeptos += ("'" + s.getCodigo() + "',");
            }

            codigosSubdeptos = codigosSubdeptos.substring(0, codigosSubdeptos.length() - 1);

            db.execSQL("delete from subdepartments where codigo not in (" + codigosSubdeptos + ") and not exists (select 1 from products where subdepartmennt_id = subdepartments._id and removed = 0 and generic = 0) ;");
            db.close();
        }
    }

    private void DeleteEmptyDepartments(HashMap<String, Department> tableDeptos) {
        if(!tableDeptos.isEmpty()) {
            SQLiteDatabase db = new DbHelper(_context).getWritableDatabase();

            String codigosDeptos = "";

            for (Department d : tableDeptos.values()) {
                codigosDeptos += ("'" + d.getCodigo() + "',");
            }

            codigosDeptos = codigosDeptos.substring(0, codigosDeptos.length() - 1);

            db.execSQL("delete from departments where codigo not in (" + codigosDeptos + ") and not exists (select 1 from products where department_id = departments._id and removed = 0 and generic = 0) and not exists (select 1 from subdepartments where department_id = departments._id);");
            db.close();
        }
    }
}
*/
