package com.pds.ficle.ep.task;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.pds.common.CajaHelper;
import com.pds.common.Config;
import com.pds.common.DbHelper;
import com.pds.common.Formatos;
import com.pds.common.LogHelper;
import com.pds.common.Logger;
import com.pds.ficle.ep.MainActivity;

import java.util.Date;

/**
 * Created by Hernan on 22/04/2015.
 */
public class ClearOldDataTask extends AsyncTask<Void, String, String> {

    private Context _context;
    private boolean _activate;
    private String TAG = "Borrado Historico";
    private String RESULT_OK = "ok";
    private String RESULT_NO_ACTIVATED = "!activated";
    boolean _hubo_error;
    private ProgressDialog dialog;

    int CONFIG_DAYS = 0;

    public ClearOldDataTask() {
    }

    public ClearOldDataTask(Context context) {
        this._context = context;
    }

    public boolean isFromMain(){
        return _context.getClass().getSimpleName().equalsIgnoreCase("MainActivity");
    }

    // onPreExecute used to setup the AsyncTask.
    @Override
    protected void onPreExecute() {

        CONFIG_DAYS = new Config(_context).HIST_DAYS;

        _activate = correspondeBorrarDatos();

        if (_activate) {
            if(isFromMain()) {
                ((MainActivity) _context).showProgress(true);
                ((MainActivity) _context).showProgressMessage("Iniciando aplicaciones...");
            }
            else{
                //iniciamos un mensaje para el usuario
                dialog = new ProgressDialog(_context);
                dialog.setMessage("Iniciando proceso de borrado de historicos. Aguarde por favor...");
                dialog.setIndeterminate(false);
                dialog.setCancelable(false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            }
        }

    }

    @Override
    protected String doInBackground(Void... params) {

        // params comes from the execute() call: params[0] is the message.
        try {

            if (!_activate) {//si no está habilitado el task no lo ejecutamos
                return RESULT_NO_ACTIVATED;
            }

            _hubo_error = false;

            SQLiteDatabase _db = new DbHelper(_context).getWritableDatabase();

            //********LOGS*********
            String log_expression = "sync = 'S' and julianday('now') - julianday(substr(fecha, 7)||'-'||substr(fecha,4,2)||'-'||substr(fecha,1,2)||' '||hora) > " + String.valueOf(CONFIG_DAYS);
            deleteGeneric(_db, LogHelper.TABLE_NAME, log_expression);

            //********VENTAS*********
            deleteVentas(_db);

            //********COMPROBANTES*********
            deleteComprobantes(_db);

            //********CAJA*********
            String caja_expression = "sync = 'S' and idVenta is null and julianday('now') - julianday(substr(fecha, 7)||'-'||substr(fecha,4,2)||'-'||substr(fecha,1,2)||' '||hora) > " + String.valueOf(CONFIG_DAYS);
            deleteGeneric(_db, CajaHelper.TABLE_NAME, caja_expression);

            //--------------------------------------------------
            return !_hubo_error ? RESULT_OK : "mal";

        } catch (Exception e) {
            return "Error: " + e.toString();
        }
    }


    @Override
    protected void onProgressUpdate(String... p) {
        //Toast.makeText(_context, p[0], Toast.LENGTH_LONG).show();
        if(isFromMain())
            ((MainActivity) _context).showProgressMessage(p[0]);
        else
            dialog.setMessage(p[0]);
    }

    // onPostExecute displays the results of the AsyncTask.
    @Override
    protected void onPostExecute(String result) {
        if (result.equals(RESULT_OK)) {//si finalizó OK => grabamos la ultima fecha de sync
            updateLastClear();
            Logger.RegistrarEvento(_context, "i", TAG, "Proceso ejecutado correctamente");

        } else if (!result.equals(RESULT_NO_ACTIVATED) && !result.equals("mal"))//sino => registramos error
            Logger.RegistrarEvento(_context, "e", TAG, result);
        /*else
            updateLastClear(true);*/

        FinalizarEjecucion();
    }

    @Override
    protected void onCancelled(String s) {
        Toast.makeText(_context, s, Toast.LENGTH_LONG).show();

        FinalizarEjecucion();
    }

    private void FinalizarEjecucion() {
        if (_activate) {
            if(isFromMain()) {
                ((MainActivity) _context).showProgress(false);
                ((MainActivity) _context).Init_Flow(6);//--> x
            }
            else{
                if (dialog.isShowing())
                    dialog.dismiss();
            }
        }
        else{
            if(!isFromMain()) {
                AlertMessage("Aún no corresponde borrar históricos. Lapso aún no superado");
            }
            else{
                ((MainActivity) _context).Init_Flow(6);//--> x
            }
        }
    }

    private boolean correspondeBorrarDatos() {
        try {
            //obtenemos la ultima fecha de borrado
            String last_clear_time_string = _context.getSharedPreferences("com.pds.ficle.ep", Context.MODE_PRIVATE).getString("last_clear_time", "2010-01-01 00:00:00");
            //Toast.makeText(_context, last_clear_time_string, Toast.LENGTH_LONG).show();

            Date last_clear_time = null;
            try {
                last_clear_time = Formatos.ObtieneDate(last_clear_time_string, Formatos.DbDateTimeFormat);
            } catch (Exception e) {
                e.printStackTrace();
            }

            //calculamos la diferencia de tiempo
            long difference = 0;
            Date now = new Date();
            if (now.after(last_clear_time))
                difference = now.getTime() - last_clear_time.getTime();
            else
                difference = last_clear_time.getTime() - now.getTime();

            //descomponemos la diferencia, que la tenemos en milisegundos
            long x = difference / 1000;
            //long seconds = x % 60;
            x /= 60;
            //long minutes = x % 60;
            x /= 60;
            //long hours = x % 24;
            x /= 24;
            long days = x;

            //ejecutamos solo si : la diferencia en dias es mayor a la configurada(*)
            // (*) como parametro configurado tomamos la mitad de los días de resguardo de datos
            // ejemplo, para 60 días de histórico, el proceso corre cada 30 días

            if (CONFIG_DAYS == 0)
                return false; //si es 0, tomamos la funcionalidad está desactivada
            else
                return days > (CONFIG_DAYS / 2);
        } catch (Exception ex) {

            Logger.RegistrarEvento(_context, "e", TAG, ex.toString());
            Toast.makeText(_context, TAG + ": " + ex.toString(), Toast.LENGTH_LONG).show();

            return false;
        }
    }

    private void updateLastClear() {

        //grabamos la ultima fecha de sync
        Date last_clear_time = new Date();
        SharedPreferences.Editor editor = _context.getSharedPreferences("com.pds.ficle.ep", Context.MODE_PRIVATE).edit();
        editor.putString("last_clear_time", Formatos.FormateaDate(last_clear_time, Formatos.DbDateTimeFormat));
        editor.commit();

    }

    /*--esto es a efectos de testear
        private void updateLastClear(boolean force) {

        //grabamos la ultima fecha de sync
        Date last_clear_time = new Date();
        SharedPreferences.Editor editor = _context.getSharedPreferences("com.pds.ficle.ep", Context.MODE_PRIVATE).edit();
        if (!force)
            editor.putString("last_clear_time", Formatos.FormateaDate(last_clear_time, Formatos.DbDateTimeFormat));
        else {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -100);
            editor.putString("last_clear_time", Formatos.FormateaDate(cal.getTime(), Formatos.DbDateTimeFormat));

        }
        editor.commit();

    }*/

    private boolean deleteGeneric(SQLiteDatabase db, String tableName, String filterExpression) {
        boolean result = false;

        String _sql = "delete from " + tableName + " where " + filterExpression + " ;";

        db.beginTransaction(); //iniciamos la transaccion
        try {
            db.execSQL(_sql);

            db.setTransactionSuccessful(); //commit transaction

            result = true;
        } catch (Exception ex) {
            //Error in between database transaction
            _hubo_error = true;
            Log.e(TAG, ex.getMessage());
            Logger.RegistrarEvento(_context, "e", TAG, ex.getMessage());
        } finally {
            db.endTransaction(); //end transaction (if setTransactionSuccessful() not called => is rollback)
        }

        return result;
    }

    private boolean deleteVentas(SQLiteDatabase db) {

        boolean result = false;

        String dias_config = String.valueOf(CONFIG_DAYS);

        db.beginTransaction(); //iniciamos la transaccion
        try {

            String _ventas_where = " sync = 'S' and julianday('now') - julianday(substr(fecha, 7)||'-'||substr(fecha,4,2)||'-'||substr(fecha,1,2)||' '||hora) > " + dias_config + " ";

            String _sql_1 = "delete from venta_det where sync = 'S' and id_venta in ( select _id from ventas where " + _ventas_where + " );";

            String _sql_2 = "delete from caja where sync = 'S' and tipoVta = 'VTA' and idVenta in ( select _id from ventas where " + _ventas_where + " );";

            String _sql_3 = "delete from ventas where " + _ventas_where + " " +
                    " and not exists (select 1 from venta_det where ventas._id = id_venta) " +
                    " and not exists (select 1 from caja where ventas._id = idVenta and tipoVta = 'VTA' ) ";

            //borramos los detalles sincronizados de las ventas historicas sincronizadas
            db.execSQL(_sql_1);

            //borramos los registros de caja sincronizados de las ventas historicas sincronizadas
            db.execSQL(_sql_2);

            //borramos las ventas historicas sincronizadas que no tengan detalles o registros de caja asociados
            db.execSQL(_sql_3);

            db.setTransactionSuccessful(); //commit transaction

            result = true;
        } catch (Exception ex) {
            //Error in between database transaction
            _hubo_error = true;
            Log.e(TAG, ex.getMessage());
            Logger.RegistrarEvento(_context, "e", TAG, ex.getMessage());
        } finally {
            db.endTransaction(); //end transaction (if setTransactionSuccessful() not called => is rollback)
        }

        return result;
    }

    private boolean deleteComprobantes(SQLiteDatabase db) {

        boolean result = false;

        String dias_config = String.valueOf(CONFIG_DAYS);

        db.beginTransaction(); //iniciamos la transaccion
        try {

            String _comprobantes_where = " sync = 'S' and julianday('now') - julianday(substr(fecha, 7)||'-'||substr(fecha,4,2)||'-'||substr(fecha,1,2)||' '||hora) > " + dias_config + " ";

            String _sql_1 = "delete from comprobante_det where sync = 'S' and id_comprobante in ( select _id from comprobantes where " + _comprobantes_where + " );";

            String _sql_2 = "delete from caja where sync = 'S' and tipoVta = 'CPB' and idVenta in ( select _id from comprobantes where " + _comprobantes_where + " );";

            String _sql_3 = "delete from comprobantes where " + _comprobantes_where + " " +
                    " and not exists (select 1 from comprobante_det where comprobantes._id = id_comprobante) " +
                    " and not exists (select 1 from caja where comprobantes._id = idVenta and tipoVta = 'CPB' ) ";

            //borramos los detalles sincronizados de los comprobantes historicos sincronizados
            db.execSQL(_sql_1);

            //borramos los registros de caja sincronizados de las comprobantes historicos sincronizados
            db.execSQL(_sql_2);

            //borramos los comprobantes historicos sincronizados que no tengan detalles o registros de caja asociados
            db.execSQL(_sql_3);

            db.setTransactionSuccessful(); //commit transaction

            result = true;
        } catch (Exception ex) {
            //Error in between database transaction
            _hubo_error = true;
            Log.e(TAG, ex.getMessage());
            Logger.RegistrarEvento(_context, "e", TAG, ex.getMessage());
        } finally {
            db.endTransaction(); //end transaction (if setTransactionSuccessful() not called => is rollback)
        }

        return result;
    }

    private void AlertMessage(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(_context);
        builder.setTitle("BORRADO HISTORICO");
        builder.setMessage(message);
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface _dialog, int which) {
                _dialog.dismiss();
            }
        });
        builder.setCancelable(false);
        builder.show();
    }
}
