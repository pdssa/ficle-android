package com.pds.ficle.ep.models.checkDownload;

import com.google.gson.annotations.SerializedName;

public class CheckDownloadItem {
    public String getPackageName() {
        return mPackageName;
    }

    public String getUrl() {
        return mUrl;
    }

    public String getLastVersion() {
        return mLastVersion;
    }

    @SerializedName("packageName")
    private String mPackageName;
    @SerializedName("url")
    private String mUrl;
    @SerializedName("lastVersion")
    private String mLastVersion;

}
