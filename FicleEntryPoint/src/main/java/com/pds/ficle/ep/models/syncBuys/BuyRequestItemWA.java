package com.pds.ficle.ep.models.syncBuys;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class BuyRequestItemWA {

    @SerializedName("buyOrigId")
    private long mId;
    @SerializedName("invoiceNumber")
    private String mInvoiceNumber;
    @SerializedName("providerId")
    private int mProviderId;
    @SerializedName("date")
    private String mDate;
    @SerializedName("subTotalX")
    private double mSubTotalX;
    @SerializedName("subTotalNg")
    private double mSubTotalNg;
    @SerializedName("subTotalI")
    private double mSubTotalI;
    @SerializedName("total")
    private double mTotal;
    @SerializedName("paymentWay")
    private String mPaymentWay;
    @SerializedName("code")
    private String mCode;
    @SerializedName("userId")
    private int mUserId;

    public BuyRequestItemWA()
    {
        super();
        this.mItems = new ArrayList<BuyDetailRequestItemWA>();
    }

    @SerializedName("buyDetails")
    public ArrayList<BuyDetailRequestItemWA> mItems;


    public void addDetail(BuyDetailRequestItemWA detail) {
        mItems.add(detail);
    }

    public void setId(long id) {
        this.mId = id;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.mInvoiceNumber = invoiceNumber;
    }

    public void setProviderId(int providerId) {
        this.mProviderId = providerId;
    }

    public void setDate(String date) {
        this.mDate = date;
    }

    public void setSubTotalX(double subTotalX) {
        this.mSubTotalX = subTotalX;
    }

    public void setSubTotalNG(double subTotalNG) {
        this.mSubTotalNg = subTotalNG;
    }

    public void setSubTotalI(double subTotalI) {
        this.mSubTotalI = subTotalI;
    }

    public void setTotal(double total) {
        this.mTotal = total;
    }

    public void setPaymentWay(String paymentWay) {
        this.mPaymentWay = paymentWay;
    }

    public void setCode(String code) {
        this.mCode = code;
    }

    public void setUserId(int userId) {
        this.mUserId = userId;
    }
}
