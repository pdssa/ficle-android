package com.pds.ficle.ep.controllers;

import android.content.Context;
import android.util.Log;

import com.pds.common.dao.DepartmentDao;
import com.pds.common.dao.ProductDao;
import com.pds.common.dao.SubDepartmentDao;
import com.pds.common.db.DepartmentTable;
import com.pds.common.db.ProductTable;
import com.pds.common.db.SubdepartmentTable;
import com.pds.common.model.Department;
import com.pds.common.model.Product;
import com.pds.common.model.SubDepartment;
import com.pds.ficle.ep.models.getProducts.GetProductsItem;

import java.util.Date;
import java.util.List;

public class ProductController {

    private static final String TAG = ProductController.class.getSimpleName();
    private Context mCtx = null;
    private DepartmentDao mDeptDao = null;
    private SubDepartmentDao mSubDeptDao = null;
    private ProductDao mProductDao = null;

    public ProductController(Context ctx)
    {
        mCtx = ctx;
        mDeptDao = new DepartmentDao(ctx.getContentResolver());
        mSubDeptDao = new SubDepartmentDao(ctx.getContentResolver());
        mProductDao = new ProductDao(ctx.getContentResolver());
    }

    public boolean anyProductByGtin(String gtin) {
        return getProdByGtin(gtin) != null;
    }

    public boolean insertProduct(GetProductsItem prod) {
        Product product = new Product();
        Department d = getDeptByName(prod.getDept());
        if(d == null) {
            Log.e(TAG, "insertProduct: No existe el departamento "+prod.getDept());
            return false;
        }
        SubDepartment sd = getSubDeptByName(prod.getSubDept(), prod.getDept());
        if(sd == null) {
            Log.e(TAG, "insertProduct: No existe el sub-departamento "+prod.getSubDept());
            return false;
        }

        product.setSubDepartment(sd);
        product.setDepartment(d);
        product.setSalePrice(prod.getPrice());
        product.setName(prod.getName());
        product.setAlta(new Date());
        product.setDescription("");
        product.setAutomatic(1);
        product.setBrand(prod.getBrand());
        product.setCode(prod.getGtin());
        product.setCodigoPadre(d.getCodigo());
        product.setContenido(prod.getContent());
        product.setIspCode(prod.getIspCode());
        product.setGeneric(false);
        product.setImage_uri("");
        product.setIvaEnabled(true);
        //FIXME: Debemos consultar otra forma de bajar el stock
        //product.setStock(prod.getStock());
        product.setMinStock(0);
        product.setIdProvider(-1);
        product.setIdTax(-1);
        product.setWeighable(false);
        product.setPurchasePrice(prod.getPurchasePrice());
        product.setQuickAccess(false);

        return mProductDao.save(product);
    }

    public boolean updateProduct(GetProductsItem prod) {

        Product p = getProdByGtin(prod.getGtin());
        if(p == null)
        {
            Log.e(TAG, "updateProduct: No se encuentra el product GTIN: "+prod.getGtin());
            return false;
        }
        else
        {
            Department d = getDeptByName(prod.getDept());
            if(d == null) {
                Log.e(TAG, "updateProduct: No existe el departamento "+prod.getDept());
                return false;
            }
            SubDepartment sd = getSubDeptByName(prod.getSubDept(), prod.getDept());
            if(sd == null) {
                Log.e(TAG, "updateProduct: No existe el sub-departamento "+prod.getSubDept());
                return false;
            }


            p.setName(prod.getName());
            p.setBrand(prod.getBrand());
            p.setIspCode(prod.getIspCode());
            p.setUnit(prod.getUnit());
            p.setContenido(prod.getContent());
            p.setCodigoPadre(prod.getDept());
            p.setDepartment(d);
            p.setStock(prod.getStock());    ///FIXME: HZ: COntrol de stock remoto
            p.setSubDepartment(sd);
            p.setPurchasePrice(prod.getPurchasePrice());
            p.setSalePrice(prod.getPrice());
            return mProductDao.update(p);

        }
    }

    private Product getProdByGtin(String gtin) {
        List<Product> result = mProductDao.list(ProductTable.COLUMN_CODE + "='" + gtin + "'", null, null);
        if(result.size() > 0)
        {
            return result.get(0);
        }
        else return null;
    }

    public boolean anyDeptByName(String dept) {
        return getDeptByName(dept) != null;
    }

    public boolean insertDept(String dept) {
        Department department = new Department();
        department.setName(dept);
        department.setCodigo(dept);
        department.setDescription("");
        department.setAlta(new Date());
        department.setAutomatic(1);
        department.setGeneric_dept(0);
        department.setVisualization_mode(0);

        return mDeptDao.save(department);
    }

    public boolean anySubDeptByName(String subDept, String dept) {
        return getSubDeptByName(subDept, dept) != null;
            
      
    }

    private Department getDeptByName(String dept) {
        List<Department> result = mDeptDao.list(DepartmentTable.COLUMN_NAME + "='" + dept + "'", null, null);
        if(result.size() > 0)
            return result.get(0);
        else return null;
    }

    private SubDepartment getSubDeptByName(String subDept, String dept) {

        Department deparment = getDeptByName(dept);
        if(deparment != null)
        {
            List<SubDepartment> result = mSubDeptDao.list(SubdepartmentTable.COLUMN_NAME + "='" + subDept + "' and "+SubdepartmentTable.COLUMN_DEPARTMENT_ID+"="+deparment.getId(), null, null);
            if(result.size() > 0)
                return result.get(0);
            else return null;
        }
        else
        {
            Log.e(TAG, "getSubDeptByName: No existe el departamento "+dept);
            return null;
        }

    }

    public boolean insertSubDept(String subDept, String dept) {
        Department deparment = getDeptByName(dept);
        if(deparment != null)
        {
            SubDepartment subd = new SubDepartment();
            subd.setAlta(new Date());
            subd.setDepartment(deparment);
            subd.setAutomatic(1);
            subd.setCodigo(subDept);
            subd.setCodigoPadre(dept);
            subd.setDescription("");
            subd.setGeneric_subdept(0);
            subd.setName(subDept);
            subd.setVisualization_mode(0);


            return mSubDeptDao.save(subd);
        }
        else
        {
            Log.e(TAG, "insertSubDept: No existe el departamento "+dept);
            return false;
        }
    }
}
