package com.pds.ficle.ep.cloud.process_entity_helpers;


import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.google.gson.Gson;
import com.pds.common.CajaHelper;
import com.pds.common.Config;
import com.pds.common.Logger;
import com.pds.ficle.ep.cloud.http_post_helpers.CajaHttpPostHelper;
import com.pds.ficle.ep.models.ApiResponse;
import com.pds.ficle.ep.models.syncCashBox.CashBoxRequestItemWA;
import com.pds.ficle.ep.models.syncCashBox.CashBoxRequestWA;
import com.pds.ficle.ep.models.syncCashBox.CashBoxResponseWA;
import com.pds.ficle.ep.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CajaProcessEntityHelper extends ProcessEntityHelper{

    public CajaProcessEntityHelper(Context ctx, Config config) {
        super(ctx, config);
    }

    @Override
    protected void updateEntitySyncState(JSONObject jsonObject) throws JSONException {
        ContentValues cv = new ContentValues();
        cv.put(CajaHelper.CAJA_ID, jsonObject.getInt(CajaHelper.CAJA_ID));
        cv.put(CajaHelper.CAJA_SYNC_STATUS, "S");

        Uri uri = ContentUris.withAppendedId(Uri.parse("content://com.pds.ficle.ep.caja.contentprovider/caja"), jsonObject.getInt(CajaHelper.CAJA_ID));

        mCtx.getContentResolver().update(uri, cv, null, null);
    }

    @Override
    protected boolean isSuccess(String response) {
        CashBoxResponseWA resp = new Gson().fromJson(response, CashBoxResponseWA.class);
        return resp.getResponseCode() == 0;
    }


    @Override
    protected String getEntityType() {
        return "CAJA";
    }

    @Override
    protected String getHttpPostDataToServer(String request) throws Exception {
        CajaHttpPostHelper helper = new CajaHttpPostHelper(mConfig);
        return helper.postDataToServer(request);
    }

    @Override
    protected String getJsonRequest() throws Exception {
        CashBoxRequestWA request = new CashBoxRequestWA();
        request.mAndroidId = mConfig.ANDROID_ID;
        request.mImei = mConfig.IMEI();
        request.mCashBox = new ArrayList<CashBoxRequestItemWA>();
        ArrayList<JSONObject> items = getPayload();
        if (items != null) {
            for (JSONObject item : items) {

                try {
                    CashBoxRequestItemWA cashBoxItem = new CashBoxRequestItemWA();
                    cashBoxItem.setId(item.getLong("_id"));

                    if(!item.get("acumulado").toString().isEmpty())
                        cashBoxItem.setAcumulado(item.getDouble("acumulado"));
                    else
                        cashBoxItem.setAcumulado(0.0);

                    try {
                        cashBoxItem.setFecha(Utils.Util_FechaHora(item.getString("fecha"),item.getString("hora")));
                    } catch (JSONException e) {
                        cashBoxItem.setFecha(Utils.Util_FechaHoraTerminal());
                    }

                    /*if(!item.get("acumuladoEfectivo").toString().isEmpty())
                        cashBoxItem.setAcumuladoEfectivo(item.getDouble("acumuladoEfectivo"));
                    else*/
                    cashBoxItem.setAcumuladoEfectivo(0.0);

                    cashBoxItem.setMonto(item.getDouble("monto"));
                    cashBoxItem.setDescVenta(item.getString("descVenta"));
                    cashBoxItem.setTipoVta(item.getString("tipoVta"));
                    if(!item.get("idVenta").toString().isEmpty())
                        cashBoxItem.setIdVenta(item.getLong("idVenta"));
                    cashBoxItem.setMedioPago(item.getString("medioPago"));
//                    cashBoxItem.setIdMotiv(item.getString("idMotivo"));
                    cashBoxItem.setTipoMov(item.getLong("tipoMov"));
                    if(!item.get("observacion").toString().isEmpty())
                        cashBoxItem.setObservacion(item.getString("observacion"));

                    request.mCashBox.add(cashBoxItem);

                }catch (Exception ex){
                    Log.e("error", ex.getMessage() + "" + ex.toString(), ex);
                    Logger.RegistrarEvento(mCtx, "e", "SyncProcess", ex.getMessage() + "" + ex.toString(), "Ent:" + getEntityType());
                }
            }
            return new Gson().toJson(request);
        }
        return  null;
    }

    @Override
    protected ArrayList<JSONObject> getPayload() throws Exception {
        Cursor caja_cursor = getCursor(
                "content://com.pds.ficle.ep.caja.contentprovider/caja",
                CajaHelper.columnas,
                CajaHelper.CAJA_SYNC_STATUS + "= 'N' ",
                "");
        return  cursorToJSONWebApi(caja_cursor);
    }


}
