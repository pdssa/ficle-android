package com.pds.ficle.ep.models;

public abstract class ApiResponse {

    public int responseCode;
    public String responseMessage;

    public ApiResponse() {
    }

    protected ApiResponse(String responseMessage) {
        this.responseMessage = responseMessage;
        this.responseCode = -1;  ///MAURO 18-02: Cambie esto de 0 a -1. si pasas un mensaje de error en el ctor
        // es porque hubo algun error (entonces el codigo al menos debe ser diferente de 0)
    }

    protected ApiResponse(String responseMessage, int responseCode) {
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
    }

    public int getResponseCode() {
        return this.responseCode;
    }

    public String getResponseMessage() {
        return this.responseMessage;
    }

    public boolean hasError() {
        return this.responseCode == 0 ? false : true;
    }

}


