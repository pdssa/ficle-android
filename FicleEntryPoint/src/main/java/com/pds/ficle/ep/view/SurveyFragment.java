package com.pds.ficle.ep.view;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.pds.ficle.ep.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SurveyFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SurveyFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnSurveyListener mListener;

    public SurveyFragment() {
        // Required empty public constructor
    }

    public void setOnSurveyListener(OnSurveyListener mListener) {
        this.mListener = mListener;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SurveyFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SurveyFragment newInstance(String param1, String param2) {
        SurveyFragment fragment = new SurveyFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    private WebView surveyView;
    private ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_survey, container, false);

        surveyView = (WebView)view.findViewById(R.id.wv_survey_webview);
        progressBar = (ProgressBar) view.findViewById(R.id.prg_survey_progress);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        LoadSurvey();

    }

    private void LoadSurvey(){
        // Configure related browser settings
        WebSettings webSettings = surveyView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setLoadsImagesAutomatically(true);

        surveyView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        String html = "<html><head>";
        html += "</head><body>";
        html += "<center>";
        html += "<script>(function(t,e,s,n){var o,c,a;t.SMCX=t.SMCX||[],e.getElementById(n)||(o=e.getElementsByTagName(s),c=o[o.length-1],a=e.createElement(s),a.type='text/javascript',a.async=!0,a.id=n,a.src=['https:'===location.protocol?'https://':'http://','widget.surveymonkey.com/collect/website/js/UMpOGlZDYtjB1KdvUNhAfsGyTNXBf0IFTEa3BsVZxOEORu9HhmdKqyXbbYC8N4Fs.js'].join(''),c.parentNode.insertBefore(a,c))})(window,document,'script','smcx-sdk');</script>";
        html += "</center></body></html>";

        //final ProgressDialog dialog = new ProgressDialog(getActivity());

        surveyView.setWebViewClient(new WebViewClient() {

            // cierra el "cargando" al finalizar
            /*@Override
            public void onPageFinished(WebView view, String url) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }*/

            // evita que los enlaces se abran fuera nuestra app en el navegador de android
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                if(url.toLowerCase().contains("survey-thanks") || url.toLowerCase().contains("survey-taken"))
                    Cerrar();

                return false;
            }
        });

        /*dialog.setMessage("Aguarde por favor...");
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();*/


        surveyView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int progress) {
                progressBar.setProgress(0);
                progressBar.setVisibility(View.VISIBLE);
                getActivity().setProgress(progress * 1000);

                progressBar.incrementProgressBy(progress);

                if (progress == 100) {
                    progressBar.setVisibility(View.GONE);
                }
            }
        });

        //surveyView.loadDataWithBaseURL("file:///android_asset/", html, "text/html", "utf-8", null);
        //surveyView.loadData(html, "text/html", "utf-8");
        surveyView.loadUrl("https://es.surveymonkey.com/r/F9HKF5R");
    }

    @Override
    public void onDetach() {

        //surveyView.getInstance().deleteAllData();
        surveyView.destroy();

        super.onDetach();
    }

    private void Cerrar() {

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if(mListener != null){
            mListener.onSurveyFinished();
        }

        /*FragmentManager fm = getActivity().getFragmentManager();

        Fragment fragment = fm.findFragmentById(R.id.ep_fragment);

        FragmentTransaction ft = fm.beginTransaction();

        ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);

        ft.hide(fragment);

        ft.commit();*/
    }

    public interface OnSurveyListener {
        public void onSurveyFinished();
    }
}
