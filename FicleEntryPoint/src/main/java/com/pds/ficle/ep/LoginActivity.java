package com.pds.ficle.ep;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Logger;
import com.pds.common.Usuario;
import com.pds.common.UsuarioAdapter;
import com.pds.common.UsuarioHelper;
import com.pds.common.services.PingServiceIntent;
import com.pds.common.util.Window;
import com.pds.ficle.ep.cloud.AppUpgrade;
import com.pds.ficle.ep.cloud.CheckDownloadTask;
import com.pds.ficle.ep.utils.PassGenerator;
import com.pds.common.util.Perfiles;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Activity which displays a login screen to the user, offering registration as
 * well.
 */
public class LoginActivity extends Activity {

    private UserLoginTask mAuthTask = null;

    // Values for email and password at the time of the login attempt.
    private String mUser;
    private String mPassword;

    // UI references.
    private Spinner mUserView;
    //private EditText mUserView;
    private EditText mPasswordView;
    private View mLoginFormView;
    private View mLoginStatusView;
    private TextView mLoginStatusMessageView;

    Usuario user = null;

    List<Usuario> list_all_users;

    private static long first_back_pressed_time = 0;
    private static int back_pressed_times = 0;
    private final static int EXIT_TIMES = 5;
    private final static long EXIT_TIME = 2000; //2 segundos
    private final static String EXIT_PWD = "1111";

    /*
    @Override
    protected void onStart() {
        super.onStart();

        // Save to shared preferences
        SharedPreferences.Editor editor = getSharedPreferences("com.pds.ficle.ep",MODE_PRIVATE).edit();
        editor.putBoolean("alert_no_conn_not_show", false); //reseteamos el estado, para mostrar
        editor.putBoolean("activity_no_conn_visible", false);
        editor.apply();
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);

            setContentView(R.layout.activity_login);

            ((TextView) findViewById(R.id.txtVersion)).setText("Version:" + getPackageManager().getPackageInfo(this.getPackageName().toString(), 0).versionName);

            if (getIntent().hasExtra("ACTION_EXEC")) {
                String action = getIntent().getStringExtra("ACTION_EXEC");

                startActivity(new Intent(action));
            }

            //updateDeviceInfo();

            //Remove notification bar
            //this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

            // Set up the login form.
            mUserView = (Spinner) findViewById(R.id.spn_users);
            //mUser = getIntent().getStringExtra(EXTRA_EMAIL);
            //mUserView = (EditText) findViewById(R.id.user);
            //mUserView.setText(mUser);

            mPasswordView = (EditText) findViewById(R.id.password);
            mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                    if (id == EditorInfo.IME_ACTION_DONE) {
                        attemptLogin();
                        return true;
                    }
                    return false;
                }
            });
            mPasswordView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.length() == 4) {
                        attemptLogin();//despues de 4 caracteres
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            mLoginFormView = findViewById(R.id.login_form);
            mLoginStatusView = findViewById(R.id.login_status);
            mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);

            findViewById(R.id.sign_in_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    attemptLogin();
                }
            });

            list_all_users = new UsuarioHelper(this).getAllList();
            mUserView.setAdapter(new UsuarioAdapter(this, list_all_users, android.R.layout.simple_spinner_dropdown_item, true));

            findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
        } catch (Exception ex) {
            Toast.makeText(this, ex.toString(), Toast.LENGTH_LONG).show();
        }
    }

    /*private void updateDeviceInfo(){
        Config cfg = new Config(this);
        updateDeviceInfo(cfg.ANDROID_ID, cfg.IMEI());
    }
    private void updateDeviceInfo(String ID, String IMEI){

        ((TextView) findViewById(R.id.txtSerie)).setText(String.format("Serie: %s - IMEI: %s", ID, IMEI));
    }*/

    @Override
    protected void onResume() {
        super.onResume();

        Window.CloseSoftKeyboard(LoginActivity.this);

        AjustaPerfilComercio();

        new InstallUpdatesTask(LoginActivity.this).checkAndInstall(new InstallUpdatesListener() {
            @Override
            public void onFinished() {
                //focus y teclado en password
                /*mPasswordView.requestFocus();
                mPasswordView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        keyboard.showSoftInput(mPasswordView, 0);
                    }
                }, 200);*/

                Window.FocusViewShowSoftKeyboard(LoginActivity.this, mPasswordView);


                //((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(mPasswordView, InputMethodManager.SHOW_FORCED);
            }
        });
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) { //solo consideramos la tecla back, el resto de las teclas son omitidas
        return keyCode == KeyEvent.KEYCODE_BACK ? super.onKeyDown(keyCode, event) : true;
    }

    @Override
    public void onBackPressed() {
        //RUTINA PARA SALIR DEL MODO KIOSCO
        long now = System.currentTimeMillis();

        if (now - first_back_pressed_time > EXIT_TIME) {
            first_back_pressed_time = now; //reiniciamos la secuencia
            back_pressed_times = 1;
        } else {
            back_pressed_times++; //continuamos la secuencia
            if (back_pressed_times == EXIT_TIMES)
                SolicitarClaveSalida(); //finalizamos la secuencia
        }
    }

    public void finalizar() {
        try {
            Intent i = new Intent();
            i.setAction("android.intent.action.Homesample");
            //i.setClassName("com.example.android.home", ".Home");
            //i.setComponent(new ComponentName("com.example.android.home", ".Home"));

            finish();

            startActivity(i);
        } catch (Exception ex) {
            Toast.makeText(LoginActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void checkClaveSalida(String passIngresada, String IMEI, String ID) {
        try {

            if (passIngresada.equals(EXIT_PWD)) {
                //salimos
                finalizar();
            } else if (CheckConfigPassword(passIngresada, IMEI, ID)) {
                //vemos si es la clave tecnica
                //mostramos un cuadro de opciones de acceso tecnico

                String[] opciones = new String[]{"RESET CLAVE ADMIN", "CLONACIÓN - DESTINO"};

                AlertDialog.Builder adb = new AlertDialog.Builder(LoginActivity.this);
                adb.setTitle("ACCESO TECNICO");

                adb.setItems(opciones, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Toast.makeText(LoginActivity.this, String.valueOf(which), Toast.LENGTH_SHORT).show();

                        switch (which) {
                            case 0: {
                                //accedemos al edit de user admin
                                /*Intent i = new Intent(LoginActivity.this, user_abm_activity.class);
                                i.putExtra("modo_edit", true);
                                i.putExtra("_id", 1);//user admin
                                startActivity(i);*/
                                ResetAdminPass();
                            }
                            break;
                            case 1: {
                                //accedemos a la opcion de clonacion
                                Intent i = new Intent(LoginActivity.this, MigrationActivity.class);
                                i.putExtra("OPERACION", 1);//DESTINO
                                startActivity(i);
                            }
                            break;
                        }
                    }
                });

                adb.setIcon(android.R.drawable.ic_lock_idle_lock);
                adb.setCancelable(true);
                adb.setNegativeButton("CANCELAR", null);

                adb.show();
            } else {
                Toast.makeText(LoginActivity.this, "Clave ingresada erronea", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(LoginActivity.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void ResetAdminPass() {
        try {
            UsuarioHelper userHelper = new UsuarioHelper(this);
            Usuario _user = userHelper.getById(this, 1);//user admin
            _user.setPassword("1234");//reset pass

            if (userHelper.update(_user)) {
                AlertMessage("La clave de admin se ha reiniciado a 1234");
            } else
                AlertMessage("No se pudo reiniciar la clave de administrador");

        } catch (Exception e) {
            AlertMessage("Error: " + e.getMessage());
        }
    }

    private void AlertMessage(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setTitle("RESET CLAVE ADMIN");
        builder.setMessage(message);
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setCancelable(false);
        builder.show();
    }

    private void SolicitarClaveSalida() {

        final Config _config = new Config(LoginActivity.this);

        //vamos a solicitar la clave de tecnico para ingresar a la edicion de la configuracion
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        // Add action buttons
        builder
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        String passIngresada = ((EditText) ((AlertDialog) dialog).findViewById(R.id.dialog_clave_config_txt_pass)).getText().toString();

                        checkClaveSalida(passIngresada, _config.IMEI(), _config.ANDROID_ID);
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

        final AlertDialog dialog = builder.create();

        // Get the layout inflater
        LayoutInflater inflater = LoginActivity.this.getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View vw = inflater.inflate(R.layout.dialog_clave_config, null);

        ((TextView) vw.findViewById(R.id.dialog_clave_config_txt_id)).setText(_config.ANDROID_ID);
        ((TextView) vw.findViewById(R.id.dialog_clave_config_txt_imei)).setText(_config.IMEI());
        ((EditText) vw.findViewById(R.id.dialog_clave_config_txt_pass)).setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    checkClaveSalida(v.getText().toString(), _config.IMEI(), _config.ANDROID_ID);
                    dialog.dismiss();
                    return true;
                }
                return false;
            }
        });
        ((EditText) vw.findViewById(R.id.dialog_clave_config_txt_pass)).setHint(R.string.hint_ing_clave_salida);

        dialog.setView(vw);
        dialog.setTitle("Ingrese Clave para salir");
        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
            }
        });

        dialog.show();

        //---------------------------------------------------------------
    }

    private boolean CheckConfigPassword(String passIngresada, String IMEI, String ID) {
        try {

            String claveTecnico = PassGenerator.GenerarClaveTecnico(new Date(), IMEI, ID);

            //updateDeviceInfo(_config.ANDROID_ID, _config.IMEI());

            return passIngresada.equals(claveTecnico);
        } catch (Exception e) {
            Toast.makeText(LoginActivity.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
        return false;
    }

    private void AjustaPerfilComercio() {
        Config config = new Config(LoginActivity.this);
        if (config.PERFIL.equals(Perfiles.PERFIL_JJD)) {
            findViewById(R.id.login_layout).setBackgroundResource(R.drawable.puntototal);
        } else if (config.PERFIL.equals(Perfiles.PERFIL_DEMO_POWAPOS)) {
            findViewById(R.id.login_layout).setBackgroundResource(R.drawable.fondo_powa);
        } else if (config.PERFIL.equals(Perfiles.PERFIL_IQCORP)) {
            findViewById(R.id.login_layout).setBackgroundResource(R.drawable.fondo_iqcorp);
        } else if (config.PERFIL.equals(Perfiles.PERFIL_PILOTO_EMBONOR)) {
            findViewById(R.id.login_layout).setBackgroundResource(R.drawable.fondo_embonor);
        } else if (config.PERFIL.equals(Perfiles.PERFIL_DEMO_CCU)) {
            findViewById(R.id.login_layout).setBackgroundResource(R.drawable.fondo_ccu);
        } else if (config.PERFIL.equals(Perfiles.PERFIL_DEMO_AL_GRAMO)) {
            findViewById(R.id.login_layout).setBackgroundResource(R.drawable.fondo_al_gramo);
        } else if (config.PERFIL.equals(Perfiles.PERFIL_DEMO_ALMACENES_CHILE)) {
            findViewById(R.id.login_layout).setBackgroundResource(R.drawable.fondo_almacenes);
        } else {
            findViewById(R.id.login_layout).setBackgroundResource(R.drawable.fondo_bpos);
        }
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        mUser = ((Usuario) mUserView.getSelectedItem()).getLogin();
        mPassword = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password.
        if (TextUtils.isEmpty(mPassword)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        } else if (mPassword.length() != 4) {
            mPasswordView.setError(getString(R.string.error_short_password));
            focusView = mPasswordView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.

            //ocultamos el teclado
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);

            //mostramos el spinner de login...
            mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
            showProgress(true);
            mAuthTask = new UserLoginTask();
            mAuthTask.execute((Void) null);
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginStatusView.setVisibility(View.VISIBLE);
            mLoginStatusView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(show ? 1 : 0)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
                        }
                    });

            mLoginFormView.setVisibility(View.VISIBLE);
            mLoginFormView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(show ? 0 : 1)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                        }
                    });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                // Simulate network access.
                Thread.sleep(1500);

                user = (new UsuarioHelper(getApplicationContext()).getByUserName(mUser));

                //int result =  user.getPassword().equals(mPassword) ? 1 : 0;
                return user.getPassword().equals(mPassword);

                //si es el admin, y no ingresó correctamente su clave, entonces probamos usandola como clave de
                //configuración del equipo, si está OK, dejamos entrar
                //if(result == 0 && mUser.equals("admin") )
                //    result = CheckConfigPassword(mPassword) ? 2 : 0;

                //return result;

            } catch (InterruptedException e) {
                return false;
            } catch (Exception e) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                //finish();
                Toast.makeText(getApplicationContext(), "Bienvenido " + user.getNombre() + "!", Toast.LENGTH_SHORT).show();
                finish();
                Logger.RegistrarEvento(getApplicationContext(), "i", "Login", "User: " + user.getNombre());

                new PingServiceIntent(true).send(getApplicationContext());

                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                //i.putExtra("_id_user_login",user.getId());
                i.putExtra("current_user", user);
                i.putExtra("sourceActivity", "LoginActivity");
                startActivity(i);
            } else {
                mPasswordView.setError(getString(R.string.error_incorrect_password));
                mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }


    }

    public interface InstallUpdatesListener {
        void onFinished();
    }

    public class InstallUpdatesTask extends AsyncTask<Void, String, Boolean> {

        private List<AppUpgrade> appsToInstall = new ArrayList<AppUpgrade>();
        private InstallUpdatesListener installUpdatesListener;
        private Context context;

        public InstallUpdatesTask(Context context) {
            this.context = context;
        }

        public void checkAndInstall(InstallUpdatesListener installUpdatesListener) {

            this.installUpdatesListener = installUpdatesListener;

            try {
                SharedPreferences pref = context.getSharedPreferences("com.pds.ficle.ep", Context.MODE_PRIVATE);
                String appsToInstallXML = pref.getString("apps_to_install", "");

                if(!TextUtils.isEmpty(appsToInstallXML)){
                    List<AppUpgrade> lstApps = CheckDownloadTask.ParseXML(appsToInstallXML);

                    for(AppUpgrade app : lstApps){
                        app.set_context(context);
                        app.set_localPath(app.get_url());//tomamos el path que el servicio descarga dejó en url
                        if(app.CorrespondeActualizar()){
                            Log.i("InstallUpdatesTask", "app to update: " + app.get_packageName());

                            appsToInstall.add(app);
                        }
                    }
                }


                /*String path = Environment.getExternalStorageDirectory() + CheckDownloadTask.downloadFolder;
                File downloadFolder = new File(path); // PATH = /mnt/sdcard/download/pds
                if (downloadFolder.exists()) {
                    for (File file : downloadFolder.listFiles()) {
                        if (file.isFile()) {
                            if (!file.getName().endsWith(".temp")) {
                                Log.i("InstallUpdatesTask", "apk found: " + file.getName());

                                appsToInstall.add(new AppUpgrade(file.getName(), file.getName(), file.getAbsolutePath(), context));
                            }
                        }
                    }
                }*/

            } catch (Exception ex) {
                Log.e("InstallUpdatesTask", "checkAndInstall", ex);
            }

            /*try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/

            if (!appsToInstall.isEmpty()) {
                execute();
            } else {
                installUpdatesListener.onFinished();
            }
        }

        @Override
        protected void onPreExecute() {
            mLoginStatusMessageView.setText(Html.fromHtml("<center>Instalando actualizaciones<br/>Por favor no apague el equipo</center>"));
            showProgress(true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {

                //buscamos si está el EntryPoint, para instalarlo al final
                AppUpgrade entryPointApp = null;
                for (AppUpgrade appToInstall : appsToInstall) {

                    if(appToInstall.isEntryPoint()){
                        entryPointApp = appToInstall;
                    }
                    else{
                        publishProgress("<center>Actualizando " + appToInstall.get_packageName() + "<br/>Por favor no apague el equipo</center>");

                        Logger.RegistrarEvento(this.context, "i", "Install: ", appToInstall.get_fileName());

                        //instalamos
                        appToInstall.Install();

                        Log.i("InstallUpdatesTask", "apk installed: " + appToInstall.get_fileName());
                    }

                }

                //instalamos el EP
                if (entryPointApp != null) {
                    publishProgress("<center>Actualizando " + entryPointApp.get_packageName() + "<br/>Por favor no apague el equipo</center>");

                    Logger.RegistrarEvento(this.context, "i", "Install: ", entryPointApp.get_fileName());

                    //instalamos
                    entryPointApp.Install();

                    Log.i("InstallUpdatesTask", "apk installed: " + entryPointApp.get_fileName());
                }


                //buscamos si está el EntryPoint, para instalarlo primero
                /*AppUpgrade entryPointApp = null;
                for (AppUpgrade appToInstall : appsToInstall) {
                    if (appToInstall.isEntryPoint()) {
                        entryPointApp = appToInstall;
                        break;
                    }
                }

                if (entryPointApp != null) {
                    appsToInstall.remove(entryPointApp);

                    if (entryPointApp.needToBeRegistered() && !entryPointApp.wasInstalledBefore()) {
                        if (entryPointApp.registerBeforeInstall()) {
                            publishProgress("<center>Actualizando " + entryPointApp.get_fileName() + "<br/>Por favor no apague el equipo</center>");

                            Logger.RegistrarEvento(this.context, "i", "Install: ", entryPointApp.get_fileName());

                            //instalamos
                            entryPointApp.Install();

                            Log.i("InstallUpdatesTask", "apk installed: " + entryPointApp.get_fileName());
                        }
                    }

                }

                for (AppUpgrade appToInstall : appsToInstall) {
                    publishProgress("<center>Actualizando " + appToInstall.get_fileName() + "<br/>Por favor no apague el equipo</center>");

                    Logger.RegistrarEvento(this.context, "i", "Install: ", appToInstall.get_fileName());

                    //instalamos
                    appToInstall.Install();

                    Log.i("InstallUpdatesTask", "apk installed: " + appToInstall.get_fileName());
                }*/

            } catch (Exception ex) {
                Log.e("InstallUpdatesTask", "checkAndInstall", ex);
            }

            return null;
        }


        @Override
        protected void onCancelled() {
            showProgress(false);

            this.installUpdatesListener.onFinished();
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            showProgress(false);

            this.installUpdatesListener.onFinished();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            mLoginStatusMessageView.setText(Html.fromHtml(values[0]));
        }
    }
}
