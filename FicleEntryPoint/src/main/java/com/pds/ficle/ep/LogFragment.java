package com.pds.ficle.ep;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.LogHelper;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class LogFragment extends Fragment {


    public LogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_log, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getView().findViewById(R.id.imageView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentManager fm = getActivity().getFragmentManager();

                Fragment fragment = fm.findFragmentById(R.id.frg_log);

                FragmentTransaction ft = fm.beginTransaction();
                if (fragment != null) {
                    //cerramos el fragment
                    fm.beginTransaction()
                            .remove(fragment)
                            .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                            .commit();
                }
                //ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
                //ft.hide(fragment);
                //ft.commit();
            }
        });

        CargamosLogs(this.getActivity());

    }

    public void CargamosLogs(Context context) {
        try {

            Cursor logs_cursor = context.getContentResolver().query(
                    Uri.parse("content://com.pds.ficle.ep.logs.contentprovider/logs"),
                    LogHelper.columnas,
                    "",
                    new String[]{},
                    LogHelper.LOG_ID + " DESC");

            List<LogRow> logs_list = new ArrayList<LogRow>();

            // recorremos los items
            if (logs_cursor.moveToFirst()) {
                do {

                    logs_list.add(new LogRow(
                            logs_cursor.getInt(LogHelper.LOG_IX_ID),
                            logs_cursor.getString(LogHelper.LOG_IX_FECHA),
                            logs_cursor.getString(LogHelper.LOG_IX_HORA),
                            logs_cursor.getString(LogHelper.LOG_IX_TITULO),
                            logs_cursor.getString(LogHelper.LOG_IX_DESCRIPCION),
                            logs_cursor.getString(LogHelper.LOG_IX_DESCRIPCION2),
                            logs_cursor.getString(LogHelper.LOG_IX_TIPO)
                    ));

                } while (logs_cursor.moveToNext());
            }

            ListView lstLogs = (ListView) this.getView().findViewById(R.id.ep_activity_main_lst_logs);
            lstLogs.setAdapter(new LogAdapter(context, logs_list));

            logs_cursor.close();

        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_LONG).show();
        }

    }


    class LogRow {
        private int _id;
        private String _fecha;
        private String _hora;
        private String _titulo;
        private String _descripcion;
        private String _descripcion2;
        private String _tipo;

        public String get_descripcion() {
            return _descripcion;
        }

        public String get_fecha() {
            return _fecha;
        }

        public int get_id() {
            return _id;
        }

        public String get_hora() {
            return _hora;
        }

        public String get_tipo() {
            return _tipo;
        }

        public String get_titulo() {
            return _titulo;
        }

        public String get_descripcion2() {
            return _descripcion2;
        }

        public LogRow(int id, String fecha, String hora, String titulo, String descripcion, String descripcion2, String tipo) {
            this._id = id;
            this._fecha = fecha;
            this._hora = hora;
            this._titulo = titulo;
            this._descripcion = descripcion;
            this._descripcion2 = descripcion2;
            this._tipo = tipo;
        }
    }


    class LogAdapter extends ArrayAdapter<LogRow> {
        private Context context;
        private List<LogRow> datos;

        public LogAdapter(Context context, List<LogRow> datos) {
            super(context, android.R.layout.simple_list_item_1, datos);

            this.context = context;
            this.datos = datos;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // En primer lugar "inflamos" una nueva vista, que será la que se
            // mostrará en la celda del ListView. Para ello primero creamos el
            // inflater, y después inflamos la vista.
            LayoutInflater inflater = LayoutInflater.from(context);
            View item = inflater.inflate(R.layout.log_item, null);

            LogRow row = datos.get(position);

            // A partir de la vista, recogeremos los controles que contiene para
            // poder manipularlos.

            // Recogemos los TextView para mostrar datos
            ((TextView) item.findViewById(R.id.log_item_titulo)).setText("#" + row.get_id() + " (" + row.get_tipo() + ") : " + row.get_titulo());
            ((TextView) item.findViewById(R.id.log_item_fecha)).setText(row.get_fecha() + " " + row.get_hora());
            ((TextView) item.findViewById(R.id.log_item_detalle)).setText(row.get_descripcion());
            if (!row.get_descripcion2().equals("")) {
                ((TextView) item.findViewById(R.id.log_item_detalle2)).setText(row.get_descripcion2());
                ((TextView) item.findViewById(R.id.log_item_detalle2)).setVisibility(View.VISIBLE);
            } else {
                ((TextView) item.findViewById(R.id.log_item_detalle2)).setVisibility(View.GONE);
            }
            // Devolvemos la vista para que se muestre en el ListView.
            return item;

        }
    }

}
