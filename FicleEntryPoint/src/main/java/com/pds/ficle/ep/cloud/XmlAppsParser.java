package com.pds.ficle.ep.cloud;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Hernan on 10/06/2014.
 */
public class XmlAppsParser {
    // sin namespaces
    private static final String ns = null;

    public ArrayList<AppUpgrade> parse(InputStream in) throws XmlPullParserException, Exception {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readXML(parser);
        }
        catch (Exception ex){
            throw ex;
        }
        finally {
            in.close();
        }
    }

    private ArrayList<AppUpgrade> readXML(XmlPullParser parser) throws XmlPullParserException, IOException {

        ArrayList<AppUpgrade> versiones = new ArrayList<AppUpgrade>();
        AppUpgrade version=new AppUpgrade();
        String text="";

        // Iniciamos buscando el tag de inicio
        parser.require(XmlPullParser.START_TAG, ns, "versiones");
        String tag = parser.getName();

        if (tag.equals("versiones")) {

            int eventType = parser.getEventType();
            while(eventType != XmlPullParser.END_DOCUMENT)
            {
                String tagname = parser.getName();

                switch (eventType) {
                    case XmlPullParser.START_TAG: {
                        if(tagname.equalsIgnoreCase("VERSIONsAll")) {
                            version = new AppUpgrade();
                        }
                    }break;
                    case XmlPullParser.TEXT: {
                        text = parser.getText();
                    }break;
                    case XmlPullParser.END_TAG: {
                        if(tagname.equalsIgnoreCase("VERSIONsAll")) {
                            versiones.add(version);
                        }
                        else if(tagname.equalsIgnoreCase("versionPackage")) {
                            version.set_packageName(text);
                        }
                        else if(tagname.equalsIgnoreCase("versionVersion")) {
                            version.set_packageLastVersion(text) ;
                        }
                        else if(tagname.equalsIgnoreCase("versionApkUrl")) {
                            version.set_url(text) ;
                        }
                    }break;
                    default:
                        break;

                }

                eventType = parser.next();
            }
        }
        return versiones;
    }
}
