package com.pds.ficle.ep.models.syncCashBox;


import com.google.gson.annotations.SerializedName;

public class CashBoxRequestItemWA {


    @SerializedName("Id")
    long id;

    @SerializedName("Date")
    String fecha;

    @SerializedName("Amount")
    double monto;

    @SerializedName("SaleId")
    long idVenta;

    @SerializedName("Description")
    String descVenta;

    @SerializedName("PaymentMethod")
    String medioPago;

    @SerializedName("SaleType")
    String   tipoVta;

    @SerializedName("ReasonId")
    String idMotiv ;

    @SerializedName("TotalAccum")
    double acumulado;

    @SerializedName("CashAccum")
    double acumuladoEfectivo;

    @SerializedName("MovementType")
    long  tipoMov;

    @SerializedName("Observation")
    String  observacion;


    public CashBoxRequestItemWA() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTipoMov() {
        return tipoMov;
    }

    public void setTipoMov(long tipoMov) {
        this.tipoMov = tipoMov;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public double getAcumulado() {
        return acumulado;
    }

    public void setAcumulado(double acumulado) {
        this.acumulado = acumulado;
    }

    public double getAcumuladoEfectivo() {
        return acumuladoEfectivo;
    }

    public void setAcumuladoEfectivo(double acumuladoEfectivo) {
        this.acumuladoEfectivo = acumuladoEfectivo;
    }

    public long getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(long idVenta) {
        this.idVenta = idVenta;
    }

    public String getTipoVta() {
        return tipoVta;
    }

    public void setTipoVta(String tipoVta) {
        this.tipoVta = tipoVta;
    }

    public String getDescVenta() {
        return descVenta;
    }

    public void setDescVenta(String descVenta) {
        this.descVenta = descVenta;
    }

    public String getMedioPago() {
        return medioPago;
    }

    public void setMedioPago(String medioPago) {
        this.medioPago = medioPago;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public String getIdMotiv() {
        return idMotiv;
    }

    public void setIdMotiv(String idMotiv) {
        this.idMotiv = idMotiv;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
}

