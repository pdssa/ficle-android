package com.pds.ficle.ep.models.syncCashBox;

import com.google.gson.annotations.SerializedName;
import com.pds.ficle.ep.models.BaseFicleRequestWA;

import java.util.ArrayList;

public class CashBoxRequestWA extends BaseFicleRequestWA {
    @SerializedName("CashBox")
    public ArrayList<CashBoxRequestItemWA> mCashBox;
}
