package com.pds.ficle.ep.models.syncBuys;

import com.google.gson.annotations.SerializedName;

public class BuyDetailRequestItemWA  {

    @SerializedName("_id")
    private int mId;
    @SerializedName("buyOrigId")
    private int mOrigId;
    @SerializedName("productId")
    private int mProductId;
    @SerializedName("quantity")
    private int mQuantity;
    @SerializedName("price")
    private double mPrice;
    @SerializedName("total")
    private double mTotal;
    @SerializedName("subTotalI")
    private double mSubTotalI;
    @SerializedName("subTotalNg")
    private double mSubTotalNg;
    @SerializedName("subTotalX")
    private double mSubTotalX;
    @SerializedName("Gtin")
    private String mGtin;

    public void setId(int id) {
        this.mId = id;
    }

    public void setOrigId(int origId) {
        this.mOrigId = origId;
    }

    public void setProductId(int productId) {
        this.mProductId = productId;
    }

    public void setQuantity(int cantidad) {
        this.mQuantity = cantidad;
    }

    public void setPrice(double precio) {
        this.mPrice = precio;
    }

    public void setTotal(double total) {
        this.mTotal = total;
    }

    public void setSubTotalI(double subTotalI) {
        this.mSubTotalI = subTotalI;
    }

    public void setSubTotalNG(double subTotalNG) {
        this.mSubTotalNg = subTotalNG;
    }

    public void setSubTotalX(double subTotalX) {
        this.mSubTotalX = subTotalX;
    }

    public void setGtin(String gtin) {
        this.mGtin = gtin;
    }
}
