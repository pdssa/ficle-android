package com.pds.ficle.ep.models.getMerchantInfo;

import com.google.gson.annotations.SerializedName;
import com.pds.ficle.ep.models.ApiResponse;

public class GetMerchantInfoResponse extends ApiResponse {
    @SerializedName("merchant")
    private String mMerchant;
    @SerializedName("name")
    private String mName;
    @SerializedName("fiscalKey")
    private String mFiscalKey;
    @SerializedName("address")
    private String mAddress;

    @SerializedName("profile")
    private String mProfile;
    @SerializedName("merchantId")
    private String mMerchantId;
    @SerializedName("commerceCode")
    private String mMerchantCode;
    @SerializedName("valeConfig")
    private ValeConfigDto mValeConfig;
    @SerializedName("checkUpdatesFlags")
    private int mCheckUpdatesFlag;


    public String getMerchant() {
        return mMerchant;
    }

    public String getName() {
        return mName;
    }

    public String getFiscalKey() {
        return mFiscalKey;
    }

    public String getAddress() {
        return mAddress;
    }


    public String getProfile() {
        return mProfile;
    }

    public String getMerchantId() {
        return mMerchantId;
    }

    public ValeConfigDto getValeConfig() {
        return mValeConfig;
    }

    public int getCheckUpdatesFlag() {
        return mCheckUpdatesFlag;
    }


    public String getMerchantCode() {
        return mMerchantCode;
    }
}
