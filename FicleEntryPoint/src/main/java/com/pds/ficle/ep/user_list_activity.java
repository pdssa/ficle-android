package com.pds.ficle.ep;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Usuario;
import com.pds.common.UsuarioAdapter;
import com.pds.common.UsuarioHelper;
import com.pds.common.activity.TimerActivity;

import java.util.List;

public class user_list_activity extends TimerActivity {
    private List<Usuario> usuarios_list;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {

            super.onCreate(savedInstanceState);
            setContentView(R.layout.user_list);

            Button btnGrabar = (Button) findViewById(R.id.user_list_btn_nuevo);
            btnGrabar.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(getApplicationContext(), user_abm_activity.class);
                    startActivity(i);
                }
            });

            Button btnCancelar = (Button) findViewById(R.id.user_list_btn_cancelar);
            btnCancelar.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish(); //cancela la actividad actual y retorna a la anterior
                }
            });

            // migramos el metodo al evento onResume() para refrescar la lista tanto al crear como al retornar
            // usuarios_list = new UsuarioHelper(this).getAllList();
            listView = (ListView) findViewById(R.id.user_list_lst_view);
            DefaultEmptyList();
            listView.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    Intent i = new Intent(getApplicationContext(), user_abm_activity.class);

                    i.putExtra("modo_edit", true);
                    i.putExtra("_id", usuarios_list.get(position).getId());

                    startActivity(i);
                }
            });

            /*
            if (savedInstanceState == null) {
                getFragmentManager().beginTransaction()
                        .add(R.id.user_list_container, new PlaceholderFragment())
                        .commit();
            }
            */
        } catch (Exception ex) {
            Toast.makeText(this, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void DefaultEmptyList(){
        TextView emptyView = new TextView(getApplicationContext());
        emptyView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        emptyView.setTextColor(getResources().getColor(R.color.texto_black));
        emptyView.setText("Sin resultados");
        emptyView.setTextSize(20);
        emptyView.setVisibility(View.GONE);
        emptyView.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);

        ((ViewGroup)listView.getParent()).addView(emptyView);
        listView.setEmptyView(emptyView);
    }

    @Override
    protected void onResume() {
        try {
            super.onResume();

            usuarios_list = new UsuarioHelper(this).getAllList();

            UsuarioAdapter adapter = new UsuarioAdapter(this, usuarios_list, android.R.layout.simple_list_item_1, false);
            listView.setAdapter(adapter);

        } catch (Exception ex) {
            Toast.makeText(this, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.user_list_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }*/

    /**
     * A placeholder fragment containing a simple view.
     */
    /*
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.user_list, container, false);
            return rootView;
        }
    }*/



}
