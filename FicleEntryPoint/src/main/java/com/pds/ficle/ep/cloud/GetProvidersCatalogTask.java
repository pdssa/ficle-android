package com.pds.ficle.ep.cloud;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Environment;

import com.pds.common.Config;
import com.pds.common.Logger;
import com.pds.common.dao.ProviderDao;
import com.pds.common.model.Provider;
import com.pds.common.util.ConnectivityUtils;
import com.pds.ficle.ep.MainActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hernan on 30/10/2014.
 *//*
public class GetProvidersCatalogTask extends AsyncTask<String, String, String> {
    private Context _context;
    private Config _config;
    private int RESP_OK = -1, RESP_MAL = -1;
    private ProgressDialog dialog;


    public void set_context(Context _context) {
        this._context = _context;
    }

    public GetProvidersCatalogTask(Context _context) {
        this._context = _context;
        this._config = new Config(_context);
    }

    public GetProvidersCatalogTask(Context _context, int code_resp_ok, int code_resp_mal) {
        this._context = _context;
        this._config = new Config(_context);
        this.RESP_OK = code_resp_ok;
        this.RESP_MAL = code_resp_mal;
    }


    private List<String> GetLinesFromServer(String _catalog) throws Exception {
        String _url = "http://" + this._config.CLOUD_SERVER_HOST + ":" + this._config.CLOUD_SERVER_PORT + "/download/csv/" + _catalog + ".csv";
        URL url = new URL(_url);

        //BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream(), "ISO-8859-1"));
        BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"));

        List<String> str = new ArrayList<String>();
        String _str;
        _str = in.readLine();
        str.add(_str.charAt(0) == '\uFEFF' ? _str.substring(1) : _str); //remove BOM char 65279
        while ((_str = in.readLine()) != null) {
            str.add(_str.charAt(0) == '\uFEFF' ? _str.substring(1) : _str); //remove BOM char 65279
        }

        in.close();

        return str;
    }

    private static boolean isSdPresent() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }

    private String processFile(List<String> registros) {
        int cantProviders = 0, cantProvidersNo = 0;

        ContentResolver contentResolver = _context.getContentResolver();
        ProviderDao providerDao = new ProviderDao(contentResolver);

        for (String registro : registros) {

            if (registro.startsWith("X;")) {
                //PROVIDER
                Provider x = new Provider(registro);
                if (providerDao.importProvider(x))
                    cantProviders++;
                else
                    cantProvidersNo++;
            }

        }

        return String.format("Proveedores creados/actualizados: %d\nProveedores omitidos: %d", cantProviders, cantProvidersNo);
    }

    @Override
    protected void onPreExecute() {
        //iniciamos un mensaje para el usuario
        dialog = new ProgressDialog(_context);
        dialog.setMessage("Iniciando descarga del maestro de proveedores. Aguarde por favor...");
        dialog.setIndeterminate(false);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private String shortResult = "";

    @Override
    protected String doInBackground(String... params) {
        try {

            if (!ConnectivityUtils.isOnline(_context)) {
                return "No hay una conexion disponible";
            }

            //1-connect and get new versions
            publishProgress("Obteniendo lista de proveedores del servidor...");

            List<String> lineas = GetLinesFromServer(params[0]);

            //2-fetch response and generate all entities
            publishProgress("Importando registros. Aguarde por favor...");

            String result = processFile(lineas);

            shortResult = result.replace("\n","-").replace("Proveedores","").replace("creados/actualizados","CR").replace("omitidos","O");

            return "Proceso finalizado correctamente: \n" + result;

        } catch (IOException e) {
            //return "No es posible conectarse. Intente mas tarde.";
            return "Error IO: " + e.getMessage();
        } catch (Exception e) {
            return "Error: " + e.getMessage();
        }
    }

    @Override
    protected void onProgressUpdate(String... p) {
        //Toast.makeText(_context, p[0], Toast.LENGTH_LONG).show();
        dialog.setMessage(p[0]);
    }

    // onPostExecute displays the results of the AsyncTask.
    @Override
    protected void onPostExecute(String result) {
        //Toast.makeText(_context, result, Toast.LENGTH_LONG).show();
        if (dialog.isShowing())
            dialog.dismiss();

        if(!result.startsWith("Error:"))
            Logger.RegistrarEvento(_context, "i", "DESCARGA PROVEEDORES", shortResult);
        else
            Logger.RegistrarEvento(_context, "e", "DESCARGA PROVEEDORES", result);


        AlertMessage(result, !result.startsWith("Error:"));
    }

    @Override
    protected void onCancelled(String s) {
        //Toast.makeText(_context, s, Toast.LENGTH_LONG).show();
        AlertMessage(s, false);
    }

    private void AlertMessage(String message, final boolean result) {
        AlertDialog.Builder builder = new AlertDialog.Builder(_context);
        builder.setTitle("IMPORTACIÓN DE PROVEEDORES");
        builder.setMessage(message);
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface _dialog, int which) {
                _dialog.dismiss();

                Finalizar(result);
            }
        });
        builder.setCancelable(false);
        builder.show();
    }

    private void Finalizar(boolean result){
        if(result && RESP_OK != -1){
            ((MainActivity)_context).Init_Flow(RESP_OK); //MANDAMOS EL CODIGO DE RESULTADO OK RECIBIDO
        }
        else if(RESP_MAL != -1){
            ((MainActivity)_context).Init_Flow(RESP_MAL); //MANDAMOS EL CODIGO DE RESULTADO MAL RECIBIDO
        }
    }
}
*/