package com.pds.ficle.ep;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Logger;
import com.pds.common.Usuario;
import com.pds.common.UsuarioHelper;
import com.pds.common.UsuarioHorizontalAdapter;
import com.pds.common.ui.HorizontalListView;
import com.pds.common.util.Perfiles;
import com.pds.common.util.Window;
import com.pds.ficle.ep.utils.PassGenerator;

import java.util.Date;
import java.util.List;


public class LoginActivity2 extends Activity {
    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.

     private static final String[] DUMMY_CREDENTIALS = new String[]{
     "foo@example.com:hello",
     "bar@example.com:world"
     };
     */
    /**
     * The default email to populate the email field with.
     */
    //public static final String EXTRA_EMAIL = "com.example.android.authenticatordemo.extra.EMAIL";

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    // Values for email and password at the time of the login attempt.
    private Usuario mUser;
    private String mPassword;

    // UI references.
    private HorizontalListView  mUserView;
    //private EditText mUserView;
    private EditText mPasswordView;
    private View mLoginFormView;
    private View mLoginStatusView;
    private TextView mLoginStatusMessageView;

    //Usuario user = null;

    List<Usuario> list_all_users;

    private static long first_back_pressed_time = 0;
    private static int back_pressed_times = 0;
    private final static int EXIT_TIMES = 5;
    private final static long EXIT_TIME = 2000; //2 segundos
    private final static String EXIT_PWD = "1111";

    /*
    @Override
    protected void onStart() {
        super.onStart();

        // Save to shared preferences
        SharedPreferences.Editor editor = getSharedPreferences("com.pds.ficle.ep",MODE_PRIVATE).edit();
        editor.putBoolean("alert_no_conn_not_show", false); //reseteamos el estado, para mostrar
        editor.putBoolean("activity_no_conn_visible", false);
        editor.apply();
    }*/

    /*private BaseAdapter mAdapter = new BaseAdapter() {

        @Override
        public int getCount() {
            return list_all_users.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View retval = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_horizontal_listview, null);
            TextView name_user = (TextView) retval.findViewById(R.id.list_row_name);
            name_user.setText(list_all_users.get(position).getNombre());

            return retval;
        }

    };*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);

            setContentView(R.layout.activity_login_new);

            ((TextView) findViewById(R.id.txtVersion)).setText("Version:" + getPackageManager().getPackageInfo(this.getPackageName().toString(), 0).versionName);

            //updateDeviceInfo();

            //Remove notification bar
            //this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

            // Set up the login form.
            mUserView = (HorizontalListView) findViewById(R.id.spn_users);
            //mUser = getIntent().getStringExtra(EXTRA_EMAIL);
            //mUserView = (EditText) findViewById(R.id.user);
            //mUserView.setText(mUser);

            mPasswordView = (EditText) findViewById(R.id.password);
            mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                    if (id == EditorInfo.IME_ACTION_DONE) {
                        attemptLogin();
                        return true;
                    }
                    return false;
                }
            });
            mPasswordView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.length() == 4) {
                        attemptLogin();//despues de 4 caracteres
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            //((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
            /*
            mPasswordView.setOnTouchListener(new OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    Toast.makeText(LoginActivity.this, "touch!", Toast.LENGTH_SHORT).show();
                    //view.requestFocusFromTouch();
                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.showSoftInput(view, 0);

                    return false;
                }
            });
            mPasswordView.setOnFocusChangeListener(new OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    Toast.makeText(LoginActivity.this, "focus change!", Toast.LENGTH_SHORT).show();

                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.showSoftInput(view, 0);
                }
            });
            mPasswordView.setFocusable(true);
            mPasswordView.setFocusableInTouchMode(true);
            */


            mLoginFormView = findViewById(R.id.login_form);
            mLoginStatusView = findViewById(R.id.login_status);
            mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);

            /*findViewById(R.id.sign_in_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    attemptLogin();
                }
            });*/


            list_all_users = new UsuarioHelper(this).getAllList();
            mUserView.setAdapter(new UsuarioHorizontalAdapter(this, list_all_users, android.R.layout.simple_spinner_dropdown_item, true));
            //mUserView.setAdapter(mAdapter);

            mUserView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    mUser = ((UsuarioHorizontalAdapter)parent.getAdapter()).getItem(position);

                    view.setSelected(true);

                    ((UsuarioHorizontalAdapter.Holder)view.getTag()).enabled = true;

                    ((UsuarioHorizontalAdapter) parent.getAdapter()).notifyDataSetChanged();

                    Toast.makeText(LoginActivity2.this, mUser.getNombre(), Toast.LENGTH_SHORT ).show();
                }
            });

            /*findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });*/
        } catch (Exception ex) {
            Toast.makeText(this, ex.toString(), Toast.LENGTH_LONG).show();

        }


    }

    private void updateDeviceInfo(){
        Config cfg = new Config(this);
        updateDeviceInfo(cfg.ANDROID_ID, cfg.IMEI());
    }
    private void updateDeviceInfo(String ID, String IMEI){

        ((TextView) findViewById(R.id.txtSerie)).setText(String.format("Serie: %s - IMEI: %s", ID, IMEI));
    }

    @Override
    protected void onResume() {
        super.onResume();

        AjustaPerfilComercio();

        //focus y teclado en password
        mPasswordView.requestFocus();
        mPasswordView.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.showSoftInput(mPasswordView, 0);
            }
        }, 200);
        //((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(mPasswordView, InputMethodManager.SHOW_FORCED);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) { //solo consideramos la tecla back, el resto de las teclas son omitidas
        return keyCode == KeyEvent.KEYCODE_BACK ? super.onKeyDown(keyCode, event) : true;
    }

    @Override
    public void onBackPressed() {
        //RUTINA PARA SALIR DEL MODO KIOSCO
        long now = System.currentTimeMillis();

        if (now - first_back_pressed_time > EXIT_TIME) {
            first_back_pressed_time = now; //reiniciamos la secuencia
            back_pressed_times = 1;
        } else {
            back_pressed_times++; //continuamos la secuencia
            if (back_pressed_times == EXIT_TIMES)
                SolicitarClaveSalida(); //finalizamos la secuencia
        }
    }

    public void finalizar() {
        try {
            Intent i = new Intent();
            i.setAction("android.intent.action.Homesample");
            //i.setClassName("com.example.android.home", ".Home");
            //i.setComponent(new ComponentName("com.example.android.home", ".Home"));

            finish();

            startActivity(i);
        } catch (Exception ex) {
            Toast.makeText(LoginActivity2.this, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void checkClaveSalida(String passIngresada, String IMEI, String ID){
        try {

            if (passIngresada.equals(EXIT_PWD)) {
                //salimos
                finalizar();
            } else if(CheckConfigPassword(passIngresada,IMEI, ID )){
                //vemos si es la clave tecnica
                //mostramos un cuadro de opciones de acceso tecnico

                String[] opciones = new String[]{"RESET CLAVE ADMIN", "CLONACIÓN - DESTINO" };

                AlertDialog.Builder adb = new AlertDialog.Builder(LoginActivity2.this);
                adb.setTitle("ACCESO TECNICO");

                adb.setItems(opciones, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Toast.makeText(LoginActivity.this, String.valueOf(which), Toast.LENGTH_SHORT).show();

                        switch (which) {
                            case 0: {
                                //accedemos al edit de user admin
                                /*Intent i = new Intent(LoginActivity.this, user_abm_activity.class);
                                i.putExtra("modo_edit", true);
                                i.putExtra("_id", 1);//user admin
                                startActivity(i);*/
                                ResetAdminPass();
                            }
                            break;
                            case 1: {
                                //accedemos a la opcion de clonacion
                                Intent i = new Intent(LoginActivity2.this, MigrationActivity.class);
                                i.putExtra("OPERACION", 1);//DESTINO
                                startActivity(i);
                            }
                            break;
                        }
                    }
                });

                adb.setIcon(android.R.drawable.ic_lock_idle_lock);
                adb.setCancelable(true);
                adb.setNegativeButton("CANCELAR",  null);

                adb.show();
            } else {
                Toast.makeText(LoginActivity2.this, "Clave ingresada erronea", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(LoginActivity2.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void ResetAdminPass(){
        try{
            UsuarioHelper userHelper = new UsuarioHelper(this);
            Usuario _user = userHelper.getById(this, 1);//user admin
            _user.setPassword("1234");//reset pass

            if(userHelper.update(_user)){
                AlertMessage("La clave de admin se ha reiniciado a 1234");
            }
            else
                AlertMessage("No se pudo reiniciar la clave de administrador");

        }
        catch (Exception e) {
            AlertMessage("Error: " + e.getMessage());
        }
    }

    private void AlertMessage(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity2.this);
        builder.setTitle("RESET CLAVE ADMIN");
        builder.setMessage(message);
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setCancelable(false);
        builder.show();
    }

    private void SolicitarClaveSalida() {

        final Config _config = new Config(LoginActivity2.this);

        //vamos a solicitar la clave de tecnico para ingresar a la edicion de la configuracion
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity2.this);
        // Add action buttons
        builder
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        String passIngresada = ((EditText) ((AlertDialog) dialog).findViewById(R.id.dialog_clave_config_txt_pass)).getText().toString();

                        checkClaveSalida(passIngresada, _config.IMEI(), _config.ANDROID_ID);
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

        final AlertDialog dialog = builder.create();

        // Get the layout inflater
        LayoutInflater inflater = LoginActivity2.this.getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View vw = inflater.inflate(R.layout.dialog_clave_config, null);

        ((TextView) vw.findViewById(R.id.dialog_clave_config_txt_id)).setText(_config.ANDROID_ID);
        ((TextView) vw.findViewById(R.id.dialog_clave_config_txt_imei)).setText(_config.IMEI());
        ((EditText) vw.findViewById(R.id.dialog_clave_config_txt_pass)).setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    checkClaveSalida(v.getText().toString(), _config.IMEI(), _config.ANDROID_ID);
                    dialog.dismiss();
                    return true;
                }
                return false;
            }
        });
        ((EditText) vw.findViewById(R.id.dialog_clave_config_txt_pass)).setHint(R.string.hint_ing_clave_salida);

        dialog.setView(vw);
        dialog.setTitle("Ingrese Clave para salir");
        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
            }
        });

        dialog.show();

        //---------------------------------------------------------------
    }

    private boolean CheckConfigPassword(String passIngresada, String IMEI, String ID){
        try {

            String claveTecnico = PassGenerator.GenerarClaveTecnico(new Date(), IMEI, ID);

            //updateDeviceInfo(_config.ANDROID_ID, _config.IMEI());

            return passIngresada.equals(claveTecnico);
        } catch (Exception e) {
            Toast.makeText(LoginActivity2.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
        return false;
    }

    private void AjustaPerfilComercio(){
        Config config = new Config(LoginActivity2.this);
        if(config.PERFIL.equals(Perfiles.PERFIL_JJD)){
            findViewById(R.id.login_layout).setBackgroundResource(R.drawable.puntototal);
        }else if (config.PERFIL.equals(Perfiles.PERFIL_DEMO_POWAPOS)){
            findViewById(R.id.login_layout).setBackgroundResource(R.drawable.fondo_powa);
        }else if (config.PERFIL.equals(Perfiles.PERFIL_IQCORP)) {
            findViewById(R.id.login_layout).setBackgroundResource(R.drawable.fondo_iqcorp);
        }else if (config.PERFIL.equals(Perfiles.PERFIL_PILOTO_EMBONOR)) {
            findViewById(R.id.login_layout).setBackgroundResource(R.drawable.fondo_embonor);
        }
        else{
            findViewById(R.id.login_layout).setBackgroundResource(R.drawable.fondo_bpos);
        }
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        //mUserView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.

        if (mUser == null){
            Toast.makeText(LoginActivity2.this, "Seleccione un usuario para ingresar al sistema", Toast.LENGTH_SHORT).show();
            return;
        }


        mPassword = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password.
        if (TextUtils.isEmpty(mPassword)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        } else if ( mPassword.length() != 4) {
            mPasswordView.setError(getString(R.string.error_short_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        /*
        if (TextUtils.isEmpty(mUser)) {
            mUserView.setError(getString(R.string.error_field_required));
            focusView = mUserView;
            cancel = true;
        }*/

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.

            //ocultamos el teclado
            Window.CloseSoftKeyboard(LoginActivity2.this);
            //InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            //imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
            //        InputMethodManager.HIDE_NOT_ALWAYS);

            //mostramos el spinner de login...
            mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
            showProgress(true);
            mAuthTask = new UserLoginTask();
            mAuthTask.execute((Void) null);
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginStatusView.setVisibility(View.VISIBLE);
            mLoginStatusView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(show ? 1 : 0)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
                        }
                    });

            mLoginFormView.setVisibility(View.VISIBLE);
            mLoginFormView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(show ? 0 : 1)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                        }
                    });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                // Simulate network access.
                Thread.sleep(1500);

                //int result =  user.getPassword().equals(mPassword) ? 1 : 0;
                return mUser.getPassword().equals(mPassword);

                //si es el admin, y no ingresó correctamente su clave, entonces probamos usandola como clave de
                //configuración del equipo, si está OK, dejamos entrar
                //if(result == 0 && mUser.equals("admin") )
                //    result = CheckConfigPassword(mPassword) ? 2 : 0;

                //return result;

            } catch (InterruptedException e) {
                return false;
            } catch (Exception e) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                //finish();
                Toast.makeText(getApplicationContext(), "Bienvenido " + mUser.getNombre() + "!", Toast.LENGTH_SHORT).show();
                finish();
                Logger.RegistrarEvento(getApplicationContext(), "i", "Login", "User: " + mUser.getNombre());
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                //i.putExtra("_id_user_login",user.getId());
                i.putExtra("current_user", mUser);
                i.putExtra("sourceActivity", "LoginActivity");
                startActivity(i);
            } else {
                mPasswordView.setError(getString(R.string.error_incorrect_password));
                mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }


    }
}
