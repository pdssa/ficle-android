package com.pds.ficle.ep.cloud;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.pds.common.Logger;


/**
 * Created by Hernan on 21/04/2016.
 */
public class DevicePublicIpTask extends AsyncTask<Void, String, String> {

    private Context context;

    public DevicePublicIpTask(Context context){
        this.context = context;
    }

    @Override
    protected String doInBackground(Void... params) {

        try {

            WebServiceRequest webServiceRequest = new WebServiceRequest();
            String str =  webServiceRequest.callWebService("https://ifcfg.me/ip");

            Log.i("DevicePublicIpTask", str);

            Logger.RegistrarEvento(context, "i", "Public IP", str);

            return str;

        }
        catch (Exception e)
        {
            Log.e("DevicePublicIpTask", "Error", e);
            Logger.RegistrarEvento(context, "i", "Public IP", "Error:" + e.getMessage());
        }

        return "";
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }
}
