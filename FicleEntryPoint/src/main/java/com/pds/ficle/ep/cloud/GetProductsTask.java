package com.pds.ficle.ep.cloud;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pds.common.Config;
import com.pds.common.Logger;
import com.pds.common.util.ConnectivityUtils;
import com.pds.ficle.ep.MainActivity;
import com.pds.ficle.ep.controllers.ProductController;
import com.pds.ficle.ep.helpers.SessionHelper;
import com.pds.ficle.ep.models.getProducts.GetProductsItem;
import com.pds.ficle.ep.models.getProducts.GetProductsResponse;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

public class GetProductsTask extends AsyncTask<Void, String, String> {

    private static final String TAG = GetComercioTask.class.getSimpleName();
    private String IMEI;
    private String ANDROID_ID;
    private String SERVER_IP;
    private ProgressDialog mDialog;
    private Context mCtx;
    private int mQtyDeptsOk = 0;
    private int mQtySubDeptsOk = 0;
    private int mQtyProdsOk = 0;

    private int mQtyDeptsKo = 0;
    private int mQtySubDeptsKo = 0;

    private int mQtyProdsUpd = 0;


    private Config mConfig;

//    public EventsFlow RESULT;

    public GetProductsTask(Context context, String imei, String androidID, String server_ip) {
        IMEI = imei;
        ANDROID_ID = androidID;
        SERVER_IP = server_ip;
        mCtx = context;
        this.mDialog = new ProgressDialog(mCtx);
        mConfig = new Config(mCtx);

    }


    private GetProductsResponse getProductsFromServer() throws Exception {

        final int CONNECTION_TIMEOUT = 30000;//the timeout until a connection is established
        final int SOCKET_TIMEOUT = 180000; //is the timeout for waiting for data

        // 1. create HttpClient
        HttpClient httpclient = new DefaultHttpClient();
        HttpParams params = httpclient.getParams();
        HttpConnectionParams.setConnectionTimeout(params, CONNECTION_TIMEOUT);
        HttpConnectionParams.setSoTimeout(params, SOCKET_TIMEOUT);

        // 2. make POST request to the given URL
        //String servidor = "http://200.69.238.26:37800/WS.asmx";
        String servidor = SERVER_IP;

        HttpGet httpGet;

        String date = parseLastUpdateTimestamp();

        httpGet = new HttpGet(servidor + "api/GetProducts?imei=" + IMEI + "&androidId=" + ANDROID_ID + "&lastUpdateTimestamp=" + date);

        // 7. Set some headers to inform server about the type of the content
        httpGet.setHeader("Accept", "application/json");
        httpGet.setHeader("Content-type", "application/json");

        // 8. Execute POST request to the given URL
        HttpResponse httpResponse = httpclient.execute(httpGet);

        // 9. receive response as inputStream
        InputStream inputStream = null;
        inputStream = httpResponse.getEntity().getContent();

        // 10. convert inputstream to string
        GetProductsResponse p = new Gson().fromJson(new InputStreamReader(inputStream, "UTF-8"), GetProductsResponse.class);

        return p;
    }

    private String parseLastUpdateTimestamp() {
        Date lastUpdateTimestamp = null;
        try {
            lastUpdateTimestamp = SessionHelper.getInstance().getLastUpdateTimestamp();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
        return gson.toJson(lastUpdateTimestamp).replace("\"", "");
    }




    // onPreExecute used to setup the AsyncTask.
    @Override
    protected void onPreExecute() {

        mDialog = new ProgressDialog(mCtx);
        mDialog.setMessage("Iniciando descarga del catalogo master. Aguarde por favor...");
        mDialog.setIndeterminate(false);
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.show();
    }

    @Override
    protected String doInBackground(Void... params) {
        try {

            if (!ConnectivityUtils.isOnline(mCtx)) {
                return "No hay una conexión disponible";
            }

            GetProductsResponse response = getProductsFromServer();


            if (response.getResponseCode() == 0) {
                String result = upsertProducts(response);


                SessionHelper.getInstance().setLastUpdateTimestamp(new Date());
                return result;
            } else {
                String err = "Error: No se pudieron obtener los productos del catalogo master ";
//                showResultMessage(err + response.getResponseMessage());
                return err;
            }

        } catch (IOException e) {
            String err = "Error: No es posible conectarse. Intente mas tarde.";
//            showResultMessage(err);
            return err;
        } catch (Exception e) {
            String err = "Error: " + e.getMessage();
//            showResultMessage(err);
            return err;
        }
    }

    private String upsertProducts(GetProductsResponse response) {

        ProductController ctrl = new ProductController(mCtx);
        int i = 0;

        ArrayList<GetProductsItem> prods = response.getProducts();

        for (GetProductsItem prod :
                prods) {

            setupDept(ctrl, prod);
            setupSubDept(ctrl, prod);
            setupProduct(ctrl, prod);
            i++;
            try {
                if(i % 30 == 0)
                    publishProgress(String.format("%02.02f", (double)(i * 100)/prods.size())+"% "+String.format("(%d/%d)", i, prods.size()));

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return String.format("Departamentos creados: %d\nDepartamentos omitidos: %d\nSubdepartamentos creados: %d\nSubdepartamentos omitidos: %d\nProductos creados: %d\nProductos actualizados: %d", mQtyDeptsOk, mQtyDeptsKo, mQtySubDeptsOk, mQtySubDeptsKo, mQtyProdsOk, mQtyProdsUpd);


    }




    private void setupProduct(ProductController ctrl, GetProductsItem prod) {
        if (ctrl.anyProductByGtin(prod.getGtin()) == false) {
            if (ctrl.insertProduct(prod) == false) {
                Log.e(TAG, "setupProduct: No se pudo insertar el producto " + prod.getName() + " (" + prod.getGtin() + ")");

            } else mQtyProdsOk++;

        } else {
            if (ctrl.updateProduct(prod) == false) {
                Log.e(TAG, "setupProduct: No se pudo actualizar el producto " + prod.getName() + " (" + prod.getGtin() + ")");
            } else mQtyProdsUpd++;
        }
    }

    private void setupSubDept(ProductController ctrl, GetProductsItem prod) {
        if (ctrl.anySubDeptByName(prod.getSubDept(), prod.getDept()) == false) {
            if (ctrl.insertSubDept(prod.getSubDept(), prod.getDept()) == false) {
                Log.e(TAG, "insertSubDept: Error al crear departamento " + prod.getDept());

            } else mQtyDeptsOk++;
        } else mQtyDeptsKo++;
    }

    private void setupDept(ProductController ctrl, GetProductsItem prod) {
        if (ctrl.anyDeptByName(prod.getDept()) == false) {
            if (ctrl.insertDept(prod.getDept()) == false) {
                Log.e(TAG, "insertDept: Error al crear departamento " + prod.getDept());

            } else mQtySubDeptsOk++;
        } else mQtySubDeptsKo++;
    }

    @Override
    protected void onProgressUpdate(String... p) {
        //Toast.makeText(mCtx, p[0], Toast.LENGTH_LONG).show();
        mDialog.setMessage("Procesando productos... "+p[0]);
    }

    // onPostExecute displays the results of the AsyncTask.
    @Override
    protected void onPostExecute(String result) {
        if (mDialog.isShowing())
            mDialog.dismiss();

        if (!result.startsWith("Error:"))
            Logger.RegistrarEvento(mCtx, "i", "DESCARGA CATALOGO", result);
        else
            Logger.RegistrarEvento(mCtx, "e", "DESCARGA CATALOGO", result);

        Log.i(TAG, "onPostExecute: RESULT IS: " + result);


        if(result.startsWith("Error:"))
        {
            showResultMessage(result);
        }
        else if (mQtyProdsUpd + mQtyProdsOk + mQtySubDeptsKo + mQtyDeptsOk + mQtyDeptsKo + mQtySubDeptsOk > 0) {
            showResultMessage(result);
        } else {
            ((MainActivity) mCtx).Init_Flow(23);
        }


    }

    private void showResultMessage(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
        builder.setTitle("IMPORTACIÓN DE CATALOGO");
        builder.setMessage(message);
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface _dialog, int which) {
                _dialog.dismiss();

                ((MainActivity) mCtx).Init_Flow(23);
            }
        });
        builder.setCancelable(false);
        builder.show();
    }


}
