package com.pds.ficle.ep.cloud.process_entity_helpers;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public abstract class ProcessEntityHelper {
    private static final String TAG = ProcessEntityHelper.class.getSimpleName();
    private int MAX_SEND = 250;//cantidad maxima de registros a enviar por envío (performance)
    protected Context mCtx;
    protected Config mConfig;
    protected boolean mHasErrors = false;
    protected boolean mHasToContinue = true;

    public ProcessEntityHelper(Context ctx, Config config)
    {
        mCtx = ctx;
        mConfig = config;
    }

    public boolean hasToContinue()
    {
        return mHasToContinue;
    }



    public void processEntity()
    {
        try {
            String request = null;
            request = getJsonRequest();


            //publishProgress("Enviando XXX al servidor...");
            if (request != null) {
                String response = getHttpPostDataToServer(request); //PostDataToServer(array, entityType);

                if(isSuccess(response) == false)
                    throw new Exception(response);


                //3-marcamos como enviados
                updateSyncState();

            } else {
                mHasToContinue = false;
            }
        } catch (Exception ex) {
            mHasErrors = true;
            Log.e(TAG, ex.getMessage() + "" + ex.toString(), ex);
            Logger.RegistrarEvento(mCtx, "e", "SyncProcess", ex.getMessage() + "" + ex.toString(), "Ent:" + getEntityType());
//            Toast.makeText(mCtx, "Error sincronizando "+getEntityType()+" : Razon:"+ex.getMessage(), Toast.LENGTH_SHORT).show();
            mHasToContinue = false;
        }
    }

    protected void updateSyncState() throws Exception {

        ArrayList<JSONObject> element = getPayload();

        if(element != null){
            for (int i = 0; i < element.size(); i++) {
                JSONObject jsonObject = element.get(i);
                updateEntitySyncState(jsonObject);
            }
        }
    }

    protected abstract void updateEntitySyncState(JSONObject jsonObject) throws JSONException;

    protected abstract boolean isSuccess(String response);

    protected abstract String getEntityType();

    protected abstract String getHttpPostDataToServer(String request) throws Exception;

    protected ArrayList<JSONObject> cursorToJSONWebApi(Cursor cursor) throws Exception {

        ArrayList<JSONObject> resultSet = new ArrayList<JSONObject>();

        int x = 0; //contador de registros

        if (cursor.moveToFirst()) {

            do {

                int totalColumn   = cursor.getColumnCount();
                int columnCounter = 0;

                JSONObject rowObject = new JSONObject();

                for (int i = 0; i < totalColumn; i++) {

                    try {

                        if (cursor.getColumnName(i) != null) {

                            switch (cursor.getType(i)) {
                                case Cursor.FIELD_TYPE_FLOAT:
                                    rowObject.put(cursor.getColumnName(i), cursor.getDouble(i));
                                    break;
                                case Cursor.FIELD_TYPE_INTEGER:
                                    rowObject.put(cursor.getColumnName(i), cursor.getInt(i));
                                    break;
                                default:
                                    if (cursor.getString(i) != null) {
                                        rowObject.put(cursor.getColumnName(i), cursor.getString(i).replace("\"", "").replace("\\", "/"));
                                    } else {
                                        rowObject.put(cursor.getColumnName(i), "");
                                    }
                                    break;
                            }
                        }

                        columnCounter++;

                    } catch (Exception ex) {
                        //throw ex;
                        Logger.RegistrarEvento(mCtx, "e", "SyncProcess", ex.getMessage() + "" + ex.toString(), "Ent:" + getEntityType());
                    }
                }

                if(totalColumn == columnCounter){
                    resultSet.add(rowObject);
                }

                x++;

            } while (cursor.moveToNext() && x < MAX_SEND);

            mHasToContinue = (x == MAX_SEND);//si cortamos, entonces vamos a seguir...

        } else {

            mHasToContinue = false;

            return null;
        }

        cursor.close();

        return resultSet;
    }


    protected Cursor getCursor(String uri, String[] columnas, String filter, String orderby) {
        String limit = " LIMIT " + String.valueOf(MAX_SEND);

        orderby = TextUtils.isEmpty(orderby) ? "1" : orderby;

        Cursor cursor = mCtx.getContentResolver().query(
                Uri.parse(uri),
                columnas,
                filter,
                null,
                orderby + limit);

        return cursor;
    }

    protected abstract String getJsonRequest() throws Exception;

    protected abstract ArrayList<JSONObject> getPayload() throws Exception;

}
