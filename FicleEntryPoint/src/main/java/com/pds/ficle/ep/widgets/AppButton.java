package com.pds.ficle.ep.widgets;

public class AppButton {
    public String Package;
    public int ImageId;

    public AppButton(String _package, int _imageId) {
        this.Package = _package;
        this.ImageId = _imageId;
    }
}