package com.pds.ficle.ep.models.syncDataTask;

import com.pds.ficle.ep.models.ApiResponse;

public class SyncDataTaskResponse extends ApiResponse {
    public SyncDataTaskResponse(int code, String msg) {
        super(msg, code);
    }
}
