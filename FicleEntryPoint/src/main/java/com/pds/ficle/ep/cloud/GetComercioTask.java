package com.pds.ficle.ep.cloud;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pds.common.Config;
import com.pds.common.ConfigHelper;
import com.pds.common.Logger;
import com.pds.common.SII.vale.util.ValeConst;
import com.pds.common.util.EventsFlow;
import com.pds.ficle.ep.MainActivity;
import com.pds.common.util.ConnectivityUtils;
import com.pds.ficle.ep.R;
import com.pds.ficle.ep.models.getMerchantInfo.GetMerchantInfoResponse;
import com.pds.ficle.ep.models.getMerchantInfo.ValeConfigDto;
import com.pds.ficle.ep.services.DownloadService;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Hernan on 04/12/2014.
 */
public class GetComercioTask extends AsyncTask<Void, String, String> {

    private static final String TAG = GetComercioTask.class.getSimpleName();
    private String IMEI;
    private String ANDROID_ID;
    private String SERVER_IP;
    private Context mCtx;

    private Config _config;


    private boolean _FIRST_CONFIG;

    public EventsFlow RESULT;

    public GetComercioTask(Context context, String imei, String androidID, String server_ip) {
        IMEI = imei;
        ANDROID_ID = androidID;
        SERVER_IP = server_ip;
        mCtx = context;
        //_FIRST_CONFIG = firstConfig;
        _config = new Config(mCtx);

        CheckEstadoTerminal();//-> con esto sabemos si podemos consultar la config

    }


    private void CheckEstadoTerminal() {
        //verificamos si es necesario configurar algun valor obligatorio
        try {
            valoresConfigurados(_config.COMERCIO, _config.ID_COMERCIO, _config.DIRECCION, _config.CLAVEFISCAL);
        } catch (Exception e) {
            e.printStackTrace();
            _FIRST_CONFIG = true;
        }

        if (_FIRST_CONFIG) {

            //syncPermitido = false;

            //vamos a solicitar que confirmen los datos del servidor para obtener la configuracion del comercio
            AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
            // Add action buttons
            builder
                    .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {

                            SERVER_IP = ((EditText) ((AlertDialog) dialog).findViewById(R.id.confirm_server_ip)).getText().toString();

                            execute();

                        }
                    })
                    .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Finalizar(EventsFlow.CONFIG_CANCELADA);
                        }
                    });

            final AlertDialog dialog = builder.create();

            // Get the layout inflater
            LayoutInflater inflater = ((Activity) mCtx).getLayoutInflater();

            // Inflate and set the layout for the dialog
            // Pass null as the parent view because its going in the dialog layout
            View vw = inflater.inflate(R.layout.dialog_confirm_server, null);

            final EditText txt_server_ip = (EditText) vw.findViewById(R.id.confirm_server_ip);

            txt_server_ip.setText(_config.CLOUD_SERVER_HOST);

            TextView.OnEditorActionListener editorActionListener = new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        SERVER_IP = txt_server_ip.getText().toString();

                        execute();

                        dialog.dismiss();
                        return true;
                    }
                    return false;
                }
            };

            txt_server_ip.setOnEditorActionListener(editorActionListener);

            dialog.setView(vw);
            dialog.setTitle("Confirmar datos del servidor");
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    ((InputMethodManager) mCtx.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(txt_server_ip.getWindowToken(), 0);
                }
            });
            dialog.show();
        } else
            execute();
    }

    private void valoresConfigurados(ContentValues cv) throws Exception {
        valoresConfigurados(cv.getAsString(ConfigHelper.CONFIG_COMERCIO), cv.getAsString(ConfigHelper.CONFIG_ID_COMERCIO), cv.getAsString(ConfigHelper.CONFIG_DIRECCION), cv.getAsString(ConfigHelper.CONFIG_CLAVEFISCAL));
    }

    private void valoresConfigurados(String comercio, String id_comercio, String domicilio, String clave_fiscal) throws Exception {

        //controlamos el comercio
        if (TextUtils.isEmpty(comercio))
            throw new Exception("El nombre comercio no puede ser vacío o nulo. Reconfigure ese parámetro por favor");

        //controlamos el codigo de comercio
        if (TextUtils.isEmpty(id_comercio))
            throw new Exception("El ID de comercio no puede ser vacío o nulo. Reconfigure ese parámetro por favor");

        //controlamos el id de terminal
        if (TextUtils.isEmpty(domicilio))
            throw new Exception("El domicilio del comercio no puede ser vacío o nulo. Reconfigure ese parámetro por favor");

        //controlamos la clave fiscal
        if (TextUtils.isEmpty(clave_fiscal))
            throw new Exception("El RUT del comercio no puede ser vacío o nulo. Reconfigure ese parámetro por favor");


    }


    private ContentValues getConfigFromServer() throws Exception {

        final int CONNECTION_TIMEOUT = 2000;//the timeout until a connection is established
        final int SOCKET_TIMEOUT = 30000; //is the timeout for waiting for data

        // 1. create HttpClient
        HttpClient httpclient = new DefaultHttpClient();
        HttpParams params = httpclient.getParams();
        HttpConnectionParams.setConnectionTimeout(params, CONNECTION_TIMEOUT);
        HttpConnectionParams.setSoTimeout(params, SOCKET_TIMEOUT);

        // 2. make POST request to the given URL
        //String servidor = "http://200.69.238.26:37800/WS.asmx";
        String servidor = SERVER_IP;

        HttpGet httpGet;

        httpGet = new HttpGet(servidor + "api/GetMerchantInfo?imei=" + IMEI + "&androidId=" + ANDROID_ID);

        // 7. Set some headers to inform server about the type of the content
        httpGet.setHeader("Accept", "application/json");
        httpGet.setHeader("Content-type", "application/json");

        // 8. Execute POST request to the given URL
        HttpResponse httpResponse = httpclient.execute(httpGet);

        if (httpResponse.getStatusLine().getStatusCode() != 200) {
            Log.e(TAG, "getConfigFromServer: Pagina no encontrada! " + servidor);
            _FIRST_CONFIG = true;
            throw new Exception("Error de conexión con el servidor: " + httpResponse.getStatusLine().getStatusCode());
        }

        // 9. receive response as inputStream
        InputStream inputStream = null;
        inputStream = httpResponse.getEntity().getContent();

        // 10. convert inputstream to string
        return ReadComercioJson(inputStream);
    }

    private ContentValues ReadComercioJson(InputStream in) throws Exception {

        ContentValues cv = new ContentValues();


        boolean valeConfigOk = false;

        try {
            GetMerchantInfoResponse p = new Gson().fromJson(new InputStreamReader(in, "UTF-8"), GetMerchantInfoResponse.class);
            if (p == null) throw new Exception("Error al leer JSON de SERVIDOR");

            cv.put(ConfigHelper.CONFIG_COMERCIO, p.getMerchant());
            cv.put(ConfigHelper.CONFIG_CLAVEFISCAL, p.getFiscalKey());
            cv.put(ConfigHelper.CONFIG_DIRECCION, p.getAddress());
            cv.put(ConfigHelper.CONFIG_ID_COMERCIO, p.getMerchantCode());
            cv.put(ConfigHelper.CONFIG_PERFIL, p.getProfile());
            cv.put(ConfigHelper.CONFIG_RAZON_SOCIAL, p.getName());


            int checkUpdates = p.getCheckUpdatesFlag();

            if (checkUpdates > 0) {
                //iniciamos el servicio de descarga de aplicaciones en background
                mCtx.startService(DownloadService.newDownloadServiceIntent(mCtx, _config.CLOUD_SERVER_HOST, _config.ANDROID_ID));
            }

            valeConfigOk = readConfigVale(p.getValeConfig(), cv, _config.SII_STATUS, _config.SII_MODALIDAD);

            if (!valeConfigOk) {
                //tenemos que deshabilitar el servicio
                disableValeService(cv, _config.SII_STATUS);
            }



/*
            //reader.beginArray();
            //while (reader.hasNext()) {
            reader.beginObject();

            while (reader.hasNext()) {

                String d = reader.nextName();


                String name = reader_config.nextName();
                if (name.equals("e")) {//mensaje de error...
                    throw new Exception(reader_config.nextString());
                } else if (name.equals("comercio")) {
                    cv.put(ConfigHelper.CONFIG_COMERCIO, reader_config.nextString());
                } else if (name.equals("claveFiscal")) {
                    cv.put(ConfigHelper.CONFIG_CLAVEFISCAL, reader_config.nextString());
                } else if (name.equals("direccion")) {
                    cv.put(ConfigHelper.CONFIG_DIRECCION, reader_config.nextString());
                } else if (name.equals("idComercio")) {
                    cv.put(ConfigHelper.CONFIG_ID_COMERCIO, reader_config.nextString());
                } else if (name.equals("perfil")) {
                    cv.put(ConfigHelper.CONFIG_PERFIL, reader_config.nextString());
                } else if (name.equals("rubro")) {
                    cv.put(ConfigHelper.CONFIG_RUBRO, reader_config.nextString());
                } else if (name.equals("catalogo")) {
                    CONFIG_CATALOGO = reader_config.nextString();
                } else if (name.equals("localidad")) {
                    cv.put(ConfigHelper.CONFIG_LOCALIDAD, reader_config.nextString());
                } else if (name.equals("razonSocial")) {
                    cv.put(ConfigHelper.CONFIG_RAZON_SOCIAL, reader_config.nextString());
                } else if (name.equals("checkUpdatesFlag")) {
                    int checkUpdates = reader_config.nextInt();

                    if(checkUpdates > 0){
                        //iniciamos el servicio de descarga de aplicaciones en background
                        mCtx.startService(DownloadService.newDownloadServiceIntent(mCtx, _config.CLOUD_SERVER_HOST, _config.ANDROID_ID));
                    }

                } else if (name.equals("cod_pais")) {
                    cv.put(ConfigHelper.CONFIG_PAIS, reader_config.nextString());



                } else if (name.equals("surv_url")) {
                    String surv_url = reader_config.nextString();

                    SharedPreferences pref = mCtx.getSharedPreferences("com.pds.ficle.ep", Context.MODE_PRIVATE);

                    //vemos si es diferente a la ultima conocida
                    if (!pref.getString("enc_url", "").equals(surv_url)) {
                        SharedPreferences.Editor editor = pref.edit();

                        //guardamos la encuesta y pedimos que respondan
                        editor.putString("enc_url", surv_url);

                        //si recibimos una encuesta del servidor
                        if (!TextUtils.isEmpty(surv_url))
                            editor.putBoolean("enc_tiene_q_resp", true);
                        else
                            editor.putBoolean("enc_tiene_q_resp", false);

                        editor.apply();
                    }
                }
                else if (name.equals("msg_url")) {
                    String message_url = reader_config.nextString();

                    SharedPreferences pref = mCtx.getSharedPreferences("com.pds.ficle.ep", Context.MODE_PRIVATE);

                    //vemos si es diferente al ultimo conocido
                    if (!pref.getString("msg_url", "").equals(message_url)) {
                        //guardamos el mensaje
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("msg_url", message_url);
                        editor.apply();
                    }
                }
                else if (name.equals("ValeConfig")) {



                    //leemos la configuracion de vale
                    valeConfigOk = readConfigVale(reader_config, cv, _config.SII_STATUS, _config.SII_MODALIDAD);
                } else {
                    reader_config.skipValue();
                }

            }

            reader.endObject();



            if (reader_config != null) reader_config.close();
            reader.close();

            //}
            //reader.endArray();
               */
        } catch (Exception ex) {
            _FIRST_CONFIG = true; //MARCAMOS ESTE FLAG PARA QUE HAGA EL COMPORTAMIENTO DE MOSTRAR MENSAJE Y SALIR
            throw ex;
        }


        return cv;
    }

    private void UpdateConfig(ContentValues cv) throws Exception {

        cv.put(ConfigHelper.CONFIG_CLOUD_HOST, SERVER_IP);


        int result = mCtx.getContentResolver().update(
                Uri.parse("content://com.pds.ficle.ep.config.contentprovider/configs/1"),
                cv,
                "",
                new String[]{});

        Logger.RegistrarEvento(mCtx, "i", "Configuracion", "Configuracion obtenida del servidor");

        if (result < 1)
            throw new Exception("No se pudo actualizar la configuracion");

    }

    private boolean readConfigVale(ValeConfigDto dto, ContentValues cv, String configValeEstadoActual, String configValeModalidadActual) {
        try {
            boolean result = false;
            boolean servicioValeHabilitadoPlataforma = false;
            String serv_ip = dto.getValeIp();
            String serv_port = String.valueOf(dto.getValePort());

            cv.put(ConfigHelper.CONFIG_SII_GIRO1, dto.getCat1());
            cv.put(ConfigHelper.CONFIG_SII_GIRO2, dto.getCat2());
            cv.put(ConfigHelper.CONFIG_SII_GIRO3, dto.getCat3());
            cv.put(ConfigHelper.CONFIG_SII_RES_VALE, dto.getResolutionText());
            cv.put(ConfigHelper.CONFIG_SII_VALE_SERVER_IP, dto.getValeIp());
            cv.put(ConfigHelper.CONFIG_SII_VALE_SERVER_PORT, dto.getValePort());
            cv.put(ConfigHelper.CONFIG_SII_VALE_SERVER_CONSULTA_QR, dto.getPathQr());
            cv.put(ConfigHelper.CONFIG_SII_VALE_TERMINAL_ID, dto.getTerminalId());
            cv.put(ConfigHelper.CONFIG_SII_VALE_MERCHANT_ID, dto.getMerchantId());
            cv.put(ConfigHelper.CONFIG_SII_VALE_MODALIDAD, dto.getOperationMode());
            servicioValeHabilitadoPlataforma = dto.isServiceEnabled();
          /*  reader_config.beginObject();

            while (reader_config.hasNext()) {
                String nameVale = reader_config.nextName();
                if (nameVale.equals("giro1")) {
                    cv.put(ConfigHelper.CONFIG_SII_GIRO1, reader_config.nextString());
                } else if (nameVale.equals("giro2")) {
                    cv.put(ConfigHelper.CONFIG_SII_GIRO2, reader_config.nextString());
                } else if (nameVale.equals("giro3")) {
                    cv.put(ConfigHelper.CONFIG_SII_GIRO3, reader_config.nextString());
                } else if (nameVale.equals("resolucion")) {
                    cv.put(ConfigHelper.CONFIG_SII_RES_VALE, reader_config.nextString());
                } else if (nameVale.equals("terminalId")) {
                    cv.put(ConfigHelper.CONFIG_SII_VALE_TERMINAL_ID, reader_config.nextString());
                } else if (nameVale.equals("ip")) {
                    serv_ip = reader_config.nextString();
                    cv.put(ConfigHelper.CONFIG_SII_VALE_SERVER_IP, serv_ip);
                } else if (nameVale.equals("port")) {
                    serv_port = reader_config.nextString();
                    cv.put(ConfigHelper.CONFIG_SII_VALE_SERVER_PORT, serv_port);
                } else if (nameVale.equals("pathQR")) {
                    cv.put(ConfigHelper.CONFIG_SII_VALE_SERVER_CONSULTA_QR, reader_config.nextString());
                } else if (nameVale.equals("modalidad")) {
                    cv.put(ConfigHelper.CONFIG_SII_VALE_MODALIDAD, reader_config.nextString());
                } else if (nameVale.equals("estadoServicio")) {
                    servicioValeHabilitadoPlataforma = reader_config.nextBoolean();


                } else {
                    reader_config.skipValue();
                }
            }*/

            if (serv_port.equals("443"))
                cv.put(ConfigHelper.CONFIG_SII_VALE_SERVER_PLATFORM, "https://" + serv_ip + ":" + serv_port);
            else
                cv.put(ConfigHelper.CONFIG_SII_VALE_SERVER_PLATFORM, "http://" + serv_ip + ":" + serv_port);


            //situaciones:
            //1. SERVICIO NO HABILITADO EN PLATAFORMA => DESHABILITAMOS LOCALMENTE
            //2. SERVICIO HABILITADO EN PLATAFORMA =>
            //2.1 CAMBIO EN MODALIDAD? => VOLVEMOS A ESTADO HABILITADO PARA QUE REINICIALICE
            //2.2 MANTIENE MODALIDAD => SI ESTA DESACTIVADO => HABILITAMOS LOCALMENTE

            if (!servicioValeHabilitadoPlataforma) {//***1***
                cv.put(ConfigHelper.CONFIG_SII_VALE_STATUS, ValeConst.VALE_STATUS_DESACTIVADO);
                result = false;
            } else {

                cv.put(ConfigHelper.CONFIG_SII_VALE_STATUS, ValeConst.VALE_STATUS_HABILITADO);
                Logger.RegistrarEvento(mCtx, "i", "VALE-HABILITACION", "HABILITADO DESDE PLATAFORMA");

                ///FIXME: HZ... Lo saque porque no estaba funcionando como esperabamos. Pero habria que revisarlo en detalle
/*
                //***2***
                if (!configValeModalidadActual.equalsIgnoreCase(cv.getAsString(ConfigHelper.CONFIG_SII_VALE_MODALIDAD))) {//***2.1***cambio en modalidad => volvemos a habilitado (aunque estuviera inicializado)
                    cv.put(ConfigHelper.CONFIG_SII_VALE_STATUS, ValeConst.VALE_STATUS_HABILITADO);

                    Logger.RegistrarEvento(mCtx, "i", "VALE-HABILITACION", "HABILITADO POR CAMBIO MODALIDAD");
                } else if (TextUtils.isEmpty(configValeEstadoActual) || configValeEstadoActual.equalsIgnoreCase(ValeConst.VALE_STATUS_DESACTIVADO)) {//***2.2***si esta desactivado localmente => habilitamos
                    cv.put(ConfigHelper.CONFIG_SII_VALE_STATUS, ValeConst.VALE_STATUS_HABILITADO);

                    Logger.RegistrarEvento(mCtx, "i", "VALE-HABILITACION", "HABILITADO DESDE PLATAFORMA");
                }
*/

                result = true;
            }


            return result;

        } catch (Exception ex) {
            Log.e(TAG, "error", ex);
            return false;
        }
    }

    private void disableValeService(ContentValues cv, String configValeEstadoActual) {
        //reset all params
        cv.put(ConfigHelper.CONFIG_SII_VALE_STATUS, ValeConst.VALE_STATUS_DESACTIVADO);
        cv.put(ConfigHelper.CONFIG_SII_GIRO1, "");
        cv.put(ConfigHelper.CONFIG_SII_GIRO2, "");
        cv.put(ConfigHelper.CONFIG_SII_GIRO3, "");
        cv.put(ConfigHelper.CONFIG_SII_RES_VALE, "");
        cv.put(ConfigHelper.CONFIG_SII_VALE_TERMINAL_ID, "");
        cv.put(ConfigHelper.CONFIG_SII_VALE_MERCHANT_ID, "");
        cv.put(ConfigHelper.CONFIG_SII_VALE_SERVER_IP, "");
        cv.put(ConfigHelper.CONFIG_SII_VALE_SERVER_PORT, "");
        cv.put(ConfigHelper.CONFIG_SII_VALE_SERVER_CONSULTA_QR, "");
        cv.put(ConfigHelper.CONFIG_SII_VALE_MODALIDAD, "");
        cv.put(ConfigHelper.CONFIG_SII_VALE_SERVER_PLATFORM, "");


        if (!TextUtils.isEmpty(configValeEstadoActual) && !configValeEstadoActual.equalsIgnoreCase(ValeConst.VALE_STATUS_DESACTIVADO)) {
            //solo para loguear cuando cambio de estado
            Logger.RegistrarEvento(mCtx, "i", "VALE-HABILITACION", "DESHABILITADO DESDE PLATAFORMA");
        }


    }


    // onPreExecute used to setup the AsyncTask.
    @Override
    protected void onPreExecute() {

        ((MainActivity) mCtx).showProgress(true);
        ((MainActivity) mCtx).showProgressMessage("Obteniendo configuración desde el servidor..");
    }

    @Override
    protected String doInBackground(Void... params) {
        try {

            if (!ConnectivityUtils.isOnline(mCtx)) {
                return "No hay una conexión disponible";
            }

            ContentValues cv = getConfigFromServer();


            try {
                valoresConfigurados(cv);
                UpdateConfig(cv);
                return "ok";
            } catch (Exception e) {
                e.printStackTrace();
                _FIRST_CONFIG = true; //MARCAMOS ESTE FLAG PARA QUE HAGA EL COMPORTAMIENTO DE MOSTRAR MENSAJE Y SALIR
                return "Terminal configurado incorrectamente. "+ e.getMessage()+" .Contacte a soporte técnico";
            }



        } catch (IOException e) {
            return "No es posible conectarse. Intente mas tarde.";
        } catch (Exception e) {
            return "Error: " + e.getMessage();
        }
    }

    @Override
    protected void onProgressUpdate(String... p) {
        //Toast.makeText(mCtx, p[0], Toast.LENGTH_LONG).show();

        ((MainActivity) mCtx).showProgressMessage(p[0]);
    }

    // onPostExecute displays the results of the AsyncTask.
    @Override
    protected void onPostExecute(String result) {
        //((MainActivity)mCtx).showProgressMessage(result);
        ((MainActivity) mCtx).showProgress(false);

        if (!result.equals("ok")) { //mostramos solo en caso de no haber finalizado correctamente
            Cancel(result);
        } else {

            Finalizar(EventsFlow.CONFIG_OBTENIDA);

            //((MainActivity) mCtx).AjustaPerfilComercio(true);

            /*if (_FIRST_CONFIG)
                ((MainActivity) mCtx).Resume();
            else {
                ((MainActivity) mCtx).AjustaPerfilComercio(true);
                ((MainActivity) mCtx).SyncDataToServer(false);
            }*/
        }
    }

    @Override
    protected void onCancelled(String s) {
        //((MainActivity)mCtx).showProgressMessage(s);
        ((MainActivity) mCtx).showProgress(false);

        Cancel(s);
    }

    private void Cancel(String s) {

        Finalizar(EventsFlow.CONFIG_NOOBTENIDA);

        Logger.RegistrarEvento(mCtx, "e", "GetConfigProcess", s);

        if (_FIRST_CONFIG)
            AlertAndClose(s); //MOSTRAMOS ERROR Y SALIMOS
        /*else {
            ((MainActivity) mCtx).AjustaPerfilComercio(false);
            //((MainActivity) mCtx).SyncDataToServer(false);
        }*/
    }

    private void AlertAndClose(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
        builder.setTitle("Obtener configuración del servidor");
        builder.setMessage(Html.fromHtml(message + "<br/>ID:" + _config.ANDROID_ID + "<br/>IMEI:" + _config.IMEI()));
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((MainActivity) mCtx).Salir();
            }
        });
        builder.setCancelable(false);
        builder.show();
    }


    private void Finalizar(EventsFlow result) {

        RESULT = result;

        if (RESULT == EventsFlow.CONFIG_CANCELADA && _FIRST_CONFIG)
            ((MainActivity) mCtx).Salir();

        if (RESULT == EventsFlow.CONFIG_NOOBTENIDA && !_FIRST_CONFIG) {
            ((MainActivity) mCtx).AjustaPerfilComercio(false);
            ((MainActivity) mCtx).Init_Flow(3);
        }

        if (RESULT == EventsFlow.CONFIG_OBTENIDA) {
            ((MainActivity) mCtx).AjustaPerfilComercio(true);
            ((MainActivity) mCtx).Init_Flow(3);

        }
    }
}
