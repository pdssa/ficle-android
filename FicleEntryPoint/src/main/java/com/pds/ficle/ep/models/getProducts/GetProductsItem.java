package com.pds.ficle.ep.models.getProducts;

import com.google.gson.annotations.SerializedName;

public class GetProductsItem {

    @SerializedName("gtin")
    private String mGtin;
    @SerializedName("ispCode")
    private String mIspCode;
    @SerializedName("name")
    private String mName;
    @SerializedName("brand")
    private String mBrand;
    @SerializedName("content")
    private double mContent;
    @SerializedName("unit")
    private String mUnit;
    @SerializedName("dept")
    private String mDept;
    @SerializedName("subDept")
    private String mSubDept;
    @SerializedName("price")
    private double mPrice;
    @SerializedName("purchasePrice")
    private double mPurchasePrice;
    @SerializedName("totalStock")
    private int mStock;

    public int getStock() {
        return mStock;
    }

    public String getGtin() {
        return mGtin;
    }

    public String getIspCode() {
        return mIspCode;
    }

    public String getName() {
        return mName;
    }

    public String getBrand() {
        return mBrand;
    }

    public double getContent() {
        return mContent;
    }

    public String getUnit() {
        return mUnit;
    }

    public String getDept() {
        return mDept;
    }

    public String getSubDept() {
        return mSubDept;
    }

    public double getPrice() {
        return mPrice;
    }

    public double getPurchasePrice() {
        return mPurchasePrice;
    }
}
