package com.pds.ficle.ep.cloud;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Environment;
import android.webkit.URLUtil;
import android.widget.Toast;

import com.pds.common.Config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hernan on 30/10/2014.
 */
/*

public class GetImagesTask extends AsyncTask<String, String, String>{

    private Context _context;
    private Config _config;

    public void set_context(Context _context) {
        this._context = _context;
    }

    public GetImagesTask(Context _context) {
        this._context = _context;
        this._config = new Config(_context);
    }

    //tiene conectividad?
    private boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager)
                _context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        return (networkInfo != null && networkInfo.isConnected());

        //si queres saber que tipo de conexion está activa
        //NetworkInfo networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        //boolean isWifiConn = networkInfo.isConnected();
        //networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        //boolean isMobileConn = networkInfo.isConnected();
    }

    private List<String> GetImagesListFromServer(String _catalog) throws Exception {
        String _url = "http://" + this._config.CLOUD_SERVER_HOST + ":" + this._config.CLOUD_SERVER_PORT + "/download/img/" + _catalog + ".txt";
        URL url = new URL(_url);

        BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

        List<String> str = new ArrayList<String>();
        String _str ;
        str.add(in.readLine());
        while ((_str = in.readLine()) != null) {
            str.add(_str);
        }

        in.close();

        return str;
    }

    private static boolean isSdPresent() {
        return Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
    }

    public void DownloadImage(String _url) throws Exception {
        String _fileName = "";
        if(_url.contains("/")) {

            String[] _comp = _url.split("/");

            _fileName = _comp[_comp.length - 1];

            String fullFilePath = Environment.getExternalStorageDirectory() + "/Productos/" + _fileName;

            File outputFile = new File(fullFilePath);
            //if(!outputFile.exists()){//evitamos descargarlo si ya existe
                DownloadFile(_url, Environment.getExternalStorageDirectory() + "/Productos/", _fileName);
            //}
        }
    }

    public void DownloadFile(String _url, String _destinationPath, String _fileName) throws Exception {
        try {

            if (!isSdPresent()) {
                throw new Exception("Tarjeta SD no disponible");
            }

            if(!URLUtil.isValidUrl(_url)){
                throw new Exception("URL no válida");
            }

            URL url = new URL(_url);
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(false);
            c.connect();

            File file = new File(_destinationPath); // PATH = /mnt/sdcard/Productos/
            if (!file.exists()) {//creamos el dir si no existe
                file.mkdirs();
            }
            File outputFile = new File(file, _fileName);
            */
/*if(file.exists()){//lo borramos para sobreescribirlo
                file.delete();
            }*//*

            FileOutputStream fos = new FileOutputStream(outputFile);

            InputStream is = c.getInputStream(); // Get from Server and Catch In Input Stream Object.

            byte[] buffer = new byte[1024];
            int len1 = 0;
            while ((len1 = is.read(buffer)) != -1) {
                fos.write(buffer, 0, len1);// Write In FileOutputStream.
            }
            fos.close();
            is.close();//en este punto el file ya deberia estar descargado en el storage

        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    protected String doInBackground(String... catalogs) {
        try {

            if(!this.isOnline()){
                return "No hay una conexion disponible";
            }

            //1-connect and get new versions
            publishProgress("Obteniendo lista de imagenes del servidor...");

            List<String> catalog = GetImagesListFromServer(catalogs[0]);

            //2-fetch response and download all files
            publishProgress("Descargando imagenes del servidor. Aguarde por favor...");
            boolean processOk = false;
            for(String imageUrl : catalog){
                DownloadImage(imageUrl);
                processOk = true;
            }

            return processOk ? "Imagenes descargadas correctamente" : "Hubo un error o no se encontraron imagenes para descargar";

        } catch (IOException e) {
            //return "No es posible conectarse. Intente mas tarde.";
            return "Error IO: " + e.getMessage();
        } catch (Exception e) {
            return "Error: " + e.getMessage();
        }
    }

    @Override
    protected void onProgressUpdate(String... p) {
        Toast.makeText(_context, p[0], Toast.LENGTH_LONG).show();
    }

    // onPostExecute displays the results of the AsyncTask.
    @Override
    protected void onPostExecute(String result) {
        Toast.makeText(_context, result, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onCancelled(String s) {
        Toast.makeText(_context, s, Toast.LENGTH_LONG).show();
    }
}
*/
