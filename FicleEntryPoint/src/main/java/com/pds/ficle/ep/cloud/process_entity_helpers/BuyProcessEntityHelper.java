package com.pds.ficle.ep.cloud.process_entity_helpers;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.google.gson.Gson;
import com.pds.common.Config;
import com.pds.common.Logger;
import com.pds.common.dao.ProductDao;
import com.pds.common.db.CompraDetalleTable;
import com.pds.common.db.CompraTable;
import com.pds.common.model.Product;
import com.pds.ficle.ep.cloud.http_post_helpers.BuyHttpPostHelper;
import com.pds.ficle.ep.models.ApiResponse;
import com.pds.ficle.ep.models.syncBuys.BuyDetailRequestItemWA;
import com.pds.ficle.ep.models.syncBuys.BuyRequestItemWA;
import com.pds.ficle.ep.models.syncBuys.BuyRequestWA;
import com.pds.ficle.ep.models.syncBuys.BuyResponseWA;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class BuyProcessEntityHelper extends ProcessEntityHelper {

    private static final String TAG = BuyProcessEntityHelper.class.getSimpleName();

    public BuyProcessEntityHelper(Context ctx, Config config) {
        super(ctx, config);
    }


    @Override
    protected void updateEntitySyncState(JSONObject jsonObject) throws JSONException {
        ContentValues cv = new ContentValues();
        cv.put(CompraTable.COMPRA_ID, jsonObject.getInt(CompraTable.COMPRA_ID));
        cv.put(CompraTable.COMPRA_SYNC_STATUS, "S");
        Uri uri = ContentUris.withAppendedId(Uri.parse("content://com.pds.ficle.ep.provider/compras"), jsonObject.getInt(CompraTable.COMPRA_ID));
        mCtx.getContentResolver().update(uri, cv, null, null);

        try {
            for (JSONObject det:
                    getDetails(jsonObject.getInt(CompraTable.COMPRA_ID))) {

                ContentValues cvDetails = new ContentValues();
                cvDetails.put(CompraDetalleTable.COMPRA_DET_ID, jsonObject.getInt(CompraDetalleTable.COMPRA_DET_ID));
                cvDetails.put(CompraDetalleTable.COLUMN_SYNC_STATUS, "S");

                Uri uriDetails = ContentUris.withAppendedId(Uri.parse("content://com.pds.ficle.ep.provider/compra_det"), jsonObject.getInt(CompraDetalleTable.COMPRA_DET_ID));

                mCtx.getContentResolver().update(uriDetails, cvDetails, null, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected boolean isSuccess(String response) {
        BuyResponseWA resp = new Gson().fromJson(response, BuyResponseWA.class);
        return resp.getResponseCode() == 0;
    }

    @Override
    protected String getEntityType() {
        return "COMPRAS";
    }

    @Override
    protected String getHttpPostDataToServer(String request) throws Exception {
        BuyHttpPostHelper helper = new BuyHttpPostHelper(mConfig);
        return helper.postDataToServer(request);
    }

    @Override
    protected String getJsonRequest() throws Exception {
        BuyRequestWA request = new BuyRequestWA();

        ProductDao prodDao = new ProductDao(mCtx.getContentResolver(), false, false);

        request.mAndroidId = mConfig.ANDROID_ID;
        request.mImei = mConfig.IMEI();
        request.mItems = new ArrayList<BuyRequestItemWA>();
        ArrayList<JSONObject> items = getPayload();
        if (items != null) {
            for (JSONObject item : items) {

                try{
                    BuyRequestItemWA requestItemWA = new BuyRequestItemWA();
                    long itemId = item.getLong(CompraTable.COMPRA_ID);
                    requestItemWA.setId(itemId);
                    requestItemWA.setInvoiceNumber(item.getString(CompraTable.COMPRA_NUMERO_FC));
                    requestItemWA.setProviderId(item.getInt(CompraTable.COMPRA_PROVEEDOR_ID));
                    requestItemWA.setDate(item.getString(CompraTable.COMPRA_FECHA));
                    requestItemWA.setSubTotalX(item.getDouble(CompraTable.COMPRA_SUBTOTAL_X));
                    requestItemWA.setSubTotalNG(item.getDouble(CompraTable.COMPRA_SUBTOTAL_NG));
                    requestItemWA.setSubTotalI(item.getDouble(CompraTable.COMPRA_SUBTOTAL_I));
                    requestItemWA.setTotal(item.getDouble(CompraTable.COMPRA_TOTAL));
                    requestItemWA.setPaymentWay(item.getString(CompraTable.COMPRA_MEDIO_PAGO));
                    requestItemWA.setCode(item.getString(CompraTable.COMPRA_CODIGO));
                    requestItemWA.setUserId(item.getInt(CompraTable.COMPRA_USER_ID));


                    ArrayList<JSONObject> details = getDetails(itemId);
                    if (details != null) {
                        for (JSONObject det:
                                details) {

                            BuyDetailRequestItemWA detail = new BuyDetailRequestItemWA();

                            detail.setId(det.getInt(CompraDetalleTable.COMPRA_DET_ID));
                            detail.setOrigId(det.getInt(CompraDetalleTable.COMPRA_DET_COMPRA_ID));
                            detail.setProductId(det.getInt(CompraDetalleTable.COMPRA_DET_PRODUCTO_ID));

                            Product p = prodDao.find(det.getInt(CompraDetalleTable.COMPRA_DET_PRODUCTO_ID));
                            if(p != null)
                                detail.setGtin(p.getCode());

                            detail.setQuantity(det.getInt(CompraDetalleTable.COMPRA_DET_CANTIDAD));
                            detail.setPrice(det.getDouble(CompraDetalleTable.COMPRA_DET_PRECIO));
                            detail.setSubTotalX(det.getDouble(CompraDetalleTable.COMPRA_DET_SUBTOTAL_X));
                            detail.setSubTotalNG(det.getDouble(CompraDetalleTable.COMPRA_DET_SUBTOTAL_NG));
                            detail.setSubTotalI(det.getDouble(CompraDetalleTable.COMPRA_DET_SUBTOTAL_I));
                            detail.setTotal(det.getDouble(CompraDetalleTable.COMPRA_DET_TOTAL));


                            requestItemWA.addDetail(detail);
                        }
                    }



                    request.mItems.add(requestItemWA);

                }catch (Exception ex){
                    Log.e(TAG, ex.getMessage() + "" + ex.toString(), ex);
                    Logger.RegistrarEvento(mCtx, "e", "SyncProcess", ex.getMessage() + "" + ex.toString(), "Ent:" + getEntityType());
                }
            }
            return new Gson().toJson(request);
        }
        return null;
    }

    @Override
    protected ArrayList<JSONObject> getPayload() throws Exception {
        Cursor ventas_cursor = getCursor(
                "content://com.pds.ficle.ep.provider/compras",
                CompraTable.columnas,
                CompraTable.COMPRA_SYNC_STATUS + "= 'N' ",
                "");
        return  cursorToJSONWebApi(ventas_cursor);
    }


    protected ArrayList<JSONObject> getDetails(long itemId) throws Exception {
        Cursor cursor = getCursor(
                "content://com.pds.ficle.ep.provider/compra_det",
                CompraDetalleTable.columnas,
                CompraDetalleTable.COLUMN_SYNC_STATUS + "= 'N' and "+CompraDetalleTable.COMPRA_DET_COMPRA_ID+"= "+itemId,
                "");


        return  cursorToJSONWebApi(cursor);
    }
}
