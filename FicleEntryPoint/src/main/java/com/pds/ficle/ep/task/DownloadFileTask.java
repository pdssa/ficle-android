package com.pds.ficle.ep.task;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.pds.common.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Hernan on 21/10/2016.
 */
public class DownloadFileTask extends AsyncTask<String, String, String> {

    private String downloadDir;
    private Context context;

    public DownloadFileTask(Context context, String downloadDir){
        this.downloadDir = downloadDir;
        this.context = context;
    }

    @Override
    protected String doInBackground(String[] urls) {

        try{
            if(urls.length > 0){

                for (String fileUrl : urls) {

                    URL url = new URL(fileUrl);
                    HttpURLConnection c = (HttpURLConnection) url.openConnection();
                    c.setRequestMethod("GET");
                    c.setDoOutput(false);
                    c.connect();

                    //String path = Environment.getExternalStorageDirectory() + "/" + this.downloadDir;
                    File downloadFolder = new File(this.downloadDir); // PATH ejemplo = /mnt/sdcard/download/pds
                    if (!downloadFolder.exists()) {//creamos el dir si no existe
                        downloadFolder.mkdirs();
                    }

                    String fileName = Uri.parse(fileUrl).getLastPathSegment();
                    File downloadedFile = new File(downloadFolder, fileName);

                    File tempFile = new File(downloadFolder, downloadedFile.getName() + ".temp");
                    FileOutputStream fos = new FileOutputStream(tempFile);

                    InputStream is = c.getInputStream(); // Get from Server and Catch In Input Stream Object.

                    byte[] buffer = new byte[1024];
                    int len1 = 0;
                    while ((len1 = is.read(buffer)) != -1) {
                        fos.write(buffer, 0, len1);// Write In FileOutputStream.
                    }
                    fos.close();
                    is.close();//en este punto el temporal ya deberia estar descargado en el storage

                    //rename temp file
                    if (tempFile.exists()) {
                        tempFile.renameTo(downloadedFile);

                        Logger.RegistrarEvento(this.context, "i", "DownloadMessage", fileName );
                    }

                }

                return "";
            }
            else
                return "";
        }
        catch (Exception ex){
            Log.e("DownloadFileTask", "Download error", ex);

            Logger.RegistrarEvento(this.context, "e", "DownloadMessage", ex.getMessage());

            return ex.getMessage();
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }
}
