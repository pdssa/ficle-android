package com.pds.ficle.ep.cloud;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.widget.Toast;

import com.google.gson.Gson;
import com.pds.common.Config;
import com.pds.common.Logger;
import com.pds.common.activity.ActividadProgress;
import com.pds.ficle.ep.MainActivity;
import com.pds.ficle.ep.models.checkDownload.CheckDownloadItem;
import com.pds.ficle.ep.models.checkDownload.CheckDownloadResponse;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by Hernan on 12/06/2014.
 */
public class CheckUpdatesTask extends BaseUpdatesAsyncTask {
    private Config _config;
    private int RESP_OK = -1, RESP_MAL = -1;
    private String downloadFolder = "/download/pds_b/";




    public void set_context(Context _context) {
        this.mCtx = _context;
    }

    public CheckUpdatesTask(Context ctx) {
        super(ctx);
        this._config = new Config(ctx);
    }

    public CheckUpdatesTask(Context ctx, int code_resp_ok, int code_resp_mal) {
        super(ctx);
        this._config = new Config(ctx);
        this.RESP_OK = code_resp_ok;
        this.RESP_MAL = code_resp_mal;
    }

    private String getVersionsFromServer() throws Exception {
        WebServiceRequest webServiceRequest = new WebServiceRequest();

        return webServiceRequest.callWebService(this._config.CLOUD_SERVER_HOST + "api/VersionCheck?androidId=" + this._config.ANDROID_ID);

    }

    private List<AppUpgrade> ParseXML(String xmlIn) throws Exception {
        XmlAppsParser parser = new XmlAppsParser();
        return parser.parse(new ByteArrayInputStream(xmlIn.getBytes("UTF-8")));
    }

    //tiene conectividad?
    private boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager)
                mCtx.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        return (networkInfo != null && networkInfo.isConnected());

        //si queres saber que tipo de conexion está activa
        //NetworkInfo networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        //boolean isWifiConn = networkInfo.isConnected();
        //networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        //boolean isMobileConn = networkInfo.isConnected();
    }


    // onPreExecute used to setup the AsyncTask.
    @Override
    protected void onPreExecute() {
        //Toast.makeText(_context, "Consultando nuevas versiones..", Toast.LENGTH_SHORT).show();

        ((ActividadProgress) mCtx).showProgress(true);
        ((ActividadProgress) mCtx).showProgressMessage("Consultando nuevas versiones..");
    }

    @Override
    protected String doInBackground(Void... params) {

        // params comes from the execute() call: params[0] is the message.
        try {

            if (!this.isOnline()) {
                return "No hay una conexion disponible";
            }

            //1-connect and get new versions
            String jsonIn = getVersionsFromServer();

            if(jsonIn == null)
            {
                return "Proceso finalizado correctamente, no se obtuvieron apps a actualizar";
            }

            publishProgress("Leyendo respuesta del servidor...");

            //2-parse response as AppUpgrade objects
            CheckDownloadResponse response = new Gson().fromJson(jsonIn, CheckDownloadResponse.class);

            publishProgress("Revisando si es necesario actualizar...");

            //3-clear auto download service
            SharedPreferences pref = mCtx.getSharedPreferences("com.pds.ficle.ep", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("apps_to_install", "");
            editor.apply();

            //4-foreach
            boolean flagInstalled = false;

            AppUpgrade app_ep = null;
            for (CheckDownloadItem item:
                 response.getVersions()) {

                if(hasToUpdateApp(item.getPackageName(), item.getLastVersion()))
                {
                    if (isEntryPoint(item.getPackageName(), getFileName(item.getUrl()))) //si es el EP, lo actualizamos al final
                    {
                        app_ep = new AppUpgrade();
                        app_ep.set_context(mCtx);
                        app_ep.set_fileName(getFileName(item.getUrl()));
                        app_ep.set_packageLastVersion(item.getLastVersion());
                        app_ep.set_packageName(item.getPackageName());
                        app_ep.set_url(item.getUrl());
                    }
                    else
                    {
                        publishProgress("Actualizando " + item.getPackageName() + "...");

                        //1-Descargamos
                        String localPath = download(downloadFolder, true, item.getUrl());

                        //2-Actualizamos
                        install(localPath, item.getPackageName(), item.getUrl());

                        flagInstalled = true;
                    }

                    if(app_ep != null){//instalamos el EP al final para evitar cortar el proceso
                        publishProgress("Actualizando " + app_ep.get_packageName() + "...");

                        //1-Descargamos
                        String localPath = download(downloadFolder, true, item.getUrl());

                        //2-Actualizamos
                        install(localPath, item.getPackageName(), item.getUrl());

                        flagInstalled = true;
                    }

                }

            }
           /* //2-parse response as AppUpgrade objects
            List<AppUpgrade> lstApps = ParseXML(jsonIn);

            publishProgress("Revisando si es necesario actualizar...");

            //3-clear auto download service
            SharedPreferences pref = mCtx.getSharedPreferences("com.pds.ficle.ep", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("apps_to_install", "");
            editor.apply();

            //4-foreach
            boolean flagInstalled = false;

            AppUpgrade app_ep = null;
            for (AppUpgrade app : lstApps) {
                app.set_context(mCtx);
                //app.set_fileName("pdvtest.apk");
                if (app.CorrespondeActualizar()) {
                    //si corresponde actualizar:

                    if (app.isEntryPoint()) //si es el EP, lo actualizamos al final
                        app_ep = app;
                    else {
                        publishProgress("Actualizando " + app.get_packageName() + "...");

                        //1-Descargamos
                        app.Download(downloadFolder, true);

                        //2-Actualizamos
                        app.Install();

                        flagInstalled = true;
                    }
                }
            }

            if(app_ep != null){//instalamos el EP al final para evitar cortar el proceso
                publishProgress("Actualizando " + app_ep.get_packageName() + "...");

                //1-Descargamos
                app_ep.Download(downloadFolder, true);

                //2-Actualizamos
                app_ep.Install();

                flagInstalled = true;
            }
*/
            return flagInstalled ? "Proceso finalizado correctamente" : "Todas las aplicaciones se encuentran actualizadas";

        } catch (IOException e) {
            //return "No es posible conectarse. Intente mas tarde.";
            return "Error IO: " + e.getMessage();
        } catch (Exception e) {
            return "Error: " + e.getMessage();
        }
    }

    @Override
    protected void onProgressUpdate(String... p) {
        //Toast.makeText(_context, p[0], Toast.LENGTH_LONG).show();
        ((ActividadProgress) mCtx).showProgressMessage(p[0]);
    }

    // onPostExecute displays the results of the AsyncTask.
    @Override
    protected void onPostExecute(String result) {
        Toast.makeText(mCtx, result, Toast.LENGTH_LONG).show();

        //((MainActivity)_context).showProgressMessage(result);
        ((ActividadProgress) mCtx).showProgress(false);

        Logger.RegistrarEvento(mCtx, "i", "CHECK UPDATES", result);

        Finalizar(!result.startsWith("Error:"));
    }

    @Override
    protected void onCancelled(String s) {
        Toast.makeText(mCtx, s, Toast.LENGTH_LONG).show();

        //((MainActivity)_context).showProgressMessage(s);
        ((ActividadProgress) mCtx).showProgress(false);

        Finalizar(false);
    }


    private void Finalizar(boolean result) {
        if (result && RESP_OK != -1) {
            ((MainActivity) mCtx).Init_Flow(4); //MANDAMOS EL CODIGO DE RESULTADO OK RECIBIDO
        } else if (RESP_MAL != -1) {
            ((MainActivity) mCtx).Init_Flow(RESP_MAL); //MANDAMOS EL CODIGO DE RESULTADO MAL RECIBIDO
        }
    }
}
