package com.pds.ficle.ep.cloud.process_entity_helpers;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.google.gson.Gson;
import com.pds.common.Config;
import com.pds.common.Logger;
import com.pds.common.VentaDetalleHelper;
import com.pds.common.VentasHelper;
import com.pds.common.db.ComprobanteDetalleTable;
import com.pds.common.db.ComprobanteTable;
import com.pds.ficle.ep.cloud.http_post_helpers.VentaHttpPostHelper;
import com.pds.ficle.ep.models.ApiResponse;
import com.pds.ficle.ep.models.syncSales.ComprobanteResponseWA;
import com.pds.ficle.ep.models.syncSales.VentaDetalleRequestItemWA;
import com.pds.ficle.ep.models.syncSales.VentaRequestItemWA;
import com.pds.ficle.ep.models.syncSales.VentaRequestWA;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ComprobanteProcessEntityHelper extends ProcessEntityHelper {
    public ComprobanteProcessEntityHelper(Context ctx, Config config) {
        super(ctx, config);
    }

    @Override
    protected void updateEntitySyncState(JSONObject jsonObject) throws JSONException {
        ContentValues cv = new ContentValues();
        cv.put(ComprobanteTable.COMPROBANTE_ID, jsonObject.getInt(ComprobanteTable.COMPROBANTE_ID));
        cv.put(ComprobanteTable.COMPROBANTE_SYNC_STATUS, "S");
        Uri uri = ContentUris.withAppendedId(Uri.parse("content://com.pds.ficle.ep.provider/comprobantes"), jsonObject.getInt(ComprobanteTable.COMPROBANTE_ID));
        mCtx.getContentResolver().update(uri, cv, null, null);

        try {
            for (JSONObject det :
                    getDetails(jsonObject.getInt(ComprobanteTable.COMPROBANTE_ID))) {

                ContentValues cvDetails = new ContentValues();
                cvDetails.put(ComprobanteDetalleTable.COMPROBANTE_DET_ID, det.getInt(ComprobanteDetalleTable.COMPROBANTE_DET_ID));
                cvDetails.put(ComprobanteDetalleTable.COLUMN_SYNC_STATUS, "S");

                Uri uriDetails = ContentUris.withAppendedId(
                        Uri.parse("content://com.pds.ficle.ep.provider/comprobante_det"),
                        det.getInt(ComprobanteDetalleTable.COMPROBANTE_DET_ID));

                mCtx.getContentResolver().update(uriDetails, cvDetails, null, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    protected boolean isSuccess(String response) {
        ComprobanteResponseWA resp = new Gson().fromJson(response, ComprobanteResponseWA.class);
        return resp.getResponseCode() == 0;
    }

    @Override
    protected String getEntityType() {
        return "COMPROBANTES";
    }

    @Override
    protected String getHttpPostDataToServer(String request) throws Exception {
        VentaHttpPostHelper helper = new VentaHttpPostHelper(mConfig);
        return helper.postDataToServer(request);
    }

    @Override
    protected String getJsonRequest() throws Exception {
        VentaRequestWA request = new VentaRequestWA();
        request.mAndroidId = mConfig.ANDROID_ID;
        request.mImei = mConfig.IMEI();
        request.mItems = new ArrayList<VentaRequestItemWA>();
        ArrayList<JSONObject> items = getPayload();

        if (items != null) {
            for (JSONObject item : items) {

                try {
                    VentaRequestItemWA requestItemWA = new VentaRequestItemWA();
                    long itemId = item.getLong(ComprobanteTable.COMPROBANTE_ID);
                    requestItemWA.setId(itemId);
                    requestItemWA.setTipo("CPB");
                    requestItemWA.setCantidad(item.getInt(ComprobanteTable.COMPROBANTE_CANTIDAD));
                    requestItemWA.setFecha(item.getString(ComprobanteTable.COMPROBANTE_FECHA));
                    requestItemWA.setTotal(item.getDouble(ComprobanteTable.COMPROBANTE_TOTAL));
                    requestItemWA.setHora(item.getString(ComprobanteTable.COMPROBANTE_HORA));
                    requestItemWA.setMedioPago(item.getString(ComprobanteTable.COMPROBANTE_MEDIO_PAGO));
                    requestItemWA.setCodigo(item.getString(ComprobanteTable.COMPROBANTE_CODIGO));
                    requestItemWA.setUserId(item.getInt(ComprobanteTable.COMPROBANTE_USERID));
                    requestItemWA.setFcType(null);
                    requestItemWA.setFcId(-1);
                    requestItemWA.setFcFolio(null);
                    requestItemWA.setFcValidation(null);
                    requestItemWA.setNeto(item.getDouble(ComprobanteTable.COMPROBANTE_NETO));
                    requestItemWA.setIva(item.getDouble(ComprobanteTable.COMPROBANTE_IVA));
                    requestItemWA.setTimbre(null);
                    requestItemWA.setStatus(item.getInt(ComprobanteTable.COMPROBANTE_ANULADA));

                    ArrayList<JSONObject> details = getDetails(itemId);
                    if (details != null) {
                        for (JSONObject det :
                                details) {

                            VentaDetalleRequestItemWA detail = new VentaDetalleRequestItemWA();

                            detail.setId(det.getInt(ComprobanteDetalleTable.COMPROBANTE_DET_ID));
                            detail.setOrigId(det.getInt(ComprobanteDetalleTable.COMPROBANTE_DET_COMPROBANTE_ID));
                            detail.setProductId(det.getInt(ComprobanteDetalleTable.COMPROBANTE_DET_PRODUCTO_ID));
                            detail.setCantidad(det.getInt(ComprobanteDetalleTable.COMPROBANTE_DET_CANTIDAD));
                            detail.setPrecio(det.getDouble(ComprobanteDetalleTable.COMPROBANTE_DET_PRECIO));
                            detail.setTotal(det.getDouble(ComprobanteDetalleTable.COMPROBANTE_DET_TOTAL));
                            detail.setSku(det.getString(ComprobanteDetalleTable.COMPROBANTE_DET_SKU));
                            detail.setNeto(det.getDouble(ComprobanteDetalleTable.COMPROBANTE_DET_NETO));
                            detail.setIva(det.getDouble(ComprobanteDetalleTable.COMPROBANTE_DET_IVA));


                            requestItemWA.addDetail(detail);
                        }
                    }


                    request.mItems.add(requestItemWA);

                } catch (Exception ex) {
                    Log.e("error", ex.getMessage() + "" + ex.toString(), ex);
                    Logger.RegistrarEvento(mCtx, "e", "SyncProcess", ex.getMessage() + "" + ex.toString(), "Ent:" + getEntityType());
                }
            }
            return new Gson().toJson(request);
        }
        return null;
    }

    @Override
    protected ArrayList<JSONObject> getPayload() throws Exception {
        Cursor ventas_cursor = getCursor(
                "content://com.pds.ficle.ep.provider/comprobantes",
                ComprobanteTable.columnas,
                ComprobanteTable.COMPROBANTE_SYNC_STATUS + "= 'N' ",
                "");
        return cursorToJSONWebApi(ventas_cursor);
    }


    protected ArrayList<JSONObject> getDetails(long itemId) throws Exception {
        Cursor cursor = getCursor(
                "content://com.pds.ficle.ep.provider/comprobante_det",
                ComprobanteDetalleTable.columnas,
                ComprobanteDetalleTable.COLUMN_SYNC_STATUS + "= 'N' and " + ComprobanteDetalleTable.COMPROBANTE_DET_COMPROBANTE_ID + "= " + itemId,
                "");


        return cursorToJSONWebApi(cursor);
    }
}
