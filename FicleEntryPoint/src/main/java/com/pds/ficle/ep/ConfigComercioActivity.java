package com.pds.ficle.ep;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Formatos;
import com.pds.common.Logger;
import com.pds.common.SII.vale.util.ValeUtils;
import com.pds.common.dialog.ConfirmDialog;
import com.pds.common.pref.PDVConfig;
import com.pds.common.util.ContentProviders;

import java.util.Date;

import android_serialport_api.Printer;

public class ConfigComercioActivity extends Activity {

    private EditText txtComercio;
    private EditText txtDireccion;
    private EditText txtClaveFiscal;
    private EditText txtIdComercio;
    private EditText txtPerfil;
    private EditText txtPais;
    private EditText txtCatVers;
    private Button btnCancelar;
    private Button btnPrinterTest;

    private Config c;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.config_comercio_activity);

        this.txtComercio = (EditText) findViewById(R.id.config_txt_comercio);
        this.txtDireccion = (EditText) findViewById(R.id.config_txt_direccion);
        this.txtClaveFiscal = (EditText) findViewById(R.id.config_txt_clavefiscal);
        this.txtIdComercio = (EditText) findViewById(R.id.config_txt_id_comercio);
        this.btnCancelar = (Button) findViewById(R.id.config_btn_cancelar);
        this.btnPrinterTest = (Button) findViewById(R.id.config_btn_printer_test);
        this.txtPerfil = (EditText) findViewById(R.id.config_txt_perfil);
        this.txtPais = (EditText) findViewById(R.id.config_txt_pais);
        this.txtCatVers = (EditText) findViewById(R.id.config_txt_catvers);


        this.btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish(); //cerramos la actividad
            }
        });

        this.btnPrinterTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String modelo = c.PRINTER_MODEL;

                try {

                    if(TextUtils.isEmpty(modelo) || modelo.equals("null"))
                        throw new Exception("Impresora no configurada");

                    final Printer printer = Printer.getInstance(modelo, ConfigComercioActivity.this);

                    printer.printerCallback = new Printer.PrinterCallback() {
                        @Override
                        public void onPrinterReady() {

                            printer.test();


                            //PASAR CONFIGURACION...

                            printer.format(1, 2, Printer.ALINEACION.CENTER);
                            printer.printLine("CONFIGURACION");

                            printer.cancelCurrentFormat();

                            printer.lineFeed();

                            printer.printLine(Formatos.FormateaDate(new Date(), Formatos.DateFormatSlashAndTime));

                            printer.lineFeed();

                            printer.printLine("COMERCIO:" + c.ID_COMERCIO);
                            printer.printLine(" " + c.COMERCIO);
                            printer.printLine(" " + c.RAZON_SOCIAL);
                            printer.printLine(" " + c.CLAVEFISCAL);
                            printer.printLine("DIRECCION");
                            printer.printLine(c.DIRECCION);
                            printer.printLine("LOCALIDAD");
                            printer.printLine(c.LOCALIDAD);
                            printer.lineFeed();
                            printer.printLine("PERFIL:" + c.PERFIL);
                            printer.printLine("PAIS:" + c.PAIS);
                            printer.printLine("RUBRO:" + c.RUBRO);

                            printer.printLine("EQUIPO");
                            printer.printLine("  ANDROID ID:" + c.ANDROID_ID);
                            printer.printLine("  IMEI:" + c.IMEI());

                            //modelo de printer
                            String[] printerModelosValues = getResources().getStringArray(R.array.printerModelsValues);
                            String[] printerModelos = getResources().getStringArray(R.array.printerModels);
                            for (int i = 0; i < printerModelosValues.length; i++) {
                                if (printerModelosValues[i].equals(c.PRINTER_MODEL)) {
                                    printer.printLine("  PRINTER:" + printerModelos[i]);
                                    break;
                                }
                            }

                            printer.printLine("  SERVER:" + c.CLOUD_SERVER_HOST);
                            printer.printLine("  TIEMP.INACT.:" + c.INACTIVITY_TIME + " mins");
                            printer.printLine("  HIST.DATOS:" + c.HIST_DAYS + " dias");
                            printer.printLine("  ULTIMO BORRADO: " + getSharedPreferences("com.pds.ficle.ep", Context.MODE_PRIVATE).getString("last_clear_time", "Nunca"));

                            printer.printLine("CAT VERS: v" + String.valueOf(getSharedPreferences("com.pds.ficle.ep", Context.MODE_PRIVATE).getInt("version_catalog", 1)));

                            printer.printLine("SINCRONIZACION");
                            printer.printLine("  ULTIMA: " + getSharedPreferences("com.pds.ficle.ep", Context.MODE_PRIVATE).getString("last_sync_time", "Nunca"));
                            printer.printLine("  INTERV.SINC.:" + c.SYNC_TIME + " mins");
                            printer.printLine("  CAJA:" + c.SYNC_CAJA);
                            printer.printLine("  LOGS:" + c.SYNC_LOGS);
                            printer.printLine("  STOCK:" + c.SYNC_STOCK);

                            printer.lineFeed();

                            if(c.PAIS.equalsIgnoreCase("CH") && c.isValeHabilitado()) {
                                printer.printLine("DATOS - VALE");
                                printer.printLine("  TER.ID:" + c.SII_TERMINAL_ID);
                                printer.printLine("  ESTADO:(" + c.SII_STATUS +") " + (c.isValeInicializado() ? "INIC." : "SIN INIC."));
                                printer.printLine("   MOD.: (" + c.SII_MODALIDAD +") " + c.getValeModalidadLabel());
                                printer.printLine("  GIRO1: " + c.SII_GIRO1);
                                printer.printLine("  GIRO2: " + c.SII_GIRO2);
                                printer.printLine("  GIRO3: " + c.SII_GIRO3);
                                printer.printLine("  RES.:" + c.SII_RESOLUCION_VALE);
                                printer.printLine("    IP:" + c.SII_SERVIDOR_IP);
                                printer.printLine("  PORT:" + c.SII_SERVIDOR_PORT);

                                printer.lineFeed();
                            }

                            if (ContentProviders.ProviderIsAvailable(ConfigComercioActivity.this, PDVConfig.CONTENT_PROVIDER.getAuthority())) {

                                Cursor cursor = getContentResolver().query(PDVConfig.CONTENT_PROVIDER, PDVConfig.PROJECTION, null, null, null);

                                printer.printLine("PDV");

                                if (cursor.moveToFirst()) {

                                    int i = cursor.getColumnIndex(PDVConfig.PDV_PIE_TICKET);
                                    printer.printLine("  PIE TICKET:" + (i != -1 ? cursor.getString(i) : "GRACIAS POR SU VISITA"));
                                    //
                                    i = cursor.getColumnIndex(PDVConfig.PDV_MEDIO_DEFAULT);
                                    printer.printLine("  MEDIO DEF.:" + (i != -1 ? cursor.getString(i) : ""));
                                    //
                                    i = cursor.getColumnIndex(PDVConfig.PDV_AJUSTE_SENCILLO);
                                    printer.printLine("  SENCILLO:" + (i != -1 ? Boolean.parseBoolean(cursor.getString(i)) : false));
                                    //
                                    i = cursor.getColumnIndex(PDVConfig.PDV_IMPRIMIR_AUTOMATICO);
                                    printer.printLine("  AUTO IMPRIMIR:" + (i != -1 ? Boolean.parseBoolean(cursor.getString(i)) : true));
                                    //
                                    i = cursor.getColumnIndex(PDVConfig.PDV_SOLICITAR_CHECKOUT);
                                    printer.printLine("  CHECKOUT:" + (i != -1 ? Boolean.parseBoolean(cursor.getString(i)) : true));
                                    //
                                    i = cursor.getColumnIndex(PDVConfig.PDV_MOSTRAR_DESGLOSE_IMPUESTOS);
                                    printer.printLine("  MOSTRAR IMPUESTOS:" + (i != -1 ? Boolean.parseBoolean(cursor.getString(i)) : true));
                                    //
                                    i = cursor.getColumnIndex(PDVConfig.PDV_TIEMPO_CONFIRMACION);
                                    printer.printLine("  TIEMP.CONFIRM.:" + (i != -1 ? cursor.getString(i) : "0") + " mins");
                                    //
                                    i = cursor.getColumnIndex(PDVConfig.PDV_MOSTRAR_PRODUCTOS_SIN_PRECIO);
                                    printer.printLine("  PROD. SIN PRECIO:" + (i != -1 ? Boolean.parseBoolean(cursor.getString(i)) : true));
                                    //
                                    //i = cursor.getColumnIndex(PDVConfig.PDV_EMITIR_VALE);
                                    //printer.printLine("  EMISION VALES:" + (i != -1 ? Boolean.parseBoolean(cursor.getString(i)) : false));
                                    //
                                    i = cursor.getColumnIndex(PDVConfig.PDV_COMPROB_HABILIT);
                                    printer.printLine("  COMPROB. HABILIT.:" + (i != -1 ? Boolean.parseBoolean(cursor.getString(i)) : false));
                                    //
                                    i = cursor.getColumnIndex(PDVConfig.PDV_ACTUALIZ_PRECIO_LISTA);
                                    printer.printLine("  ACTUALIZ. PRECIO CAT.:" + (i != -1 ? Boolean.parseBoolean(cursor.getString(i)) : true));
                                    //...
                                }
                            }
                            printer.footer();
                            //printer.end();

                            if(printer.isCashDrawerActivated()) {
                                new ConfirmDialog(ConfigComercioActivity.this, "APERTURA CAJON", "¿Desea probar la apertura del cajón?")
                                        .set_positiveButtonText("ACEPTAR")
                                        .set_positiveButtonAction(new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                printer.openCashDrawer();
                                            }
                                        })
                                        .set_negativeButtonText("CANCELAR")
                                        .set_negativeButtonAction(new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        })
                                        .set_icon(R.drawable.cashdrawer_icon)
                                        .show();
                            }
                        }
                    };


                    if (!printer.initialize()) {
                        Toast.makeText(ConfigComercioActivity.this, "Error al inicializar", Toast.LENGTH_SHORT).show();
                        printer.end();
                    }

                } catch (Exception ex) {
                    Toast.makeText(ConfigComercioActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        LeerConfig();
    }

    public void LeerConfig() {

        try {
            c = new Config(this, true);

            this.txtComercio.setText(c.COMERCIO);
            this.txtDireccion.setText(c.DIRECCION);
            this.txtClaveFiscal.setText(c.CLAVEFISCAL);
            this.txtIdComercio.setText(c.ID_COMERCIO);
            this.txtPerfil.setText(c.PERFIL);
            this.txtPais.setText(c.PAIS);
            this.txtCatVers.setText(String.valueOf(getSharedPreferences("com.pds.ficle.ep", Context.MODE_PRIVATE).getInt("version_catalog", 1)));

        } catch (Exception ex) {
            Toast.makeText(this, "Error al leer la configuracion:" + ex.toString(), Toast.LENGTH_LONG).show();
        }
    }
}
