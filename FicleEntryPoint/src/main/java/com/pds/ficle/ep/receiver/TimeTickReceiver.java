package com.pds.ficle.ep.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Hernan on 19/11/2014.
 */
public class TimeTickReceiver extends BroadcastReceiver {

    private final SimpleDateFormat _dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    private final SimpleDateFormat _timeFormat = new SimpleDateFormat("HH:mm");
    private TextView txtFecha;
    private TextView txtHora;

    private String scheduledHour = "04:00";

    public IntentFilter getIntentFilter() {
        return intentFilter;
    }

    public IntentFilter intentFilter;

    public TimeTickReceiver(TextView _txtFecha, TextView _txtHora){
        intentFilter = new IntentFilter(Intent.ACTION_TIME_TICK);
        txtFecha = _txtFecha;
        txtHora = _txtHora;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().compareTo(Intent.ACTION_TIME_TICK) == 0) {
            Date now = new Date();
            String hora = _timeFormat.format(now);

            txtFecha.setText(_dateFormat.format(now));
            txtHora.setText(hora);

            //si es la hora configurada para sync de info => lo hacemos
            /*if(hora.equals(scheduledHour) && interactionListener != null)
                interactionListener.onSyncScheduled();
                */

        }
    }

    public OnTimeTickInteractionListener interactionListener;

    public void setInactivityListener(OnTimeTickInteractionListener listener) {
        this.interactionListener = listener;
    }

    public interface OnTimeTickInteractionListener {
        void onSyncScheduled();
    }

}
