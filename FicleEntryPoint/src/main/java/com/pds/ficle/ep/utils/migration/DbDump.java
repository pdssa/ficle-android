package com.pds.ficle.ep.utils.migration;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Hernan on 13/01/2015.
 */
public class DbDump {

    private String mDestXmlPath = "/sdcard/export.xml";

    private SQLiteDatabase mDb;
    private Exporter mExporter;

    public interface MigrationInterface {
        void onTableExported(String message);
    }

    public DbDump(SQLiteDatabase db, String destXml) {
        mDb = db;
        mDestXmlPath = destXml;

    }

    public int exportData(boolean unicoFile, MigrationInterface migrationInterface) {

        int cant_tablas = 0;

        try {

            // get the tables out of the given sqlite database
            String sql = "SELECT * FROM sqlite_master where type = 'table'";

            Cursor cur = mDb.rawQuery(sql, new String[0]);
            cur.moveToFirst();

            String tableName;

            if (unicoFile) {

                try {
                    // create a file on the sdcard to export the
                    // database contents to
                    File myFile = new File(mDestXmlPath + "export.xml");
                    myFile.createNewFile();

                    FileOutputStream fOut = new FileOutputStream(myFile);
                    BufferedOutputStream bos = new BufferedOutputStream(fOut);

                    mExporter = new Exporter(bos);
                } catch (FileNotFoundException e) {
                    migrationInterface.onTableExported("__    ERROR: " + e.getMessage());
                } catch (IOException e) {
                    migrationInterface.onTableExported("__    ERROR: " + e.getMessage());
                }

                mExporter.setUniqueFile(unicoFile);
                mExporter.startDbExport(mDb.getPath());

                while (cur.getPosition() < cur.getCount()) {
                    tableName = cur.getString(cur.getColumnIndex("name"));

                    // don't process these two tables since they are used
                    // for metadata
                    if (!tableName.equals("android_metadata")
                            && !tableName.equals("sqlite_sequence")
                            && !tableName.equals("apps")
                            && !tableName.equals("productos")
                            && !tableName.equals("alicuotas")) {

                        migrationInterface.onTableExported("    EXPORTANDO TABLA " + tableName.toUpperCase());

                        exportTable(tableName);

                        cant_tablas++;
                    }

                    cur.moveToNext();
                }
                mExporter.endDbExport();
                mExporter.close();

            } else {
                while (cur.getPosition() < cur.getCount()) {

                    tableName = cur.getString(cur.getColumnIndex("name"));

                    // don't process these two tables since they are used
                    // for metadata
                    if (!tableName.equals("android_metadata")
                            && !tableName.equals("sqlite_sequence")
                            && !tableName.equals("apps")
                            && !tableName.equals("productos")
                            && !tableName.equals("alicuotas")) {

                        try {
                            // create a file on the sdcard to export the
                            // database contents to
                            File myFile = new File(mDestXmlPath + tableName + ".xml");
                            myFile.createNewFile();

                            FileOutputStream fOut = new FileOutputStream(myFile);
                            BufferedOutputStream bos = new BufferedOutputStream(fOut);

                            mExporter = new Exporter(bos);
                        } catch (FileNotFoundException e) {
                            migrationInterface.onTableExported("__    ERROR: " + e.getMessage());
                        } catch (IOException e) {
                            migrationInterface.onTableExported("__    ERROR: " + e.getMessage());
                        }

                        mExporter.setUniqueFile(unicoFile);
                        mExporter.startDbExport(mDb.getPath());

                        migrationInterface.onTableExported("    EXPORTANDO TABLA " + tableName.toUpperCase());

                        exportTable(tableName);

                        cant_tablas++;

                        mExporter.endDbExport();
                        mExporter.close();
                    }

                    cur.moveToNext();

                }

            }

        } catch (IOException e) {
            migrationInterface.onTableExported("__    ERROR: " + e.getMessage());
        } catch (Exception e) {
            migrationInterface.onTableExported("__    ERROR: " + e.getMessage());
        }

        return cant_tablas;
    }

    private void exportTable(String tableName) throws IOException {
        mExporter.startTable(tableName);

        // get everything from the table
        String sql = "select * from " + tableName;
        Cursor cur = mDb.rawQuery(sql, new String[0]);
        int numcols = cur.getColumnCount();

        cur.moveToFirst();

        // move through the table, creating rows
        // and adding each column with name and value
        // to the row
        while (cur.getPosition() < cur.getCount()) {
            mExporter.startRow();
            String name;
            String val;
            for (int idx = 0; idx < numcols; idx++) {
                name = cur.getColumnName(idx);
                val = cur.getString(idx);

                //pasamos a xml entities
                if (val != null)
                    val = val.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;");

                mExporter.addColumn(name, val);
            }

            mExporter.endRow();
            cur.moveToNext();
        }

        cur.close();

        mExporter.endTable();
    }

    class Exporter {
        private static final String CLOSING_WITH_TICK = "'>";
        private static final String START_DB = "<export-database name='";
        private static final String END_DB = "</export-database>";
        private static final String START_TABLE = "<table name='";
        private static final String END_TABLE = "</table>";
        private static final String START_ROW = "<row>";
        private static final String END_ROW = "</row>";
        private static final String START_COL = "<col name='";
        private static final String END_COL = "</col>";

        private BufferedOutputStream mbufferos;

        private boolean uniqueFile;

        public void setUniqueFile(boolean _uniqueFile) {
            this.uniqueFile = _uniqueFile;
        }

        public Exporter() throws FileNotFoundException {
            this(new BufferedOutputStream(new FileOutputStream(mDestXmlPath)));
        }

        public Exporter(BufferedOutputStream bos) {
            mbufferos = bos;
        }

        public void close() throws IOException {
            if (mbufferos != null) {
                mbufferos.close();
            }
        }

        public void startDbExport(String dbName) throws IOException {
            String stg = START_DB + dbName + CLOSING_WITH_TICK;
            mbufferos.write(stg.getBytes());
        }

        public void endDbExport() throws IOException {
            mbufferos.write(END_DB.getBytes());
        }

        public void startTable(String tableName) throws IOException {
            String stg = START_TABLE + tableName + CLOSING_WITH_TICK;
            mbufferos.write(stg.getBytes());
        }

        public void endTable() throws IOException {
            mbufferos.write(END_TABLE.getBytes());
        }

        public void startRow() throws IOException {
            mbufferos.write(START_ROW.getBytes());
        }

        public void endRow() throws IOException {
            mbufferos.write(END_ROW.getBytes());
        }

        public void addColumn(String name, String val) throws IOException {
            String stg = START_COL + name + CLOSING_WITH_TICK + val + END_COL;
            mbufferos.write(stg.getBytes());
        }
    }
}
