package com.pds.ficle.ep;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Logger;
import com.pds.common.activity.TimerActivity;
import com.pds.ficle.ep.utils.PassGenerator;

import java.util.Date;

/**
 * Created by Hernan on 06/12/13.
 */
public class ConfigUserActivity extends TimerActivity {
    private static final String TAG = ConfigUserActivity.class.getSimpleName();
   /* private EditText txtComercio;
    private EditText txtDireccion;
    private EditText txtClaveFiscal;
    private EditText txtIdComercio;
    private EditText txtPerfil;
    //private TextView txtIMEI;
    //private TextView txtSERIAL_SIM;
    //private TextView txtIMSI;
    //private TextView txtRecargaNroComercio;
    //private TextView txtRecargaIdTerminal;
    private Button btnGrabar;*/
    private Button btnCancelar;
//    private Spinner spnPrinterModel;
//    private Button btnPrinterTest;
//    private Button btnPrintConfig;
//
//    private String[] printerModelosValues;
//
//    //private Config c;
//
//    //CONFIG DE BOLETA
//    private TextView boleta_txtHora1;
//    private TextView boleta_txtHora2;
//
//    //CONFIG DE PDV
//    private EditText pdv_txtPieTicket;
//    private Spinner pdv_spnMedios;
//    private String[] mediosValues;
//    private CheckBox pdv_chkAjusteSencillo, pdv_chkShowCheckout, pdv_chkShowImpuestos, pdv_chkPrintAutomatic, pdv_chkProdSinPrecios, pdv_chkEmitirComprob, pdv_chkActualizPrecioCatalogo;
//    private EditText pdv_txtConfirmTime;
//
//    private boolean mostrarAdvertenciaReinicio;
//
//    //variables para comparar
//    private String old_hora_a;
//    private String old_hora_b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.config_user_layout);

        this.btnCancelar = (Button) findViewById(R.id.config_btn_cancelar);


        //****************CONFIG DEL COMERCIO*********************
        findViewById(R.id.config_btn_comercio).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //CambiarEstadoPanel(findViewById(R.id.config_comercio_layout));
                startActivity(new Intent(ConfigUserActivity.this, ConfigComercioActivity.class));
            }
        });


        //****************CONFIG DE PDV*********************
        findViewById(R.id.config_btn_pdv).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //CambiarEstadoPanel(findViewById(R.id.config_pdv_layout));
                startActivity(new Intent(ConfigUserActivity.this, ConfigPDVActivity.class));
            }
        });


        this.btnCancelar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                finish(); //cerramos la actividad
            }
        });


        findViewById(R.id.config_btn_tecnico).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent i = new Intent(view.getContext(), config_activity.class);
                //startActivity(i);

                final Config _config = new Config(ConfigUserActivity.this);

                //vamos a solicitar la clave de tecnico para ingresar a la edicion de la configuracion
                AlertDialog.Builder builder = new AlertDialog.Builder(ConfigUserActivity.this);
                // Add action buttons
                builder
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                checkConfigPassword(((EditText) ((AlertDialog) dialog).findViewById(R.id.dialog_clave_config_txt_pass)).getText().toString().trim(), _config.IMEI(), _config.ANDROID_ID);
                            }
                        })
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });

                final AlertDialog dialog = builder.create();

                // Get the layout inflater
                LayoutInflater inflater = ConfigUserActivity.this.getLayoutInflater();

                // Inflate and set the layout for the dialog
                // Pass null as the parent view because its going in the dialog layout
                View vw = inflater.inflate(R.layout.dialog_clave_config, null);

                ((TextView) vw.findViewById(R.id.dialog_clave_config_txt_id)).setText(_config.ANDROID_ID);
                ((TextView) vw.findViewById(R.id.dialog_clave_config_txt_imei)).setText(_config.IMEI());
                ((EditText) vw.findViewById(R.id.dialog_clave_config_txt_pass)).setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            checkConfigPassword(v.getText().toString(), _config.IMEI(), _config.ANDROID_ID);
                            dialog.dismiss();
                            return true;
                        }
                        return false;
                    }
                });

                dialog.setView(vw);
                dialog.setTitle("Ingrese Clave de Técnico");
                dialog.setCanceledOnTouchOutside(false);
                dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                    }
                });

                dialog.show();
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

        //LeerConfig();
    }

    private void checkConfigPassword(String passIngresada, String IMEI, String ID) {
        try {
            String claveTecnico = PassGenerator.GenerarClaveTecnico(new Date(), IMEI, ID);
            Log.e(TAG, "checkConfigPassword: CLAVE TECNICA: "+ claveTecnico);
            if (passIngresada.equals(claveTecnico)) {
                Logger.RegistrarEvento(ConfigUserActivity.this, "i", "Configuracion", "Acceso a menu tecnico");

                Intent i = new Intent(ConfigUserActivity.this, config_activity.class);
                startActivity(i);
            } else {
                Toast.makeText(ConfigUserActivity.this, "Clave ingresada erronea", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(ConfigUserActivity.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }


}