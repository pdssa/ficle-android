package com.pds.ficle.ep.models.syncSales;

import com.google.gson.annotations.SerializedName;
import com.pds.ficle.ep.models.BaseTrnxRequestItemWA;

import java.util.ArrayList;

public class VentaRequestItemWA  extends BaseTrnxRequestItemWA {
    public VentaRequestItemWA()
    {
        super();
        this.mItems = new ArrayList<VentaDetalleRequestItemWA>();
    }

    @SerializedName("SaleDetails")
    public ArrayList<VentaDetalleRequestItemWA> mItems;


    public void addDetail(VentaDetalleRequestItemWA detail) {
        mItems.add(detail);
    }

}
