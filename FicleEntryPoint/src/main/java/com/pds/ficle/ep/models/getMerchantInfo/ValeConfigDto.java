package com.pds.ficle.ep.models.getMerchantInfo;

import com.google.gson.annotations.SerializedName;

public class ValeConfigDto {


    @SerializedName("terminalId")
    private String mTerminalId;
    @SerializedName("merchantId")
    private String mMerchantId;
    @SerializedName("cat1")
    private String mCat1;
    @SerializedName("cat2")
    private String mCat2;
    @SerializedName("cat3")
    private String mCat3;
    @SerializedName("resolutionText")
    private String mResolutionText;
    @SerializedName("siiCategory")
    private int mSiiCategory;
    @SerializedName("isServiceEnabled")
    private boolean mIsServiceEnabled;
    @SerializedName("valeIp")
    private String mValeIp;
    @SerializedName("valePort")
    private int mValePort;
    @SerializedName("pathQR")
    private String mPathQr;
    @SerializedName("valeOperationMode")
    private String mOperationMode;

    public String getTerminalId() {
        return mTerminalId;
    }

    public String getCat1() {
        return mCat1;
    }

    public String getCat2() {
        return mCat2;
    }

    public String getCat3() {
        return mCat3;
    }

    public String getResolutionText() {
        return mResolutionText;
    }

    public int getSiiCategory() {
        return mSiiCategory;
    }

    public boolean isServiceEnabled() {
        return mIsServiceEnabled;
    }

    public String getValeIp() {
        return mValeIp;
    }

    public int getValePort() {
        return mValePort;
    }

    public String getPathQr() {
        return mPathQr;
    }

    public String getOperationMode() {
        return mOperationMode;
    }

    public String getMerchantId() {
        return mMerchantId;
    }
}
