package com.pds.ficle.ep.cloud;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.util.Xml;

import com.google.gson.Gson;
import com.pds.common.Logger;
import com.pds.ficle.ep.models.checkDownload.CheckDownloadItem;
import com.pds.ficle.ep.models.checkDownload.CheckDownloadResponse;

import org.xmlpull.v1.XmlSerializer;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jose on 7/20/16.
 */
public class CheckDownloadTask extends BaseUpdatesAsyncTask {

    private static String TAG = CheckDownloadTask.class.getSimpleName();

    private String HOST = null;
    private String ANDROIDID = null;

    public static String downloadFolder = "/download/pds_a/";

    public CheckDownloadTask(Context ctx, String host, String androidid) {
        super(ctx);
        this.HOST = host;
        this.ANDROIDID = androidid;
    }

    private String getVersionsFromServer() throws Exception {
        WebServiceRequest webServiceRequest = new WebServiceRequest();
        String url = HOST  + "api/VersionCheck?androidId=" + ANDROIDID;
        return webServiceRequest.callWebService(url);

    }

    public static List<AppUpgrade> ParseXML(String xmlIn) throws Exception {
        XmlAppsParser parser = new XmlAppsParser();
        return parser.parse(new ByteArrayInputStream(xmlIn.getBytes("UTF-8")));
    }

    private boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager)
                mCtx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    @Override
    protected String doInBackground(Void... params) {
        try {
            Log.v(TAG, "Starting downloading apks...");
            Logger.RegistrarEvento(mCtx, "i", "Download: Start", "");

            if (!this.isOnline()) {
                return "No hay una conexion disponible";
            }

            String jsonIn = getVersionsFromServer();

            publishProgress("Leyendo respuesta del servidor...");

            CheckDownloadResponse response = new Gson().fromJson(jsonIn, CheckDownloadResponse.class);

//            List<AppUpgrade> lstApps = ParseXML(jsonIn);

            publishProgress("Revisando si es necesario actualizar...");

            //4-foreach
            int count = 0;

            List<AppUpgrade> lstAppsToInstall = new ArrayList<AppUpgrade>();

            for (CheckDownloadItem item:
                 response.getVersions()) {

                String localPath = null;

                if(hasToUpdateApp(item.getPackageName(), item.getLastVersion()) == true)
                {
                    localPath = download(downloadFolder, false, item.getUrl());

                    AppUpgrade appUpg= new AppUpgrade();
                    appUpg.set_context(mCtx);
                    appUpg.set_fileName(getFileName(item.getUrl()));
                    appUpg.set_localPath(localPath);
                    appUpg.set_packageLastVersion(item.getLastVersion());
                    appUpg.set_packageName(item.getPackageName());
                    appUpg.set_url(item.getUrl());


                    lstAppsToInstall.add(appUpg);
                    count++;
                }
                else {
                    deleteFileIfExists(downloadFolder, item.getUrl());
                }
            }

            /*
            List<AppUpgrade> lstAppsToInstall = new ArrayList<AppUpgrade>();

            for (AppUpgrade app : lstApps) {
                app.set_context(mCtx);

                if (app.CorrespondeActualizar()) {
                    //si corresponde actualizar

                    //1-Descargamos (solo si no existe)
                    app.Download(downloadFolder, false);

                    lstAppsToInstall.add(app);

                    count++;
                } else {
                    app.DeleteFileIfExists(downloadFolder);
                }
            }*/

            String xmlAppsToInstall = "";
            if (lstAppsToInstall.size() > 0) {
                try {
                    StringWriter buffer = new StringWriter();
                    writeXML(lstAppsToInstall, buffer);
                    xmlAppsToInstall = buffer.toString();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    xmlAppsToInstall = "";
                }
            }

            SharedPreferences pref = mCtx.getSharedPreferences("com.pds.ficle.ep", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("apps_to_install", xmlAppsToInstall);
            editor.apply();

            return count > 0 ? "Descargas: " + String.valueOf(count) : "No update require";

        } catch (IOException e) {
            //return "No es posible conectarse. Intente mas tarde.";
            return "Error IO: " + e.getMessage();
        } catch (Exception e) {
            return "Error: " + e.getMessage();
        }
    }


    @Override
    protected void onProgressUpdate(String... p) {

    }

    @Override
    protected void onPostExecute(String result) {
        Log.v(TAG, "Finished downloading apks...");
        Logger.RegistrarEvento(mCtx, "i", "Download: End", result);
    }

    @Override
    protected void onCancelled(String s) {

    }

    private void writeXML(List<AppUpgrade> appsList, StringWriter outputStream) throws Exception {
        //we create a XmlSerializer in order to write xml data
        XmlSerializer serializer = Xml.newSerializer();

        //we set the PrintWriter as output for the serializer
        serializer.setOutput(outputStream);
        serializer.startDocument(null, null);

        //tag ROOT
        serializer.startTag(null, "versiones");

        for (AppUpgrade appUpgrade : appsList) {

            serializer.startTag(null, "VERSIONsAll");

            serializer.startTag(null, "versionPackage");
            serializer.text(appUpgrade.get_packageName());
            serializer.endTag(null, "versionPackage");

            serializer.startTag(null, "versionVersion");
            serializer.text(appUpgrade.get_packageLastVersion());
            serializer.endTag(null, "versionVersion");

            serializer.startTag(null, "versionApkUrl");
            serializer.text(appUpgrade.get_localPath());
            serializer.endTag(null, "versionApkUrl");

            serializer.endTag(null, "VERSIONsAll");
        }

        serializer.endTag(null, "versiones");

        serializer.endDocument();

        //escribimos el xml dentro del OutputStream
        serializer.flush();

        //finally we close the file stream
        outputStream.flush();

    }
}
