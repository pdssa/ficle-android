package com.pds.ficle.ep;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.ficle.ep.utils.migration.MigrationTask;

import android_serialport_api.Printer;


public class MigrationActivity extends Activity {

    TextView txtResult;
    TextView txtLastDigitId;
    boolean destino = false;
    Button btnOrigen, btnDestino, btnPrinterTest;
    CheckBox chkStorageAlternativo;
    private String[] printerModelosValues;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_migration);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);//evitamos el sleep durante la ejecucion de esta aplicacion

        txtResult = (TextView) findViewById(R.id.migration_txt_results);
        txtLastDigitId = (TextView) findViewById(R.id.migration_txt_id);

        btnOrigen = (Button) findViewById(R.id.migration_btn_origen);
        btnDestino = (Button) findViewById(R.id.migration_btn_destino);

        chkStorageAlternativo = (CheckBox)findViewById(R.id.migration_chk_storage_alternativo);

        btnOrigen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    if (!destino) {

                        Config cfg = new Config(MigrationActivity.this);

                        String last4DigId = cfg.ANDROID_ID.substring(cfg.ANDROID_ID.length() - 4);

                        new MigrationTask(MigrationActivity.this, MigrationTask.HOST_MODE.SOURCE_HOST, 0, txtResult, "", chkStorageAlternativo.isChecked(), last4DigId).execute();

                    }
                    else {
                        if (getSelectedModel().equals("null"))
                            throw new Exception("Por favor, configure la impresora para continuar");

                        if(TextUtils.isEmpty(txtLastDigitId.getText()) || txtLastDigitId.getText().toString().length() < 4 )
                            throw new Exception("Por favor, ingrese los ultimos 4 digitos del ID del terminal de origen");

                        new MigrationTask(MigrationActivity.this, MigrationTask.HOST_MODE.DESTINATION_HOST, 1, txtResult, getSelectedModel(), chkStorageAlternativo.isChecked(), txtLastDigitId.getText().toString().toLowerCase()).execute();
                    }
                } catch (Exception ex) {
                    Toast.makeText(MigrationActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnDestino.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //new MigrationTask(MigrationActivity.this, MigrationTask.HOST_MODE.DESTINATION_HOST, txtResult).execute();
                btnOrigen.setText("FASE 1");
                btnDestino.setText("FASE 2");

                //habilitamos la seleccion de la impresora actual
                findViewById(R.id.migration_lay_printer).setVisibility(View.VISIBLE);

                destino = true;
                findViewById(R.id.migration_txt_mensaje).setVisibility(View.VISIBLE);

                btnDestino.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {

                            if (getSelectedModel().equals("null"))
                                throw new Exception("Por favor, configure la impresora para continuar");

                            if(TextUtils.isEmpty(txtLastDigitId.getText()) || txtLastDigitId.getText().toString().length() < 4 )
                                throw new Exception("Por favor, ingrese los ultimos 4 digitos del ID del terminal de origen");

                            new MigrationTask(MigrationActivity.this, MigrationTask.HOST_MODE.DESTINATION_HOST, 2, txtResult, getSelectedModel(),chkStorageAlternativo.isChecked(), txtLastDigitId.getText().toString().toLowerCase()).execute();

                        } catch (Exception ex) {
                            Toast.makeText(MigrationActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    }
                });

            }
        });

        //tomamos los modelos de printers
        printerModelosValues = getResources().getStringArray(R.array.printerModelsValues);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.printerModels, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ((Spinner) findViewById(R.id.migration_spn_printer)).setAdapter(adapter);


        btnPrinterTest = (Button) findViewById(R.id.migration_btn_printer_test);
        btnPrinterTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    if (getSelectedModel().equals("null"))
                        throw new Exception("Impresora no configurada");

                    final Printer printer = Printer.getInstance(getSelectedModel(), MigrationActivity.this);

                    printer.printerCallback = new Printer.PrinterCallback() {
                        @Override
                        public void onPrinterReady() {
                            printer.test();
                            //printer.end();
                        }
                    };


                    if (!printer.initialize()) {
                        Toast.makeText(MigrationActivity.this, "Error al inicializar", Toast.LENGTH_SHORT).show();
                        printer.end();
                    }

                } catch (Exception ex) {
                    Toast.makeText(MigrationActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        });

        if (getIntent().hasExtra("OPERACION")) {
            switch (getIntent().getExtras().getInt("OPERACION")) {
                case 0:
                    btnOrigen.performClick();
                    break;
                case 1:
                    btnDestino.performClick();
                    break;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private String getSelectedModel() {
        int selected = (int) ((Spinner) findViewById(R.id.migration_spn_printer)).getSelectedItemId();

        return printerModelosValues[selected];
    }
}
