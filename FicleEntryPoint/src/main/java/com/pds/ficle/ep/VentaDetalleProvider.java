package com.pds.ficle.ep;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import com.pds.common.DbHelper;
import com.pds.common.VentaDetalleHelper;

/**
 * Created by Hernan on 22/11/13.
 */
public class VentaDetalleProvider extends ContentProvider {
    //para el UriMatcher
    private static final int VENTA_DETS = 1;
    private static final int VENTA_DET_ID = 2;

    private static final String AUTHORITY = "com.pds.ficle.ep.venta_detalles.contentprovider";

    private static final String BASE_PATH = "venta_detalles";

    //Uri: content://com.pds.ficle.ep.venta_detalles.contentprovider/venta_detalles
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);

    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/venta_detalles";
    public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/venta_detalle";


    //inicializamos las reglas posibles del UriMatcher:
    //tres opciones: que no coincida, que coincida sin id => valor 1, que coincida con id => valor 2
    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {

        uriMatcher.addURI(AUTHORITY, BASE_PATH, VENTA_DETS);

        uriMatcher.addURI(AUTHORITY, BASE_PATH + "/#", VENTA_DET_ID);
    }

    private DbHelper database;

    @Override
    public boolean onCreate() {
        database = new DbHelper(getContext());
        return false;
    }

    @Override
    public String getType(Uri uri) {
        int uriType = uriMatcher.match(uri);

        switch (uriType) {
            case VENTA_DETS:
                return "vnd.android.cursor.dir/vnd.pds.venta_detalle";
            case VENTA_DET_ID:
                return "vnd.android.cursor.item/vnd.pds.venta_detalle";
            default:
                return null;
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {

        int uriType = uriMatcher.match(uri);

        long new_id = 0;

        switch (uriType) {
            case VENTA_DETS:
                new_id = database.getWritableDatabase().insert(VentaDetalleHelper.TABLE_NAME, null, values);
                break;
            case VENTA_DET_ID:
                break;
            default:
                throw new IllegalArgumentException("URI desconocida: " + uri);
        }

        return ContentUris.withAppendedId(CONTENT_URI, new_id);


    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int cont;

        int uriType = uriMatcher.match(uri);

        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();

        builder.setTables(VentaDetalleHelper.TABLE_NAME);

        //Si es una consulta a un ID concreto construimos el WHERE
        String where = selection;
        switch (uriType) {
            case VENTA_DETS:
                break;
            case VENTA_DET_ID:
                where = VentaDetalleHelper.VENTA_DET_ID + "=" + uri.getLastPathSegment();
                break;
            default:
                throw new IllegalArgumentException("URI desconocida: " + uri);
        }

        cont = database.getWritableDatabase().update(VentaDetalleHelper.TABLE_NAME, values, where, selectionArgs);

        return cont;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        int uriType = uriMatcher.match(uri);

        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();

        builder.setTables(VentaDetalleHelper.TABLE_NAME);

        //Si es una consulta a un ID concreto construimos el WHERE
        switch (uriType) {
            case VENTA_DETS:
                break;
            case VENTA_DET_ID:
                builder.appendWhere(VentaDetalleHelper.VENTA_DET_ID + "=" + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("URI desconocida: " + uri);
        }

        Cursor cursor = builder.query(database.getWritableDatabase(), projection, selection, selectionArgs, null, null, sortOrder);

        return cursor;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int cont;

        int uriType = uriMatcher.match(uri);

        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();

        builder.setTables(VentaDetalleHelper.TABLE_NAME);

        //Si es una consulta a un ID concreto construimos el WHERE
        String where = selection;
        switch (uriType) {
            case VENTA_DETS:
                break;
            case VENTA_DET_ID:
                where = VentaDetalleHelper.VENTA_DET_ID + "=" + uri.getLastPathSegment();
                break;
            default:
                throw new IllegalArgumentException("URI desconocida: " + uri);
        }

        cont = database.getWritableDatabase().delete(VentaDetalleHelper.TABLE_NAME, where, selectionArgs);

        return cont;
    }

}