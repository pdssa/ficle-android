package com.pds.ficle.ep.models.checkDownload;

import com.google.gson.annotations.SerializedName;
import com.pds.ficle.ep.models.ApiResponse;

import java.util.ArrayList;

public class CheckDownloadResponse extends ApiResponse {


    @SerializedName("versions")
    private ArrayList<CheckDownloadItem> mVersions;

    public ArrayList<CheckDownloadItem> getVersions() {
        return mVersions;
    }

}
