package com.pds.ficle.ep;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.os.Build;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Logger;
import com.pds.common.SII.vale.listener.InformeListener;
import com.pds.common.SII.vale.listener.RequiereCierreZListener;
import com.pds.common.SII.vale.model.InformeReq;
import com.pds.common.SII.vale.model.InformeResp;
import com.pds.common.SII.vale.model.InitTerminalReq;
import com.pds.common.SII.vale.model.InitTerminalResp;
import com.pds.common.SII.vale.services.CierreZtask;
import com.pds.common.SII.vale.services.ConsultaRequiereCierreZtask;
import com.pds.common.SII.vale.services.InicializarTerminalTask;
import com.pds.common.SII.vale.listener.InitTerminalListener;
import com.pds.common.SII.vale.util.ValeUtils;
import com.pds.common.Usuario;
import com.pds.common.activity.ActividadProgress;
import com.pds.common.activity.TimerActivity;
import com.pds.common.dialog.Dialog;
import com.pds.common.services.PingServiceIntent;
import com.pds.common.ui.GifView;
import com.pds.common.util.ConnectivityUtils;
import com.pds.common.util.EventsFlow;
import com.pds.ficle.ep.cloud.CheckUpdatesTask;
import com.pds.ficle.ep.cloud.DevicePublicIpTask;
import com.pds.ficle.ep.cloud.GetComercioTask;
import com.pds.ficle.ep.cloud.GetProductsTask;
import com.pds.ficle.ep.cloud.GetRealTimeClock;
import com.pds.ficle.ep.cloud.SyncDataTask;
import com.pds.ficle.ep.cloud.SyncDataTaskEP;
import com.pds.ficle.ep.helpers.SessionHelper;
import com.pds.ficle.ep.receiver.TimeTickReceiver;
import com.pds.ficle.ep.task.ClearOldDataTask;
import com.pds.ficle.ep.task.DownloadFileTask;
import com.pds.ficle.ep.task.SelfTestTask;
import com.pds.ficle.ep.widgets.AppButton;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class MainActivity extends TimerActivity implements ActividadProgress {
    private static final String TAG = MainActivity.class.getSimpleName();
    private int _id_user_login;
    private String _nombre_user_login;
    private Usuario _user;
    TimeTickReceiver _broadcastReceiver;
    private final SimpleDateFormat _dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    private final SimpleDateFormat _timeFormat = new SimpleDateFormat("HH:mm");
    private View btnUsers;
    private View btnConfiguracion;
    private View btnDashboard;
    private View btnHelp;
    private ImageButton btnExit;
    TextView txtWelcome_panel;
    private Config _config;
    private boolean syncPermitido;
    private boolean startFromLogin = false;
    private String lastPackage;

    private GetRealTimeClock getRealTimeClock;
    private SyncDataTaskEP syncDataTaskEP;
    private GetComercioTask getComercioTask;
    private ClearOldDataTask clearOldDataTask;

    private int _windowFlags;
    private Handler mHandler;
    private boolean mStopHandler = false;

    @Override
    public void onStart() {
        super.onStart();

        //NetworkStateReceiver.getInstance(this).checkConnectionState();

        _broadcastReceiver = new TimeTickReceiver((TextView) findViewById(R.id.panel_txt_fecha), (TextView) findViewById(R.id.panel_txt_hora));


        registerReceiver(_broadcastReceiver, _broadcastReceiver.getIntentFilter());

        //_networkStateReceiver = new NetworkStateReceiver();
        //registerReceiver(_networkStateReceiver, )

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(com.pds.ficle.ep.R.layout.ep_activity_main);


            ((TextView) findViewById(R.id.txtVersion)).setText("Version:" + getPackageManager().getPackageInfo(this.getPackageName().toString(), 0).versionName);
            SessionHelper.getInstance().init(getApplicationContext());

            //obtenemos los parametros pasamos a la actividad
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                //tomamos el id de usuario
                //this._id_user_login = extras.getInt("_id_user_login");
                this._user = (Usuario) extras.getSerializable("current_user");

                if (savedInstanceState == null) {
                    if (extras.containsKey("sourceActivity")) {
                        this.startFromLogin = extras.getString("sourceActivity", "").equals("LoginActivity");
                        getIntent().removeExtra("sourceActivity");
                        extras.remove("sourceActivity");
                    } else
                        this.startFromLogin = false;
                } else if (savedInstanceState.containsKey("alreadyStarted")) {
                    this.startFromLogin = false;
                }

            }

            AsignarViews();

            AsignarEventos();

            _config = new Config(this);

            if (startFromLogin)
                Init_Flow(0);


        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(this, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    @Override
    protected void onResume() {

        super.setActive();

        super.onResume();

        Resume();
    }



    private void setSyncTimer() {
        mStopHandler = false;
        if(mHandler == null)
        {
            mHandler = new Handler();
            Runnable runnable = new Runnable() {
                @Override
                public void run() {

                    try {
                        if(SessionHelper.getInstance().isInitialized() == false)
                        {
                            mHandler.postDelayed(this, 5000);
                        }
                        else if(mStopHandler)
                        {
                            Log.w(TAG, "run: Sync timer DONE!");
                            return;
                        }
                        else if(SessionHelper.getInstance().hasToSyncData())
                        {
                            SyncDataTask syncTask = new SyncDataTask(MainActivity.this, null, false, true);
                            syncTask.execute();
                            mHandler.postDelayed(this, 60000);
                        }
                        else {
                            mHandler.postDelayed(this, 5000);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        mHandler.postDelayed(this, 5000);

                    }

                }
            };

// start it with:
            mHandler.postDelayed(runnable, 60000);
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("alreadyStarted", true);

        super.onSaveInstanceState(outState);
    }


    public void Init_Flow(int last_step) {
        Init_Flow(last_step, "");
    }

    public void Init_Flow(int last_step, String value_step) {

        switch (last_step) {
            case 0: {

                _windowFlags = getWindow().getAttributes().flags;//guardamos para restaurar al final

                getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);//evitamos el sleep durante la ejecucion de los pasos

                //STEP FLOW 1 : verificamos la hora actual
                getRealTimeClock = new GetRealTimeClock(this, 1);
                getRealTimeClock.execute();
            }
            break;
            case 1: {
                //STEP FLOW 2 : verificamos la hora actual (de nuevo, por si estaba mal)
                getRealTimeClock = new GetRealTimeClock(this, 2);
                getRealTimeClock.execute();
            }
            break;
            case 2: {
                //STEP FLOW 3: get configuracion
                getComercioTask = new GetComercioTask(this, _config.IMEI(), _config.ANDROID_ID, _config.CLOUD_SERVER_HOST);
            }
            break;
            case 21: {
                //STEP FLOW 2.1: RUTINA DE DESCARGA DE CATALOGO EN PRIMER LOGIN
                showProgress(true);
                showProgressMessage("Importando Catálogo de Productos...");
                GetProductsTask task = new GetProductsTask(MainActivity.this, _config.IMEI(), _config.ANDROID_ID, _config.CLOUD_SERVER_HOST);
                task.execute();
                //new GetProductCatalogTask(MainActivity.this, "", value_step, 23, 3).execute(value_step);
            }
            break;
          /*  case 22: {
                //STEP FLOW 2.2: RUTINA DE DESCARGA DE PROVEEDORES EN PRIMER LOGIN
                if (_config.PAIS.equalsIgnoreCase("ch")) {
                    showProgressMessage("Importando Maestro de Proveedores...");
                    ///FIXME: Regvisar esto HZ
                //    new GetProvidersCatalogTask(MainActivity.this, 23, 3).execute("providers_ch");
                } else {
                    Init_Flow(23);
                }
            }
            break;*/
            case 23: {
                //STEP FLOW 2.3: RUTINA DE INSTALACION DE APPS EN PRIMER LOGIN
                new CheckUpdatesTask(MainActivity.this, 3, 3).execute();

            }
            break;
            case 3: {
                //STEP FLOW 3 : sincronizacion de datos
                //dado que el control de hora no salio OK => forzamos el sync sin chequear la ultima sync
//                boolean sync_sin_check = getRealTimeClock.RESULT != EventsFlow.FECHAHORA_CORREGIDA;
                showProgress(false);
                syncDataTaskEP = new SyncDataTaskEP(this, null, false, false, true);
                syncDataTaskEP.execute();

            }
            break;
            case 4: {
                //DESAFECTAMOS ESTE STEP
                //Init_Flow(last_step + 1);//pasamos al siguiente paso

                //STEP FLOW 4: SELF TEST SII-VALE
                //vemos si la emision de vales está habilitada

                setSyncTimer();
                if (_config.isValeInicializado()) {
                    SelfTestTask selfTestTask = new SelfTestTask(this, last_step + 1);
                    selfTestTask.execute();
                } else
                    Init_Flow(last_step + 1);//pasamos al siguiente paso

            }
            break;
            case 5: {
                //STEP FLOW 5 : borrado de datos históricos
                clearOldDataTask = new ClearOldDataTask(this);
                clearOldDataTask.execute();
            }
            break;
            case 6: {
                //STEP FLOW 6: ACTIVACIÓN DE SERVICIO DE VALE


                final int nextStep = last_step + 1;

                if (_config.isValeHabilitado() && !_config.isValeInicializado()) {
                    InitTerminalReq req = new InitTerminalReq(_config.SII_MERCHANT_ID, _config, ValeUtils.GetUserID(_config.SII_TERMINAL_ID, getContentResolver()));

                    InicializarTerminalTask inicializarTerminalTask = new InicializarTerminalTask(this, _config.isValeModalidadDemo(), new InitTerminalListener() {
                        @Override
                        public void onInitTerminalCompletado(InitTerminalResp resp) {
                            Dialog.Alert(MainActivity.this, getString(_config.isValeModalidadDemo() ? R.string.vale_activac_serv_demo : R.string.vale_activac_serv), getString(R.string.vale_activac_serv_bienvenida), android.R.drawable.ic_dialog_info);
                        }

                        @Override
                        public void onError(boolean reintentarAhora) {

                            if (reintentarAhora) {
                                Init_Flow(6);//repetimos el step
                            } else {
                                //reintentar luego
                                Dialog.Alert(MainActivity.this, getString(_config.isValeModalidadDemo() ? R.string.vale_activac_serv_atencion_demo : R.string.vale_activac_serv_atencion), getString(R.string.vale_activac_serv_recuerde), android.R.drawable.ic_dialog_alert);
                            }

                        }
                    }, _config.SII_MERCHANT_ID);

                    inicializarTerminalTask.execute(req);
                } else if (_config.isValeInicializado()) {

                    boolean cierreIntervalExceeded = ValeUtils.isCierreZIntervalExceeded(MainActivity.this);

                    //dado que estamos excedidos, grabamos que es requerido el cierre
                    ValeUtils.updateIsCierreZRequired(MainActivity.this, cierreIntervalExceeded);

                    if (cierreIntervalExceeded) {
                        //consultamos a plataforma si es necesario realizar cierre z
                        ConsultaRequiereCierreZtask requiereCierreZtask = new ConsultaRequiereCierreZtask(MainActivity.this, new RequiereCierreZListener() {
                            @Override
                            public void onConsultaCompletada(boolean requiereCierreZ) {

                                if (requiereCierreZ) {

                                    //corresponde hacer cierre cierre z
                                    CierreZtask inf = new CierreZtask(MainActivity.this, _config, true, new InformeListener() {
                                        @Override
                                        public void onConsultaCompletada(String response) {

                                        }

                                        @Override
                                        public void onConsultaCompletada(InformeResp response) {
                                            Init_Flow(nextStep);//pasamos al siguiente paso
                                        }

                                        @Override
                                        public void onError() {
                                            Init_Flow(nextStep);//pasamos al siguiente paso
                                        }
                                    });
                                    inf.startDialog();

                                } else {
                                    //dado que se verificó que no es requerido, entonces grabamos que NO es requerido el cierre
                                    ValeUtils.updateIsCierreZRequired(MainActivity.this, false);

                                    Init_Flow(nextStep);//pasamos al siguiente paso
                                }
                            }

                            @Override
                            public void onError() {
                                Init_Flow(nextStep);
                            }
                        });

                        InformeReq request = new InformeReq(_config, ValeUtils.GetUserID(_config.SII_TERMINAL_ID, getContentResolver()), _config.SII_MERCHANT_ID);
                        requiereCierreZtask.execute(request);
                    } else
                        Init_Flow(nextStep);//pasamos al siguiente paso
                } else
                    Init_Flow(nextStep);//pasamos al siguiente paso
            }
            break;
            case 7: {
                //STEP FLOW 7: show messages

                String messageUrl = getSharedPreferences("com.pds.ficle.ep", Context.MODE_PRIVATE).getString("msg_url", "");

                ProcesarMensajes(messageUrl, last_step + 1);
            }
            break;
            /*case 8: {
                //Toast.makeText(MainActivity.this, "step 8 !", Toast.LENGTH_SHORT).show();

                //STEP FLOW 8: respuesta de encuesta
                boolean tieneEncPend = getSharedPreferences("com.pds.ficle.ep", Context.MODE_PRIVATE).getBoolean("enc_tiene_q_resp", false);
                String ultEncUrl = getSharedPreferences("com.pds.ficle.ep", Context.MODE_PRIVATE).getString("enc_url", "");

                //luego borrar:
                //tieneEncPend = true;
                //ultEncUrl = "https://es.surveymonkey.com/r/F9HKF5R";
                //-----

                if (tieneEncPend && !TextUtils.isEmpty(ultEncUrl)) {
                    CargarEncuesta(ultEncUrl);
                }

                //vamos a mostrar los productos creados por una actualizacion de catalogo
                //MostrarProductosCreados();

                getWindow().addFlags(_windowFlags);//restauramos los flags iniciales
            }*/
        }
    }

    private void ProcesarMensajes(String messageUrl, final int nextStep) {

        String messagesPath = Environment.getExternalStorageDirectory() + "/msgs";
        String messageName = Uri.parse(messageUrl).getLastPathSegment();

        boolean hasToClear = false;
        boolean hasToDownload = false;
        boolean hasToShow = false;

        if (TextUtils.isEmpty(messageUrl)) {
            hasToClear = true;
        } else {
            //tenemos el mensaje ya descargado?

            boolean exists = false;

            File messagesFolder = new File(messagesPath); // PATH = /mnt/sdcard/download/pds
            if (messagesFolder.exists()) {//entramos si existe
                File messageFile = new File(messagesFolder, messageName);
                exists = messageFile.exists();//vemos si existe el file
            }

            if (!exists) {
                //NO => limpiamos y descargamos...
                hasToClear = true;
                hasToDownload = true;
            } else {
                //SI => lo mostramos ...
                hasToShow = true;
            }
        }


        if (hasToClear) {
            //limpiamos los messages que tengamos en la carpeta
            File messagesFolder = new File(messagesPath); // PATH = /mnt/sdcard/download/pds
            if (messagesFolder.exists() && messagesFolder.listFiles().length > 0) {//entramos si existe y hay files
                for (File file : messagesFolder.listFiles()) {
                    file.delete();
                }
            }
        }

        if (hasToShow) {
            //mostramos el mensaje
            Dialog.ImageDialog(this, messagesPath + "/" + messageName, new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    //seguimos
                    Init_Flow(nextStep);
                }
            });

            return;
        }

        if (hasToDownload) {
            //ponemos a descargar el file
            DownloadFileTask downloadFileTask = new DownloadFileTask(this, messagesPath);
            downloadFileTask.execute(messageUrl);
        }

        //seguimos
        Init_Flow(nextStep);
    }

    /*private void MostrarProductosCreados() {
        try {
            SharedPreferences pref = getSharedPreferences("com.pds.ficle.ep", Context.MODE_PRIVATE);

            Set<String> prods_creados = pref.getStringSet("prods_creados", null);
            int versActual = pref.getInt("version_catalog", 1);

            if (prods_creados != null) {

                String mensaje = String.format("Se han creado/actualizado %s productos.<br/>Version actual del catalogo: v%s", prods_creados.size(), versActual);

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("ACTUALIZACION DE CATALOGO");
                builder.setMessage(Html.fromHtml(mensaje));
                builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface _dialog, int which) {
                        _dialog.dismiss();
                    }
                });
                builder.setCancelable(false);
                builder.show();

            }
        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, "MostrarProdsCreados: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
            Logger.RegistrarEvento(MainActivity.this, "e", "MostrarProdsCreados", "Error:" + ex.getMessage());
        }
    }*/

    public void Resume() {

        lastPackage = "";

        ((TextView) findViewById(R.id.panel_txt_fecha)).setText(_dateFormat.format(new Date()));
        ((TextView) findViewById(R.id.panel_txt_hora)).setText(_timeFormat.format(new Date()));

        _config = new Config(this);

        ValidaSeguridadPerfil();

        AjustaPerfilComercio(false);


        this._nombre_user_login = _user.getNombre();
        String pie = "Comercio " + _config.COMERCIO + " / Usuario: " + (this._nombre_user_login == null ? "" : this._nombre_user_login);
        ((TextView) findViewById(R.id.panel_txt_user)).setText(pie);

    }


    @Override
    public void onBackPressed() {
        try {

            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Salir")
                    .setMessage("Desea salir?")
                    .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mStopHandler = true;
                            btnExit.performClick();
                        }

                    })
                    .setNegativeButton("NO", null)
                    .show();

        } catch (Exception ex) {
            Toast.makeText(this, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) { //solo consideramos la tecla back, el resto de las teclas son omitidas
        return keyCode == KeyEvent.KEYCODE_BACK ? super.onKeyDown(keyCode, event) : true;
    }


    public void AjustaPerfilComercio(boolean renewConfig) {

        if (renewConfig)
            _config = new Config(MainActivity.this);

        setupDefaultProfile();//dejamos el perfil por default, y luego aplicamos cambios especificos por perfil

        List<AppButton> listApps = getProfileListApps(_config.PERFIL);


        //armamos los botones de aplicaciones

        //ajustamos el alto de los botones en forma proporcional
        int q = listApps.size();
        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1f / Math.max(q, 4));

        LinearLayout layout = (LinearLayout) findViewById(R.id.ep_activity_main_apps);
        layout.removeAllViews();

        for (AppButton appButton : listApps) {

            View btnApp = null;

            if (appButton.Package.equals("com.pds.promos")) {
                btnApp = new GifView(this, appButton.ImageId);

                LinearLayout wrapper = new LinearLayout(MainActivity.this);
                wrapper.setLayoutParams(p);
                wrapper.setGravity(Gravity.CENTER);

                wrapper.setTag(appButton.Package);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    // If we're running on Honeycomb or newer, then we can use the Theme's
                    // selectableItemBackground to ensure that the View has a pressed state
                    TypedValue outValue = new TypedValue();
                    getTheme().resolveAttribute(android.R.attr.selectableItemBackground, outValue, true);
                    wrapper.setBackgroundResource(outValue.resourceId);
                }

                wrapper.setOnClickListener(onBtnAppClick);

                wrapper.addView(btnApp);

                layout.addView(wrapper);

                continue;

            } else {
                btnApp = new ImageButton(this);

                ((ImageButton) btnApp).setImageResource(appButton.ImageId);

                btnApp.setLayoutParams(p);

            }

            btnApp.setTag(appButton.Package);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                // If we're running on Honeycomb or newer, then we can use the Theme's
                // selectableItemBackground to ensure that the View has a pressed state
                TypedValue outValue = new TypedValue();
                getTheme().resolveAttribute(android.R.attr.selectableItemBackground, outValue, true);
                btnApp.setBackgroundResource(outValue.resourceId);
            }

            btnApp.setOnClickListener(onBtnAppClick);

            layout.addView(btnApp);
        }

    }

    private OnClickListener onBtnAppClick = new OnClickListener() {
        @Override
        public void onClick(View view) {
            String _package = view.getTag().toString();

            CallPackage(_package);
        }
    };

    private void setupDefaultProfile() {
        findViewById(R.id.logo_bpos).setVisibility(View.VISIBLE);

        findViewById(R.id.ep_activity_main_container).setBackgroundResource(R.drawable.fondo_nuevo);

        findViewById(R.id.ep_activity_main_p_apps).setVisibility(View.VISIBLE);

        ((ImageButton) findViewById(R.id.ep_activity_main_btn_pdv)).setImageResource(R.drawable.pdv);
        ((ImageButton) findViewById(R.id.ep_activity_main_btn_cuenta_cliente)).setImageResource(R.drawable.cuenta_cliente);
        ((ImageButton) findViewById(R.id.ep_activity_main_btn_cashdrawer)).setImageResource(R.drawable.cash);
        ((ImageButton) findViewById(R.id.ep_activity_main_btn_horario)).setImageResource(R.drawable.control_horario);


    }

    public void SyncDataToServer(boolean onExit) {
        //if (syncPermitido)
        new SyncDataTaskEP(MainActivity.this, onExit ? "Salir" : null, onExit, false, false).execute();
        //else if (onExit)
        //    Salir();

    }

    public void SalirAccion(String accion) {
        //btnExit.performClick();

        try {
            Logger.RegistrarEvento(getApplicationContext(), "i", "Logout", "User: " + _nombre_user_login);

            //detenemos el time receiver
            if (_broadcastReceiver != null) {
                unregisterReceiver(_broadcastReceiver);
                _broadcastReceiver = null;
            }

            //detenemos el timer => estamos por salir
            MainActivity.super.stopInactivityTimer();

            new PingServiceIntent(false).send(getApplicationContext());

            Intent i = new Intent(MainActivity.this, LoginActivity.class);

            if (!TextUtils.isEmpty(accion))
                i.putExtra("ACTION_EXEC", accion);

            startActivity(i);


        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void Salir() {
        //Toast.makeText(MainActivity.this, "Gracias por utilizar Punto Total!", Toast.LENGTH_SHORT).show();
        if (_broadcastReceiver != null) {
            unregisterReceiver(_broadcastReceiver);
            _broadcastReceiver = null;
        }

        new PingServiceIntent(false).send(getApplicationContext());

        startActivity(new Intent(MainActivity.this, LoginActivity.class));

        finish();
    }

    @Override
    public void onFinish() {
        btnExit.performClick();
    }

    public void AsignarViews() {
        this.txtWelcome_panel = (TextView) findViewById(R.id.panel_txt_welcome);
    }

    public void AsignarEventos() {

        //ESTABLECEMOS APERTURA USERS
        btnUsers = findViewById(R.id.ep_activity_main_btn_users);
        btnUsers.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_user.getId_perfil() == 1) {//si es user ADMIN => dejamos que ingrese al listado de usuarios
                    Intent i = new Intent(view.getContext(), user_list_activity.class);
                    startActivity(i);
                } else {//si no es ADMIN => ingresa solo a edit sus datos
                    Intent i = new Intent(view.getContext(), user_abm_activity.class);
                    i.putExtra("modo_edit", true);
                    i.putExtra("_id", _user.getId());
                    startActivity(i);
                }
            }
        });

        //ESTABLECERMOS CONFIGURACION
        btnConfiguracion = findViewById(R.id.ep_activity_main_btn_config);
        btnConfiguracion.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(), ConfigUserActivity.class);
                startActivity(i);
            }
        });

        //activity_help
        //ACCESO A LA AYUDA
        btnHelp = findViewById(R.id.ep_activity_main_btn_help);
        btnHelp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(), help.class);
                startActivity(i);
            }
        });

        //ESTABLECEMOS EL ACCESO A PRODUCTOS
        findViewById(R.id.ep_activity_main_btn_productos).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                CallPackage("com.pds.ficle.gestprod");
            }
        });


        //DASHBOARD
        btnDashboard = findViewById(R.id.ep_activity_main_btn_dashboard);
        btnDashboard.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                CallPackage("com.pds.ficle.dashboard");
            }
        });

        //ESTABLECEMOS EL CIERRE DE SESION
        btnExit = (ImageButton) findViewById(R.id.ep_activity_main_btn_exit);
        btnExit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Logger.RegistrarEvento(getApplicationContext(), "i", "Logout", "User: " + _nombre_user_login);

                    //detenemos el time receiver
                    if (_broadcastReceiver != null) {
                        unregisterReceiver(_broadcastReceiver);
                        _broadcastReceiver = null;
                    }

                    //detenemos el timer => estamos por salir
                    MainActivity.super.stopInactivityTimer();

                    //SyncDataToServer(true);
                    ///FIXME: Solo hacer el sync de datos
                    SalirAccion("");

                } catch (Exception ex) {
                    Toast.makeText(view.getContext(), "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });

        //ESTABLECEMOS EL ACCESO A LA APP PDV ( PUNTO DE VENTA)
        ImageButton btnPDV = (ImageButton) findViewById(R.id.ep_activity_main_btn_pdv);
        btnPDV.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                CallPackage("com.pds.ficle.pdv");
            }
        });


        //ESTABLECEMOS EL ACCESO A LA APP CONTROL HORARIO
        ImageButton btnControlHorario = (ImageButton) findViewById(R.id.ep_activity_main_btn_horario);
        btnControlHorario.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                CallPackage("com.pds.ficle.ctrlhora");
            }
        });

        //ESTABLECEMOS EL ACCESO A LA APP CASHDRAWER
        ImageButton btnCashDrawer = (ImageButton) findViewById(R.id.ep_activity_main_btn_cashdrawer);
        btnCashDrawer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                CallPackage("com.pds.ficle.cashdrawer");
            }
        });

        //ESTABLECEMOS EL ACCESO A LA APP CUENTA CLIENTE
        ImageButton btnCC = (ImageButton) findViewById(R.id.ep_activity_main_btn_cuenta_cliente);
        btnCC.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                CallPackage("com.pds.ficle.cuentacliente");
            }
        });

    }

    private void CallPackage(String _package) {
        try {

            if (_package.equalsIgnoreCase("com.alexvas.dvr")) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_MAIN);
                i.setComponent(new ComponentName("com.alexvas.dvr", "com.alexvas.dvr.activity.LiveViewActivity"));
                startActivity(i);
            } else {
                PackageManager manager = getPackageManager();
                Intent i = manager.getLaunchIntentForPackage(_package);
                if (i == null)
                    throw new NameNotFoundException();

                i.addCategory(Intent.CATEGORY_LAUNCHER);
                i.putExtra("current_user", _user);

                startActivity(i);
            }

            //detenemos el timer
            super.stopInactivityTimer();

            lastPackage = _package;
        } catch (NameNotFoundException nex) {
            Toast.makeText(MainActivity.this, "Lo sentimos, la aplicación no se encuentra disponible", Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void ValidaSeguridadPerfil() {
        if (_user.getId_perfil() == 1) {
            btnUsers.setEnabled(true);
            btnConfiguracion.setEnabled(true);
        } else {
            //btnUsers.setEnabled(false);
            btnConfiguracion.setEnabled(false);
        }
    }


    /**
     * Shows the progress UI and hides the main layout.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        final View _progressView = findViewById(R.id.ep_progress);
        final View _epMainView = findViewById(R.id.ep_activity_main_container);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            _progressView.setVisibility(View.VISIBLE);
            _progressView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(show ? 1 : 0)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            _progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                        }
                    });


            _epMainView.setVisibility(View.VISIBLE);
            _epMainView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(show ? 0 : 1)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            _epMainView.setVisibility(show ? View.GONE : View.VISIBLE);
                        }
                    });

        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            _progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            _epMainView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    public void showProgressMessage(CharSequence message) {
        TextView _progressMessageView = (TextView) findViewById(R.id.progress_status_message);

        _progressMessageView.setText(message);
    }

    private List<AppButton> getProfileListApps(String PERFIL) {
        List<AppButton> list = new ArrayList<AppButton>();


        list.add(new AppButton("com.pds.ficle.vale", R.drawable.vale));


        return list;
    }


}
