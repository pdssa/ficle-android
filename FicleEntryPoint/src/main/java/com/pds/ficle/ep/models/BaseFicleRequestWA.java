package com.pds.ficle.ep.models;

import com.google.gson.annotations.SerializedName;

public abstract class BaseFicleRequestWA {
    @SerializedName("AndroidId")
    public String mAndroidId;
    @SerializedName("IMEI")
    public String mImei;
}
