package com.pds.ficle.ep.models.getProducts;

import com.google.gson.annotations.SerializedName;
import com.pds.ficle.ep.models.ApiResponse;

import java.util.ArrayList;

public class GetProductsResponse extends ApiResponse {
    @SerializedName("products")
    private ArrayList<GetProductsItem> mProducts;

    public ArrayList<GetProductsItem> getProducts() {
        return mProducts;
    }
}
