package com.pds.ficle.ep.cloud;

/**
 * Created by Hernan on 10/06/2014.
 */
public class Version {
    private String _packageName;
    private String _lastVersion;
    private String _apkUrl;

    public String get_packageName() {
        return _packageName;
    }

    public void set_packageName(String _packageName) {
        this._packageName = _packageName;
    }

    public String get_lastVersion() {
        return _lastVersion;
    }

    public void set_lastVersion(String _lastVersion) {
        this._lastVersion = _lastVersion;
    }

    public String get_apkUrl() {
        return _apkUrl;
    }

    public void set_apkUrl(String _apkUrl) {
        this._apkUrl = _apkUrl;
    }
}
