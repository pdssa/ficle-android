package com.pds.ficle.ep;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import com.pds.common.DbHelper;
import com.pds.common.VentasHelper;

/**
 * Created by Hernan on 22/11/13.
 */
public class VentaProvider extends ContentProvider {
    //para el UriMatcher
    private static final int VENTAS = 1;
    private static final int VENTA_ID = 2;
    private static final int VENTAS_APP = 3;
    private static final int VENTAS_VDR = 4;
    private static final int VENTAS_APP_MES = 5;
    private static final int VENTAS_VDR_MES = 6;

    private static final String AUTHORITY = "com.pds.ficle.ep.ventas.contentprovider";

    private static final String BASE_PATH = "ventas";
    private static final String BASE_PATH_APP = "ventas_app";
    private static final String BASE_PATH_VDR = "ventas_vddor";
    private static final String BASE_PATH_APP_MES = "ventas_app_mes";
    private static final String BASE_PATH_VDR_MES = "ventas_vddor_mes";

    //Uri: content://com.pds.ficle.ep.ventas.contentprovider/ventas
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);

    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/ventas";
    public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/venta";
    public static final String CONTENT_TYPE_APP = ContentResolver.CURSOR_DIR_BASE_TYPE + "/ventas_app";
    public static final String CONTENT_TYPE_VDR = ContentResolver.CURSOR_DIR_BASE_TYPE + "/ventas_vddor";


    //inicializamos las reglas posibles del UriMatcher:
    //tres opciones: que no coincida, que coincida sin id => valor 1, que coincida con id => valor 2
    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {

        uriMatcher.addURI(AUTHORITY, BASE_PATH, VENTAS);

        uriMatcher.addURI(AUTHORITY, BASE_PATH + "/#", VENTA_ID);

        uriMatcher.addURI(AUTHORITY, BASE_PATH_APP, VENTAS_APP);

        uriMatcher.addURI(AUTHORITY, BASE_PATH_VDR, VENTAS_VDR);

        uriMatcher.addURI(AUTHORITY, BASE_PATH_APP_MES, VENTAS_APP_MES);

        uriMatcher.addURI(AUTHORITY, BASE_PATH_VDR_MES, VENTAS_VDR_MES);
    }

    private DbHelper database;

    @Override
    public boolean onCreate() {
        database = new DbHelper(getContext());
        return false;
    }

    @Override
    public String getType(Uri uri) {
        int uriType = uriMatcher.match(uri);

        switch (uriType) {
            case VENTAS:
                return "vnd.android.cursor.dir/vnd.pds.venta";
            case VENTA_ID:
                return "vnd.android.cursor.item/vnd.pds.venta";
            case VENTAS_APP:
                return "vnd.android.cursor.dir/vnd.pds.venta";
            case VENTAS_VDR:
                return "vnd.android.cursor.dir/vnd.pds.venta";
            case VENTAS_APP_MES:
                return "vnd.android.cursor.dir/vnd.pds.venta";
            case VENTAS_VDR_MES:
                return "vnd.android.cursor.dir/vnd.pds.venta";
            default:
                return null;
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {

        int uriType = uriMatcher.match(uri);

        long new_id = 0;

        switch (uriType) {
            case VENTAS:
                new_id = database.getWritableDatabase().insert(VentasHelper.TABLE_NAME, null, values);
                break;
            case VENTA_ID:
                break;
            default:
                throw new IllegalArgumentException("URI desconocida: " + uri);
        }

        return ContentUris.withAppendedId(CONTENT_URI, new_id);


    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int cont;

        int uriType = uriMatcher.match(uri);

        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();

        builder.setTables(VentasHelper.TABLE_NAME);

        //Si es una consulta a un ID concreto construimos el WHERE
        String where = selection;
        switch (uriType) {
            case VENTAS:
                break;
            case VENTA_ID:
                where = VentasHelper.VENTA_ID + "=" + uri.getLastPathSegment();
                break;
            default:
                throw new IllegalArgumentException("URI desconocida: " + uri);
        }

        cont = database.getWritableDatabase().update(VentasHelper.TABLE_NAME, values, where, selectionArgs);

        return cont;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        int uriType = uriMatcher.match(uri);

        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();

        if (uriType == VENTAS_APP)
            builder.setTables(VentasHelper.VW_VENTAS_DIARIAS_APP);
        else if (uriType == VENTAS_VDR)
            builder.setTables(VentasHelper.VW_VENTAS_DIARIAS_VDDOR);
        else if (uriType == VENTAS_APP_MES)
            builder.setTables(VentasHelper.VW_VENTAS_MES_APP);
        else if (uriType == VENTAS_VDR_MES)
            builder.setTables(VentasHelper.VW_VENTAS_MES_VDDOR);
        else
            builder.setTables(VentasHelper.TABLE_NAME);

        //Si es una consulta a un ID concreto construimos el WHERE
        switch (uriType) {
            case VENTAS:
                break;
            case VENTAS_APP:
                break;
            case VENTAS_VDR:
                break;
            case VENTAS_APP_MES:
                break;
            case VENTAS_VDR_MES:
                break;
            case VENTA_ID:
                builder.appendWhere(VentasHelper.VENTA_ID + "=" + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("URI desconocida: " + uri);
        }

        Cursor cursor = builder.query(database.getWritableDatabase(), projection, selection, selectionArgs, null, null, sortOrder);

        return cursor;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int cont;

        int uriType = uriMatcher.match(uri);

        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();

        builder.setTables(VentasHelper.TABLE_NAME);

        //Si es una consulta a un ID concreto construimos el WHERE
        String where = selection;
        switch (uriType) {
            case VENTAS:
                break;
            case VENTA_ID:
                where = VentasHelper.VENTA_ID + "=" + uri.getLastPathSegment();
                break;
            default:
                throw new IllegalArgumentException("URI desconocida: " + uri);
        }

        cont = database.getWritableDatabase().delete(VentasHelper.TABLE_NAME, where, selectionArgs);

        return cont;
    }

}
