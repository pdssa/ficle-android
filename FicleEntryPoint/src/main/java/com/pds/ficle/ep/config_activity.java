package com.pds.ficle.ep;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.ConfigHelper;
import com.pds.common.Logger;
import com.pds.common.activity.ActividadProgress;
import com.pds.common.activity.TimerActivity;
import com.pds.common.dialog.ConfirmDialog;
import com.pds.ficle.ep.cloud.CheckUpdatesTask;
import com.pds.ficle.ep.task.ClearOldDataTask;

import java.util.ArrayList;
import java.util.List;

import android_serialport_api.Printer;

/**
 * Created by Hernan on 06/12/13.
 */
public class config_activity extends TimerActivity implements ActividadProgress {

    private EditText txtServerHost;

    private EditText txtSyncTime;
    private EditText txtHistDays;
    private EditText txtInactivityTime;

    private Button btnGrabar;
    private Button btnCancelar;
    private Spinner spnPrinterModel;
    private Button btnPrinterTest;
    private Spinner spnPais;

    private String[] printerModelosValues;
    private String[] paisesValues;

    private CheckBox chkSyncLog;
    private CheckBox chkSyncCaja;
    private CheckBox chkSyncStock;

    private Config c;

    private boolean mostrarAdvertenciaReinicio;



    //variables para comparar
    private String old_inact_time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.config_layout);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);//evitamos el sleep durante la ejecucion de esta aplicacion


        this.spnPrinterModel = (Spinner) findViewById(R.id.config_spn_printer);
        this.spnPais = (Spinner) findViewById(R.id.config_spn_pais);
        this.txtServerHost = (EditText) findViewById(R.id.config_txt_ip);
        this.btnGrabar = (Button) findViewById(R.id.config_btn_grabar);
        this.btnCancelar = (Button) findViewById(R.id.config_btn_cancelar);
        this.btnPrinterTest = (Button) findViewById(R.id.config_btn_printer_test);
        this.chkSyncLog = (CheckBox) findViewById(R.id.config_chk_sync_log);
        this.chkSyncCaja = (CheckBox) findViewById(R.id.config_chk_sync_caja);
        this.chkSyncStock = (CheckBox) findViewById(R.id.config_chk_sync_stock);
        this.txtSyncTime = (EditText) findViewById(R.id.config_txt_sync_time);
        this.txtHistDays = (EditText) findViewById(R.id.config_txt_hist_days);
        this.txtInactivityTime = (EditText) findViewById(R.id.config_txt_inact_time);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.printerModels, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.spnPrinterModel.setAdapter(adapter);

        this.printerModelosValues = getResources().getStringArray(R.array.printerModelsValues);

        ArrayAdapter<CharSequence> pais_adapter = ArrayAdapter.createFromResource(this, R.array.paises, android.R.layout.simple_spinner_item);
        pais_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.spnPais.setAdapter(pais_adapter);

        this.paisesValues = getResources().getStringArray(R.array.paisesValues);


        //****************CONFIG DE BOLETA*********************
        /*findViewById(R.id.config_btn_boleta).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                CambiarEstadoPanel(findViewById(R.id.config_boleta_layout));
            }
        });
        boleta_txtFMServerHost = (EditText) findViewById(R.id.config_boleta_txt_fm_ip);
        boleta_txtFMServerPort = (EditText) findViewById(R.id.config_boleta_txt_fm_port);
        boleta_txtPDSServerHost = (EditText) findViewById(R.id.config_boleta_txt_pds_ip);
        boleta_txtPDSServerPort = (EditText) findViewById(R.id.config_boleta_txt_pds_port);*/
        //*****************************************************

        //**************CONFIG DE PDV**************************
        //pdv_config_chk_vale = (CheckBox) findViewById(R.id.config_pdv_chk_vale);
        //*****************************************************

        this.btnGrabar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                try {

                    boolean huboError = false;
                    String mensajeError = "";
                    mostrarAdvertenciaReinicio = false;


                    //grabamos la configuracion general
                    if (!SaveConfig()) {
                        huboError = true;
                        mensajeError = "Error al grabar configuracion";
                    }

                    //grabamos configuracion de cada modulo
                    /*if (!SaveConfigBoleta()) {
                        huboError = true;
                        mensajeError = "Error al grabar configuracion de Boleta Electrónica";
                    }*/

                    //grabamos configuracion de cada modulo
                    /*if (!SaveConfigPDV()) {
                        huboError = true;
                        mensajeError = "Error al grabar configuracion de PDV";
                    }*/

                    //ok
                    //CerrarPaneles();

                    Logger.RegistrarEvento(config_activity.this, "i", "Configuracion", "Se ha actualizado la configuracion");

                    if (!huboError) {
                        Toast.makeText(config_activity.this, "Configuracion actualizada con exito", Toast.LENGTH_SHORT).show();
                        if (mostrarAdvertenciaReinicio)
                            AlertMessage("Atención: Debe reiniciar el equipo para que apliquen los cambios actualizados");
                    } else
                        Toast.makeText(config_activity.this, mensajeError, Toast.LENGTH_SHORT).show();

                } catch (Exception ex) {
                    Toast.makeText(config_activity.this, "No se pudo actualizar la configuracion: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        });

        this.btnCancelar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                finish(); //cerramos la actividad
            }
        });


        //APERTURA DE LOGS
        View btnLogs = findViewById(R.id.config_btn_logs);
        btnLogs.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentLogs();

            }
        });

        Button btnCheck = (Button) findViewById(R.id.config_btn_check);
        btnCheck.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                new CheckUpdatesTask(config_activity.this).execute();
            }
        });

        findViewById(R.id.config_btn_acciones).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowPopupMenuOptions(view);
            }
        });

        findViewById(R.id.config_btn_menu).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowPopupMenu(view);
            }
        });

        this.btnPrinterTest.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                int selected = (int) spnPrinterModel.getSelectedItemId();

                String modelo = printerModelosValues[selected];

                try {

                    if (modelo.equals("null"))
                        throw new Exception("Impresora no configurada");

                    final Printer printer = Printer.getInstance(modelo, config_activity.this);

                    printer.printerCallback = new Printer.PrinterCallback() {
                        @Override
                        public void onPrinterReady() {
                            printer.test();
                            //printer.end();
                        }
                    };


                    if (!printer.initialize()) {
                        Toast.makeText(config_activity.this, "Error al inicializar", Toast.LENGTH_SHORT).show();
                        printer.end();
                    } else if (printer.isCashDrawerActivated()) {
                        new ConfirmDialog(config_activity.this, "APERTURA CAJON", "¿Desea probar la apertura del cajón?")
                                .set_positiveButtonText("ACEPTAR")
                                .set_positiveButtonAction(new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        printer.openCashDrawer();
                                    }
                                })
                                .set_negativeButtonText("CANCELAR")
                                .set_negativeButtonAction(new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .set_icon(R.drawable.cashdrawer_icon)
                                .show();
                    }


                    /*
                    if(printer instanceof PrinterPOWA){

                        Toast.makeText(config_activity.this, "initializing POWAPrinter", Toast.LENGTH_SHORT).show();

                        printer.printerCallback = new Printer.PrinterCallback() {
                            @Override
                            public void onPrinterReady() {
                                printer.test();
                                //printer.end();
                            }
                        };


                        if (!printer.initialize()) {
                            Toast.makeText(config_activity.this, "Error al inicializar", Toast.LENGTH_SHORT).show();
                            printer.end();
                        }
                    }
                    else{

                        if (!printer.initialize()) {
                            Toast.makeText(config_activity.this, "Error al inicializar", Toast.LENGTH_SHORT).show();
                            printer.end();
                        } else {
                            printer.test();
                        }
                    }
                    */


                } catch (Exception ex) {
                    Toast.makeText(config_activity.this, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        LeerConfig();
    }

    public void ShowPopupMenu(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return onOptionsItemSelected(item);
            }
        });
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.config, popup.getMenu());

        popup.show();
    }

    public void ShowPopupMenuOptions(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return onOptionsItemSelected(item);
            }
        });
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.config_options, popup.getMenu());

        popup.show();
    }

    private boolean SaveConfig() {

        //vamos a revisar los obligatorios
        boolean error = false;

        //String comercio = txtComercio.getText().toString().trim();
        //String domicilio = txtDireccion.getText().toString().trim();
        //String claveFiscal = txtClaveFiscal.getText().toString().trim();
        String server_host = txtServerHost.getText().toString().trim();
        //String idComercio = txtIdComercio.getText().toString().trim();
        String syncTime = txtSyncTime.getText().toString().trim();
        String histDays = txtHistDays.getText().toString().trim();
        String inactTime = txtInactivityTime.getText().toString().trim();





        if (TextUtils.isEmpty(server_host)) {
            error = true;
            txtServerHost.setError("Servidor obligatorio");
        }



        if (TextUtils.isEmpty(histDays)) {
            error = true;
            txtHistDays.setError("Dias resguardo histórico obligatorio");
        } else if (!TextUtils.isDigitsOnly(histDays)) {
            error = true;
            txtHistDays.setError("Dias resguardo histórico debe ser numerico");
        }

        if (TextUtils.isEmpty(syncTime)) {
            error = true;
            txtSyncTime.setError("Tiempo obligatorio");
        } else if (!TextUtils.isDigitsOnly(syncTime)) {
            error = true;
            txtSyncTime.setError("Tiempo debe ser numerico");
        }

        if (TextUtils.isEmpty(inactTime)) {
            error = true;
            txtInactivityTime.setError("Minutos de inactividad obligatorios");
        } else if (!TextUtils.isDigitsOnly(inactTime)) {
            error = true;
            txtInactivityTime.setError("Minutos de inactividad debe ser numerico");
        }

        if (!error) {
            ContentValues cv = new ContentValues();

            //cv.put(ConfigHelper.CONFIG_COMERCIO, comercio);
            //cv.put(ConfigHelper.CONFIG_DIRECCION, domicilio);
            //cv.put(ConfigHelper.CONFIG_CLAVEFISCAL, claveFiscal);
            //cv.put(ConfigHelper.CONFIG_REC_NRO_COMERCIO, txtRecargaNroComercio.getText().toString());
            //cv.put(ConfigHelper.CONFIG_REC_ID_TERMINAL, txtRecargaIdTerminal.getText().toString());

            cv.put(ConfigHelper.CONFIG_CLOUD_HOST, server_host);
            //cv.put(ConfigHelper.CONFIG_ID_COMERCIO, idComercio);

            int selected = (int) spnPrinterModel.getSelectedItemId();
            String modelo = printerModelosValues[selected];

            cv.put(ConfigHelper.CONFIG_PRINTER_MODEL, modelo);

            cv.put(ConfigHelper.CONFIG_SYNC_CAJA, chkSyncCaja.isChecked());
            cv.put(ConfigHelper.CONFIG_SYNC_LOGS, chkSyncLog.isChecked());
            cv.put(ConfigHelper.CONFIG_SYNC_STOCK, chkSyncStock.isChecked());

            String pais = paisesValues[(int) spnPais.getSelectedItemId()];

            cv.put(ConfigHelper.CONFIG_PAIS, pais);

            cv.put(ConfigHelper.CONFIG_SYNC_TIME, syncTime);
            cv.put(ConfigHelper.CONFIG_HIST_DIAS, histDays);
            cv.put(ConfigHelper.CONFIG_INACTIVITY_TIME, Integer.parseInt(inactTime));

            int result = config_activity.this.getContentResolver().update(
                    Uri.parse("content://com.pds.ficle.ep.config.contentprovider/configs/1"),
                    cv,
                    "",
                    new String[]{});

            Logger.RegistrarEvento(config_activity.this, "i", "Configuracion", "Actualizacion de configuracion");

            if (!inactTime.equals(old_inact_time))
                mostrarAdvertenciaReinicio = true;

            return result > 0;
        } else
            return false;


    }



    public void LeerConfig() {

        try {
            c = new Config(this, true);

            //this.txtComercio.setText(c.COMERCIO);
            //this.txtDireccion.setText(c.DIRECCION);
            //this.txtClaveFiscal.setText(c.CLAVEFISCAL);
            //this.txtRecargaNroComercio.setText(c.RECARGA_NRO_COMERCIO);
            //this.txtRecargaIdTerminal.setText(c.RECARGA_ID_TERMINAL);
            //this.txtIMEI.setText(c.IMEI());
            //this.txtIMSI.setText(c.IMSI);
            //this.txtSERIAL_SIM.setText(c.SIM_SERIALNUMBER);
            this.txtServerHost.setText(c.CLOUD_SERVER_HOST);
            //this.txtIdComercio.setText(c.ID_COMERCIO);
            this.chkSyncCaja.setChecked(c.SYNC_CAJA);
            this.chkSyncLog.setChecked(c.SYNC_LOGS);
            this.chkSyncStock.setChecked(c.SYNC_STOCK);

            //this.txtPerfil.setText(c.PERFIL);

            this.txtSyncTime.setText(String.valueOf(c.SYNC_TIME));
            this.txtHistDays.setText(String.valueOf(c.HIST_DAYS));

            old_inact_time = String.valueOf(c.INACTIVITY_TIME);
            this.txtInactivityTime.setText(old_inact_time);

            int modelIndex = 0;
            for (int i = 0; i < printerModelosValues.length; i++) {
                if (printerModelosValues[i].equals(c.PRINTER_MODEL)) {
                    modelIndex = i;
                    break;
                }
            }

            this.spnPrinterModel.setSelection(modelIndex);

            int paisIndex = 0;
            for (int i = 0; i < paisesValues.length; i++) {
                if (paisesValues[i].equals(c.PAIS)) {
                    paisIndex = i;
                    break;
                }
            }

            this.spnPais.setSelection(paisIndex);

            //LeerConfigAplicaciones();

            List<ConfigRow> valores = new ArrayList<ConfigRow>();
            valores.add(new ConfigRow("IMEI / MEID", c.IMEI()));
            valores.add(new ConfigRow("Subscriber ID (IMSI)", c.IMSI));
            valores.add(new ConfigRow("SIM Card Serial", c.SIM_SERIALNUMBER));
            valores.add(new ConfigRow("Android ID", c.ANDROID_ID));
            valores.add(new ConfigRow("BPOS ID", c.GOOGLE_ACCOUNT));
            valores.add(new ConfigRow("WiFi MAC Address", c.WIFI_MAC));
            valores.add(new ConfigRow("Ethernet MAC Address", c.LAN_MAC));
            ListView lstValores = (ListView) findViewById(R.id.config_lst_values);
            lstValores.setAdapter(new ConfigAdapter(this, valores));

            List<ConfigRow> valores_sim = new ArrayList<ConfigRow>();
            valores_sim.add(new ConfigRow("STATE", c.SIM_STATE));
            valores_sim.add(new ConfigRow("PHONE TYPE", c.SIM_PHONE_TYPE));
            valores_sim.add(new ConfigRow("COUNTRY ISO", c.SIM_COUNTRY));
            valores_sim.add(new ConfigRow("PHONE NUMBER", c.SIM_LINE1_NRO));
            valores_sim.add(new ConfigRow("NETWORK COUNTRY ISO", c.SIM_NETWORK_COUNTRY));
            valores_sim.add(new ConfigRow("NETWORK OPERATOR CODE", c.SIM_NETWORK_OPERATOR));
            valores_sim.add(new ConfigRow("NETWORK OPERATOR NAME", c.SIM_NETWORK_OPERATOR_NAME));
            valores_sim.add(new ConfigRow("NETWORK TYPE", c.SIM_NETWORK_TYPE));
            valores_sim.add(new ConfigRow("OPERATOR CODE", c.SIM_OPERATOR_CODE));
            valores_sim.add(new ConfigRow("OPERATOR NAME", c.SIM_OPERATOR_NAME));
            valores_sim.add(new ConfigRow("VOICE MAIL NUMBER", c.SIM_VOICE_MAIL_NRO));
            valores_sim.add(new ConfigRow("VOICE MAIL TEXT ID", c.SIM_VOICE_MAIL));
            ListView lstValoresSim = (ListView) findViewById(R.id.config_lst_values_sim);
            lstValoresSim.setAdapter(new ConfigAdapter(this, valores_sim));

        } catch (Exception ex) {
            Toast.makeText(this, "Error al leer la configuracion:" + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    /*private void LeerConfigAplicaciones() {
        if(ContentProviders.ProviderIsAvailable(this, BoletaConfig.CONTENT_PROVIDER.getAuthority()))
            LeerConfigBoleta();

        if (ContentProviders.ProviderIsAvailable(this, PDVConfig.CONTENT_PROVIDER.getAuthority()))
            LeerConfigPDV();
    }*/

    /*private void LeerConfigBoleta() {
        try {

            //solo aplica si tiene el perfil de FacturaMovil
            if (c.PERFIL.equals(Perfiles.PERFIL_FACTURA_MOVIL) || c.PERFIL.equals(Perfiles.PERFIL_IQCORP)) {

                findViewById(R.id.config_btn_boleta).setVisibility(View.VISIBLE);

                Cursor cursor = getContentResolver().query(BoletaConfig.CONTENT_PROVIDER, BoletaConfig.PROJECTION, null, null, null);

                if (cursor.moveToFirst()) {

                    int i = cursor.getColumnIndex(BoletaConfig.BOLETA_FM_HOST);
                    boleta_txtFMServerHost.setText(i != -1 ? cursor.getString(i) : "");
                    i = cursor.getColumnIndex(BoletaConfig.BOLETA_FM_PORT);
                    boleta_txtFMServerPort.setText(i != -1 ? cursor.getString(i) : "");
                    i = cursor.getColumnIndex(BoletaConfig.BOLETA_PDS_HOST);
                    boleta_txtPDSServerHost.setText(i != -1 ? cursor.getString(i) : "");
                    i = cursor.getColumnIndex(BoletaConfig.BOLETA_PDS_PORT);
                    boleta_txtPDSServerPort.setText(i != -1 ? cursor.getString(i) : "");
                }

            }

        } catch (Exception ex) {
            Log.e("com.pds.ficle.ep", ex.getMessage());
        }
    }*/

    /*private void LeerConfigPDV() {
        try {
            Cursor cursor = getContentResolver().query(PDVConfig.CONTENT_PROVIDER, PDVConfig.PROJECTION, null, null, null);

            if (cursor.moveToFirst()) {

                int i = cursor.getColumnIndex(PDVConfig.PDV_EMITIR_VALE);
                pdv_config_chk_vale.setChecked(i != -1 ? Boolean.parseBoolean(cursor.getString(i)) : false);
                //

            }

        } catch (Exception ex) {
            Toast.makeText(config_activity.this, "Error al leer configuracion de PDV : " + ex.toString(), Toast.LENGTH_SHORT).show();
        }

    }*/

    /*
    private void CambiarEstadoPanel(View panel) {
        if (panel.getVisibility() == View.VISIBLE)
            panel.setVisibility(View.GONE);
        else {
            //primero cerramos todos
            CerrarPaneles();
            //abrimos este
            panel.setVisibility(View.VISIBLE);
        }
    }*/

    public void FragmentLogs() {

        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.frg_log) != null) {

            // Create a new Fragment to be placed in the activity layout
            LogFragment fragment = new LogFragment();

            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.frg_log, fragment);
            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            ft.commit();
        }

    }

    /*private void CerrarPaneles() {
        findViewById(R.id.config_boleta_layout).setVisibility(View.GONE);
    }*/


    class ConfigRow {
        private String _titulo;
        private String _descripcion;

        public String get_descripcion() {
            return _descripcion;
        }

        public String get_titulo() {
            return _titulo;
        }

        public ConfigRow(String titulo, String descripcion) {
            this._titulo = titulo;
            this._descripcion = descripcion;
        }
    }

    class ConfigAdapter extends ArrayAdapter<ConfigRow> {
        private Context context;
        private List<ConfigRow> datos;

        public ConfigAdapter(Context context, List<ConfigRow> datos) {
            super(context, android.R.layout.simple_list_item_1, datos);

            this.context = context;
            this.datos = datos;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // En primer lugar "inflamos" una nueva vista, que será la que se
            // mostrará en la celda del ListView. Para ello primero creamos el
            // inflater, y después inflamos la vista.
            LayoutInflater inflater = LayoutInflater.from(context);
            View item = inflater.inflate(R.layout.log_item, null);

            ConfigRow row = datos.get(position);

            // A partir de la vista, recogeremos los controles que contiene para
            // poder manipularlos.

            // Recogemos los TextView para mostrar datos
            ((TextView) item.findViewById(R.id.log_item_titulo)).setText(row.get_titulo());
            ((TextView) item.findViewById(R.id.log_item_fecha)).setVisibility(View.GONE);
            ((TextView) item.findViewById(R.id.log_item_detalle)).setText(row.get_descripcion());
            ((TextView) item.findViewById(R.id.log_item_detalle2)).setVisibility(View.GONE);

            // Devolvemos la vista para que se muestre en el ListView.
            return item;

        }
    }

    /**
     * Shows the progress UI and hides the main layout.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        final View _progressView = findViewById(R.id.ep_progress);
        final View _epMainView = findViewById(R.id.config_main);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            _progressView.setVisibility(View.VISIBLE);
            _progressView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(show ? 1 : 0)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            _progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                        }
                    });


            _epMainView.setVisibility(View.VISIBLE);
            _epMainView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(show ? 0 : 1)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            _epMainView.setVisibility(show ? View.GONE : View.VISIBLE);
                        }
                    });

        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            _progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            _epMainView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    public void showProgressMessage(CharSequence message) {
        TextView _progressMessageView = (TextView) findViewById(R.id.progress_status_message);
        _progressMessageView.setText(message);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.config, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
          /*  case R.id.action_get_images: {
                getImagesFromServer("frut_verd");
                return true;
            }
            case R.id.action_get_images_cigarrillos: {
                getImagesFromServer("cigarr_ar");
                return true;
            }
            case R.id.action_generate_products: {
                importProductsFromServer("cat_productos_ch", "");
                return true;
            }
            case R.id.action_generate_products_ch_almacen: {
                importProductsFromServer("cat_almacen_ch", "");
                return true;
            }
            case R.id.action_generate_products_ch_botilleria: {
                importProductsFromServer("cat_alm_boti_ch", "");
                return true;
            }
            case R.id.action_generate_products_cigarrillos: {
                importProductsFromServer("cat_cigarrillos", "");
                return true;
            }
            case R.id.action_generate_providers_ch: {
                importProvidersFromServer("providers_ch");
                return true;
            }
            case R.id.action_generate_products_ar: {
                importProductsFromServer("cat_productos_ar", "");
                return true;
            }*/
            /*case R.id.action_get_categorias: {
                importCategoriasFromServer("cat_gs1");
                return true;
            }*/
            case R.id.action_opt_clonar_terminal: {
                Intent i = new Intent(config_activity.this, MigrationActivity.class);
                startActivity(i);
            }
            break;
            case R.id.action_opt_borrado_historico: {
                new ClearOldDataTask(this).execute();
            }
            break;
        }

        return super.onOptionsItemSelected(item);
    }

   /* private void getImagesFromServer(String catalog) {
        try {
           *//* GetImagesTask _getTask = new GetImagesTask(this);
            _getTask.execute(catalog);*//*
        } catch (Exception ex) {
            Toast.makeText(this, "Error:" + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }*/

  /*  private void importProductsFromServer(String catalog, String categorias) {
        try {
            GetProductCatalogTask _getTask = new GetProductCatalogTask(this, categorias, catalog);
            //String _url = "http://" + c.CLOUD_SERVER_HOST + ":" + c.CLOUD_SERVER_PORT + "/download/csv/" + catalog + ".csv";
            //_getTask.execute(_url);

        } catch (Exception ex) {
            Toast.makeText(this, "Error:" + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }*/



    private void importProvidersFromServer(String catalog) {
        try {
            /*GetProvidersCatalogTask _getTask = new GetProvidersCatalogTask(this);
            _getTask.execute(catalog);*/
        } catch (Exception ex) {
            Toast.makeText(this, "Error:" + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void AlertMessage(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(config_activity.this);
        builder.setTitle("Configuracion Actualizada");
        builder.setMessage(message);
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface _dialog, int which) {
                _dialog.dismiss();
            }
        });
        builder.setCancelable(false);
        builder.show();
    }

}