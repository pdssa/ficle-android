package com.pds.ficle.ep.provider;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.pds.common.DbHelper;
import com.pds.common.db.CategoriaTable;
import com.pds.common.db.CierreTable;
import com.pds.common.db.ClienteTable;
import com.pds.common.db.ComboItemTable;
import com.pds.common.db.ComboTable;
import com.pds.common.db.CompraDetalleTable;
import com.pds.common.db.CompraTable;
import com.pds.common.db.ComprobanteDetalleTable;
import com.pds.common.db.ComprobanteTable;
import com.pds.common.db.CuentaTable;
import com.pds.common.db.DepartmentTable;
import com.pds.common.db.HistMovStockTable;
import com.pds.common.db.PedidoDetalleTable;
import com.pds.common.db.PedidoTable;
import com.pds.common.db.ProductProvidersTable;
import com.pds.common.db.ProductTable;
import com.pds.common.db.PromocionTable;
import com.pds.common.db.ProviderTable;
import com.pds.common.db.RangosFoliosTable;
import com.pds.common.db.SubdepartmentTable;
import com.pds.common.db.TaxTable;
import com.pds.common.db.TipoDocTable;
import com.pds.common.db.VentaAnuladaDetalleTable;
import com.pds.common.db.VentaAnuladaTable;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by raul.lopez on 5/16/2014.
 */
public class GenericProvider extends ContentProvider {
    static final String PROVIDER_NAME = "com.pds.ficle.ep.provider";
    static final String URL = "content://" + PROVIDER_NAME + "/";

    static final Uri DEPARTMENTS_CONTENT_URI = Uri.parse(URL + DepartmentTable.TABLE_NAME);
    static final Uri SUBDEPARTMENTS_CONTENT_URI = Uri.parse(URL + SubdepartmentTable.TABLE_NAME);
    static final Uri PROVIDERS_CONTENT_URI = Uri.parse(URL + ProviderTable.TABLE_NAME);
    static final Uri PRODUCTS_CONTENT_URI = Uri.parse(URL + ProductTable.TABLE_NAME);
    static final Uri TAXES_CONTENT_URI = Uri.parse(URL + TaxTable.TABLE_NAME);
    static final Uri CLIENTES_CONTENT_URI = Uri.parse(URL + ClienteTable.TABLE_NAME);
    static final Uri CUENTACTE_CONTENT_URI = Uri.parse(URL + CuentaTable.TABLE_NAME);
    static final Uri CUENTACTE_TOTALES_URI = Uri.parse(URL + CuentaTable.VW_TOTALES_NAME);
    static final Uri COMPROBANTE_CONTENT_URI = Uri.parse(URL + ComprobanteTable.TABLE_NAME);
    static final Uri COMPROBANTE_DET_CONTENT_URI = Uri.parse(URL + ComprobanteDetalleTable.TABLE_NAME);
    static final Uri COMPRA_CONTENT_URI = Uri.parse(URL + CompraTable.TABLE_NAME);
    static final Uri COMPRA_DET_CONTENT_URI = Uri.parse(URL + CompraDetalleTable.TABLE_NAME);
    static final Uri PRODUCT_PROVIDERS_CONTENT_URI = Uri.parse(URL + ProductProvidersTable.TABLE_NAME);
    static final Uri CIERRE_URI = Uri.parse(URL + CierreTable.TABLE_NAME);
    static final Uri HIST_MOV_STOCK_URI = Uri.parse(URL + HistMovStockTable.TABLE_NAME);
    static final Uri CATEGORIA_URI = Uri.parse(URL + CategoriaTable.TABLE_NAME);
    static final Uri RANGOS_FOLIOS_URI = Uri.parse(URL + RangosFoliosTable.TABLE_NAME);
    static final Uri TIPO_DOC_URI = Uri.parse(URL + TipoDocTable.TABLE_NAME);
    static final Uri PEDIDO_URI = Uri.parse(URL + PedidoTable.TABLE_NAME);
    static final Uri PEDIDO_DET_URI = Uri.parse(URL + PedidoDetalleTable.TABLE_NAME);
    static final Uri VENTA_ANULADA_URI = Uri.parse(URL + VentaAnuladaTable.TABLE_NAME);
    static final Uri VENTA_ANULADA_DET_URI = Uri.parse(URL + VentaAnuladaDetalleTable.TABLE_NAME);
    static final Uri COMBO_URI = Uri.parse(URL + ComboTable.TABLE_NAME);
    static final Uri COMBO_ITEM_URI = Uri.parse(URL + ComboItemTable.TABLE_NAME);
    static final Uri PROMOCION_URI = Uri.parse(URL + PromocionTable.TABLE_NAME);
    static final Uri MAS_VENDIDOS_URI = Uri.parse(URL + ProductTable.VW_TOP_VENTAS_NAME);

    static final UriMatcher sUriMatcher;

    private static final int DEPARTMENTS = 1;
    private static final int DEPARTMENT_ID = 2;
    private static final int SUB_DEPARTMENTS = 3;
    private static final int SUB_DEPARTMENT_ID = 4;
    private static final int PROVIDERS = 5;
    private static final int PROVIDER_ID = 6;
    private static final int PRODUCTS = 7;
    private static final int PRODUCT_ID = 8;
    private static final int TAXES = 9;
    private static final int TAX_ID = 10;
    private static final int CLIENTES = 11;
    private static final int CLIENTE_ID = 12;
    private static final int CUENTACTES = 13;
    private static final int CUENTACTE_ID = 14;
    private static final int CUENTACTE_TOTALES = 15;
    private static final int CUENTACTE_TOTAL_ID = 16;
    private static final int COMPROBANTES = 17;
    private static final int COMPROBANTE_ID = 18;
    private static final int COMPROBANTE_DETS = 19;
    private static final int COMPROBANTE_DET_ID = 20;
    private static final int COMPRAS = 21;
    private static final int COMPRA_ID = 22;
    private static final int COMPRA_DETS = 23;
    private static final int COMPRA_DET_ID = 24;
    private static final int PRODUCT_PROVIDERS = 25;
    private static final int PRODUCT_PROVIDER_ID = 26;
    private static final int CIERRES = 27;
    private static final int CIERRE_ID = 28;
    private static final int HIST_MOV_STOCK = 29;
    private static final int HIST_MOV_STOCK_ID = 30;
    private static final int CATEGORIA = 31;
    private static final int CATEGORIA_ID = 32;
    private static final int RANGOS_FOLIOS = 33;
    private static final int RANGOS_FOLIOS_ID = 34;
    private static final int TIPO_DOC = 35;
    private static final int TIPO_DOC_ID = 36;
    private static final int PEDIDO = 37;
    private static final int PEDIDO_ID = 38;
    private static final int PEDIDO_DETALLE = 39;
    private static final int PEDIDO_DETALLE_ID = 40;
    private static final int VENTA_ANULADA = 41;
    private static final int VENTA_ANULADA_ID = 42;
    private static final int VENTA_ANULADA_DET = 43;
    private static final int VENTA_ANULADA_DET_ID = 44;
    private static final int COMBO = 45;
    private static final int COMBO_ID = 46;
    private static final int COMBO_ITEM = 47;
    private static final int COMBO_ITEM_ID = 48;
    private static final int PROMOCION = 49;
    private static final int PROMOCION_ID = 50;
    private static final int MAS_VENDIDOS = 51;

    private static Map<Integer, TableInformation> tableInformationMap;
    private SQLiteDatabase db;

    private static class TableInformation {
        public String tableName;
        public Uri uri;
        public String idColumnName;

        public TableInformation(String tableName, Uri uri, String idColumnName) {
            this.tableName = tableName;
            this.uri = uri;
            this.idColumnName = idColumnName;
        }
    }

    static {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(PROVIDER_NAME, "departments", DEPARTMENTS);
        sUriMatcher.addURI(PROVIDER_NAME, "departments/#", DEPARTMENT_ID);
        sUriMatcher.addURI(PROVIDER_NAME, "subdepartments", SUB_DEPARTMENTS);
        sUriMatcher.addURI(PROVIDER_NAME, "subdepartments/#", SUB_DEPARTMENT_ID);
        sUriMatcher.addURI(PROVIDER_NAME, "providers", PROVIDERS);
        sUriMatcher.addURI(PROVIDER_NAME, "providers/#", PROVIDER_ID);
        sUriMatcher.addURI(PROVIDER_NAME, "products", PRODUCTS);
        sUriMatcher.addURI(PROVIDER_NAME, "products/#", PRODUCT_ID);
        sUriMatcher.addURI(PROVIDER_NAME, "taxes", TAXES);
        sUriMatcher.addURI(PROVIDER_NAME, "taxes/#", TAX_ID);
        sUriMatcher.addURI(PROVIDER_NAME, "clientes", CLIENTES);
        sUriMatcher.addURI(PROVIDER_NAME, "clientes/#", CLIENTE_ID);
        sUriMatcher.addURI(PROVIDER_NAME, "ctacte", CUENTACTES);
        sUriMatcher.addURI(PROVIDER_NAME, "ctacte/#", CUENTACTE_ID);
        sUriMatcher.addURI(PROVIDER_NAME, CuentaTable.VW_TOTALES_NAME, CUENTACTE_TOTALES);
        sUriMatcher.addURI(PROVIDER_NAME, CuentaTable.VW_TOTALES_NAME + "/#", CUENTACTE_TOTAL_ID);
        sUriMatcher.addURI(PROVIDER_NAME, ComprobanteTable.TABLE_NAME, COMPROBANTES);
        sUriMatcher.addURI(PROVIDER_NAME, ComprobanteTable.TABLE_NAME + "/#", COMPROBANTE_ID);
        sUriMatcher.addURI(PROVIDER_NAME, ComprobanteDetalleTable.TABLE_NAME, COMPROBANTE_DETS);
        sUriMatcher.addURI(PROVIDER_NAME, ComprobanteDetalleTable.TABLE_NAME + "/#", COMPROBANTE_DET_ID);
        sUriMatcher.addURI(PROVIDER_NAME, CompraTable.TABLE_NAME, COMPRAS);
        sUriMatcher.addURI(PROVIDER_NAME, CompraTable.TABLE_NAME + "/#", COMPRA_ID);
        sUriMatcher.addURI(PROVIDER_NAME, CompraDetalleTable.TABLE_NAME, COMPRA_DETS);
        sUriMatcher.addURI(PROVIDER_NAME, CompraDetalleTable.TABLE_NAME + "/#", COMPRA_DET_ID);
        sUriMatcher.addURI(PROVIDER_NAME, ProductProvidersTable.TABLE_NAME, PRODUCT_PROVIDERS);
        sUriMatcher.addURI(PROVIDER_NAME, ProductProvidersTable.TABLE_NAME + "/#", PRODUCT_PROVIDER_ID);
        sUriMatcher.addURI(PROVIDER_NAME, CierreTable.TABLE_NAME, CIERRES);
        sUriMatcher.addURI(PROVIDER_NAME, CierreTable.TABLE_NAME + "/#", CIERRE_ID);
        sUriMatcher.addURI(PROVIDER_NAME, HistMovStockTable.TABLE_NAME, HIST_MOV_STOCK);
        sUriMatcher.addURI(PROVIDER_NAME, HistMovStockTable.TABLE_NAME + "/#", HIST_MOV_STOCK_ID);
        sUriMatcher.addURI(PROVIDER_NAME, CategoriaTable.TABLE_NAME, CATEGORIA);
        sUriMatcher.addURI(PROVIDER_NAME, CategoriaTable.TABLE_NAME + "/#", CATEGORIA_ID);
        sUriMatcher.addURI(PROVIDER_NAME, RangosFoliosTable.TABLE_NAME, RANGOS_FOLIOS);
        sUriMatcher.addURI(PROVIDER_NAME, RangosFoliosTable.TABLE_NAME + "/#", RANGOS_FOLIOS_ID);
        sUriMatcher.addURI(PROVIDER_NAME, TipoDocTable.TABLE_NAME, TIPO_DOC);
        sUriMatcher.addURI(PROVIDER_NAME, TipoDocTable.TABLE_NAME + "/#", TIPO_DOC_ID);
        sUriMatcher.addURI(PROVIDER_NAME, PedidoTable.TABLE_NAME, PEDIDO);
        sUriMatcher.addURI(PROVIDER_NAME, PedidoTable.TABLE_NAME + "/#", PEDIDO_ID);
        sUriMatcher.addURI(PROVIDER_NAME, PedidoDetalleTable.TABLE_NAME, PEDIDO_DETALLE);
        sUriMatcher.addURI(PROVIDER_NAME, PedidoDetalleTable.TABLE_NAME + "/#", PEDIDO_DETALLE_ID);
        sUriMatcher.addURI(PROVIDER_NAME, VentaAnuladaTable.TABLE_NAME, VENTA_ANULADA);
        sUriMatcher.addURI(PROVIDER_NAME, VentaAnuladaTable.TABLE_NAME + "/#", VENTA_ANULADA_ID);
        sUriMatcher.addURI(PROVIDER_NAME, VentaAnuladaDetalleTable.TABLE_NAME, VENTA_ANULADA_DET);
        sUriMatcher.addURI(PROVIDER_NAME, VentaAnuladaDetalleTable.TABLE_NAME + "/#", VENTA_ANULADA_DET_ID);
        sUriMatcher.addURI(PROVIDER_NAME, ComboTable.TABLE_NAME, COMBO);
        sUriMatcher.addURI(PROVIDER_NAME, ComboTable.TABLE_NAME + "/#", COMBO_ID);
        sUriMatcher.addURI(PROVIDER_NAME, ComboItemTable.TABLE_NAME, COMBO_ITEM);
        sUriMatcher.addURI(PROVIDER_NAME, ComboItemTable.TABLE_NAME + "/#", COMBO_ITEM_ID);
        sUriMatcher.addURI(PROVIDER_NAME, PromocionTable.TABLE_NAME, PROMOCION);
        sUriMatcher.addURI(PROVIDER_NAME, PromocionTable.TABLE_NAME + "/#", PROMOCION_ID);
        sUriMatcher.addURI(PROVIDER_NAME, ProductTable.VW_TOP_VENTAS_NAME, MAS_VENDIDOS);

        TableInformation departmentTableInfo = new TableInformation(DepartmentTable.TABLE_NAME, DEPARTMENTS_CONTENT_URI, DepartmentTable.COLUMN_ID);
        TableInformation subDepartmentTableInfo = new TableInformation(SubdepartmentTable.TABLE_NAME, SUBDEPARTMENTS_CONTENT_URI, SubdepartmentTable.COLUMN_ID);
        TableInformation providerTableInfo = new TableInformation(ProviderTable.TABLE_NAME, PROVIDERS_CONTENT_URI, ProviderTable.COLUMN_ID);
        TableInformation productTableInfo = new TableInformation(ProductTable.TABLE_NAME, PRODUCTS_CONTENT_URI, ProductTable.COLUMN_ID);
        TableInformation taxTableInfo = new TableInformation(TaxTable.TABLE_NAME, TAXES_CONTENT_URI, TaxTable.COLUMN_ID);
        TableInformation clienteTableInfo = new TableInformation(ClienteTable.TABLE_NAME, CLIENTES_CONTENT_URI, ClienteTable.COLUMN_ID);
        TableInformation cuentacteTableInfo = new TableInformation(CuentaTable.TABLE_NAME, CUENTACTE_CONTENT_URI, CuentaTable.COLUMN_ID);
        TableInformation cuentacteTotalesViewInfo = new TableInformation(CuentaTable.VW_TOTALES_NAME, CUENTACTE_TOTALES_URI, CuentaTable.COLUMN_VW_CLIENTE_ID);
        TableInformation comprobanteTableInfo = new TableInformation(ComprobanteTable.TABLE_NAME, COMPROBANTE_CONTENT_URI, ComprobanteTable.COMPROBANTE_ID);
        TableInformation comprobanteDetTableInfo = new TableInformation(ComprobanteDetalleTable.TABLE_NAME, COMPROBANTE_DET_CONTENT_URI, ComprobanteDetalleTable.COMPROBANTE_DET_ID);
        TableInformation compraTableInfo = new TableInformation(CompraTable.TABLE_NAME, COMPRA_CONTENT_URI, CompraTable.COMPRA_ID);
        TableInformation compraDetTableInfo = new TableInformation(CompraDetalleTable.TABLE_NAME, COMPRA_DET_CONTENT_URI, CompraDetalleTable.COMPRA_DET_ID);
        TableInformation productProvidersTableInfo = new TableInformation(ProductProvidersTable.TABLE_NAME, PRODUCTS_CONTENT_URI, ProductProvidersTable.COLUMN_ID);
        TableInformation cierreTableInfo = new TableInformation(CierreTable.TABLE_NAME, CIERRE_URI, CierreTable.COLUMN_ID);
        TableInformation histMovStockTableInfo = new TableInformation(HistMovStockTable.TABLE_NAME, HIST_MOV_STOCK_URI, HistMovStockTable.STOCK_ID);
        TableInformation categoriaTableInfo = new TableInformation(CategoriaTable.TABLE_NAME, CATEGORIA_URI, CategoriaTable.COLUMN_ID);
        TableInformation rangosFoliosTableInfo = new TableInformation(RangosFoliosTable.TABLE_NAME, RANGOS_FOLIOS_URI, RangosFoliosTable.COLUMN_ID);
        TableInformation pedidosTableInfo = new TableInformation(PedidoTable.TABLE_NAME, PEDIDO_URI, PedidoTable.PEDIDO_ID);
        TableInformation pedidosDetalleTableInfo = new TableInformation(PedidoDetalleTable.TABLE_NAME, PEDIDO_DET_URI, PedidoDetalleTable.PEDIDO_DET_ID);
        TableInformation tipoDocTableInfo = new TableInformation(TipoDocTable.TABLE_NAME, TIPO_DOC_URI, TipoDocTable.COLUMN_ID);
        TableInformation ventaAnuladaTableInfo = new TableInformation(VentaAnuladaTable.TABLE_NAME, VENTA_ANULADA_URI, VentaAnuladaTable.COLUMN_ID);
        TableInformation ventaAnuladaDetalleTableInfo = new TableInformation(VentaAnuladaDetalleTable.TABLE_NAME, VENTA_ANULADA_DET_URI, VentaAnuladaDetalleTable.COLUMN_ID);
        TableInformation comboTableInfo = new TableInformation(ComboTable.TABLE_NAME, COMBO_URI, ComboTable.COLUMN_ID);
        TableInformation comboItemTableInfo = new TableInformation(ComboItemTable.TABLE_NAME, COMBO_ITEM_URI, ComboItemTable.COLUMN_ID);
        TableInformation promocionTableInfo = new TableInformation(PromocionTable.TABLE_NAME, PROMOCION_URI, PromocionTable.COLUMN_ID);
        TableInformation masVendidosTableInfo = new TableInformation(ProductTable.VW_TOP_VENTAS_NAME, MAS_VENDIDOS_URI, ProductTable.COLUMN_ID);

        tableInformationMap = new TreeMap<Integer, TableInformation>();
        tableInformationMap.put(DEPARTMENT_ID, departmentTableInfo);
        tableInformationMap.put(DEPARTMENTS, departmentTableInfo);
        tableInformationMap.put(SUB_DEPARTMENT_ID, subDepartmentTableInfo);
        tableInformationMap.put(SUB_DEPARTMENTS, subDepartmentTableInfo);
        tableInformationMap.put(PROVIDER_ID, providerTableInfo);
        tableInformationMap.put(PROVIDERS, providerTableInfo);
        tableInformationMap.put(PRODUCTS, productTableInfo);
        tableInformationMap.put(PRODUCT_ID, productTableInfo);
        tableInformationMap.put(TAXES, taxTableInfo);
        tableInformationMap.put(TAX_ID, taxTableInfo);
        tableInformationMap.put(CLIENTES, clienteTableInfo);
        tableInformationMap.put(CLIENTE_ID, clienteTableInfo);
        tableInformationMap.put(CUENTACTES, cuentacteTableInfo);
        tableInformationMap.put(CUENTACTE_ID, cuentacteTableInfo);
        tableInformationMap.put(CUENTACTE_TOTALES, cuentacteTotalesViewInfo);
        tableInformationMap.put(CUENTACTE_TOTAL_ID, cuentacteTotalesViewInfo);
        tableInformationMap.put(COMPROBANTES, comprobanteTableInfo);
        tableInformationMap.put(COMPROBANTE_ID, comprobanteTableInfo);
        tableInformationMap.put(COMPROBANTE_DETS, comprobanteDetTableInfo);
        tableInformationMap.put(COMPROBANTE_DET_ID, comprobanteDetTableInfo);
        tableInformationMap.put(COMPRAS, compraTableInfo);
        tableInformationMap.put(COMPRA_ID, compraTableInfo);
        tableInformationMap.put(COMPRA_DETS, compraDetTableInfo);
        tableInformationMap.put(COMPRA_DET_ID, compraDetTableInfo);
        tableInformationMap.put(PRODUCT_PROVIDERS, productProvidersTableInfo);
        tableInformationMap.put(PRODUCT_PROVIDER_ID, productProvidersTableInfo);
        tableInformationMap.put(CIERRES, cierreTableInfo);
        tableInformationMap.put(CIERRE_ID, cierreTableInfo);
        tableInformationMap.put(HIST_MOV_STOCK, histMovStockTableInfo);
        tableInformationMap.put(HIST_MOV_STOCK_ID, histMovStockTableInfo);
        tableInformationMap.put(CATEGORIA, categoriaTableInfo);
        tableInformationMap.put(CATEGORIA_ID, categoriaTableInfo);
        tableInformationMap.put(RANGOS_FOLIOS, rangosFoliosTableInfo);
        tableInformationMap.put(RANGOS_FOLIOS_ID, rangosFoliosTableInfo);
        tableInformationMap.put(TIPO_DOC, tipoDocTableInfo);
        tableInformationMap.put(TIPO_DOC_ID, tipoDocTableInfo);
        tableInformationMap.put(PEDIDO, pedidosTableInfo);
        tableInformationMap.put(PEDIDO_ID, pedidosTableInfo);
        tableInformationMap.put(PEDIDO_DETALLE, pedidosDetalleTableInfo);
        tableInformationMap.put(PEDIDO_DETALLE_ID, pedidosDetalleTableInfo);
        tableInformationMap.put(VENTA_ANULADA, ventaAnuladaTableInfo);
        tableInformationMap.put(VENTA_ANULADA_ID, ventaAnuladaTableInfo);
        tableInformationMap.put(VENTA_ANULADA_DET, ventaAnuladaDetalleTableInfo);
        tableInformationMap.put(VENTA_ANULADA_DET_ID, ventaAnuladaDetalleTableInfo);
        tableInformationMap.put(COMBO, comboTableInfo);
        tableInformationMap.put(COMBO_ID, comboTableInfo);
        tableInformationMap.put(COMBO_ITEM, comboItemTableInfo);
        tableInformationMap.put(COMBO_ITEM_ID, comboItemTableInfo);
        tableInformationMap.put(PROMOCION, promocionTableInfo);
        tableInformationMap.put(PROMOCION_ID, promocionTableInfo);
        tableInformationMap.put(MAS_VENDIDOS, masVendidosTableInfo);
    }

    @Override
    public ContentProviderResult[] applyBatch(ArrayList<ContentProviderOperation> operations) throws OperationApplicationException {
        return super.applyBatch(operations);
    }

    @Override
    public boolean onCreate() {
        //Log.d("GenericProvider", "onCreate: creating database");
        Context context = getContext();
        DbHelper dbHelper = new DbHelper(context);
        db = dbHelper.getWritableDatabase();
        return db != null;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Log.d("GenericProvider", "query " + uri);
        Cursor cursor;
        int match = sUriMatcher.match(uri);
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        String groupBy = null;
        String having = null;
        String limit = null;

        TableInformation tableInfo = tableInformationMap.get(match);
        if (tableInfo == null) {
            throw new IllegalArgumentException("Invalid URI " + uri);
        }
        if (match != MAS_VENDIDOS && match % 2 == 0) {
            queryBuilder.appendWhere(buildIdExpression(tableInfo, uri));
        }
        queryBuilder.setTables(tableInfo.tableName);

        cursor = queryBuilder.query(db, projection, selection, selectionArgs, groupBy, having, sortOrder, limit);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    private String buildIdExpression(TableInformation tableInformation, Uri uri) {
        return String.format("%s = %s", tableInformation.idColumnName, uri.getPathSegments().get(1));
    }

    @Override
    public String getType(Uri uri) {
        Log.d("GenericProvider", uri.toString());
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        Log.d("GenericProvider", "insert " + uri);
        int match = sUriMatcher.match(uri);
        TableInformation tableInfo = tableInformationMap.get(match);
        if (tableInfo == null || (match % 2 == 0)) {
            throw new IllegalArgumentException("Invalid uri " + uri);
        }
        long rowId = db.insert(tableInfo.tableName, "", values);
        if (rowId <= 0) {
            return null;
        }
        Uri newRecordUri = ContentUris.withAppendedId(tableInfo.uri, rowId);
        getContext().getContentResolver().notifyChange(newRecordUri, null);
        return newRecordUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        Log.d("GenericProvider", "delete " + uri);
        int uriType = sUriMatcher.match(uri);
        int deletedRows = 0;
        TableInformation tableInfo = tableInformationMap.get(uriType);
        if (tableInfo == null) {
            throw new IllegalArgumentException("Invalid uri " + uri);
        }
        if (uriType % 2 == 0) {
            selection = buildWhereClause(selection, tableInfo.idColumnName, uri);
        }
        deletedRows = db.delete(tableInfo.tableName, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return deletedRows;
    }

    private String buildWhereClause(String selection, String idColumnName, Uri uri) {
        String whereClause = String.format("%s = %s", idColumnName, uri.getPathSegments().get(1));
        if (TextUtils.isEmpty(selection)) {
            return whereClause;
        }
        return String.format("%s and %s", whereClause, selection);
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        Log.d("GenericProvider", "update " + uri);
        int updatedRows = 0;
        int match = sUriMatcher.match(uri);
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        TableInformation tableInfo = tableInformationMap.get(match);
        if (tableInfo == null) {
            throw new IllegalArgumentException("Invalid uri " + uri);
        }
        if (match % 2 == 0) {
            selection = this.buildWhereClause(selection, tableInfo.idColumnName, uri);
        }
        queryBuilder.setTables(tableInfo.tableName);
        updatedRows = db.update(tableInfo.tableName, values, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return updatedRows;
    }
}
