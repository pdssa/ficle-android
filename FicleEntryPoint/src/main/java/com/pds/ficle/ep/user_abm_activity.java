package com.pds.ficle.ep;

import android.app.AlertDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.PerfilUser;
import com.pds.common.SII.vale.model.CambioUser;
import com.pds.common.SII.vale.services.CambiarUserIDOnlineTask;
import com.pds.common.SII.vale.util.ValeUtils;
import com.pds.common.Usuario;
import com.pds.common.UsuarioHelper;
import com.pds.common.activity.Actividad;
import com.pds.common.dialog.TimePickerDialog;

public class user_abm_activity extends Actividad {
    private TextView txtUsername;
    private TextView txtNombre;
    private TextView txtApellido;
    private TextView txtPassword;
    private TextView txtPasswordConfirma;
    private TextView txtCelular;
    private TextView txtHorarioEntrada;
    private TextView txtHorarioSalida;
    private Button btnGrabar;
    private Button btnCancelar;
    private Button btnEliminar;
    private boolean modo_edit = false;
    private Usuario user = null;
    private UsuarioHelper userHelper = null;
    private Spinner spnPerfil;
    private final String[] perfiles = new String[]{"Administrador", "Vendedor", "Invitado"};
    private final String[] perfiles_sin_admin = new String[]{ "Vendedor", "Invitado"};

    private ArrayAdapter<String> adaptador;
    private String claveActual;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.user_abm);


            userHelper = new UsuarioHelper(this);

            //asignamos los view de la activity
            this.txtUsername = (TextView) findViewById(R.id.user_abm_txt_user);
            this.txtApellido = (TextView) findViewById(R.id.user_abm_txt_apellido);
            this.txtNombre = (TextView) findViewById(R.id.user_abm_txt_nombre);
            this.txtCelular = (TextView) findViewById(R.id.user_abm_txt_celular);
            this.txtPassword = (TextView) findViewById(R.id.user_abm_txt_password);
            this.txtPasswordConfirma = (TextView) findViewById(R.id.user_abm_txt_password2);
            this.txtHorarioEntrada = (TextView) findViewById(R.id.user_abm_txt_entrada);
            this.txtHorarioSalida = (TextView) findViewById(R.id.user_abm_txt_salida);
            this.btnGrabar = (Button) findViewById(R.id.user_abm_btn_grabar);
            this.btnEliminar = (Button) findViewById(R.id.config_btn_check);
            this.btnCancelar = (Button) findViewById(R.id.config_btn_cancelar);
            this.spnPerfil = (Spinner) findViewById(R.id.user_abm_spn_perfil);

            this.txtHorarioEntrada.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    TimePickerDialog.showTimePickerDialog(user_abm_activity.this,"Ingrese Hora de Entrada", new OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                            String hora = "00" + String.valueOf(hourOfDay);
                            hora = hora.substring(hora.length() - 2);
                            String minutos = "00" + String.valueOf(minute);
                            minutos = minutos.substring(minutos.length() - 2);

                            txtHorarioEntrada.setText(hora + ":" + minutos);
                        }
                    });
                }
            });

            this.txtHorarioSalida.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    TimePickerDialog.showTimePickerDialog(user_abm_activity.this,"Ingrese Hora de Salida", new OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                            String hora = "00" + String.valueOf(hourOfDay);
                            hora = hora.substring(hora.length() - 2);
                            String minutos = "00" + String.valueOf(minute);
                            minutos = minutos.substring(minutos.length() - 2);

                            txtHorarioSalida.setText(hora + ":" + minutos);
                        }
                    });
                }
            });


            btnGrabar.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        grabar();
                    } catch (Exception e) {
                        Toast.makeText(view.getContext(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            });

            btnEliminar.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {

                    //TODO: no debería eliminar el registro, solo darlo de baja...
                    new AlertDialog.Builder(user_abm_activity.this)
                            .setIcon(android.R.drawable.ic_menu_help)
                            .setTitle("Eliminar usuario")
                            .setMessage("Desea eliminar el usuario actual?")
                            .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    try {
                                        if (userHelper.delete(user)) {
                                            Toast.makeText(user_abm_activity.this, "Usuario eliminado con exito", Toast.LENGTH_SHORT).show();

                                            finish(); //cancela la actividad actual y retorna a la anterior
                                        } else
                                            Toast.makeText(user_abm_activity.this, "No se pudo eliminar el usuario!", Toast.LENGTH_SHORT).show();

                                    } catch (Exception e) {
                                        Toast.makeText(user_abm_activity.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                }
                            })
                            .setNegativeButton("NO", null)
                            .setCancelable(false)
                            .show();

                }
            });

            this.btnCancelar.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish(); //cancela la actividad actual y retorna a la anterior
                }
            });


            //obtenemos los parametros pasamos a la actividad
            Bundle extras = getIntent().getExtras();

            if (extras != null) {
                //vemos si es modo de edicion
                modo_edit = extras.getBoolean("modo_edit", false);

                user = (userHelper.getById(this, extras.getInt("_id")));
            }

            //cargamos los perfiles de usuario
            CargarPerfiles(modo_edit && user.getId() == 1);

            //ajustamos los controles segun el modo de la actividad
            if (modo_edit) {

                CargarUsuario();

                if (this.user.getId() == 1) {
                    //usuario admin: no se puede eliminar
                    btnEliminar.setVisibility(View.INVISIBLE);

                    //solo se pueden editar algunos campos, otros no están disponibles para edición
                    this.txtUsername.setEnabled(false);
                    this.txtApellido.setEnabled(false);
                    this.txtNombre.setEnabled(false);
                    this.spnPerfil.setEnabled(false);

                } else {
                    btnEliminar.setVisibility(View.VISIBLE);
                }


            } else {
                //TODO: un user comun no se deberia poder eliminar a si mismo (hay que considerar el usuario que esta logueado)
                btnEliminar.setVisibility(View.INVISIBLE);
            }



            /*
            if (savedInstanceState == null) {
                getFragmentManager().beginTransaction()
                        .add(R.id.user_abm_container, new PlaceholderFragment())
                        .commit();
            }
            */
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
            //Dialog.Alert(getApplicationContext(), "Error!" , e.getMessage());
        }
    }

    private void CargarUsuario() {
        this.txtUsername.setText(user.getLogin());
        this.txtApellido.setText(user.getApellido());
        this.txtNombre.setText(user.getNombre());
        this.txtCelular.setText(user.getCelular());
        this.txtPassword.setText(user.getPassword());
        this.txtPasswordConfirma.setText(user.getPassword());
        this.txtHorarioEntrada.setText(user.getHora_entrada());
        this.txtHorarioSalida.setText(user.getHora_salida());
        this.spnPerfil.setSelection(adaptador.getPosition(this.perfiles[user.getId_perfil() - 1].toString()));
        claveActual = user.getPassword();
    }

    private void CargarPerfiles(boolean con_admin) {
        adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, con_admin ? perfiles : perfiles_sin_admin);
        this.spnPerfil.setAdapter(adaptador);
        this.spnPerfil.setSelection(adaptador.getPosition("Vendedor"));
    }

    private void grabar() throws Exception {
        try {

            //reseteamos los errores
            txtUsername.setError(null);
            txtPassword.setError(null);
            txtPasswordConfirma.setError(null);
            txtNombre.setError(null);

            boolean cancel = false;
            View focusView = null;

            // Validamos los datos de entrada
            if (TextUtils.isEmpty(txtUsername.getText().toString())) {
                txtUsername.setError(getString(R.string.error_field_required));
                focusView = txtUsername;
                cancel = true;
            }

            if (TextUtils.isEmpty(txtPassword.getText().toString())) {
                txtPassword.setError(getString(R.string.error_field_required));
                focusView = txtPassword;
                cancel = true;
            } else if (txtPassword.length() != 4) {
                txtPassword.setError(getString(R.string.error_short_password));
                focusView = txtPassword;
                cancel = true;
            }

            if (TextUtils.isEmpty(txtPasswordConfirma.getText().toString())) {
                txtPasswordConfirma.setError(getString(R.string.error_field_required));
                focusView = txtPasswordConfirma;
                cancel = true;
            } else if (txtPasswordConfirma.length() != 4) {
                txtPasswordConfirma.setError(getString(R.string.error_short_password));
                focusView = txtPasswordConfirma;
                cancel = true;
            }

            if (!txtPassword.getText().toString().equals(txtPasswordConfirma.getText().toString())) {
                txtPasswordConfirma.setError("Passwords no coinciden");
                focusView = txtPasswordConfirma;
                cancel = true;
            }

            // Revisamos el nombre
            if (TextUtils.isEmpty(txtNombre.getText().toString())) {
                txtNombre.setError(getString(R.string.error_field_required));
                focusView = txtNombre;
                cancel = true;
            }


            if (cancel) {
                //hubo un error mostramos los errores en pantalla
                focusView.requestFocus();
            } else {

                if (modo_edit) {
                    user.setLogin(txtUsername.getText().toString());
                    user.setNombre(txtNombre.getText().toString());
                    user.setApellido(txtApellido.getText().toString());
                    user.setPassword(txtPassword.getText().toString());
                    user.setCelular(txtCelular.getText().toString());
                    //user.setId_perfil(adaptador.getPosition(spnPerfil.getSelectedItem().toString()) + 1);
                    user.setId_perfil(PerfilUser.getPerfil(spnPerfil.getSelectedItem().toString()));
                    user.setHora_entrada(txtHorarioEntrada.getText().toString().trim());
                    user.setHora_salida(txtHorarioSalida.getText().toString().trim());

                    if (userHelper.update(user)) {
                        Toast.makeText(this, "Usuario actualizado con exito", Toast.LENGTH_SHORT).show();

                        //si el user es admin, y está habilitada la emisión de vales, enviamos el cambio de password al servidor
                        Config c = new Config(this);

                        if(c.isValeInicializado() && user.getId_perfil() == 1) {

                            String clave_actual = ValeUtils.GetUserID(c.SII_TERMINAL_ID, claveActual);
                            String clave_nueva = ValeUtils.GetUserID(c.SII_TERMINAL_ID, user.getPassword());

                            CambioUser cambioUser = new CambioUser(c, clave_actual, clave_nueva, c.SII_MERCHANT_ID);

                            new CambiarUserIDOnlineTask(user_abm_activity.this, new CambiarUserIDOnlineTask.CambiarUserIDOnlineListener() {
                                @Override
                                public void onClaveModificada() {
                                    finish(); //cancela la actividad actual y retorna a la anterior
                                }

                                @Override
                                public void onError() {
                                    finish(); //cancela la actividad actual y retorna a la anterior
                                }
                            }).execute(cambioUser);
                        }
                        else
                            finish(); //cancela la actividad actual y retorna a la anterior

                    } else
                        Toast.makeText(this, "No se pudo actualizar el usuario!", Toast.LENGTH_SHORT).show();

                } else {
                    user = new Usuario();

                    user.setLogin(txtUsername.getText().toString());
                    user.setNombre(txtNombre.getText().toString());
                    user.setApellido(txtApellido.getText().toString());
                    user.setPassword(txtPassword.getText().toString());
                    user.setCelular(txtCelular.getText().toString());
                    //user.setId_perfil(adaptador.getPosition(spnPerfil.getSelectedItem().toString()) + 1);
                    user.setId_perfil(PerfilUser.getPerfil(spnPerfil.getSelectedItem().toString()));
                    user.setHora_entrada(txtHorarioEntrada.getText().toString().trim());
                    user.setHora_salida(txtHorarioSalida.getText().toString().trim());

                    userHelper.create(user);

                    Toast.makeText(this, "Usuario creado con exito", Toast.LENGTH_SHORT).show();

                    finish(); //cancela la actividad actual y retorna a la anterior
                }
            }


        } catch (Exception ex) {
            throw ex;
        }


    }



}
