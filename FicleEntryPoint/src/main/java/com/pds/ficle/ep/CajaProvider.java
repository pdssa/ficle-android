package com.pds.ficle.ep;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import com.pds.common.CajaHelper;
import com.pds.common.DbHelper;


/**
 * Created by Hernan on 03/02/14.
 */
public class CajaProvider extends ContentProvider {
    //para el UriMatcher
    private static final int CAJA = 1;
    private static final int CAJA_ID = 2;

    private static final String AUTHORITY = "com.pds.ficle.ep.caja.contentprovider";

    private static final String BASE_PATH = "caja";

    //Uri: content://com.pds.ficle.ep.logs.contentprovider/logs
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);

    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/caja";
    public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/caja_";

    //inicializamos las reglas posibles del UriMatcher:
    //tres opciones: que no coincida, que coincida sin id => valor 1, que coincida con id => valor 2
    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {

        uriMatcher.addURI(AUTHORITY, BASE_PATH, CAJA);

        uriMatcher.addURI(AUTHORITY, BASE_PATH + "/#", CAJA_ID);

    }

    private DbHelper database;

    @Override
    public boolean onCreate() {
        database = new DbHelper(getContext());
        return false;
    }

    @Override
    public String getType(Uri uri) {
        int uriType = uriMatcher.match(uri);

        switch (uriType) {
            case CAJA:
                return "vnd.android.cursor.dir/vnd.pds.caja";
            case CAJA_ID:
                return "vnd.android.cursor.item/vnd.pds.caja";
            default:
                return null;
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {

        int uriType = uriMatcher.match(uri);

        long new_id = 0;

        switch (uriType) {
            case CAJA:
                new_id = database.getWritableDatabase().insert(CajaHelper.TABLE_NAME, null, values);
                break;
            case CAJA_ID:
                break;
            default:
                throw new IllegalArgumentException("URI desconocida: " + uri);
        }

        return ContentUris.withAppendedId(CONTENT_URI, new_id);


    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int cont;

        int uriType = uriMatcher.match(uri);

        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();

        builder.setTables(CajaHelper.TABLE_NAME);

        //Si es una consulta a un ID concreto construimos el WHERE
        String where = selection;
        switch (uriType) {
            case CAJA:
                break;
            case CAJA_ID:
                where = CajaHelper.CAJA_ID + "=" + uri.getLastPathSegment();
                break;
            default:
                throw new IllegalArgumentException("URI desconocida: " + uri);
        }

        cont = database.getWritableDatabase().update(CajaHelper.TABLE_NAME, values, where, selectionArgs);

        return cont;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        //int limite = 100; //limitamos la cantidad de registros obtenidos
        int uriType = uriMatcher.match(uri);

        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();

        builder.setTables(CajaHelper.VIEW_NAME);

        //Si es una consulta a un ID concreto construimos el WHERE
        switch (uriType) {
            case CAJA:
                break;
            case CAJA_ID:
                builder.appendWhere(CajaHelper.CAJA_ID + "=" + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("URI desconocida: " + uri);
        }

        Cursor cursor = builder.query(database.getWritableDatabase(), projection, selection, selectionArgs, null, null, sortOrder);

        return cursor;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int cont;

        int uriType = uriMatcher.match(uri);

        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();

        builder.setTables(CajaHelper.TABLE_NAME);

        //Si es una consulta a un ID concreto construimos el WHERE
        String where = selection;
        switch (uriType) {
            case CAJA:
                break;
            case CAJA_ID:
                where = CajaHelper.CAJA_ID + "=" + uri.getLastPathSegment();
                break;
            default:
                throw new IllegalArgumentException("URI desconocida: " + uri);
        }

        cont = database.getWritableDatabase().delete(CajaHelper.TABLE_NAME, where, selectionArgs);

        return cont;
    }

}
