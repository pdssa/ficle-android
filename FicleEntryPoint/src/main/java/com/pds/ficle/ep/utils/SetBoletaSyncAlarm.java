package com.pds.ficle.ep.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.text.TextUtils;
import android.util.Log;

import com.pds.common.pref.BoletaConfig;
import com.pds.ficle.ep.services.SyncService;
import com.pds.common.util.ContentProviders;

import java.util.Calendar;

/**
 * Created by Hernan on 19/02/2015.
 */
public class SetBoletaSyncAlarm {

    public void setAlarm(Context context) {
        try {
            if (ContentProviders.ProviderIsAvailable(context, BoletaConfig.CONTENT_PROVIDER.getAuthority())) {

                //leemos los horarios configurados
                Cursor cursor = context.getContentResolver().query(BoletaConfig.CONTENT_PROVIDER, BoletaConfig.PROJECTION, null, null, null);

                String hora1 = "", hora2 = "";

                if (cursor.moveToFirst()) {

                    int i = cursor.getColumnIndex(BoletaConfig.BOLETA_HORA_SYNC_1);
                    hora1 = (i != -1 ? cursor.getString(i) : "");
                    i = cursor.getColumnIndex(BoletaConfig.BOLETA_HORA_SYNC_2);
                    hora2 = (i != -1 ? cursor.getString(i) : "");
                }

                cursor.close();

                if (TextUtils.isEmpty(hora1) && TextUtils.isEmpty(hora2)) {
                    Log.d("SetBoletaSyncAlarm", "No se encontraron alarmas configuradas para Boleta");
                    return;//salimos si no hay alarmas configuradas
                }

                Log.d("SetBoletaSyncAlarm", String.format("Alarmas leidas %s y %s", hora1, hora2));

                Calendar horario1 = null, horario2 = null;

                if (!TextUtils.isEmpty(hora1)) {
                    //si tenemos un horario 1 configurado, armamos la instancia del calendario
                    horario1 = Calendar.getInstance();
                    horario1.setTimeInMillis(System.currentTimeMillis());
                    horario1.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hora1.substring(0, 2)));
                    horario1.set(Calendar.MINUTE, Integer.parseInt(hora1.substring(3, 5)));
                    horario1.set(Calendar.SECOND, 0);
                    horario1.set(Calendar.MILLISECOND, 0);
                }

                if (!TextUtils.isEmpty(hora2)) {
                    //si tenemos un horario 2 configurado, armamos la instancia del calendario
                    horario2 = Calendar.getInstance();
                    horario2.setTimeInMillis(System.currentTimeMillis());
                    horario2.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hora2.substring(0, 2)));
                    horario2.set(Calendar.MINUTE, Integer.parseInt(hora2.substring(3, 5)));
                    horario2.set(Calendar.SECOND, 0);
                    horario2.set(Calendar.MILLISECOND, 0);
                }

                //seteamos las alarmas
                String alarm = Context.ALARM_SERVICE;
                AlarmManager am = (AlarmManager) context.getSystemService(alarm);

                Intent intent = new Intent(SyncService.ACTION_SYNC);
                PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);

                int type = AlarmManager.RTC_WAKEUP;//por horario y q contemple la pantalla apagada
                long interval = AlarmManager.INTERVAL_DAY;//una vez al dia

                if (horario1 != null) {//seteamos la primer alarma
                    am.setRepeating(type, horario1.getTimeInMillis(), interval, pi);
                    Log.i("SetBoletaSyncAlarm", String.format("Alarma 1 (%s) de Boleta seteadas OK", hora1));
                }
                if (horario2 != null) {//seteamos la segunda alarma
                    am.setRepeating(type, horario2.getTimeInMillis(), interval, pi);
                    Log.i("SetBoletaSyncAlarm", String.format("Alarma 2 (%s) de Boleta seteadas OK", hora2));
                }

            } else {
                Log.d("SetBoletaSyncAlarm", "No se encontró CP de Boleta para setear las alarmas");
            }
        } catch (Exception ex) {
            Log.e("SetBoletaSyncAlarm", ex.toString());
        }

    }
}
