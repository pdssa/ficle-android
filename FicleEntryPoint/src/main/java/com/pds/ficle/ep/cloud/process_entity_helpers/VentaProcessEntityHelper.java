package com.pds.ficle.ep.cloud.process_entity_helpers;


import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.google.gson.Gson;
import com.pds.common.Config;
import com.pds.common.Logger;
import com.pds.common.VentaDetalleHelper;
import com.pds.common.VentasHelper;
import com.pds.ficle.ep.cloud.http_post_helpers.VentaHttpPostHelper;
import com.pds.ficle.ep.models.ApiResponse;
import com.pds.ficle.ep.models.syncSales.VentaDetalleRequestItemWA;
import com.pds.ficle.ep.models.syncSales.VentaRequestItemWA;
import com.pds.ficle.ep.models.syncSales.VentaRequestWA;
import com.pds.ficle.ep.models.syncSales.VentaResponseWA;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class VentaProcessEntityHelper extends ProcessEntityHelper {
    public VentaProcessEntityHelper(Context ctx, Config config) {
        super(ctx, config);
    }

    @Override
    protected void updateEntitySyncState(JSONObject jsonObject) throws JSONException {
        ContentValues cv = new ContentValues();
        cv.put(VentasHelper.VENTA_ID, jsonObject.getInt(VentasHelper.VENTA_ID));
        cv.put(VentasHelper.VENTA_SYNC_STATUS, "S");
        Uri uri = ContentUris.withAppendedId(Uri.parse("content://com.pds.ficle.ep.ventas.contentprovider/ventas"), jsonObject.getInt(VentasHelper.VENTA_ID));
        mCtx.getContentResolver().update(uri, cv, null, null);

        try {
            for (JSONObject det:
                    getDetails(jsonObject.getInt(VentasHelper.VENTA_ID))) {

                ContentValues cvDetails = new ContentValues();
                cvDetails.put(VentaDetalleHelper.VENTA_DET_ID, det.getInt(VentaDetalleHelper.VENTA_DET_ID));
                cvDetails.put(VentaDetalleHelper.COLUMN_SYNC_STATUS, "S");

                Uri uriDetails = ContentUris.withAppendedId(
                        Uri.parse("content://com.pds.ficle.ep.venta_detalles.contentprovider/venta_detalles"),
                        det.getInt(VentaDetalleHelper.VENTA_DET_ID));

                mCtx.getContentResolver().update(uriDetails, cvDetails, null, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }



    }

    @Override
    protected boolean isSuccess(String response) {
        VentaResponseWA resp = new Gson().fromJson(response, VentaResponseWA.class);
        return resp.getResponseCode() == 0;
    }

    @Override
    protected String getEntityType() {
        return "VENTAS";
    }

    @Override
    protected String getHttpPostDataToServer(String request) throws Exception {
        VentaHttpPostHelper helper = new VentaHttpPostHelper(mConfig);
        return helper.postDataToServer(request);
    }

    @Override
    protected String getJsonRequest() throws Exception {
        VentaRequestWA request = new VentaRequestWA();
        request.mAndroidId = mConfig.ANDROID_ID;
        request.mImei = mConfig.IMEI();
        request.mItems = new ArrayList<VentaRequestItemWA>();
        ArrayList<JSONObject> items = getPayload();

        if (items != null) {
            for (JSONObject item : items) {

                try{
                    VentaRequestItemWA requestItemWA = new VentaRequestItemWA();
                    long itemId = item.getLong(VentasHelper.VENTA_ID);
                    requestItemWA.setId(itemId);
                    requestItemWA.setTipo("VTA");
                    requestItemWA.setCantidad(item.getInt(VentasHelper.VENTA_CANTIDAD));
                    requestItemWA.setFecha(item.getString(VentasHelper.VENTA_FECHA));
                    requestItemWA.setTotal(item.getDouble(VentasHelper.VENTA_TOTAL));
                    requestItemWA.setHora(item.getString(VentasHelper.VENTA_HORA));
                    requestItemWA.setMedioPago(item.getString(VentasHelper.VENTA_MEDIO_PAGO));
                    requestItemWA.setCodigo(item.getString(VentasHelper.VENTA_CODIGO));
                    requestItemWA.setUserId(item.getInt(VentasHelper.VENTA_USERID));
                    requestItemWA.setFcType(item.getString(VentasHelper.VENTA_FC_TYPE));
                    requestItemWA.setFcId(item.getInt(VentasHelper.VENTA_FC_ID));
                    requestItemWA.setFcFolio(item.getString(VentasHelper.VENTA_FC_FOLIO));
                    requestItemWA.setFcValidation(item.getString(VentasHelper.VENTA_FC_VALIDATION));
                    requestItemWA.setNeto(item.getDouble(VentasHelper.VENTA_NETO));
                    requestItemWA.setIva(item.getDouble(VentasHelper.VENTA_IVA));
                    requestItemWA.setTimbre(item.getString(VentasHelper.VENTA_TIMBRE));
                    requestItemWA.setStatus(item.getInt(VentasHelper.VENTA_ANULADA));

                    ArrayList<JSONObject> details = getDetails(itemId);
                    if (details != null) {
                        for (JSONObject det:
                                details) {

                            VentaDetalleRequestItemWA detail = new VentaDetalleRequestItemWA();

                            detail.setId(det.getInt(VentaDetalleHelper.VENTA_DET_ID));
                            detail.setOrigId(det.getInt(VentaDetalleHelper.VENTA_DET_VENTA_ID));
                            detail.setProductId(det.getInt(VentaDetalleHelper.VENTA_DET_PRODUCTO_ID));
                            detail.setCantidad(det.getInt(VentaDetalleHelper.VENTA_DET_CANTIDAD));
                            detail.setPrecio(det.getDouble(VentaDetalleHelper.VENTA_DET_PRECIO));
                            detail.setTotal(det.getDouble(VentaDetalleHelper.VENTA_DET_TOTAL));
                            detail.setSku(det.getString(VentaDetalleHelper.VENTA_DET_SKU));
                            detail.setNeto(det.getDouble(VentaDetalleHelper.VENTA_DET_NETO));
                            detail.setIva(det.getDouble(VentaDetalleHelper.VENTA_DET_IVA));


                            requestItemWA.addDetail(detail);
                        }
                    }



                    request.mItems.add(requestItemWA);

                }catch (Exception ex){
                    Log.e("error", ex.getMessage() + "" + ex.toString(), ex);
                    Logger.RegistrarEvento(mCtx, "e", "SyncProcess", ex.getMessage() + "" + ex.toString(), "Ent:" + getEntityType());
                }
            }
            return new Gson().toJson(request);
        }
        return null;
    }

    @Override
    protected ArrayList<JSONObject> getPayload() throws Exception {
        Cursor ventas_cursor = getCursor(
                "content://com.pds.ficle.ep.ventas.contentprovider/ventas",
                VentasHelper.columnas,
                VentasHelper.VENTA_SYNC_STATUS + "= 'N' ",
                "");
        return  cursorToJSONWebApi(ventas_cursor);
    }


    protected ArrayList<JSONObject> getDetails(long itemId) throws Exception {
        Cursor cursor = getCursor(
                "content://com.pds.ficle.ep.venta_detalles.contentprovider/venta_detalles",
                VentaDetalleHelper.columnas,
                VentaDetalleHelper.COLUMN_SYNC_STATUS + "= 'N' and "+VentaDetalleHelper.VENTA_DET_VENTA_ID+"= "+itemId,
                "");


        return  cursorToJSONWebApi(cursor);
    }



}
