package com.pds.ficle.ep.utils.migration;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Xml;

import com.pds.common.CajaHelper;
import com.pds.common.VentasHelper;
import com.pds.common.db.CompraTable;
import com.pds.common.db.ProductTable;

import org.xmlpull.v1.XmlPullParser;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hernan on 13/01/2015.
 */
public class DbImport {

    private String mDestXmlPath = "/sdcard/export.xml";

    private SQLiteDatabase mDb;
    private Importer mImporter;

    public interface MigrationInterface {
        void onTableImported(String message);
    }

    private MigrationInterface _migrationInterface;

    public DbImport(SQLiteDatabase db, String destXml) {
        mDb = db;
        mDestXmlPath = destXml;
    }

    public int ImportData(MigrationInterface migrationInterface) {
        int cant_tablas = 0;

        _migrationInterface = migrationInterface;

        try {

            migrationInterface.onTableImported("    VERIFICANDO ESTADO DE LA BASE DE DATOS DEL EQUIPO...");

            if (!checkStatusDB()) {
                throw new Exception("BASE DE DATOS NO SE ENCUENTRA VACÍA! Por favor, limpiar DATOS del EntryPoint desde Configuración de Android -> Aplicaciones");
            }

            migrationInterface.onTableImported("        BASE DE DATOS OK...");


            migrationInterface.onTableImported("    OBTENIENDO TABLAS EXPORTADAS...");

            mImporter = new Importer(mDestXmlPath);

            mImporter.ReadTables();

            for (File file : mImporter.getTables()) {

                XMLTable tb = mImporter.readXML(file);

                migrationInterface.onTableImported("    IMPORTANDO TABLA " + tb.name + " ...");

                int cant_rows = ImportTable(tb);

                migrationInterface.onTableImported(String.format("       CANTIDAD REGISTROS: %d ", cant_rows));

                cant_tablas++;
            }

        } catch (Exception ex) {
            migrationInterface.onTableImported("__    ERROR: " + ex.getMessage());
        }

        return cant_tablas;
    }

    private int ImportTable(XMLTable table) {

        int cant_rows = 0;
        try {

            if (table.name.equals("log")) {
                //limpiamos primero la tabla de logs -> dado que se generan logs desde el inicio del EntryPoint
                mDb.execSQL("DELETE FROM log;");
            }

            //ejecutamos los insert de cada fila de la tabla
            for (XMLRow row : table.rowList) {
                try {
                    mDb.execSQL(row.toString(table.name));
                } catch (Exception ex) {
                    //throw new Exception(ex.getMessage() + " - " + row.toString(table.name));
                    //si da un error con alguna fila, mostramos el error y continuamos procesando la siguiente
                    _migrationInterface.onTableImported("__       ERROR: " + ex.getMessage() + " - " + row.toString(table.name));
                }
            }

            //ejecutamos los insert de la tabla
            //mDb.execSQL(table.toString());

            cant_rows = table.rowList.size();
        } catch (Exception ex) {
            _migrationInterface.onTableImported("__       ERROR: " + ex.getMessage());
        }

        return cant_rows;
    }

    private boolean checkStatusDB() {

        boolean empty = true;

        //verificamos que estén vacías algunas tablas: ventas, productos, caja, compras
        empty = checkEmptyTable(VentasHelper.TABLE_NAME);

        if (empty) {
            empty = checkEmptyTable(ProductTable.TABLE_NAME);

            if (empty) {
                empty = checkEmptyTable(CajaHelper.TABLE_NAME);

                if (empty) {
                    empty = checkEmptyTable(CompraTable.TABLE_NAME);
                }
            }
        }

        return empty;
    }

    private boolean checkEmptyTable(String tableName) {
        // get the tables out of the given sqlite database
        String sql = "SELECT COUNT(*) FROM " + tableName;

        Cursor cur = mDb.rawQuery(sql, new String[0]);
        cur.moveToFirst();

        return cur.getInt(0) == 0;
    }


    class Importer {

        private List<File> xmlTables;

        public List<File> getTables() {
            return xmlTables;
        }

        private String mDestXmlPath;

        public Importer(String destXmlPath) {
            mDestXmlPath = destXmlPath;
        }

        public void ReadTables() {

            String current_file = "";

            try {

                File dir = new File(mDestXmlPath);

                xmlTables = new ArrayList<File>();

                for (File xml : dir.listFiles()) {
                    try {
                        current_file = xml.getName();
                        xmlTables.add(xml);
                        //xmlTables.add(readXML(xml));
                    } catch (Exception ex) {
                        _migrationInterface.onTableImported("__    ERROR EN " + current_file + " : " + ex.getMessage());
                    }
                }

            } catch (Exception ex) {
                _migrationInterface.onTableImported("__    ERROR : " + ex.getMessage());
            }

        }

        public XMLTable readXML(File f) throws Exception {

            InputStream in = null;

            in = new FileInputStream(f);

            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();

            XMLTable table = new XMLTable();
            XMLRow row = null;
            XMLCol col = null;
            String text = "";

            // Iniciamos buscando el tag de inicio
            parser.require(XmlPullParser.START_TAG, null, "export-database");
            String tag = parser.getName();

            if (tag.equals("export-database")) {

                int eventType = parser.getEventType();
                while (eventType != XmlPullParser.END_DOCUMENT) {
                    String tagname = parser.getName();

                    switch (eventType) {
                        case XmlPullParser.START_TAG: {
                            if (tagname.equalsIgnoreCase("table")) {
                                table.name = parser.getAttributeValue(0);
                            } else if (tagname.equalsIgnoreCase("row")) {
                                row = new XMLRow();
                            } else if (tagname.equalsIgnoreCase("col")) {
                                col = new XMLCol();
                                col.name = parser.getAttributeValue(0);
                                text = ""; //limpiamos el valor
                            }
                        }
                        break;
                        case XmlPullParser.TEXT: {
                            text = parser.getText();
                        }
                        break;
                        case XmlPullParser.END_TAG: {
                            if (tagname.equalsIgnoreCase("row")) {
                                table.rowList.add(row);
                            } else if (tagname.equalsIgnoreCase("col")) {
                                col.setValue(text);

                                row.colList.add(col);
                            }
                        }
                        break;
                        default:
                            break;

                    }

                    eventType = parser.next();
                }
            }

            in.close();

            return table;
        }
    }

    class XMLTable {
        public String name;
        public List<XMLRow> rowList;

        public XMLTable() {
            rowList = new ArrayList<XMLRow>();
        }

        @Override
        public String toString() {
            String s = "";

            //concatenamos todas las instrucciones para ejecutarlas todas juntas
            for (XMLRow row : rowList) {
                s += row.toString(name);
            }

            return s;
        }
    }

    class XMLRow {
        public List<XMLCol> colList;

        public XMLRow() {
            colList = new ArrayList<XMLCol>();
        }

        public String toString(String tableName) {

            if (tableName.equals("config")) {
                return UpdateConfigStatement(tableName); //hacemos un update especial, para no pisar la config de la printer con la del terminal de origen
            } else if (tableName.equals("taxes")) {
                return UpdateStatement(tableName);
            } else if (tableName.equals("usuarios")) {

                //para el caso del user admin (_id = 1) debemos actualizar
                boolean esFilaAdmin = false;

                //buscamos si es la fila del user admin
                for (XMLCol col : colList) {
                    if (col.name.equals("_id") && col.getValue().equals("1")) {
                        esFilaAdmin = true;
                        break;
                    }
                }

                if (esFilaAdmin) {
                    return UpdateStatement(tableName);
                } else {
                    return InsertStatement(tableName);
                }

            } else {
                return InsertStatement(tableName);
            }
        }

        private String InsertStatement(String tableName) {
            String insert = "INSERT INTO " + tableName + " (";

            //recorremos todas las columnas para armar el insert explicito
            for (XMLCol col : colList) {
                insert += col.name + ",";
            }

            //quitamos la ultima coma, y preparamos para setear los datos
            insert = insert.substring(0, insert.length() - 1) + ") values (";

            for (XMLCol col : colList) {
                if (col.getValue().equals("null"))
                    insert += "null ,";
                else
                    insert += "'" + col.getValue().replace("'", "") + "',";
            }

            //cerramos el statement
            insert = insert.substring(0, insert.length() - 1) + ");";

            return insert;
        }

        private String UpdateStatement(String tableName) {
            String update = "UPDATE " + tableName + " SET ";

            //recorremos todas las columnas para armar el update explicito
            for (XMLCol col : colList) {

                if (!col.name.equals("_id")) { //no incluimos la columna _id en el update
                    if (col.getValue().equals("null"))
                        update += col.name + " = null ,";
                    else
                        update += col.name + " = '" + col.getValue() + "',";
                }
            }

            //continuamos el statement
            update = update.substring(0, update.length() - 1) + " ";

            //agregamos la condición del where
            for (XMLCol col : colList) {
                if (col.name.equals("_id")) {
                    update += "WHERE _id = " + col.getValue() + ";";
                    break;
                }
            }

            return update;
        }

        private String UpdateConfigStatement(String tableName) {
            String update = "UPDATE " + tableName + " SET ";

            //recorremos todas las columnas para armar el update explicito
            for (XMLCol col : colList) {

                if (!col.name.equals("_id") && !col.name.equals("printer_model")) { //no incluimos la columna _id en el update, ni la printer
                    if (col.getValue().equals("null"))
                        update += col.name + " = null ,";
                    else
                        update += col.name + " = '" + col.getValue() + "',";
                }
            }

            //continuamos el statement
            update = update.substring(0, update.length() - 1) + " ";

            //agregamos la condición del where
            for (XMLCol col : colList) {
                if (col.name.equals("_id")) {
                    update += "WHERE _id = " + col.getValue() + ";";
                    break;
                }
            }

            return update;
        }
    }

    class XMLCol {
        public String name;
        private String value;

        public String getValue() {
            return value.replace("'", "");
        }

        public void setValue(String value) {
            this.value = value.replace("'", "");
        }
    }
}
