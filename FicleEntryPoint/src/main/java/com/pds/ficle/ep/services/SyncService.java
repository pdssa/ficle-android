package com.pds.ficle.ep.services;

/**
 * Created by Hernan on 19/02/2015.
 */

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.Context;

import com.pds.ficle.ep.cloud.SyncDataTask;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * helper methods.
 */
public class SyncService extends BroadcastReceiver {
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    public static final String ACTION_SYNC = "com.pds.ficle.ep.SYNC_DATA_ACTION";

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startActionSync(Context context) {
        Intent intent = new Intent();
        intent.setAction(ACTION_SYNC);
        context.sendBroadcast(intent);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        //esta clase recibe el aviso que es necesario sincronizar datos y ejecuta dicha accion
        //use: SyncService.startActionSync(this);
        SyncDataTask syncDataTask = new SyncDataTask(context, null, false, true);
        syncDataTask.execute();
    }


}

