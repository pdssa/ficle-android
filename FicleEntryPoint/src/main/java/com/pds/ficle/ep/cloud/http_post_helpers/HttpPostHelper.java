package com.pds.ficle.ep.cloud.http_post_helpers;


import com.pds.common.Config;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public abstract class HttpPostHelper {
    final int CONNECTION_TIMEOUT = 7000;//the timeout until a connection is established
    final int SOCKET_TIMEOUT = 30000; //is the timeout for waiting for data

    protected String mServidor;
    private Config mConfig;

    public HttpPostHelper(Config config)
    {
        mConfig = config;
    }

    public String postDataToServer(String request) throws Exception {

        // 1. create HttpClient
        HttpClient httpclient = new DefaultHttpClient();
        HttpParams params = httpclient.getParams();
        HttpConnectionParams.setConnectionTimeout(params, CONNECTION_TIMEOUT);
        HttpConnectionParams.setSoTimeout(params, SOCKET_TIMEOUT);

        // 2. make POST request to the given URL
        mServidor = this.mConfig.CLOUD_SERVER_HOST;
        HttpPost httpPost = getHttpPostForThisRequest();


        // 5. set json to StringEntity
        StringEntity se = new StringEntity(request, "UTF-8");

        // 6. set httpPost Entity
        httpPost.setEntity(se);

        // 7. Set some headers to inform server about the type of the content
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");

        // 8. Execute POST request to the given URL
        HttpResponse httpResponse = httpclient.execute(httpPost);//TODO: acá tengo un bug por socket timeout...

        // 9. receive response as inputStream
        InputStream inputStream = null;
        inputStream = httpResponse.getEntity().getContent();

        // 10. convert inputstream to string
        String result = "";
        if (inputStream != null) {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            result = "";
            while ((line = bufferedReader.readLine()) != null)
                result += line;

            inputStream.close();
        } else
            result = "No ha sido posible el envio al servidor";

        return result;
    }

    protected abstract HttpPost getHttpPostForThisRequest();

}
