package com.pds.ficle.ep.utils.migration;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.Html;
import android.text.TextUtils;
import android.util.Xml;
import android.widget.TextView;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.DbHelper;
import com.pds.common.Formatos;
import com.pds.common.pref.PDVConfig;
import com.pds.common.pref.ValeConfig;
import com.pds.common.util.ContentProviders;

import org.xmlpull.v1.XmlPullParser;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android_serialport_api.Printer;

/**
 * Created by Hernan on 13/01/2015.
 */
public class MigrationTask extends AsyncTask<Void, String, String> {

    private Context _context;
    private HOST_MODE _host_mode;
    private String _folderName;
    private TextView txtResult;
    private int _fase;
    private boolean useExternalStorage;
    private String _sufixId;


    private List<String> _results;

    private String PRINTER_MODEL;

    private enum TYPE_FILE {
        APK_FILE,
        SQLITE_XML_FILE,
        CONFIG_XML_FILE,
        PICS_DIR
    }

    public enum HOST_MODE {
        SOURCE_HOST,
        DESTINATION_HOST
    }

    public MigrationTask(Context context, HOST_MODE host_mode, int fase, TextView txt_results, String printer_model, boolean storageAlternativo, String sufix) {
        this._context = context;
        this._host_mode = host_mode;
        this.txtResult = txt_results;
        this._fase = fase;
        this.PRINTER_MODEL = printer_model;
        this.useExternalStorage = storageAlternativo;
        this._sufixId = sufix;

        if (TextUtils.isEmpty(this.PRINTER_MODEL)) {
            this.PRINTER_MODEL = new Config(context).PRINTER_MODEL;
        }
    }

    // onPreExecute used to setup the AsyncTask.
    @Override
    protected void onPreExecute() {
        //Toast.makeText(_context, "Iniciando proceso..", Toast.LENGTH_SHORT).show();
        txtResult.setText(""); //limpiamos logs
        _results = new ArrayList<String>();

        AddMessage("INICIANDO PROCESO EN " + _host_mode.name(), true);

        _folderName = Formatos.FormateaDate(new Date(), Formatos.DateFormat);

        //la carpeta de origen, va a ser generada con fecha y los 4 ultimos del android id
        _folderName += "_" + this._sufixId;

        CheckUSBStatus();
    }

    @Override
    protected String doInBackground(Void... params) {
        try {

            if (_host_mode == HOST_MODE.SOURCE_HOST) {

                //PASO 1: OBTENER LOS APK's de las APPS INSTALADAS
                GetInstalledApps();

                //PASO 2: VOLCAR TABLAS SQLITE A XML
                ExportSQLiteDB();

                //PASO 3: VOLCAR CONFIG APPS A XML
                ExportSharedPreferences();

                //PASO 4: COPIA LOS PICTURES
                ExportPictures();

                return "PROCESO FINALIZADO. REVISE SI OCURRIERON ERRORES";

            }

            if (_host_mode == HOST_MODE.DESTINATION_HOST) {

                if (_fase == 1) {
                    //PASO 1: INSTALAR las APPS desde los APK's relevados
                    InstallApps();
                }

                if (_fase == 2) {
                    //PASO 2: IMPORTAR XML A SQLITE
                    ImportSQLiteDB();

                    //PASO 3: IMPORTAR CONFIG XML A APPS
                    ImportSharedPreferences();

                    //PASO 4: COPIA LOS PICTURES
                    ImportPictures();

                    return "PROCESO FINALIZADO. REVISE SI OCURRIERON ERRORES";
                }
            }

            return "";

        } catch (Exception ex) {
            return "ERROR: " + ex.toString();
        }
    }


    @Override
    protected void onProgressUpdate(String... p) {
        //Toast.makeText(_context, p[0], Toast.LENGTH_LONG).show();
        for (String s : p) {
            if (s.startsWith("__"))//prefijo que usamos para marcar que es un error
                AddError(s.replace("__", ""), true);
            else
                AddMessage(s, true);
        }
    }

    @Override
    protected void onPostExecute(String result) {

        if (!TextUtils.isEmpty(result)) {

            //Toast.makeText(_context, result, Toast.LENGTH_LONG).show();
            AddMessage(result, true);

            new AlertDialog.Builder(_context)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Proceso finalizado")
                    .setMessage("Desea imprimir los resultados?")
                    .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            printResults();
                        }

                    })
                    .setNegativeButton("NO", null)
                    .setCancelable(false)
                    .show();
        }
    }

    @Override
    protected void onCancelled(String s) {
        //Toast.makeText(_context, s, Toast.LENGTH_LONG).show();
        if (!TextUtils.isEmpty(s))
            AddMessage(s, true);
    }

    //region PASO 1
    private void GetInstalledApps() {

        publishProgress("PASO 1: RELEVANDO APLICACIONES");

        PackageManager pm = _context.getPackageManager();
        int cant_apk = 0;

        for (ApplicationInfo app : pm.getInstalledApplications(0)) {
            //app.packageName
            //app.sourceDir

            if (app.packageName.startsWith("com.pds.") && !app.packageName.equals("com.pds.ficle.ep")) {//excluimos el EntryPoint

                try {

                    publishProgress("    COPIANDO " + app.packageName + ".apk ...");

                    String filePath = GetUSBFilePath(TYPE_FILE.APK_FILE, app.packageName);

                    FileInputStream source = new FileInputStream(app.sourceDir);
                    FileOutputStream destination = new FileOutputStream(filePath);

                    // Transfer bytes from in to out
                    FileChannel inChannel = source.getChannel();
                    FileChannel outChannel = destination.getChannel();
                    inChannel.transferTo(0, inChannel.size(), outChannel);

                    source.close();
                    destination.close();

                    cant_apk++;
                } catch (Exception ex) {
                    //Log.e("MigrationTask", ex.getMessage());
                    publishProgress("__    ERROR: " + ex.getMessage());
                }
            }
        }

        if (cant_apk > 0) {
            publishProgress(String.format("    %d APLICACIONES COPIADAS", cant_apk));
        } else {
            publishProgress("__    NO SE PUDIERON COPIAR APLICACIONES");
        }
    }

    private void InstallApps() {

        publishProgress("PASO 1: INSTALANDO APLICACIONES");

        try {
            String apk_path = GetUSBPath(TYPE_FILE.APK_FILE);

            File apk_dir = new File(apk_path);

            final File[] apk_files = apk_dir.listFiles();

            final int apk_index = 0;

            if (apk_files.length > 0) {
                InstallApp(apk_files, apk_index, false);
            }

        } catch (Exception ex) {
            publishProgress("__    ERROR: " + ex.getMessage());
        }
    }

    private void InstallApp(final File[] apk_files, final int apk_index, final boolean reintento) {
        try {

            //hay alguna otra app para instalar??
            if (apk_index >= apk_files.length) {
                publishProgress("FASE 1: FINALIZADA. REVISE SI HAN OCURRIDO ERRORES");
                onPostExecute("PROCESO FINALIZADO. REVISE SI OCURRIERON ERRORES");
                return;
            }

            if (!apk_files[apk_index].getName().equals("com.pds.ficle.ep")) {


                if (!reintento)
                    publishProgress("    INSTALANDO " + apk_files[apk_index].getName() + " ...");
                else
                    publishProgress("    REINTENTANDO INSTALAR " + apk_files[apk_index].getName() + " ...");

                ((Activity) _context).runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        AlertDialog.Builder builder = new AlertDialog.Builder(_context);
                        if (!reintento)
                            builder.setTitle("INSTALANDO " + apk_files[apk_index].getName());
                        else
                            builder.setTitle("REINTENTANDO INSTALAR " + apk_files[apk_index].getName());
                        builder.setMessage("PRESIONE ACEPTAR AL FINALIZAR LA INSTALACION");
                        builder.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //checkeamos si fue instalado
                                boolean installed = checkInstalled(apk_files[apk_index].getName().replace(".apk", "").replace("_", "."));

                                if (installed) {
                                    publishProgress("       Aplicacion instalada OK");
                                    InstallApp(apk_files, apk_index + 1, false);//siguiente APK
                                } else if (!reintento) {
                                    InstallApp(apk_files, apk_index, true);//volvemos a intentar mismo APK
                                } else {
                                    //ya descartamos este apk
                                    publishProgress("__       Aplicacion no instalada");
                                    InstallApp(apk_files, apk_index + 1, false);//siguiente APK
                                }

                            }
                        });
                        builder.setCancelable(false);
                        builder.show();
                    }
                });
                //disparamos la instalacion
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(apk_files[apk_index]), "application/vnd.android.package-archive");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                this._context.startActivity(intent);

            /*
            Thread t = new Thread() {

                int intentos = 0;

                public void run() {

                    boolean installed = checkInstalled(apk_files[apk_index].getName().replace(".apk", "").replace("_", "."));
                    while (!installed && intentos < 3) {
                        intentos++;
                        publishProgress(String.format("       Aguardando instalacion (%d,%d)...", intentos, 3));
                        try {
                            this.sleep(5000);
                        } catch (InterruptedException e) {
                            Log.e("MigrationTask", e.getMessage());
                        }
                    }

                    //Now it has been installed
                    if (installed) {
                        publishProgress("       Aplicacion instalada OK");
                    } else {
                        publishProgress("__       Aplicacion no instalada");
                    }

                    InstallApp(apk_files, apk_index + 1);//siguiente APK
                }
            };
            t.start();
            */
            } else //NO INSTALAR ENTRY POINT
                InstallApp(apk_files, apk_index + 1, false);//siguiente APK

        } catch (Exception ex) {
            publishProgress("__       ERROR: " + ex.getMessage());
        }
    }

    //endregion

    //region PASO 2
    private void ExportSQLiteDB() {

        publishProgress("PASO 2: EXPORTANDO BASE DE DATOS");

        String xml_path = GetUSBPath(TYPE_FILE.SQLITE_XML_FILE);

        try {
            DbDump db = new DbDump(new DbHelper(_context).getWritableDatabase(), xml_path);

            int cant = db.exportData(false, new DbDump.MigrationInterface() {
                @Override
                public void onTableExported(String message) {
                    publishProgress(message);
                }
            });

            if (cant > 0) {
                publishProgress(String.format("    %d TABLAS EXPORTADAS", cant));
            } else {
                publishProgress("__    NO SE PUDIERON EXPORTAR TABLAS");
            }

        } catch (Exception ex) {
            //Log.e("MigrationTask", ex.getMessage());
            publishProgress("__    ERROR: " + ex.getMessage());
        }
    }

    private void ImportSQLiteDB() {

        publishProgress("PASO 2: IMPORTANDO BASE DE DATOS . ATENCION: Esta operación puede demorar varios minutos");

        String xml_path = GetUSBPath(TYPE_FILE.SQLITE_XML_FILE);

        try {
            DbImport db = new DbImport(new DbHelper(_context).getWritableDatabase(), xml_path);

            int cant = db.ImportData(new DbImport.MigrationInterface() {
                @Override
                public void onTableImported(String message) {
                    publishProgress(message);
                }
            });

            if (cant > 0) {
                publishProgress(String.format("    %d TABLAS IMPORTADAS", cant));
            } else {
                publishProgress("__    NO SE PUDIERON IMPORTAR TABLAS");
            }

        } catch (Exception ex) {
            //Log.e("MigrationTask", ex.getMessage());
            publishProgress("__    ERROR: " + ex.getMessage());
        }
    }
    //endregion

    //region PASO 3
    private void ExportSharedPreferences() {

        publishProgress("PASO 3: EXPORTANDO CONFIGURACION");

        //BOLETA ELECTRONICA
        /*publishProgress("    CONFIGURACION BOLETA");
        if (ContentProviders.ProviderIsAvailable(_context, BoletaConfig.CONTENT_PROVIDER.getAuthority())) {
            String filePath = GetUSBFilePath(TYPE_FILE.CONFIG_XML_FILE, "boleta");
            SaveConfigToXML("boleta", filePath, BoletaConfig.CONTENT_PROVIDER, BoletaConfig.PROJECTION);
        } else {
            publishProgress("__       ERROR: DATOS NO DISPONIBLES");
        }*/

        //PDV
        publishProgress("    CONFIGURACION PDV");
        if (ContentProviders.ProviderIsAvailable(_context, PDVConfig.CONTENT_PROVIDER.getAuthority())) {
            String filePath = GetUSBFilePath(TYPE_FILE.CONFIG_XML_FILE, "pdv");
            SaveConfigToXML("pdv", filePath, PDVConfig.CONTENT_PROVIDER, PDVConfig.PROJECTION);
        } else {
            publishProgress("__       ERROR: DATOS NO DISPONIBLES");
        }


        //VALE
        if (ContentProviders.ProviderIsAvailable(_context, ValeConfig.CONTENT_PROVIDER.getAuthority())) {
            publishProgress("    CONFIGURACION VALE");
            String filePath = GetUSBFilePath(TYPE_FILE.CONFIG_XML_FILE, "vale");
            SaveConfigToXML("vale", filePath, ValeConfig.CONTENT_PROVIDER, ValeConfig.PROJECTION);
        }
    }

    private void ImportSharedPreferences() {

        publishProgress("PASO 3: IMPORTANDO CONFIGURACION");

        //BOLETA ELECTRONICA
        /*publishProgress("    CONFIGURACION BOLETA");
        if (ContentProviders.ProviderIsAvailable(_context, BoletaConfig.CONTENT_PROVIDER.getAuthority())) {

            String filePath = GetUSBFilePath(TYPE_FILE.CONFIG_XML_FILE, "boleta");
            ImportConfigFromXML(filePath, BoletaConfig.CONTENT_PROVIDER);
        } else {
            publishProgress("__       ERROR: APLICACION NO DISPONIBLE");
        }*/

        //PDV
        publishProgress("    CONFIGURACION PDV");
        if (ContentProviders.ProviderIsAvailable(_context, PDVConfig.CONTENT_PROVIDER.getAuthority())) {
            String filePath = GetUSBFilePath(TYPE_FILE.CONFIG_XML_FILE, "pdv");
            ImportConfigFromXML(filePath, PDVConfig.CONTENT_PROVIDER);
        } else {
            publishProgress("__       ERROR: APLICACION NO DISPONIBLE");
        }

        //VALE
        String filePath = GetUSBFilePath(TYPE_FILE.CONFIG_XML_FILE, "vale");
        if (new File(filePath).exists()) {
            publishProgress("    CONFIGURACION VALE");
            if (ContentProviders.ProviderIsAvailable(_context, ValeConfig.CONTENT_PROVIDER.getAuthority())) {
                ImportConfigFromXML(filePath, ValeConfig.CONTENT_PROVIDER);
            } else {
                publishProgress("__       ERROR: APLICACION NO DISPONIBLE");
            }
        }
    }
    //endregion

    //region PASO 4

    private void ExportPictures() {

        publishProgress("PASO 4: EXPORTANDO IMAGENES");

        String srcDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath();
        String destDir = GetUSBPath(TYPE_FILE.PICS_DIR);

        try {

            copyDirectoryOneLocationToAnotherLocation(new File(srcDir), new File(destDir));

        } catch (Exception ex) {
            //Log.e("MigrationTask", ex.getMessage());
            publishProgress("__    ERROR: " + ex.getMessage());
        }
    }

    private void ImportPictures() {
        publishProgress("PASO 4: IMPORTANDO IMAGENES");

        String srcDir = GetUSBPath(TYPE_FILE.PICS_DIR);
        String destDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath();

        try {

            copyDirectoryOneLocationToAnotherLocation(new File(srcDir), new File(destDir));

        } catch (Exception ex) {
            //Log.e("MigrationTask", ex.getMessage());
            publishProgress("__    ERROR: " + ex.getMessage());
        }
    }

    private static void copyDirectoryOneLocationToAnotherLocation(File sourceLocation, File targetLocation) throws Exception {

        if (sourceLocation.isDirectory()) {
            if (!targetLocation.exists()) {
                targetLocation.mkdir();
            }

            String[] children = sourceLocation.list();
            for (int i = 0; i < sourceLocation.listFiles().length; i++) {

                copyDirectoryOneLocationToAnotherLocation(new File(sourceLocation, children[i]),
                        new File(targetLocation, children[i]));
            }
        } else {

            InputStream in = new FileInputStream(sourceLocation);
            OutputStream out = new FileOutputStream(targetLocation);

            // Copy the bits from instream to outstream
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        }

    }

    //endregion

    private void AddMessage(String message, boolean newLine) {
        //txtResult.append(message + (newLine ? "\n" : ""));
        _results.add(message);
        txtResult.append(Html.fromHtml(message + (newLine ? "<br/>" : "")));
    }

    private void AddError(String message, boolean newLine) {
        _results.add(message);
        txtResult.append(Html.fromHtml("<font color=#E33633>" + message + "</font>" + (newLine ? "<br/>" : "")));

    }

    private void CheckUSBStatus() {

        try {

            //si es HOST revisamos que exista la carpeta de origen
            if (_host_mode == HOST_MODE.DESTINATION_HOST) {

                String path = GetStoragePath() + _folderName + "/";

                File dir = new File(path);

                if (!dir.exists()) {
                    AddError("VERIFICANDO USB... -> DIRECTORIO DE ORIGEN " + _folderName + " NO ENCONTRADO", true);

                    this.cancel(true);

                } else {
                    AddMessage("VERIFICANDO USB... -> OK", true);
                }
            } else {
                AddMessage("VERIFICANDO USB...", false);

                String path = GetStoragePath() + _folderName;

                File dir = new File(path + "/");

                if (dir.exists()) {//el directorio ya existe, tenemos que renombrarlo

                    int i = 0;

                    String newName = path + "-bak";

                    File newDir = new File(newName + "/");

                    while (newDir.exists()) {

                        i++;

                        newName = path + "-bak" + (i > 0 ? String.valueOf(i) : "");

                        newDir = new File(newName + "/");

                    }

                    if (dir.renameTo(newDir))
                        AddMessage(" -> DIRECTORIO DE ORIGEN " + _folderName + " RENOMBRADO", true);
                    else {
                        AddError(" -> DIRECTORIO DE ORIGEN EXISTENTE", true);

                        this.cancel(true);
                    }
                } else {
                    AddMessage(" -> OK", true);
                }

            }
        } catch (Exception ex) {
            AddError("ERROR: " + ex.getMessage(), true);

            this.cancel(true);
        }
    }

    private String GetUSBFilePath(TYPE_FILE type_file, String fileName) {
        String path = GetUSBPath(type_file);
        String extension = "";

        if (type_file == TYPE_FILE.APK_FILE)
            extension = ".apk";
        else if (type_file == TYPE_FILE.SQLITE_XML_FILE)
            extension = ".xml";
        else if (type_file == TYPE_FILE.CONFIG_XML_FILE)
            extension = ".xml";

        return path + fileName.replace('.', '_') + extension;
    }

    private String GetUSBPath(TYPE_FILE type_file) {
        String path = GetStoragePath() + _folderName;

        if (type_file == TYPE_FILE.APK_FILE)
            path += "/apk/";
        else if (type_file == TYPE_FILE.SQLITE_XML_FILE)
            path += "/sql/";
        else if (type_file == TYPE_FILE.CONFIG_XML_FILE)
            path += "/cfg/";
        else if (type_file == TYPE_FILE.PICS_DIR)
            path += "/pics/";
        else
            path += "/";

        File dir = new File(path);

        if (!dir.exists()) {//creamos el dir si no existe
            dir.mkdirs();
        }

        return path; // PATH = /mnt/usb_storage/1200/apk/
    }

    private String GetStoragePath() {
        if (useExternalStorage)
            return Environment.getExternalStorageDirectory().getAbsolutePath() + "/";//acceso a sdcard o memoria interna del terminal
        else
            return "/mnt/usb_storage/";//acceso a usb
    }

    private void SaveConfigToXML(String tag, String filePath, Uri providerUri, String[] columns) {

        String CLOSE_WITH_TICK = "'>";
        String START_XML = "<xml>";
        String END_XML = "</xml>";
        String START_TABLE = "<settings name='";
        String END_TABLE = "</settings>";
        String START_SETTING = "<setting name='";
        String END_SETTING = "</setting>";

        try {
            BufferedOutputStream mbufferos = new BufferedOutputStream(new FileOutputStream(filePath));

            mbufferos.write(START_XML.getBytes());

            Cursor cursor = _context.getContentResolver().query(providerUri, columns, null, null, null);

            if (cursor.moveToFirst()) {

                String temp = START_TABLE + tag + CLOSE_WITH_TICK;
                mbufferos.write(temp.getBytes());

                String[] properties = cursor.getColumnNames();

                for (String property : properties) {

                    temp = START_SETTING + property + CLOSE_WITH_TICK;
                    mbufferos.write(temp.getBytes());

                    int i = cursor.getColumnIndex(property);
                    String value = cursor.getString(i);

                    mbufferos.write(value.getBytes());

                    mbufferos.write(END_SETTING.getBytes());

                }

                mbufferos.write(END_TABLE.getBytes());
            }

            mbufferos.write(END_XML.getBytes());

            if (mbufferos != null) {
                mbufferos.close();
            }
        } catch (Exception ex) {
            //Log.e("MigrationTask", ex.toString());
            publishProgress("__    ERROR: " + ex.toString());
        }
    }

    private void ImportConfigFromXML(String filePath, Uri providerUri) {

        try {
            InputStream in = new FileInputStream(filePath);

            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();

            // Iniciamos buscando el tag de inicio
            parser.require(XmlPullParser.START_TAG, null, "xml");
            String tag = parser.getName();

            ContentValues cv = null;
            String text = "";
            String name = "";

            if (tag.equals("xml")) {

                int eventType = parser.getEventType();
                while (eventType != XmlPullParser.END_DOCUMENT) {
                    String tagname = parser.getName();

                    switch (eventType) {
                        case XmlPullParser.START_TAG: {
                            if (tagname.equalsIgnoreCase("settings")) {
                                cv = new ContentValues();
                            } else if (tagname.equalsIgnoreCase("setting")) {
                                name = parser.getAttributeValue(0);
                            }
                        }
                        break;
                        case XmlPullParser.TEXT: {
                            text = parser.getText();
                        }
                        break;
                        case XmlPullParser.END_TAG: {
                            if (tagname.equalsIgnoreCase("setting")) {
                                cv.put(name, text);
                            }
                        }
                        break;
                        default:
                            break;

                    }

                    eventType = parser.next();
                }
            }

            in.close();

            _context.getContentResolver().insert(providerUri, cv);

        } catch (Exception ex) {
            //Log.e("MigrationTask", ex.getMessage());
            publishProgress("__    ERROR: " + ex.toString());
        }
    }

    private boolean checkInstalled(String packageName) {

        PackageManager pm = _context.getPackageManager();
        boolean app_installed = false;
        try {
            pm.getPackageInfo(packageName, 0);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

    private void printResults() {
        try {
            final Printer printer = Printer.getInstance(this.PRINTER_MODEL, _context);

            printer.delayActivated = true; //activamos el delay, porque es mucho texto
            printer.printerCallback = new Printer.PrinterCallback() {
                @Override
                public void onPrinterReady() {

                    printer.printLine("MIGRACION " + Formatos.FormateaDate(new Date(), Formatos.FullDateTimeFormatNoSeconds));
                    printer.lineFeed();

                    for (String s : _results) {
                        printer.printLine(s);
                    }

                    printer.footer();
                }
            };

            if (!printer.initialize()) {
                Toast.makeText(_context, "Error al inicializar", Toast.LENGTH_SHORT).show();
                printer.end();
            }

        } catch (Exception ex) {
            Toast.makeText(_context, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
