package com.pds.ficle.ep.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.pds.ficle.ep.LoginActivity;

/**
 * Created by Hernan on 12/04/2014.
 */
public class PackageReceiver extends BroadcastReceiver {

    private String TAG = "PKG_RECEIVER";

    private String ACCION_REMOVED = "android.intent.action.PACKAGE_REMOVED";
    private String ACCION_ADDED = "android.intent.action.PACKAGE_ADDED";
    private String ACCION_REPLACED = "android.intent.action.PACKAGE_REPLACED";

    @Override
    public void onReceive(Context context, Intent intent) {
        //referencia http://www.xinotes.net/notes/note/1335/
        //esta clase recibe el aviso que Android termino de instalar/actualizar una aplicacion

        boolean actualizacion = intent.getBooleanExtra(Intent.EXTRA_REPLACING, false);

        String action = intent.getAction();
        String data = intent.getData().toString();

        Log.d(TAG, "Action: " + action);
        Log.d(TAG, "Data: " + data);
        Log.d(TAG, "EXTRA_REPLACING?: " + actualizacion);

        if(actualizacion && action.equals(ACCION_REPLACED)){ //esperamos por el intent de REPLACED
            if(data.equalsIgnoreCase("package:com.pds.ficle.ep")){ //SI ES EL ENTRY POINT => LO VOLVEMOS A INICIAR
                Intent i = new Intent(context, LoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }
        }

    }

}
