package com.pds.ficle.ep.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import com.pds.ficle.ep.cloud.CheckDownloadTask;

/**
 * Created by jose on 7/1/16.
 */
public class DownloadService extends IntentService {

    private static final String TAG = "DownloadService";
    public static final String HOST = "__HOST";
    public static final String ANDROIDID = "__ANDROIDID";
    private Context context;

    public static Intent newDownloadServiceIntent(Context context, String host, String androidId){

        Intent intentservice = new Intent(context, DownloadService.class);
        intentservice.putExtra(DownloadService.ANDROIDID, androidId);
        intentservice.putExtra(DownloadService.HOST, host);

        return intentservice;
    }

    public DownloadService()
    {
        super(DownloadService.class.getName());
        this.context = this;
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        String host = intent.getStringExtra(HOST);
        String androidid = intent.getStringExtra(ANDROIDID);

        new CheckDownloadTask(context,host,androidid).execute();
    }

}