package com.pds.ficle.ep.cloud;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.text.Html;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.Formatos;
import com.pds.common.Logger;
import com.pds.ficle.ep.MainActivity;
import com.pds.ficle.ep.R;
import com.pds.ficle.ep.helpers.SessionHelper;

import java.lang.reflect.Method;
import java.util.Date;

/**
 * Created by Hernan on 12/06/2014.
 */
public class SyncDataTaskEP extends SyncDataTask {

    public SyncDataTaskEP(Context _context, String callbackMethod, boolean onExit, boolean silentMode, boolean activated) {
        super(_context, callbackMethod, onExit, silentMode);
        //activated = true;//TEST: para forzar el sync
        //vemos si corresponde actualizar
        if (activated)//si vino en true => lo consideramos sync forzado
            set_activateSync(true);
        else
            set_activateSync(SessionHelper.getInstance().hasToSyncData());
    }

    // onPreExecute used to setup the AsyncTask.
    @Override
    protected void onPreExecute() {
        //Toast.makeText(_context, "Consultando nuevas versiones..", Toast.LENGTH_SHORT).show();

        if (!_silentMode && isSyncActivated()) {
            ((MainActivity) _context).showProgress(true);
            ((MainActivity) _context).showProgressMessage("Preparando informacion a enviar..");
        }
    }

    @Override
    protected void onProgressUpdate(String... p) {
        //Toast.makeText(_context, p[0], Toast.LENGTH_LONG).show();
        if (!_silentMode)
            ((MainActivity) _context).showProgressMessage(p[0]);
    }

    // onPostExecute displays the results of the AsyncTask.
    @Override
    protected void onPostExecute(String result) {
        if (result.equals("ok"))//si finalizó OK => grabamos la ultima fecha de sync
            updateLastSync();
        else if(!result.equals("!ok") && !result.equals("mal"))//sino => registramos error (el !OK es el que no corresponde sync, no grabamos ese mensaje)
            Logger.RegistrarEvento(_context, "e", "SyncProcess", result);

        FinalizarEjecucion(false, result);
    }

    @Override
    protected void onCancelled(String s) {
        FinalizarEjecucion(true, s);
    }

    private void FinalizarEjecucion(boolean onCancelled, String message) {
        //((MainActivity)_context).showProgressMessage(result);
        if (!_silentMode && isSyncActivated())
            ((MainActivity) _context).showProgress(false);
/*
        try {
            if (this._callbackMethod != null) {
                Method accion = ((MainActivity) _context).getClass().getMethod(this._callbackMethod, null);
                accion.invoke(((MainActivity) _context), null);
            }
        } catch (Exception ex) {
            if (!_silentMode)
                AlertMessage("Error: " + ex.getMessage());//Toast.makeText(_context, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }

        if (onCancelled) {
            if (!_silentMode)
                AlertMessage(message);//Toast.makeText(_context, message, Toast.LENGTH_LONG).show();
        } else {
            //mostramos solo en caso de no haber finalizado correctamente
            if (!message.contains("ok") && !message.equals("mal") && !_silentMode && isSyncActivated())
                AlertMessage(message);//Toast.makeText(_context, message, Toast.LENGTH_SHORT).show();
        }

        if(!_syncingOnExit && !_alert) {//llamamos al siguiente proceso
            ((MainActivity) _context).Init_Flow(NEXT_STEP);
        }*/

        ((MainActivity) _context).Init_Flow(21);
    }

    private int NEXT_STEP = 4;

    private String ultSyncLabel(){
        //obtenemos la ultima fecha de sync
        String last_sync_time_string = _context.getSharedPreferences("com.pds.ficle.ep", Context.MODE_PRIVATE).getString("last_sync_time", "2010-01-01 00:00:00");

        Date last_sync_time = null;
        try {
            last_sync_time = Formatos.ObtieneDate(last_sync_time_string, Formatos.DbDateTimeFormat);

            return Formatos.FormateaDate(last_sync_time, Formatos.DateFormatSlashAndTime);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }



    private void updateLastSync() {

        //grabamos la ultima fecha de sync
        Date last_sync_time = new Date();
        SharedPreferences.Editor editor = _context.getSharedPreferences("com.pds.ficle.ep", Context.MODE_PRIVATE).edit();
        editor.putString("last_sync_time", Formatos.FormateaDate(last_sync_time, Formatos.DbDateTimeFormat));
        editor.commit();

    }

    boolean _alert = false;
    private void AlertMessage(String message) {

        _alert = true;

        if(_syncingOnExit){
            Toast.makeText(_context, message, Toast.LENGTH_SHORT).show();
        }
        else {
            AlertDialog.Builder builder = new AlertDialog.Builder(_context);
            builder.setTitle("CONEXION DE RED");
            builder.setIcon(R.drawable.wifi_icon);
            builder.setMessage(Html.fromHtml("<b>Atención: </b>" + message + "<br/>Fecha Ultima Conexión Exitosa: " + ultSyncLabel()));
            builder.setNeutralButton("REINTENTAR", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface _dialog, int which) {
                    _dialog.dismiss();

                    //cerramos EP
                    ((MainActivity) _context).SalirAccion("");
                }
            });
            builder.setPositiveButton("CANCELAR", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface _dialog, int which) {
                    _dialog.dismiss();

                    //llamamos al siguiente proceso
                    ((MainActivity) _context).Init_Flow(NEXT_STEP);
                }
            });
            builder.setNegativeButton("CONFIGURAR RED", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface _dialog, int which) {
                    _dialog.dismiss();

                    //cerramos EP
                    ((MainActivity) _context).SalirAccion(Settings.ACTION_WIFI_SETTINGS);

                    //abrimos los settings de Android
                    //_context.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));

                }
            });
            builder.setCancelable(false);
            builder.show();
        }
    }
}
