package com.pds.ficle.ep;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import com.pds.common.DbHelper;
import com.pds.common.LogHelper;

/**
 * Created by Hernan on 24/11/13.
 */
public class LogProvider extends ContentProvider {
    //para el UriMatcher
    private static final int LOGS = 1;
    private static final int LOG_ID = 2;
    private static final int LOG_SYNC = 3;

    private static final String AUTHORITY = "com.pds.ficle.ep.logs.contentprovider";

    private static final String BASE_PATH = "logs";
    private static final String BASE_PATH_SYNC = "logs_sync";

    //Uri: content://com.pds.ficle.ep.logs.contentprovider/logs
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);

    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/logs";
    public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/log";


    //inicializamos las reglas posibles del UriMatcher:
    //tres opciones: que no coincida, que coincida sin id => valor 1, que coincida con id => valor 2
    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {

        uriMatcher.addURI(AUTHORITY, BASE_PATH, LOGS);

        uriMatcher.addURI(AUTHORITY, BASE_PATH + "/#", LOG_ID);

        uriMatcher.addURI(AUTHORITY, BASE_PATH_SYNC, LOG_SYNC);
    }

    private DbHelper database;

    @Override
    public boolean onCreate() {
        database = new DbHelper(getContext());
        return false;
    }

    @Override
    public String getType(Uri uri) {
        int uriType = uriMatcher.match(uri);

        switch (uriType) {
            case LOGS:
                return "vnd.android.cursor.dir/vnd.pds.log";
            case LOG_ID:
                return "vnd.android.cursor.item/vnd.pds.log";
            case LOG_SYNC:
                return "vnd.android.cursor.dir/vnd.pds.log";
            default:
                return null;
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {

        int uriType = uriMatcher.match(uri);

        long new_id = 0;

        switch (uriType) {
            case LOGS:
                new_id = database.getWritableDatabase().insert(LogHelper.TABLE_NAME, null, values);
                break;
            case LOG_ID:
                break;
            default:
                throw new IllegalArgumentException("URI desconocida: " + uri);
        }

        return ContentUris.withAppendedId(CONTENT_URI, new_id);


    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int cont;

        int uriType = uriMatcher.match(uri);

        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();

        builder.setTables(LogHelper.TABLE_NAME);

        //Si es una consulta a un ID concreto construimos el WHERE
        String where = selection;
        switch (uriType) {
            case LOGS:
                break;
            case LOG_ID:
                where = LogHelper.LOG_ID + "=" + uri.getLastPathSegment();
                break;
            default:
                throw new IllegalArgumentException("URI desconocida: " + uri);
        }

        cont = database.getWritableDatabase().update(LogHelper.TABLE_NAME, values, where, selectionArgs);

        return cont;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        int limite = 50; //limitamos la cantidad de registros obtenidos
        int uriType = uriMatcher.match(uri);

        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();

        builder.setTables(LogHelper.TABLE_NAME);

        //Si es una consulta a un ID concreto construimos el WHERE
        switch (uriType) {
            case LOGS:
                break;
            case LOG_ID:
                builder.appendWhere(LogHelper.LOG_ID + "=" + uri.getLastPathSegment());
                break;
            case LOG_SYNC:
                break;
            default:
                throw new IllegalArgumentException("URI desconocida: " + uri);
        }

        Cursor cursor = builder.query(database.getWritableDatabase(), projection, selection, selectionArgs, null, null, sortOrder, uriType == LOG_SYNC ? null : String.valueOf(limite) );

        return cursor;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int cont;

        int uriType = uriMatcher.match(uri);

        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();

        builder.setTables(LogHelper.TABLE_NAME);

        //Si es una consulta a un ID concreto construimos el WHERE
        String where = selection;
        switch (uriType) {
            case LOGS:
                break;
            case LOG_ID:
                where = LogHelper.LOG_ID + "=" + uri.getLastPathSegment();
                break;
            default:
                throw new IllegalArgumentException("URI desconocida: " + uri);
        }

        cont = database.getWritableDatabase().delete(LogHelper.TABLE_NAME, where, selectionArgs);

        return cont;
    }

}

