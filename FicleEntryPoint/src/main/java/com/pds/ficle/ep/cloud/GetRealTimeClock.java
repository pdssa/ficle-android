package com.pds.ficle.ep.cloud;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.provider.Settings;

import com.pds.common.Formatos;
import com.pds.common.Logger;
import com.pds.common.util.ConnectivityUtils;
import com.pds.common.util.EventsFlow;
import com.pds.ficle.ep.MainActivity;

import android.os.SystemClock;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Date;

/**
 * Created by Hernan on 22/12/2014.
 */
public class GetRealTimeClock extends AsyncTask<Void, String, String> {

    private Context _context;
    private final String TIME_SERVER = "pool.ntp.org";
    private final int CONNECTION_TIMEOUT = 3000;//the timeout until a connection is established
    private int _intento_nro;
    public EventsFlow RESULT;

    public GetRealTimeClock(Context context, int intento_nro) {
        _context = context;
        _intento_nro = intento_nro;
    }

    private boolean GetTimeFromServer() {

        if (!ConnectivityUtils.isOnline(_context)) {
            return false;
        }

        SntpClient client = new SntpClient();
        if (client.requestTime(TIME_SERVER, CONNECTION_TIMEOUT)) {
            long serverTimeMilisec = client.getNtpTime() + SystemClock.elapsedRealtime() - client.getNtpTimeReference();
            Date serverTime = new Date(serverTimeMilisec);

            SharedPreferences.Editor editor = _context.getSharedPreferences("com.pds.ficle.ep", Context.MODE_PRIVATE).edit();
            editor.putString("last_server_time", Formatos.FormateaDate(serverTime, Formatos.DbDateTimeFormatTimeZone));
            editor.commit();

            return true;
        } else return false;
    }

    private boolean CompareServerVsLocalTime(boolean fecha_obtenida) {

        String last_server_time_string = _context.getSharedPreferences("com.pds.ficle.ep", Context.MODE_PRIVATE).getString("last_server_time", Formatos.FormateaDate(new Date(), Formatos.DbDateTimeFormat));

        try {
            Date last_server_time;
            //este IF es para mantener compatibilidad, por si no se contaba con la fecha guardada con el timezone
            if(last_server_time_string.length() > 19)
                last_server_time = Formatos.ObtieneDate(last_server_time_string, Formatos.DbDateTimeFormatTimeZone);
            else
                last_server_time = Formatos.ObtieneDate(last_server_time_string, Formatos.DbDateTimeFormat);

            Date local_time = new Date();

            //SI SE PUDO OBTENER LA FECHA => BUSCAMOS DIFERENCIA
            if (fecha_obtenida) {

                //calculamos la diferencia de tiempo
                long difference = 0;
                if (local_time.after(last_server_time))
                    difference = local_time.getTime() - last_server_time.getTime();
                else
                    difference = last_server_time.getTime() - local_time.getTime();

                //si la diferencia es mayor a 2 hs => consideramos que es necesario corregir
                long x = difference / 1000;
                //long seconds = x % 60;
                x /= 60;
                //long minutes = x % 60;
                x /= 60;
                long hours = x % 24;
                x /= 24;
                long days = x;

                return days == 0 && hours < 2;
            } else {
                //SINO SE PUDO OBTENER LA FECHA => NECESITAMOS QUE LA ACTUAL SEA POSTERIOR O IGUAL A LA ULTIMA REGISTRADA
                return !local_time.before(last_server_time);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    private boolean CheckDateTimeProcess() {
        boolean fecha_obtenida = GetTimeFromServer();

        return CompareServerVsLocalTime(fecha_obtenida);
    }

    // onPreExecute used to setup the AsyncTask.
    @Override
    protected void onPreExecute() {
        if(_intento_nro == 1) {
            ((MainActivity) _context).showProgress(true);
            ((MainActivity) _context).showProgressMessage("Iniciando terminal...");
        }
    }


    @Override
    protected String doInBackground(Void... params) {
        try {

            return CheckDateTimeProcess() ? "ok" : "mal";

        } catch (Exception e) {
            return "Error: " + e.getMessage();
        }
    }

    // onPostExecute displays the results of the AsyncTask.
    @Override
    protected void onPostExecute(String result) {

        ((MainActivity) _context).showProgress(false);

        if (!result.equals("ok")) {

            if (_intento_nro == 1) {
                //mostramos solo en caso de no haber finalizado correctamente
                String mensaje =
                        "Atención: la fecha actual del terminal no es correcta.<br/>Por favor, ajústela manualmente desde la configuración del sistema operativo.<br/>Presione Configurar para continuar..." +
                                "<br/>Pasos a seguir:<br/>1. Desactivar (en caso de esta activada) la opción \"Fecha y hora automáticas\"<br/>2. Establecer Fecha<br/>3. Establecer Hora<br/>4. Activar la opción \"Fecha y hora automáticas\" ";
                AlertWrongDate(Html.fromHtml(mensaje));

                //Fecha y hora automáticas -> OFF
                //Establecer Fecha
                //Establecer Hora
                //Fecha y hora automáticas -> ON
            } else
                Finalizar(EventsFlow.FECHAHORA_ATENDIDA_NOCORREGIDA);

        }
        else
            Finalizar(EventsFlow.FECHAHORA_CORREGIDA);

    }

    private void AlertWrongDate(Spanned message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(_context);
        builder.setTitle("Fecha actual");
        builder.setMessage(message);
        builder.setPositiveButton("Configurar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //abrimos las settings de date & time
                ((Activity) _context).startActivityForResult(new Intent(Settings.ACTION_DATE_SETTINGS), 0);

                ConsultarDateCorregida();
            }
        });
        builder.setCancelable(false);
        builder.show();
    }

    private void ConsultarDateCorregida() {
        new AlertDialog.Builder(_context)
                .setIcon(android.R.drawable.ic_menu_help)
                .setTitle("Fecha actual")
                .setMessage("Pudo llevar a cabo la correccion de la fecha y hora actual?")
                .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //segundo intento de control
                        //new GetRealTimeClock(_context, 2).execute(); //verificamos la hora actual
                        //_intento_nro = 2;
                        //execute();
                        ((MainActivity)_context).Init_Flow(1); //-> VAMOS A CONSULTAR DE NUEVO LA FECHA PARA VALIDAR
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Finalizar(EventsFlow.FECHAHORA_NOATENDIDA_NOCORREGIDA);
                    }
                })
                .setCancelable(false)
                .show();
    }

    private void Finalizar(EventsFlow result){
        RESULT = result;

        if(RESULT == EventsFlow.FECHAHORA_ATENDIDA_NOCORREGIDA)
            Logger.RegistrarEvento(_context, "e", "Error Fecha Hora", "Diferencia atendida no resuelta");

        if(RESULT == EventsFlow.FECHAHORA_NOATENDIDA_NOCORREGIDA)
            Logger.RegistrarEvento(_context, "e", "Error Fecha Hora", "Diferencia no resuelta");


        ((MainActivity)_context).Init_Flow(2);
    }

/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

    /**
     * {@hide}
     * <p/>
     * Simple SNTP client class for retrieving network time.
     * <p/>
     * Sample usage:
     * <pre>SntpClient client = new SntpClient();
     * if (client.requestTime("time.foo.com")) {
     *     long now = client.getNtpTime() + SystemClock.elapsedRealtime() - client.getNtpTimeReference();
     * }
     * </pre>
     */
    class SntpClient {
        private static final String TAG = "SntpClient";
        private static final int REFERENCE_TIME_OFFSET = 16;
        private static final int ORIGINATE_TIME_OFFSET = 24;
        private static final int RECEIVE_TIME_OFFSET = 32;
        private static final int TRANSMIT_TIME_OFFSET = 40;
        private static final int NTP_PACKET_SIZE = 48;
        private static final int NTP_PORT = 123;
        private static final int NTP_MODE_CLIENT = 3;
        private static final int NTP_VERSION = 3;
        // Number of seconds between Jan 1, 1900 and Jan 1, 1970
        // 70 years plus 17 leap days
        private static final long OFFSET_1900_TO_1970 = ((365L * 70L) + 17L) * 24L * 60L * 60L;
        // system time computed from NTP server response
        private long mNtpTime;
        // value of SystemClock.elapsedRealtime() corresponding to mNtpTime
        private long mNtpTimeReference;
        // round trip time in milliseconds
        private long mRoundTripTime;

        /**
         * Sends an SNTP request to the given host and processes the response.
         *
         * @param host    host name of the server.
         * @param timeout network timeout in milliseconds.
         * @return true if the transaction was successful.
         */
        public boolean requestTime(String host, int timeout) {
            DatagramSocket socket = null;
            try {
                socket = new DatagramSocket();
                socket.setSoTimeout(timeout);
                InetAddress address = InetAddress.getByName(host);
                byte[] buffer = new byte[NTP_PACKET_SIZE];
                DatagramPacket request = new DatagramPacket(buffer, buffer.length, address, NTP_PORT);
                // set mode = 3 (client) and version = 3
                // mode is in low 3 bits of first byte
                // version is in bits 3-5 of first byte
                buffer[0] = NTP_MODE_CLIENT | (NTP_VERSION << 3);
                // get current time and write it to the request packet
                long requestTime = System.currentTimeMillis();
                long requestTicks = SystemClock.elapsedRealtime();
                writeTimeStamp(buffer, TRANSMIT_TIME_OFFSET, requestTime);
                socket.send(request);
                // read the response
                DatagramPacket response = new DatagramPacket(buffer, buffer.length);
                socket.receive(response);
                long responseTicks = SystemClock.elapsedRealtime();
                long responseTime = requestTime + (responseTicks - requestTicks);
                // extract the results
                long originateTime = readTimeStamp(buffer, ORIGINATE_TIME_OFFSET);
                long receiveTime = readTimeStamp(buffer, RECEIVE_TIME_OFFSET);
                long transmitTime = readTimeStamp(buffer, TRANSMIT_TIME_OFFSET);
                long roundTripTime = responseTicks - requestTicks - (transmitTime - receiveTime);
                // receiveTime = originateTime + transit + skew
                // responseTime = transmitTime + transit - skew
                // clockOffset = ((receiveTime - originateTime) + (transmitTime - responseTime))/2
                //             = ((originateTime + transit + skew - originateTime) +
                //                (transmitTime - (transmitTime + transit - skew)))/2
                //             = ((transit + skew) + (transmitTime - transmitTime - transit + skew))/2
                //             = (transit + skew - transit + skew)/2
                //             = (2 * skew)/2 = skew
                long clockOffset = ((receiveTime - originateTime) + (transmitTime - responseTime)) / 2;
                // if (false) Log.d(TAG, "round trip: " + roundTripTime + " ms");
                // if (false) Log.d(TAG, "clock offset: " + clockOffset + " ms");
                // save our results - use the times on this side of the network latency
                // (response rather than request time)
                mNtpTime = responseTime + clockOffset;
                mNtpTimeReference = responseTicks;
                mRoundTripTime = roundTripTime;
            } catch (Exception e) {
                Log.d(TAG, "request time failed: " + e);
                return false;
            } finally {
                if (socket != null) {
                    socket.close();
                }
            }
            return true;
        }

        /**
         * Returns the time computed from the NTP transaction.
         *
         * @return time value computed from NTP server response.
         */
        public long getNtpTime() {
            return mNtpTime;
        }

        /**
         * Returns the reference clock value (value of SystemClock.elapsedRealtime())
         * corresponding to the NTP time.
         *
         * @return reference clock corresponding to the NTP time.
         */
        public long getNtpTimeReference() {
            return mNtpTimeReference;
        }

        /**
         * Returns the round trip time of the NTP transaction
         *
         * @return round trip time in milliseconds.
         */
        public long getRoundTripTime() {
            return mRoundTripTime;
        }

        /**
         * Reads an unsigned 32 bit big endian number from the given offset in the buffer.
         */
        private long read32(byte[] buffer, int offset) {
            byte b0 = buffer[offset];
            byte b1 = buffer[offset + 1];
            byte b2 = buffer[offset + 2];
            byte b3 = buffer[offset + 3];
            // convert signed bytes to unsigned values
            int i0 = ((b0 & 0x80) == 0x80 ? (b0 & 0x7F) + 0x80 : b0);
            int i1 = ((b1 & 0x80) == 0x80 ? (b1 & 0x7F) + 0x80 : b1);
            int i2 = ((b2 & 0x80) == 0x80 ? (b2 & 0x7F) + 0x80 : b2);
            int i3 = ((b3 & 0x80) == 0x80 ? (b3 & 0x7F) + 0x80 : b3);
            return ((long) i0 << 24) + ((long) i1 << 16) + ((long) i2 << 8) + (long) i3;
        }

        /**
         * Reads the NTP time stamp at the given offset in the buffer and returns
         * it as a system time (milliseconds since January 1, 1970).
         */
        private long readTimeStamp(byte[] buffer, int offset) {
            long seconds = read32(buffer, offset);
            long fraction = read32(buffer, offset + 4);
            return ((seconds - OFFSET_1900_TO_1970) * 1000) + ((fraction * 1000L) / 0x100000000L);
        }

        /**
         * Writes system time (milliseconds since January 1, 1970) as an NTP time stamp
         * at the given offset in the buffer.
         */
        private void writeTimeStamp(byte[] buffer, int offset, long time) {
            long seconds = time / 1000L;
            long milliseconds = time - seconds * 1000L;
            seconds += OFFSET_1900_TO_1970;
            // write seconds in big endian format
            buffer[offset++] = (byte) (seconds >> 24);
            buffer[offset++] = (byte) (seconds >> 16);
            buffer[offset++] = (byte) (seconds >> 8);
            buffer[offset++] = (byte) (seconds >> 0);
            long fraction = milliseconds * 0x100000000L / 1000L;
            // write fraction in big endian format
            buffer[offset++] = (byte) (fraction >> 24);
            buffer[offset++] = (byte) (fraction >> 16);
            buffer[offset++] = (byte) (fraction >> 8);
            // low order bits should be random data
            buffer[offset++] = (byte) (Math.random() * 255.0);
        }
    }
}
