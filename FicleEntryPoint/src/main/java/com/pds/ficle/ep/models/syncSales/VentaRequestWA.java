package com.pds.ficle.ep.models.syncSales;

import com.google.gson.annotations.SerializedName;
import com.pds.ficle.ep.models.BaseFicleRequestWA;

import java.util.ArrayList;

public class VentaRequestWA extends BaseFicleRequestWA {
    @SerializedName("Ventas")
    public ArrayList<VentaRequestItemWA> mItems;
}

