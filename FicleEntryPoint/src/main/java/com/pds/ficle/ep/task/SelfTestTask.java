package com.pds.ficle.ep.task;

import android.content.Context;
import android.os.AsyncTask;
import android.text.Html;
import android.text.TextUtils;
import android.widget.Toast;

import com.pds.common.Config;
import com.pds.common.SII.vale.util.ValeUtils;
import com.pds.ficle.ep.MainActivity;

import android_serialport_api.Printer;

/**
 * Created by Hernan on 22/04/2015.
 */
public class SelfTestTask extends AsyncTask<Void, CharSequence, String> {

    private Context _context;
    private String RESULT_OK = "ok";

    private int next_step;

    int segundos = 3;

    public SelfTestTask() {
    }

    public SelfTestTask(Context context, int nextStep) {
        this._context = context;
        this.next_step = nextStep;
    }

    // onPreExecute used to setup the AsyncTask.
    @Override
    protected void onPreExecute() {
        ((MainActivity) _context).showProgress(true);
        ((MainActivity) _context).showProgressMessage(Html.fromHtml("<center>INICIANDO SELF-TEST<br/>POR FAVOR AGUARDE</center>"));
    }

    @Override
    protected String doInBackground(Void... params) {

        try {

            //simulamos comprobacion de errores
            Thread.sleep(segundos * 1000);

            publishProgress(Html.fromHtml("<center>OBTENIENDO DATOS DEL EQUIPO<br/></center>"));

            Thread.sleep(segundos * 1000);

            publishProgress(Html.fromHtml("<center>IMPRIMIENDO SELF-TEST VALE ELECTRONICO<br/></center>"));

            Thread.sleep(2 * 1000);

            return RESULT_OK ;

        } catch (Exception e) {
            return "Error: " + e.toString();
        }
    }


    @Override
    protected void onProgressUpdate(CharSequence... p) {
        //Toast.makeText(_context, p[0], Toast.LENGTH_LONG).show();
        ((MainActivity) _context).showProgressMessage(p[0]);
    }

    private Config c;

    // onPostExecute displays the results of the AsyncTask.
    @Override
    protected void onPostExecute(String result) {

        /*FinalizarEjecucion();//TODO: BORRAR
        return;*/


        if (result.equals(RESULT_OK)) {//si finalizó OK => imprimimos el ticket de resultado
            c = new Config(_context, true);
            String modelo = c.PRINTER_MODEL;

            try {

                if(TextUtils.isEmpty(modelo) || modelo.equals("null"))
                    throw new Exception("Impresora no configurada");

                final Printer printer = Printer.getInstance(modelo, _context);

                printer.delayActivated = true;
                printer.printerCallback = new Printer.PrinterCallback() {
                    @Override
                    public void onPrinterReady() {

                        printer.sendSeparadorHorizontal('=');
                        printer.format(1, 1, Printer.ALINEACION.CENTER);
                        printer.printLine("SELF-TEST OK");
                        printer.sendSeparadorHorizontal('=');

                        printer.printLine("NRO.COMERCIO: " + c.ID_COMERCIO);
                        printer.printLine("NRO.COMERCIO VALE: " + c.SII_MERCHANT_ID);
                        printer.printLine("R.U.T.:       " + ValeUtils.FormatRUT(c.CLAVEFISCAL));
                        printer.printLine("MODO:         " + c.getValeModalidadLabel() );
                        printer.printLine("TIPO:         " + c.getConnectivityType());
                        printer.lineFeed();

                        printer.printLine("ID TERMINAL: " + c.SII_TERMINAL_ID);
                        printer.printLine("MODELO:      " + c.getModelName());
                        printer.printLine("VERSION:     " + ValeUtils.getAppVersion(_context));
                        printer.printLine("IMEI:        " + c.IMEI());
                        printer.printLine("S/N :        " + c.ANDROID_ID);

                        if(!TextUtils.isEmpty(c.SIM_SERIALNUMBER)) {
                            printer.printLine("SERIE SIM:   " + c.SIM_SERIALNUMBER);
                            printer.printLine("IMSI:        " + c.IMSI);
                            printer.printLine("SIM:         " + c.SIM_OPERATOR_NAME);
                        }

                        printer.lineFeed();
                        printer.sendSeparadorHorizontal('=');

                        printer.footer();

                        printer.end();
                    }
                };


                if (!printer.initialize()) {
                    Toast.makeText(_context, "Error al inicializar impresora", Toast.LENGTH_SHORT).show();
                    printer.end();
                }

            } catch (Exception ex) {
                Toast.makeText(_context, "Error al imprimir: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        FinalizarEjecucion();
    }

    @Override
    protected void onCancelled(String s) {
        Toast.makeText(_context, s, Toast.LENGTH_LONG).show();

        FinalizarEjecucion();
    }

    private void FinalizarEjecucion() {
        ((MainActivity) _context).showProgress(false);
        ((MainActivity) _context).Init_Flow(next_step);
    }

}
