package com.pds.ficle.ep.cloud;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Created by Hernan on 10/06/14.
 */
public class WebServiceRequest {
    public static final int CONNECTION_TIMEOUT = 20000;//the timeout until a connection is established
    public static final int SOCKET_TIMEOUT = 30000; //is the timeout for waiting for data

    public String callWebService(String url) throws IOException, Exception {
        final DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpParams params = httpClient.getParams();
        HttpConnectionParams.setConnectionTimeout(params, CONNECTION_TIMEOUT);
        HttpConnectionParams.setSoTimeout(params, SOCKET_TIMEOUT);

        HttpProtocolParams.setUseExpectContinue(httpClient.getParams(), true);

        //GET
        HttpGet httpGet = new HttpGet(url);


        // POST
        //HttpPost httpPost = new HttpPost(url);
        //httpPost.setHeader("Content-Type", "text/xml; charset=utf-8");

        String responseString = null;

        try {
            //
            //HttpEntity entity = new StringEntity(mensaje, HTTP.UTF_8);
            //httpPost.setEntity(entity);

            ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
                @Override
                public String handleResponse(HttpResponse httpResponse) throws ClientProtocolException, IOException {
                    HttpEntity entity = httpResponse.getEntity();

                    StringBuffer out = new StringBuffer();
                    byte[] b = EntityUtils.toByteArray(entity);
                    out.append(new String(b, 0, b.length));

                    return out.toString();
                }
            };

            responseString = httpClient.execute(httpGet, responseHandler);
        } catch (UnsupportedEncodingException uee) {
            throw new Exception(uee);
        } catch (ClientProtocolException cpe) {
            throw new Exception(cpe);
        } catch (IOException ioe) {
            throw ioe;
        } finally {
            //cerramos la conexion
            httpClient.getConnectionManager().shutdown();
        }
        return responseString;
    }
}
